<?php

	////////////////////////////////////////////////
	// Edit to bring live site down for maintenance. 
	////////////////////////////////////////////////
	$now = new DateTime;
	$down_date = new DateTime('2020-12-01'); // or e.g. 2016-01-01 21:00:02

	$begin = new DateTime('16:30');
	$end = new DateTime('17:30');

	if (explode('/', $_SERVER['REQUEST_URI'])[1] === 'urmc_reporter' && $now->diff($down_date)->days === 0 && $now >= $begin && $now <= $end)
	{
?>
		<img src="images/maintainance.png">
<?php
	}
	else
	{
		ob_start();			

		require_once('config.php');

		require_once('logic/'.$page.'.php');
		
		// if malicious activity was found while loading logic do not proceed
		// still deciding if I should lock user account and sign them out.
		if (isset($fatal_err_occured) && $fatal_err_occured)
		{
			$message = 'Fatal Error Occurred';
			
			require_once('templates/header.php');
		}
		else
		{
		
			
			require_once('templates/header.php');
			
			require_once('templates/'.$page.'.php');

			require_once('templates/footer.php');		
		}


		$db->close_db_conn();

		/////////////////////////////////
		// load phpword.  When I move to the page __DIR__ will need to be updated.
		////////////////////////////////
		// $vendorDirPath = realpath(__DIR__ . '/vendor');

		// if (file_exists($vendorDirPath . '/autoload.php')) {
		//     require($vendorDirPath . '/autoload.php');
		// } 


		ob_end_flush();
		exit;
	}


?>
