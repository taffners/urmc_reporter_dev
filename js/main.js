// globals
var  site_version = $('#site-version-num').html(),
     site_title = 'Molecular Diagnostics Infotrack';

require.config({
     urlArgs: 'version='+site_version
});

require(['router'], function(router)
{	     
	//////////////////////////////////////////////////////////////////////
     // stop submitting form with pressing enter for inputs with the turn-off-enter-key class  
     // Previously it was only for #make_run_template_form.  This is useful
     // for using the scanner.  Scanners add and enter key after scanning
     // and it will submit the form if I do not deactivate it.
     //////////////////////////////////////////////////////////////////////
     $('.turn-off-enter-key').on('keyup keypress', function(e) 
     {      
          var keyCode = e.keyCode || e.which;
          
          // keycode 13 is the return or enter key.
          if (keyCode === 13) 
          { 
               e.preventDefault();
               return false;
          }
     });
     
     // add title info for a standard format for the entire page for somethings
     $('.required-field').each(function()
     {
          $(this).prop('title','This is a required field');
     });

     $('.mulitple-field').each(function()
     {
          $(this).prop('title','Multiple can be selected');
     });


     // change check boxes required but only require one checked.
     var  requiredCheckboxes = $(':checkbox[required]');
     
     $(':checkbox[required]').on('change', function(e) 
     {
          var  checkboxGroup = requiredCheckboxes
                    .filter('[name="' + $(this).attr('name') + '"]'),
               isChecked = checkboxGroup
                    .is(':checked');               
               checkboxGroup.prop('required', !isChecked);
     });


     // start the router
     router.startRouting();

});
