define(['models/utils'],function(utils)
{
     // Set up a single page application router.
     var  _routes =   [
                         // Login user pages
                         {page: 'generic', controller:'generic'},
                         {page: 'home', controller:'home'},
                         {page: 'add_visit', controller:'add_visit'},
                         {page: 'knowledge_base', controller:'knowledge_base'},
                         {page: 'reports_unfinished', controller:'reports_unfinished'},
                         {page: 'visit_info', controller:'visit_info'},
                         {page: 'patient_info', controller:'patient_info'},
                         {page: 'add_patient', controller:'patient_info'},
                         {page: 'patient_found', controller:'patient_found'},
                         {page: 'verify_variants', controller: 'verify_variants'},
                         {page: 'show_edit_report', controller: 'show_edit_report'},
                         {page: 'add_variant_interpetation', controller: 'add_variant_interpetation'},
                         {page: 'review_report', controller: 'review_report'},
                         {page: 'add_tiers', controller: 'add_tiers'},
                         {page: 'observed_search_results', controller: 'observed_search_results'},
                         {page: 'patient_search_results', controller: 'patient_search_results'},
                         {page: 'search_observed_variant', controller: 'search_observed_variant'},
                         {page: 'search_patients', controller: 'search_patients'},
                         {page: 'add_interpretation', controller: 'add_interpretation'},
                         {page: 'check_required_confirm', controller: 'check_required_confirm'},
                         {page: 'add_summary', controller: 'add_summary'},
                         {page: 'pending_confirmations', controller: 'pending_confirmations'},
                         {page: 'update_tiers_kb', controller: 'update_tiers_kb'},

                         {page: 'add_DNA_conc', controller: 'generic'},
                         {page: 'add_amplicon_conc', controller: 'generic'},
                         {page: 'check_tsv_upload', controller: 'check_tsv_upload'},
                         {page: 'add_gene_interpetation', controller: 'add_gene_interpetation'},
                         {page: 'login', controller: 'login'},
                         {page: 'add_variant', controller: 'add_variant'},
                         {page: 'update_knowledge_base', controller: 'update_knowledge_base'},
                         {page: 'delete_gene_knowledge_base', controller: 'delete_gene_knowledge_base'},
                         {page: 'stop_reporting', controller: 'stop_reporting'},
                         {page: 'admin', controller: 'admin'},
                         {page: 'make_myeloid_run_sheet', controller: 'make_myeloid_run_sheet'},
                         {page: 'upload_NGS_data', controller: 'upload_NGS_data'},
                         {page: 'qc_strand_bias', controller: 'qc_strand_bias'},
                         {page: 'training', controller: 'training'},
                         {page: 'qc', controller: 'generic'},
                         {page: 'panel_info', controller: 'generic'},
                         
                         {page: 'review_control', controller: 'generic'},
                         {page: 'add_library_pool', controller: 'generic'},
                         {page: 'pool_download_run_template', controller: 'pool_download_run_template'},
                         {page: 'pool_dilution_worksheet', controller: 'pool_dilution_worksheet'},
                         {page: 'pool_make_run_template', controller: 'pool_make_run_template'},
                         {page: 'add_sample_log_book', controller: 'add_sample_log_book'},
                         {page: 'sample_log_book', controller: 'sample_log_book'},
                         {page: 'add_test', controller: 'add_test'},
                         {page: 'update_test', controller: 'update_test'},
                         {page: 'update_extraction_info', controller: 'update_test'},
                         {page: 'performance_audit', controller: 'performance_audit'},
                         {page: 'setup_tests', controller: 'generic'},
                         {page: 'setup_instruments', controller: 'generic'},
                         {page: 'add_instrument', controller: 'generic'},
                         {page: 'upload_validation_document', controller: 'upload_validation_document'},
                         {page: 'QC_variant_status', controller: 'QC_variant_status'},
                         {page: 'tech_audit', controller: 'generic'},
                         {page: 'dev_progress_report', controller: 'dev_progress_report'},
                         {page: 'pool_plan_torrent_run', controller: 'pool_plan_torrent_run'},
                         {page: 'pool_upload_data', controller: 'pool_upload_data'},
                         {page: 'pool_select_results', controller: 'generic'},
                         {page: 'pool_qc_backup_data', controller: 'generic'},
                         {page: 'pool_add_barcode', controller: 'pool_add_barcode'},
                         {page: 'check_thermo_apis', controller: 'check_thermo_apis'},
                         {page: 'pool_download_excel', controller: 'pool_download_excel'},
                         {page: 'add_new_sample_type', controller: 'add_new_sample_type'},
                         {page: 'transfered_to_patient_chart', controller: 'transfered_to_patient_chart'},
                         {page: 'download_report', controller: 'transfered_to_patient_chart'},
                         {page: 'performance_audit_worksheet', controller: 'performance_audit_worksheet'},
                         {page: 'alter_default_tracked_tests', controller: 'alter_default_tracked_tests'},
                         {page: 'add_new_orderable_test', controller: 'add_new_orderable_test'},
                         {page: 'add_a_validation', controller: 'add_a_validation'},
                         {page: 'cummulative_month_audit', controller: 'cummulative_month_audit'},
                         {page: 'add_a_reflex_test_tracker', controller: 'add_a_reflex_test_tracker'},
                         {page: 'report_bug_fix', controller: 'report_bug_fix'},
                         {page: 'add_single_analyte_pool', controller: 'add_single_analyte_pool'},
                         {page: 'new_home', controller: 'new_home'},
                         {page: 'infotrack_test_report_steps', controller: 'infotrack_test_report_steps'},
                         {page: 'historic_controls', controller: 'historic_controls'},
                         {page: 'update_confirmation', controller: 'update_confirmation'}
                         
                         
                                     
                    ],
          // _default_page = 'login', // use this as default once login is built
          default_page = 'home',
          _php_vars = utils.PhpVars(),
          _page = '',
          _browser_ok,
          _test_status;

     function _startRouting(test_status)
     {
          // Purpose: Set route to default and start the hash check which starts the correct controller

          // if the browser is not chrome 
          utils.CheckBrowser(); 

          _hashCheck();
     }

     function _SetTestStatus(test_status)
     {
          _test_status = test_status;
     }

     function _hashCheck()
     {

          // Purpose: Check to see the anchor part of the url
          var  _i,
               _current_route,
               
               // new as of 2/11/2020.  Still use php_vars on most things.
               // since internet explorer does not support URL find if browser is supported before calling URL
               url = new URL(window.location.href),
               params = new URLSearchParams(url.search.slice(1)),
               _curr_page = params.get('page'),
               _found_router = false;

          // reset php_vars to current this is the old method of finding params before I know about URL and URLSearchParams
          _php_vars = utils.PhpVars();

          // check if hash has changed
          if (_curr_page != _page)
          {
               // if it changed find the controller of the changed hash and load that controller
               for (_i = 0; _current_route = _routes[_i++];)
               {

                    // example _current_route = {page: "pool_upload_data", controller: "pool_upload_data"}
                    if (_curr_page === _current_route.page)
                    {    
                         _found_router = true;
                         //////////////////////////////////////////////////////////////////////////////////
                         // Wait with loader active
                         // if php_vars has loader_only present then do not show the page only the loader.  Just show the loader
                         // and wait for the number of milliseconds that the loader_only is equal to
                         //////////////////////////////////////////////////////////////////////////////////
                         if (params.has('loader_only'))
                         {
                              // this will show the rainbow loader if not done in HTML.
                              $('#init-hide-loader').removeClass('hide').removeClass('d-none');

                              var  new_url = location.pathname+'?',
                                   timeout_milliseconds = params.get('loader_only');

                              // Make new url for reload after wait.
                              params.delete('loader_only')

                              new_url+= params.toString();

                              setTimeout(function()
                              {
                                 location.replace(new_url);
                                 _loadController(_current_route.controller);

                              }, timeout_milliseconds);
                         }
                         else
                         {      
                              // Turn of loader if header already was modified so the loader doesn't get stuck on
                              // This happens when downloading a file from php
                              if (_curr_page !== 'pool_download_excel')
                              {
                                   // show uploader when submit button is pushed
                                   $('form, .submit-show-loader').on('submit', function(e)
                                   {
                                        // do not activate rainbow loader if form did not validate
                                        $('#init-hide-loader').removeClass('hide').removeClass('d-none');
                                        $('.loader').show();
                                   });  
                              }                            

                              _loadController(_current_route.controller);
                         }

                         break;
                    }
               }




               // If a router was not found set to the default router generic
               if (!_found_router)
               {
                    // show uploader when submit button is pushed
                    $('form, .submit-show-loader').on('submit', function(e)
                    {
                         // do not activate rainbow loader if form did not validate
                         $('#init-hide-loader').show();
                    });


                    _loadController(_routes[0].controller);
               }

               // load common classes 
               require(['views/events/forms/common_form_classes'], function(common_form_classes)
               {                  
                    common_form_classes.StartEvents();
               });

               // set page to current page
               _page = _curr_page;
          }

     }

     function _loadController(controller_name)
     {
          // Purpose: Change the pages which are loaded if the hash was changed via require

          require(['controllers/' + controller_name], function(controller)
          {            

               controller.start(_test_status, _php_vars);
          });

     }

     function _SetLocalStorageVars()
     {
          //Purpose: Set Local Storage variables

          // make sure local storage is supported
          if (typeof(Storage) !== 'undefined')
          {

          }
          else
          {
               $('#message_center')
                    .attr('class', 'alert alert-danger show')
                    .append('Sorry, your browser does not support web storage...');
          }
     }

     function _GetCurrPage()
     {
          // Purpose: Get the current page in phpVars if page is not in php_vars make page the default_page.  If the home page or login is loaded make sure page is set to home or login.


          // load pages where page is defined and not equal to login or home
          if ( ('page' in _php_vars) && ( _php_vars['page'] != 'home' || _php_vars['page'] != 'login' ) )
          {
               return _php_vars['page'];
          }

          // load home page when home-check-page-here is found and page == home
          else if ( ('page' in _php_vars) &&
          ( $('#home-check-page-here').length > 0 )
          ( _php_vars['page'] == 'home'))
          {
               return _php_vars['page'];
          }

          // load home page when login-check-page-here is found and page == login
          else if ( ('page' in _php_vars) &&
          ( $('#login-check-page-here').length > 0 )
          ( _php_vars['page'] == 'login'))
          {
               return _php_vars['page'];
          }

          // if page is not defined redirect to login
          else
          {
               window.location.href = "?page=login";
          }

     }

     return {
          startRouting: _startRouting,
          hashCheck:_hashCheck,
          SetLocalStorageVars:_SetLocalStorageVars
     };

});
