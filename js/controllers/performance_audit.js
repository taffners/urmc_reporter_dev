define(['models/utils','models/google_charts'],function(utils, google_charts)
{
     function _BindEvents(php_vars)
     {
          // Toggle test that are shown in the table depending on if the checkbox is on or not
          $('.tracked_tests-ckbx').on('click', function()
          {
               var  checked = $(this).prop('checked'),
                    test_name_class = $(this).data('test_name'),
                    test_name = test_name_class.replace('test_', '');

               if (checked)
               {
                    // show the rows that match the test name checked
                    $('.'+test_name_class).show().removeClass('d-none');

                    // append test name to the all-tracked-samples-table-header-span
                    $('#all-tracked-samples-table-header-span')
                         .append(' <span id="'+test_name+'_table_header">'+test_name+'</span>');


               }
               else
               {
                    $('.'+test_name).hide().addClass('d-none'); 

                    // Remove test name to the all-tracked-samples-table-header-span
                    $('#'+test_name+'_table_header').remove();
                         
   
               }
          });


          // add summary of turn around times bar chart
          $.ajax(
          {
               type:'POST',
               url: 'utils/audits.php?',

               dataType: 'text',
               data: {
                         'query_filter': 'month_counts',
                         'filter':php_vars['filter'],
                         'month':php_vars['month'] != undefined ? php_vars['month'] : '',
                         'year':php_vars['year'] != undefined ? php_vars['year'] : ''
               },
               success: function(response)
               {
                    if (response ==='Post Does not contain query_filter')
                    {

                    }
                    else
                    {
                         var  json_response = JSON.parse(response),
                              chart_data = [['Month','passed', 'failed']],
                              chart_height = 100,
                              chart_height_per_month = 20,
                              curr_month,
                              div_id = 'month_counts';
                             
                         // Get all the possible curr_status
                         json_response.forEach(function(val, i)
                         {
                              chart_height += chart_height_per_month;   
                              curr_month = [val['month']+' '+val['year'], parseInt(val['passed_tests']), parseInt(val['failed_count'])];
                              chart_data.push(curr_month);
                         });   

                         $('#'+div_id).height(chart_height);    

                         google_charts.drawBarChart(div_id, 'Turn Around Times', chart_data, '', 'horizontal', 'Number of Tests');
                    }

               },
               error: function(textStatus, errorThrown)
               {
                    utils.dialog_window('problem with ajax call ;( ' + errorThrown + ' ' + textStatus);
               }
          });

          // add summary of tissue types bar chart
          $.ajax(
          {
               type:'POST',
               url: 'utils/audits.php?',

               dataType: 'text',
               data: {
                         'query_filter': 'tissue_counts',
                         'filter':php_vars['filter'],
                         'month':php_vars['month'] != undefined ? php_vars['month'] : '',
                         'year':php_vars['year'] != undefined ? php_vars['year'] : ''
               },
               success: function(response)
               {
                    if (response ==='Post Does not contain query_filter')
                    {

                    }
                    else
                    {

                         var  json_response = JSON.parse(response),
                              possible_tissues = [], 
                              months = [],
                              chart_data = [],                      
                              chart_height = 100,
                              chart_height_per_month = 15,
                              num_tissues,
                              curr_month,
                              adding_curr_month,
                              curr_counts,
                              div_id = 'tissue_counts',
                              i,j,k;
                    
                         // get all possible test_tissues
                         json_response.forEach(function(val, i)
                         {
                              if (!possible_tissues.includes(val['test_tissue']))
                              {    
                                   possible_tissues.push(val['test_tissue']);
                              }

                              curr_month = val['month']+' '+val['year'];
                              if (!months.includes(curr_month))
                              {    
                                   months.push(curr_month);
                              }
                         });

                         // add chart header to chart data
                         num_tissues = possible_tissues.length;

                         // If there are more than 5 tissue types the legend will not fit so increase the height for sample types larger than 5
                         if (num_tissues > 5 && num_tissues < 7)
                         {
                              num_tissues = num_tissues * 1.6;
                         }
                         else if (num_tissues >=7)
                         {
                              num_tissues = num_tissues * 2.5;
                         }

                         possible_tissues.sort();
                         possible_tissues.unshift('Month');
                         chart_data.push(possible_tissues);                            

                         // Get all of the data for each month
                         months.forEach(function(adding_curr_month,i)
                         {
                              chart_height += (chart_height_per_month * num_tissues);

                              curr_counts = Array(possible_tissues.length).fill(0);

                              json_response.forEach(function(val, j)
                              {
                                   // if this is the current month find index of test_tissue
                                   // and add it to curr_counts
                                   curr_month = val['month']+' '+val['year'];
                                   if (curr_month === adding_curr_month)
                                   {
                                        k = possible_tissues.indexOf(val['test_tissue']);

                                        curr_counts.splice(k,1, parseInt(val['count_test_tissue'])); 
                                   }
                              });
                              curr_counts.splice(0,1,adding_curr_month);
                              chart_data.push(curr_counts);

                         });

                         $('#'+div_id).height(chart_height);    

                         google_charts.drawBarChart(div_id, 'Counts of Tissues', chart_data, '', 'horizontal', 'Number of Tests');
                    }

               },
               error: function(textStatus, errorThrown)
               {
                    utils.dialog_window('problem with ajax call ;( ' + errorThrown + ' ' + textStatus);
               }
          });

           // add samples tissue per test
          $.ajax(
          {
               type:'POST',
               url: 'utils/audits.php?',

               dataType: 'text',
               data: {
                         'query_filter': 'samples_tissues_per_test',
                         'filter':php_vars['filter'],
                         'month':php_vars['month'] != undefined ? php_vars['month'] : '',
                         'year':php_vars['year'] != undefined ? php_vars['year'] : ''
               },
               success: function(response)
               {
                    if (response ==='Post Does not contain query_filter')
                    {

                    }
                    else
                    {

                         var  json_response = JSON.parse(response),
                              possible_tissues = [], 
                              tests = [],
                              chart_data = [],                      
                              chart_height = 100,
                              chart_height_per_test = 27,
                              num_tissues,
                              adding_curr_test,
                              curr_counts,
                              div_id = 'samples_tissues_per_test',
                              i,j,k;
                    
                         // get all possible test_tissues
                         json_response.forEach(function(val, i)
                         {
                              if (!possible_tissues.includes(val['test_tissue']))
                              {    
                                   possible_tissues.push(val['test_tissue']);
                              }

                              curr_test = val['test_name'];
                              if (!tests.includes(curr_test))
                              {    
                                   tests.push(curr_test);
                              }
                         });

                         // add chart header to chart data
                         num_tissues = possible_tissues.length;

                         possible_tissues.sort();
                         possible_tissues.unshift('Test');
                         chart_data.push(possible_tissues);                            

                         // Get all of the data for each test
                         tests.forEach(function(adding_curr_test,i)
                         {
                              chart_height += chart_height_per_test;

                              curr_counts = Array(possible_tissues.length).fill(0);

                              json_response.forEach(function(val, j)
                              {
                                   // if this is the current month find index of test_tissue
                                   // and add it to curr_counts
                                   curr_test = val['test_name'];
                                   if (curr_test === adding_curr_test)
                                   {
                                        k = possible_tissues.indexOf(val['test_tissue']);

                                        curr_counts.splice(k,1, parseInt(val['num_tissues'])); 
                                   }
                              });
                              curr_counts.splice(0,1,adding_curr_test);
                              chart_data.push(curr_counts);

                         });

                         $('#'+div_id).height(chart_height);    

                         google_charts.drawBarChart(div_id, 'Counts of Tissues Per Test', chart_data, '', 'horizontal', 'Number of Tissues', true);
                    }

               },
               error: function(textStatus, errorThrown)
               {
                    utils.dialog_window('problem with ajax call ;( ' + errorThrown + ' ' + textStatus);
               }
          });

          // add summary samples per test
          $.ajax(
          {
               type:'POST',
               url: 'utils/audits.php?',
               dataType: 'text',
               data: {
                         'query_filter': 'sampes_per_test',
                         'filter':php_vars['filter'],
                         'month':php_vars['month'] != undefined ? php_vars['month'] : '',
                         'year':php_vars['year'] != undefined ? php_vars['year'] : ''
               },
               success: function(response)
               {
                    if (response ==='Post Does not contain query_filter')
                    {

                    }
                    else
                    {
                         var  json_response = JSON.parse(response),
                              chart_data = [['Test','Count']],
                              chart_height = 100,
                              chart_height_per_test = 30,
                              curr_test,
                              div_id = 'sampes_per_test';
                             
                         // Get all the possible curr_status
                         json_response.forEach(function(val, i)
                         {
                              chart_height += chart_height_per_test;   
                              curr_test = [val['test_name'], parseInt(val['num_tests'])];
                              chart_data.push(curr_test);
                         });   

                         $('#'+div_id).height(chart_height);    

                         google_charts.drawBarChart(div_id, 'Number of Samples per Test', chart_data, '', 'horizontal', 'Number of Samples');
                    }

               },
               error: function(textStatus, errorThrown)
               {
                    utils.dialog_window('problem with ajax call ;( ' + errorThrown + ' ' + textStatus);
               }
          });

          // if the year or the month is changed redirect to new page
          $('#month, #year').on('change', function()
          {
               var  selected_month = $('#month').val(),
                    selected_year = $('#year').val();
               
               window.location.href = '?page=performance_audit&filter=months&month='+selected_month+'&year='+selected_year;

          });


          //////////////////////////////////////////////////////////////////
          // Edit Table Cell events
          //////////////////////////////////////////////////////////////////
          // start editing cell
          // Start editing cell
          // reference https://codewithmark.com/easily-edit-html-table-rows-or-cells-with-jquery
          var  DATE_BEFORE_EDIT;

          $('.row_data').on('click', function(event)
          {             
               event.preventDefault(); 

               var  row_div = $(this);             
               
               DATE_BEFORE_EDIT = row_div.html().trim();

               if($(this).attr('edit_type') == 'button')
               {
                    return false; 
               }

               //make div editable
               $(this).closest('div').attr('contenteditable', 'true');

               //add bg css
               $(this).addClass('bg-warning').css('padding','5px').css('color', 'black');

               $(this).focus();             
          });

          // save cell into database
          $('.row_data').on('focusout', function(event) 
          {
               event.preventDefault();

               var  row_id = $(this).closest('tr').attr('row_id'), 
                    row_div = $(this),             
                    col_name = row_div.attr('col_name'),
                    val = row_div.html().trim(),
                    ordered_test_id = $(this).attr('ordered_test_id'),
                    post_array = {};
                    post_array['col_val'] = val;
                    post_array['ordered_test_id'] = ordered_test_id;
                    post_array['col_name'] = col_name,
                    dateReg = /([12]\d{3}-(0[1-9]|1[0-2])-(0[1-9]|[12]\d|3[01]))/;

                    if (!val.match(dateReg) || val === DATE_BEFORE_EDIT)
                    {
                         row_div.removeClass('bg-warning').css('padding','').html(DATE_BEFORE_EDIT);
                    }
                    else
                    {
                         // Add data new date to ordered_test_table
                         $.ajax(
                         {
                              type:'POST',
                              url:'utils/ordered_test_update_dates.php',
                              dataType:'text',
                              data:post_array,
                              success: function(response)
                              {

                              },
                              error: function(textStatus, errorThrown)
                              {
                                   utils.dialog_window('problem with ajax call ;( ' + errorThrown + ' ' + textStatus);
                              }
                         });
                    }

               ////////////////////////////////////////////////////////
               // Add data to data base
                    // If row_id is not present add a new row to strand_bias_table
                         // Refresh page after entry so the rest of the fields
                         // will have the new strand_bias_id
                    // Make sure field is not empty and only numerical 
               ////////////////////////////////////////////////////////
               // if (utils.isPositveNumber(val))
               // {
               //      // use ajax to add to db
               //      $.ajax(
               //      {
               //           type:'POST',
               //           url: 'utils/update_strand_bias.php?',

               //           dataType: 'text',
               //           data: post_array,
               //           success: function(response)
               //           {
               //                var  sb_response = JSON.parse(response);

               //                // if sb_id originally empty reload page or strand_bias
               //                // might have changed
               //                if (sb_id == '' || sb_response['strand_bias'] != '')
               //                {
               //                     window.location.reload(true);
               //                }                                                 
               //           },
               //           error: function(textStatus, errorThrown)
               //           {
               //                utils.dialog_window('problem with ajax call ;( ' + errorThrown + ' ' + textStatus);
               //           }
               //      });  
               //      // if row_id is empty refresh page

               //      // if all ref -, ref +, variant -, and variant + are not empty 
               //      // calculate strand bias
               // }

               // else
               // {
               //      error_message = 'Enter an integer';
               //      utils.dialog_window(error_message, 'AlertId');
               //      row_div.html('___');
               // }
               row_div.removeClass('bg-warning').css('padding','');               
          });         
     }

     function _SetView()
     {

     }

     function _start(test_status, php_vars)
     {
          _SetView();
          _BindEvents(php_vars);

     }

     return {
          start:_start
     };
});
