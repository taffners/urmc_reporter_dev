define(['models/utils'],function(utils)
{
     function _BindEvents(php_vars)
     {
          $('input.unique-value-required').on('change',function()
          {
               var  all_values = [];

               $('input.unique-value-required').each(function()
               {
                    if (all_values.includes($(this).val()))
                    {
                         utils.dialog_window('The index is required to be unique for the pool.');
                         $(this).val('');
                    }
                    else
                    {
                         all_values.push($(this).val());
                    }
                    
               });
          });
     }

     function _SetView()
     {

     }

     function _start(php_vars)
     {
          _SetView();
          _BindEvents(php_vars);
     }

     return {
          start:_start
     };
});
