define(function()
{
     function _BindEvents(php_vars)
     {

          // add interpt from CIViC to gene text area
          $('.add-civic').on('click', function()
          {
               var  _gene = $(this).data('gene'),
                    _interpt = $(this).data('interpt');

               $('textarea#' + _gene).empty().text(_interpt); 
               $('button#add_gene_interpetation_submit').removeClass('disabled-link');
          });

          // Add to tables with a class of dataTable the sorting ability from bootstraps datatables https://datatables.net/manual/styling/bootstrap
          require(['views/events/TableEvents'], function(TableEvents)
          {
               TableEvents.StartEvents();
          });

          // enable submit button once a change has occurred in type=text fields
          require(['views/events/activate_submit_button'], function(activate_submit_button)
          {
               activate_submit_button.StartEvents();
          });

          // enable date field calendar popups
          require(['views/events/date_fields'], function(date_fields)
          {
               date_fields.StartEvents();
          });

          // // In the variant table cells larger than 20 characters are cut.  This will enable the more button and less button
          require(['views/events/toggle_teaser_complete_strs'], function(toggle_teaser_complete_strs)
          {
               toggle_teaser_complete_strs.StartEvents();
          });

          $('.add_interpt_on_click').on('click', function()
          {
               var  _gene = $(this).data('gene'),
                    _interpt = $(this).data('interpt');

               $('textarea#' + _gene).empty().text(_interpt); 
               $('button#add_gene_interpetation_submit').removeClass('disabled-link');                 
          });

     }

     function _SetView()
     {

     }

     function _start(php_vars)
     {
          _SetView();
          _BindEvents(php_vars);
     }

     return {
          start:_start
     };
});
