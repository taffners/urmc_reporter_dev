define(['models/utils'],function(utils)
{
     function _BindEvents(php_vars)
     {
          var  myeloid_info, coverage_file; 

          /////////////////////////////////////////////////////////////////
          // Check TSV file uploaded.  Each panel type needs a different
          // check.  A more through check will occur using PHP this check will 
          // just make sure the user choose the correct file.
          /////////////////////////////////////////////////////////////////          
          document.getElementById('validation-files-id').addEventListener('change', function()
          {
               utils.calculateMulitpleMD5FileHash('validation-files-id', 'md5-file-hash-div', 'md5_file_hash_array');            
          });

     }

     function _start(test_status, php_vars)
     {

          _BindEvents(php_vars);
     }

     return {
          start:_start
     };
});
