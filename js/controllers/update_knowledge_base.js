define(['models/utils'], function(utils)
{
     function _BindEvents(php_vars)
     {
          // get gene info from cosmic_tsv and update in knowledge base
          $('.update-gene-in-knowledge-base').on('click', function()
          {
               var gene_name = $(this).data('gene');

               // send gene to update to server
               $.ajax(
               {
                    type:'POST',
                    url:'utils/update_knowledge_base_for_gene.php?',
                    dataType:'json',
                    data:{
                         'gene': gene_name
                    },
                    success:function(response)
                    {
                         if (response['err'])
                         {
                              utils.dialog_window(response['err']);
                         }
                         else
                         {
                              // change background color of table.
                              $('#gene_update_cell_'+gene_name).css('background', '#22c222');

                              // dialog_window response
                              var dialog = 'The gene ' + response['gene'] + ' was just updated.  ' + response['num_inserted_snvs'] + ' snvs were inserted.  There are a total of ' + response['line_num'] + ' snvs in cosmic tsv.';
                              utils.dialog_window(dialog);
                         }
                    },
                    error: function(textStatus, errorThrown)
                    {
                         utils.dialog_window('problem with ajax call ;( ' + errorThrown + ' ' + textStatus);
                    }


               });
          });
     }

     function _SetView()
     {

     }

     function _start(php_vars)
     {
          _SetView();
          _BindEvents(php_vars);
     }

     return {
          start:_start
     };
});
