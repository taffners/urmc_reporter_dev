define(function()
{
     function _BindEvents(php_vars)
     {
         // insert activate button to insert and remove fields with a known field name.
          // require(['views/events/template_inputs_with_del_btn'], function(template_inputs_with_del_btn)
          // {
          //      template_inputs_with_del_btn.StartEvents(field_lens);
          // });

          // enable submit button once a change has occurred in type=text fields
          require(['views/events/activate_submit_button'], function(activate_submit_button)
          {
               activate_submit_button.StartEvents();
          });

          // Activate hotspot variants section
          $('input[name="hotspot_variants"]').on('change', function()
          {
               var  hotspots = $(this).val();

               if (hotspots == 'yes')
               {
                    $('#hot-spots-section').removeClass('d-none').show();
                    $('#hotspot_type').attr('required', true);
                    $('#hotspot_val_type').attr('required', true);
                    
               }
               else
               {
                    $('#hot-spots-section').hide();
                    $('#hotspot_type').removeAttr('required');
                    $('#hotspot_val_type').removeAttr('required');

                    $('#hotspot_type').attr("checked", false);
                    $('#hotspot_val_type').attr("checked", false);
               }
          });
          
          $('form').on('keyup keypress', function(e) 
          {
               var keyCode = e.keyCode || e.which;
               
               if (keyCode === 13) 
               { 
                    e.preventDefault();
                    return false;
               }
          });

          $('#assay_type_id').on('change', function()
          {
               if ($(this).val() == 9)
               {
                    $('#all-non-sendout-fields').hide(); 

                    $('#any-turnaround_time').removeAttr('required');
                    $('#any-turnaround_time').val('');

                    $('#Negative-turnaround_time').removeAttr('required');
                    $('#Negative-turnaround_time').val('');

                    $('#Positive-turnaround_time').removeAttr('required');
                    $('#Positive-turnaround_time').val('');                  
               }

               else
               {
                    $('#all-non-sendout-fields').show().removeClass('d-none');
               }

          });

          // Change 
          $('input[type=radio][name=include_in_turn_around_calculation]').on('change', function()
          {
               var  tat = $(this).val();

               if (tat === 'no')
               {
                    $('#turn-around-calculation-fields').hide();
                    $('.time-depends-on-result-no-required-input').removeAttr('required');
                    $('.time-depends-on-result-yes-required-input').removeAttr('required');
                    $('input[type=radio][name=turnaround_time_tracked]').removeAttr('required');
                    $('input[type=radio][name=turnaround_time_tracked][value="no"]').prop('checked', true);

                    $('input[type=radio][name=time_depends_on_result]').removeAttr('required');
                    $('input[type=radio][name=time_depends_on_result][value="no"]').prop('checked', true);

                    $('#any-turnaround_time').removeAttr('required');
                    $('#any-turnaround_time').val('');

                    $('#Negative-turnaround_time').removeAttr('required');
                    $('#Negative-turnaround_time').val('');

                    $('#Positive-turnaround_time').removeAttr('required');
                    $('#Positive-turnaround_time').val('');

                    $('input[type=radio][name=turnaround_time_depends_on_ordered_test]').removeAttr('required');
                    $('input[type=radio][name=turnaround_time_depends_on_ordered_test][value="no"]').prop('checked', true);
                    
                    $('#dependency_orderable_tests_id').removeAttr('required');
                    $('#dependency_orderable_tests_id').removeAttr('selected');
               }
               else
               {
                    $('#turn-around-calculation-fields').show().removeClass('d-none'); 

                    $('.time-depends-on-result-no-required-input').attr('required', true);
                    $('.time-depends-on-result-yes-required-input').attr('required', true);
                    $('input[type=radio][name=turnaround_time_tracked]').attr('required', true);
                    

                    $('input[type=radio][name=time_depends_on_result]').attr('required', true);


                    $('input[type=radio][name=turnaround_time_depends_on_ordered_test]').attr('required', true);
               }
          });

          $('input[type=radio][name=time_depends_on_result]').on('change', function()
          {
               var  tat = $(this).val();

               if (tat === 'no')
               {
                    $('#time-depends-on-result-yes').hide();
                    $('.time-depends-on-result-yes-required-input').removeAttr('required');


                    $('#time-depends-on-result-no').show().removeClass('d-none');
                    $('.time-depends-on-result-no-required-input').attr('required', true);
                    $('#Positive-turnaround_time').val('');
                    $('#Negative-turnaround_time').val('');
                   
               }
               else
               {
                    $('#time-depends-on-result-yes').show().removeClass('d-none');
                    $('.time-depends-on-result-yes-required-input').attr('required', true);


                    $('#time-depends-on-result-no').hide();
                    $('.time-depends-on-result-no-required-input').removeAttr('required');
                    $('#any-turnaround_time').val('');   
               }
          });

          $('input[type=radio][name=turnaround_time_depends_on_ordered_test]').on('change', function()
          {
               var  depend = $(this).val();

               if (depend === 'no')
               {
                    $('#depend-test-div').hide();
                    $('#dependency_orderable_tests_id').removeAttr('required');
               }
               else
               {
                    $('#depend-test-div').removeClass('d-none').show();
                    $('#dependency_orderable_tests_id').attr('required', true);
               }

          });

     }

     function _SetView()
     {
          // set up the form for updating.
          $(document).ready(function()
          {
               var  tat = $('input[type=radio][name=include_in_turn_around_calculation]:checked').val(),
                    hotspots = $('input[type=radio][name=hotspot_variants]:checked').val()
                    assay_type_id = $('#assay_type_id').val();

               if (hotspots == 'yes')
               {
                    $('#hot-spots-section').removeClass('d-none').show();

                    $('#hotspot_type').attr('required', true);
                    $('#hotspot_val_type').attr('required', true);

               }
               else
               {
                    $('#hot-spots-section').hide(); 

                    $('#hotspot_type').removeAttr('required');
                    $('#hotspot_val_type').removeAttr('required');

                    $('#hotspot_type').attr("checked", false);
                    $('#hotspot_val_type').attr("checked", false);

               }

               if (tat === 'no')
               {
                    $('#turn-around-calculation-fields').hide();
                    $('.time-depends-on-result-no-required-input').removeAttr('required');
                    $('.time-depends-on-result-yes-required-input').removeAttr('required');
                    $('input[type=radio][name=turnaround_time_tracked]').removeAttr('required');
                    $('input[type=radio][name=turnaround_time_tracked][value="no"]').prop('checked', true);

                    $('input[type=radio][name=time_depends_on_result]').removeAttr('required');
                    $('input[type=radio][name=time_depends_on_result][value="no"]').prop('checked', true);

                    $('#any-turnaround_time').removeAttr('required');
                    $('#any-turnaround_time').val('');

                    $('#Negative-turnaround_time').removeAttr('required');
                    $('#Negative-turnaround_time').val('');

                    $('#Positive-turnaround_time').removeAttr('required');
                    $('#Positive-turnaround_time').val('');

                    $('input[type=radio][name=turnaround_time_depends_on_ordered_test]').removeAttr('required');
                    $('input[type=radio][name=turnaround_time_depends_on_ordered_test][value="no"]').prop('checked', true);
                    
                    $('#dependency_orderable_tests_id').removeAttr('required');
                    $('#dependency_orderable_tests_id').removeAttr('selected');                  
               }


               if (assay_type_id == 9)
               {
                    $('#all-non-sendout-fields').hide(); 

                    $('#any-turnaround_time').removeAttr('required');
                    $('#any-turnaround_time').val('');

                    $('#Negative-turnaround_time').removeAttr('required');
                    $('#Negative-turnaround_time').val('');

                    $('#Positive-turnaround_time').removeAttr('required');
                    $('#Positive-turnaround_time').val('');                  
               }


               // if turnaround any is not filled in and turn around time positive is then required status needs to be updated
               if   (
                         $('#any-turnaround_time').val().length == 0 && 
                         $('#Positive-turnaround_time').val().length != 0 &&
                         $('#Negative-turnaround_time').val().length != 0
                    )
               {

                    $('#any-turnaround_time').removeAttr('required');                    
                    $('#Negative-turnaround_time').prop('required', true);
                    $('#Positive-turnaround_time').prop('required', true);
               }
               

          });


     }

     function _start(php_vars)
     {
          _SetView();
          _BindEvents(php_vars);
     }

     return {
          start:_start
     };
});
