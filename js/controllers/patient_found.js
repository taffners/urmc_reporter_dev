define(['models/utils'],function(utils)
{
     function _BindEvents()
     {

          // enable back button as going to previous page
          $('#back_button').on('click', function()
          {
               parent.history.back();
               return false;
          });

          // toggle progress report bar
          require(['views/events/toggle_progress_bar'], function(toggle_progress_bar)
          {
               toggle_progress_bar.StartEvents();
          });
     }

     function _SetView()
     {

     }

     function _start()
     {
          _SetView();
          _BindEvents();
     }

     return {
          start:_start
     };
});