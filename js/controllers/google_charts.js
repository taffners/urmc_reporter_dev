define(['models/utils'], function(utils)
{
     var  hex_color_list = {
            
               // 'Cyan':'#42d4f4',
               
               // 'Green':'#3cb44b',
                              
                     
               'blue':'#337AB7',

               'Orange':'#f58231',
               
               'Blue':'#0018F9',           

               // 'Azure':'#4363d8',
               'Purple':'#911eb4',
               // 'Magenta':'#f032e6',
               'Grey':'#a9a9a9',
               'Scarlet':'#FF2400',
               'Mint':'#aaffc3',
               'Lavender':'#e6beff',
               'Pink':'#f79ebd',
               // 'Maya':'#73C2FB',
               
               
               'Electric':'#8F00FF',         
               'Forest':'#0B6623',
               'Yellow_too':'#f2ff40',
               'bright_purple':'#ee00ff',
                
               'Navy':'#000075',               
               'dark_purlple':'#4d0073',
               'green': '#28a745',
               'Maroon':'#800000',
               'Brown':'#9A6324',
               'pink':'#cc2586', 
               'blue_grey':'#004d73',
                              
               'dark_grey':'#827f75',
               'green_grey':'#728f83',
               'Teal':'#469990',
               'Fluorescent_pink':'#e6194B',
               'Yellow':'#ffe119',
               'Lime':'#bfef45',
               'Apricot':'#ffd8b1',
               'bright_green':'#44ff00',
          };
     function _drawMaterialBarChart(insert_id, chart_title, data_array, sub_title, bar_type, x_axis_label)
     {
          // NOT USING THIS.  I CAN NOT FIGURE HOW TO CHANGE TICKS. MATERIAL IS STILL BETA
          google.charts.load('current', {'packages':['bar']});
          
          google.charts.setOnLoadCallback(drawChart);

          function drawChart() 
          {
               var data = new google.visualization.arrayToDataTable(data_array);

               var options = 
               {
                    chart: 
                    {
                         title: chart_title,
                         subtitle: sub_title
                    },
                    bars: bar_type, // Required for Material Bar Charts.   
                    axes: 
                    {
                         x: 
                         {
                              0: 
                              { 
                                   label: x_axis_label,
                              } 
                         }
                    },                    
                    hAxis: 
                    {                         
                         ticks: [1,3]
                    },
                    bar: { groupWidth: "90%" }           
               }
                            
               var chart = new google.charts.Bar(document.getElementById(insert_id));
               
               chart.draw(data, google.charts.Bar.convertOptions(options));
          }
     }

     function _drawBarChart(insert_id, chart_title, data_array, sub_title, bar_type, x_axis_label, isStackedVar=false)
     {
          if (data_array.length > 1)
          {
               google.charts.load('current', {packages: ['corechart', 'bar']});
               google.charts.setOnLoadCallback(drawAxisTickColors);

               function drawAxisTickColors() 
               {
                    var data = google.visualization.arrayToDataTable(data_array);

                    var options = 
                    {
                         title: chart_title,
                         titleTextStyle: 
                         {
                              bold: true,
                              fontSize: 14,
                              color: '#4d4d4d'
                         },
                         chartArea: 
                         {
                              width: '40%',
                              height: '50%'
                         },
                         hAxis: 
                         {
                              title: x_axis_label,
                              minValue: 0,
                              textStyle: 
                              {
                                   bold: true,
                                   fontSize: 12,
                                   color: '#4d4d4d'
                              },
                              titleTextStyle: 
                              {
                                   bold: true,
                                   fontSize: 12,
                                   color: '#4d4d4d'
                              },
                              gridlines:{count:4}
                         },
                         vAxis: 
                         {
                              // title: 'City',
                              textStyle: 
                              {
                                   fontSize: 12,
                                   bold: false,
                                   color: '#4d4d4d'
                              } //,
                              // titleTextStyle: 
                              // {
                              //      fontSize: 12,
                              //      bold: true,
                              //      color: '#4d4d4d'
                              // }
                         },
                         isStacked: isStackedVar
                    };
                    var chart = new google.visualization.BarChart(document.getElementById(insert_id));
                    chart.draw(data, options);
               }
          }
          else
          {
               $('#'+insert_id).append(chart_title + ' is empty');
          }
     }

     function _drawPieChart(insert_id, chart_title, data_array, height)
     {
          if (data_array.length > 1)
          {
               google.charts.load('current', {packages: ['corechart']});
               google.charts.setOnLoadCallback(drawPie);

               var  colors_arr = [],
                    color_names = Object.keys(hex_color_list);

               for (var i = 0; i < color_names.length; ++i) 
               {
                   
                    colors_arr.push(hex_color_list[color_names[i]]);
               }

               function drawPie() 
               {
                    var data = google.visualization.arrayToDataTable(data_array);

                    var options = 
                    {
                         title: chart_title,
                         pieSliceText: 'label',
                         titleTextStyle: 
                         {
                              bold: true,
                              fontSize: 14,
                              color: '#4d4d4d'
                         },
                         legend:'none',
                         is3D: true,
                         pieStartAngle: 100,
                         width: '100%',
                         height: height,
                         chartArea:
                         {
                             left:5,
                             top: 20,
                             width: '100%',
                             height: height,
                         },
                         colors: colors_arr
                    };
                    var chart = new google.visualization.PieChart(document.getElementById(insert_id));
                    chart.draw(data, options);
               }
          }
          else
          {
               $('#'+insert_id).append(chart_title + ' is empty');
          }
     }

     function _drawLineGraph(insert_id, chart_title, data_array, sub_title, bar_type, x_axis_label)
     {
          if (data_array.length > 1)
          {
               google.charts.load('current', {packages: ['corechart', 'line']});
               google.charts.setOnLoadCallback(drawLine);

               function drawLine() 
               {
                    var data = google.visualization.arrayToDataTable(data_array);

                    var options = 
                    {
                         title: chart_title,
                         titleTextStyle: 
                         {
                              bold: true,
                              fontSize: 14,
                              color: '#4d4d4d'
                         },
                         chartArea: 
                         {
                              width: '40%',
                              height: '50%'
                         },
                         hAxis: 
                         {
                              title: x_axis_label,
                              minValue: 0,
                              textStyle: 
                              {
                                   bold: true,
                                   fontSize: 12,
                                   color: '#4d4d4d'
                              },
                              titleTextStyle: 
                              {
                                   bold: true,
                                   fontSize: 12,
                                   color: '#4d4d4d'
                              },
                              gridlines:{count:4}
                         },
                         vAxis: 
                         {
                              // title: 'City',
                              textStyle: 
                              {
                                   fontSize: 12,
                                   bold: false,
                                   color: '#4d4d4d'
                              } //,
                              // titleTextStyle: 
                              // {
                              //      fontSize: 12,
                              //      bold: true,
                              //      color: '#4d4d4d'
                              // }
                         }
                    };
                   
                    var chart = new google.visualization.LineChart(document.getElementById(insert_id));
                    chart.draw(data, options);
               }
          }
          else
          {
               $('#'+insert_id).append(chart_title + ' is empty');
          }
     }

     function _drawTable(insert_id, chart_title, data_array, sub_title, bar_type, x_axis_label)
     {
          if (data_array.length > 1)
          {
               google.charts.load('current', {packages: ['corechart', 'table']});
               google.charts.setOnLoadCallback(drawTable);

               function drawTable() 
               {
                    var data = google.visualization.arrayToDataTable(data_array);

                    var options = 
                    {
                         title: chart_title,
                         titleTextStyle: 
                         {
                              bold: true,
                              fontSize: 14,
                              color: '#4d4d4d'
                         }
                         
                    };
                    var table = new google.visualization.Table(document.getElementById(insert_id));
                    table.draw(data, options);
               }
          }
          else
          {
               $('#'+insert_id).append(chart_title + ' is empty');
          }
     
     }

     function _drawPatientTimeline(row_data, options)
     {       
          // used in micro infotrack
          var  _options   = options;     
          // (JSON, JSON) -> draw a timeline in options(insert_id)
          google.charts.load('current', {packages:['timeline']});
          google.charts.setOnLoadCallback(drawChart);

          function drawChart() 
          {            
               let  container = document.getElementById(_options['insert_id']),
                    chart = new google.visualization.Timeline(container),
                    dataTable = new google.visualization.DataTable(),
                    i, 
                    rows=[],
                    curr_row, 
                    start_utcDate, 
                    start_local_date,
                    end_utcDate, 
                    end_local_date,
                    map_catergory,
                    bar_color,
                    firstKey,
                    _hex_color_list = utils.clone(hex_color_list),
                    extra_no_color_samples = '<b>The following categories are set to black because I ran out of colors &#128546</b>:<br>',
                    extra_samples = false, 
                    row_name,
                    colorMap = {};

               $('#message').empty();

               dataTable.addColumn({ type: 'string', id: 'Group' });
               dataTable.addColumn({ type: 'string', id: 'Category' });
               dataTable.addColumn({ type: 'string', role: 'style' });
               dataTable.addColumn({ type: 'date', id: 'Start' });
               dataTable.addColumn({ type: 'date', id: 'End' });
               
               for (i = 0; i < row_data.length; i++)
               {                    
                    curr_row = row_data[i];


                    var  test_utcDate = new Date(curr_row['time_stamp']), //Date object a day behind
                         test_local_date_time = new Date(test_utcDate.getTime() + test_utcDate.getTimezoneOffset() * 60000),
                         test_local_date = new Date(test_local_date_time.toDateString()),
                         followingDay = new Date(test_local_date.getTime() + 86400000);

                         followingDay.toLocaleDateString();
                    //////////////////////////////////////////////////////
                    // set color of bar depending mutation
                    ///////////////////////////////////////////////////////
                    map_catergory = curr_row['mutation']; 

                    if (map_catergory !== null)
                    {
                         map_catergory_group = map_catergory.split(' (')[0];     
                    }                   
                    else
                    {
                         map_catergory_group = null;
                    }

                    // find if color is already assigned and if not pick a select color hex_color_list 
                    if (map_catergory_group in colorMap)
                    {
                         bar_color = colorMap[map_catergory_group];
                    }
                    else
                    {
                         if (Object.keys(_hex_color_list).length === 0)
                         {
                              if (extra_samples)
                              {
                                   extra_no_color_samples+= ', ';
                              }

                              extra_samples = true;

                              extra_no_color_samples+= map_catergory_group;

                              // set color to black
                              colorMap[map_catergory_group] = '#000000';

                              // assign color to current bar
                              bar_color = colorMap[map_catergory_group];
                         }
                         else
                         {
                              // get the first object in the hex_color_list
                              firstKey = Object.keys(_hex_color_list)[0];

                              // add color for this map_catergory to colorMap
                              colorMap[map_catergory_group] = _hex_color_list[firstKey];

                              // assign color to current bar
                              bar_color = colorMap[map_catergory_group];

                              // remove color from hex_color_list so it isn't used again
                              delete _hex_color_list[firstKey];
                         }                         
                    }
                    rows.push(['Patient Variant History', map_catergory, bar_color, test_local_date, followingDay]);
               }

               dataTable.addRows(rows)  

               var  rowHeight = 80,
                    keyRowHeight = 24,
                    keyHeight = Object.keys(colorMap).length * keyRowHeight,
                    svgHeight = (1 + 1) * rowHeight,
                    chartHeight = svgHeight + keyHeight;

               var   options = 
                    {
                         timeline: 
                         { 
                              groupByRowLabel: true,
                              rowLabelStyle: 
                              {
                                   fontName: 'Roboto Condensed',
                                   fontSize: 14,
                                   color: '#333333'
                              },
                              barLabelStyle: 
                              {
                              fontName: 'Roboto Condensed',
                              fontSize: 14
                              }
                         },                          
                         avoidOverlappingGridLines: true,
                         height: chartHeight,
                         width: '90%'
                    };

               chart.draw(dataTable, options);
      
          }
     }

     function _drawQuantiyTimeBarChart()
     {
          google.charts.load('current', {'packages':['bar']});
          
          google.charts.setOnLoadCallback(drawChart);

          function drawChart() 
          {
             var data = new google.visualization.DataTable();
      data.addColumn('timeofday', 'Time of Day');
      data.addColumn('number', 'Emails Received');

      data.addRows([
        [[8, 30, 45], 5],
        [[9, 0, 0], 10],
        [[10, 0, 0, 0], 12],
        [[10, 45, 0, 0], 13],
        [[11, 0, 0, 0], 15],
        [[12, 15, 45, 0], 20],
        [[13, 0, 0, 0], 22],
        [[14, 30, 0, 0], 25],
        [[15, 12, 0, 0], 30],
        [[16, 45, 0], 32],
        [[16, 59, 0], 42]
      ]);

      var options = {
        title: 'Total Emails Received Throughout the Day',
        height: 450
      };

      var chart = new google.charts.Bar(document.getElementById('time_access_bar_chart'));

      chart.draw(data, google.charts.Bar.convertOptions(options));  
          }
          

     }

     return {
          drawBarChart:_drawBarChart,
          drawQuantiyTimeBarChart:_drawQuantiyTimeBarChart,
          drawPatientTimeline:_drawPatientTimeline,
          drawLineGraph:_drawLineGraph,
          drawTable:_drawTable,
          drawPieChart:_drawPieChart
     };
});
