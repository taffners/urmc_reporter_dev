define(['models/utils'],function(utils)
{
     function _BindEvents(php_vars)
     {

          // Check that API keys are correct
          $('.check-api-key-matches').on('click', function()
          {
               var  selected_server = $(this).data('selected_server'),
                    input_field_id = $(this).data('input_field_id'),
                    input_data = $('#'+input_field_id).val();

         
               // Make sure the the input field is not empty 
               if (input_data.length > 0)
               {
                    $.ajax(
                    {
                         type:'POST',
                         url: 'utils/check_thermo_apis.php?',
                         dataType: 'text',
                         data: {
                                   'selected_server': selected_server,
                                   'check_type':'api_key',
                                   'curr_key':input_data
                         },
                         success: function(response)
                         {
                              if (response.indexOf('not') >=0)
                              {
                                   $('#'+selected_server+'-api-key-msg-check').addClass('alert-danger').removeClass('alert-success');
                              }
                              else
                              {
                                   $('#'+selected_server+'-api-key-msg-check').removeClass('alert-danger').addClass('alert-success');
                              }

                              if (response)
                              {
                                   $('#'+selected_server+'-api-key-msg-check').empty().html('<h2>'+response+'</h2>');
                              }                 
                         },
                         error: function(textStatus, errorThrown)
                         {
                              utils.dialog_window('problem with ajax call ;( ' + errorThrown + ' ' + textStatus);
                         }
                    });  
               }
               else
               {
                    utils.dialog_window('Fill in input_data', input_field_id);
               }                        
          });
     }

     function _SetView()
     {

     }

     function _start(test_status, php_vars)
     {
          _SetView();
          _BindEvents(php_vars);
     }

     return {
          start:_start
     };
});
