define(['models/utils'],function(utils)
{
     function _BindEvents(php_vars)
     {
          // if the browser is not chrome 
          utils.CheckBrowser();  
              
          // Add to tables with a class of dataTable the sorting ability from bootstraps datatables https://datatables.net/manual/styling/bootstrap
          require(['views/events/TableEvents'], function(TableEvents)
          {
               TableEvents.StartEvents(true);
          });

          // a click on the pending report will bring up edit report 
          require(['views/events/showEditReport'], function(showEditReport)
          {
               showEditReport.StartEvents();
          });

            
     }

     function _SetView()
     {

     }

     function _start(php_vars)
     {
          _SetView();
          _BindEvents(php_vars);
     }

     return {
          start:_start
     };
});
