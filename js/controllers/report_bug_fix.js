define(['models/utils'],function(utils)
{
     function _BindEvents(php_vars)
     {
  
          // for each editable-select add restrictions on data length          
          var  field_lens = {'column_name[]': '255'};

          utils.typeRestrictionField(field_lens);  

          // insert activate button to insert and remove fields with a known field name.
          require(['views/events/template_inputs_with_del_btn'], function(template_inputs_with_del_btn)
          {
               template_inputs_with_del_btn.StartEvents(field_lens);
          });

     }

     function _setLongDefault()
     {
          $('.long-read-sample-type-option').show();
          $('.short-read-sample-type-option').hide();
     }

     function _setShortDefault()
     {
          $('.long-read-sample-type-option').hide();
          $('.short-read-sample-type-option').show();
     }

     function _otherSample(view_status)
     {
          if (view_status === 'show')
          {
               $('.other-sample-number-option').show();     
          }
          else
          {
               $('.other-sample-number-option').hide();
          }         
     }

     function _longNoShort(view_status)
     {
          if (view_status === 'show')
          {
               $('.long-read-num-without-short').show();     
          }
          else
          {
               $('.long-read-num-without-short').hide();
          } 
     }


     function _SetView()
     {
          _setShortDefault();
          _otherSample('hide');
     }

     function _start(php_vars)
     {
          _SetView();
          _BindEvents(php_vars);
     }

     return {
          start:_start
     };
});
