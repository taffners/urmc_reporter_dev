define(function()
{
     function _BindEvents(php_vars)
     {
          $('form').on('keyup keypress', function(e) 
          {
               var keyCode = e.keyCode || e.which;
               
               if (keyCode === 13) 
               { 
                    e.preventDefault();
                    return false;
               }
          });


          // a button to unslect all checkboxes labeled with class chkbx
          $('#unselect-chkbx').on('click', function()
          {
               $('.chkbx').prop('checked', false);
          });
     }

     function _SetView()
     {
           //////////////////////////////////////////////////
          // sortable fields
          //////////////////////////////////////////////////
          $( 'ul#sortable-order-samples').sortable({
               connectWith:".connectedSortable"
          }).disableSelection();
     }

     function _start(php_vars)
     {
          _SetView();
          _BindEvents(php_vars);
     }

     return {
          start:_start
     };
});
