define(function()
{
     function _BindEvents(php_vars)
     {
          // on click of unknown protein check box insert p.?
          $('input:checkbox').on('change', function()
          {
               $('#amino_acid_change').val('p.?');
               console.log('here2');
          });
     }

     function _SetView()
     {

     }

     function _start(php_vars)
     {
          _SetView();
          _BindEvents(php_vars);
     }

     return {
          start:_start
     };
});
