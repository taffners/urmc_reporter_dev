define(['models/utils', 'models/tour'],function(utils, tour)
{
     function _BindEvents(php_vars)
     {
          // Add to tables with a class of dataTable the sorting ability from bootstraps datatables https://datatables.net/manual/styling/bootstrap
          require(['views/events/TableEvents'], function(TableEvents)
          {
               TableEvents.StartEvents();
          });

          // enable submit button once a change has occurred in type=text fields
          require(['views/events/activate_submit_button'], function(activate_submit_button)
          {
               activate_submit_button.StartEvents();
          });

          // toggle progress report bar
          require(['views/events/toggle_progress_bar'], function(toggle_progress_bar)
          {
               toggle_progress_bar.StartEvents();
          });

          // enable date field calendar popups
          require(['views/events/date_fields'], function(date_fields)
          {
               date_fields.StartEvents();
          });

          // // In the variant table cells larger than 20 characters are cut.  This will enable the more button and less button
          require(['views/events/toggle_teaser_complete_strs'], function(toggle_teaser_complete_strs)
          {
               toggle_teaser_complete_strs.StartEvents();
          });

          $('form').on('keyup keypress', function(e) 
          {
               var keyCode = e.keyCode || e.which;
               
               if (keyCode === 13) 
               { 
                    e.preventDefault();
                    return false;
               }
          });

          // capitalize data entry.  Example aDAM -> Adam
          $('.capitalize').on('keyup paste', function(e)
          {
               var  textBox = e.target,
                    start = textBox.selectionStart,
                    end = textBox.selectionStart;
                    
                    textBox.value = textBox.value.charAt(0).toUpperCase() + textBox.value.slice(1).toLowerCase();
                    textBox.setSelectionRange(start, end);
          });


          $('#unselect-chkbx').on('click', function()
          {
               $('.chkbx').prop('checked', false);
          });


          $('#take-a-tour-btn').removeClass('d-none').on('click', function()
          {
               $('#ngs-dropdown').addClass('open');

               let  i,
                    thermofisher_backup_files = tour.thermofisher_backup_files(),
                    
                    tour_data,
                    tour_data_len;

               tour_data = [
                    {
                         backdrop: true,
                         orphan: true,                         
                         content: '<h3 style="text-align: center;"><b>Purpose of Pool Upload Data Page</b></h3>Once this page loads all of the data for the chips will be backed up.<br><br>Now it is time to upload the data'
                    },
                    
                    {
                         element: '#legend_0',
                         placement: 'bottom',
                         backdrop: true,
                         content: '<h3 style="text-align: center;"><b>Backup Location</b></h3>The address to where the data was backed up to can be found here.'
                    },
                    {
                         element: '#data-upload-th',
                         placement: 'left',
                         content: '<h3 style="text-align: center;"><b>Upload Data</b></h3>Use the yellow buttons here to upload data.  Green dots represent data that has already been uploaded.'
                    },
                    {
                         element: '#legend_1',
                         placement: 'top',
                         content: '<h3 style="text-align: center;"><b>Second Chip</b></h3>The second chip is located below the first chip.'
                    }
                    

               ];
                             
               tour_data = utils.mergeArrays(tour_data,thermofisher_backup_files,2);
               

               tour.start_tour(tour_data, '');
          });




     }

     function _SetView()
     {

     }

     function _start(php_vars)
     {
          _SetView();
          _BindEvents(php_vars);
     }

     return {
          start:_start
     };
});
