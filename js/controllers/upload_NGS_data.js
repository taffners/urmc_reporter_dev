define(['models/utils'],function(utils)
{
     function _BindEvents(php_vars)
     {
          var  myeloid_info, coverage_file; 

          /////////////////////////////////////////////////////////////////
          // Check TSV file uploaded.  Each panel type needs a different
          // check.  A more through check will occur using PHP this check will 
          // just make sure the user choose the correct file.
          /////////////////////////////////////////////////////////////////          
          document.getElementById('NGS_file_upload').addEventListener('change', function()
          {
               var  ngs_panel = $('#ngs-panel').val(),
                    md5_hash = utils.calculateMD5FileHash('NGS_file_upload', 'ngs-tsv-md5'),
                    sample_type = $(this).data('sample_type'),
                    ngs_panel_id = $(this).data('ngs_panel_id');

      
               if (ngs_panel === 'Myeloid/CLL Mutation Panel')
               {                 
                    _checkTSV('NGS_file_upload', 'myeloid', sample_type);

               }
               else if (ngs_panel === 'Oncomine Focus Hotspot Assay')
               {         
                    // Check sample name to make sure the correct sample is being uploaded.
                    // sample is named by mol_num_soft_lab_num or mol_num_visit_id for controls           
                    _checkTSV('NGS_file_upload', 'ofa', sample_type);
               }
               else
               {
                    utils.dialog_window('Something is wrong.  The ngs panel '+ngs_panel+' is not recognized.  Please tell Samantha.');
               }               
          });

          /////////////////////////////////////////////////////////////////
          // Get all sample names in Coverage file so user can choose/confirm
          // the correct one.
          /////////////////////////////////////////////////////////////////
          document.getElementById('depth_thres_upload').addEventListener('change', function()
          {
               // find MD numb and soft path number by visit_id
               var  md_num, soft_num,
                    md5_hash = utils.calculateMD5FileHash('depth_thres_upload', 'amp-cov-md5');

               $('.md-num-td').each(function()
               {
                    if ($(this).text() !== undefined)
                    {
                         md_num = $(this).text()
                    }
               });

               $('.soft-path-td').each(function()
               {
                    if ($(this).text() !== undefined)
                    {
                         soft_num = $(this).text()
                    }
               });

               _readCovFile('depth_thres_upload', md_num, soft_num);
          });

          /////////////////////////////////////////////////////////////////
          // Manual override sample name
          ///////////////////////////////////////////////////////////////// 
          $('#manual_sample_name_override').on('click', function()
          {
               $('#sample_name').attr('readonly', false);

               $('#manual_override_performed').val('yes');
          });

          /////////////////////////////////////////////////////////////////
          // submit form 
          /////////////////////////////////////////////////////////////////          
          $('#upload_NGS_data_submit').on('click', function()
          {
               // validate that fields are not empty form
               var  wet_bench_techs = $('div.form-group input[type=checkbox]:checked').length > 0,
                    run_date = $('#run_date').val() != '',
                    ngs_tsv = $('#NGS_file_upload').val() != '',
                    cov_txt = $('#depth_thres_upload').val() != '';
              
               if (!wet_bench_techs)
               {
                    utils.dialog_window('Choose a wet bench tech');
               }
               else if(!run_date)
               {
                    utils.dialog_window('Empty run date');
               }
               else if (!ngs_tsv)
               {
                    utils.dialog_window('add a ngs tsv');
               }
               else if (!cov_txt)
               {
                    utils.dialog_window('add a coverage file');
               }               
               else
               {
                    //////////////////////////////////////////////////////
                    // if not-listed is chosen as sample name ask user to confirm that 
                    // no low coverage amplicons are present.  Then ask for a sample name
                    // if user does not confirm tell them to to recheck coverage file uploaded.
                    //////////////////////////////////////////////////////
                    if ($('select#sample_name').val() == 'not listed')
                    {
                         // show dialog here
                         $('#ConfirmId').dialog('open');
                    }
                    else 
                    {
                         if ($('#not_found').val() !== 'low_amp_present' && $('#not_found').val() !== 'no_low_amp')
                         {
                              $('#not_found').val('low_amp_present');
                         }

                         _submit_to_php();
                    }                    
               }               

               // Stop form from submitting. reason to use return false over 
               // preventDefault https://stackoverflow.com/questions/1357118/event-preventdefault-vs-return-false
               // preventDefault was not working.
               return false;
          })

          /////////////////////////////////////////////////////////////////
          // Make hover card for info pertaining to the myeloid NGS TSV File.
          // activated when hover-card class is myeloid-ngs-info-hover-card
          /////////////////////////////////////////////////////////////////
          myeloid_info = '<ul class="hover-card-list" style="font-size:16px;">';
               myeloid_info += '<li><b>Create a Reporting folder</b> within the run folder <br>(example: O:\\Clinical\\Myeloid\\new\\181102_C4MFK)</li>';
               myeloid_info += '<li>In VariantStudio, <b>apply the desired filter</b> to the current sample from the home tab.  This is typically the main filter. In cases that there are no reportable variants from this filter, but there is one or more variants with low VAF(s) you can choose to use the sensitivity filter.  <b>DO NOT APPLY</b> a <b>FILTER</b> to the <b>POSITIVE CONTROL</b>.  Some variants we look for in the positive control are filtered out with the filter.</li>';
               myeloid_info += '<li>Click on the <b>Reports tab</b> and choose <b>Filtered Variants</b> in the Export section.  Save the file in the Reporting folder within the correct run folder.  This is named with the MD_Soft# (example: 4442-18_H8312658.tsv)</li>';
               myeloid_info += '<li>Each Patient has their own NGS TSV file.</li>';
          myeloid_info += '</ul>';

          $('#myeloid-ngs-info-hover-card').hovercard(
          {
               detailsHTML: myeloid_info,
               width: 550              
          });

          ofa_info = '<ul class="hover-card-list">';
               ofa_info += '<li>Login to Ion Reporter at https://10.149.59.209/ir/. User Name: molecdiag@urmc.edu pass: danryan1 <br><b>DO NOT USE Internet Explorer. USE CHROME.</b> </li>';
               ofa_info += '<li>If this screen seen click Advanced and then click proceed.<br><img src="images/login_to_iontorrent.png" style="height:250px;"></li>';
               ofa_info += '<li>Click on the Analyses Tab.<br><img src="images/ion_torrent_analyses_tab.png" style="height:50px;"></li>';
               ofa_info += '<li>Make sure the Workflow is <b>OFA_5-10_complex_v3_filter_updated</b>. If the Workflow is correct, select the sample you will be analyzing.  Refer to page 106 of SOP for more information.</li>';
               ofa_info += '<li>Make sure filter chain <b>OFAv190712_5-10 (5.4)</b> is selected.  Then click on download tab and Current Results TSV.<br><img src="images/ion_torrent_download.png" style="height:100px;"></li>';
               ofa_info += '<li>This will just get the tsv ready to download.  The next step is to click on the Home tab and Notifications and download the report from here.<br><img src="images/ion_torrent_download.png" style="height:100px;"></li>';
          ofa_info += '</ul>';

          $('#ofa-ngs-info-hover-card').hovercard(
          {
               detailsHTML: ofa_info,
               width: 770              
          });
          /////////////////////////////////////////////////////////////////
          // Make hover card for info pertaining to the Coverage File. 
          // This file is made by Bill.
          // activated when hover-card class is coverage-info-hover-card
          /////////////////////////////////////////////////////////////////
          myeloid_coverage_file = '<ul class="hover-card-list">';
               myeloid_coverage_file += '<li><b>Create a Reporting folder</b> within the run folder <br>(example: O:\\Clinical\\Myeloid\\new\\181102_C4MFK)</li>';
               myeloid_coverage_file += '<li>From the Data folder within the run folder <b>copy the depth coverage file</b> <br>(example: 181102_C4MFK_depth_coverage) and paste this into your newly created run folder.</li>';
               myeloid_coverage_file += '<li>For a given run all patients use the <b>same depth coverage file</b> when uploading to the reporter, but have their own tsv file.  These are all located in the reporting folder.</li>'; 
          myeloid_coverage_file += '</ul>';

          $('#myeloid-coverage-info-hover-card').hovercard(
          {
               detailsHTML: myeloid_coverage_file,
               width: 550              
          });

          var  ofa_coverage_file = '<ul class="hover-card-list">';
                    ofa_coverage_file += '<li>Login to Torrent Server at http://10.149.58.208/home/. User Name: molecdiag@urmc.edu pass: danryan1 <br><b>DO NOT USE Internet Explorer. USE CHROME.</b> </li>';
                    ofa_coverage_file += '<li>If this screen seen click Advanced and then click proceed.<br><img src="images/login_to_iontorrent.png" style="height:250px;"></li>';

                    ofa_coverage_file += '<li>Click on the data tab <br><img src="images/Torrent_server_data_tab.png" style="height:50px;"></li>';
                    ofa_coverage_file += '<li>Open your run by clicking ont he report name.</li>';
                    ofa_coverage_file += '<li>Scroll down to the coverageAnalysis and click on the Barcode Name for each sample.<br><img src="images/Torrent_server_data_cov_analysis.png" style="height:150px;"></li>';
                    ofa_coverage_file += '<li>Scroll down to the bottom of the page and download amplicon coverage summary file. <br><img src="images/Torrent_server_data_cov_analysis_file.png" style="height:150px;"></li>';
                    ofa_coverage_file += '<li><b>Make sure to add a new file for each sample.</b></li>';
               ofa_coverage_file += '</ul>';

          $('#ofa-coverage-info-hover-card').hovercard(
          {
               detailsHTML: ofa_coverage_file,
               width: 770              
          });
     }

     function _submit_to_php()
     {    
          // since submission was halted to check inputs this form will not have $_POST['upload_NGS_data_submit'] therefor PHP will not process it without this.  So it needs to be added here.

          // add a input so form will submit
          var  submit_input = '<input type="text" class="form-control" name="upload_NGS_data_submit"/>';
          $('#not-found-row').append(submit_input);
          
          // submit form
          document.getElementById("upload_NGS_data").submit();
     }

     function _checkTSV(input_file_id, tsv_type, sample_type)
     {
          // (str, str) -> if incorrect tsv notify user and remove file

          // check if TSV attempting to upload contains columns necessary for the 
          // current panel.  
          var  files = document.getElementById(input_file_id).files;
         
          if (!files.length) 
          {
               utils.dialog_window('Please select a file!');
               return;
          }

          var  file = files[0], 
               reader = new FileReader(),
               col_type,
               header_row_num = 0,
               header_cols;

          // expected columns
          if (tsv_type === 'myeloid')
          {
               header_cols = ["Classification", 'Gene', 'HGVSc', 'HGVSp', 'Alt Variant Freq', 'Reference Allele', 'Allelic Depths', 'Read Depth'];
          }
          else if (tsv_type === 'ofa')
          {
               // these headers are no longer used.  They are the headers used if the TSV file is downloaded from the IR website not the IR API
               // var old_header_cols = ['Genes', 'Coding', 'Amino Acid Change', '% Frequency', 'Genotype', 'Allele Coverage', 'Coverage'];

               header_cols = ['gene', 'coding', 'protein', '%_frequency', 'genotype', 'allele_coverage', 'coverage'];
          }

          // If we use onloadend, we need to check the readyState.
          reader.onloadend = function(evt) 
          {
               // make sure The entire read request has been completed.
               if (evt.target.readyState == FileReader.DONE) // DONE == 2
               { 
                    var  i, header, split_line, sample_name, curr_val, curr_option,
                         
                         content = evt.target.result.split('\n'),
                         f_lines = content.length;
                             
                    //////////////////////////////////////////////////////////////////////////////
                    // The SENSC control for the myeloid panel needs to be the unfiltered variant studio
                    // tsv.  For one sample run with filter on there were 54 rows in the tsv including 
                    // header.  For the sample sample 629 rows were in the unfiltered sample.  
                    // Therefore if the sample is checked to have a minimum of 600 rows this should 
                    // ensure that the unfiltered tsv was provided.
                    //////////////////////////////////////////////////////////////////////////////  
                    if (tsv_type === 'myeloid'  && sample_type === 'Positive Control' && f_lines < 200)
                    {
                         utils.dialog_window('The Myeloid Positive Control should have at least 200 lines in the file.  When the TSV file is produced via Variant Studio make sure there no filter is applied.');
                         $('#' + input_file_id).val('');                          

                         return;
                    }

                    // Skip over lines that starts with ##
                    if (tsv_type === 'ofa')
                    {
                         
                         // find the header.  It will be the first line without ##.
                         // Find header vals of interest while finding the header
                         for (i=0; i<f_lines; i++)
                         {
                              if (!content[i].startsWith('##'))
                              {
                                   header_row_num = i;
                                   break;
                              }
                         }
                    }


                    // check that the header is present and contains the correct 
                    // columns.  If it doesn't remove file and notify user.
                    if   (
                              f_lines >= 1 && 
                              !utils.CheckInArray(header_cols, content[header_row_num].split('\t')) 
                         )
                    {
                         utils.dialog_window('The uploaded NGS TSV file does not appear to be the correct format for the '+tsv_type+' panel. Errored out on line num ('+(parseInt(header_row_num)+1)+'):<br><br>'+ content[header_row_num]+'<br><br>Refer to the example file.');
                         $('#' + input_file_id).val('');                          

                         return;                        
                    }
               }
          }

          // start reader.           
          reader.readAsBinaryString(file);          
     }

     function _readCovFile(input_file_id, md_num, soft_num) 
     {
          // (str, str, str) -> 
          // read a portion of a file from and <input type="file"
          var  files = document.getElementById(input_file_id).files;
          
          if (!files.length) 
          {
               utils.dialog_window('Please select a file!');
               return;
          }

          var  file = files[0], 
               reader = new FileReader();

          // If we use onloadend, we need to check the readyState.
          reader.onloadend = function(evt) 
          {
               // make sure The entire read request has been completed.
               if (evt.target.readyState == FileReader.DONE) // DONE == 2
               { 
                    var  i, header, split_line, sample_name, curr_val, curr_option,
                         total_reads, merged_header_array,
                         low_amp_present = false,
                         I_think_this_is_your_sample = false,
                         samples_found = [],
                         content = evt.target.result.split('\n'),
                         f_lines = content.length,
                         sample_name = $('#sample_name').val(),
                         popup_html = '<div class="alert alert-info" role="alert"><strong>Please confirm that your sample does not have any low coverage data present.</strong></div>',
                         bills_cov_file = utils.CheckInArray(["RunID", "FlowCell", "Sample", "Gene", "Amplicon", "Exon", "Coding Region", "Depth"], content[0].trim().split('\t')),
                         torrent_cov_file = utils.CheckInArray(["Gene", "Amplicon", "Sample", "Coverage", "chrom", "start", "stop"], content[0].trim().split('\t'));
                         
                         // Old columns for OFA for file directly downloaded from the torrent  server website
                         // torrent_cov_file = utils.CheckInArray(["contig_id", "contig_srt", "contig_end", "region_id", "attributes", "gc_count", "overlaps", "fwd_e2e", "rev_e2e", "total_reads", "fwd_reads", "rev_reads", "cov20x", "cov100x",  "cov500x"], content[0].trim().split('\t'));

                    ///////////////////////////////////////////////////////////
                    // Add to a hidden input field (cov_file_type_id) what type of 
                    // coverage file is being uploaded.  This will make the uploader
                    // more dynamic than tying coverage file type to panel.  PHP will 
                    // access this hidden field and extract information based on it
                    ///////////////////////////////////////////////////////////
                    if (bills_cov_file)
                    {
                         $('#cov_file_type_id').val('depth_coverage');
                    }
                    else if (torrent_cov_file)
                    {
                         $('#cov_file_type_id').val('torrent_coverage_file'); 
                    }       

                    // check that the header is present and contains the correct 
                    // columns.  If it doesn't remove file and notify user.
                    if (f_lines >= 1 && (!bills_cov_file && !torrent_cov_file))
                    {                         
                         utils.dialog_window('Coverage File does not contain a header or it does not contain the correct columns.  Refer to the example file.');
                         $('#' + input_file_id).val('');                          

                         return;                        
                    }

                    // Since bills_cov_file provides sample name for myeloid need to search file and make a list and popup to confirm no sample name.  
                    // For OFA check if any row has a total_reads < 500. If it is set 
                    // input not_found ===  low_amp_present otherwise set to no_low_amp
                    // create header array and start making table for popup

                    header = content[0].split('\t');

                    // add header table to html
                    popup_html += '<table class="formated_table"><tr><th>'+content[0].split('\t').join('</th><th>')+'</th></tr>';
                    
                    ////////////////////////////////////////////////////
                    // show user in popup what is contained in uploaded folder so
                    // they can confirm that no low cov is present
                    // Add all unique Sample names to a drop down list  
                         // find all unique Sample in coverage file 
                         // if the name has both the md_num and the soft_num in 
                         // sample name add it to the beginning of the list
                              // skip the header (first line) make into a json  
                    ////////////////////////////////////////////////////
                    for (i=1; i < f_lines && content[i] !== ''; i++)
                    {

                         // find total_reads count for torrent_cov_file.  
                         if (torrent_cov_file)
                         {
                              merged_header_array = utils.MergeArraysToKeyValPairs( header, content[i].split('\t'));

                               
                              // sample name should always be set since it is added by PHP in template.  It isn't added PHP will
                              // stop the the submit for empty sample name anyway
                             
                              if   (
                                        merged_header_array['Coverage'] < 5000 //&& 
                                        //sample_name === merged_header_array['Sample'] 
                                   )
                              {
                                 
                                   $('#not_found').val('low_amp_present');
                                   low_amp_present = true;
                                   break;
                              }
                         }

                         // Find low coverage data and add data table to popup
                         else if (bills_cov_file)
                         {
                              // add row table to confirm popup
                              popup_html += '<tr><td>'+content[i].split('\t').join('</th><td>')+'</td></tr>';

                              split_line = content[i].split('\t');

                              // Make json of header (keys) split_line (values)
                              header_line_obj = utils.MergeArraysToKeyValPairs(header, split_line);
                        
                              // find all unique Sample in file and add them to samples_found array md_num, soft_num                         
                              sample_name = header_line_obj['Sample'];
                              if (typeof sample_name !== 'undefined' && !samples_found.includes(sample_name))
                              {
                                   if (sample_name.includes(md_num) && sample_name.includes(soft_num))
                                   {
                                        I_think_this_is_your_sample = true;
                                        samples_found.unshift(sample_name);
                                   }
                                   else
                                   {
                                        samples_found.push(sample_name);    
                                   }
                              }  
                         }                                            
                    }  

                    if (torrent_cov_file && low_amp_present === false)
                    {
                         $('#not_found').val('no_low_amp');
                    }

                    else if (bills_cov_file)
                    {
                         popup_html+='</table>';

                         // add cov to confirm pop up
                         $('#ConfirmId').html(popup_html);

                         // add buttons and make a hidden modal to open once form is submitted.
                         _confirmLowCoverage()

                         ////////////////////////////////////////////////////
                         // Make a list to add to sample_name
                         // I'm pulling names directly from low coverage file to avoid 
                         // typos. 
                         // Things to consider: Sample might not have an input in the low 
                         // coverage or the Coverage file might be empty.  Under not 
                         // option activate listener which deals with sample name equal 
                         // to not listed.
                         // include a choice in samples_found array a choice of not listed
                         // if I_think_this_is_your_sample is equal to true put not listed
                         // as the last choice.  Otherwise put as first choice.
                         ////////////////////////////////////////////////////

                         // clear out sample_name select
                         $('#sample_name').empty();

                         // add all samples as a new option for sample_name select.  
                         if (I_think_this_is_your_sample)
                         {
                              samples_found.push('not listed');
                         }
                         else
                         {
                              samples_found.unshift('not listed');
                         }

                         for (i=0; i < samples_found.length; i++)
                         {
                              curr_val = samples_found[i];

                              curr_option = '<option value="'+curr_val+'">'+curr_val+'</option>';
                              $('#sample_name').append(curr_option);
                         }

                         // show sample_name_row_id
                         $('#sample_name_row_id').removeClass('hide').removeClass('d-none').show();      
                    }
               }
          };

          // start reader.           
          reader.readAsBinaryString(file);
     }

     function _confirmLowCoverage()
     {
          // Make all of the confirm windows.  The user needs to confirm that 
          // no low coverage is found and needs to enter sample name via a pop up
         
          var  btn_array = 
          {
               'I confirm': function()
               {
                    // change input#not_found to no_low_amp
                    $('input#not_found').val('no_low_amp');                    

                    // close dialog
                    $('#ConfirmId').dialog('close');                    
                    
                    // ask user for sample name
                    var  pophtml = '<div class="form-group row required-input">';

                    pophtml += '<div class="col-xs-12 col-sm-4 col-md-4">';
                    pophtml += '<label for="run_date" class="form-control-label" >Sample Name: <span class="required-field">*</span></label>';
                    pophtml += '</div>';
                    pophtml += '<div class="col-xs-12 col-sm-8 col-md-8 pull-left">';
                    pophtml += '<input type="text" class="form-control" id="update_sample_name" name="run_date" required/>';
                    pophtml += '</div>';
                    pophtml += '</div>';

                    // add alert if not filled out to show
                    pophtml += '<div id="no_sample_name_pop_up_alert" class="alert alert-danger d-none" role="alert">Add a Sample Name</div>'

                    $('#add-sample-name-modal').html(pophtml);

                    var sub_btn_array = 
                    {
                         'submit' : function()
                         {
                              // check to make sure update_sample_name is not empty if it is empty show no_sample_name_pop_up_alert
                              if ($('#update_sample_name').val() !== '')
                              {
                                   var  samp = $('#update_sample_name').val();

                                   // Change add a input for select sample_name to input value for update_sample_name
                                   $('select#sample_name').append(new Option(samp, samp, true, true));
                                   _submit_to_php();
                              }
                              else
                              {    
                                   $('#no_sample_name_pop_up_alert').removeClass('hide').removeClass('d-none').show();
                              }                            
                         }
                    }
                    $('#add-sample-name-modal').dialog(
                    {               
                         resizable: true,
                         height: 400,
                         width: 500,
                         modal: true,
                         buttons: sub_btn_array
                    });

                    $('#add-sample-name-modal').dialog('open');



                    
               },
               'This is the wrong file':function()
               {
                    // set value of input#not_found to ''
                    $('#not_found').val('');

                    $('#depth_thres_upload').val('');

                    // if they click yes 
                    $( this ).dialog( "close" );
               },
               'Something went wrong':function()
               {
                    // redirect to bug reporter.
                    window.location.href = "?page=notes_slides";

                    $( this ).dialog( "close" );
               }
          }

          $('#ConfirmId').dialog(
          {               
               resizable: true,
               height: 400,
               width: 500,
               modal: true,
               buttons: btn_array
          });

          // change title
          $('#ConfirmId').dialog('option', 'title', 'Confirm Low Coverage Info!!!');

          $('#ConfirmId').dialog( "close" );
     }


     function _start(php_vars)
     {
          _BindEvents(php_vars);
     }

     return {
          start:_start
     };
});
