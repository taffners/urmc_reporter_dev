define(['models/utils'], function(utils)
{
     function _BindEvents(php_vars)
     {
          // // Add to tables with a class of dataTable the sorting ability from bootstraps datatables https://datatables.net/manual/styling/bootstrap
          // require(['views/events/TableEvents'], function(TableEvents)
          // {
          //      TableEvents.StartEvents();
          // });
          var sort_table = $('.sort_table').DataTable({paging:false});

          // Add run sheet table listeners
          require(['views/events/run_sheet_events'], function(run_sheet_events)
          {
               run_sheet_events.StartEvents('add_sample_to_run_sheet');
          });

          //////////////////////////////////////////////////////////////////
          // Add Row
          //////////////////////////////////////////////////////////////////
          $('#add_row').on('click', function()
          {
               var  row_num;

               if (localStorage.getItem('row_num') === null) 
               {
                    row_num = 1;
                    window.localStorage.setItem('row_num', '1');
               }
               else if (localStorage.getItem("row_num") != null)
               {
                    row_num = parseInt(window.localStorage.getItem('row_num')) + 1;
                    window.localStorage.setItem('row_num', row_num);
               }

               // increase number of 
               current_lib_tube_num = utils.update_increament_local_storage_var('current_lib_tube_num', true);

               var  row = '<tr row_id="a'+row_num+'">',
                    editable_row = '<td><div class="row_data" edit_type="click">__</div></td>',
                    checkbox_row = '<td><input id="cbx_a'+row_num+'" type="checkbox" class="add_sample_to_run_sheet_'+row_num+'" checked/></td>';
               
               row += checkbox_row; // include
               row += editable_row; // md# soft path #
               row += editable_row; // Patient Name
               row += '<td><div class="row_data" edit_type="click" col_name="qubit" id="qubit_a'+row_num+'">__</div></td>'; // Qubit
               row += '<td><div class="row_data" edit_type="click" col_name="total_vol" id="total_vol_a'+row_num+'">__</div></td>'; // Dil total Vol
               row += '<td id="DNA_vol_a'+row_num+'"></td>'; // DNA vol
               row += '<td id="AE_vol_a'+row_num+'"></td>' // AE vol
               row += '<td id="lib_tube_a'+row_num+'">'+current_lib_tube_num+'</td>' // Library Tube
               row += '<td><div class="row_data" edit_type="click">A7</div></td>'; // Index 1 i7
               row += '<td><div class="row_data" edit_type="click">A5</div></td>'; // Index 2 i5
               row += '</tr>';

               $('#make_run_sheet_tbody').append(row);

               // Add run sheet table listeners
               require(['views/events/run_sheet_events'], function(run_sheet_events)
               {
                    run_sheet_events.StartEvents('add_sample_to_run_sheet_'+row_num);
               });
          });

          //////////////////////////////////////////////////////////////////
          // submit data to 
          //////////////////////////////////////////////////////////////////
          $('#submit_and_download').on('click',function()
          {
               
               var  i,j, oCells, div_contents, cellLength, div_contents, cellVal,
                    include_row, cbx_id, cbx_check_status,row_data,
                    oTable = document.getElementById('make_run_sheet_tbody'),
                    row_len = oTable.rows.length,
                    col_json = {
                         0:'include',
                         1:'sample_ids',
                         2:'patient_name',
                         3:'qubit',
                         4:'dil_total_vol',
                         5:'dna_vol',
                         6:'ae_vol',
                         7:'library_tube',
                         8:'index_i7',
                         9:'index_i5'
                    };
               
               ///////////////////////////////////////////////////////
               // Add general info
               ///////////////////////////////////////////////////////
               // Make sure ngs_date and type myeloid is not already in database

               // make sure library prep date and ngs run date not empty
               var  lib_prep_date = $('#library_prep_date').val(),
                    ngs_run_date = $('#ngs_run_date').val(),
                    version = $('#version').val();

               if (lib_prep_date === '')
               {
                    error_message = 'Enter library prep date';
                    utils.dialog_window(error_message, 'AlertId');
               }
               else if (ngs_run_date === '')
               {
                    error_message = 'Enter ngs run date';
                    utils.dialog_window(error_message, 'AlertId');
               }
               else
               {
                    $.ajax(
                    {
                       type: 'POST',
                       url: 'utils/add_run_sheet_general.php',
                       dataType: 'json',
                       data:  {
                                   'library_prep_date':lib_prep_date,
                                   'ngs_run_date':ngs_run_date,
                                   'version':version,
                                   'sheet_panel_type':'myeloid'
                              } 
                    }).done(function(response)
                    {
                         if (response['err'] == 'already added')
                         {
                              error_message = 'ngs run date and version already added';
                              utils.dialog_window(error_message, 'AlertId');
                         }

                         // add table using the returned run_sheet_general_id
                         else
                         {
                              ///////////////////////////////////////////////////////
                              // Add table data
                              ///////////////////////////////////////////////////////

                              // loop through rows
                              for (i = 0; i < row_len; i++)
                              {
                                   //gets cells of current row  
                                   oCells = oTable.rows.item(i).cells;

                                   //gets amount of cells of current row
                                   cellLength = oCells.length;

                                   include_row = false; 
                                   row_data = {'run_sheet_general_id':response['run_sheet_general_id']}

                                   //loops through each cell in current row
                                   for(j = 0; j < cellLength; j++)
                                   {
                                        // get cell info here
                                        cellVal = oCells.item(j).innerHTML;

                                        if (cellVal.indexOf('"cbx_') !== -1)
                                        {
                                             cbx_id = $(cellVal).attr('id');
                                             include_row = $('#'+cbx_id).is(':checked');

                                             // only include rows which are included
                                             if (!include_row)
                                             {
                                                  break;
                                             }
                                        }

                                        // find all columns which have divs in them
                                        else if (cellVal.indexOf('<div') !== -1)
                                        {
                                             div_contents = $(cellVal).html().trim();
                                             row_data[col_json[j]] = div_contents;
                                        }

                                        else
                                        {
                                             row_data[col_json[j]] = cellVal;
                                        }
                                   }

                                   // make sure this row is included and then add
                                   if (include_row)
                                   {
                                        $.ajax(
                                        {
                                           type: 'POST',
                                           url: 'utils/add_run_sheet_row.php',
                                           dataType: 'json',
                                           data:  row_data 
                                        }).done(function(response)
                                        {

                                        });
                                   }                                  
                              }  

                              ///////////////////////////////////////////////////////
                              // redirect to page to show run sheet
                              ///////////////////////////////////////////////////////
                              $(document).ajaxStop(function() {

                                   window.location.href= '?page=view_run_sheet&run_sheet_general_id='+response['run_sheet_general_id'];
                              });
                         }
                    });
               }



            

               ///////////////////////////////////////////////////////
               // redirect to page to show pdf of table
               ///////////////////////////////////////////////////////

          });

     }

     function _SetView()
     {

     }

     function _start(php_vars)
     {
          window.localStorage.removeItem('current_lib_tube_num');
          _SetView();
          _BindEvents(php_vars);
     }

     return {
          start:_start
     };
});
