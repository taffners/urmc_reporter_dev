define(['models/utils'],function(utils)
{
     function _BindEvents(php_vars)
     {
          $('#assign-new-mol-num').on('click',function()
          {
               var  selected_type = $('input[type=radio][name=sample_type]').val();
               
               utils.getNextMolNum(selected_type, php_vars, true);
          });

          // Enable changing form based on which test was chosen.
          $('select#orderable_tests_id').on('change', function()
          {
               var  data_attr = $(this).find(':selected').data();

               ////////////////////////////////////
               // reset form
               ////////////////////////////////////
               $('#consent_required').hide();
               $('#ngs_required_fields').hide();
               $('#history_fields').hide();
               $('#dob').removeAttr('required');
               $('#consent_info_first_input').removeAttr('required');
               $('#consent_for_research_first_input').removeAttr('required');
               $('#sex-M').removeAttr('required');
               $('#soft_lab_num').removeAttr('required');
               $('#req_physician').removeAttr('required');

               $('.notes-for-work-sheet-row').hide();

               ////////////////////////////////////
               // Show all fields sections that test require.
               ////////////////////////////////////
               if (data_attr['consent_required'] === 'yes')
               {               
                    // custom_fields.InsertCustomField(insert_id,  0,'consent_info');
                    $('#consent_required').removeClass('hide').removeClass('d-none').show();
                    $('#consent_info_first_input').attr('required', true);
                    $('#consent_for_research_first_input').attr('required', true);
               }
               
               if (data_attr['mol_num_required'] === 'yes')
               {
                    $('#ngs_required_fields').removeClass('hide').removeClass('d-none').show();

                    //////////////////////////////////////////////////////////////////////////
                    // Add the next molecular number depending on if the sample is a Patient Sample or a Validation sample.  
                    //////////////////////////////////////////////////////////////////////////
                    // find out what the sample type is. 
                    var  sample_type = $('input[type=radio][name=sample_type]').val();

                    // Set MolNum to next one
                    utils.getNextMolNum(sample_type, php_vars);

                    // Manage required fields. Make date of Birth Required
                    $('#dob').attr('required', true);
                    $('#sex-M').attr('required', true);
                    $('#soft_lab_num').attr('required', true);
                    $('#req_physician').attr('required', true);
               }

               if (data_attr['previous_positive_required'] === 'yes')
               {
                    //////////////////////////////////////////////////////////////////////////
                    // Check the patient history for this test.
                    //////////////////////////////////////////////////////////////////////////
                    $('#history_fields').removeClass('hide').removeClass('d-none').show();
                    // $('#history_overview').attr('required', true);
                    $('.required-history-field').attr('required', true);
               }

               // show a notes-for-work-sheet if present 
               $('#notes-for-work-sheet-' + $(this).val())
                    .removeClass('hide')
                    .removeClass('d-none')
                    .show();

          });

          $('input[type=radio][name=sample_type]').on('change', function()
          {
               var  sample_type = $(this).val();

               // Set MolNum to next one
               utils.getNextMolNum(sample_type, php_vars)             
          });

          // enable submit button once a change has occurred in type=text fields
          require(['views/events/activate_submit_button'], function(activate_submit_button)
          {
               activate_submit_button.StartEvents();
          });

          // enable date field calendar popups
          require(['views/events/date_fields'], function(date_fields)
          {
               date_fields.StartEvents();
          });

          // prevent submitting form using enter 
          $('form').on('keyup keypress', function(e) 
          {
               var keyCode = e.keyCode || e.which;
               
               if (keyCode === 13) 
               { 
                    e.preventDefault();
                    return false;
               }
          });

          // for each editable-select add restrictions on data length          
          var  field_lens = {'column_name[]': '255'};

          utils.typeRestrictionField(field_lens);  

          // insert activate button to insert and remove fields with a known field name.
          require(['views/events/template_inputs_with_del_btn'], function(template_inputs_with_del_btn)
          {
               template_inputs_with_del_btn.StartEvents(field_lens);
          });
     }

     function _SetView()
     {

     }

     function _start(test_status, php_vars)
     {
          _SetView();
          _BindEvents(php_vars);
     }

     return {
          start:_start
     };
});
