define(['models/utils'], function(utils)
{
     function _BindEvents(php_vars)
     {
  
          // toggle progress report bar
          require(['views/events/toggle_progress_bar'], function(toggle_progress_bar)
          {
               toggle_progress_bar.StartEvents();
          });

          // toggle variant_comment_modal
          require(['views/events/modals/activate_variant_modal'], function(activate_variant_modal)
          {
               activate_variant_modal.StartEvents();
          });


          // activate scrolling to top of page
          $('.scroll_to_top_page').on('click', function()
          {             
               window.scrollTo(0,0);
          });



     }

     function _SetView()
     {

     }

     function _start(php_vars)
     {
          _SetView();
          _BindEvents(php_vars);
     }

     return {
          start:_start
     }
});