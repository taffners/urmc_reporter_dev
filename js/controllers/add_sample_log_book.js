define(['models/utils'],function(utils)
{
     function _BindEvents(php_vars)
     {

          // toggle progress report bar
          require(['views/events/toggle_progress_bar'], function(toggle_progress_bar)
          {
               toggle_progress_bar.StartEvents();
          });

          // enable date field calendar popups
          require(['views/events/date_fields'], function(date_fields)
          {
               date_fields.StartEvents();
          }); 

          $('form').on('keyup keypress', function(e) 
          {
               var keyCode = e.keyCode || e.which;
               
               if (keyCode === 13) 
               { 
                    e.preventDefault();
                    return false;
               }
          });

          // capitalize data entry.  Example aDAM -> Adam
          $('.capitalize').on('keyup paste', function(e)
          {
               var  textBox = e.target,
                    start = textBox.selectionStart,
                    end = textBox.selectionStart;
                    
                    textBox.value = textBox.value.charAt(0).toUpperCase() + textBox.value.slice(1).toLowerCase();
                    textBox.setSelectionRange(start, end);
          });

          $('#uncapitalize').on('click', function(e)
          {
               // for some reason the form is auto submitting when this button is pushed therefore this prevents submission of the form
               event.preventDefault();

               // removes the bound event-listeners of capitalize of the elements.
               $('.capitalize').unbind();
          })

          $('#remove-md-readonly').on('click', function(e)
          {
               e.preventDefault();
               $('#mol_num').attr('readonly', false); 
          });

          $('.assign-unk-num').on('click', function()
          {
               var  _field_type = $(this).data('unk-field');

               $.ajax(
                         {
                              type:'POST',
                              url: 'utils/get_next_unk_nums_soft_lab.php?',
                              data: {
                                   'unk_field': _field_type,
                              },
                              dataType: 'text',
                              
                              success: function(response)
                              {
                                   var  next_unk = 1,
                                        last_unk_array = JSON.parse(response),
                                        last_unk = last_unk_array[0]['last_unk_num'];

                                   // no assigned UNK number will default to 1 otherwise find the next number
                                   if (last_unk)
                                   {
                                        next_unk = parseInt(last_unk) + 1;                                     
                                   }

                                   $('#'+_field_type)
                                        .val('UNK'+next_unk);
                                   
                              },
                              error: function(textStatus, errorThrown)
                              {
                                   dialog_window('problem with ajax call ;( ' + errorThrown + ' ' + textStatus);
                              }
                         });

          });

          ////////////////////////////////////////////////////////////////////////////
          // Anonymous bone marrow transplant donor
               // Toggle field requirements for anonymous bone marrow transplant donor. These donor samples only have donor ID number. They do not have names, gender, dob, or mrn.  Sometimes a soft-lab-num will be entered which is the patients soft-lab-num.
               // Toggle field name=sample_type_toggle will:
                    // remove requirements on the following fields
                         // medical_record_num
                         // soft_lab_num
                    // hide required span tag which contain the red star
                         // soft-lab-required-span
                         // mrn-required-span
                    // Will change last_name label to donor id
                         // donor-id-label, last-name-label
                    // Will fill in first_name with anonymous_donor
          ////////////////////////////////////////////////////////////////////////////
          $('input[type=radio][name=sample_type_toggle]').on('change', function()
          {
               _SetView();
          });

          // change permissions on account
          $('.rm-comment').on('click', function()
          {
               let  comment_id = $(this).data('comment-id'),
                    comment_type = $(this).data('comment-type'),
                    comment_ref = $(this).data('comment-ref'),
                    comment = $(this).data('comment'),
                    button_array =
                    {
                         cancel: function()
                         {
                              $('#ConfirmId').dialog('close');

                         },
                         'yes': function()
                         {
                              $('#ConfirmId').dialog('close');
                              $.ajax(
                              {
                                   type:'POST',
                                   url: 'utils/del_comment.php?',

                                   dataType: 'text',
                                   data: {
                                             'comment_id': comment_id,
                                             'comment_type': comment_type,
                                             'comment_ref':comment_ref 
                                   },
                                   success: function(response)
                                   {

                                        window.location.reload(true)

                                   },
                                   error: function(textStatus, errorThrown)
                                   {
                                        dialog_window('problem with ajax call ;( ' + errorThrown + ' ' + textStatus);
                                   }
                              });

                         }
                    },
                    popup_msg = 'Are you sure you want to delete the message <b>permanently</b>: <br><br>'+comment;
               utils.confirmPopUp(popup_msg, button_array);
          });
     }

     function _SetView()
     {
          let  sample_type_toggle = $('input[type=radio][name=sample_type_toggle]:checked').val();

          // anonymous_donor
          if (sample_type_toggle === 'anonymous_donor')
          {
               $('#medical_record_num').prop('required', false);
               $('#soft_lab_num').prop('required', false);
               $('#soft_path_num').prop('required', false);

               $('#soft-lab-required-span').hide();
               $('#mrn-required-span').hide();

               $('#donor-id-label').removeClass('hide').removeClass('d-none').show();
               $('#last-name-label').hide();

               $('#soft-path-num-label').removeClass('hide').removeClass('d-none').show();
               $('#cap-sample-label').hide();

               $('.dob-sex-required-span').hide();
               $('input[name="sex"]').prop('required', false);
               $('input[name="dob"]').prop('required', false);
               // dash is used so word warp occurs on sample log book 
               $('#first_name').val('anonymous-donor');                  
          }

          else if (sample_type_toggle === 'identity_testing')
          {
               $('#medical_record_num').prop('required', false);
               $('#soft_lab_num').prop('required', false);
               $('#soft_path_num').prop('required', false);

               $('#soft-lab-required-span').hide();
               $('#mrn-required-span').hide();

               $('#donor-id-label').hide();
               $('#last-name-label').removeClass('hide').removeClass('d-none').show();

               $('#soft-path-num-label').removeClass('hide').removeClass('d-none').show();
               $('#cap-sample-label').hide(); 

               $('.dob-sex-required-span').hide();
               $('input[name="sex"').prop('required', false);                
          }

          // CAP Sample
               // Medical record numbers are used multiple times for a CAP sample.
                    // For the last year there have only been 2 MRN numbers.  Therefore the patient NGS patients will always have the same patient.  This is why this is being implemented to make sure the CAP ID is transfered to NGS sample
          else if (sample_type_toggle === 'cap_sample')
          {
               $('#medical_record_num').prop('required', true);
               $('#soft_lab_num').prop('required', true);
               $('#soft_path_num').prop('required', true);

               $('#soft-lab-required-span').removeClass('hide').removeClass('d-none').show()
               $('#mrn-required-span').removeClass('hide').removeClass('d-none').show();

               $('#donor-id-label').hide();
               $('#last-name-label').removeClass('hide').removeClass('d-none').show();

               $('#cap-sample-label').removeClass('hide').removeClass('d-none').show();
               $('#soft-path-num-label').hide();

               // dash is used so word warp occurs on sample log book 
               $('#first_name').val('Cap');     
               $('#last_name').val('Sample'); 

               $('.dob-sex-required-span').hide();
               $('input[name="sex"').prop('required', false);    

               // Since Medical record numbers are almost always the same add the last medical record number used for a cap sample if MRN is empty
               if ($('#medical_record_num').val().length === 0)
               {
                    $.ajax(
                    {
                         type:'POST',
                         url: 'utils/find_last_CAP_MRN.php?',

                         dataType: 'text',
                         
                         success: function(response)
                         {                           
                              $('#medical_record_num').val(response.trim());

                         },
                         error: function(textStatus, errorThrown)
                         {
                              dialog_window('problem with ajax call ;( ' + errorThrown + ' ' + textStatus);
                         }
                    });
               }
          }

          // sample
          else
          {
               $('#medical_record_num').prop('required', true);
               $('#soft_lab_num').prop('required', true);
               $('#soft_path_num').prop('required', false);

               $('#soft-lab-required-span').removeClass('hide').removeClass('d-none').show();
               $('#mrn-required-span').removeClass('hide').removeClass('d-none').show();

               $('#donor-id-label').hide();
               $('#last-name-label').removeClass('hide').removeClass('d-none').show();

               $('#soft-path-num-label').removeClass('hide').removeClass('d-none').show();
               $('#cap-sample-label').hide();

               $('.dob-sex-required-span').removeClass('hide').removeClass('d-none').show();
               $('input[name="sex"]').prop('required', true);
               $('input[name="dob"]').prop('required', true);
          }
     }

     function _start(php_vars)
     {
          _SetView();
          _BindEvents(php_vars);
     }

     return {
          start:_start
     };
});
