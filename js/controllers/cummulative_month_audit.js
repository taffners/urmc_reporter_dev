define(['models/utils','models/google_charts'],function(utils, google_charts)
{
     function _BindEvents(php_vars)
     {
          // add Number of tests
          $.ajax(
          {
               type:'POST',
               url: 'utils/audits.php?',

               dataType: 'text',
               data: {
                         'query_filter': 'cummulative_test_counts',
                         'filter':'',
                         'month':php_vars['month'] != undefined ? php_vars['month'] : '',
                         'year':php_vars['year'] != undefined ? php_vars['year'] : ''
               },
               success: function(response)
               {
                    if (response ==='Post Does not contain query_filter')
                    {

                    }
                    else
                    {
                         var  json_response = JSON.parse(response),
                              chart_data = [['Month','Number of tests']],
                              chart_height = 1000,
                              
                              curr_month,
                              div_id = 'monthly-count-graphs';
                    
                         // Get all the possible curr_status
                         json_response.forEach(function(val, i)
                         {
                                 
                              curr_month = [val['month']+' '+val['year'], parseInt(val['num_tests'])];
                              chart_data.push(curr_month);
                         });   

                         $('#'+div_id+'-chart').height(700);  

                         google_charts.drawLineGraph(div_id+'-chart', 'Number of Tests Completed Each Month', chart_data, '', 'vertical', 'Months');
                         
                    }

               },
               error: function(textStatus, errorThrown)
               {
                    utils.dialog_window('problem with ajax call ;( ' + errorThrown + ' ' + textStatus);
               }
          });

          
          // number of each test per month
          $.ajax(
          {
               type:'POST',
               url: 'utils/audits.php?',

               dataType: 'text',
               data: {
                         'query_filter': 'cummulative_counts_per_test',
                         'filter':'',
                         'month':php_vars['month'] != undefined ? php_vars['month'] : '',
                         'year':php_vars['year'] != undefined ? php_vars['year'] : ''
               },
               success: function(response)
               {
                    if (response ==='Post Does not contain query_filter')
                    {

                    }
                    else
                    {
                         var  json_response = JSON.parse(response),
                              chart_data =  [],
                             
                              curr_month,
                              div_id = 'monthly-test-counts',
                              tests = [],
                              all_dates = [],
                              curr_array,
                              date_found,
                              total;

                         json_response.forEach(function(val, i)
                         {                                 
                              // tests
                              if (!tests.includes(val['test_name']))
                              {
                                   tests.push(val['test_name'])
                              }

                              // dates
                              if (!all_dates.includes(val['month_year']))
                              {
                                   all_dates.push(val['month_year'])
                              }
                         }); 
                         tests.push('total');
                         chart_data.push(tests);
                         chart_data[0].unshift('Dates');


                         all_dates.forEach(function(date, di)
                         { 
                              curr_array = [date];
                              total = 0;
                              tests.forEach(function(test, ti)
                              { 
                                   date_found = false;
                                   
                                   json_response.forEach(function(count, ci)
                                   { 
                                        if (test !== 'Dates' && test !== 'total' && count['test_name'] == test && count['month_year'] == date)
                                        {
                                             curr_array.push(parseInt(count['num_tests']));
                                             
                                             date_found = true;

                                             total = total + parseInt(count['num_tests']);
                                             console.log(total)
                                        }

                                   });
                                   if (test !== 'Dates' && test !== 'total' && !date_found)
                                   {
                                        curr_array.push(0);
                                   }

                              });
                              curr_array.push(total);
                              chart_data.push(curr_array);

                         });

                        
                         $('#'+div_id+'-table').height(700);  
                         google_charts.drawTable(div_id+'-table', 'Number of Test Each Completed Each Month', chart_data, '', 'vertical', 'Months');
                    }

               },
               error: function(textStatus, errorThrown)
               {
                    utils.dialog_window('problem with ajax call ;( ' + errorThrown + ' ' + textStatus);
               }
          });


                 
     }

     function _SetView()
     {

     }

     function _start(test_status, php_vars)
     {
          _SetView();
          _BindEvents(php_vars);

     }

     return {
          start:_start
     };
});
