define(['models/utils'],function(utils)
{
     function _BindEvents(php_vars)
     {

          // if a reflex tracker is added to this test remind user if the trigger result is selected. 
          // Make sure weak positive and weak negative also tigger 
          if ($('input[name="trigger_result"]').length)
          {
               $('input[name="test_result"]').on('change', function()
               {
                    var  selected_val = this.value,
                         tigger_result = $('input[name="trigger_result"]').val();

                    if (selected_val.includes(tigger_result))
                    {
                         var  next_test_name = $('input[name="next_test_name"]').val(),
                              msg = 'Add a <b>' + next_test_name + '</b> test to this sample.  <br><br><b>' + next_test_name + '</b> is the next test in the Reflex tracker.<br><br><b>AGAIN MAKE SURE YOU MANUALLY ADD A ' + next_test_name + ' TO THIS SAMPLE.  THIS WILL NOT BE DONE AUTOMATICALLY.</b>';

                         $('#AlertId').attr('title', 'ALERT: Add Next Reflex Test');

                         utils.dialog_window(msg, 'reflex-update-section');
                    }
               });
          }

          $('input[type="radio"][name=extraction_method]').on('change', function()
          {
               var  selected_val = this.value;

               $('#extractors_row').hide();
               $('#extractors_input_1').removeAttr('required');

               if (selected_val == 'Maxwell')
               {                   
                    $('#extractors_row').removeClass('hide').removeClass('d-none').show();
                    $('#extractors_input_1').attr('required', true);
               }
          });

          $('.required-linked-fields').on('change', function()
          {
               var  required_linked_field = $(this).data('required_linked_field');

               $('#'+required_linked_field).attr('required', true);
               $('#'+required_linked_field + '_span').removeClass('hide').removeClass('d-none').show();

          });

          $('form').on('keyup keypress', function(e) 
          {
               var keyCode = e.keyCode || e.which;
               
               if (keyCode === 13) 
               { 
                    e.preventDefault();
                    return false;
               }
          });


          $('input[type="radio"][name=dilution_exist]').on('change', function()
          {
               var  selected_val = this.value,
                    dilution_type = $('input[type="radio"][name=dilution_type]:checked').val();

               if (selected_val === 'yes')
               {
                    $('#dilution-type-span').removeClass('hide').removeClass('d-none').show();
                    $('#dilution-type-precise').attr('required', true);

                    if (dilution_type === 'precise')
                    {
                         _precise_setup();
                    }
               }
               else
               {
                    $('#dilution-type-span').hide();
                    $('#dilution-type-precise').removeAttr('required');

                    $('#dilution-calculator').hide();
                    $('#x-dilution').hide();

                    $('#times_dilution').removeAttr('required');
                    $('#target_conc').removeAttr('required');
                    $('#dilution_final_vol').removeAttr('required');

                    $('input[name=dilution_final_vol]').removeAttr('value');
                    $('input[name=target_conc]').removeAttr('value');
                    $('input[name=dilution_final_vol]').val('');
                    $('input[name=target_conc]').val('');

                    $('input[name=vol_dna]').removeAttr('value');
                    $('input[name=vol_eb]').removeAttr('value');
                    $('input[name=vol_dna]').val('');
                    $('input[name=vol_eb]').val('');
               }
          });


          $('input[type="radio"][name=dilution_type]').on('change', function()
          {
               var  selected_val = this.value,
                    stock_conc = $('#stock_conc').val();

               if (selected_val === 'precise')
               {
                    _precise_setup();

               }
               else if (selected_val === 'x_dilution')
               {

                    $('#dilution-calculator').hide();
                    $('#x-dilution').removeClass('hide').removeClass('d-none').show();

                    $('#times_dilution').removeAttr('required');
                    $('#target_conc').removeAttr('required');
                    $('#dilution_final_vol').removeAttr('required');
                    $('#stock_conc').removeAttr('required');


                    $('#times_dilution').attr('required', true);
                    
               }
          });

          $('#stock_conc, #dilution_final_vol, #target_conc, #elution_volume').on('change', function()
          {
               var  stock_conc = $(this).val();

               _empty_stock_conc_alert(stock_conc);

               _runDilutionCalculator();
          });

     }


     function _precise_setup()
     {
          $('#dilution-calculator').removeClass('hide').removeClass('d-none').show();
          $('#x-dilution').hide();


          $('#times_dilution').attr('required', true);
          $('#target_conc').attr('required', true);
          $('#dilution_final_vol').attr('required', true);
          $('#stock_conc').attr('required', true);

          $('#times_dilution').removeAttr('required');

          var  dilution_final_vol = $('input[name=dilution_final_vol]').val(),
               target_conc = $('input[name=target_conc]').val();

          if (dilution_final_vol.length === 0)
          {
               $('input[name=dilution_final_vol]').val(50);
          }

          if (target_conc.length === 0)
          {
               $('input[name=target_conc]').val(50);
          }    

          _empty_stock_conc_alert(stock_conc);

          _runDilutionCalculator();               
     }


     function _empty_stock_conc_alert(stock_conc)
     {
         
          if (stock_conc === undefined || !stock_conc || stock_conc.length > 100)
          {
               $('#alert-stock-conc-empty').removeClass('hide').removeClass('d-none').show();
          }
          else
          {
               $('#alert-stock-conc-empty').hide();
          }
     }

     function _runDilutionCalculator()
     {

          var  dilution_type = $('input[type="radio"][name=dilution_type]:checked').val();

          // only run if dilution type is precise
          if (dilution_type === 'precise')
          {             
               var  c1 = $('input[name=stock_conc]').val(),
                    v2 = $('input[name=dilution_final_vol]').val(),
                    c2 = $('input[name=target_conc]').val()
                    elution_vol = $('input[name=elution_volume]').val();


               if (c1 > 5 && v2 > 0 && c1 > 0)
               {
                    // c1v1 = c2v2
                    // c2v2/c1
                    var  v1 = (c2 * v2)/c1,
                         eb_vol = v2 - v1;

                    if (elution_vol !== undefined && elution_vol.length !== 0 && v1 >= elution_vol)
                    {
                         utils.dialog_window('The volume of DNA is greater than the elution volume', 'stock_conc');
                    }

                    else if (v1 > 0 && eb_vol > 0)
                    {
                         $('#vol_dna').val(Math.round(v1 * 10) / 10);

                         $('#vol_eb').val(Math.round(eb_vol * 10) / 10);
                    }
                    else if (v1 <= 0)
                    {
                         utils.dialog_window('The volume of DNA can not be negative or equal to zero', 'stock_conc');
                    }
                    else if (vol_eb <= 0)
                    {
                         utils.dialog_window('The volume of EB can not be negative or equal to zero', 'stock_conc');
                    }
               }

               else if (c1 <= 5)
               {
                    utils.dialog_window('Stock Concentration needs to be greater than 5 for this calculator to work', 'stock_conc');
               }
               else if (c2 <= 0)
               {
                    utils.dialog_window('Target Concentration needs to be greater than 0', 'target_conc');
               }
               else if (v2 <= 0)
               {
                    utils.dialog_window('Dilution final volume needs to be greater than 0', 'dilution_final_vol');
               }
          }
     }

     function _SetView()
     {

     }

     function _start(php_vars)
     {
          _SetView();
          _BindEvents(php_vars);
     }

     return {
          start:_start
     };
});
