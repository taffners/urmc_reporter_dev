define(['models/utils'],function(utils)
{
     function _BindEvents(php_vars)
     {
  
          // for each editable-select add restrictions on data length          
          var  field_lens = {'column_name[]': '255'};

          utils.typeRestrictionField(field_lens);  

          // insert activate button to insert and remove fields with a known field name.
          require(['views/events/template_inputs_with_del_btn'], function(template_inputs_with_del_btn)
          {
               template_inputs_with_del_btn.StartEvents(field_lens);
          });
          
          // At the top of the page the reflex name is an auto generate name that occurs on any new linker change
          // update reflex name with the order of tests
          // Example name:
               // CALR (trigger: Negative) reflex to JAK2 (trigger: Negative) reflex to NGS Myeloid
          $('.div-to-append-linked-tests-required-field, input[class="added-radio-update-name"]').on('change', function()
          {
               var  i,
                    tests = [],
                    results = []
                    curr_reflex_name = '';

               $('.div-to-append-linked-tests-required-field').each(function()
               {
                    if ($(this).val().length > 0)
                    {
                         tests.push($(this).val());     
                    }                   
               });

               $('input[class="added-radio-update-name"]:checked').each(function()
               {
                    if ($(this).val().length > 0)
                    {
                         results.push($(this).val());     
                    } 
               })

               for (i=0; i<tests.length; i++)
               {
                    if (i != 0)
                    {
                         curr_reflex_name += ' reflex to ';
                    }
                    curr_reflex_name += tests[i];

                    if (tests.length === results.length && results[i] !== 'Last Reflex Test')
                    {
                         curr_reflex_name += ' (trigger: ' + results[i]+')';     
                    }                   
               }   

               $('#reflex_name').val(curr_reflex_name);
          });


     }

     function _setLongDefault()
     {
          $('.long-read-sample-type-option').show();
          $('.short-read-sample-type-option').hide();
     }

     function _setShortDefault()
     {
          $('.long-read-sample-type-option').hide();
          $('.short-read-sample-type-option').show();
     }

     function _otherSample(view_status)
     {
          if (view_status === 'show')
          {
               $('.other-sample-number-option').show();     
          }
          else
          {
               $('.other-sample-number-option').hide();
          }         
     }

     function _longNoShort(view_status)
     {
          if (view_status === 'show')
          {
               $('.long-read-num-without-short').show();     
          }
          else
          {
               $('.long-read-num-without-short').hide();
          } 
     }


     function _SetView()
     {
          _setShortDefault();
          _otherSample('hide');
     }

     function _start(php_vars)
     {
          _SetView();
          _BindEvents(php_vars);
     }

     return {
          start:_start
     };
});
