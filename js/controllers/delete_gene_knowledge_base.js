define(['models/utils'], function(utils)
{
     function _BindEvents(php_vars)
     {
          // get gene info from cosmic_tsv and update in knowledge base
          $('.delete-gene-in-knowledge-base').on('click', function()
          {
               var gene_name = $(this).data('gene');

               // send gene to update to server
               $.ajax(
               {
                    type:'POST',
                    url:'utils/delete_knowledge_base_for_gene.php?',
                    dataType:'json',
                    data:{
                         'gene': gene_name
                    },
                    success:function(response)
                    {
console.log(response);
                    },
                    error: function(textStatus, errorThrown)
                    {
                         utils.dialog_window('problem with ajax call ;( ' + errorThrown + ' ' + textStatus);
                    }

               });
          });
     }

     function _SetView()
     {

     }

     function _start(php_vars)
     {
          _SetView();
          _BindEvents(php_vars);
     }

     return {
          start:_start
     };
});
