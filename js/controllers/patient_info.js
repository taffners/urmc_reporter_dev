define(function()
{
     function _BindEvents(php_vars)
     {
          // Add to tables with a class of dataTable the sorting ability from bootstraps datatables https://datatables.net/manual/styling/bootstrap
          require(['views/events/TableEvents'], function(TableEvents)
          {
               TableEvents.StartEvents();
          });

          // Add visit info form events
          // Including submitting form and data checks
          require(['views/events/forms/patient_info_form'], function(patient_info_form)
          {
               patient_info_form.StartEvents();
          });

          // enable date field calendar popups
          require(['views/events/date_fields'], function(date_fields)
          {
               date_fields.StartEvents();
          });

          // enable submit button once a change has occurred in type=text fields
          require(['views/events/activate_submit_button'], function(activate_submit_button)
          {
               activate_submit_button.StartEvents();
          });

          // toggle progress report bar
          require(['views/events/toggle_progress_bar'], function(toggle_progress_bar)
          {
               toggle_progress_bar.StartEvents();
          });
     }

     function _SetView()
     {

     }

     function _start(php_vars)
     {
          _SetView();
          _BindEvents(php_vars);
     }

     return {
          start:_start
     };
});