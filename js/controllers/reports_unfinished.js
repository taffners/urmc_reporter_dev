define(['models/utils'],function(utils)
{

  var  _comment_type = "knowledge_base_table";

     function _BindEvents(php_vars)
     {
          // Add to tables with a class of dataTable the sorting ability from bootstraps datatables https://datatables.net/manual/styling/bootstrap
          require(['views/events/TableEvents'], function(TableEvents)
          {
               TableEvents.StartEvents();
          });   

          
          // a click on the pending report will bring up edit report 
          require(['views/events/showEditReport'], function(showEditReport)
          {
               showEditReport.StartEvents();
          });

 
     }

     function _AddTableRows(ajax_response, tbody_id, insert_order)
     {
          // (json, str, array) ->
          // Purpose:


     }


     function _SetView()
     {

     }

     function _start(php_vars)
     {
          _SetView();
          _BindEvents(php_vars);
     }

     return {
          start:_start
     };
});