define(['models/utils','models/google_charts'],function(utils, google_charts)
{
     function _BindEvents(php_vars)
     {
  
          // Toggle test that are shown in the table depending on if the checkbox is on or not
          $('.tracked_tests-ckbx').on('click', function()
          {
               var  checked = $(this).prop('checked'),
                    test_name_class = $(this).data('test_name'),
                    test_name = test_name_class.replace('test_', '');

               if (checked)
               {
                    // show the rows that match the test name checked
                    $('.'+test_name_class).show().removeClass('d-none');

                    // append test name to the all-tracked-samples-table-header-span
                    $('#all-tracked-samples-table-header-span')
                         .append(' <span id="'+test_name+'_table_header">'+test_name+'</span>');


               }
               else
               {
                    $('.'+test_name).hide().addClass('d-none'); 

                    // Remove test name to the all-tracked-samples-table-header-span
                    $('#'+test_name+'_table_header').remove();
                         
   
               }
          })

          // if the year or the month is changed redirect to new page
          $('#month, #year').on('change', function()
          {
               var  selected_month = $('#month').val(),
                    selected_year = $('#year').val();
               
               window.location.href = '?page=performance_audit_worksheet&filter=months&month='+selected_month+'&year='+selected_year;

          });


          //////////////////////////////////////////////////////////////////
          // Edit Table Cell events
          //////////////////////////////////////////////////////////////////
          // start editing cell
          // Start editing cell
          // reference https://codewithmark.com/easily-edit-html-table-rows-or-cells-with-jquery
          var  DATE_BEFORE_EDIT;

          $('.row_data').on('click', function(event)
          {             
               event.preventDefault(); 

               var  row_div = $(this);             
               
               DATE_BEFORE_EDIT = row_div.html().trim();

               if($(this).attr('edit_type') == 'button')
               {
                    return false; 
               }

               //make div editable
               $(this).closest('div').attr('contenteditable', 'true');

               //add bg css
               $(this).addClass('bg-warning').css('padding','5px').css('color', 'black');

               $(this).focus();             
          });

          // save cell into database
          $('.row_data').on('focusout', function(event) 
          {
               event.preventDefault();

               var  row_id = $(this).closest('tr').attr('row_id'), 
                    row_div = $(this),             
                    col_name = row_div.attr('col_name'),
                    val = row_div.html().trim(),
                    ordered_test_id = $(this).attr('ordered_test_id'),
                    post_array = {};
                    post_array['col_val'] = val;
                    post_array['ordered_test_id'] = ordered_test_id;
                    post_array['col_name'] = col_name,
                    dateReg = /([12]\d{3}-(0[1-9]|1[0-2])-(0[1-9]|[12]\d|3[01]))/;

                    if (!val.match(dateReg) || val === DATE_BEFORE_EDIT)
                    {
                         row_div.removeClass('bg-warning').css('padding','').html(DATE_BEFORE_EDIT);
                    }
                    else
                    {
                         // Add data new date to ordered_test_table
                         $.ajax(
                         {
                              type:'POST',
                              url:'utils/ordered_test_update_dates.php',
                              dataType:'text',
                              data:post_array,
                              success: function(response)
                              {

                              },
                              error: function(textStatus, errorThrown)
                              {
                                   utils.dialog_window('problem with ajax call ;( ' + errorThrown + ' ' + textStatus);
                              }
                         });
                    }

               ////////////////////////////////////////////////////////
               // Add data to data base
                    // If row_id is not present add a new row to strand_bias_table
                         // Refresh page after entry so the rest of the fields
                         // will have the new strand_bias_id
                    // Make sure field is not empty and only numerical 
               ////////////////////////////////////////////////////////
               // if (utils.isPositveNumber(val))
               // {
               //      // use ajax to add to db
               //      $.ajax(
               //      {
               //           type:'POST',
               //           url: 'utils/update_strand_bias.php?',

               //           dataType: 'text',
               //           data: post_array,
               //           success: function(response)
               //           {
               //                var  sb_response = JSON.parse(response);

               //                // if sb_id originally empty reload page or strand_bias
               //                // might have changed
               //                if (sb_id == '' || sb_response['strand_bias'] != '')
               //                {
               //                     window.location.reload(true);
               //                }                                                 
               //           },
               //           error: function(textStatus, errorThrown)
               //           {
               //                utils.dialog_window('problem with ajax call ;( ' + errorThrown + ' ' + textStatus);
               //           }
               //      });  
               //      // if row_id is empty refresh page

               //      // if all ref -, ref +, variant -, and variant + are not empty 
               //      // calculate strand bias
               // }

               // else
               // {
               //      error_message = 'Enter an integer';
               //      utils.dialog_window(error_message, 'AlertId');
               //      row_div.html('___');
               // }
               row_div.removeClass('bg-warning').css('padding','');               
          });   


          $('.barcode-insert-here').each(function()
          {
               var  curr_id = $(this).attr('id'),
                    barcode = $(this).data('barcode');

               _displayBarcode(curr_id, barcode);
          });
               // _displayBarcode('insert-here-test', 'test');
// console.log('here')               
//                $("#insert-here-test").JsBarcode("Hi!");
     }


     function _displayBarcode(insert_id, barcode)
     {
          // find if insert id exists before displaying

          if ($("#"+insert_id).length != 0)
          {
               JsBarcode("#"+insert_id, barcode,
               {
                    width: 1,
                    height: 40
               });
          }

          
     }


     function _SetView()
     {

     }

     function _start(test_status, php_vars)
     {
          _SetView();
          _BindEvents(php_vars);

     }

     return {
          start:_start
     };
});
