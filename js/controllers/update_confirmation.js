define(function()
{
     function _BindEvents(php_vars)
     {

          // enable submit button once a change has occurred in type=text fields
          require(['views/events/activate_submit_button'], function(activate_submit_button)
          {
               activate_submit_button.StartEvents();
          });

          $('form').on('keyup keypress', function(e) 
          {
               var keyCode = e.keyCode || e.which;
               
               if (keyCode === 13) 
               { 
                    e.preventDefault();
                    return false;
               }
          });

          // Toggle the confirmation result in the message depending on what outcome is selected
          $('input[name="outcome"]').on('change',function()
          {
                    var  outcome = $(this).val(),
                         message = $('textarea[name="message"]').text();

                    if (outcome === 'not confirmed')
                    {
                         message = message.replace('CONFIRMATION RESULT: confirmed', 'CONFIRMATION RESULT: not confirmed');
                    }
                    else
                    {
                         message = message.replace('CONFIRMATION RESULT: not confirmed', 'CONFIRMATION RESULT: confirmed');
                    }

                    $('textarea[name="message"]').text(message);
          });
     }

     function isInViewPoint(select_id)
     {
          // make sure id exists 
          if ($('#' + select_id).length == 0)
          {
                return false;
          }

          let  elem = document.querySelector('#' + select_id),
               elementTop =  elem.getBoundingClientRect()['top'],
               elementBottom = elementTop + $('#' + select_id).outerHeight(),
               viewportTop = $(window).scrollTop(),
               viewportBottom = viewportTop + window.outerHeight ;
        
          return elementBottom < viewportBottom;
     }

     function getDocHeight() 
     {
          let D = document;
          
          return Math.max(
               D.body.scrollHeight, D.documentElement.scrollHeight,
               D.body.offsetHeight, D.documentElement.offsetHeight,
               D.body.clientHeight, D.documentElement.clientHeight
          );
     }

     function _SetView()
     {

     }

     function _start(php_vars)
     {
          _SetView();
          _BindEvents(php_vars);
     }

     return {
          start:_start
     };
});
