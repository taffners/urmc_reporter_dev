define(['models/utils'], function(utils)
{
     function _BindEvents(php_vars)
     {          
          // Add to tables with a class of dataTable the sorting ability from bootstraps datatables https://datatables.net/manual/styling/bootstrap
          require(['views/events/TableEvents'], function(TableEvents)
          {
               TableEvents.StartEvents();
          });
     
          // // In the variant table cells larger than 20 characters are cut.  This will enable the more button and less button
          require(['views/events/toggle_teaser_complete_strs'], function(toggle_teaser_complete_strs)
          {
               toggle_teaser_complete_strs.StartEvents();
          });

          // toggle progress report bar
          require(['views/events/toggle_progress_bar'], function(toggle_progress_bar)
          {
               toggle_progress_bar.StartEvents();
          });

          /////////////////////////////////////////////////////////////////
          // Make hover card to describe strand bias
          /////////////////////////////////////////////////////////////////
          strand_bias_info = 'Strand bias occurs when the genotype inferred from information presented by the forward strand and the reverse strand disagrees.  For example, at a given position in the genome, the reads mapped to the forward strand support a heterozygous genotype, while the reads mapped to the reverse strand support a homozygous genotype.';

          $('#strand-bias-descript-hover-card').hovercard(
          {
               detailsHTML: strand_bias_info,
               width: 550              
          });

          //////////////////////////////////////////////////////////////////
          // Edit Table Cell events
          //////////////////////////////////////////////////////////////////
          // start editing cell
          // Start editing cell
          // reference https://codewithmark.com/easily-edit-html-table-rows-or-cells-with-jquery
          $('.row_data').on('click', function(event)
          {             
               event.preventDefault(); 

               if($(this).attr('edit_type') == 'button')
               {
                    return false; 
               }

               //make div editable
               $(this).closest('div').attr('contenteditable', 'true');

               //add bg css
               $(this).addClass('bg-warning').css('padding','5px').css('color', 'black');

               $(this).focus();             
          });

          // save cell into database
          $('.row_data').on('focusout', function(event) 
          {
               event.preventDefault();

               var  row_id = $(this).closest('tr').attr('row_id'), 
                    row_div = $(this),             
                    col_name = row_div.attr('col_name'),
                    val = row_div.html().trim(),
                    sb_id = $(this).attr('id').replace(col_name+'_', ''),
                    post_array = {};
                    post_array['col_val'] = val;
                    post_array['strand_bias_id'] = sb_id;
                    post_array['col_name'] = col_name;

                    if (row_id != '')
                    {
                         post_array['observed_variant_id'] = row_id;
                    }

               ////////////////////////////////////////////////////////
               // Add data to data base
                    // If row_id is not present add a new row to strand_bias_table
                         // Refresh page after entry so the rest of the fields
                         // will have the new strand_bias_id
                    // Make sure field is not empty and only numerical 
               ////////////////////////////////////////////////////////
               if (utils.isPositveNumber(val))
               {


                    // use ajax to add to db
                    $.ajax(
                    {
                         type:'POST',
                         url: 'utils/update_strand_bias.php?',

                         dataType: 'text',
                         data: post_array,
                         success: function(response)
                         {
                              var  sb_response = JSON.parse(response);

                              // if sb_id originally empty reload page or strand_bias
                              // might have changed
                              if (sb_id == '' || sb_response['strand_bias'] != '')
                              {
                                   window.location.reload(true);
                              }                                                 
                         },
                         error: function(textStatus, errorThrown)
                         {
                              utils.dialog_window('problem with ajax call ;( ' + errorThrown + ' ' + textStatus);
                         }
                    });  
                    // if row_id is empty refresh page

                    // if all ref -, ref +, variant -, and variant + are not empty 
                    // calculate strand bias
               }

               else
               {
                    error_message = 'Enter an integer';
                    utils.dialog_window(error_message, 'AlertId');
                    row_div.html('___');
               }
               row_div.removeClass('bg-warning').css('padding','').css('color', 'white');               
          });  

     }

     function _SetView()
     {

     }

     function _start(php_vars)
     {
          _SetView();
          _BindEvents(php_vars);
     }

     return {
          start:_start
     }
});