define(['models/utils'], function(utils)
{
     function _BindEvents(php_vars)
     {

          // toggle progress report bar
          require(['views/events/toggle_progress_bar'], function(toggle_progress_bar)
          {
               toggle_progress_bar.StartEvents();
          });

          $('#ngs_run_date').on('change', function()
          {
               let ngs_run_date = $(this).val();

               if (!utils.strIsDateMMYYDD(ngs_run_date))
               {
                    utils.dialog_window('The sequencing date format needs to be DD/MM/YYYY', 'ngs_run_date');    
               }
               else
               {
                    _setRunPlanNames(ngs_run_date);     
               }
               
          });

          // download csv templates
          $('#download-ofa-template').on('click', function()
          {
               var  _pool_chip_linker_id = $(this).data('pool_chip_linker_id'),

                    // Obtain from HTML/PHP how many chips there are in the library
                    _chip_num = $('#chip_count').val(),
                    _rundate = $('#ngs_run_date').val();                      

               if (_rundate.length !== 0 && utils.strIsDateMMYYDD(_rundate))
               {
                    if (_chip_num == 2)
                    {               
                         _download_ofa_run_template(_pool_chip_linker_id, 'A');
                         _download_ofa_run_template(_pool_chip_linker_id, 'B');
                    }
                    else
                    {    
                         _download_ofa_run_template(_pool_chip_linker_id, 'A');
                    }
               }
               else
               {
                    utils.dialog_window('Add a sequencing date', 'ngs_run_date');
               }
          });

     }

     function _setRunPlanNames(seq_date)
     {
          let  num_chips = $('#chip_count').val();

          _setRunPlan(seq_date, 'A');


          if (num_chips == 2)
          {
               _setRunPlan(seq_date, 'B');
          }        
     }

      function _setRunPlan(seq_date,chip)
     {
          let  excel_workbook_num_id = $('#excel_workbook_num_id').val(),
               library_pool_id = $('#library_pool_id_' + chip).val(),

               OFA_PLANNAME = $('#OFA_PLANNAME').val(),
               run_plan_name
               seq_date_YYYYMMDD = utils.US_date_to_YYYYMMDD(seq_date);


          run_plan_name = 'Run' + excel_workbook_num_id + '_' + library_pool_id + '_';
          run_plan_name+= seq_date_YYYYMMDD + '_'+chip+'_' + OFA_PLANNAME;

          $('#run-name-chip-'+chip.toLowerCase()+'-copy-paste').empty().html(run_plan_name);
          _displayBarcode('run-name-chip-'+chip.toLowerCase()+'-barcode', run_plan_name);
          $('#run_plan_name_' + chip).val(run_plan_name);

     }


     function _displayBarcode(insert_id, barcode)
     {
          // find if insert id exists before displaying

          if ($("#"+insert_id).length != 0)
          {
               JsBarcode("#"+insert_id, barcode,
               {
                    width: 1,
                    height: 40
               });
          }

          
     }


     function _updateClassPattern(update_class, pattern)
     {
          $('.'+update_class).each(function()
          {
               $(this).attr('pattern', pattern);
          });
     }

     function _extractChipBarcode(scanned_barcode)
     {
          // Thermofisher places barcodes on the chips when scanned they contain 19 digits however, the torrent server extracts a subset of the 19 digit string and calls it the chipBarcode.  This function will take a 19 digit string and extract 11 digits from [1:12]
          // Example
          // (21DAEJ00624241530v1) -> DAEJ0062424

          return scanned_barcode.substring(2,12)
     }

     function _download_ofa_run_template(pool_chip_linker_id, chip)
     {
          var  file_name = $('#run_plan_name_' + chip).val()+'_run_template.csv';
          
          $.ajax(
          {
               type:'POST',
               url: 'utils/download_ofa_run_template.php?',

               dataType: 'text',
               data: {
                         'chip': chip, 
                         'pool_chip_linker_id': pool_chip_linker_id
               },
               success: function(response)
               {
                    // csv_response is a string formated like a csv file.
                    // use window.open and encodeURI functions to download the CSV file
                    var  csv_json = JSON.parse(response),
                         csv_content = "data:text/csv;charset=utf-8,",
                         chip = csv_json['chip'],
                         pool_chip_linker_id = csv_json['pool_chip_linker_id'];

                    $.each(csv_json, function(i, row)
                    {
                         
                         if (i !== 'chip' && i !== 'pool_chip_linker_id')
                         {
                              csv_content += row + "\r\n";
                         }
                    }); 

                    var  encodedUri = encodeURI(csv_content),
                         link = document.createElement("a"),
                         today = utils.GetTodaysDateYYMMDD();

                    link.setAttribute("href", encodedUri);
                    link.setAttribute("download", file_name);
                    document.body.appendChild(link); // Required for FF

                    link.click(); // This will download the data file named "my_data.csv".
               },
               error: function(textStatus, errorThrown)
               {
                    dialog_window('problem with ajax call ;( ' + errorThrown + ' ' + textStatus);
               }
          });
     }

     function _SetView()
     {
          var  _chip_a_barcode = $('#run_plan_name_A').val(),
               _chip_b_barcode = $('#run_plan_name_B').val();

          // add barcodes initially
          _displayBarcode("run-name-chip-a-barcode", _chip_a_barcode);
          _displayBarcode("run-name-chip-b-barcode", _chip_b_barcode);
     }

     function _start(_test_status, php_vars)
     {
     
          _SetView();
       
          _BindEvents(php_vars);
     }

     return {
          start:_start
     };
});
