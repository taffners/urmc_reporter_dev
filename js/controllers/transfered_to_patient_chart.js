define(['models/utils', 'models/tour'],function(utils, tour)
{
     function _BindEvents(php_vars)
     {
          // Add to tables with a class of dataTable the sorting ability from bootstraps datatables https://datatables.net/manual/styling/bootstrap
          require(['views/events/TableEvents'], function(TableEvents)
          {
               TableEvents.StartEvents();
          });

          ////////////////////////////////////////////////////////////////////////////
          // Change scan status
          ////////////////////////////////////////////////////////////////////////////
          require(['views/events/update_scan_status'], function(update_scan_status)
          {
               update_scan_status.StartEvents();
          });


          $('#take-a-tour-btn').removeClass('d-none').on('click', function()
          {
               tour.start_tour([
                    {
                         backdrop: true,
                         orphan: true,
                         content: '<h3 style="text-align: center;"><b>Purpose</b></h3>The purpose of this page is to track the scanning process of NGS reports into SOFTPATH LIS module for sign-out by a pathologist.'
                    },
                    {
                         element: '#not-complete-scan-status-btn',
                         placement: 'right',
                         content: '<h3 style="text-align: center;"><b>Pending Scan Status</b></h3>Click here to show all reports that have <b>not</b> been scanned into patient charts'
                    },
                    {
                         element: '#complete-scan-status-btn',
                         placement: 'right',
                         content: '<h3 style="text-align: center;"><b>Complete Scan Status</b></h3>Click here to show all reports that have been scanned into patient charts'
                    },
                    {
                         element: '#softpath-users',
                         placement: 'right',
                         content: '<h3 style="text-align: center;"><b>Responsible Persons</b></h3>If you name is listed here it is your responsibility to keep this list up to date.'
                    },
                    {
                         element: '#pdf-header',
                         placement: 'right',
                         content: '<h3 style="text-align: center;"><b>Show Reports</b></h3>Click green buttons with pdf icons in this column to show the reports.'
                    },
                    {
                         element: '#scan-status-update-header',
                         placement: 'right',
                         content: '<h3 style="text-align: center;"><b>Scan Status</b></h3>After reports are made, it is your responsibility to follow the Scanning molecular NGS reports to SOFTPATH sop and update this list via the radio buttons in this column.'
                    },
                    {
                         element: '#training-btn',
                         placement: 'bottom',
                         content: '<h3 style="text-align: center;"><b>SOP</b></h3>The Scanning Molecular NGS reports into SOFTPATH sop can be found by clicking on training -> FAQ -> Describe softpath level permissions'
                    }

               ], 'The purpose of this page is to track reports as they are scanned into patient charts.');
          });

          
     }

     


     function _start(php_vars)
     {

          _BindEvents(php_vars);
     }

     return {
          start:_start
     };
});
