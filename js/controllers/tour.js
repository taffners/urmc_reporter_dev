define(function()
{
     let  site_title = 'Molecular Diagnostics Infotrack';

     function _start_tour(tour_data, page_purpose)
     {
          //Purpose: make a tour using bootstrap tour

          let  templateWithoutNextPrev = "<div class='popover tour'><div class='arrow'></div><h3 class='popover-title'></h3><div class='popover-content'></div><div class='popover-navigation'><button class='btn btn-default' data-role='end' style='margin-bottom:10px;'>End tour</button></div></div>",
               tour = new Tour(
               {
                    storage:false
               });

          // add an introduction to the tour
          tour_data.unshift({
                         backdrop: true,
                         orphan: true,
                         content: 'Welcome to the tour for <b>'+site_title+'</b>.  This tour is to orient you to the current page.  <br><br>' + page_purpose
                    })

          tour.addSteps(tour_data);



          // Initialize the tour
          tour.init(true);

          // Start the tour
          tour.start(true);
     };

     return {
          start_tour:_start_tour
     };
});
