define(['models/utils','models/google_charts'],function(utils, google_charts)
{
     function _BindEvents(php_vars)
     {
          // Toggle test that are shown in the table depending on if the checkbox is on or not
          $('.tracked_tests-ckbx').on('click', function()
          {
               var  checked = $(this).prop('checked'),
                    test_name_class = $(this).data('test_name'),
                    test_name = test_name_class.replace('test_', ''),
                    orderable_tests_id = $(this).data('orderable_tests_id'),
                    turnaround_time_tracked;

               if (checked)
               {
                    turnaround_time_tracked = 'yes';
               }
               else
               {
                    turnaround_time_tracked = 'no';    
   
               }

                    $.ajax(
                    {
                         type:'POST',
                         url: 'utils/update_turnaround_time_tracked.php?',

                         dataType: 'text',
                         data: {
                              'turnaround_time_tracked': turnaround_time_tracked,
                              'orderable_tests_id':orderable_tests_id
                         },
                         success: function(response)
                         {
console.log(response)                                                                            
                         },
                         error: function(textStatus, errorThrown)
                         {
                              utils.dialog_window('problem with ajax call ;( ' + errorThrown + ' ' + textStatus);
                         }
                    }); 
          });


         
     }

     function _SetView()
     {

     }

     function _start(test_status, php_vars)
     {
          _SetView();
          _BindEvents(php_vars);

     }

     return {
          start:_start
     };
});
