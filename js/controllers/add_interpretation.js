define(['models/utils'],function(utils)
{

  var  _comment_type = "knowledge_base_table";

     function _BindEvents(php_vars)
     { 
          
          // // In the variant table cells larger than 20 characters are cut.  This will enable the more button and less button
          require(['views/events/toggle_teaser_complete_strs'], function(toggle_teaser_complete_strs)
          {
               toggle_teaser_complete_strs.StartEvents();
          });
          
          // // add a comment to the comment table
          // $('.submit_interp').on('click', function()
          // { 
          //      // get id of knowledge
          //      // get comment related to the knowledge id
          //      var  knowledge_id = $(this).attr('id').replace('submit_interp_', ''),
          //           textarea_id = '#textarea_' + knowledge_id,
          //           textarea = $(textarea_id).val();
                              

          //      // check input not empty and not larger than 65535
          //      if (textarea !== '' && textarea.length < 65535 && textarea !== localStorage.getItem('previous_comment'))
          //      {
                   
          //           $.ajax(
          //           {
          //                type: 'POST',
          //                url: 'utils/add_comment.php',
          //                dataType: 'json',
          //                data: {
          //                          'user_id': '1',
          //                          'comment_ref': knowledge_id,
          //                          'comment_type': _comment_type,
          //                          'comment':textarea
          //                     },
          //                success: function(response)
          //                {
          //                     // reload page
          //                     window.location.reload();
          //                } 
          //           });
                    

          //      }
          //      else
          //      {
          //           utils.dialog_window('Please make sure the input is not empty, not larger than 65,535 characters, and not the same as the previous interpretation.', textarea)
          //      }
               
          // });


          // if a row of interpretation is clicked add text to textarea 
          $('.interp_table_row').on('click', function()
          {
               // get contents of td .comment_cell
               var  previous_comment = $(this).find('.comment_cell').text().trim(),
                    knowledge_id = $(this).attr('id').split('_')[0];

               // add previous comment to both local storage and the textarea          
               $('#textarea_' + knowledge_id).val(previous_comment);

               localStorage.setItem('previous_comment', previous_comment);

               // show textarea
               $('#div_textarea_' + knowledge_id).show();

               // scroll to textarea
               utils.scroll('#toggle_textarea_btn_' + knowledge_id);

          });

          // activate tier modal
          require(['views/events/tier_update_delete'], function(tier_update_delete)
          {
               tier_update_delete.StartEvents();
          });   

          // show interpretation and tier info should by unhiding unhide tier-interpretation-section
          $('#tier-interpretation-section').show();

          // Add to tables with a class of dataTable the sorting ability from bootstraps datatables https://datatables.net/manual/styling/bootstrap
          require(['views/events/TableEvents'], function(TableEvents)
          {
               TableEvents.StartEvents();
          });   

     }

     function _AddTableRows(ajax_response, tbody_id, insert_order)
     {
          // (json, str, array) ->
          // Purpose:


     }


     function _SetView()
     {

     }

     function _start(php_vars)
     {
          _SetView();
          _BindEvents(php_vars);
     }

     return {
          start:_start
     };
});