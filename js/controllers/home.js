define(['models/utils', 'models/tour'],function(utils, tour)
{
     function _BindEvents(php_vars)
     {
          // if the browser is not chrome 
          utils.CheckBrowser();  
              
          // Add to tables with a class of dataTable the sorting ability from bootstraps datatables https://datatables.net/manual/styling/bootstrap
          require(['views/events/TableEvents'], function(TableEvents)
          {
               TableEvents.StartEvents(true);
          });

          // a click on the pending report will bring up edit report 
          require(['views/events/showEditReport'], function(showEditReport)
          {
               showEditReport.StartEvents();
          });

          // scroll to variants 
          require(['views/events/scroll_events'], function(scroll_events)
          {
               scroll_events.StartEvents();
          });

          // make ajax call to toggle pending status of sample
          $('.pending_pre_sample_toggle').on('click', function()
          {
               var _visit_id = $(this).attr('id').replace('pending_toggle_', '');

               // send visit_id to php.  Where a query is to see if visit_id and step pre_pending is 
               // is in pre_step_visit table.  

               // If visit_id in pre_step_visit table toggle to not pending.
                    // Delete from db
                    // return ??

               // If visit_id not in pre_step_visit table toggle to pending.
                    // Add to db
                    // return ??

              $.ajax(
              {
                  type: 'POST',
                  url: 'utils/update_pre_pending.php',
                  // dataType: 'json',
                  data: {'visit_id': _visit_id} 
              })
              .done(function(pending_status)
              {               
                    // activate dna button
                    if (pending_status == 'on')
                    {
                         
                         $('#add_dna_conc_btn_' + _visit_id).removeClass('disabled').addClass();
                    }
                    else
                    {
                         $('#add_dna_conc_btn_' + _visit_id).addClass('disabled');
                    }
               });

               $(this).toggleClass('btn-success btn-warning');
               location.reload();
               return false;

          });  

          $('.open_pool').on('click',function()
          {
               var  pool_chip_linker_id = $(this).attr('id').replace('pool_', ''),
                    ngs_panel_id = $(this).data('ngs_panel_id');

               // go to a showEditReport with id of run
               window.location.href = '?page=pool_summary&pool_chip_linker_id=' + pool_chip_linker_id+'&ngs_panel_id='+ngs_panel_id;
          });  


                         
          $('#take-a-tour-btn').removeClass('d-none').on('click', function()
          {
               $('#ngs-dropdown').addClass('open');

               let  i,
                    color_keys = tour.color_keys(),
                    table_info = tour.table_info(),
                    sample_log_book_info = tour.sample_log_book_info(),
                    tour_data,
                    tour_data_len;

               tour_data = [
                    {
                         backdrop: true,
                         orphan: true,                         
                         content: '<h3 style="text-align: center;"><b>'+site_title+'</b></h3>'+site_title + ' is an electronic medical record system built for the University of Rochester Clinical Molecular Diagnostics Laboratory.<br><br>It is developing into a one stop, start to finish software tool where samples are tracked from login to report generation.<br><br>Single analyte and NGS tests are all logged into MD Infotrack providing a test tracking system which allows managers the ability to easily produce monthly reports and track audits to evaluate laboratory productivity.<br><br>MD Infotrack has evolved into an analysis and report building system for NGS tests. Allowing users to access the growing knowledge base along with integrated publicly available databases.<br><br><b>Created by:</b> Samantha Taffner'
                    },
                    {
                         backdrop: true,
                         orphan: true,
                         content: '<h3 style="text-align: center;"><b>Home Page</b></h3>Homepage views will differ tremendously depending on your permission level.  <br><br>With NGS permissions your homepage provides access to all NGS reports.  <br><br>Users with only log book permissions will see the sample log book as their home page.  <br><br>The most basic view will be users without either NGS or log book permissions.  This view will leave your homepage blank.'
                    },
                    {
                         element: 'nav',
                         placement: 'bottom',
                         backdrop: true,
                         content: '<h3 style="text-align: center;"><b>Tool Bar</b></h3>To find out more information about the tool bar hover over each icon and a description will appear.  Give it a try.  Throughout ' + site_title + 'built in hover help menus are available.'
                    },
                    {
                         element: '#version-update-alert',
                         placement: 'bottom',
                         backdrop: true,
                         content: '<h3 style="text-align: center;"><b>Updates</b></h3>'+site_title + ' updates very often.  All recent update information will be provided here.'
                    },
                    {
                         element: '#toggle-samples-div',
                         placement: 'top',
                         backdrop: true,
                         content: '<h3 style="text-align: center;"><b>Filter Samples Shown</b></h3>The samples shown on the NGS home page can be toggled using the buttons to display OFA, Myeloid, or both OFA and Myeloid samples.<br><br>Green button indicates active view.'
                    },
                    {
                         element: '#pre-ngs-samples-list-div',
                         placement: 'top',
                         backdrop: true,
                         content: '<h3 style="text-align: center;"><b>Overview of Pre NGS workflow</b></h3><ul><li>Add all samples to the sample log book as they arrive</li><li>Add DNA extraction test to sample if necessary</li><li>Complete DNA extraction test</li><li>Add NGS test to sample</li><li>Add controls Samples</li><li>Add NGS library Pool</li><li>Follow NGS library pool workflow</li><li>Upload Variant TSV and Coverage File</li></ul>'
                    },
                    {
                         element: '#pre-ngs-samples-list-div',
                         placement: 'top',
                         backdrop: true,
                         content: '<h3 style="text-align: center;"><b>Pre NGS Samples</b></h3>The Pre NGS samples list shows all samples waiting to be sequenced.'
                    },
                    
                    {
                         element: '#show-send-outs-btn',
                         placement: 'left',
                         backdrop: true,
                         content: '<h3 style="text-align: center;"><b>NGS Send Outs</b></h3>NGS samples that are sent out to outside labs can be seen by clicking on this button. To see all the non send out samples again use the buttons in the Toggle samples which appear by NGS panel section.'
                    },
                    {
                         element: '#pre-ngs-visit-col',
                         placement: 'left',
                         
                         content: '<h3 style="text-align: center;"><b>Pre NGS Color Code</b></h3>Circles indicate stage of processing. Each circle can be clicked.  Most circles are buttons which will bring up a form to edit information corresponding to the step.<br><br><img src="images/color_scheme_key_step_processing.png" width="400px"><br>The circles are not accessible by users with View Only permissions. Samples can be added to this table by adding a NGS test to the sample log book. '
                    },
                    {
                         element: '#pre-ngs-cancel-col',
                         placement: 'left',
                        
                         content: '<h3 style="text-align: center;"><b>Cancel Pre NGS tests</b></h3>To cancel pre NGS tests use the <button class="btn btn-danger" title="Cancel Test"><span class="fa fa-times"><span></span></span></button> each row in the pre ngs samples table. <br><br>This will remove samples from any pools also.'
                    },
                    
                    
                    {
                         element: '#pre-ngs-editors-completed-steps',
                         placement: 'top',
                         
                         content: '<h3 style="text-align: center;"><b>Editor / Completed Steps</b></h3>The editors / completed steps column shows the initial user that completed each step.  It does not reflect any updates.'
                    },
                    
                    
                    {
                         //wait 
                         element: '#pending-ngs-library-pool-table-head > tr',
                         placement: 'top',
                                                
                         content: '<h3 style="text-align: center;"><b>Library Pools Table</b></h3>Library Pools are used to set up and organize all of your samples for the NGS run.  The Pre NGS Library Pools table show pools started within the last two weeks by default.  The library pool workflow is accessible by clicking on the row in this table.',
                         duration: 1000
                    },
                    {
                         element: '#pending-ngs-library-pool-table-head > tr',
                         placement: 'top',
                                                 
                         content: '<h3 style="text-align: center;"><b>Library Pools Table</b></h3>Library Pools are used to set up and organize all of your samples for the NGS run.  The Pre NGS Library Pools table show pools started within the last two weeks by default.  The library pool workflow is accessible by clicking on the row in this table.'
                    },
                    {
                         element: '#historic-pools-btn',
                         placement: 'left',
                         backdrop: true,
                         content: '<h3 style="text-align: center;"><b>Historic Library Pools</b></h3>All historic pools can be shown by clicking on this button.'
                    },
                    {
                         element: '#ngs-dropdown',
                         placement: 'bottom',
                         backdrop: true,
                         content: '<h3 style="text-align: center;"><b>Make a New Library Pools</b></h3>To generate a make sure all controls and samples are added first.  Then in this NGS menu choose Add Library NGS Pool'
                    },


                    {
                         element: '#outstanding-reports-row',
                         placement: 'top',
                         backdrop: true,
                         content: '',
                         duration:1000
                    },
                    {
                         element: '#outstanding-reports-row',
                         placement: 'top',
                         backdrop: true,
                         content: '<h3 style="text-align: center;"><b>Outstanding Reports Table</b></h3>Reports in the outstanding reports table are in the report generating phase.  Sequencing has been performed, sequencing data was transfered to ' + site_title + ', and the report is currently being written.<br><br>If a sample row turns green when moving the mouse over it, that indicates the Report Building workflow is accessible for the sample. Clicking on the row, will then take the user to the workflow.  This functionality is not available for users with View Only permissions.'
                    },
                    {
                         element: '#outstanding-reports-row',
                         placement: 'top',
                         backdrop: true,
                         content: '<h3 style="text-align: center;"><b>Outstanding Reports Table</b></h3>Make sure control samples are signed out before patient samples.'
                    },
                    {
                         element: '#outstanding-reports-editors-completed-steps',
                         placement: 'top',
                         
                         content: '<h3 style="text-align: center;"><b>Editor / Completed Steps</b></h3>The editors / completed steps column show which steps have been completed so far. It also shows the initial user that completed each step.  It does not reflect any updates.  '
                    },
                    {
                         element: '#completed-reports-row',
                         placement: 'top',
                         
                         content: '',
                         duration:1000
                    },
                    {
                         element: '#completed-reports-row',
                         placement: 'top',
                         backdrop: true,
                         content: '<h3 style="text-align: center;"><b>Completed Reports Table</b></h3>All Completed reports can be found here.  '
                    },
                    {
                         element: '#pdf-th',
                         placement: 'top',
                         
                         content: '<h3 style="text-align: center;"><b>Access Finished Report</b></h3> Click <a href="#" type="button" class="btn btn-success btn-success-hover"><img src="images/adobe-pdf-icon.png" alt="pdf icon" style="height:30px;width:30px"></a> to access the current finished report with and with out the header.  Additionally revised reports and the comments provided report building workflow can be found here.' 
                    },

                    {
                         element: '#revise-th',
                         placement: 'top',
                         
                         content: '<h3 style="text-align: center;"><b>Revise Reports</b></h3>The blue Revise button will allow the user to edit a previously completed report.  This button is only visible with Revise permissions.'
                    },
                    
                    {
                         element: 'footer',
                         placement: 'top',
                         
                         content: '',
                         duration:1000
                    },
                    {
                         element: 'footer',
                         placement: 'top',
                         backdrop: true,
                         content: '<h3 style="text-align: center;"><b>Footer Info</b></h3>The current version can be found here.'
                    }
               ];

               tour_data = utils.mergeArrays(tour_data,table_info,5);
               
               tour_data = utils.mergeArrays(tour_data,color_keys,1);

               /////////////////////////////////////////
               // Insert sample log book shared tour data the homepage is the sample log book.  Users with log book permissions but not NGS permissions will have the sample log book as their main page.
               /////////////////////////////////////////
               
               // find if the sample log book exists
               if ($('#sample-log-book-table').length != 0)
               {
                    tour_data_len = tour_data.length
                    tour_data = utils.mergeArrays(tour_data,sample_log_book_info,tour_data_len-2);
               }

               tour.start_tour(tour_data, '');
          });              
     }

     function _SetView()
     {

     }

     function _start(php_vars)
     {
          _SetView();
          _BindEvents(php_vars);
     }

     return {
          start:_start
     };
});
