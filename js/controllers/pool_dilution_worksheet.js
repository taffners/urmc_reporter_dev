define(['models/utils'], function(utils)
{
     function _BindEvents(php_vars)
     {
          // update calculation if dilution-total-js-calc was changed
          $('.dilution-total-js-calc').on('change', function()
          {
               var  visit_id = $(this).data('visit_id'),
                    new_dilution_total_val = $(this).val(),
                    dna_stock_conc = $(this).data('dna_conc'), 
                    DNA_vol = Math.round(((5 * new_dilution_total_val) / dna_stock_conc) * 10) / 10, // solution is making a 5ng/ul dilution
                    AE_vol = Math.round((new_dilution_total_val - DNA_vol) * 10) / 10;

                    // Change the span that the user sees for DNA and AE Volumne
                    $('#span_dna_vol_js_calc_'+visit_id).html(DNA_vol);
                    $('#span_ae_vol_js_calc_'+visit_id).html(AE_vol);
          });

          
          // toggle progress report bar
          require(['views/events/toggle_progress_bar'], function(toggle_progress_bar)
          {
               toggle_progress_bar.StartEvents();
          });

          
     }

     function _SetView()
     {

     }

     function _start(php_vars)
     {
          window.localStorage.removeItem('current_lib_tube_num');
          _SetView();
          _BindEvents(php_vars);
     }

     return {
          start:_start
     };
});
