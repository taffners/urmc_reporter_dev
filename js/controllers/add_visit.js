define(['models/utils'],function(utils)
{
     function _BindEvents(php_vars)
     {
          ////////////////////////////////////////////////////////////////////////
          // on change of the positive control or negative control add last version used for the 
          // control   
          ////////////////////////////////////////////////////////////////////////
          $('#control-type-id-positive, #control-type-id-negative').on('change', function()
          {
               _showLastControlVersion();
          });

          $('#ngs_panel_id').on('change', function()
          {
               _showLastControlVersion();
          });


          // toggle form for patient sample, positive control, negative control
          $('input[type=radio][name=sample_type]').on('change', function()
          {
               var  selected_type = $(this).val();

               _showLastControlVersion();

               switch (selected_type)
               {
                    case 'Patient Sample':
                         // show all patient fields and hide control fields
                         $('#all-fields-control').hide();
                         $('#all-fields-sample').removeClass('hide').removeClass('d-none').show();

                         // remove mol_num required status
                         $('#mol_num').prop('required', true);
                         $('#order_num').prop('required', true);
                         $('#soft_lab_num').prop('required', true);
                         
                         // remove control required status
                         $('#version').prop('required', false);
                         $('.version-lot-label').hide();

                         // edit the mol number which is really the order_num in the database.
                         utils.getNextMolNum(selected_type);
                         break;
                    case 'Positive Control':
                         // show all control fields and hide sample fields
                         $('#all-fields-control').removeClass('hide').removeClass('d-none').show();
                         $('#all-fields-sample').hide();

                         // remove mol_num required status
                         $('#mol_num').prop('required', false);
                         $('#order_num').prop('required', false);
                         $('#soft_lab_num').prop('required', false);
                         $('#version').prop('required', false);                         

                         // Add control required status
                         $('#version').prop('required', true);

                         // Show the label for the version which is appropriate for the selected control
                         $('.version-lot-label').hide();
                         $('#label_'+$('#control-type-id-positive').val()).show();

                         // Show positive-control-drop-down and hide negative-control-drop-down
                         $('#positive-control-drop-down').removeClass('hide').removeClass('d-none').show();
                         $('#negative-control-drop-down').hide();

                         break;
                    case 'Negative Control':
                         // show all control fields and hide sample fields
                         $('#all-fields-control').removeClass('hide').removeClass('d-none').show();
                         $('#all-fields-sample').hide();

                         // remove mol_num required status
                         $('#mol_num').prop('required', false);
                         $('#order_num').prop('required', false);
                         $('#soft_lab_num').prop('required', false);
                         
                         // remove pattern for order num
                         $('#order_num').removeAttr('pattern');

                         // Add control required status
                         $('#version').prop('required', true);

                         // Show the label for the version which is appropriate for the selected control
                         $('.version-lot-label').hide();
                         $('#label_'+$('#control-type-id-negative').val()).show();

                         // hide positive-control-drop-down and show negative-control-drop-down
                         $('#positive-control-drop-down').hide();
                         $('#negative-control-drop-down').removeClass('hide').removeClass('d-none').show();                    
                         break;
                    case 'Validation':
                    case 'Sent Out':
                         // show all patient fields and hide control fields
                         $('#all-fields-control').hide();
                         $('#all-fields-sample').removeClass('hide').removeClass('d-none').show();

                         // remove mol_num required status
                         $('#mol_num').prop('required', true);
                         $('#order_num').prop('required', true);
                         $('#soft_lab_num').prop('required', true);

                         // remove control required status
                         $('#version').prop('required', false);
                         $('.version-lot-label').hide();

                         // edit the mol number which is really the order_num in the database.  
                         // mol_num and order_num was mixed up because I didn't realize there 
                         // was a md# and mol_num and mol_num already was being used for md#
                         utils.getNextMolNum(selected_type);

                         break;

               }
          });

          $('#control-type-id-positive').on('change', function()
          {
               // Show the label for the version which is appropriate for the selected control
               $('.version-lot-label').hide();
               $('#label_'+$('#control-type-id-positive').val()).show();
               
          });
          
          $('#control-type-id-negative').on('change', function()
          {
               // Show the label for the version which is appropriate for the selected control
               $('.version-lot-label').hide();
               $('#label_'+$('#control-type-id-negative').val()).show();
          });

          // enable submit button once a change has occurred in type=text fields
          require(['views/events/activate_submit_button'], function(activate_submit_button)
          {
             
               activate_submit_button.StartEvents();
          });
     }

     function _showLastControlVersion()
     {
          var  ngs_panel_id = $('#ngs_panel_id').val(),
               control_type_id = '';

          // find which type of control is being used
          if ($('input[type=radio][name=sample_type]:checked').val() === 'Positive Control')
          {
               control_type_id = $('#control-type-id-positive').val();
          }
          else if ($('input[type=radio][name=sample_type]:checked').val() === 'Negative Control')
          {
               control_type_id = $('#control-type-id-negative').val();
          }

          if (ngs_panel_id !== '' && control_type_id !== '')
          {
               // use ajax to find last version used 
               $.ajax(
               {
                    type:'POST',
                    url: 'utils/control_last_version.php?',

                    dataType: 'text',
                    data: {
                              'control_type_id': control_type_id, 
                              'ngs_panel_id': ngs_panel_id
                    },
                    success: function(response)
                    {
                         // show last version used to user
                         if (JSON.parse(response) === undefined || JSON.parse(response).length == 0)
                         {
                              $('#last-version-used').empty().removeClass('alert alert-info');
                         }
                         else
                         {
                              var  last_ver_json  = JSON.parse(response)[0];
                                   last_ver_str   = '<b>Last used version</b>: '+last_ver_json['version'];

                                   last_ver_str+= ' <b>By</b>: '+last_ver_json['user_name'];
                                   last_ver_str+= ' <b>On</b>: '+last_ver_json['version_date'];

                                   $('#last-version-used').empty().html(last_ver_str).addClass('alert alert-info');
                         }                      
                    },
                    error: function(textStatus, errorThrown)
                    {
                         dialog_window('problem with ajax call ;( ' + errorThrown + ' ' + textStatus);
                    }
               });
          }
     }


     function _SetView()
     {

     }

     function _start(php_vars)
     {
          _SetView();
          _BindEvents(php_vars);
     }

     return {
          start:_start
     };
});
