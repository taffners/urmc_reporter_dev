define(['models/utils'], function(utils)
{
     function _BindEvents(php_vars)
     {
          $('#pool_download_excel_new_submit').on('click', function(e)
          {
              $('#pool_download_excel_refresh').show().removeClass('d-none').removeClass('hide');
              $('#pool_download_excel_new_submit').hide();

          });

          $('#pool_download_excel_refresh').on('click', function()
          {
              location.reload();
          });


          // toggle progress report bar
          require(['views/events/toggle_progress_bar'], function(toggle_progress_bar)
          {
               toggle_progress_bar.StartEvents();
          });

          // toggle excel workbook num input.  If the user needs to input an old excel workbook num then show the excel workbook num input field.
          // However, make them confirm that this is what they want to do.
          $('input[name="status_excel_workbook"]').on('change', function()
          {

               let  _selected = $(this).val();

               if (_selected === 'old')
               {
                    var  _popup_msg = '<div class="alert alert-danger" style="font-size:18px;"><strong>ARE YOU SURE YOU WOULD LIKE USE AN OLD OFA LIBRARY PREP EXCEL SHEET?  <br><br><span class="fas fa-exclamation-triangle"></span>THIS IS NOT ADVISABLE! <span class="fas fa-exclamation-triangle"></span></strong><br><br> The only reason why you should be reusing a Run OFA Library Prep Excel sheet is that this pool is being made because a sample failed the initial pool</div>',
                    _button_array =
                         {
                              cancel: function()
                              {
                                   _resetFieldsToDefault();   
                                   $('#ConfirmId').dialog('close');                                                                   
                              },
                              'yes': function()
                              {
                                   $('#excel-workbork-num-div').show();
                                   $('input[name="excel_workbook_num"]').val('');
                                   $('#ConfirmId').dialog('close'); 
                                   $('#pool_download_excel_ajax_submit').hide();
                                   $('#pool_download_excel_submit').show().removeClass('d-none');
                                   $('#pool_download_excel_new_submit').hide();


                              }
                         };
                    utils.confirmPopUp(_popup_msg, _button_array);     
               }
               else
               {
                    _resetFieldsToDefault();                                                                      
               }               
          });
     }

     function _resetFieldsToDefault()
     {
          // Hide the excel workbook num input and empty the field
          $('#excel-workbork-num-div').hide();
          $('input[name="excel_workbook_num"]').val('')

          $('#pool_download_excel_ajax_submit').show().removeClass('d-none');
          $('#pool_download_excel_submit').hide();

          // change variable back to new
          $('#status_excel_workbook_new').prop('checked', true);
          $('#status_excel_workbook_old').prop('checked', false);
          $('#pool_download_excel_new_submit').show().removeClass('d-none');          
     }

     function _SetView()
     {

     }

     function _start(_test_status, php_vars)
     {


          window.localStorage.removeItem('current_lib_tube_num');
          _SetView();
          _BindEvents(php_vars);
     }

     return {
          start:_start
     };
});
