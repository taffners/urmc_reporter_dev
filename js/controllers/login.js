define(['models/utils'],function(utils)
{
     function _BindEvents(php_vars)
     {    
          // if the browser is not chrome 
          utils.CheckBrowser();

     }

     function _SetView()
     {

     }

     function _start(php_vars)
     {
          _SetView();
          _BindEvents(php_vars);
     }

     return {
          start:_start
     };
});
