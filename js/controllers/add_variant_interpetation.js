define(['models/utils'], function(utils)
{
     function _BindEvents(php_vars)
     {
          // Add to tables with a class of dataTable the sorting ability from bootstraps datatables https://datatables.net/manual/styling/bootstrap
          require(['views/events/TableEvents'], function(TableEvents)
          {
               TableEvents.StartEvents();
          });

          // enable submit button once a change has occurred in type=text fields
          require(['views/events/activate_submit_button'], function(activate_submit_button)
          {
               activate_submit_button.StartEvents();
          });
     
          // // In the variant table cells larger than 20 characters are cut.  This will enable the more button and less button
          require(['views/events/toggle_teaser_complete_strs'], function(toggle_teaser_complete_strs)
          {
               toggle_teaser_complete_strs.StartEvents();
          });

          // toggle progress report bar
          require(['views/events/toggle_progress_bar'], function(toggle_progress_bar)
          {
               toggle_progress_bar.StartEvents();
          });

         // scroll to variants
          require(['views/events/scroll_events'], function(scroll_events)
          {
               scroll_events.StartEvents();
          });


          // add this interpetation to observed_variant_table as interpt_id
          $('.interp_table_row').on('click', function()
          {
               var  _alert_id = $(this).data('alert-id'),
                    _interpt_id = $(this).data('interpt-id'),
                    _interpt_text = $(this).data('interpt-text'),
                    _observed_variant_id = $(this).data('observed-variant-id');

               // add selected interpt text to alert
               $('#' + _alert_id).empty().text(_interpt_text);
               
               // get contents of td 
               var  previous_comment = $(this).data('interpt-text'),
                    knowledge_id = $(this).data('knowledge-id');

               // add previous comment to both local storage and the textarea   
               $('textarea#div_textarea_' + knowledge_id).empty().text(previous_comment);
               $('button#add_variant_interpetation_submit').removeClass('disabled-link');
               
               // perform ajax to update interpt_id in observed_variant_table
               $.ajax(
               {
                    type:'POST',
                    url: 'utils/change_interpt_id.php?',

                    dataType: 'text',
                    data: {'observed_variant_id': _observed_variant_id, 'interpt_id': _interpt_id},
                    success: function(response)
                    {


                    },
                    error: function(textStatus, errorThrown)
                    {
                         utils.dialog_window('problem with ajax call ;( ' + errorThrown + ' ' + textStatus, '#permissions-legend');
                    }
               });
          });

          // Hide variant interpretation.  DO NOT DELETE.  THIS WILL REMOVE VARIANTS 
          // INTERPTS FROM PREVIOUS REPORTS.  Just set comment_table.status to 'hide'
          $('.hide-variant').on('click', function()
          {
               var  comment_id = $(this).data('comment-id'),
                    _comment = $(this).data('interpt-text'),
                    _popup_msg = '<strong>ARE YOU SURE YOU WOULD LIKE TO DELETE THE FOLLOWING</strong><br><br>' + _comment,
                    _button_array =
                              {
                                   cancel: function()
                                   {
                                        $('#ConfirmId').dialog('close');

                                   },
                                   'yes': function()
                                   {
                                        // perform ajax to update interpt_id in observed_variant_table
                                        $.ajax(
                                        {
                                             type:'POST',
                                             url: 'utils/hide_comment.php?',

                                             dataType: 'text',
                                             data: {'comment_id': comment_id},
                                             success: function(response)
                                             {

                                                  $('#ConfirmId').dialog('close');
                                                  window.location.reload(true);
                                             },
                                             error: function(textStatus, errorThrown)
                                             {
                                                  utils.dialog_window('problem with ajax call ;( ' + errorThrown + ' ' + textStatus, '#permissions-legend');
                                             }
                                        });

                                   }
                              };
               utils.confirmPopUp(_popup_msg, _button_array);

               event.preventDefault();
          });

     }

     function _SetView()
     {

     }

     function _start(php_vars)
     {
          _SetView();
          _BindEvents(php_vars);
     }

     return {
          start:_start
     }
});