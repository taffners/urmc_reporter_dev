define(['models/utils'], function(utils)
{
     function _BindEvents(php_vars)
     {

          // toggle progress report bar
          require(['views/events/toggle_progress_bar'], function(toggle_progress_bar)
          {
               toggle_progress_bar.StartEvents();
          });

          // activate clicking on a variant row and scrolling to knowledge base
          $('.scroll_to_variant').on('click', function(event)
          {
            
               // get the data-scroll-to-raised-card and scroll to this div in the raised cards
               var  _scroll_id = $(this).data('scroll-to-raised-card'),
                    _id_exists = utils.scroll('#' + _scroll_id) // if true div exists otherwise search name checker
                    // if id does not exists search name checker
                    if (_id_exists === 'nope' )
                    {
                         utils.dialog_window('This variant is missing from the knowledge base.  Please notify Samantha about the missing data in the knowledge base.');
                    }
                    // if there are more than one of this variant alert user
                    else if (_id_exists === 'not unique')
                    {
                         utils.dialog_window('There are multiple entries for this variant in the knowledge base.  Please notify Samantha about the duplication in the knowledge base.');
                    }                                 
               
          });

          // activate scrolling to top of page
          $('.scroll_to_top_page').on('click', function()
          {             
               window.scrollTo(0,0);
          });

          $('input:radio.add_tier_to_variant').on('change', function()
          {    
               var  _tier = $(this).data('tier'),
                    _observed_variant_id = $(this).data('observed-variant-id'),
                    _run_id = $(this).data('run-id'),
                    _patient_id = $(this).data('patient-id'),
                    _visit_id = $(this).data('visit-id');

               if (_tier !== null)
               {
                    _report_link = '?page=generate_report&run_id='+_run_id+'&visit_id='+_visit_id+'&patient_id='+_patient_id+'&header_status=on';
               }
               else
               {
                    _report_link = '';
               }

               // change the tier value for the clicked observed variant
               $.ajax(
               {
                    type:'POST',
                    url: 'utils/change_variant_tier.php?',

                    dataType: 'text',
                    data: {
                              'observed_variant_id': _observed_variant_id, 
                              'tier': _tier,
                              'tier_id': $(this).data('tier-id'),
                              'user_id': $(this).data('user-id'),
                              'knowledge_id': $(this).data('knowledge-id'),
                              'tissue_id': $(this).data('tissue-id'),
                              'report_link':_report_link

                    },
                    success: function(response)
                    {

                         window.location.reload(true)

                    },
                    error: function(textStatus, errorThrown)
                    {
                         dialog_window('problem with ajax call ;( ' + errorThrown + ' ' + textStatus);
                    }
               });
          });


          $('.loader').fadeOut(500);
     }

     function _SetView()
     {

     }

     function _start(php_vars)
     {
          _SetView();
          _BindEvents(php_vars);
     }

     return {
          start:_start
     }
});