define(['models/utils'], function(utils)
{
     function _BindEvents(php_vars)
     {
          // Add to tables with a class of dataTable the sorting ability from bootstraps datatables https://datatables.net/manual/styling/bootstrap
          require(['views/events/TableEvents'], function(TableEvents)
          {
               TableEvents.StartEvents();
          });

          // enable submit button once a change has occurred in type=text fields
          require(['views/events/activate_submit_button'], function(activate_submit_button)
          {
               activate_submit_button.StartEvents();
          });

          // toggle progress report bar
          require(['views/events/toggle_progress_bar'], function(toggle_progress_bar)
          {
               toggle_progress_bar.StartEvents();
          });

          // enable date field calendar popups
          require(['views/events/date_fields'], function(date_fields)
          {
               date_fields.StartEvents();
          });

          // // In the variant table cells larger than 20 characters are cut.  This will enable the more button and less button
          require(['views/events/toggle_teaser_complete_strs'], function(toggle_teaser_complete_strs)
          {
               toggle_teaser_complete_strs.StartEvents();
          });

          // download csv templates
          $('#download-ofa-template').on('click', function()
          {
               var  _pool_chip_linker_id = $(this).data('pool_chip_linker_id'),
                    _chip_A = $('#chip_flow_cellA').val(),
                    _chip_B = $('#chip_flow_cellB').val(),

                    // Obtain from PHP how many chips there are in the library
                    _chip_num = 2;

               // make sure the both chip_A barcode and chip B barcode are not empty and are 19 characters long
               if   (
                         _chip_A === '' ||
                         _chip_A.length != 19 
                    )
               {
                    utils.dialog_window('Please make sure each chip A barcode is filled out ', 'chip_flow_cellA');
               }

               // if there's only one chip then it is not necessary to run this check
               else if   ( 
                              _chip_num === 2 &&
                              (                        
                                   _chip_B === '' ||
                                   _chip_B.length != 19
                              )
                         )
               {
                    utils.dialog_window('Please make sure each chip B barcode is filled out ', 'chip_flow_cellB');
               }

               else if (_chip_num === 2)
               {
                    var  _chip_A_barcode = _extractChipBarcode(_chip_A),_chip_B_barcode = _extractChipBarcode(_chip_B);

                    _download_ofa_run_template(_pool_chip_linker_id, 'A', _chip_A_barcode);
                    _download_ofa_run_template(_pool_chip_linker_id, 'B', _chip_B_barcode);
               }
               else
               {
                    var  _chip_A_barcode = _extractChipBarcode(_chip_A);
                    _download_ofa_run_template(_pool_chip_linker_id, 'A', _chip_A_barcode);
               }
          });

          // For some reason Bill and Huijie want an option to manually type data and not use a barcode scanner.  This is highly unadvisable therefore I'm going to nag the user when they push this button 
          $('input[type=radio][name=entry-type-toggle]').on('change', function()
          {
               var  entry_type = $(this).val(),
                    pattern;
               
               if (entry_type == 'human')
               {
                         pattern = '.{11}';

                         var  _popup_msg = '<strong>ARE YOU SURE YOU WOULD LIKE TO MANUALLY ENTER THE BARCODE?  THIS IS NOT ADVISABLE!  MANAGERS WILL BE AUTOMATICALLY NOTIFIED OF YOUR DECISION APON SUBMITING THIS FORM.</strong><br><br>',
                              _button_array =
                                   {
                                        cancel: function()
                                        {
                                             // Check barcode and do not change pattern
                                             $('#barcode-input-selection').prop('checked', true);
                                             $('#human-input-selection').prop('checked', false);
                                             $('#ConfirmId').dialog('close');
                                             pattern = '.{19}';

                                             _updateClassPattern('turn-off-enter-key', pattern);
                                        },
                                        'yes': function()
                                        {
                                             _updateClassPattern('turn-off-enter-key', pattern);
                                             $('#ConfirmId').dialog('close');
                                        }
                                   };
                         utils.confirmPopUp(_popup_msg, _button_array);
               }

               else
               {     
                    pattern = '.{19}';  
                    _updateClassPattern('turn-off-enter-key', pattern);
               }
          });
     }

     function _updateClassPattern(update_class, pattern)
     {
          $('.'+update_class).each(function()
          {
               $(this).attr('pattern', pattern);
          });
     }

     function _extractChipBarcode(scanned_barcode)
     {
          // Thermofisher places barcodes on the chips when scanned they contain 19 digits however, the torrent server extracts a subset of the 19 digit string and calls it the chipBarcode.  This function will take a 19 digit string and extract 11 digits from [1:12]
          // Example
          // (21DAEJ00624241530v1) -> DAEJ0062424

          return scanned_barcode.substring(2,12)
     }

     function _download_ofa_run_template(pool_chip_linker_id, chip, barcode)
     {
          $.ajax(
          {
               type:'POST',
               url: 'utils/download_ofa_run_template.php?',

               dataType: 'text',
               data: {
                         'chip': chip, 
                         'pool_chip_linker_id': pool_chip_linker_id
               },
               success: function(response)
               {
                    // csv_response is a string formated like a csv file.
                    // use window.open and encodeURI functions to download the CSV file
                    var  csv_json = JSON.parse(response),
                         csv_content = "data:text/csv;charset=utf-8,",
                         chip = csv_json['chip'],
                         pool_chip_linker_id = csv_json['pool_chip_linker_id'];

                    $.each(csv_json, function(i, row)
                    {
                         
                         if (i !== 'chip' && i !== 'pool_chip_linker_id')
                         {
                              csv_content += row + "\r\n";
                         }
                    }); 

                    var  encodedUri = encodeURI(csv_content),
                         link = document.createElement("a"),
                         today = utils.GetTodaysDateYYMMDD();

                    link.setAttribute("href", encodedUri);
                    link.setAttribute("download", today+'_'+barcode+'_'+chip+'_'+pool_chip_linker_id+'.csv');
                    document.body.appendChild(link); // Required for FF

                    link.click(); // This will download the data file named "my_data.csv".
               },
               error: function(textStatus, errorThrown)
               {
                    dialog_window('problem with ajax call ;( ' + errorThrown + ' ' + textStatus);
               }
          });
     }


     function _SetView()
     {

     }

     function _start(php_vars)
     {
          _SetView();
          _BindEvents(php_vars);
     }

     return {
          start:_start
     };
});
