define(['models/utils'], function(utils)
{
     function _BindEvents(php_vars)
     {
          // bind date picker
          $('.date-picker').datepicker({
               changeMonth: true,
               changeYear: true
          });
     }

     function _SetView()
     {

     }

     function _start(php_vars)
     {
          _SetView();
          _BindEvents(php_vars);
     }

     return {
          start:_start
     }
});