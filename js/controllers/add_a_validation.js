define(['models/utils'],function(utils)
{
     function _BindEvents(php_vars)
     {
          var  browser_version = utils.get_browser();

          $('#browser_version').val(browser_version['name'] + ' ' + browser_version['version']);        

     }


     function _SetView()
     {

     }

     function _start(php_vars)
     {
          _SetView();
          _BindEvents(php_vars);
     }

     return {
          start:_start
     };
});
