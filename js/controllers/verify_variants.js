define(['models/utils'], function(utils)
{
     function _BindEvents(php_vars)
     {
          // Add to tables with a class of dataTable the sorting ability from bootstraps datatables https://datatables.net/manual/styling/bootstrap
          require(['views/events/TableEvents'], function(TableEvents)
          {
               TableEvents.StartEvents();
          });
     
          // // In the variant table cells larger than 20 characters are cut.  This will enable the more button and less button
          require(['views/events/toggle_teaser_complete_strs'], function(toggle_teaser_complete_strs)
          {
               toggle_teaser_complete_strs.StartEvents();
          });

          // // on click of a check box add to database
          $('td > input:checkbox').on('change', function()
          {             
               // Purpose: Every time a value is checked in the admin permissions update value in database.  Id is set up likeadd id for each td is input_userRoleID_userID_role_ID.

               var  _observed_variant_id = $(this)
                         .attr('id')
                         .replace('toggle_include_variant_', ''),
                    _db_task = $(this).is(":checked") ? 'add': 'delete';

               //change value in database
               $.ajax(
               {
                    type:'POST',
                    url: 'utils/change_include_variant_status.php?',

                    dataType: 'text',
                    data: {'observed_variant_id': _observed_variant_id, 'db_task': _db_task},
                    success: function(response)
                    {
                         window.location.reload(true)
                    },
                    error: function(textStatus, errorThrown)
                    {
                         dialog_window('problem with ajax call ;( ' + errorThrown + ' ' + textStatus);
                    }
               });
          });

          // change classification 
          require(['views/events/change_classification'], function(change_classification)
          {
               change_classification.StartEvents();
          });

          // toggle progress report bar
          require(['views/events/toggle_progress_bar'], function(toggle_progress_bar)
          {
               toggle_progress_bar.StartEvents();
          });

          // toggle variant_comment_modal
          require(['views/events/modals/activate_variant_modal'], function(activate_variant_modal)
          {
               activate_variant_modal.StartEvents();
          });

          // scroll to variants
          require(['views/events/scroll_events'], function(scroll_events)
          {
               scroll_events.StartEvents();
          });


          // Add flow chart
          google.charts.load('current', {packages:["orgchart"]});

          // only draw chart of id to add chart is present
          if ($('#chart_div').length != 0)
          {
               google.charts.setOnLoadCallback(drawChart);

               function drawChart() 
               {
                    var data = new google.visualization.DataTable();
                    data.addColumn('string', 'Name');
                    data.addColumn('string', 'Manager');

                    // For each orgchart box, provide the name, manager, and tooltip to show.
                    data.addRows(
                    [
                         ['QC Complete', ''],
                         ['Not Director','QC Complete'],
                         ['Director','QC Complete'],
                         ['All Variants uploaded are in Knowledge base', 'Director'],
                         ['Access Denied', 'Not Director'],
                         ['Variants are present not in Knowledge base', 'Director'],
                         ['Access Granted', 'All Variants uploaded are in Knowledge base'],
                         ['Variants not in knowledge base are NOT classified Benign or Seq Error', 'Variants are present not in Knowledge base'],
                         ['Variants not in knowledge base are classified Benign or Seq Error', 'Variants are present not in Knowledge base'],
                         ['Access Denied ', 'Variants not in knowledge base are NOT classified Benign or Seq Error'],
                         ['Access Granted ', 'Variants not in knowledge base are classified Benign or Seq Error']
                    ]);

                    data.setRowProperty(4, 'style', 'border: 5px solid red');
                    data.setRowProperty(6, 'style', 'border: 5px solid green');
                    data.setRowProperty(9, 'style', 'border: 5px solid red');
                    data.setRowProperty(10, 'style', 'border: 5px solid green');
                    // Create the chart.
                    var chart = new google.visualization.OrgChart(document.getElementById('chart_div'));
                    // Draw the chart, setting the allowHtml option to true for the tooltips.
                    chart.draw(data, {allowHtml:true});

                    $('.google-visualization-orgchart-table').css('border-collapse', 'separate');
               } 
          }

         


     }

     function _SetView()
     {

     }

     function _start(php_vars)
     {
          _SetView();
          _BindEvents(php_vars);
     }

     return {
          start:_start
     }
});