define(['models/utils','models/google_charts'],function(utils, google_charts)
{
     function _BindEvents(php_vars)
     {

          // activate the links to the ngs home page
          require(['views/events/scroll_events'], function(scroll_events)
          {
               scroll_events.StartEvents();
          });




          
          $.ajax(
          {
               type:'POST',
               url: 'utils/new_home.php?',

               dataType: 'text',
               data: {
                         'query_filter': 'pending-counts'
               },
               success: function(response)
               {
                    if (response ==='Post Does not contain query_filter')
                    {

                    }
                    else
                    {
                         var  json_response = JSON.parse(response),
                              chart_data = [['Test','count']],
                              chart_height = 340,
                              curr_test,
                              div_id = 'all-pending-counts-pie-chart';
                             
                         // Get all the possible curr_status
                         json_response.forEach(function(val, i)
                         {
 
                              curr_test = [val['test_name'], parseInt(val['count'])];
                              chart_data.push(curr_test);
                         });   

                         $('#'+div_id).height(chart_height);    

                         google_charts.drawPieChart(div_id, 'Pending Test Counts', chart_data,chart_height);
                    }

               },
               error: function(textStatus, errorThrown)
               {
                    utils.dialog_window('problem with ajax call ;( ' + errorThrown + ' ' + textStatus);
               }
          });

          $.ajax(
          {
               type:'POST',
               url: 'utils/new_home.php?',

               dataType: 'text',
               data: {
                         'query_filter': 'in-progress-test-counts'
               },
               success: function(response)
               {
                    if (response ==='Post Does not contain query_filter')
                    {

                    }
                    else
                    {
                         var  json_response = JSON.parse(response),
                              chart_data = [['Test','count']],
                              chart_height = 500,
                              curr_test,
                              div_id = 'all-in-progress-counts-pie-chart';
                             
                         // Get all the possible curr_status
                         json_response.forEach(function(val, i)
                         {
 
                              curr_test = [val['test_name'], parseInt(val['count'])];
                              chart_data.push(curr_test);
                         });   

                         $('#'+div_id).height(chart_height);   

                         google_charts.drawPieChart(div_id, 'In Progress Test Counts', chart_data);
                    }

               },
               error: function(textStatus, errorThrown)
               {
                    utils.dialog_window('problem with ajax call ;( ' + errorThrown + ' ' + textStatus);
               }
          });

          
     }

     function _SetView()
     {

     }

     function _start(test_status, php_vars)
     {
          _SetView();
          _BindEvents(php_vars);

     }

     return {
          start:_start
     };
});
