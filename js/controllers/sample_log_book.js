define(['models/utils', 'models/tour'],function(utils, tour)
{
     function _BindEvents(php_vars)
     {
          // Add to tables with a class of dataTable the sorting ability from bootstraps datatables https://datatables.net/manual/styling/bootstrap
          require(['views/events/TableEvents'], function(TableEvents)
          {
               TableEvents.StartEvents(true);
          });

          // // In the variant table cells larger than 20 characters are cut.  This will enable the more button and less button
          require(['views/events/toggle_teaser_complete_strs'], function(toggle_teaser_complete_strs)
          {
               toggle_teaser_complete_strs.StartEvents();
          });

          $('#take-a-tour-btn').removeClass('d-none').on('click', function()
          {
               $('#ngs-dropdown').addClass('open');

               let  color_keys = tour.color_keys(),
                    i,
                    table_info = tour.table_info(),
                    sample_log_book_info = tour.sample_log_book_info(),
                    tour_data = [],
                    tour_data_len = tour_data.length;

               tour_data = utils.mergeArrays(tour_data,color_keys,tour_data_len);
               tour_data_len = tour_data.length;

               tour_data = utils.mergeArrays(tour_data,table_info,tour_data_len);
               
               tour_data_len = tour_data.length;

               tour_data = utils.mergeArrays(tour_data,sample_log_book_info,tour_data_len);
               

               tour.start_tour(tour_data, '');
          });
     }

     function _SetView()
     {

     }

     function _start(php_vars)
     {
          _SetView();
          _BindEvents(php_vars);
     }

     return {
          start:_start
     };
});
