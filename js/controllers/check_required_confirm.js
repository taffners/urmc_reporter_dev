define(['models/utils'], function(utils)
{
     function _BindEvents(php_vars)
     {

          $('#check_required_confirm_submit').removeClass('disabled-link');

          // add a hidden field for chosen choose_snp_or_indel
          $('.choose_snp_or_indel').on('change',function()
          {
               var  selected = $(this).val(),
                    observed_variant_id = $(this).data('observed_variant_id'),
                    selected_html = '<input type="hidden" name="update_snp_or_indel['+observed_variant_id+']" value="'+selected+'" id="input_observed_id_'+observed_variant_id+'">',
                    drop_down_count = parseInt($('#drop_down_count').val());

               // remove any previous selection
               $('#input_observed_id_'+observed_variant_id).remove();

               // add current selection as a hidden field
               $('#div-snp-or-indel').append(selected_html);

               // if the drop_down_count is the same number of inputs in div-snp-or-indel show nav buttons
               var  input_count = $('#div-snp-or-indel > input').length;

               if (drop_down_count === input_count)
               {                
                    $('#nav-buttons').removeClass('hide').removeClass('d-none').show();

                    $('#replace-nav-buttons').hide();
               }
          });
          
          // toggle progress report bar
          require(['views/events/toggle_progress_bar'], function(toggle_progress_bar)
          {
               toggle_progress_bar.StartEvents();
          });

     }

     function handle_hidden_values(str_group_num, clicked, values_for_hiddens, confirm_status)
     {
          // (str, str (radio, refresh), json, str) -> adds to div
          // Purpose: Add new hidden values if a radio was clicked and confirm status is pending.
          // delete any previous hidden fields for the str_group_num
          // if a refresh button was clicked clear any previous radio buttons clicked.  There could be a bug here since it clears all previously clicked radio buttons.  However, it will not delete any hidden fields so it should be fine.

          var  _value_keys = Object.keys(values_for_hiddens)

          for (_i = 0; _i < _value_keys.length; _i++)
          {               
               _group_key = str_group_num + '_' + _value_keys[_i]

               // remove any previous value
               $('#' + _group_key).remove();

               if (clicked === 'radio' && confirm_status === 'pending')
               {
                    _input = '<input type="text" class="hide" id="' + _group_key + '" name="' + str_group_num + '[' + _value_keys[_i] + ']" value="' + values_for_hiddens[_value_keys[_i]] + '">';
               
                    // add a new hidden value                                  
                    $('div#hidden_confirm_input_div').append(_input);
               }     
          }

          // uncheck radio buttons for refresh btn
          if (clicked === 'refresh')
          {              
               $("input[type='radio']").each(function()
               {
                    if ($(this).is(':checked'))
                    {
                         $(this).prop('checked', false);
                    }
               });  
          }          
     }


     function _SetView()
     {

     }

     function _start(php_vars)
     {
          _SetView();
          _BindEvents(php_vars);
     }

     return {
          start:_start
     }
});