define(function()
{
     function _BindEvents(php_vars)
     {
          // Add to tables with a class of dataTable the sorting ability from bootstraps datatables https://datatables.net/manual/styling/bootstrap
          require(['views/events/TableEvents'], function(TableEvents)
          {
               TableEvents.StartEvents();
          });

          // change permissions on account
          $('.toggle-on-off-status').on('click', function()
          {
               var  table = $(this).data('table'),
                    value = $(this).data('value'),
                    curr_status = $(this).data('curr-status'),
                    id = $(this).data('id');

               $.ajax(
               {
                    type:'POST',
                    url: 'utils/update_status_on_off.php?',

                    dataType: 'text',
                    data: {
                              'table': table,
                              'value': value,
                              'curr_status':curr_status,
                              'id':id 

                    },
                    success: function(response)
                    {

                         window.location.reload(true)

                    },
                    error: function(textStatus, errorThrown)
                    {
                         dialog_window('problem with ajax call ;( ' + errorThrown + ' ' + textStatus);
                    }
               });

          });




     }

     function _SetView()
     {

     }

     function _start(php_vars)
     {
          _SetView();
          _BindEvents(php_vars);
     }

     return {
          start:_start
     };
});
