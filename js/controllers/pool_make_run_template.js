define(['models/utils'], function(utils)
{
     function _BindEvents(php_vars)
     {
          // Add to tables with a class of dataTable the sorting ability from bootstraps datatables https://datatables.net/manual/styling/bootstrap
          require(['views/events/TableEvents'], function(TableEvents)
          {
               TableEvents.StartEvents();
          });

          // enable submit button once a change has occurred in type=text fields
          require(['views/events/activate_submit_button'], function(activate_submit_button)
          {
               activate_submit_button.StartEvents();
          });

          // toggle progress report bar
          require(['views/events/toggle_progress_bar'], function(toggle_progress_bar)
          {
               toggle_progress_bar.StartEvents();
          });

          // enable date field calendar popups
          require(['views/events/date_fields'], function(date_fields)
          {
               date_fields.StartEvents();
          });

          // // In the variant table cells larger than 20 characters are cut.  This will enable the more button and less button
          require(['views/events/toggle_teaser_complete_strs'], function(toggle_teaser_complete_strs)
          {
               toggle_teaser_complete_strs.StartEvents();
          });

          // download csv templates
          $('#make_run_template').on('click', function()
          {
               var  _chip = $(this).data('chip'),
                    _pool_chip_linker_id = $(this).data('pool_chip_linker_id'),
                    _reagent_cartidge = $('#reagent_cartidge').val(),
                    _chip_flow_cell = $('#chip_flow_cell').val();

               // check that reagent_cartidge and chip_flow_cell are not empty  
               if (_chip_flow_cell === '')
               {
                    utils.dialog_window('Please provide the Flow Cell ID #.');
               }
               else if (_reagent_cartidge === '')
               {
                    utils.dialog_window('Please provide the Reagent Cartridge Lot #.');
               }

               // use FTP to transfer to miseq computer
               // https://shellcreeper.com/move-files-server-to-server-using-simple-php/

               ///////////////////////////////////////////////////////////////////////
               // Make a csv to download
               ///////////////////////////////////////////////////////////////////////
               $.ajax(
               {
                    type:'POST',
                    url: 'utils/download_myeloid_run_template.php?',

                    dataType: 'text',
                    data: { 
                              'pool_chip_linker_id': _pool_chip_linker_id,
                              'reagent_cartidge': _reagent_cartidge,
                              'chip_flow_cell':_chip_flow_cell
                    },
                    success: function(response)
                    {

                         // csv_response is a string formated like a csv file.
                         // use window.open and encodeURI functions to download the CSV file
                         var  csv_json = JSON.parse(response),
                              csv_content = "data:text/csv;charset=utf-8,",
                              reagent_cartidge = csv_json['reagent_cartidge'],
                              pool_chip_linker_id = csv_json['pool_chip_linker_id'];

                         $.each(csv_json, function(i, row)
                         {
                              
                              if (i !== 'reagent_cartidge' && i !== 'pool_chip_linker_id')
                              {
                                   csv_content += row + "\r\n";
                              }
                         }); 

                         var  encodedUri = encodeURI(csv_content),
                              link = document.createElement("a"),
                              today = utils.GetTodaysDate().replace('/', '_');

                         link.setAttribute("href", encodedUri);
                         link.setAttribute("download", reagent_cartidge+'.csv');
                         document.body.appendChild(link); // Required for FF

                         link.click(); // This will download the data file named "my_data.csv".
                    },
                    error: function(textStatus, errorThrown)
                    {
                         dialog_window('problem with ajax call ;( ' + errorThrown + ' ' + textStatus);
                    }
               });
          });

                    // download csv templates
          $('#make_run_template_updated').on('click', function()
          {
               var  _chip = $(this).data('chip'),
                    _pool_chip_linker_id = $(this).data('pool_chip_linker_id'),
                    _reagent_cartidge = $('#reagent_cartidge').val(),
                    _chip_flow_cell = $('#chip_flow_cell').val();

               // check that reagent_cartidge and chip_flow_cell are not empty  
               if (_chip_flow_cell === '')
               {
                    utils.dialog_window('Please provide the Flow Cell ID #.');
               }
               else if (_reagent_cartidge === '')
               {
                    utils.dialog_window('Please provide the Reagent Cartridge Lot #.');
               }

               // use FTP to transfer to miseq computer
               // https://shellcreeper.com/move-files-server-to-server-using-simple-php/

               ///////////////////////////////////////////////////////////////////////
               // Make a csv to download
               ///////////////////////////////////////////////////////////////////////
               $.ajax(
               {
                    type:'POST',
                    url: 'utils/download_myeloid_run_template_updated.php?',

                    dataType: 'text',
                    data: { 
                              'pool_chip_linker_id': _pool_chip_linker_id,
                              'reagent_cartidge': _reagent_cartidge,
                              'chip_flow_cell':_chip_flow_cell
                    },
                    success: function(response)
                    {

                         // csv_response is a string formated like a csv file.
                         // use window.open and encodeURI functions to download the CSV file
                         var  csv_json = JSON.parse(response),
                              csv_content = "data:text/csv;charset=utf-8,",
                              reagent_cartidge = csv_json['reagent_cartidge'],
                              pool_chip_linker_id = csv_json['pool_chip_linker_id'];

                         $.each(csv_json, function(i, row)
                         {
                              
                              if (i !== 'reagent_cartidge' && i !== 'pool_chip_linker_id')
                              {
                                   csv_content += row + "\r\n";
                              }
                         }); 

                         var  encodedUri = encodeURI(csv_content),
                              link = document.createElement("a"),
                              today = utils.GetTodaysDate().replace('/', '_');

                         link.setAttribute("href", encodedUri);
                         link.setAttribute("download", reagent_cartidge+'.csv');
                         document.body.appendChild(link); // Required for FF

                         link.click(); // This will download the data file named "my_data.csv".
                    },
                    error: function(textStatus, errorThrown)
                    {
                         dialog_window('problem with ajax call ;( ' + errorThrown + ' ' + textStatus);
                    }
               });
          });




     }

     function _SetView()
     {

     }

     function _start(php_vars)
     {
          _SetView();
          _BindEvents(php_vars);
     }

     return {
          start:_start
     };
});
