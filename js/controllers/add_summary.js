define(['models/utils'], function(utils)
{
     function _BindEvents(php_vars)
     {

          // toggle progress report bar
          require(['views/events/toggle_progress_bar'], function(toggle_progress_bar)
          {
               toggle_progress_bar.StartEvents();
          });

          // enable submit button once a change has occurred in type=text fields
          require(['views/events/activate_submit_button'], function(activate_submit_button)
          {
               activate_submit_button.StartEvents();
          });

          $('li.add-summary').on('click', function()
          {
               var  summary = $(this).data('summary');

               $('div > textarea#summary_textarea').val(summary);
               $('button[type="submit"]').removeClass('disabled-link'); 
          });

     }

     function _SetView()
     {

     }

     function _start(php_vars)
     {
          _SetView();
          _BindEvents(php_vars);
     }

     return {
          start:_start
     }
});