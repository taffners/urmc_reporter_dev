define(['models/utils'], function(utils)
{
     function _BindEvents(php_vars)
     {
          // Add to tables with a class of dataTable the sorting ability from bootstraps datatables https://datatables.net/manual/styling/bootstrap
          require(['views/events/TableEvents'], function(TableEvents)
          {
               TableEvents.StartEvents();
          });

     
          // // In the variant table cells larger than 20 characters are cut.  This will enable the more button and less button
          require(['views/events/toggle_teaser_complete_strs'], function(toggle_teaser_complete_strs)
          {
               toggle_teaser_complete_strs.StartEvents();
          });

          // activate scrolling to top of page
          $('.scroll_to_top_page').on('click', function()
          {             
               window.scrollTo(0,0);
          });

          // after search table loads rows which were included in a report have the class hover_success_on if clicked show pdf from this report.
          $('.hover_success_on').on('click', function()
          {
               var  _run_id = $(this).data('run-id'),
                    _patient_id = $(this).data('patient-id'),
                    _visit_id = $(this).data('visit-id');
            
               window.open('?page=generate_report&header_status=on&run_id=' + _run_id + '&patient_id=' + _patient_id + '&visit_id=' + _visit_id, '_blank');
          });
     }

     function _SetView()
     {

     }

     function _start(php_vars)
     {
          _SetView();
          _BindEvents(php_vars);
     }

     return {
          start:_start
     }
});