define(['models/utils','models/google_charts'],function(utils, google_charts)
{
     function _BindEvents(php_vars)
     {
          // add Number of tests
          $.ajax(
          {
               type:'POST',
               url: 'utils/audits.php?',

               dataType: 'text',
               data: {
                         'query_filter': 'cummulative_test_counts',
                         'filter':'',
                         'month':php_vars['month'] != undefined ? php_vars['month'] : '',
                         'year':php_vars['year'] != undefined ? php_vars['year'] : ''
               },
               success: function(response)
               {
                    if (response ==='Post Does not contain query_filter')
                    {

                    }
                    else
                    {
                         var  json_response = JSON.parse(response),
                              chart_data = [['Month','Number of tests']],
                              chart_height = 1000,
                              
                              curr_month,
                              div_id = 'monthly-count-graphs';
                    
                         // Get all the possible curr_status
                         json_response.forEach(function(val, i)
                         {
                                 
                              curr_month = [val['month']+' '+val['year'], parseInt(val['num_tests'])];
                              chart_data.push(curr_month);
                         });   

                         $('#'+div_id+'-chart').height(700);  

                         google_charts.drawLineGraph(div_id+'-chart', 'Number of Tests Completed Each Month', chart_data, '', 'vertical', 'Number of Tests');

                         // google_charts.drawTable(div_id+'-table', 'Number of Tests Completed Each Month', chart_data, '', 'vertical', 'Number of Tests');
                    }

               },
               error: function(textStatus, errorThrown)
               {
                    utils.dialog_window('problem with ajax call ;( ' + errorThrown + ' ' + textStatus);
               }
          });

          // number of samples
          // $.ajax(
          // {
          //      type:'POST',
          //      url: 'utils/audits.php?',

          //      dataType: 'text',
          //      data: {
          //                'query_filter': 'cummulative_test_counts',
          //                'filter':'',
          //                'month':php_vars['month'] != undefined ? php_vars['month'] : '',
          //                'year':php_vars['year'] != undefined ? php_vars['year'] : ''
          //      },
          //      success: function(response)
          //      {
          //           if (response ==='Post Does not contain query_filter')
          //           {

          //           }
          //           else
          //           {
          //                var  json_response = JSON.parse(response),
          //                     chart_data = [['Month','Number of tests']],
          //                     chart_height = 500,
                              
          //                     curr_month,
          //                     div_id = 'monthly-count-graphs';
                    
          //                // Get all the possible curr_status
          //                json_response.forEach(function(val, i)
          //                {
                                 
          //                     curr_month = [val['month']+' '+val['year'], parseInt(val['num_tests'])];
          //                     chart_data.push(curr_month);
          //                });   

          //                $('#'+div_id).height(1000);  
          //                $('#'+div_id).width(2000);  

          //                google_charts.drawLineGraph(div_id, 'Number of Tests Completed Each Month', chart_data, '', 'vertical', 'Number of Tests');
          //           }

          //      },
          //      error: function(textStatus, errorThrown)
          //      {
          //           utils.dialog_window('problem with ajax call ;( ' + errorThrown + ' ' + textStatus);
          //      }
          // });
                 
     }

     function _SetView()
     {

     }

     function _start(test_status, php_vars)
     {
          _SetView();
          _BindEvents(php_vars);

     }

     return {
          start:_start
     };
});
