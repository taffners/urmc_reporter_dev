define(['models/utils'],function(utils)
{
     let  site_title = 'Molecular Diagnostics Infotrack';

     function _start_tour(tour_data, page_purpose)
     {
          //Purpose: make a tour using bootstrap tour

          // add an introduction to the tour
          // tour_data.unshift({
          //                backdrop: true,
          //                orphan: true,
          //                content: 'Welcome to the tour for <b>'+site_title+'</b>.  This tour is to orient you to the current page.  <br><br>' + page_purpose,
                         
          //           });


          let  tour = new Tour(
               {
                    name: 'tourist',
                    steps:tour_data,
                    framework: 'bootstrap4',
                    debug: true
                    // debug:true
               });

          tour.restart();

          // Start the tour
          tour.start();

          tour.restart();
     }

     function _color_keys()
     {
          let  color_keys = [
               {
                    backdrop: true,
                    orphan: true,
                    content: '<h3 style="text-align: center;"><b>Color Keys</b></h3> Colors have meaning in ' +site_title+'.  Refer to these keys to familiarize you with the color meanings. <br><img src="images/color_scheme_key.png" width="400px">'
               },
               {
                    backdrop: true,
                    orphan: true,
                    content: '<h3 style="text-align: center;"><b>Sample log book color code</b></h3><img src="images/sample_log_book_colors.png" width="200px">'
               }];
          return color_keys
     }

     function _table_info()
     {
          let = data_table_info =[
          {
               element: '#DataTables_Table_0_filter',
               placement: 'left',
               backdrop: true,
               content: '',
               duration:1000
          },
          {
               element: '#DataTables_Table_0_filter',
               placement: 'left',
               backdrop: true,
               content: '<h3 style="text-align: center;"><b>General Table Info</b></h3>Most tables provided in ' + site_title + ' can be searched using a search box which will be in the upper right hand corner like this.'
          },
          {
               element: '#DataTables_Table_0 > thead',
               placement: 'top',
               content: '<h3 style="text-align: center;"><b>General Table Info</b></h3>Tables can be sorted by each columns by clicking on the <span class="fas fa-sort"></span> located in the header of most tables.'
          },
          {
               backdrop: true,
               orphan: true,
               content: '<h3 style="text-align: center;"><b>Hover Green Highlight Rows</b></h3>Some tables when you hover over the row it turns green.  This indicates that the row can be clicked and more info will appear.'               
          },
          
          {
               element: '#DataTables_Table_0_info',
               placement: 'right',
               backdrop: true,
               content: '',
               duration:1000
          },
          {
               element: '#DataTables_Table_0_info',
               placement: 'right',
               backdrop: true,
               content: '<h3 style="text-align: center;"><b>General Table Info</b></h3>Some tables will only show a limited amount of entries and they are called pagination tables.  The lower left hand corner of pagination tables will show more information on the total number of rows and what is currently showing.'
          },
          {
               element: '#DataTables_Table_0_length',
               placement: 'right',
               backdrop: true,
               content: '<h3 style="text-align: center;"><b>General Table Info</b></h3>You can alter the number of entries shown in Pagination tables by using the drop down menu in the upper left hand corner like this.'
          },               
          {
               element: '#DataTables_Table_0_paginate',
               placement: 'left',
               backdrop: true,
               content: '<h3 style="text-align: center;"><b>General Table Info</b></h3>You can see the next set of samples by using these buttons at the lower right hand part of the table.'
          }];
          return data_table_info;
     }

     function _sample_log_book_info()
     {
          let  sample_log_book_info =[
                              
               {
          
                    element: '#sample-log-book-table',
                    placement: 'top',
                    backdrop: true,
                    content: '<h3 style="text-align: center;"><b>Sample Log Book</b></h3>The sample log book is represented by this table that looks like Notebook paper.'
          
               },
               {
          
                     backdrop: true,
                    orphan: true,
                    content: '<h3 style="text-align: center;"><b>History of Sample Log Book</b></h3>Historically the Molecular Diagnostics lab used a paper notebook as their sample log book.  This electronic notebook replaced the physical notebook on May 31st, 2019.  All samples before this date will not be found in this notebook and you will have to refer back to the paper notebook.  '
          
               },
               {
          
                    backdrop: true,
                    orphan: true,
                    content: '<h3 style="text-align: center;"><b>Sample Log Book</b></h3>There are two types of objects represented in the sample log book. <ul> <li><b>Samples</b></b> <ul> <li>Shown in Rows</li><li>Added by add sample to log book</li><li>Updated by clicking <a href="#" class="btn btn-primary btn-primary-hover"> <img src="images/sample_log_book_edit.png" style="margin-left:5px;height:45px;width:45px;"> </a></li></ul> </li><li><b>Tests</b></b> <ul> <li>Shown in Summary tests Added column</li><li>Added to a sample by clicking add test, in the sample log book</li><li>Updated by clicking <a href="#" class="btn btn-primary btn-primary-hover"> Edit/Update<br>Print/ Cancel<br>Test</a></li></ul> </li></ul>'
                    // <ul>
                    //      <li><b>Samples</b></b>
                    //           <ul>
                    //                <li>Shown in Rows</li>
                    //                <li>Added by add sample to log book</li>
                    //                <li>Updated by clicking <a href="#" class="btn btn-primary btn-primary-hover"> 
                    //                     <img src="images/sample_log_book_edit.png" style="margin-left:5px;height:45px;width:45px;">
                    //                </a></li>
                    //           </ul>
                    //      </li>
                    //      <li><b>Tests</b></b>
                    //           <ul>
                    //                <li>Shown in Summary tests Added column</li>
                    //                <li>Added to a sample by clicking add test, in the sample log book</li>
                    //                <li>Updated by clicking <a href="#" class="btn btn-primary btn-primary-hover"> Edit/Update<br>Print/ Cancel<br>Test</a></li>
                    //           </ul>
                    //      </li>
                    // </ul>
                     
          
               },
               {
          
                    element: '#comments-th',
                    placement: 'top',

                    content: '<h3 style="text-align: center;"><b>Comments</b></h3>All comments added under the sample but not the test are shown here.  This makes all sample level comments searchable.'          
               },
               {
          
                    element: '#summary-tests-added-th',
                    placement: 'top',
                    content: '<h3 style="text-align: center;"><b>Tests Added</b></h3>All tests are shown in the Summary Tests added Column.  Progress of test is indicated by the color dot next to the test.  Hover over the color dot for more information on what it represents. '
          
               },
              
               {         
                    element: '#edit-update-ordered-tests-th',
                    placement: 'top',
                    
                    content: '<h3 style="text-align: center;"><b>Everything tests</b></h3>This column provides access to everything related to tests. Such as:<ul> <li>Edit information about an ordered test</li><li>Update the status of the test</li><li>Print ordered test sheets</li><li>Cancel tests</li><li>Add new tests to the sample</li><li>Also the entire page can be printed as into a sample trace report</li></ul>'
                    
          
               },
               {
          
                    element: '#add-test-th',
                    placement: 'top',

                    content: '<h3 style="text-align: center;"><b>Add a test</b></h3>Add a test to this sample.  Make sure DNA extracts and Nanodrop is added as a test too.'
          
               },
               {
          
                    element: '#edit-sample-th',
                    placement: 'top',

                    content: '<h3 style="text-align: center;"><b>Edit Sample</b></h3>Click on the <a href="#" class="btn btn-primary btn-primary-hover"> <img src="images/sample_log_book_edit.png" style="margin-left:5px;height:45px;width:45px;"> </a> button to edit all of the information about the sample.'
          
               },
               
               {
          
                    element: '#add-sample-to-log-book-btn',
                    placement: 'bottom',

                    content: '<h3 style="text-align: center;"><b></b></h3>To add a sample to the log book either click this button or use the tool bar log book -> Add Sample to Log Book.'
          
               },

               {
          
                    backdrop: true,
                    orphan: true,

                    content: '<h3 style="text-align: center;"><b>Searching Sample Log Book</b></h3>There are 4 ways to search the sample log book.  <ul><li>Search all samples</li><li>Show all samples with pending tests</li><li>Show all samples with in progress tests</li><li>Filter currently shown samples</li></ul>'
               },
               {          
                    element: '#search-all-samples-div',
                    placement: 'left',
                    backdrop: true,
                    content: '<h3 style="text-align: center;"><b>Search all samples</b></h3>The database of all samples can be searched from this search box.  It can also be searched based on date the sample arrived in the lab.'
               },
              
               {
               
                    element: '#pending-tests-btn',
                    placement: 'bottom',

                    content: '<h3 style="text-align: center;"><b>Show all samples with pending tests</b></h3>When clicking on this button any sample that has at least one pending test will appear in the sample log book.  This list can be further filtered down by using the filter described in the next lesson. A green pending test dot will notify you that which tests for each sample are pending.'
               
               },
               {
               
                    element: '#tests-in-progress',
                    placement: 'bottom',

                    content: '<h3 style="text-align: center;"><b>Show all samples with In progress tests</b></h3>When clicking on this button any sample that has at least one in progress test will appear in the sample log book.  This list can be further filtered down by using the filter described in the next lesson. A green/yellow in progress test dot will notify you that which tests for each sample are in progress.'
               
               },
               {
          
                    element: '#DataTables_Table_0_filter',
                    placement: 'left',
                    backdrop: true,

                    content: '<h3 style="text-align: center;"><b>Filter currently shown samples</b></h3>All searches can be narrowed down further with this search box.  It will not search the database but it will filter the current samples in sample log book.'
          
               },
               {
               
                    element: '#sample-in-progress-workbook',
                    placement: 'bottom',

                    content: '<h3 style="text-align: center;"><b>The in progress workbook</b></h3>This page was made for the purpose of copy and pasting into word.'
               
               }
               

          ];

          return sample_log_book_info;
     }

     function _thermofisher_backup_files()
     {
          let  igv_videos = _igv_videos(),
               tf_backup =[
          {
               backdrop: true,
               orphan: true, 
               content: '<h3 style="text-align: center;"><b>Overview of VariantCaller Data Pulled</b></h3> <ul> <li><b>bam</b> <ul> <li>The bam folder contains the bam and bai files.</li><li>These Bam and Bai files are obtained from the Ion reporter VariantCaller workflow. The vcf_VariantCaller files are also obtained via the VariantCaller workflow.</li></ul> </li><li><b>vcf_VariantCaller</b> <ul> <li>These vcf files are obtained from the Ion reporter VariantCaller workflow. The Bam and Bai files are also obtained via the VariantCaller workflow.</li></ul> </li></ul> <br><br> These files are useful to look at the variants in <a href="http://software.broadinstitute.org/software/igv/" target="_blank">IGV</a>.  The next step will provide a video on the basic use of IGV.'
               // <ul>
               //      <li><b>bam</b>
               //           <ul>
               //                <li>The bam folder contains the bam and bai files.</li>
               //                <li>These Bam and Bai files are obtained from the Ion reporter VariantCaller workflow.  The vcf_VariantCaller files are also obtained via the VariantCaller workflow.</li>
               //           </ul>
               //      </li>
                    
               //      <li><b>vcf_VariantCaller</b>
               //           <ul>
               
               //                <li>These vcf files are obtained from the Ion reporter VariantCaller workflow.  The Bam and Bai files are also obtained via the VariantCaller workflow.</li>
               //           </ul>
               //      </li>
                    
               // </ul>
               
          },
          {
               backdrop: true,
               orphan: true, 
               content: '<h3 style="text-align: center;"><b>Overview of Data '+site_title+' Upload Files</b></h3><ul> <li><b>coverage</b> <ul> <li>The coverage files are made by '+site_title+' by combining ofa amplicons bed file with the qc/ *bcmatrix.tsv and qc/ *select_api_dump.json files.</li><li>all_coverage_amplicons.tsv file is used for </li></ul> </li><li><b>vars_(workflow)_(ion_reporter_version)_(analysis_start_date)</b> <ul> <li>Variant Files to upload to '+site_title+' can be found here</li><li>These tsv files are obtained after running the Ion reporter Analysis API. Data returned from the API provides data_links -> filtered_variants/unfiltered_variants.</li><li>Filtered in this file name refers to variants filtered by the workflow</li><li>Since these files vary depending on workflow, ion reporter version, and analysis date it is important to keep track of this information. This is the reason why the folder name contains this info</li><li>The folder can found programmatically easily by using a <a href="https://en.wikipedia.org/wiki/Regular_expression" target="_blank">Regular expression</a></li><li>'+site_title+' uses the *filtered_SB_added.tsv file from this folder to obtain the variant data for all samples</li><li>'+site_title+' uses the *AcroMetrix*unfiltered_SB_added.tsv file from this folder to obtain the variant data for the AcroMetrix control</li><li>The filtered.tsv and unfiltered.tsv files are obtained from the Ion Reporter directly</li><li>'+site_title+' makes the filtered_SB_added.tsv from merging data from the filtered.tsv with vcf_(workflow)_(ion_reporter_version)_(analysis_start_date)/ *_filtered.vcf file.</li><li>'+site_title+' makes the unfiltered_SB_added.tsv from merging data from the filtered.tsv with vcf_(workflow)_(ion_reporter_version)_(analysis_start_date)/ *_unfiltered.vcf file.</li></ul> </li><li><b>vcf_(workflow)_(ion_reporter_version)_(analysis_start_date)</b> <ul> <li>Ion reporter produces many versions of vcf files. These vcf files are obtained after running the Ion reporter Analysis API. Data returned from the API provides data_links -> filtered_variants/unfiltered_variants. Filtered in this file name refers to filtered by the workflow</li><li>Since these files vary depending on workflow, ion reporter version, and analysis date it is important to keep track of this information. This is the reason why the folder name contains this info</li></ul> </li></ul>'
               // <ul>               
               //      <li><b>coverage</b>
               //           <ul>
               //                <li>The coverage files are made by '+site_title+' by combining <a href="/var/www/urmc_reporter_data/ofa_bed_files/ofa_amplicons_db_v1.1.1.txt" download>this ofa amplicons bed file</a> with the qc/ *bcmatrix.tsv and qc/ *select_api_dump.json files.</li>
               //                <li>all_coverage_amplicons.tsv file is used for </li>
               //           </ul>
               //      </li>
              
               //      <li><b>vars_(workflow)_(ion_reporter_version)_(analysis_start_date)</b>
               //           <ul>
               //                <li>Variant Files to upload to '+site_title+' can be found here</li>
               //                <li>These tsv files are obtained after running the Ion reporter Analysis API.  Data returned from the API provides data_links -> filtered_variants/unfiltered_variants.</li>
               //                <li>Filtered in this file name refers to variants filtered by the workflow</li>
               //                <li>Since these files vary depending on workflow, ion reporter version, and analysis date it is important to keep track of this information.  This is the reason why the folder name contains this info</li>
               //                <li>The folder can found programmatically easily by using a <a href="https://en.wikipedia.org/wiki/Regular_expression" target="_blank">Regular expression</a></li>
               //                <li>'+site_title+' uses the *filtered_SB_added.tsv file from this folder to obtain the variant data for all samples</li>
               //                <li>'+site_title+' uses the *AcroMetrix*unfiltered_SB_added.tsv file from this folder to obtain the variant data for the AcroMetrix control</li>
               //                <li>The filtered.tsv and unfiltered.tsv files are obtained from the Ion Reporter directly</li>
               //                <li>'+site_title+' makes the filtered_SB_added.tsv from merging data from the filtered.tsv with vcf_(workflow)_(ion_reporter_version)_(analysis_start_date)/ *_filtered.vcf file.</li>
               //                <li>'+site_title+' makes the unfiltered_SB_added.tsv from merging data from the filtered.tsv with vcf_(workflow)_(ion_reporter_version)_(analysis_start_date)/ *_unfiltered.vcf file.</li>
               //                     </ul>
               //                </li>
               //      <li><b>vcf_(workflow)_(ion_reporter_version)_(analysis_start_date)</b>
               //           <ul>

               //                <li>Ion reporter produces many versions of vcf files.  These vcf files are obtained after running the Ion reporter Analysis API.  Data returned from the API provides data_links -> filtered_variants/unfiltered_variants.  Filtered in this file name refers to filtered by the workflow</li>
               //                <li>Since these files vary depending on workflow, ion reporter version, and analysis date it is important to keep track of this information.  This is the reason why the folder name contains this info</li>
               //           </ul>
               //      </li>
               
               // </ul>
               
          },
          {
               backdrop: true,
               orphan: true, 
               content: '<h3 style="text-align: center;"><b>Overview of the Remaining Files</b></h3><ul> <li><b>md5s</b> <ul> <li>After every file is copied from the original location in '+site_title+' it checks to make sure the copy is exactly the same as the original. There are multiple files created to track this process. If a file is not copied correctly '+site_title+' will try again up to 10 times. After 10 times it will produce an error.</li><li><b>md5</b>: is the algorigthm '+site_title+' uses to check if check the files are the same. <a href="https://en.wikipedia.org/wiki/MD5" target="_blank">more info</a></li><li><b>Files</b> <ul> <li><u>pool_qc_backup_data_md5.tsv</u>: used to track the transfer of qc data in the qc folder</li><li><u>ir_backup_tracker.tsv</u>: used to summarize the full completion of the transfer performed during the pool upload data step.</li><li><u>ir_backup.tsv</u>: used to track every file which is copied outside of the qc folder.</li></ul> </li></ul> </li><li><b>qc</b> <ul> <li>All the qc data pulled from the torrent server or ion reporter can be found here</li></ul> </li><li><b>log files</b> <ul> <li>These files keep track of the back up process or errors that occured</li><li>Because of thermofishers settings on the zip file obtained it will always produce the error fchmod (file attributes) error: Operation not permitted warning: cannot set modif./access times. These errors can be ignored.</li></ul> </li></ul>'
               // <ul>                    
               //      <li><b>md5s</b>
               //           <ul>
               //                <li>After every file is copied from the original location in '+site_title+' it checks to make sure the copy is exactly the same as the original.  There are multiple files created to track this process.  If a file is not copied correctly '+site_title+' will try again up to 10 times.  After 10 times it will produce an error.</li>
               //                <li><b>md5</b>: is the algorigthm '+site_title+' uses to check if check the files are the same. <a href="https://en.wikipedia.org/wiki/MD5" target="_blank">more info</a></li>
               //                <li><b>Files</b>
               //                     <ul>
               //                          <li><u>pool_qc_backup_data_md5.tsv</u>: used to track the transfer of qc data in the qc folder</li>
               //                          <li><u>ir_backup_tracker.tsv</u>: used to summarize the full completion of the transfer performed during the pool upload data step.</li>
               //                          <li><u>ir_backup.tsv</u>: used to track every file which is copied outside of the qc folder.</li>
               //                     </ul>
               //                </li>
               //           </ul>
               //      </li>
               //      <li><b>qc</b>
               //           <ul>
               //                <li>All the qc data pulled from the torrent server or ion reporter can be found here</li>
               //           </ul>
               //      </li>
                    
               //      <li><b>log files</b>
               //           <ul>
               //                <li>These files keep track of the back up process or errors that occured</li>
               //                <li>Because of thermofishers settings on the zip file obtained it will always produce the error fchmod (file attributes) error: Operation not permitted warning:  cannot set modif./access times.  These errors can be ignored.</li>
               //           </ul>
               //      </li>
               // </ul>
               
          }];


          tf_backup = utils.mergeArrays(tf_backup,igv_videos,1);

          return tf_backup;
     }
      function _igv_videos()
     {
          let  igv =[
          {
               backdrop: true,
               orphan: true,                         
               content: 'Here are two good videos to introduce you to IGV. <ul><li><a href="https://www.youtube.com/watch?v=E_G8z_2gTYM" target="_blank">IGV Sequencing Data Basics</a></li><li><a href="https://www.youtube.com/watch?v=EpD2ZHM7Q8Q" target="_blank">IGV VCF Basics</a></li></ul>'
          }];
          return igv;
     }

     return {
          start_tour:_start_tour,
          color_keys:_color_keys,
          table_info:_table_info,
          sample_log_book_info:_sample_log_book_info,
          thermofisher_backup_files:_thermofisher_backup_files,
          igv_videos:_igv_videos
     };
});
