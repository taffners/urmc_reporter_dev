define(['models/utils'], function(utils)
{
     function _RunUtilsTests()
     {
          // Purpose: Test functions in the Utils Module

          // var  _non_empty_ids =
          //      {
          //           'firstName':'Fill out your first name.',
          //           'lastName':'Fill out your last name.'
          //      };

          // Test merge array works
          QUnit.test('Utils Test: Check if Merge array works', function(assert)
          {
               // check if it picks up the first one being empty
               assert.deepEqual(utils.mergeArrays([{'a':'a'}, {'b':'b'}], [{'c':'c'}, {'e':'e'}], 1 ), [{'a':'a'}, {'c':'c'}, {'e':'e'}, {'b':'b'}], 'Testing mergeArrays');
          });

          QUnit.test('Utils Test: MergeArraysToKeyValPairs', function(assert)
          {
               
               assert.deepEqual(utils.MergeArraysToKeyValPairs([1,2,3], ['a','b','c']) , {1:'a', 2:'b', 3:'c'}, 'nums with strs');
          });

          QUnit.test('Utils Test: CheckInArray', function(assert)
          {
               // check if it picks up the first one being empty
               assert.equal(utils.CheckInArray(['a', 'b', 'c'], ['a', 'b', 'c']), true,'same');
               assert.equal(utils.CheckInArray(100000,100000), true,'Large positive numbers');
               assert.equal(utils.CheckInArray(['a', 'b', 'c'], ['a', 'b', 'c', 'd']), true,'Part of a larger array');
               assert.equal(utils.CheckInArray(['a', 'b', 'c'], ['a', 'b']), false,'not complete');
               assert.equal(utils.CheckInArray(['a', 'b', 1], ['a', 'b', 1]),true, 'strs and ints');
               assert.equal(utils.CheckInArray(['a', 1, 'b'], ['a', 'b', 1]), true,'strs and ints reordered');
               assert.equal(utils.CheckInArray('string','strings'), true,'strings');

          });

          QUnit.test('Utils Test: GetHeightClass', function(assert)
          {

               assert.equal(utils.GetHeightClass('row'), 1669.09,'class row, This is set for a ');
               assert.equal(utils.GetHeightClass('row'), 1669.09,'class row');
          });


          QUnit.test('Utils Test: US_date_to_YYYYMMDD', function(assert)
          {
               // !!!!!! NEED to add to the function to restrict formats
               assert.equal(utils.US_date_to_YYYYMMDD('08/13/2020'), '20200813','August Date');
          });

          QUnit.test('Utils Test: isValidDate', function(assert)
          {
               // !!!!!! NEED to add to the function to restrict formats
               assert.equal(utils.isValidDate('08/13/2020'), true,'August Date');
               assert.equal(utils.isValidDate('13/13/2020'), false,'13 months');
               assert.equal(utils.isValidDate('01/32/2020'), false,'32 days');
               assert.equal(utils.isValidDate('1/32/2020'), false,'1 digit month');
               assert.equal(utils.isValidDate('08/13/0920'), false,'date in year 920');
               assert.equal(utils.isValidDate('mm/13/0920'), false,'string in date');

               assert.equal(utils.isValidDate('202008130'), false,'August YYYYMMDD Date');
          });

          QUnit.test('Utils Test: isValidDateYYYYMMDD', function(assert)
          {
               // !!!!!! NEED to add to the function to restrict formats
               assert.equal(utils.isValidDateYYYYMMDD('2020-08-20'), true,'August Date');
               assert.equal(utils.isValidDateYYYYMMDD('2020-10-20'), true,'October Date');
               assert.equal(utils.isValidDateYYYYMMDD('2020-13-13'), false,'13 months');
               assert.equal(utils.isValidDateYYYYMMDD('2020-01-32'), false,'32 days');
               assert.equal(utils.isValidDateYYYYMMDD('2020-1-10'), false,'1 digit month');
               assert.equal(utils.isValidDateYYYYMMDD('0920-01-10'), false,'date in year 920');
               assert.equal(utils.isValidDateYYYYMMDD('2020-10-m1'), false,'string in date');

               assert.equal(utils.isValidDateYYYYMMDD('08/13/2020'), false,'mm/dd/yyyy format');

               assert.equal(utils.isValidDateYYYYMMDD('202008130'), false,'August YYYYMMDD Date');
          });


          QUnit.test('Utils Test: strIsDateYYYYMMDD', function(assert)
          {
               assert.equal(utils.strIsDateYYYYMMDD('2020-05-05'), true,'May date');
               assert.equal(utils.strIsDateYYYYMMDD('2020-05-5'), false,'one digit day');
               assert.equal(utils.strIsDateYYYYMMDD('0925-05-05'), true,'year 0925');
               assert.equal(utils.strIsDateYYYYMMDD('08/13/2020'), false,'August mmddYYYY Date');
               assert.equal(utils.strIsDateYYYYMMDD('20200813'), false,'August YYYYMMDD Date');
               assert.equal(utils.strIsDateYYYYMMDD('08/13/0920'), false,'date in year 920');
          });

          QUnit.test('Utils Test: strIsDateMMYYDD', function(assert)
          {
               assert.equal(utils.strIsDateMMYYDD('08/13/2020'), true,'August mmddYYYY Date');
               assert.equal(utils.strIsDateMMYYDD('20200813'), false,'August YYYYMMDD Date');
               assert.equal(utils.strIsDateMMYYDD('08/13/0920'), true,'date in year 920');
          });

          QUnit.test('Utils Test: GetCurrURL', function(assert)
          {
               assert.equal(utils.GetCurrURL(), 'https://pathlamp.urmc-sh.rochester.edu/devs/urmc_reporter/?page=admin&access_filter=all','check if current url is correct');
             
          });

          QUnit.test('Utils Test: PhpVars', function(assert)
          {
               assert.deepEqual(utils.PhpVars(), {'access_filter':'all', 'page':'admin'},'check current url php vars');
             
          });



          QUnit.test('Utils Test: isPositveNumber', function(assert)
          {
               // check if it picks up the first one being empty
               assert.equal(utils.isPositveNumber(1), true,'Test 1');
               assert.equal(utils.isPositveNumber(100000), true,'Large positive number');
               assert.equal(utils.isPositveNumber(0.1), false,'Positive decimal smaller than 1');
               assert.equal(utils.isPositveNumber(0.999), false,'Positive decimal smaller than 1 but close');
               assert.equal(utils.isPositveNumber(-1), false,'negative number');
               assert.equal(utils.isPositveNumber(-100000302),false, 'negative large number');
               assert.equal(utils.isPositveNumber(-10.05), false,'negative decimal');
               assert.equal(utils.isPositveNumber('string'), false,'string');

          });


          QUnit.test('Utils Test: isPositveNumber', function(assert)
          {

               localStorage.removeItem('update_increament_local_storage_var');
               // check if it picks up the first one being empty
               assert.equal(utils.update_increament_local_storage_var('update_increament_local_storage_var', true), 1,'Test 1');

               
          });



          // // Test Validate Email
          // QUnit.test('Utils Test: Check Validate Email', function(assert)
          // {
          //      // Test a valid email address
          //      assert.ok(utils.ValidateEmail('taffners@gmail.com'), 'Test a valid email address');

          //      // Test an foreign email address
          //      assert.ok(utils.ValidateEmail('taffners@gmail.ca'), 'Test a valid foriegn email address');

          //      assert.ok(utils.ValidateEmail('firstname.lastname@domain.com'), 'Email contains dot in the address field');

          //      assert.ok(utils.ValidateEmail('email@subdomain.domain.com'), 'Email contains dot with subdomain');

          //      assert.ok(utils.ValidateEmail('firstname+lastname@domain.com'), 'Plus sign is considered valid character');

          //      assert.ok(utils.ValidateEmail('email@[123.123.123.123]'), '	Square bracket around IP address is considered valid');

          //      assert.ok(utils.ValidateEmail('“email”@domain.com'), 'Quotes around email is considered valid');

          //      assert.ok(utils.ValidateEmail('1234567890@domain.com'), 'Digits in address are valid');

          //      assert.ok(utils.ValidateEmail('email@domain-one.com'), 'Dash in domain name is valid');

          //      assert.ok(utils.ValidateEmail('_______@domain.com'), '	Underscore in the address field is valid');

          //      assert.ok(utils.ValidateEmail('email@domain.name'), '.name is valid Top Level Domain name');

          //      assert.ok(utils.ValidateEmail('email@domain.co.jp'), 'Dot in Top Level Domain name also considered valid (use co.jp as example here)');

          //      assert.ok(utils.ValidateEmail('firstname-lastname@domain.com'), 'Dash in address field is valid');

          //      // Not valid email addresses
          //      assert.ok(!utils.ValidateEmail('email@123.123.123.123'), 'Domain is valid IP address');

          //      assert.ok(!utils.ValidateEmail('taffnersgmail.com'), '	Missing @');

          //      assert.ok(!utils.ValidateEmail('taffners@gmailcom'), '	Missing top level domain (.com/.net/.org/etc)');

          //      assert.ok(!utils.ValidateEmail('plainaddress'), '	Missing @ sign and domain');

          //      assert.ok(!utils.ValidateEmail('#@%^%#$@#$@#.com'), 'Garbage');

          //      assert.ok(!utils.ValidateEmail('@domain.com'), 'Missing username');

          //      assert.ok(!utils.ValidateEmail('Joe Smith <email@domain.com>'), 'Encoded html within email is invalid');

          //      assert.ok(!utils.ValidateEmail('email@domain@domain.com'), 'Two @ sign');

          //      assert.ok(!utils.ValidateEmail('.email@domain.com'), '	Leading dot in address is not allowed');

          //      assert.ok(!utils.ValidateEmail('email.@domain.com'), '	Trailing dot in address is not allowed');

          //      assert.ok(!utils.ValidateEmail('email..email@domain.com'), 'Multiple dots');

          //      assert.ok(!utils.ValidateEmail('email@domain.com (Joe Smith)'), 'Text followed email is not allowed');

          //      assert.ok(!utils.ValidateEmail('email@domain..com'), '	Multiple dot in the domain portion is invalid');
          // });

          // // Test Validate only ints
          // QUnit.test('Utils Test: Check Validate only ints', function(assert)
          // {
          //      // vaild only ints
          //      assert.ok(utils.ValidateOnlyInts('12345678911'), '11 only ints');

          //      assert.ok(!utils.ValidateOnlyInts('12345a8b11'), 'letters included');

          //      assert.ok(!utils.ValidateOnlyInts('1!@#$11'), 'special chars included');
          // });

          // // test Validate Only Int PhoneNum
          // QUnit.test('Utils Test: Check Validate Only Int PhoneNum', function(assert)
          // {
          //      // vaild only ints
          //      assert.ok(utils.ValidateOnlyIntPhoneNum('12345678911'), '11 only ints');

          //      assert.ok(!utils.ValidateOnlyIntPhoneNum('1234567891'), 'Not enough ints');

          //      assert.ok(!utils.ValidateOnlyIntPhoneNum('12345a8b11'), 'letters included');

          //      assert.ok(!utils.ValidateOnlyIntPhoneNum('1!@#$11'), 'special chars included');
          // });

          // QUnit.test('Utils New line to HTML', function(assert)
          // {
          //      // \r\n|\r|\n
          //      assert.equal(utils.NewLineToHTML('1st line\r\nsecond line'), '1st line<br>second line', 'test \r\n');
          //      assert.equal(utils.NewLineToHTML('1st line\rsecond line'), '1st line<br>second line', 'test \r');
          //      assert.equal(utils.NewLineToHTML('1st line\nsecond line'), '1st line<br>second line', 'test \n');
          //      assert.equal(utils.NewLineToHTML('1st line second line'), '1st line second line', 'test no new line');
          // });

          // QUnit.test('Utils Replace Caplital letters with dash & lower case ', function(assert)
          // {
          //      assert.equal(utils.ReplaceCapWithDash('firstSecond'), 'first-second', 'test one cap');

          //      assert.equal(utils.ReplaceCapWithDash('tubeID'), 'tube-i-d', 'test two cap');
          // });

          // QUnit.test('Utils Find a word in a str and cut it at the str', function(assert)
          // {
          //      assert.equal(utils.FindWordInStrAndCut('pop-up-accordion group_num_20', 'group_num_'), '20', 'test1');

          //      assert.equal(utils.FindWordInStrAndCut('pop-up-accordion group_num_20', 'pop-up-'), 'accordion', 'test2');
          // });
     }

     return {
          RunUtilsTests:_RunUtilsTests
     }
});
