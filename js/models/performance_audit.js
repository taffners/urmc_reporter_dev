define(['models/utils','models/google_charts'],function(utils, google_charts)
{
     function _BindEvents(php_vars)
     {
  
          // add summary of logins Google horizontal bar chart
          $.ajax(
          {
               type:'POST',
               url: 'utils/audits.php?',

               dataType: 'text',
               data: {'query_filter': 'month_counts'
               },
               success: function(response)
               {
                    if (response ==='Post Does not contain query_filter')
                    {

                    }
                    else
                    {
                         var  json_response = JSON.parse(response),
                              chart_data = [['Month','passed', 'failed']],
                              chart_height = 100,
                              chart_height_per_month = 50,
                              curr_month,
                              div_id = 'month_counts';
                             
                         // Get all the possible curr_status
                         json_response.forEach(function(val, i)
                         {
                              chart_height += chart_height_per_month;   
                              curr_month = [val['month']+' '+val['year'], parseInt(val['passed_tests']), parseInt(val['failed_count'])];
                              chart_data.push(curr_month);
                         });   

                         $('#'+div_id).height(chart_height);    

                         h_axis = {
                              'title':'Number of Tests'
                         }

                         google_charts.drawBarChart(div_id, 'Turn Around Times', chart_data, '', 'horizontal');
                    }

               },
               error: function(textStatus, errorThrown)
               {
                    utils.dialog_window('problem with ajax call ;( ' + errorThrown + ' ' + textStatus);
               }
          });
     }

     function _SetView()
     {

     }

     function _start(test_status, php_vars)
     {
          _SetView();
          _BindEvents(php_vars);
     }

     return {
          start:_start
     };
});
