define(function()
{
     function _BindEvents(php_vars)
     {
          // Add to tables with a class of dataTable the sorting ability from bootstraps datatables https://datatables.net/manual/styling/bootstrap
          require(['views/events/TableEvents'], function(TableEvents)
          {
               TableEvents.StartEvents();
          });

          // enable submit button once a change has occurred in type=text fields
          require(['views/events/activate_submit_button'], function(activate_submit_button)
          {
               activate_submit_button.StartEvents();
          });

          // toggle progress report bar
          require(['views/events/toggle_progress_bar'], function(toggle_progress_bar)
          {
               toggle_progress_bar.StartEvents();
          });

          // enable date field calendar popups
          require(['views/events/date_fields'], function(date_fields)
          {
               date_fields.StartEvents();
          });

          // // In the variant table cells larger than 20 characters are cut.  This will enable the more button and less button
          require(['views/events/toggle_teaser_complete_strs'], function(toggle_teaser_complete_strs)
          {
               toggle_teaser_complete_strs.StartEvents();
          });

          $('form').on('keyup keypress', function(e) 
          {
               var keyCode = e.keyCode || e.which;
               
               if (keyCode === 13) 
               { 
                    e.preventDefault();
                    return false;
               }
          });

          // capitalize data entry.  Example aDAM -> Adam
          $('.capitalize').on('keyup paste', function(e)
          {
               var  textBox = e.target,
                    start = textBox.selectionStart,
                    end = textBox.selectionStart;
                    
                    textBox.value = textBox.value.charAt(0).toUpperCase() + textBox.value.slice(1).toLowerCase();
                    textBox.setSelectionRange(start, end);
          });

          // a button to unslect all checkboxes labeled with class chkbx
          $('#unselect-chkbx').on('click', function()
          {
               $('.chkbx').prop('checked', false);
          });

          // Sometimes there is not enough room for the progress bar.  If this is the case remove the affix
          $(document).ready(console.log(isInViewPoint('report-nav')));


          // toggle-last-step-not-visible
          // 



     }

     function isInViewPoint(select_id)
     {
          // make sure id exists 
          if ($('#' + select_id).length == 0)
          {
                return false;
          }

          let  elem = document.querySelector('#' + select_id),
               elementTop =  elem.getBoundingClientRect()['top'],
               elementBottom = elementTop + $('#' + select_id).height(),
               viewportTop = $(window).scrollTop(),
               viewportBottom = viewportTop + getDocHeight();
console.log(elementTop)   

console.log(elementBottom)   
console.log(viewportTop)   
console.log(viewportBottom)          
          return elementBottom > viewportTop && elementTop < viewportBottom;
     }

     function getDocHeight() 
     {
          let D = document;
          
          return Math.max(
               D.body.scrollHeight, D.documentElement.scrollHeight,
               D.body.offsetHeight, D.documentElement.offsetHeight,
               D.body.clientHeight, D.documentElement.clientHeight
          );
     }

     function _SetView()
     {

     }

     function _start(php_vars)
     {
          _SetView();
          _BindEvents(php_vars);
     }

     return {
          start:_start
     };
});
