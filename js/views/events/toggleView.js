define(function()
{
     function _StartEvents(paging_status)
     {
          // Purpose: With multiple toggle buttons which view is shown.

          // how to use:
               // Add all buttons in view
               // Add toggle-view class to buttons
               // Add a button to show all
                    // id ="all-btn-id"
                    // All ids related to this toggle but not all add as a comma separated list under data-toggle-view-ids
               // Add a view toggle btn
                    // id ="currid-btn-id"
                    // All ids related to this toggle but not all add as a comma separated list under data-toggle-view-ids
               // Enclose data in a div to hide with id currid-view
                    // Example
                         // <div class="row" id="sams-view">
               // Example:
                    // <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                    //      <button type="button" id="all-btn-id" class="btn btn-primary toggle-view" data-toggle-view-ids="sams,phils">All</button>
                    // </div>
                    // <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                    //      <button type="button" id="sams-btn-id" class="btn btn-primary toggle-view" data-toggle-view-ids="sams,phils">Samantha</button>
                    // </div>
                    // <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                    //      <button type="button" id="phils-btn-id" class="btn btn-primary toggle-view" data-toggle-view-ids="sams,phils">Phil</button>
                    // </div>

          $('.toggle-view').on('click', function()
          {
               var  all_view_ids = $(this).data('toggle-view-ids').split(','),
                    curr_id = $(this).attr('id').replace('-btn-id', ''),
                    i;

               // set all button to green if it was clicked
               if (curr_id === 'all')
               {                    
                    $('#'+curr_id+'-btn-id').removeClass('btn-primary');
                    
                    $('#'+curr_id+'-btn-id').addClass('btn-success');
               }
               else
               {                 
                    $('#all-btn-id').addClass('btn-primary');                    
                    $('#all-btn-id').removeClass('btn-success');
               }

               // Hide views not equal to curr id
               for (i in all_view_ids)
               {
                    if (curr_id === 'all')
                    {
                         // Turn all sections to show 
                         $('#'+all_view_ids[i]+'-view').removeClass('hide').removeClass('d-none').show();

                         // turn current button to blue since it isn't all
                         $('#'+all_view_ids[i]+'-btn-id').addClass('btn-primary');
                         
                         $('#'+all_view_ids[i]+'-btn-id').removeClass('btn-success');

                    }

                    else if (all_view_ids[i] === curr_id)
                    {                
                         // turn this button green
                         $('#'+all_view_ids[i]+'-btn-id').removeClass('btn-primary');
                         
                         $('#'+all_view_ids[i]+'-btn-id').addClass('btn-success');
                         
                         // Show this data
                         $('#'+all_view_ids[i]+'-view').removeClass('hide').removeClass('d-none').show();
                         
                    }
                    else
                    {                  
                         // turn this button blue
                         $('#'+all_view_ids[i]+'-btn-id').addClass('btn-primary');
                         
                         $('#'+all_view_ids[i]+'-btn-id').removeClass('btn-success');
                         
                         // hide this data
                         $('#'+all_view_ids[i]+'-view').hide();
                    }
               }

          });

          $('.toggle-groups').on('click', function()
          {
               var  i,
                    hide_groups = $(this).data('hide').split(','),
                    show_groups = $(this).data('show').split(',');
               

               for (i in hide_groups)
               {
                    if (hide_groups[i] != '')
                    {
                         $('.'+hide_groups[i]).hide();
                         $('#'+hide_groups[i]+'-toggle-groups').addClass('btn-primary');
                         $('#'+hide_groups[i]+'-toggle-groups').removeClass('btn-success');
                    }
               }

               for (i in show_groups)
               {
                    $('.'+show_groups[i]).show(); 
                    $('#'+show_groups[i]+'-toggle-groups').removeClass('btn-primary');
                    $('#'+show_groups[i]+'-toggle-groups').addClass('btn-success');                
               }


          });

     }

     return {
          StartEvents:_StartEvents
     }
});
