define(['models/utils'],function(utils)
{
     function _StartEvents(php_vars)
     {
          // Add date picker to fields for a workaround for firefox not having type="date"
          $('.date-time-picker').each(function()
          {
               $(this).datetimepicker(
               {
                    sideBySide: true,
                    format: 'MM/DD/YYYY HH:mm:ss'
               });
          });

          // ensure date submitted is valid
          $('input[type=date]').on('focusout', function()
          {
               var  d = $(this).val();

               if (!utils.isValidDateYYYYMMDD(d))
               {
                    utils.dialog_window('Date '+d+' is no not valid');
               }
          });



          // ensure date submitted is valid
          $('.copy-paste-date').on('focusout', function()
          {
               var  d = $(this).val();

               if (!utils.isValidDate(d))
               {
                    $(this).val('');
                    utils.dialog_window('Date '+d+' is no not valid');
                    
               }
          });
     }

     return {
          StartEvents:_StartEvents
     }
});
