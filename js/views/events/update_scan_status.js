define(['models/utils'],function(utils)
{
     function _StartEvents(paging_status)
     {
          // Purpose: change scan status in run_info_table
          $('.toggle-transferred-to-patient-chart-status').on('change', function()
          {
               let  run_id = $(this).data('run_id'),
                    val = $(this).data('val');

               $.ajax(
               {
                    type:'POST',
                    url: 'utils/update_scan_status.php?',

                    dataType: 'text',
                    data: {
                              'run_id': run_id,
                              'val': val
                    },
                    success: function(response)
                    {
                         if (response.includes('error'))
                         {
                              utils.dialog_window('problem with ajax call: ' + response);         
                         }
                         // window.location.reload(true)

                    },
                    error: function(textStatus, errorThrown)
                    {
                         utils.dialog_window('problem with ajax call ;( ' + errorThrown + ' ' + textStatus);
                    }
               });
          });
     }

     return {
          StartEvents:_StartEvents
     }
});
