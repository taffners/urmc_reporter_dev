define(['models/utils'],function(utils)
{
     function _StartEvents()
     {
   
          // toggle modal for update or delete tier
          $('.del_update_tier').on('click', function()
          {
               var  variant_name = $(this).data('variant'),
                    knowledge_id = $(this).attr('id').replace('del_update_tier', '');

               // if the modal is already loaded knowledge id will now be undefined
               // find the knowledge id on the select 
               if (knowledge_id === undefined)
               {
                    knowledge_id = $('#tier-tissue-add-table-tier-select').data('knowledge_id');
               }

               // add header to modal
               $('#tier-modal-title').html('Update or Delete Tier Info for: <b>' + variant_name + '</b>');

               // make sure confirm window is hidden
               _hide_confirm_box();

               // build current tier table for deleting
               $.when(_build_curr_tier_table(knowledge_id)).done(function()
               {
              
                    // activate delete tier button
                    $('.delete_tier').on('click', function()
                    {
                         // get tier data for confirm window
                         // get xref_id (xref_id_del_)
                         var  tier = $(this).data('tier'),
                              tissue = $(this).data('tissue'),
                              summary = $(this).data('summary'),
                              xref_id = $(this).attr('id').replace('vt_xref_id_del_', '');
         
                         // remove all selected_danger rows
                         _hide_confirm_box();

                         // change the table row color to selected
                         $(this).addClass('selected_danger');

                         // ask user if they would really like to delete this
                         $('#confirm_del_tier_msg').empty().html('Do you want to delete the tier: <br><b>' + tier + ' ' + summary + ' ' + tissue +'</b>'); 
              
                         // show msg
                         $('#confirm_del_tier').attr('class', 'show');

                         // once no button is clicked hide and empty confirm 
                         $('button#confirm_del_tier_buttons_no').on('click', function()
                         {
                              _hide_confirm_box();
                         });

                         // delete variant if yes is clicked
                         $('button#confirm_del_tier_buttons_yes').on('click', function()
                         {
                              
                              $.ajax(
                              {
                                 type: 'GET',
                                 url: 'utils/del_tier.php',
                                 dataType: 'json',
                                 data:  {
                                             'vt_xref_id':xref_id
                                        } 
                              }).done(function(response)
                              {
                                   $('#tier_modal').modal('toggle');
                                   
                                   // reload page
                                   window.location.reload();
                              });

                         });
                    });

               });

               // load select lists for adding a tier and tissue type
               // since these lists are loading dynamically wait until both ajax calls 
               // are done before activating submit-new-tier button
               $.when(_add_tiers_to_select(knowledge_id), _add_tissues_to_select(knowledge_id)).done(function()
               {
                  
                    // activate submit new tier button
                    $('#submit-new-tier').on('click', function()
                    {
                         // get tier and tissue info
                         var  tier_id = $('#tier-tissue-add-table-tier-select')
                                        .find(':selected')
                                        .val(),
                              tissue_id = $('#tier-tissue-add-table-tissue-select')
                                        .find(':selected')
                                        .val(),
                              knowledge_id = $('#tier-tissue-add-table-tissue-select')
                                        .data('knowledge_id'),
                              user_id = '1';

                         // make sure tier is not empty
                         if (tier_id !== '')
                         {                
                              // make sure it isn't already in the database
                              $.when(_find_tier_already_there(tier_id, tissue_id, knowledge_id, user_id)).done(function(response)
                              {
                                   // add to database
                                   if(response === 'not_found')
                                   {
                                        // hide msg center
                                        $('#tier-tissue-add-table-msg-center')
                                             .hide();

                                        // add tier-tissue to xref                                   
                                        $.ajax(
                                        {
                                           type: 'GET',
                                           url: 'utils/add_tier.php',
                                           data:  {
                                                      'tier_id':tier_id, 
                                                      'tissue_id':tissue_id, 
                                                      'knowledge_id':knowledge_id, 
                                                      'user_id':user_id 
                                                  } 
                                        }).done(function(response)
                                        {              
                                             // make sure added
                                             if (response.indexOf('Oops!!') === -1)
                                             {
                                                  $('#tier_modal').modal('toggle');
                                                  
                                                  // reload page
                                                  window.location.reload();

                                             }
                                             else
                                             {
                                                  // change msg center to danger
                                                  $('#tier-tissue-add-table-msg-center')
                                                       .attr('class','alert alert-danger')
                                                       .html(response);
                                             }                                              
                                        });                                           
                                   }                                  
                                   else
                                   {
                                        $('#tier-tissue-add-table-msg-center')
                                             .removeClass('hide')
                                             .removeClass('d-none')
                                             .show()
                                             .html(response);
                                   }
                              });

                              
                         }

                    });
               });

               $('#tier_modal').modal('toggle');            
          });
     }

     function _hide_confirm_box()
     {
          $('#confirm_del_tier').hide();
          $('#confirm_del_tier_msg').empty();
     
          $('.selected_danger').each(function()
          {                          
               $(this).removeClass('selected_danger');
          });
     }

     function _build_curr_tier_table(knowledge_id)
     {

          // empty out tier table
          $('#tier-tissue-delete-table-body')
               .empty()
               .html('<tr class="odd"><td valign="top" colspan="4" class="dataTables_empty">No data available in table</td></tr>');
         
          // ajax call to find current tiers for this variant
          return $.ajax(
          {
             type: 'GET',
             url: 'utils/get_current_tiers.php',
             dataType: 'json',
             data:  {
                         'knowledge_id':knowledge_id
                    }  
          }).done(function(response)
          {         
  
               // sort the response by tier and then tissue
           
               response.sort(function(a, b)
               {
                    return a.tier.localeCompare(b.tier);
               });



               // if there are now tiers do not make table
               if (response.length > 0)
               {                
                    // response = JSON.parse(response);
                    // Make rows 
                    var  _i,
                         _all_rows = '';

                    for (_i=0; _i<response.length; _i++)
                    {
                         // add new row
                         _all_rows += '<tr id="vt_xref_id_del_' + response[_i]['vt_xref_id'] + '" class="hover_danger_on delete_tier" data-tier="' + response[_i]['tier'] + '" data-tissue="'+ response[_i]['tissue'] + '" data-summary="'+response[_i]['tier_summary']+'">';

                         // add vt_xref_id
                         _all_rows += '<td class="d-none">'+response[_i]['vt_xref_id']+'</td>';

                         // add tier
                         _all_rows += '<td>'+response[_i]['tier']+'</td>';

                         // add tier_summary
                         _all_rows += '<td>'+response[_i]['tier_summary']+'</td>';

                         // add tissue                    
                         _all_rows += '<td>'+response[_i]['tissue']+'</td>'            
                    }

                    _all_rows += '</tr>';
               
                    // add add a new row for each tier to tier-tissue-delete-table-body
                    $('#tier-tissue-delete-table-body')
                         .empty()
                         .append(_all_rows);
               }            
               
          });
     }

     function _add_tiers_to_select(knowledge_id)
     {
          // add all possible tiers to select menu
          return $.ajax(
          {
             type: 'GET',
             url: 'utils/get_all_tiers.php',
             dataType: 'json' 
          }).done(function(response)
          {
               // add options to select menu
               var  _i,
                    _all_options = '<option value="" selected="selected"></option>';

               for (_i=0; _i<response.length; _i++)
               {
                   
                    // add each option to all options
                    _all_options += '<option value="'+ response[_i]['tier_id']+'">'+response[_i]['tier'] +':'+response[_i]['tier_summary']+ '</option>'        
               }
         
               // add add a new row for each tier to tier-tissue-delete-table-body
               $('#tier-tissue-add-table-tier-select')
                    .empty()
                    .append(_all_options)
                    .attr('data-knowledge_id', knowledge_id);
          });
     }
     function _add_tissues_to_select(knowledge_id)
     {
          // add all possible Tissues to select menu
     
          return $.ajax(
          {
             type: 'GET',
             url: 'utils/get_all_tissues.php',
             dataType: 'json' 
          }).done(function(response)
          {
             
               // add options to select menu
               var  _i,
                    _all_options = '<option value="" selected="selected"></option>';

               for (_i=0; _i<response.length; _i++)
               {
                   
                    // add each option to all options
                    _all_options += '<option value="'+ response[_i]['tissue_id']+'">'+response[_i]['tissue']+ '</option>'        
               }
         
               // add add a new row for each tier to tier-tissue-delete-table-body
               $('#tier-tissue-add-table-tissue-select')
                    .empty()
                    .append(_all_options)
                    .attr('data-knowledge_id', knowledge_id);
          });
     }

     function _find_tier_already_there(tier_id, tissue_id, knowledge_id, user_id)
     {
          // find if tier present for variant

          return $.ajax(
          {
             type: 'GET',
             url: 'utils/find_specific_tier.php',
             data:  {
                        'tier_id':tier_id, 
                        'tissue_id':tissue_id, 
                        'knowledge_id':knowledge_id, 
                        'user_id':user_id 
                    } 
          });
     }

     return {
          StartEvents:_StartEvents
     }
});



