define(function()
{
     function _StartEvents()
     {

          $('input.no-spaces').bind(
          {
               keydown: function(keyPress)
               {
                    if (keyPress.keyCode == 32)
                    {
                         $(this)
                              .attr('title', 'Spaces are not allowed for this field')
                              .tooltip()
                              .mouseover();
                         return false;
                    }
               }
          });

          $('.uncheck-radio-btn').on('click',function()
          {
               var  name_radio_input = $(this).data('name_radio_input');

               $('input[name="'+name_radio_input+'"]').prop('checked', false);
          });

          $('input.no-period').bind(
          {
               keydown: function(keyPress)
               {
                    if (keyPress.keyCode == 190)
                    {
                         $(this)
                              .attr('title', 'Periods are not allowed for this field')
                              .tooltip()
                              .mouseover();
                         return false;
                    }
               }
          });

          // enable submit button once a change has occurred in type=text fields
          require(['views/events/activate_submit_button'], function(activate_submit_button)
          {             
               activate_submit_button.StartEvents();
          });
     }

     return {
          StartEvents:_StartEvents
     }
});
