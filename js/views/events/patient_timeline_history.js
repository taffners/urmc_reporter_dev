define(['models/google_charts'], function(google_charts)
{
     function _StartEvents(paging_status)
     {
          // Purpose:  add a patient timeline history if patient-timeline-history id is present.

          var  url = new URL(window.location.href),
               params = new URLSearchParams(url.search.slice(1)),
               patient_id = params.get('patient_id'),
               options = {
                         'insert_id':'patient-timeline-history'
                    };


          // Make sure insert location exists before running.
          if ($('#'+options['insert_id'])[0])
          {

               $.ajax(
               {
                    type:'POST',
                    url: 'utils/patient_timeline_history.php',
                    dataType: 'text',
                    data: {
                         'patient_id': patient_id 
                    },
                    success: function(response)
                    {    
console.log(response)                         
                         // Only include if not a control and not the only visit.
                         if (!response.includes('skip'))
                         {
                              var  timelineJSON = JSON.parse(response);

                              google_charts.drawPatientTimeline(timelineJSON, options);
                         }                      
                    },
                    error: function(textStatus, errorThrown)
                    {
                         utils.dialog_window('problem with ajax call ;( ' + errorThrown + ' ' + textStatus);
                    }
               });
          }
     }

     return {
          StartEvents:_StartEvents
     }
});
