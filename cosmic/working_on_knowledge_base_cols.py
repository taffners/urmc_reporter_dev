"Accession Number": "ENST00000275493", === transcript (http://www.ensembl.org/Homo_sapiens/Transcript/Summary?t=ENST00000275493) (include a check if ensembl or NM number)
    "FATHMM prediction": "", add col
    "FATHMM score": "", add col


    "Gene name": "EGFR", === col genes
 
    "HGNC ID": "3236",   add col (https://www.genenames.org/cgi-bin/gene_symbol_report?hgnc_id=HGNC:3236)

    "Mutation AA": "p.?del",  === amino_acid_change
    "Mutation CDS": "c.?",    === coding
    "Mutation Description": "Deletion - In frame",  === type
    "Mutation ID": "COSM13243",  === cosmic
    "Mutation genome position": "", example ("10:43114486-43114491") can get chr and locus

    "Pubmed_PMID": "22052230", === refs



# need to convert AA to longform