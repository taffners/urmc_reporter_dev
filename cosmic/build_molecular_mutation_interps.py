import json
import pymysql.cursors
import pymysql

from lib import utils
from lib import database

def RepresentsInt(s):
	try: 
		int(s)
		return True
	except ValueError:
		return False

def read_interpts_csv(interpt_file):
	'''

	'''
	conn = pymysql.connect(host='localhost',
	                  user='root',
	                  password='Taffne972832',
	                  db='ofa_dev',                          
	                  cursorclass=pymysql.cursors.DictCursor)

	db = database.database(conn)

	with open(interpt_file, 'r') as rf:

		line = rf.readline().rstrip() # ignore header
		line = rf.readline().rstrip()
		while line != '':

			split_line = line.split('"')
			knowledge_base_info = split_line[0].rstrip().split(',')
			genes = knowledge_base_info[0]
			coding = knowledge_base_info[1]
			amino_acid_change = knowledge_base_info[2]
			sql_date = split_line[-1].replace(',', '').lstrip().rstrip()
			
			# confirm date.  If all parts of the date are formated correctly to enter into db change ignore_date to False and add a time_stamp to insert statement
			ignore_date = True
			split_date = sql_date.split('-')
			# make sure len of split date is 
			if len(split_date) == 3:
				# first number needs to be a 4 int, between 2014 and 2018
				year = split_date[0]
				if RepresentsInt(year):
					year = int(year)
					if year >= 2014 and year <= 2018:
						# make sure middle number is an int between 1 and 12
						month = split_date[1]
						if RepresentsInt(month):
							month = int(month)
							if month >= 1 and month <= 12:
								# make sure date is an int between 1 and 31
								date = split_date[2]
								if RepresentsInt(date):
									date = int(date)
									if date >= 1 and date <= 31:
										ignore_date = False
		

			interpt = split_line[1]


			# Find knowledge_id for the genes,amino_acid_change,and coding
			search_var = {
			'genes': genes,
			'amino_acid_change': amino_acid_change,
			'coding': coding
			}

			# check if record in db
			search_result = db.listAll('find-var-in-knowledge-base', search_var)
			knowledge_id = search_result[0]['knowledge_id']

			# Query db to see if comment already is in db
			search_var = {
			'knowledge_id': knowledge_id,
			'comment': interpt
			}
			comment_result = db.listAll('comment-by-id-and-comment', search_var)
			
			# add if comment_result is empty
			if not comment_result:
				add_dict = {}
				add_dict['comment_ref'] = knowledge_id
				add_dict['comment_type'] = 'knowledge_base_table'
				add_dict['comment'] = interpt
				add_dict['user_id'] = '2'
				if not ignore_date:
					add_dict['time_stamp'] = sql_date

				

				db.addOrModifyRecord('comment_table', add_dict)





			line = rf.readline().rstrip()

if __name__ == "__main__":

	read_interpts_csv('../interpts.csv')