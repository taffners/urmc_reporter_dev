import json
import pymysql.cursors
import pymysql

from lib import utils
from lib import database

def get_current_gene_keep_list():
    

    genes_in_assays = []

    with open('genes_in_assays.csv', 'r') as giaf:

        line = giaf.readline().rstrip()

        while line != '':
            print line
            line = giaf.readline().rstrip() 


def genes_keep(curr_gene):
         
    # genes_in_assays =['ABL1', 'AKT1', 'AKT3', 'ALK', 'AR', 'AXL', 'BRAF', 'CCND1', 'CDH1', 'CDK4', 'CDK6', 'CTNNB1', 'DDR2', 'EGFR', 'ERBB2', 'ERBB3', 'ERBB4', 'ERG', 'ESR1', 'ETV1', 'ETV4', 'ETV5', 'FBXW7', 'FGFR1', 'FGFR2', 'FGFR3', 'FGFR4','FOXL2', 'GNA11','GNAS', 'GNAQ', 'HRAS', 'IDH1', 'IDH2', 'JAK1', 'JAK2', 'JAK3', 'KIT', 'KRAS', 'MAP2K1', 'MAP2K2', 'MET', 'MTOR', 'MYC', 'MYCN', 'NRAS', 'NTRK1', 'NTRK2', 'NTRK3', 'PDGFRA', 'PIK3CA', 'PPARG', 'RAF1', 'RET', 'ROS1', 'SMO', 'APC', 'SMAD4', 'SRC', 'STK11', 'TP53', 'HER2', 'MSH6', 'PTEN', 'ASXL1', 'BCOR', 'CSF3R', 'DNMT3A', 'ETV6', 'EZH2', 'FLT3', 'GATA2', 'MPL', 'MYD88', 'NOTCH1', 'NPM1', 'PHF6', 'PTPN11', 'RUNX1', 'SETBP1', 'SF3B1', 'SRSF2', 'STAG2', 'TET2', 'U2AF1', 'WT1', 'ZRSR2', 'ANKRD26', 'ATRX', 'BCORL1', 'BTK', 'CALR', 'CBL', 'CBLB', 'CBLC', 'CCND2', 'CDKN2A', 'CEBPA', 'CUX1', 'CXCR4', 'DCK', 'DDX41']     
    if curr_gene in genes_in_assays:
        return True
    else:
        return False

def convert_short_hand_AA(short_AA):

    AA_key_dict = {
        'A':'Ala',
        'C':'Cys',
        'D':'Asp',
        'E':'Glu',
        'F':'Phe',
        'G':'Gly',
        'H':'His',
        'I':'Ile',
        'K':'Lys',
        'L':'Leu',
        'M':'Met',
        'N':'Asn',
        'P':'Pro',
        'Q':'Gln',
        'R':'Arg',
        'S':'Ser',
        'T':'Thr',
        'V':'Val',
        'W':'Trp',
        'Y':'Tyr'
    }

    return_AA = ''

    short_keys = AA_key_dict.keys()

    for c in short_AA:
        if c in short_keys:
            return_AA += AA_key_dict[c]
        else:
            return_AA += c

    return return_AA

def getCosmicMutant():
    '''
        use sftp to download CosmicMutantExport
        https://cancer.sanger.ac.uk/cosmic/download

        email: samantha_taffner@urmc.rochester.edu
        pass: DefaultPassword123456

        loc file get /files/grch38/cosmic/v82/CosmicGenomeScreensMutantExport.tsv.gz
    '''
    pass

def convertKeyNamesToDbCol(input_dict):

    key_to_col_dict = {
        "Accession Number": 'transcript',
        "FATHMM prediction": "fathmm_prediction", 
        "FATHMM score": "fathmm_score", 
        "Gene name": "genes",
        "HGNC ID": "hgnc",
        "Mutation AA": "amino_acid_change", 
        "amino_acid_change_abbrev":"amino_acid_change_abbrev",
        "Mutation CDS": "coding",   
        "Mutation Description": "mutation_type",
        "Mutation ID": "cosmic",
        "Pubmed_PMID": "pmid", 
    }

    # get each of the keys from the to_col_dict from the input_dict and make an output dict with the value as the key
    cosmic_keys = key_to_col_dict.keys()
    return_dict = {}

    for k in cosmic_keys:

        new_key = key_to_col_dict[k]
        
        if k == 'Mutation ID':

            new_val = input_dict[k].replace('COSM', '')

        else:
            
            new_val = input_dict[k]


        return_dict[new_key] = new_val

    # now split up "Mutation genome position" example: (9:5022174-5022174) into chr and loc

    genome_pos = input_dict["Mutation genome position"]

    if genome_pos != '' and ':' in genome_pos:
        return_dict['chr'], return_dict['loc'] = genome_pos.split(':')

    else:
        return_dict['chr'] = ''
        return_dict['loc'] = ''


    # now add columns which are not being added to now so replacement happens
    return_dict['exon'] = ''
    return_dict['dbsnp'] = ''
    return_dict['pfam'] = ''


    return return_dict

def getDbColumns(mutant_export):
    '''

    '''
    # keep_cols = ['Gene_name', 'Primary_site', 'Mutation_ID', 'Mutation_CDS', 'Mutation_Description', 'FATHMM_prediction', 'FATHMM_score', 'Pubmed_PMID']

    # varchar_len = getLongestVarChar(mutant_export, keep_cols) + 1
    # col_link_i = {}

    conn = pymysql.connect(host='localhost',
                            user='root',
                            password='Taffne972832',
                            db='ofa_dev',                          
                            cursorclass=pymysql.cursors.DictCursor)

    db = database.database(conn)

    with open(mutant_export, 'r') as of:

        # get columns for the table
        keys_list = of.readline().rstrip().split('\t')
        line = of.readline().rstrip()
        count = 0

        while line != '':
            
            split_line = line.split('\t')

            temp_dict = utils.make_dict_from_two_lists(keys_list, split_line)

            # find if the current gene is in the a urmc assay
            keep_gene = genes_keep(temp_dict['Gene name'])

            # All Mutation Description = ['Substitution - coding silent', 'Substitution - Missense', 'Insertion - In frame', 'Deletion - In frame', 'Substitution - Nonsense', 'Unknown', 'Deletion - Frameshift', 'Insertion - Frameshift', 'Complex - frameshift', 'Complex - deletion inframe', 'Whole gene deletion', 'Complex - insertion inframe', 'Complex', 'Nonstop extension', 'Complex - compound substitution', 'Frameshift']

            if keep_gene and 'Deletion' not in temp_dict['Mutation Description'] and '?' not in temp_dict['Mutation CDS'] and '?' not in temp_dict['Mutation AA']:
                
                # add AA abbreviation
                temp_dict['amino_acid_change_abbrev'] = temp_dict['Mutation AA']

                # convert AA and update temp_dict
                temp_dict['Mutation AA'] = convert_short_hand_AA(temp_dict['Mutation AA'])

                
                # print json.dumps(temp_dict, indent=4, sort_keys=True) 
                count += 1             
              
                # get only the info needed in db col names
                temp_dict = convertKeyNamesToDbCol(temp_dict)

                search_var = {
                    'genes': temp_dict['genes'],
                    'amino_acid_change': temp_dict['amino_acid_change'],
                    'coding': temp_dict['coding']
                }
                # check if record in db
                search_result = db.listAll('find-var-in-knowledge-base', search_var)

                # it looks like the only pmid is the only thing that differs between all variants need to get pmid and concat the new pmid num if it doesn't already exist
                # find if not empty. if it is empty this var has not been added so add it
                if search_result:
                    # example of search_result
                    # [{u'knowledge_id': 1, u'loc': '25245350-25245350', u'fathmm_score': '.97875', u'cosmic': 'COSM521', u'coding': 'c.35G>A', u'notes': None, u'genes': 'KRAS', u'amino_acid_change': 'p.Gly12Asp', u'fathmm_prediction': 'PATHOGENIC', u'pfam': None, u'chr': '12', u'dbsnp': None, u'exon': None, u'location': None, u'time_stamp': datetime.datetime(2017, 10, 25, 12, 14, 28), u'pmid': '11215737', u'transcript': 'ENST00000311936', u'mutation_type': 'Substitution - Missense', u'hgnc': '6407'}]

                    #  need to get pmid and knowledge_id
                    db_pmid = search_result[0]['pmid']
                    db_knowledge_id =search_result[0]['knowledge_id']
                    
                    # find if the new pmid not in db_pmid. Otherwise just move on to next variant
                    if temp_dict['pmid'] not in db_pmid:
                        if db_pmid != '':
                            db_pmid += ','
                        db_pmid += temp_dict['pmid']

                        # replace new pmid with concat pmids
                        temp_dict['pmid'] = db_pmid

                        # now add the knowledge id so replacement occurs instead of adding a new record
                        temp_dict['knowledge_id'] = db_knowledge_id
                        
                        db.addOrModifyRecord('knowledge_base_table', temp_dict)

                else:
                    
                    db.addOrModifyRecord('knowledge_base_table', temp_dict)

            line = of.readline().rstrip()

    db.close_db_connect()

def getLongestVarChar(mutant_export,keep_cols):
    '''

    '''
    with open(mutant_export, 'r') as of:

        line = of.readline().rstrip()
        longest_len = 0

        while line != '':

            split_line = line.split('\t')


            for col_val in split_line:

                # if the length of the curr column value is longer than longest_len update it
                if len(col_val) > longest_len:
                    longest_len = len(col_val)

            line = of.readline().rstrip()

    print longest_len
    return longest_len

def add_tiers(mutant_export):
     '''


     '''
     ignore_cols = ['CGP Study', 'Sample Name', 'Census Gene', 'Drug Name', 'Genome Coordinates (GRCh38)',   'Zygosity', 'Somatic Status', 'Sample Source' , 'Sample ID', 'Transcript', 'AA Mutation', 'CDS Mutation', 'ID Mutation', 'Pubmed Id']

     with open(mutant_export, 'r') as rf:

          header = rf.readline().rstrip().split('\t')
          line = rf.readline().rstrip()
          histology_l = []
          while line != '':
               
               d = {}
               split_line = line.split('\t')
               gene = ''
               histology = ''
               for i in range(0,len(header)):
                    
                    if header[i] not in ignore_cols:
                         if header[i] == 'Gene Name':
                              gene = split_line[i]
                         elif header[i] == 'Tissue Subtype 2':
                              histology = split_line[i]
                              
                         d[header[i]] = split_line[i]


               if gene == 'EGFR':
                    if histology not in histology_l:
                         histology_l.append(histology)
                    # print json.dumps(d, indent=4, sort_keys=True)
               line = rf.readline().rstrip()
     # Primary Tissue ['soft_tissue', 'lung', 'haematopoietic_and_lymphoid_tissue', 'breast', 'skin', 'prostate', 'NS', 'thyroid', 'central_nervous_system', 'large_intestine']
     # Tissue Subtype 1['fibrous_tissue_and_uncertain_origin', 'NS', 'right_lower_lobe', 'right_upper_lobe', 'right_middle_lobe', 'shoulder', 'right', 'face', 'left_upper_lobe', 'left_bronchus', 'left', 'lymph_node', 'left_lower_lobe', 'brain', 'colon', 'leg']
     print histology_l

if __name__ == "__main__":

     # getDbColumns('../../cosmic/CosmicMutantExport.tsv')
    get_current_gene_keep_list()

     # add_tiers('../../cosmic/CosmicResistanceMutations_v83.tsv')

     # add_tiers('../../cosmic/CosmicMutantExport.tsv')
