import datetime

def is_list_nested(in_list):
	'''
		(list) -> bool

		Check if a list is nested.  Return true if it is nested.  Return false for unnested lists

		>>> (['SCH8S', 'SCH8N', ['SCH7N', 'SCH7S']]) -> True
		>>> (['S418', 'S428', 'S434', 'S514', 'SWCC6', 'SWCC7']) -> False
	'''
	return any(isinstance(i, list) for i in in_list)

def flatten_list(in_list):
	'''
		(list) -> list

		if input list is nested return flatten list.  If input list is not nested return input

		only works on single nested lists.  For higher nesting recursion will need to be implemented 
	'''

	# check if list is nested
	if is_list_nested(in_list):
		# flatten list
		flat_list = []
		
		for elm in in_list:
			
			# if the element is a list iterate over it otherwise just add elm
			if isinstance(elm, list):
				for sub_elm in elm:
					flat_list.append(sub_elm)
			else:
				flat_list.append(elm)
		return flat_list
	else:
		return in_list

def make_dict_from_two_lists(key_list, value_list):
	'''

	'''
	return dict(zip(key_list, value_list))

def make_dict_with_same_values(key_list, value):
	'''
		(list, any) -> dict		
	'''

	return_dict = {}

	for k in key_list:

		return_dict[k] = value

	return return_dict


def list_to_char_seperated(in_list, char, newline=True):
	'''
		Purpose:  Accept a list and return a char seperated str

		Newline is turned to False the list char will not be a newline
	'''
	char_line = ''
	len_list = len(in_list)

	for i in range(len_list):
		if i == len_list - 1 and not newline:
			char_line += '%s'%(in_list[i])
		elif i == len_list -1 and newline:
			char_line += '%s\n'%(in_list[i])
		else:
			char_line += '%s%s'%(in_list[i], char)
	return char_line		

def convert_db_date_format(in_date):
	'''
	change dates from MM/D/YYYY to YYYY-MM-DD
	'''
	if in_date != '':

		in_date = datetime.datetime.strptime(in_date, '%m/%d/%Y')

		return in_date.strftime('%Y-%m-%d')
	else:
		return in_date