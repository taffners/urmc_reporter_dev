import pymysql.cursors
import pymysql
from lib import utils

class database():

	def __init__(self, conn):
		self.conn = conn

	def close_db_connect(self):
		self.conn.close()

	def addOrModifyRecord(self, table, input_dict):
		'''
			(str, dict) -> int
			Add a row to a table in the database.
		'''

		count = 0
		replace = 'REPLACE '+ self.sanitize(table) + ' SET '
		in_names = input_dict.keys()

		# only include column if it is present in table
		cursor = self.conn.cursor()
		cursor.execute('SHOW columns FROM ' + table)

		# structure of table_dict 
		# [{u'Extra': u'auto_increment', u'Default': None, u'Field': u'knowledge_id', u'Key': u'PRI', u'Null': u'NO', u'Type': u'int(11)'}, ....]
		table_dict = cursor.fetchall()

		table_cols = []
		# get only Fields
		for col in table_dict:
			table_cols.append(col['Field'])

		for name in in_names:
			
			count += 1

			if name in table_cols:

				replace += self.sanitize(name) + ' = "' + self.sanitize(input_dict[name]) + '"'

				if count < len(input_dict):

					replace += ', '

		cursor.execute(replace)
		self.conn.commit()
		return cursor.lastrowid


	def listAll(self, query, where=''):

		if query == 'find-var-in-knowledge-base':
			select = '''
				SELECT  	* 

				FROM 	knowledge_base_table

				WHERE 	genes = "''' + self.sanitize(where['genes']) + '''" AND 
						amino_acid_change = "''' + self.sanitize(where['amino_acid_change']) + '''" AND
						coding = "''' + self.sanitize(where['coding']) + '''"
			'''

		elif query == 'comment-by-id-and-comment':
			select = '''
				SELECT 	*

				FROM 	comment_table

				WHERE 	comment_ref = "''' + self.sanitize(where['knowledge_id']) + '''" AND 
						comment = "''' + self.sanitize(where['comment']) + '''" 
			'''

		if select != '':
			results = []

			# perform query
			cursor = self.conn.cursor()
			cursor.execute(select)
			search_result = cursor.fetchall()

		return search_result


	def sanitize(self,dirty_string):
		dirty_string = str(dirty_string)
		no_ticks_string = dirty_string.replace('`', '')
		clean_string = pymysql.escape_string(no_ticks_string)
		return clean_string


