<?php

class WorkDays
{
    // https://stackoverflow.com/questions/336127/calculate-business-days?page=1&tab=active#tab-top
    private function getHolidays()
    {
        //from https://www.timeanddate.com/holidays/us/
        $holidaysData = '
        {   
            "URMC_Holidays":
            {                
                "events":
                [
                    {
                        "title":"New Year’s Day",
                        "date":"2017-01-01"
                    },
                    {
                        "title":"Martin Luther King Jr. Day",
                        "date":"2017-01-16"
                    },
                    {
                        "title":"Memorial Day",
                        "date":"2017-05-29"
                    },
                    {
                        "title":"Independence Day",
                        "date":"2017-07-04"
                    },
                    {
                        "title":"Labor Day",
                        "date":"2017-09-04"
                    },
                    {
                        "title":"Thanksgiving Day",
                        "date":"2017-11-23"
                    },
                    {
                        "title":"Thanksgiving Following Day",
                        "date":"2017-11-24"
                    },
                    {
                        "title":"Christmas Day",
                        "date":"2017-12-25"                       
                    },


                    {
                        "title":"New Year’s Day",
                        "date":"2017-01-01"
                    },
                    {
                        "title":"Martin Luther King Jr. Day",
                        "date":"2017-01-16"
                    },
                    {
                        "title":"Memorial Day",
                        "date":"2017-05-29"
                    },
                    {
                        "title":"Independence Day",
                        "date":"2017-07-04"
                    },
                    {
                        "title":"Labor Day",
                        "date":"2017-09-04"
                    },
                    {
                        "title":"Thanksgiving Day",
                        "date":"2017-11-23"
                    },
                    {
                        "title":"Thanksgiving Following Day",
                        "date":"2017-11-24"
                    },
                    {
                        "title":"Christmas Day",
                        "date":"2017-12-25"                    
                    },
                    {
                        "title":"New Year’s Day",
                        "date":"2018-01-01"
                    },
                    {
                        "title":"Martin Luther King Jr. Day",
                        "date":"2018-01-15"
                    },
                    {
                        "title":"Memorial Day",
                        "date":"2018-05-28"
                    },
                    {
                        "title":"Independence Day",
                        "date":"2018-07-04"
                    },
                    {
                        "title":"Labor Day",
                        "date":"2018-09-03"
                    },
                    {
                        "title":"Thanksgiving Day",
                        "date":"2018-11-22"
                    },
                    {
                        "title":"Thanksgiving Following Day",
                        "date":"2018-11-23"
                    },
                    {
                        "title":"Christmas Day",
                        "date":"2018-12-25"                        
                    },

                    {
                        "title":"New Year’s Day",
                        "date":"2019-01-01"
                    },
                    {
                        "title":"Martin Luther King Jr. Day",
                        "date":"2019-01-21"
                    },
                    {
                        "title":"Memorial Day",
                        "date":"2019-05-27"
                    },
                    {
                        "title":"Independence Day",
                        "date":"2019-07-04"
                    },
                    {
                        "title":"Labor Day",
                        "date":"2019-09-02"
                    },
                    {
                        "title":"Thanksgiving Day",
                        "date":"2019-11-28"
                    },
                    {
                        "title":"Thanksgiving Following Day",
                        "date":"2019-11-29"
                    },
                    {
                        "title":"Christmas Day",
                        "date":"2019-12-25"                        
                    },
                    {
                        "title":"New Year’s Day",
                        "date":"2020-01-01"
                    },
                    {
                        "title":"Martin Luther King Jr. Day",
                        "date":"2020-01-20"
                    },
                    {
                        "title":"Memorial Day",
                        "date":"2020-05-25"
                    },
                    {
                        "title":"Independence Day",
                        "date":"2020-07-04"
                    },
                    {
                        "title":"Labor Day",
                        "date":"2020-09-07"
                    },
                    {
                        "title":"Thanksgiving Day",
                        "date":"2020-11-26"
                    },
                    {
                        "title":"Thanksgiving Following Day",
                        "date":"2020-11-27"
                    },
                    {
                        "title":"Christmas Day",
                        "date":"2020-12-25"                        
                    },
                    {
                        "title":"New Year’s Day",
                        "date":"2021-01-01"
                    },
                    {
                        "title":"Martin Luther King Jr. Day",
                        "date":"2021-01-18"
                    },
                    {
                        "title":"Memorial Day",
                        "date":"2021-05-31"
                    },
                    {
                        "title":"Independence Day",
                        "date":"2021-07-04"
                    },
                    {
                        "title":"Labor Day",
                        "date":"2021-09-06"
                    },
                    {
                        "title":"Thanksgiving Day",
                        "date":"2021-11-25"
                    },
                    {
                        "title":"Thanksgiving Following Day",
                        "date":"2021-11-26"
                    },
                    {
                        "title":"Christmas Day",
                        "date":"2021-12-25"                        
                    },
                    {
                        "title":"New Year’s Day",
                        "date":"2022-01-01"
                    },
                    {
                        "title":"Martin Luther King Jr. Day",
                        "date":"2022-01-17"
                    },
                    {
                        "title":"Memorial Day",
                        "date":"2022-05-30"
                    },
                    {
                        "title":"Independence Day",
                        "date":"2022-07-04"
                    },
                    {
                        "title":"Labor Day",
                        "date":"2022-09-05"
                    },
                    {
                        "title":"Thanksgiving Day",
                        "date":"2022-11-24"
                    },
                    {
                        "title":"Thanksgiving Following Day",
                        "date":"2022-11-25"
                    },
                    {
                        "title":"Christmas Day",
                        "date":"2022-12-25"                        
                    }
                ]
            }
        }';

        $holidaysArray = json_decode($holidaysData, true);

        $holidays = [];
        foreach ($holidaysArray["URMC_Holidays"]['events'] as $event) 
        {
            $holidays[] = $event['date'];
        }

        return $holidays;
    }

    public function getWorkingDays($dateFrom, $dateTo)
    {
        $holidays = $this->getHolidays();
        $startDate = new DateTime($dateFrom);
        $endDate = new DateTime($dateTo);
        $interval = new DateInterval('P1D');
        $dateRange = new DatePeriod($startDate, $interval, $endDate);
        $results = [];
        foreach ($dateRange as $date) 
        {
            if ($date->format('N') < 6 && !in_array($date->format('Y-m-d'), $holidays)) 
            {
                $results[] = $date->format("Y-m-d");
            }
        }
        return $results;
    }

    public function numWorkDays($dateFrom, $dateTo)
    {
        $workDays = $this->getWorkingDays($dateFrom, $dateTo);
        return sizeOf($workDays);
    }

}



?>