<?php
	class DateFuncs
	{
		public function HumanReadableDateTime($unixTime)
		{
   			$dt = new DateTime("@$unixTime");
   			return $dt->format('Y-m-d H:i:s');
		}

		public function ChangeDateFormatUS($in_date, $check_array)
		{
			// Purpose: Convert date format from YYYY-MM-DD to MM/DD/YYYY

			$is_empty = $this->CheckEmptyDate($in_date, $check_array);

			return $is_empty ? '' : date('m/d/Y', strtotime($in_date));
		}

		private function CheckEmptyDate($in_date, $check_array)
		{
			// Purpose: check if date is equal to '' or '00/00/0000'.  If it is return true otherwise return false

			if($in_date === '' || $in_date === '0000-00-00' || $in_date === '00/00/0000')
			{
				return TRUE;
			}
			elseif($check_array)
			{
				return !isset($in_date);
			}
			else
			{
				return FALSE;
			}
		}

		public function ChangeDateFormatSQL($US_format_date)
		{
			// Purpose: Convert date format from MM/DD/YYYY to YYYY-MM-DD
			// (str) -> str
			// input MM/DD/YYYY
			// output YYYY-MM-DD

			if (empty($US_format_date))
			{
				return '';
			}

			return date('Y-m-d', strtotime($US_format_date));

		}

		public function convertToUTC($mysql_date_time)
		{
			// (2020-08-13 12:47:00) -> 2020-08-13T12:51:43


			$strtotime_date = strtotime($mysql_date_time);

			return date('Y-m-d\TH:i:s', $strtotime_date);
	
		}

		public function ChangeDateTimeFormatUS($in_date, $check_array)
		{
			
			$date_time = explode(' ', $in_date);
			$date = $date_time[0];
			$time = $date_time[1];

			return $this->ChangeDateFormatUS($date, $check_array).' '.$time;
		}

		public function CheckSQLDateFormat($in_date)
		{
			// Check if sql date format
			// '1970-01-01 12:00:00' > True
			$regex = '/\d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2}/';
			return preg_match($regex, $in_date);
		}

		public function CheckUSDateFormat($in_date)
		{
			// Check if sql date format
			// '05/31/2018 12:35:37' > True
			$regex = '/\d{2}[\/]\d{2}[\/]\d{4} \d{2}:\d{2}:\d{2}/';
			return preg_match($regex, $in_date);
		}

		public function ChangeDateTimeFormatSQL($in_date)
		{
			// Change date to SQL format or include a default value if empty or not a date 
			// 05/31/2018 12:35:37 -> 2018-05-31 12:35:37
			if (empty($in_date) || !$this->CheckUSDateFormat($in_date))
			{
				return DEFAULT_DATE_TIME;
			}

			else if ($this->CheckUSDateFormat($in_date))
			{
				$date_time = explode(' ', $in_date);
				$date = $date_time[0];
				$time = $date_time[1];

				return $this->ChangeDateFormatSQL($date).' '.$time;
			}
			
		}

	}

?>
