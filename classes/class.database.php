<?php

	class DataBase
	{
		private $conn;
		private $ip;
		private $ip_loc;

		// cryptor methods
		private $cipher = 'AES-128-CBC'; // advanced encryption standard
		private $cryp_key;
		private $logged_tables = array(
							'visit_table', 
							'visits_in_pool_table', 
							'pool_chip_linker_table',
							'run_info_table',
							'sample_log_book_table',
							'ordered_test_table',
							'ntc_table',
							'patient_table'
				);
	
		public function __construct($conn, $cryp_key, $site_title, $root_url)
		{
			$this->conn = $conn;
			$this->cryp_key = $cryp_key;
			$this->site_title = $site_title;
			$this->root_url = $root_url;
		}

		public function SetIP()
		{

			if(isset($_SERVER['HTTP_CLIENT_IP']))
			{
			// This will fetch the IP address when user is from Shared Internet services.
				$this->ip = $_SERVER['HTTP_CLIENT_IP'];
				
			}
			elseif (isset($_SERVER['REMOTE_ADDR']))
			{			
				// This contains the real IP address of the client. That is the most reliable value you can find from the user.
				$this->ip = $_SERVER['REMOTE_ADDR'];			
			}
			else
			{
				$this->ip = 'unknown';
			}	

			if ($this->ip == '::1')
			{
				$this->ip = '128.151.71.16';
			}	

			// get location of ip 
			// $this->ip_loc = $this->IPInfo($this->ip);
			$this->ip_loc = null;
			return $this->ip_loc;
			// if not in the Rochester, if unknown, if format not ip == logoff
		}

		public function IPInfo($purpose = "location") 
		{
			// Not using because the way the network is set up.
			$output = NULL;

			$purpose = str_replace(array("name", "\n", "\t", " ", "-", "_"), NULL, strtolower(trim($purpose)));
			$support    = array("country", "countrycode", "state", "region", "city", "location", "address");
			$continents = array(
				"AF" => "Africa",
				"AN" => "Antarctica",
				"AS" => "Asia",
				"EU" => "Europe",
				"OC" => "Australia (Oceania)",
				"NA" => "North America",
				"SA" => "South America"
			);

			if (filter_var($this->ip, FILTER_VALIDATE_IP) && in_array($purpose, $support)) 
			{
				// 
				$ipdat = @json_decode(file_get_contents("http://www.geoplugin.net/json.gp?ip=" . $this->ip));
			
				if (@strlen(trim($ipdat->geoplugin_countryCode)) == 2) 
				{
					switch ($purpose) 
					{
						case "location":
							$output = array(
							    "city"           => @$ipdat->geoplugin_city,
							    "state"          => @$ipdat->geoplugin_regionName,
							    "country"        => @$ipdat->geoplugin_countryName,
							    "country_code"   => @$ipdat->geoplugin_countryCode,
							    "continent"      => @$continents[strtoupper($ipdat->geoplugin_continentCode)],
							    "continent_code" => @$ipdat->geoplugin_continentCode,
							    "ip" 			 => $this->ip
							);
							var_dump($ipdat);
						break;

						case "address":
							$address = array($ipdat->geoplugin_countryName);
							if (@strlen($ipdat->geoplugin_regionName) >= 1)
							{
								$address[] = $ipdat->geoplugin_regionName;
							}
		    
							if (@strlen($ipdat->geoplugin_city) >= 1)
							{
								$address[] = $ipdat->geoplugin_city;
							}

							$output = implode(", ", array_reverse($address));
							break;
		
						case "city":
							$output = @$ipdat->geoplugin_city;
							break;
						
						case "state":
							$output = @$ipdat->geoplugin_regionName;
							break;
						
						case "region":
							$output = @$ipdat->geoplugin_regionName;
							break;
		
						case "country":
							$output = @$ipdat->geoplugin_countryName;
							break;
		
						case "countrycode":
							$output = @$ipdat->geoplugin_countryCode;
							break;
					}
				}
			}
		
			return $output;
		}

		public function AddLog($action, $msg, $details=NULL)
		{
			// Every action to the database needs to add a log to log_table
			// possible actions: 

			// What to log:
				// IP Address
				// user id
				// action (login, listAll, addOrModifyRecord, Delete)
				// msg
				// time_stamp

			if (defined('USER_ID'))
			{
				$user_id = $this->sanitize(USER_ID);				
			}	
			else
			{
				$user_id = 0;
			}		
			
			$log_insert = 'INSERT INTO log_table (user_id, ip_address, action, msg) VALUES';
			$log_insert .= '("'.$user_id.'", "';
			
			$log_insert .= isset($this->ip) ? $this->sanitize($this->ip): '';

			$log_insert .= '", "'.$this->sanitize($action).'", ';
			$log_insert .= '"'.$this->sanitize($msg).'")';

			$result = mysqli_query($this->conn, $log_insert);
			
			// get the Id of the inserted record
			$lastId = mysqli_insert_id($this->conn);


			// listAll, addOrModifyRecord, Delete will have more info in the detailed_log_table
			// make sure $details is not null
			if ($details !== NULL)
			{
				if (!is_string($details) && !is_int($details))
				{
					// convert $where to string
					$details = implode(',', $details);					
				}

				$details_insert = 'INSERT INTO detailed_log_table (details, log_id) VALUES';
				$details_insert .= '("'.$this->sanitize($details).'", ';
				$details_insert .= '"'.$this->sanitize($lastId).'") ';
				$result = mysqli_query($this->conn, $details_insert);
			}

			// see if this ip address has visited before
			// $ip_exist = $this->listAll('ip-info-exist',$this->ip);

			// // if the ip address has not visited before add the location to ip_info_table
			// 	// store ip info if not already in db in ip_info
			// 		// continent 
			// 		// country
			// 		// state
			// 		// city
			
			// if (empty($ip_exist))
			// {
			// 	$ip_loc_insert = 'INSERT INTO ip_info_table (ip_address, continent, country, state, city) VALUES';
			// 	$ip_loc_insert .= '("'.$this->sanitize($this->ip).'", ';
			// 	$ip_loc_insert .= '"'.$this->sanitize($this->ip_loc['continent']).'", ';
			// 	$ip_loc_insert .= '"'.$this->sanitize($this->ip_loc['country']).'", ';
			// 	$ip_loc_insert .= '"'.$this->sanitize($this->ip_loc['state']).'", ';
			// 	$ip_loc_insert .= '"'.$this->sanitize($this->ip_loc['city']).'") ';
			// 	$result = mysqli_query($this->conn, $ip_loc_insert);
			// }
		}

		public function GetTableColNames($table)
		{
			$cols = $this->listAll('get-table-col-names', $table);

			$collasped_cols = array();

			foreach ($cols as $key => $value)
			{

				array_push($collasped_cols, $value['COLUMN_NAME']);
			}

			return $collasped_cols;
		}

		public function updateRecord($table, $primary_key, $primary_key_val, $update_field, $update_val)
		{
			$update = 'UPDATE '.$this->sanitize($table).' SET '.$this->sanitize($update_field).' = "'.$this->sanitize($update_val).'"'.' WHERE '.$this->sanitize($primary_key).' = "'.$this->sanitize($primary_key_val).'"';

			$result = mysqli_query($this->conn, $update) or die('Oops!! Died ');

			// get the Id of the inserted record
			$lastId = mysqli_insert_id($this->conn);

			// add to log
			if ( in_array($table, $this->logged_tables))
			{
				$this->AddLog('updateRecord', $table, $update);	
			}

			return [$result, $lastId];
		}

		public function addOrModifyRecord($table, $inputs)
		{
			
			$count = 0;
			$replace = 'REPLACE '.$this->sanitize($table).' SET ';

			foreach($inputs as $name => $value)
			{
				// only include column if it is present in table
				$table_cols = $this->GetTableColNames($table);

				if (empty($table_cols))
				{
					die('Oops!! The following table is not found: ');
				}

				$count++;

				if (in_array($name, $table_cols))
				{
					// if field is null, type int, and allows nulls do not sanitize use null as value 
					if (empty($value) && $value !== 0)
					{
						if (!isset($colInfo))
						{
							$colInfo = $this->listAll('db-column-info', $table);
						}

						// find column info in array of all field info in table $colInfo.  It is done this way to reduce queries of db.
						if (isset($colInfo) && !empty($colInfo))
						{
							foreach ($colInfo as $key => $col)
							{
								if ($col['COLUMN_NAME'] === $name)
								{
									if 	(
											(
												$col['DATA_TYPE'] === 'float' ||
												$col['DATA_TYPE'] === 'int' ||
												$col['DATA_TYPE'] === 'decimal'
											)
											&&

											(
												$col['IS_NULLABLE'] === 'YES'
											)
										)
									{
										$replace .= $this->sanitize($name).' = NULL';
									}
									else
									{
										$replace .= $this->sanitize($name).' = "'.$this->sanitize($value).'"';
									}
									
									break;
								}
							}
						}
						else
						{
							$replace .= $this->sanitize($name).' = "'.$this->sanitize($value).'"';
						}
					}
					else
					{
						$replace .= $this->sanitize($name).' = "'.$this->sanitize($value).'"';
					}					

					if($count < sizeof($inputs))
					{
						$replace .= ', ';
					}
				}
			}
			// remove comma at the end of replace statement
			$replace = rtrim($replace, ', ');

			// add to log
			if ( in_array($table, $this->logged_tables))
			{
				$this->AddLog('addOrModifyRecord', $table, $replace);	
			}		

			$result = mysqli_query($this->conn, $replace) or die('Oops!! Died ');

			// get the Id of the inserted record
			$lastId = mysqli_insert_id($this->conn);

			return [$result, $lastId];
		}

		public function deleteRecord($table, $inputs)
		{
			$count = 0;
			$delete = 'DELETE FROM '.$this->sanitize($table).' WHERE ';

			foreach($inputs as $name => $value)
			{
				$delete .= $this->sanitize($name).' = "'.$this->sanitize($value).'"';

				$count++;

				if($count < sizeof($inputs))
				{
					$delete .= ' AND ';
				}
			}

			// add to log
			$this->AddLog('Delete', $table, $delete);

			$result = mysqli_query($this->conn, $delete) or die();
			$lastId = mysqli_insert_id($this->conn);

			return [$result, $lastId];
		}

		public function deleteRecordById($table, $id_name, $id)
		{
			$count = 0;
			$delete = 'DELETE FROM '.$this->sanitize($table).' WHERE '.$this->sanitize($id_name).' = '.$this->sanitize($id);

			// add to log
			$this->AddLog('Delete', $table, $delete);

			$result = mysqli_query($this->conn, $delete) or die();
			$lastId = mysqli_insert_id($this->conn);

			return [$result, $lastId];
		}

		public function listAll($query, $where=NULL)
		{
	
			switch($query)
			{	

				case 'curr-confirmations':
					$select =
					'
					SELECT 		*

					FROM 		confirmation_table ct 

					WHERE 		ct.observed_variant_id = "'.$this->sanitize($where).'"
					';
					break;

				case 'pending-confirmation-list':
					$select =
					'
					SELECT 		ct.*,
								rit.status AS report_status,
								rit.sample_name,
								ovt.genes,
								ovt.coding,
								ovt.amino_acid_change,
								otst.test_name,
								vt.order_num,
								vt.mol_num,
								vt.soft_lab_num

					FROM 		confirmation_table ct 

					LEFT JOIN 	run_info_table rit 
					ON 			rit.run_id = ct.run_id

					LEFT JOIN 	observed_variant_table ovt 
					ON 			ovt.observed_variant_id = ct.observed_variant_id

					LEFT JOIN 	ordered_test_table ott 
					ON 			ott.visit_id = ct.visit_id

					LEFT JOIN 	orderable_tests_table otst 
					ON 			otst.orderable_tests_id = ott.orderable_tests_id

					LEFT JOIN 	visit_table vt
					ON 			vt.visit_id = ct.visit_id

					WHERE 		ct.status = "pending"
					';
					break;


				case 'all-confirmation-list':
					$select =
					'
					SELECT 		ct.*,
								rit.status AS report_status,
								rit.sample_name,
								ovt.genes,
								ovt.coding,
								ovt.amino_acid_change,
								otst.test_name,
								vt.order_num,
								vt.mol_num,
								vt.soft_lab_num

					FROM 		confirmation_table ct 

					LEFT JOIN 	run_info_table rit 
					ON 			rit.run_id = ct.run_id

					LEFT JOIN 	observed_variant_table ovt 
					ON 			ovt.observed_variant_id = ct.observed_variant_id

					LEFT JOIN 	ordered_test_table ott 
					ON 			ott.visit_id = ct.visit_id

					LEFT JOIN 	orderable_tests_table otst 
					ON 			otst.orderable_tests_id = ott.orderable_tests_id

					LEFT JOIN 	visit_table vt
					ON 			vt.visit_id = ct.visit_id

					';
					break;


				case 'confirmation-by-id':
					$select = 
					'
					SELECT 		ct.*,
								rit.status AS report_status,
								rit.sample_name,
								ovt.genes,
								ovt.coding,
								ovt.amino_acid_change,
								otst.test_name,
								vt.order_num,
								vt.mol_num,
								vt.soft_lab_num,
								CONCAT(ut.first_name," " ,ut.last_name) AS confirmation_requester_name,
								ut.email_address AS confirmation_requester_email

					FROM 		confirmation_table ct 

					LEFT JOIN 	run_info_table rit 
					ON 			rit.run_id = ct.run_id

					LEFT JOIN 	observed_variant_table ovt 
					ON 			ovt.observed_variant_id = ct.observed_variant_id

					LEFT JOIN 	ordered_test_table ott 
					ON 			ott.visit_id = ct.visit_id

					LEFT JOIN 	visit_table vt
					ON 			vt.visit_id = ct.visit_id

					LEFT JOIN 	orderable_tests_table otst 
					ON 			otst.orderable_tests_id = ott.orderable_tests_id

					LEFT JOIN 	user_table ut 
					ON 			ut.user_id = ct.user_id 

					WHERE 		ct.confirmation_id = "'.$this->sanitize($where).'"
					';				
					break;

				case 'last-single-analyte-pool-run-number':
					$select =
					'
					SELECT 		sapt.run_number,
								sapt.start_date,
								sapt.status,
								CONCAT(ut.first_name," " ,ut.last_name) AS tech_name,
								otst.test_name

					FROM 		single_analyte_pool_table sapt

					LEFT JOIN 	user_table ut 
					ON 			ut.user_id = sapt.user_id

					LEFT JOIN 	orderable_tests_table otst
					ON 			sapt.orderable_tests_id = otst.orderable_tests_id

					WHERE 		sapt.orderable_tests_id = "'.$this->sanitize($where).'"

					ORDER BY 	sapt.start_date DESC,
								sapt.single_analyte_pool_id DESC 

					LIMIT 		1
					';
					break;

				case 'single-analyte-pool-by-id':
					$select =
					'
					SELECT 		sapt.status AS single_analyte_pool_status,
								CONCAT(sapt.run_number, "-", DATE_FORMAT(sapt.time_stamp, "%y")) AS single_analyte_pool_run_number

					FROM 		single_analyte_pool_table sapt

					WHERE 		sapt.single_analyte_pool_id = "'.$this->sanitize($where).'"
					';
					break;


				case 'all-sample-reflexes':
					$select = 
					'
					SELECT 		CONCAT(ut.first_name," " ,ut.last_name) AS user_name,
								rt.reflex_id,
								rt.reflex_name,
								ortt.sample_log_book_id,
								ortt.reflex_status,
								GROUP_CONCAT(CONCAT(ott.test_name," " ,slrxt.status) SEPARATOR ", ") AS link_status

					FROM 		ordered_reflex_test_table ortt 

					LEFT JOIN 	user_table ut
					ON 			ut.user_id = ortt.user_id

					LEFT JOIN 	reflex_table rt
					ON 			rt.reflex_id = ortt.reflex_id 

					LEFT JOIN 	status_linked_reflexes_xref_table slrxt
					ON 			slrxt.ordered_reflex_test_id = ortt.ordered_reflex_test_id

					LEFT JOIN 	reflex_test_link_table rtlt 
					ON 			rtlt.reflex_test_link_id = slrxt.reflex_test_link_id

					LEFT JOIN 	orderable_tests_table ott 
					ON 			ott.orderable_tests_id = rtlt.orderable_tests_id

					WHERE 		ortt.sample_log_book_id = "'.$this->sanitize($where).'"

					GROUP BY 	ortt.ordered_reflex_test_id

					';
					break;

				case 'reflex-link-sample-test':
					$select = 
					'
					SELECT 		rt.reflex_id,
								rt.reflex_name,
								ortt.ordered_reflex_test_id,
								ortt.sample_log_book_id,
								ortt.reflex_status,
								rtlt.reflex_value,
								rtlt.reflex_order,
								rtlt.reflex_test_link_id,
								rtlt.orderable_tests_id,
								slrxt.status_linked_reflexes_xref_id,
								slrxt.status AS link_status,
								ott.test_name

					FROM 		ordered_reflex_test_table ortt 

					JOIN 		status_linked_reflexes_xref_table slrxt 
					ON 			ortt.ordered_reflex_test_id = slrxt.ordered_reflex_test_id

					JOIN 		reflex_test_link_table rtlt 
					ON 			slrxt.reflex_test_link_id = rtlt.reflex_test_link_id 

					JOIN 		reflex_table rt
					ON 			rt.reflex_id = rtlt.reflex_id 	

					JOIN 		orderable_tests_table ott
					ON 			ott.orderable_tests_id = rtlt.orderable_tests_id		

					WHERE 		ortt.sample_log_book_id = "'.$this->sanitize($where['sample_log_book_id']).'" AND 		
								rtlt.orderable_tests_id = '.$this->sanitize($where['orderable_tests_id']).' AND
								ortt.reflex_status <> "complete"
					';			
					break;

				case 'nextReflexes':
					$select = 
					'
					SELECT 		rt.reflex_id,
								rt.reflex_name,
								ortt.sample_log_book_id,
								ortt.reflex_status,
								rtlt.reflex_value,
								rtlt.reflex_order,
								rtlt.reflex_test_link_id,
								rtlt.orderable_tests_id,
								slrxt.status_linked_reflexes_xref_id,
								slrxt.status AS link_status,
								ott.test_name

					FROM 		ordered_reflex_test_table ortt 

					JOIN 		status_linked_reflexes_xref_table slrxt 
					ON 			ortt.ordered_reflex_test_id = slrxt.ordered_reflex_test_id

					JOIN 		reflex_test_link_table rtlt 
					ON 			slrxt.reflex_test_link_id = rtlt.reflex_test_link_id 

					JOIN 		reflex_table rt
					ON 			rt.reflex_id = rtlt.reflex_id 	

					JOIN 		orderable_tests_table ott
					ON 			ott.orderable_tests_id = rtlt.orderable_tests_id		

					WHERE 		ortt.sample_log_book_id = "'.$this->sanitize($where['sample_log_book_id']).'" AND slrxt.status_linked_reflexes_xref_id <> '.$this->sanitize($where['status_linked_reflexes_xref_id']).' AND
								rtlt.reflex_order > '.$this->sanitize($where['reflex_order']).'
					
					ORDER BY 	 rtlt.reflex_order ASC
					';			
					break;

				case 'all-reflex-available':
					$select = 
					'
					SELECT 	*

					FROM 	reflex_table
					';
					break;		
				case 'get-all-active-users':
					$select = 
					'
					SELECT 	*,
							CONCAT(first_name," " ,last_name) AS user_name

					FROM 	user_table

					WHERE 	password_need_reset <> 2
					';
					break;

				case 'qc_db-all-bugs-pending':
					$select =
					'
					SELECT 		* 

					FROM 		bugs_reported_table

					WHERE 		status = "pending" AND 
								website_name = "'.$this->sanitize($where).'"
					';
					break;

				case 'qc_db-pending-maintenance':
					$select = 
					'
					SELECT 		* 

					FROM 		bugs_reported_table

					WHERE 		status = "pending" AND 
								website_name = "'.$this->sanitize($where).'" AND 
								report_type = "maintenance"
					';
					break;

				case 'qc_db-pending-maintenance-now':
					$select = 
					'
					SELECT 		* 

					FROM 		bugs_reported_table

					WHERE 		status = "pending" AND 
								website_name = "'.$this->sanitize($where).'" AND 
								report_type = "maintenance" AND
								downtime_occurs = "yes" AND
								now() between maintenance_start AND maintenance_end  


					';
					break;

				case 'qc_db-all-bugs':
					$select =
					'
					SELECT 		brt.*,
								bft.commit_id,
								bft.user_name AS fix_user_name,
								bft.summary_of_bug,
								bft.resulted_in_downtime,
								bft.downtime_type,
								TIMESTAMPDIFF(hour,bft.downtime_start_time,bft.downtime_end_time) AS downtime_in_hour

					FROM 		bugs_reported_table brt

					LEFT JOIN 	bug_fix_table bft 
					ON 			bft.bugs_reported_id = brt.bugs_reported_id

					WHERE 		brt.website_name = "'.$this->sanitize($where).'"
					';

					break;

				case 'qc_db-bug-by-id':
					$select = 
					'
					SELECT 		* 

					FROM 		bugs_reported_table

					WHERE 		bugs_reported_id = "'.$this->sanitize($where).'"
					';
					break;

				case 'qc_db-all-qc-pages':
					$select =
					'
					SELECT 		pt.page_id,
								pt.user_id,
								pt.user_name,
								pt.page_name,
								pt.page_description,
								pt.how_to_access_page,
								pt.page_assumption,
								pt.time_stamp,
								pt.website_name,
								pt.printing_necessary,
								pt.page_type,
								pt.permissions,
								qvpp_vw.num_validations,
								qvpp_vw.all_page_validation_ids,
								mt.module_name

					FROM 		page_table pt

					LEFT JOIN 	qc_validations_per_page_vw qvpp_vw
					ON 			qvpp_vw.page_id = pt.page_id 

					LEFT JOIN 	module_table mt
					ON 			mt.module_id = pt.module_id

					WHERE 		pt.website_name = "'.$this->sanitize($where).'"

					ORDER BY 	pt.page_name
					';
					break;

				case 'qc_db-all-page-qc-validations':
					$select =
					'
					SELECT 		pvt.*

					FROM 		page_validation_table pvt			

					WHERE 		pvt.page_id = "'.$this->sanitize($where).'"

					ORDER BY 	pvt.time_stamp DESC

					';
					break;

				case 'qc_db-all-modules':
					$select =
					'
					SELECT 		*

					FROM 		module_table
					';	
					break;

				case 'qc_db-module-by-id':
					$select =
					'
					SELECT 		*

					FROM 		module_table

					WHERE 		module_id = "'.$this->sanitize($where).'"
					';	
					break;

				case 'qc_db-all-last-validations':
					$select =
					'
					SELECT 		*, 
								max(time_stamp) 

					FROM 		page_validation_table 
					
					GROUP BY 	page_id
					';
				
					break;

				case 'qc_db-all-qc-checklists':
					$select =
					'
					SELECT 		*

					FROM 		check_list_validation_step_ordered_vw
					
					';			
					break;

				case 'qc_db-qc-page-info':
					$select =
					'
					SELECT 		pt.*,
								mt.module_name

					FROM 		page_table pt

					LEFT JOIN 	module_table mt
					ON 			pt.module_id = mt.module_id

					WHERE 		pt.page_id = "'.$this->sanitize($where).'"
					';
				
					break;

				case 'qc_db-qc-page-info-by-name':
					$select =
					'
					SELECT 	* 

					FROM 	page_table

					WHERE 	page_name = "'.$this->sanitize($where).'"
					';
					break;

				case 'qc_db-page-validation-info':
					$select = 
					'
					SELECT 	*

					FROM 	page_validation_table

					WHERE 	page_validation_id = "'.$this->sanitize($where).'"
					';
					break;
				 
				case 'qc_db-qc-checklist-item':
					$select =
					'
					SELECT 	* 

					FROM 	check_list_validation_step_table

					WHERE 	check_list_validation_step_id = "'.$this->sanitize($where).'"
					';
					break;

				case 'qc_db-qc-checklist-items-for-validation':
					$select =
					'
					SELECT 		pvst.page_validation_step_id,
								pvst.page_id,
								pvst.page_validation_id,
								pvst.actual_result,
								clvst.step_name,
								clvst.description,
								clvst.category 

					FROM 		page_validation_step_table pvst

					LEFT JOIN 	check_list_validation_step_table clvst
					ON 			clvst.check_list_validation_step_id = pvst.check_list_validation_step_id

					WHERE 		pvst.page_validation_id = "'.$this->sanitize($where).'"

					ORDER BY 	clvst.category
					';
					break;

				case 'lock-page-info':
					$select = 
					'
					SELECT 	*

					FROM 	lock_page_table

					WHERE 	page_lock_name = "'.$this->sanitize($where).'"
					';									
					break;

				case 'current-lock-page-records':
					$select =
					'
					SELECT 		lrt.*,
								UNIX_TIMESTAMP(now()) - UNIX_TIMESTAMP(lrt.time_stamp) AS seconds_diff, 
								now() as now,
								lpt.lock_seconds_length,
								lpt.lock_seconds_length - (UNIX_TIMESTAMP(now()) - UNIX_TIMESTAMP(lrt.time_stamp)) AS seconds_left,
								TIMESTAMPADD(SECOND, lpt.lock_seconds_length - (UNIX_TIMESTAMP(now()) - UNIX_TIMESTAMP(lrt.time_stamp)), now()) AS unlock_time,
								CONCAT(ut.first_name," " ,ut.last_name) AS user_name

					FROM 		lock_record_table lrt

					LEFT JOIN 	lock_page_table lpt 
					ON 			lpt.lock_page_id = lrt.lock_page_id

					LEFT JOIN 	user_table ut
					ON 			ut.user_id = lrt.user_id

					WHERE 		lrt.lock_page_id = '.$this->sanitize($where['lock_page_id']).' AND
								lrt.ref_id = '.$this->sanitize($where['ref_id']).'

					HAVING 		seconds_diff < lpt.lock_seconds_length

					ORDER BY 	lock_record_id DESC
					';	
					
					break;

				case 'tests-with-turnaround-time-tracked':
					$select = 
					'
					SELECT 	orderable_tests_id,
							test_name,
							full_test_name,
							turnaround_time_tracked

					FROM 	orderable_tests_table

					ORDER BY test_name ASC
					';
					break;

				case 'next-auto-increment-num':
					$select =
					'
					SELECT 	AUTO_INCREMENT 

					FROM 	information_schema.TABLES 

					WHERE 	TABLE_SCHEMA = "'.$this->sanitize($where['TABLE_SCHEMA']).'" AND 
							TABLE_NAME = "'.$this->sanitize($where['TABLE_NAME']).'";
					';
					break;
				case 'patient-visit-history':	
					$select = 
					'
					SELECT 		vt.patient_id, 
								COUNT(*) as visit_count, 
								pt.first_name, 
								pt.last_name


					FROM 		visit_table vt 

					LEFT JOIN 	patient_table pt 
					ON 			pt.patient_id = vt.patient_id 

					

					WHERE 		vt.patient_id = "'.$this->sanitize($where).'" AND 
								vt.run_id <> 0

					GROUP BY 	vt.patient_id 
					';
					break;

				case 'patient-by-visit-id':
					$select =
					'
					SELECT 		CONCAT(pt.first_name, " ", pt.last_name) as patient_name,
								vt.*,
								np.type AS test_name,
								ctt.type,
								ctt.control_name 

					FROM 		visit_table vt 

					LEFT JOIN 	patient_table pt 
					ON 			pt.patient_id = vt.patient_id

					LEFT JOIN 	ngs_panel np
					ON 			np.ngs_panel_id = vt.ngs_panel_id

					LEFT JOIN 	control_type_table ctt 
					ON 			ctt.control_type_id = vt.control_type_id

					WHERE 		vt.visit_id = "'.$this->sanitize($where).'"

					';
					break;

				case 'patient-timeline-history':
					$select =
					'
					SELECT 		ovt.run_id,
								ovt.observed_variant_id,
								vt.control_type_id,
								CONCAT(ovt.genes," " ,ovt.coding," ", ovt.amino_acid_change, " (", ovt.frequency, ")") AS mutation, 
								vt.time_stamp 

					FROM 		visit_table vt 

					LEFT JOIN 	observed_variant_table ovt 
					ON 			vt.run_id = ovt.run_id 

					WHERE 		vt.patient_id = "'.$this->sanitize($where).'" AND 
								vt.run_id <> 0 AND
								(
									vt.control_type_id = 0 OR 
									vt.control_type_id IS NULL
								);
					';				
					break;
				case 'library_pool_by_id':
					$select =
					'
					SELECT 	*

					FROM 	library_pool_table

					WHERE 	library_pool_id = "'.$this->sanitize($where).'"
					';
					break;
				case 'library-pool-qc-metrics':
					$select =
					'
					SELECT 	*

					FROM 	qc_run_metrics_table qrmt 

					WHERE 	qrmt.library_pool_id = "'.$this->sanitize($where).'"
					';
					break;
				case 'torrent-results-pool-chip-linker':
					$select = 
					'
					SELECT 		trt.*,
								pclt.library_pool_id_A,
								pclt.library_pool_id_B

					FROM 		torrent_results_table trt

					LEFT JOIN 	pool_chip_linker_table pclt 
					ON 			pclt.pool_chip_linker_id = trt.pool_chip_linker_id

					WHERE 		trt.pool_chip_linker_id = "'.$this->sanitize($where).'"
					';				
					break;

				case 'tech-test-performance-history':
					$select =
					'
					SELECT 	tth_vw.test_name, 
							tth_vw.task, 
							tth_vw.users_name, 
							ROUND(AVG(tth_vw.diff_turnaround_time), 1) AS avg_diff_from_expected_turnaround_time, 
							ROUND(AVG(actual_turnaround_time), 1) AS avg_actual_turnaround_time, 
							COUNT(test_name) AS count_test_tasks

					FROM 	tech_test_history_vw tth_vw';

					if (isset($where) && $where == '6_months')
					{
						$select.= ' WHERE tth_vw.time_stamp >= DATE_SUB(now(),INTERVAL  6 MONTH)';
					}
					else if (isset($where) && $where == '3_months')
					{
						$select.= ' WHERE tth_vw.time_stamp >= CURDATE() - INTERVAL  3 MONTH';
					}
					else if (isset($where) && $where == '1_month')
					{
						$select.= ' WHERE tth_vw.time_stamp >= CURDATE() - INTERVAL  1 MONTH';
					}
					else if (isset($where) && $where == '7days')
					{
						$select.= ' WHERE tth_vw.time_stamp >= CURDATE() - INTERVAL  7 DAY';
					}

					$select.=' 
					GROUP BY 	tth_vw.user_id, 
							tth_vw.task, 
							tth_vw.test_name

					ORDER BY 	tth_vw.users_name, 
							tth_vw.test_name, 
							tth_vw.task
					';
					break;

				case 'dna-extraction-per-tech':
					$select = 
					'
					SELECT 	AVG(elt.purity) AS avg_purity,
							COUNT(elt.purity) AS purity_count,
							MIN(elt.purity) AS purity_min,
							STDDEV(elt.purity) AS purity_stddev,
							AVG(stock_conc) AS avg_stock_conc,
							COUNT(stock_conc) AS count_stock_conc,
							CONCAT(ut.first_name," " ,ut.last_name) AS extractors_name,
							COUNT(extraction_log_id) AS total_count,
							ut.user_id

					FROM 	extraction_log_table elt 

					LEFT JOIN users_performed_task_table uptt 
					ON 		uptt.ref_id = elt.extraction_log_id AND
							uptt.ref_table = "extraction_log_table"

					LEFT JOIN user_table ut
					ON 		ut.user_id = uptt.user_id	

					WHERE 	ut.user_id IS NOT NULL AND 
							ut.user_id <> 1 AND
							ut.user_id <> 0';

					if (isset($where) && $where == '6_months')
					{
						$select.= ' AND elt.time_stamp >= DATE_SUB(now(),INTERVAL  6 MONTH)';
					}
					else if (isset($where) && $where == '3_months')
					{
						$select.= ' AND elt.time_stamp >= CURDATE() - INTERVAL  3 MONTH';
					}
					else if (isset($where) && $where == '1_month')
					{
						$select.= ' AND elt.time_stamp >= CURDATE() - INTERVAL  1 MONTH';
					}
					else if (isset($where) && $where == '7days')
					{
						$select.= ' AND elt.time_stamp >= CURDATE() - INTERVAL  7 DAY';
					}

					$select.= ' GROUP BY 	uptt.user_id';
					break;

				case 'lab-address-now':
					$select =
					'
					SELECT 	*

					FROM 	lab_address_table

					WHERE 	active_date <= "'.$this->sanitize($where).'"

					ORDER BY 	active_date DESC
					';
					break;

				case 'ordered-test-by-visit-id':
					$select =
					'
					SELECT 	*

					FROM 	ordered_test_table ott 

					WHERE 	ott.visit_id = "'.$this->sanitize($where).'"
					';				
					break;

				case 'mol-num-in-db':
					$select =
					'
					SELECT 	*

					FROM 	sample_log_book_table

					WHERE 	mol_num = "'.$this->sanitize($where).'"
					';
					break;

				case 'soft-path-num-in-db':
					$select = 
					'
					SELECT 	*

					FROM 	sample_log_book_table

					WHERE 	soft_path_num = "'.$this->sanitize($where).'"
					';
					break;

				case 'db-column-info':
					$select =
					'
					SELECT 	COLUMN_NAME, 
							DATA_TYPE, 
							CHARACTER_MAXIMUM_LENGTH,
							IS_NULLABLE 
					
					FROM 	information_schema.columns 
					WHERE 	table_schema = "'.DB_NAME.'" AND 
							table_name = "'.$this->sanitize($where).'" 
					';
					break; 
				case 'extraction-log-by-id':
					$select =
					'
					SELECT 	*

					FROM 	extraction_log_table 

					WHERE 	extraction_log_id = "'.$this->sanitize($where).'"
					';
					break;

				case 'extraction-log':
					$select =
					'					
					SELECT 	otst.test_name,
							elt.extraction_method,
							elt.stock_conc,
							elt.dilution_exist,
							elt.dilutions_conc,
							elt.extraction_log_id,
							elt.times_dilution,
							elt.purity,
							elt.vol_dna,
							elt.vol_eb,
							elt.dilution_final_vol,
							elt.elution_volume,
							elt.target_conc,
							elt.dna_conc_control_quant,
							elt.quantification_method,
							tpue_vw.user_tasks

					FROM 	ordered_test_table ott

					LEFT JOIN orderable_tests_table otst
					ON 		otst.orderable_tests_id = ott.orderable_tests_id

					LEFT JOIN tasks_per_users_extraction_vw tpue_vw 
					ON 		ott.ordered_test_id = tpue_vw.ordered_test_id

					LEFT JOIN extraction_log_table elt
					ON 		elt.ordered_test_id = ott.ordered_test_id

					WHERE 	ott.sample_log_book_id = "'.$this->sanitize($where).'" AND
							(otst.extraction_info = "yes" OR
							otst.quantification_info = "yes")
					';
					break;

				case 'pending-single-analyte-tests':
					$select =
					'
					SELECT 		slbt.*,
								ott.ordered_test_id, 
								ott.start_date,
								otst.test_name,
								ecps_vw.stock_concentrations,
								ecps_vw.dilution_concentrations,
								ecps_vw.extraction_count


					FROM 		ordered_test_table ott 

					LEFT JOIN 	sample_log_book_table slbt
					ON 			slbt.sample_log_book_id = ott.sample_log_book_id

					LEFT JOIN 	extraction_count_per_sample_vw ecps_vw 
					ON 			ecps_vw.sample_log_book_id = ott.sample_log_book_id

					LEFT JOIN 	orderable_tests_table otst
					ON 			otst.orderable_tests_id = ott.orderable_tests_id

					WHERE 		ott.orderable_tests_id = "'.$this->sanitize($where).'" AND 
								ott.test_status = "pending" 

					
					';
					break;

				case 'all-single-analyte-tests':
					$select = 
					'
					SELECT 		otst.test_name,
								otst.max_num_samples_per_run, 
								COUNT(ott.ordered_test_id) as count,
								otst.orderable_tests_id,
								att.assay_name 

					FROM 		ordered_test_table ott 

					LEFT JOIN 	orderable_tests_table otst 
					ON 			ott.orderable_tests_id = otst.orderable_tests_id  

					LEFT JOIN 	assay_type_table att
					ON 			att.assay_type_id = otst.assay_type_id

					WHERE 		ott.test_status = "pending" 

					GROUP BY 	otst.test_name,
								otst.orderable_tests_id
					';
					break;

				case 'pending-test-counts':
					$select =
					'
					SELECT 		otst.test_name, 
								COUNT(ott.ordered_test_id) as count,
								otst.orderable_tests_id 

					FROM 		ordered_test_table ott 

					LEFT JOIN 	orderable_tests_table otst 
					ON 			ott.orderable_tests_id = otst.orderable_tests_id  

					WHERE 		ott.test_status = "pending" 

					GROUP BY 	otst.test_name ASC

					ORDER BY 	count DESC
					';
					break;

				case 'test-in-single-analyte-pool':
					$select =
					'
					SELECT 		tipt.single_analyte_pool_id 

					FROM 		tests_in_pool_table tipt 

					WHERE 		tipt.ordered_test_id = "'.$this->sanitize($where).'"
					';
					break;

				case 'test-reporting-regulation-info':
					$select =
					'
					SELECT 	*

					FROM 	test_reporting_regulation_info_table trrit

					WHERE 	trrit.orderable_tests_id = "'.$this->sanitize($where).'"
					';
					break;

				case 'pending-single-analyte-pools':
					$select = 
					'
					SELECT 		sapt.*,
								CONCAT(ut.first_name," " ,ut.last_name) AS user_name,
								otst.test_name,
								GROUP_CONCAT(ct.comment SEPARATOR ", ") as comments,
								adisap_vw.all_assigned_directors

					FROM 		single_analyte_pool_table sapt 

					LEFT JOIN 	user_table ut
					ON 			ut.user_id = sapt.user_id

					LEFT JOIN 	assigned_directors_in_single_analyte_pool_vw adisap_vw
					ON 			adisap_vw.single_analyte_pool_id = sapt.single_analyte_pool_id

					LEFT JOIN 	orderable_tests_table otst
					ON 			otst.orderable_tests_id = sapt.orderable_tests_id

					LEFT JOIN 	comment_table ct
					ON 			sapt.single_analyte_pool_id = ct.comment_ref AND 
								ct.comment_type = "single_analyte_pool_table"

					WHERE 		sapt.status = "pending"

					GROUP BY 	ct.comment_ref,
								sapt.single_analyte_pool_id
					';					
					break;

				case 'pending-single-analyte-pools-my-pools':
					$select = 
					'
					SELECT 		sapt.*,
								CONCAT(ut.first_name," " ,ut.last_name) AS user_name,
								otst.test_name,
								GROUP_CONCAT(ct.comment SEPARATOR ", ") as comments,
								adisap_vw.all_assigned_directors

					FROM 		single_analyte_pool_table sapt 

					LEFT JOIN 	user_table ut
					ON 			ut.user_id = sapt.user_id

					LEFT JOIN 	assigned_directors_in_single_analyte_pool_vw adisap_vw
					ON 			adisap_vw.single_analyte_pool_id = sapt.single_analyte_pool_id

					LEFT JOIN 	orderable_tests_table otst
					ON 			otst.orderable_tests_id = sapt.orderable_tests_id

					LEFT JOIN 	comment_table ct
					ON 			sapt.single_analyte_pool_id = ct.comment_ref AND 
								ct.comment_type = "single_analyte_pool_table"

					WHERE 		sapt.status = "pending" AND
								sapt.user_id = "'.$this->sanitize($where).'" 

					GROUP BY 	ct.comment_ref,
								sapt.single_analyte_pool_id
					';					
					break;

				case 'pending-single-analyte-pools-reportable':
					$select = 
					'
					SELECT 		sapt.*,
								CONCAT(ut.first_name," " ,ut.last_name) AS user_name,
								otst.test_name,
								GROUP_CONCAT(ct.comment SEPARATOR ", ") as comments,
								adisap_vw.all_assigned_directors

					FROM 		single_analyte_pool_table sapt 

					LEFT JOIN 	user_table ut
					ON 			ut.user_id = sapt.user_id

					LEFT JOIN 	assigned_directors_in_single_analyte_pool_vw adisap_vw
					ON 			adisap_vw.single_analyte_pool_id = sapt.single_analyte_pool_id

					LEFT JOIN 	orderable_tests_table otst
					ON 			otst.orderable_tests_id = sapt.orderable_tests_id

					LEFT JOIN 	comment_table ct
					ON 			sapt.single_analyte_pool_id = ct.comment_ref AND 
								ct.comment_type = "single_analyte_pool_table"

					WHERE 		sapt.status = "pending" AND
								otst.infotrack_report = "yes"

					GROUP BY 	ct.comment_ref,
								sapt.single_analyte_pool_id
					';					
					break;

				case 'single-analyte-pool-view':
					$select = 
					'
					SELECT 		*

					FROM 		user_homepage_table 

					WHERE 		feature = "single_analyte_pool_view"  AND 
								user_id = "'.$this->sanitize($where).'" 
					';				
					break;

				case 'single-analyte-pool-view-all':
					$select = 
					'
					SELECT 		uht.*,
								CONCAT(ut.first_name," " ,ut.last_name) AS user_name

					FROM 		user_homepage_table uht 

					LEFT JOIN 	user_table ut
					ON 			ut.user_id = uht.user_id

					WHERE 		uht.feature = "single_analyte_pool_view"  
					';				
					break;

				case 'in-progress-test-counts':
					$select =
					'
					SELECT 		otst.test_name, 
								COUNT(ott.ordered_test_id) as count,
								otst.orderable_tests_id 

					FROM 		ordered_test_table ott 

					LEFT JOIN 	orderable_tests_table otst 
					ON 			ott.orderable_tests_id = otst.orderable_tests_id  

					WHERE 		ott.test_status = "In progress" 

					GROUP BY 	otst.test_name ASC

					ORDER BY 	count DESC
					';
					break;

				case 'task-id-by-task':
					$select =
					'
					SELECT 	tt.task_id

					FROM 	task_table tt

					WHERE 	tt.task = "'.$this->sanitize($where).'" 
					';
					break;

				case 'instrument-by-id':
					$select = 
					'
					SELECT 	it.*

					FROM 	instrument_table it 

					WHERE 	it.instrument_id = "'.$this->sanitize($where).'" 
					';
					break;

				case 'all-instrument-service-contracts':
					$select =
					'
					SELECT 	isct.instrument_service_contract_id,
							isct.expiration_date,
							isct.po,
							isct.description,
							isct.contract_num,
							isct.quote_num,
							isct.start_date,
							isct.price,
							DATEDIFF(isct.expiration_date, NOW()) AS days_left

					FROM 	instrument_service_contract_table isct

					WHERE 	isct.instrument_id = "'.$this->sanitize($where).'"

					ORDER BY 	isct.expiration_date DESC
					';							
					break;



				case 'instrument-service-contract-by-id':
					$select = 
					'
					SELECT 	isct.*

					FROM 	instrument_service_contract_table isct 

					WHERE 	isct.instrument_service_contract_id = "'.$this->sanitize($where).'" 
					';
					break;
				case 'required-unique-instrument-field':
					$select =
					'
					SELECT 	*

					FROM 	instrument_table

					WHERE 	'.$this->sanitize($where['field']).' = "'.$this->sanitize($where['val']).'"
					';		
					break;
				case 'tasks':
					$select =
					'
					SELECT DISTINCT 	it.task

					FROM 			instrument_table it 

					ORDER BY 			it.task
					';
					break;
				case 'manufacturers':
					$select =
					'
					SELECT DISTINCT 	it.manufacturer

					FROM 			instrument_table it 

					ORDER BY 			it.manufacturer
					';
					break;

				case 'all-validations-by-instrument-id':
					$select =
					'
					SELECT 	ivt.instrument_validation_id,
							ivt.instrument_id,
							ivt.validation_date,
							ivt.time_stamp,
							CONCAT(ut.first_name," " ,ut.last_name) AS user_name,
							GROUP_CONCAT(ivdt.name_uploaded_as SEPARATOR ", ") AS validation_docs

					FROM 	instrument_validation_table ivt 

					LEFT JOIN user_table ut 
					ON 		ut.user_id = ivt.user_id

					LEFT JOIN instrument_validation_doc_table ivdt 
					ON 		ivdt.instrument_validation_id = ivt.instrument_validation_id

					WHERE 	ivt.instrument_id = "'.$this->sanitize($where).'"

					GROUP BY 	ivdt.instrument_validation_id
					';
				
					break;

				case 'instrument-validations-by-val-date':
					$select =
					'
					SELECT 	*

					FROM 	instrument_validation_table ivt 

					WHERE 	ivt.validation_date = "'.$this->sanitize($where['validation_date']).'" AND
							ivt.instrument_id = "'.$this->sanitize($where['instrument_id']).'"
					';
					break;

				case 'validations-by-instrument_validation_id':
					$select =
					'
					SELECT 	ivt.*,
							GROUP_CONCAT(ivdt.name_uploaded_as SEPARATOR ", ") AS validation_docs	

					FROM 	instrument_validation_table ivt 

					LEFT JOIN instrument_validation_doc_table ivdt 
					ON 		ivdt.instrument_validation_id = ivt.instrument_validation_id

					WHERE 	ivt.instrument_validation_id = "'.$this->sanitize($where).'"
					';
					break;

				case 'get-instrument-info-by-instrument-validation':
					$select =
					'
					SELECT 	it.*,
							ivt.validation_date

					FROM 	instrument_validation_table ivt 

					LEFT JOIN instrument_table it 
					ON 		it.instrument_id = ivt.instrument_id

					WHERE 	ivt.instrument_validation_id = "'.$this->sanitize($where).'"
					';			
					break;

				case 'instrument-groups-description':
					$select =
					'
					SELECT 	it.*

					FROM 	instrument_table it 

					WHERE 	it.task LIKE "%'.$this->sanitize($where).'%" AND 
							it.instrument_status = "active" 

					ORDER BY 	it.yellow_tag_num
					';
					break;

				case 'instrument-used-in-single-analyte-pool':
					$select =
					'
					SELECT 		it.*,
								sapi_vw.sap_status

					FROM 		instrument_table it 

					LEFT JOIN 	single_analyte_pool_instruments_vw sapi_vw 
					ON 			sapi_vw.single_analyte_pool_id = "'.$this->sanitize($where['single_analyte_pool_id']).'" AND 	
								it.instrument_id = sapi_vw.instrument_id

					WHERE 	it.task LIKE "%'.$this->sanitize($where['instrument']).'%" AND 
							it.instrument_status = "active" 

					ORDER BY 	it.yellow_tag_num
					';				
					break;

				case 'instrument-groups-description-quantifiers':
					$select =
					'
					SELECT 	it.*

					FROM 	instrument_table it 

					WHERE 	(
								it.task = "Qubit" OR
								it.task = "Quantus" OR
								it.task = "Nanodrop" 
							) AND  
							it.instrument_status = "active" 

					ORDER BY 	it.yellow_tag_num
					';
					break;

				case 'all-instruments':
					$select =
					'
					SELECT 	it.*, 
							isct.max_expiry,
							isct.days_till_expiry,
							GROUP_CONCAT(ivt.instrument_validation_id SEPARATOR ", ") AS instrument_validation_ids
					
					FROM 	instrument_table it
					LEFT JOIN (
								SELECT 	instrument_id,
									 	DATEDIFF(MAX(expiration_date), NOW()) AS days_till_expiry, 
									 	MAX(expiration_date) AS max_expiry 

								FROM 	instrument_service_contract_table 
								GROUP BY 	instrument_id
							) isct
					ON 		isct.instrument_id = it.instrument_id 

					LEFT JOIN instrument_validation_table ivt
					ON 		ivt.instrument_id = it.instrument_id

					GROUP BY 	ivt.instrument_id,
							it.instrument_id,
							it.yellow_tag_num,
							it.task,
							it.manufacturer,
							it.serial_num,
							it.clinical_engineering_num,
							it.instrument_status,
							it.description,
							it.model

					';		
					break;
				case 'all-instruments-with-service-contract-info':
					$select =
					'
					SELECT 	it.*, 
							isct.max_expiry,
							isct.days_till_expiry,
							isct.expiration_date,
							isct.instrument_service_contract_id,
							GROUP_CONCAT(aet.automatic_email_type SEPARATOR ", ") AS email_types_sent

					FROM 	instrument_table it
					JOIN (
								SELECT 	instrument_id,
									 	DATEDIFF(MAX(expiration_date), NOW()) AS days_till_expiry, 
									 	MAX(expiration_date) AS max_expiry,
									 	expiration_date,
									 	instrument_service_contract_id 

								FROM 	instrument_service_contract_table 
								GROUP BY 	instrument_id
							) isct
					ON 		isct.instrument_id = it.instrument_id 

					LEFT JOIN automatic_email_table aet 
					ON 		aet.ref_id = isct.instrument_service_contract_id

					WHERE 	isct.days_till_expiry <= 90

					GROUP BY 	aet.ref_id				
					';		
					break;

				case 'auto-email-service-contract':
					$select 	=
					'
					SELECT 	isct.instrument_service_contract_id,
							isct.instrument_id,
							aet.automatic_email_type

					FROM 	instrument_service_contract_table isct 

					LEFT JOIN automatic_email_table aet 
					ON 		aet.ref_id = isct.instrument_service_contract_id AND
							aet.automatic_email_type = "'.$this->sanitize($where['automatic_email_type']).'"

					WHERE 	isct.expiration_date = "'.$this->sanitize($where['max_expiry']).'" 
					';
					break;
				case 'max-turn-around-time':
					$select =
					'
					SELECT 	MAX(turnaround_time) AS max_turnaround_time  

					FROM 	test_turnaround_table 

					WHERE 	orderable_tests_id = "'.$this->sanitize($where).'" 

					GROUP BY 	orderable_tests_id;
					';
					break;
				case 'turn-around-time':
					$select =
					'
					SELECT 	turnaround_time,
							dependent_result,
							status  

					FROM 	test_turnaround_table 

					WHERE 	orderable_tests_id = "'.$this->sanitize($where).'" 
					';
					break;

				case 'turn-around-time-by-dependent-result':
					$select =
					'
					SELECT 	*

					FROM 	test_turnaround_table 

					WHERE 	orderable_tests_id = "'.$this->sanitize($where['orderable_tests_id']).'" AND 
							dependent_result =  "'.$this->sanitize($where['dependent_result']).'"
					';
					break;

				case 'custom-field':
					$select = 
					'
					SELECT 	cft.field_type,
							cft.field_name,
							GROUP_CONCAT(cfot.option SEPARATOR ",") AS options

					FROM 	custom_field_table cft

					LEFT JOIN custom_field_options_table cfot
					ON 		cfot.custom_field_id = cft.custom_field_id

					WHERE 	cft.ref_table = "'.$this->sanitize($where['ref_table']).'" AND
							cft.ref_table_id = "'.$this->sanitize($where['ref_table_id']).'" 
					GROUP BY 	cfot.custom_field_id							
					';				
					break;

				case 'dna-conc-field-controller':
					$select =
					'
					SELECT 	npsrt.field_controller

					FROM 	ngs_panel_step_regulator_table npsrt 

					LEFT JOIN ngs_flow_steps_table nfst 
					ON 		nfst.ngs_flow_steps_id = npsrt.ngs_flow_steps_id

					WHERE 	npsrt.ngs_panel_id = "'.$this->sanitize($where['ngs_panel_id']).'" AND 
							nfst.step_regulator_name = "'.$this->sanitize($where['step_regulator_name']).'"
					';
					break;

				case 'on-panel-step-regulators':
					$select = 
					'
					SELECT 	GROUP_CONCAT(nfst.step_regulator_name SEPARATOR ", ") AS on_panel_steps

					FROM 	ngs_panel_step_regulator_table npsrt

					LEFT JOIN ngs_flow_steps_table nfst 
					ON 		nfst.ngs_flow_steps_id = npsrt.ngs_flow_steps_id

					WHERE 	npsrt.ngs_panel_id = "'.$this->sanitize($where).'" AND
							npsrt.status = "on"

					GROUP BY 	npsrt.ngs_panel_id
					';								
					break;

				case 'hapmap-vaf':
					$select =
					'
					SELECT 	ht.gene,
							ht.hapmap_sample,
							ht.chr,
							ht.pos,
							ht.vaf,
							npsrt.ngs_panel_id,
							npsrt.status AS hapmap_regulator_staus 

					FROM 	ngs_panel_step_regulator_table npsrt

					LEFT JOIN ngs_flow_steps_table nfst 
					ON 		nfst.ngs_flow_steps_id = npsrt.ngs_flow_steps_id

					LEFT JOIN hapmap_table ht
					ON 		ht.ngs_panel_id = npsrt.ngs_panel_id AND 
							ht.chr =  "'.$this->sanitize($where['chr']).'" AND
							ht.pos =  "'.$this->sanitize($where['pos']).'"

					WHERE 	npsrt.ngs_panel_id = "'.$this->sanitize($where['ngs_panel_id']).'" AND
							nfst.step_regulator_name = "hapmap"
		
					';							
					break;
				case 'step-regulator-status':
					$select =
					'
					SELECT 	npsrt.status

					FROM 	ngs_panel_step_regulator_table npsrt

					LEFT JOIN ngs_flow_steps_table nfst 
					ON 		nfst.ngs_flow_steps_id = npsrt.ngs_flow_steps_id

					WHERE 	npsrt.ngs_panel_id = "'.$this->sanitize($where['ngs_panel_id']).'" AND
							nfst.step_regulator_name = "'.$this->sanitize($where['step_regulator_name']).'"
					';
					break;

				case 'CAP-medical-record-nums':
					$select =
					'
					SELECT 	slbt.medical_record_num, 
							slbt.time_stamp 

					FROM 	sample_log_book_table slbt 

					WHERE 	slbt.sample_type_toggle = "cap_sample"

					ORDER BY 	slbt.time_stamp DESC
					';
					break;

				case 'all-assay-types':
					$select = 
					'
					SELECT 	*

					FROM assay_type_table 

					ORDER BY assay_name
					';
					break;

				case 'is-assay-type-ngs':
					$select = 
					'
					SELECT 	*,
							CASE 
								WHEN assay_name LIKE "%ngs%" THEN "yes"
								ELSE "no"
							END AS is_ngs_assay

					FROM assay_type_table 

					WHERE 	assay_type_id = "'.$this->sanitize($where).'"
					';
					break;

				case 'all-ngs-panels':
					$select =
					'
					SELECT 		ott.ngs_panel_id,
								ott.test_name

					FROM 		orderable_tests_table ott

					LEFT JOIN 	assay_type_table att
					ON 			att.assay_type_id = ott.assay_type_id

					WHERE 		att.assay_name = "ngs"
					';
					break;

				case 'orderable-tests-by-name':
					$select = 
					'
					SELECT 	*

					FROM 	orderable_tests_table

					WHERE 	test_name = "'.$this->sanitize($where['test_name']).'" OR
							full_test_name = "'.$this->sanitize($where['full_test_name']).'"
					';
					break;

				case 'orderable-tests-by-test-name-only':
					$select = 
					'
					SELECT 	*

					FROM 	orderable_tests_table

					WHERE 	test_name = "'.$this->sanitize($where).'" 
					';
					break;

				case 'all-test-types':
					$select = 
					'
					SELECT DISTINCT test_type 

					FROM 	orderable_tests_table

					ORDER BY 	test_type
					';
					break;

				case 'all-tests-available':
					$select =
					'
					SELECT 	ott.*,
							att.assay_name,
							CONCAT(ut.first_name," " ,ut.last_name) AS user_name,
							ott2.test_name AS dependent_test,
							tts_vw.avg_turnaround_time,
							tts_vw.max_turnaround_time,
							tts_vw.num_tests

					FROM 		orderable_tests_table ott 

					LEFT JOIN 	assay_type_table att 
					ON 			att.assay_type_id = ott.assay_type_id

					LEFT JOIN 	user_table ut
					ON 			ut.user_id = ott.user_id

					LEFT JOIN 	orderable_tests_table ott2 
					ON 			ott.dependency_orderable_tests_id = ott2.orderable_tests_id

					LEFT JOIN 	turnaround_time_test_stats_vw tts_vw 
					ON 			tts_vw.orderable_tests_id = ott.orderable_tests_id

					ORDER BY 	ott.test_name ASC
					';			
					break;

				case 'all-tests-available-active-tests-minus-sendouts':
					$select =
					'
					SELECT 	ott.*,
							att.assay_name,
							CONCAT(ut.first_name," " ,ut.last_name) AS user_name

					FROM 		orderable_tests_table ott 

					LEFT JOIN 	assay_type_table att 
					ON 			att.assay_type_id = ott.assay_type_id

					LEFT JOIN 	user_table ut
					ON 			ut.user_id = ott.user_id

					WHERE 		ott.send_out_test = "no"

					ORDER BY 	ott.test_name ASC
					';			
					break;
				case 'all-tests-available-with-results-reported-or-sendouts':
					$select =
					'
					SELECT 	ott.*,
							att.assay_name,
							CONCAT(ut.first_name," " ,ut.last_name) AS user_name
							
					FROM 	orderable_tests_table ott 

					LEFT JOIN assay_type_table att 
					ON 		att.assay_type_id = ott.assay_type_id

					LEFT JOIN user_table ut
					ON 		ut.user_id = ott.user_id

					WHERE 		ott.test_result = "yes" OR 
								ott.send_out_test = "yes" OR
								ott.mol_num_required = "yes"

					ORDER BY 	ott.test_name ASC
					';			
					break;

				case 'reflex':
					$select =
					'
					SELECT 	*

					FROM 	reflex_table

					WHERE 	reflex_id = "'.$this->sanitize($where).'"
					';
					break;

				case 'reflex-links':
					$select = 
					'	
					SELECT 		rtlt.*,
								ott.test_name

					FROM 		reflex_test_link_table rtlt

					LEFT JOIN 	orderable_tests_table ott 
					ON 			ott.orderable_tests_id = rtlt.orderable_tests_id

					WHERE 		rtlt.reflex_id = "'.$this->sanitize($where).'"

					ORDER BY 	rtlt.reflex_order
					';
					break;

				case 'reflex-by-name':
					$select =
					'
					SELECT 	*

					FROM 	reflex_table 

					WHERE 	reflex_name = "'.$this->sanitize($where).'"
					';
					break;

				case 'all-reflexes-available':
					$select =
					'
					SELECT 	* 

					FROM 	reflex_table
					';
					break;

				case 'orderable-tests':
					$select =
					'
					SELECT 	ott.*,
							att.assay_name

					FROM 	orderable_tests_table ott 

					LEFT JOIN assay_type_table att 
					ON 		att.assay_type_id = ott.assay_type_id

					WHERE 	ott.orderable_tests_id = "'.$this->sanitize($where).'"
					';
					break;

				case 'num-pending-tests-in-single-analyte-pool':
					$select =
					'
					SELECT 		COUNT(single_analyte_pool_id) AS count

					FROM 		tests_in_pool_table tipt

					LEFT JOIN 	ordered_test_table ott 
					ON 			ott.ordered_test_id = tipt.ordered_test_id

					WHERE 		tipt.single_analyte_pool_id = "'.$this->sanitize($where).'" AND
								(
									ott.test_status = "In Progress" OR 
									ott.test_status = "pending"
								)
								

					GROUP BY 	tipt.single_analyte_pool_id,
								ott.test_status
					';				
					break;

				case 'samples-in-single-analyte-pool':
					$select  =
					'
					SELECT 		tipt.*,
								ott.orderable_tests_id,
								ott.sample_log_book_id,
								ott.test_status,
								ott.start_date,
								ott.finish_date,
								ott.test_result,
								ott.visit_id,
								CONCAT(slbt.mol_num,"_" ,slbt.soft_lab_num) as sample_name,
								CONCAT(slbt.first_name, " ", slbt.last_name) as patient_name,
								slbt.medical_record_num,
								otst.test_name,
								otst.mol_num_required,
								otst.extraction_info,
								otst.quantification_info,
								att.assay_name,
								vt.run_id, 
								vt.patient_id,
								sapt.status,
								sapt.start_date,
								sapt.run_number,
								CONCAT(ut.first_name," " ,ut.last_name) AS pool_tech

					FROM 		tests_in_pool_table tipt

					LEFT JOIN 	single_analyte_pool_table sapt 
					ON 			sapt.single_analyte_pool_id = tipt.single_analyte_pool_id

					LEFT JOIN 	user_table ut
					ON 			ut.user_id = sapt.user_id

					LEFT JOIN 	ordered_test_table ott
					ON 			ott.ordered_test_id = tipt.ordered_test_id

					LEFT JOIN 	orderable_tests_table otst
					ON 			otst.orderable_tests_id = ott.orderable_tests_id

					LEFT JOIN 	sample_log_book_table slbt 
					ON 			slbt.sample_log_book_id = ott.sample_log_book_id

					LEFT JOIN 	assay_type_table att 
					ON 			att.assay_type_id = otst.assay_type_id

					LEFT JOIN 	visit_table vt
					ON 			vt.visit_id = ott.visit_id

					WHERE 		tipt.single_analyte_pool_id = "'.$this->sanitize($where).'"

					ORDER BY 	tipt.order_num
					';
					break;

				case 'observed-hotspots':
					$select =
					'
					SELECT 			tht.ordered_test_genetic_call,
									oht.observed_val,
									oht.observed_control_val

					FROM 			observed_hotspots_table oht 

					LEFT JOIN 		test_hotspots_table tht 
					ON 				tht.test_hotspots_id = oht.test_hotspots_id

					WHERE 			oht.ordered_test_id = "'.$this->sanitize($where).'"
					';
					break;

				case 'all-sample-tests':
					$select = 
					'
					SELECT 	ott.ordered_test_id,
							ott.orderable_tests_id,
							ott.sample_log_book_id,
							ott.visit_id,
							ott.soft_path_num,
							ott.outside_case_num,
							ott.consent_info,
							ott.consent_date,
							ott.consent_for_research,
							ott.start_date,
							ott.finish_date,
							ott.test_result,
							ott.expected_turnaround_time,
							ott.test_status,
							ott.time_stamp,
							ott.qc_status,
							ott.qc_overview,
							
							otst.test_name,
							otst.mol_num_required,
							otst.extraction_info,
							otst.quantification_info,
							otst.infotrack_report,
							otst.hotspot_variants,
							otst.hotspot_type,
							att.assay_name,
							ttcv.min_turnaround_time,
							ttcv.max_turnaround_time,
							GROUP_CONCAT(ct.comment SEPARATOR ", ") AS comments,
							vt.run_id,
							vt.ngs_panel_id,
							vt.patient_id,
							CONCAT(ut_dir.first_name, ut_dir.last_name) AS assigned_director_name

					FROM 	ordered_test_table ott 

					LEFT JOIN orderable_tests_table otst 
					ON 		otst.orderable_tests_id = ott.orderable_tests_id

					LEFT JOIN assay_type_table att 
					ON 		att.assay_type_id = otst.assay_type_id

					LEFT JOIN turnaround_time_concat_vw ttcv 
					ON 		otst.orderable_tests_id = ttcv.orderable_tests_id

					LEFT JOIN 	visit_table vt
					ON 			vt.visit_id = ott.visit_id 

					LEFT JOIN 	user_table ut_dir 
					ON 			ott.assigned_director_user_id = ut_dir.user_id 

					LEFT JOIN comment_table ct 
					ON 		ct.comment_ref = ott.ordered_test_id AND 
							ct.comment_type = "ordered_test_table"

					WHERE 	ott.sample_log_book_id = "'.$this->sanitize($where).'" 

					GROUP BY 	ct.comment_ref,
								ott.ordered_test_id,
								ott.orderable_tests_id,
								ott.sample_log_book_id,
								ott.visit_id,
								ott.soft_path_num,
								ott.outside_case_num,
								ott.consent_info,
								ott.consent_date,
								ott.consent_for_research,
								ott.start_date,
								ott.finish_date,
								ott.qc_status,
								ott.qc_overview,
								ott.expected_turnaround_time,
								ott.test_status,
								ott.time_stamp,
							
								otst.test_name,
								att.assay_name,
								ttcv.min_turnaround_time,
								ttcv.max_turnaround_time

					ORDER BY 	ott.start_date ASC
					';							
					break;

				case 'specific-sample-test':
					$select = 
					'
					SELECT 	ott.ordered_test_id,
							ott.orderable_tests_id,
							ott.sample_log_book_id,
							ott.visit_id,
							ott.soft_path_num,
							ott.outside_case_num,
							ott.consent_info,
							ott.consent_date,
							ott.consent_for_research,
							ott.start_date,
							ott.finish_date,
							ott.test_result,
							ott.expected_turnaround_time,
							ott.test_status,
							ott.time_stamp,
							ott.qc_status,
							ott.qc_overview,
							ppt.previous_positive_id,
							ppt.history_overview,
							ppt.history_mol_num,
							ppt.history_soft_path_num,
							ppt.history_test_tissue,
							ppt.history_date,
							otst.test_name,
							otst.mol_num_required,
							otst.extraction_info,
							otst.quantification_info,
							otst.infotrack_report,
							otst.hotspot_variants,
							otst.hotspot_type,
							att.assay_name,
							ttcv.min_turnaround_time,
							ttcv.max_turnaround_time,
							GROUP_CONCAT(ct.comment SEPARATOR ", ") AS comments,
							vt.run_id,
							vt.ngs_panel_id,
							vt.patient_id,
							CONCAT(ut_dir.first_name, ut_dir.last_name) AS assigned_director_name

					FROM 	ordered_test_table ott 

					LEFT JOIN orderable_tests_table otst 
					ON 		otst.orderable_tests_id = ott.orderable_tests_id

					LEFT JOIN assay_type_table att 
					ON 		att.assay_type_id = otst.assay_type_id

					LEFT JOIN previous_positive_table ppt
					ON 		ott.ordered_test_id = ppt.ordered_test_id

					LEFT JOIN turnaround_time_concat_vw ttcv 
					ON 		otst.orderable_tests_id = ttcv.orderable_tests_id

				 	LEFT JOIN 	visit_table vt
					ON 			vt.visit_id = ott.visit_id 

					LEFT JOIN 	user_table ut_dir 
					ON 			ott.assigned_director_user_id = ut_dir.user_id 

					LEFT JOIN comment_table ct 
					ON 		ct.comment_ref = ott.ordered_test_id AND 
							ct.comment_type = "ordered_test_table"

					WHERE 	ott.ordered_test_id = "'.$this->sanitize($where).'"

					GROUP BY 	ct.comment_ref,
								ott.ordered_test_id,
								ott.orderable_tests_id,
								ott.sample_log_book_id,
								ott.visit_id,
								ott.soft_path_num,
								ott.outside_case_num,
								ott.consent_info,
								ott.consent_date,
								ott.consent_for_research,
								ott.start_date,
								ott.finish_date,
								ott.qc_status,
								ott.qc_overview,
								ott.expected_turnaround_time,
								ott.test_status,
								ott.time_stamp,
								ppt.previous_positive_id,
								ppt.history_overview,
								ppt.history_mol_num,
								ppt.history_soft_path_num,
								ppt.history_test_tissue,
								ppt.history_date,
								otst.test_name,
								att.assay_name,
								ttcv.min_turnaround_time,
								ttcv.max_turnaround_time

					ORDER BY 	ott.start_date ASC
					';							
					break;

				case 'ordered-test-plus-sample-info':
					$select =
					'
					SELECT 		slbt.soft_lab_num,
								slbt.mol_num,
								slbt.medical_record_num,
								slbt.last_name,
								otst.test_name,
								CONCAT("Add comment to ",otst.test_name, ": ", slbt.soft_lab_num, ", ", slbt.mol_num, ", ", slbt.medical_record_num, ", ", slbt.last_name) AS page_title

					FROM 		ordered_test_table ott

					LEFT JOIN 	sample_log_book_table slbt 
					ON 			slbt.sample_log_book_id = ott.sample_log_book_id

					LEFT JOIN 	orderable_tests_table otst 
					ON 			otst.orderable_tests_id = ott.orderable_tests_id

					WHERE 		ott.ordered_test_id = "'.$this->sanitize($where).'"
					';
					break;

				case 'ordered-test-summary-info':
					$select = 
					'
					SELECT 	ott.ordered_test_id,
							ott.orderable_tests_id,
							ott.sample_log_book_id,
							ott.visit_id,
							ott.soft_path_num,
							ott.outside_case_num,
							ott.consent_info,
							ott.consent_date,
							ott.consent_for_research,
							ott.start_date,
							ott.finish_date,
							ott.test_result,
							ott.expected_turnaround_time,
							ott.test_status,
							ott.time_stamp,
							ott.qc_status,
							ott.qc_overview,
							ppt.previous_positive_id,
							ppt.history_overview,
							ppt.history_mol_num,
							ppt.history_soft_path_num,
							ppt.history_test_tissue,
							ppt.history_date,
							otst.test_name,
							otst.mol_num_required,
							otst.extraction_info,
							otst.quantification_info,
							otst.infotrack_report,
							otst.hotspot_variants,
							otst.hotspot_type,
							att.assay_name,
							ttcv.min_turnaround_time,
							ttcv.max_turnaround_time,
							GROUP_CONCAT(ct.comment SEPARATOR ", ") AS comments,
							vt.run_id,
							vt.ngs_panel_id,
							vt.patient_id,
							CONCAT(ut_dir.first_name, ut_dir.last_name) AS assigned_director_name

					FROM 	ordered_test_table ott 

					LEFT JOIN orderable_tests_table otst 
					ON 		otst.orderable_tests_id = ott.orderable_tests_id

					LEFT JOIN assay_type_table att 
					ON 		att.assay_type_id = otst.assay_type_id

					LEFT JOIN previous_positive_table ppt
					ON 		ott.ordered_test_id = ppt.ordered_test_id

					LEFT JOIN turnaround_time_concat_vw ttcv 
					ON 		otst.orderable_tests_id = ttcv.orderable_tests_id

					LEFT JOIN 	visit_table vt
					ON 			vt.visit_id = ott.visit_id

					LEFT JOIN 	user_table ut_dir 
					ON 			ott.assigned_director_user_id = ut_dir.user_id 

					LEFT JOIN comment_table ct 
					ON 		ct.comment_ref = ott.ordered_test_id AND 
							ct.comment_type = "ordered_test_table"

					WHERE 	ott.ordered_test_id = "'.$this->sanitize($where).'" 

					GROUP BY 	ct.comment_ref,
								ott.ordered_test_id,
								ott.orderable_tests_id,
								ott.sample_log_book_id,
								ott.visit_id,
								ott.soft_path_num,
								ott.outside_case_num,
								ott.consent_info,
								ott.consent_date,
								ott.consent_for_research,
								ott.start_date,
								ott.finish_date,
								
								ott.expected_turnaround_time,
								ott.test_status,
								ott.qc_status,
								ott.qc_overview,

								ott.time_stamp,
								ppt.previous_positive_id,
								ppt.history_overview,
								ppt.history_mol_num,
								ppt.history_soft_path_num,
								ppt.history_test_tissue,
								ppt.history_date,
								otst.test_name,
								att.assay_name,
								ttcv.min_turnaround_time,
								ttcv.max_turnaround_time

					ORDER BY 	ott.start_date ASC
					';							
					break;



				case 'extraction-users-tasks':
					$select =
					'
					SELECT 	elt.extraction_log_id,
							elt.extraction_method,
							elt.stock_conc,
							elt.dilution_exist,
							elt.dilutions_conc,
							elt.times_dilution,
							elt.target_conc,
							elt.purity,
							elt.vol_dna,
							elt.vol_eb,
							elt.dilution_final_vol,
							elt.elution_volume,
							elt.dna_conc_control_quant,
							elt.quantification_method,
							tt.task,
							GROUP_CONCAT(CONCAT(ut.first_name," " ,ut.last_name) SEPARATOR ", ") AS users,
							(
								SELECT 	GROUP_CONCAT("Yellow Tag:", it.yellow_tag_num SEPARATOR ", ") AS instruments

								FROM 	instrument_used_table iut

								LEFT JOIN instrument_table it
								ON 		it.instrument_id = iut.instrument_id

								WHERE 	iut.ref_id = elt.extraction_log_id AND 
										iut.ref_table = "extraction_log_table"

								GROUP BY 	iut.ref_id

							) AS instruments_used

					FROM 	extraction_log_table elt

					LEFT JOIN users_performed_task_table uptt
					ON 		uptt.ref_id = elt.extraction_log_id AND 
							uptt.ref_table = "extraction_log_table" 

					LEFT JOIN task_table tt 
					ON 		uptt.task_id = tt.task_id

					LEFT JOIN user_table ut
					ON 		ut.user_id = uptt.user_id

					WHERE 	elt.ordered_test_id = "'.$this->sanitize($where).'"

					GROUP BY 	tt.task
					';						
					break;

				case 'users-performed-tasks':
					$select =
					'
					SELECT 	tt.task,
							GROUP_CONCAT(CONCAT(ut.first_name," " ,ut.last_name) SEPARATOR ", ") AS users,
							(
								SELECT 	GROUP_CONCAT("Yellow Tag:", it.yellow_tag_num SEPARATOR ", ") AS instruments

								FROM 	instrument_used_table iut

								LEFT JOIN instrument_table it
								ON 		it.instrument_id = iut.instrument_id

								WHERE 	iut.ref_id = "'.$this->sanitize($where).'" AND 
										iut.ref_table = "ordered_test_table"

								GROUP BY 	iut.ref_id
								
							) AS instruments_used

					FROM 	users_performed_task_table uptt

					LEFT JOIN task_table tt 
					ON 		uptt.task_id = tt.task_id

					LEFT JOIN user_table ut
					ON 		ut.user_id = uptt.user_id

					WHERE 	uptt.ref_table = "ordered_test_table" AND 
							uptt.ref_id = "'.$this->sanitize($where).'"

					GROUP BY 	tt.task
					';
					break;

				case 'users-wet-bench-ngs-tasks':
					$select =
					'
					SELECT 		urtx.task,
								vt.visit_id,
								vt.run_id,
								vt.patient_id,
								vt.ngs_panel_id,
								CONCAT(ut.first_name," " ,ut.last_name) AS user_name

					FROM 		ordered_test_table ott 

					JOIN 		visit_table vt 
					ON 			ott.visit_id = vt.visit_id

					LEFT JOIN 	user_run_task_xref urtx 
					ON 			urtx.run_id = vt.run_id 

					LEFT JOIN 	user_table ut 
					ON 			ut.user_id = urtx.user_id

					WHERE 		ott.ordered_test_id = "'.$this->sanitize($where).'"
					';
					break;

				case 'visit-from-ordered-test-table-id':
					$select =
					'
					SELECT 		CONCAT(vt.mol_num,"_" ,vt.soft_lab_num) AS sample_name,
								vt.*

					FROM 		ordered_test_table ott 

					LEFT JOIN 	visit_table vt
					ON 			vt.visit_id = ott.visit_id


					WHERE 	ott.ordered_test_id = "'.$this->sanitize($where).'"
					';
					break;

				case 'ordered-test-only-by-id':
					$select =
					'
					SELECT 	* 

					FROM 	ordered_test_table

					WHERE 	ordered_test_id = "'.$this->sanitize($where).'"
					';
					break;

				case 'dependency-ordered-within-day-completed':
					$select =
					'
					SELECT 	ott.test_status, 
							ott.ordered_test_id, 
							ott.orderable_tests_id, 
							otst.test_name, 
							ott.start_date, 
							ott.finish_date   

					FROM 	ordered_test_table ott 

					LEFT JOIN orderable_tests_table otst 
					ON 		otst.orderable_tests_id = ott.orderable_tests_id 

					WHERE 	ott.sample_log_book_id = "'.$this->sanitize($where['sample_log_book_id']).'" AND
						ott.orderable_tests_id = "'.$this->sanitize($where['dependency_orderable_tests_id']).'" AND 
						"'.$this->sanitize($where['test_start_date']).'" >= DATE_SUB(ott.start_date, INTERVAL 1 DAY) AND
						"'.$this->sanitize($where['test_start_date']).'" = DATE_ADD(ott.start_date, INTERVAL 1 DAY) AND 
							ott.test_status = "complete"
					';
					break;

				case 'ordered-test-info-ngs':
					$select =
					'
					SELECT 	ott.ordered_test_id,
							ott.orderable_tests_id,
							ott.sample_log_book_id,
							ott.visit_id,
							ott.soft_path_num,
							ott.outside_case_num,
							ott.consent_info,
							ott.consent_date,
							ott.consent_for_research,
							ott.start_date,
							ott.finish_date,							
							ott.expected_turnaround_time,
							ott.test_status,
							ott.time_stamp,
							otst.previous_positive_required,
							otst.extraction_info,
							otst.quantification_info,
							otst.idylla_instruments,
							otst.hotspot_variants,
							otst.hotspot_type,
							otst.hotspot_val_type,
							
							otst.test_name,
							otst.full_test_name,
							otst.test_type,
							att.assay_name,
							CONCAT(ut.first_name," " ,ut.last_name) AS user_name,
							vtcc_vw.problems,
							vt.order_num

					FROM 	ordered_test_table ott 

					LEFT JOIN user_table ut
					ON 		ut.user_id = ott.user_id

					LEFT JOIN orderable_tests_table otst 
					ON 		otst.orderable_tests_id = ott.orderable_tests_id

					LEFT JOIN assay_type_table att 
					ON 		att.assay_type_id = otst.assay_type_id

					LEFT JOIN visit_table vt
					ON 		vt.visit_id = ott.visit_id

					LEFT JOIN visit_table_concat_comments_vw vtcc_vw
					ON 		vtcc_vw.visit_id = vt.visit_id

					WHERE 	ott.ordered_test_id = "'.$this->sanitize($where).'"
					';							
					break;

				case 'all-added-extra-fields':
					$select =
					'
					SELECT 	*

					FROM 	extra_tracked_table ett 

					WHERE 	ett.ref_id = "'.$this->sanitize($where['ref_id']).'" AND
							ett.ref_table = "'.$this->sanitize($where['ref_table']).'" 
					';
					break;

				case 'ordered-test-info':
					$select =
					'
					SELECT 		ott.ordered_test_id,
								ott.orderable_tests_id,
								ott.sample_log_book_id,
								ott.visit_id,
								ott.soft_path_num,
								ott.outside_case_num,
								ott.consent_info,
								ott.consent_date,
								ott.consent_for_research,
								ott.start_date,
								ott.finish_date,
								otst.previous_positive_required,
								otst.extraction_info,
								otst.quantification_info,
								otst.idylla_instruments,
								otst.hotspot_variants,
								otst.hotspot_type,
								otst.hotspot_val_type,
								ott.expected_turnaround_time,
								ott.test_status,
								ott.time_stamp,
								otst.test_name,
								otst.full_test_name,
								otst.test_type,
								att.assay_name,
								CONCAT(ut.first_name," " ,ut.last_name) AS user_name,
								ottcc_vw.problems

					FROM 		ordered_test_table ott 

					LEFT JOIN 	user_table ut
					ON 			ut.user_id = ott.user_id

					LEFT JOIN 	orderable_tests_table otst 
					ON 			otst.orderable_tests_id = ott.orderable_tests_id

					LEFT JOIN 	assay_type_table att 
					ON 			att.assay_type_id = otst.assay_type_id

					LEFT JOIN 	ordered_test_table_concat_comments_vw ottcc_vw
					ON 			ottcc_vw.ordered_test_id = ott.ordered_test_id

					WHERE 		ott.ordered_test_id = "'.$this->sanitize($where).'"
					';						
					break;

				case 'test-histories':
					$select =
					'
					SELECT 		*

					FROM 		previous_positive_table ppt 

					WHERE 		ppt.ordered_test_id = "'.$this->sanitize($where).'"
					';
					break;

				case 'all-idylla-variants':
					$select = 
					'
					SELECT DISTINCT 	tht.ordered_test_genetic_call,
										tht.genes,
										tht.orderable_tests_id

					FROM 				test_hotspots_table tht 

					WHERE 				tht.orderable_tests_id =  "'.$this->sanitize($where).'"
					';
					break;

				case 'all-hotspot-variants-no-duplicates':
					$select = 
					'
					SELECT DISTINCT tht.*
								
					FROM 		test_hotspots_table tht 

					WHERE 		tht.orderable_tests_id =  "'.$this->sanitize($where).'"
					';
					break;

				case 'all-hotspot-variants':
					$select = 
					'
					SELECT  	thgbotgc_vw.*

					FROM		test_hotspots_grouped_by_ordered_test_genetic_call_vw thgbotgc_vw 
				
					WHERE 		thgbotgc_vw.orderable_tests_id =  "'.$this->sanitize($where).'"
					';
					break;


				case 'all-hotspots-by-knowledge-base':
					$select = 
					'
					SELECT  	tht.*,
								otst.test_name,
								otst.infotrack_report,
								kbt.knowledge_id

					FROM 		test_hotspots_table tht 

					LEFT JOIN 	orderable_tests_table otst
					ON 			otst.orderable_tests_id = tht.orderable_tests_id

					LEFT JOIN 	knowledge_base_table kbt
					ON 			tht.genes = kbt.genes AND 
								tht.coding = kbt.coding AND 
								tht.amino_acid_change = kbt.amino_acid_change

					WHERE 		tht.orderable_tests_id =  "'.$this->sanitize($where).'"
					';					
					break;

				case 'ordered-test-concat-comments':
					$select =
					'
					SELECT 	* 

					FROM 	ordered_test_table_concat_comments_vw ottcc_vw 

					WHERE 	ottcc_vw.ordered_test_id = "'.$this->sanitize($where).'"
					';
					break;

				case 'patient-dob-sex-from-sample-log-book-id':
					$select = 
					'
					SELECT DISTINCT 	pt.dob,
										pt.sex

					FROM 	  			ordered_test_table ott

					LEFT JOIN 			visit_table vt 
					ON 					vt.visit_id = ott.visit_id

					LEFT JOIN 			patient_table pt
					ON 					pt.patient_id = vt.patient_id

					WHERE 				ott.sample_log_book_id = "'.$this->sanitize($where).'" AND
										ott.visit_id IS NOT NULL AND
										ott.visit_id <> 0
					';
					break;

				case 'all-samples-in-log-book-by-sample-log-book-id':
					$select = 
					'
					SELECT 	slbt.sample_log_book_id,
							slbt.possible_sample_types_id,
							slbt.first_name,
							slbt.last_name,
							slbt.soft_path_num,
							slbt.soft_lab_num,
							slbt.mol_num,
							slbt.medical_record_num,
							slbt.mpi,
							slbt.test_tissue,
							slbt.date_received,
							slbt.block,
							slbt.WBC_count,
							slbt.h_and_e_circled,
							slbt.tumor_cellularity,
							slbt.outside_case_num,
							slbt.dob,
							slbt.sex,
							CONCAT(ut2.first_name," " ,ut2.last_name) AS initial_login_user_name,
							slbt.time_stamp,
							CONCAT(ut.first_name," " ,ut.last_name) AS user_name,
							pstt.sample_type,
							slbccvw.problems_special_separator AS problems,
							otpsvw.ordered_tests,					
							otpsvw.ordered_tests_by_reserach_consent AS consent_for_research,
							otpsvw.ordered_tests_by_consent_received AS consent_recieved,
							otpsvw.ordered_test_ids,
							otpsvw.test_status_concat

					FROM 	sample_log_book_table slbt 

					LEFT JOIN ordered_tests_per_sample_vw otpsvw 
					ON 		otpsvw.sample_log_book_id = slbt.sample_log_book_id

					LEFT JOIN sample_log_book_concat_comments_vw slbccvw 
					ON 		slbccvw.sample_log_book_id = slbt.sample_log_book_id

					LEFT JOIN user_table ut
					ON 		slbt.user_id = ut.user_id
					
					LEFT JOIN user_table ut2
					ON 		slbt.initial_login_user_id = ut2.user_id

					LEFT JOIN possible_sample_types_table pstt 
					ON 		pstt.possible_sample_types_id = slbt.possible_sample_types_id

					WHERE 	slbt.sample_log_book_id = "'.$this->sanitize($where).'"

					ORDER BY 	slbt.date_received DESC
					';				
					break;

				case 'all-sample-visits':
					$select = 
					'
					SELECT 		vt.*,
								pt.*

					FROM 		ordered_test_table ott

					LEFT JOIN 	visit_table vt
					ON 			vt.visit_id = ott.visit_id

					LEFT JOIN 	patient_table pt
					ON 			pt.patient_id = vt.patient_id

					WHERE 		ott.sample_log_book_id = "'.$this->sanitize($where).'" AND
								ott.visit_id IS NOT NULL AND
								ott.visit_id != 0								
					';
					break;

				case 'num-completed-reports-per-patient':
					$select =
					'
					SELECT 		rit.run_id,
								vt.visit_id,
								vt.patient_id,
								rit.time_stamp

					FROM 		visit_table vt

					JOIN 		run_info_table rit
					ON 			rit.run_id = vt.run_id

					WHERE 		vt.patient_id = "'.$this->sanitize($where).'" AND
								rit.status = "completed"					
					';				
					break;

				case 'non-stop-samples-in-log-book-by-sample-log-book-id':
					$select = 
					'
					SELECT 		slbt.sample_log_book_id,
								slbt.possible_sample_types_id,
								slbt.first_name,
								slbt.last_name,
								slbt.soft_path_num,
								slbt.soft_lab_num,
								slbt.mol_num,
								slbt.medical_record_num,
								slbt.mpi,
								slbt.dob,
								slbt.sex,
								slbt.test_tissue,
								slbt.date_received,
								slbt.block,
								slbt.WBC_count,
								slbt.h_and_e_circled,
								slbt.tumor_cellularity,
								slbt.outside_case_num,
								ut2.first_name || " " || ut2.last_name AS initial_login_user_name,
								slbt.time_stamp,
								ut.first_name || " " || ut.last_name AS user_name,
								pstt.sample_type,
								slbccvw.problems_special_separator AS problems,
								nsotpsvw.ordered_tests,					
								nsotpsvw.ordered_tests_by_reserach_consent AS consent_for_research,
								nsotpsvw.ordered_tests_by_consent_received AS consent_recieved,
								nsotpsvw.ordered_test_ids,
								nsotpsvw.test_status_concat

					FROM 		sample_log_book_table slbt 

					LEFT JOIN 	non_stop_ordered_tests_per_sample_vw nsotpsvw 
					ON 			nsotpsvw.sample_log_book_id = slbt.sample_log_book_id

					LEFT JOIN 	sample_log_book_concat_comments_vw slbccvw 
					ON 			slbccvw.sample_log_book_id = slbt.sample_log_book_id

					LEFT JOIN 	user_table ut
					ON 			slbt.user_id = ut.user_id
					
					LEFT JOIN 	user_table ut2
					ON 			slbt.initial_login_user_id = ut2.user_id

					LEFT JOIN 	possible_sample_types_table pstt 
					ON 			pstt.possible_sample_types_id = slbt.possible_sample_types_id

					WHERE 		slbt.sample_log_book_id = "'.$this->sanitize($where).'"

					ORDER BY 	slbt.date_received DESC
					';									
					break;

				case 'all-samples-in-log-book':
					$select = 
					'
					SELECT 	slbt.sample_log_book_id,
							slbt.possible_sample_types_id,
							slbt.first_name,
							slbt.last_name,
							slbt.soft_path_num,
							slbt.soft_lab_num,
							slbt.mol_num,
							slbt.medical_record_num,
							slbt.mpi,
							slbt.test_tissue,
							slbt.date_received,
							slbt.block,
							slbt.WBC_count,
							slbt.h_and_e_circled,
							slbt.tumor_cellularity,
							slbt.outside_case_num,
							slbt.time_stamp,
							CONCAT(ut.first_name," " ,ut.last_name) AS user_name,
							pstt.sample_type,
							slbccvw.problems,
							otpsvw.ordered_tests,
							otpsvw.test_status_concat

					FROM 	sample_log_book_table slbt 

					LEFT JOIN ordered_tests_per_sample_vw otpsvw 
					ON 		otpsvw.sample_log_book_id = slbt.sample_log_book_id

					LEFT JOIN sample_log_book_concat_comments_vw slbccvw 
					ON 		slbccvw.sample_log_book_id = slbt.sample_log_book_id

					LEFT JOIN user_table ut
					ON 		slbt.user_id = ut.user_id

					LEFT JOIN possible_sample_types_table pstt 
					ON 		pstt.possible_sample_types_id = slbt.possible_sample_types_id

					ORDER BY 	slbt.sample_log_book_id DESC
					';							
					break;

				case 'search-samples-in-log-book':
					$select = 
					'
					SELECT 	slbt.sample_log_book_id,
							slbt.possible_sample_types_id,
							slbt.first_name,
							slbt.last_name,
							slbt.soft_path_num,
							slbt.soft_lab_num,
							slbt.mol_num,
							slbt.medical_record_num,
							slbt.mpi,
							slbt.test_tissue,
							slbt.date_received,
							slbt.block,
							slbt.WBC_count,
							slbt.h_and_e_circled,
							slbt.tumor_cellularity,
							slbt.outside_case_num,
							slbt.time_stamp,
							CONCAT(ut.first_name," " ,ut.last_name) AS user_name,
							pstt.sample_type,
							slbccvw.problems,
							otpsvw.ordered_tests,
							otpsvw.test_status_concat

					FROM 	sample_log_book_table slbt 

					LEFT JOIN ordered_tests_per_sample_vw otpsvw 
					ON 		otpsvw.sample_log_book_id = slbt.sample_log_book_id

					LEFT JOIN sample_log_book_concat_comments_vw slbccvw 
					ON 		slbccvw.sample_log_book_id = slbt.sample_log_book_id

					LEFT JOIN user_table ut
					ON 		slbt.user_id = ut.user_id

					LEFT JOIN possible_sample_types_table pstt 
					ON 		pstt.possible_sample_types_id = slbt.possible_sample_types_id';

					$whereSQL = ' WHERE ';
					if (isset($where['search']) && !empty($where['search']))
					{
						$whereSQL.='( ';
						foreach (explode(' ', $where['search']) as $key => $val)
						{
								if ($whereSQL != ' WHERE ( ')
								{
									$whereSQL.= ' OR ';
								}
								
								$whereSQL.=' slbt.sample_log_book_id LIKE "%'.$this->sanitize($val).'%" OR';
								$whereSQL.=' slbt.possible_sample_types_id LIKE "%'.$this->sanitize($val).'%" OR';
								$whereSQL.=' slbt.first_name LIKE "%'.$this->sanitize($val).'%" OR';
								$whereSQL.=' slbt.last_name LIKE "%'.$this->sanitize($val).'%" OR';
								$whereSQL.=' slbt.soft_path_num LIKE "%'.$this->sanitize($val).'%" OR';
								$whereSQL.=' slbt.soft_lab_num LIKE "%'.$this->sanitize($val).'%" OR';
								$whereSQL.=' slbt.mol_num LIKE "%'.$this->sanitize($val).'%" OR';
								$whereSQL.=' slbt.medical_record_num LIKE "%'.$this->sanitize($val).'%" OR';
								$whereSQL.=' slbt.mpi LIKE "%'.$this->sanitize($val).'%" OR';
								$whereSQL.=' slbt.test_tissue LIKE "%'.$this->sanitize($val).'%" OR';
								$whereSQL.=' slbt.date_received LIKE "%'.$this->sanitize($val).'%" OR';
								$whereSQL.=' slbt.block LIKE "%'.$this->sanitize($val).'%" OR';
								$whereSQL.=' slbt.WBC_count LIKE "%'.$this->sanitize($val).'%" OR';
								$whereSQL.=' slbt.h_and_e_circled LIKE "%'.$this->sanitize($val).'%" OR';
								$whereSQL.=' slbt.tumor_cellularity LIKE "%'.$this->sanitize($val).'%" OR';
								$whereSQL.=' slbt.outside_case_num LIKE "%'.$this->sanitize($val).'%" OR';
								
								$whereSQL.=' pstt.sample_type LIKE "%'.$this->sanitize($val).'%" OR';
								$whereSQL.=' slbccvw.problems LIKE "%'.$this->sanitize($val).'%" OR';
								$whereSQL.=' otpsvw.ordered_tests LIKE "%'.$this->sanitize($val).'%" OR';
								$whereSQL.=' otpsvw.test_status_concat LIKE "%'.$this->sanitize($val).'%"';
						}
						$whereSQL.=' ) ';
					}

					if (isset($where['search_start_date']) && !empty($where['search_start_date']))
					{
						if ($whereSQL !== ' WHERE ')
						{
							$whereSQL.= ' AND ';
						}
						$whereSQL.= 'DATE(slbt.date_received) >= DATE("'.$where['search_start_date'].'") ';

					}

					if (isset($where['search_end_date']) && !empty($where['search_end_date']))
					{
						if ($whereSQL !== ' WHERE ')
						{
							$whereSQL.= ' AND ';
						}
						$whereSQL.= 'DATE(slbt.date_received) <= DATE("'.$where['search_end_date'].'") ';

					}

					$select.=$whereSQL.' ORDER BY 	slbt.sample_log_book_id DESC';									
						
					break;

				case 'filter-no-tests-ordered-samples-in-log-book':
					$select = 
					'
					SELECT 	slbt.sample_log_book_id,
							slbt.possible_sample_types_id,
							slbt.first_name,
							slbt.last_name,
							slbt.soft_path_num,
							slbt.soft_lab_num,
							slbt.mol_num,
							slbt.medical_record_num,
							slbt.mpi,
							slbt.test_tissue,
							slbt.date_received,
							slbt.block,
							slbt.WBC_count,
							slbt.h_and_e_circled,
							slbt.tumor_cellularity,
							slbt.outside_case_num,
							slbt.time_stamp,
							CONCAT(ut.first_name," " ,ut.last_name) AS user_name,
							pstt.sample_type,
							slbccvw.problems,
							otpsvw.ordered_tests,
							otpsvw.test_status_concat,
							orpsvw.ordered_reflexes,
							orpsvw.reflex_status_concat
							
					FROM 	sample_log_book_table slbt 

					LEFT JOIN 	ordered_tests_per_sample_vw otpsvw 
					ON 			otpsvw.sample_log_book_id = slbt.sample_log_book_id

					LEFT JOIN 	user_table ut
					ON 			slbt.user_id = ut.user_id

					LEFT JOIN 	sample_log_book_concat_comments_vw slbccvw 
					ON 			slbccvw.sample_log_book_id = slbt.sample_log_book_id

					LEFT JOIN 	possible_sample_types_table pstt 
					ON 			pstt.possible_sample_types_id = slbt.possible_sample_types_id
					
					LEFT JOIN 	ordered_reflexes_per_sample_vw orpsvw 
					ON 			orpsvw.sample_log_book_id = slbt.sample_log_book_id

					WHERE 		otpsvw.ordered_tests = "DNA Extraction" OR
								otpsvw.ordered_tests = "Nanodrop"

					ORDER BY 	slbt.sample_log_book_id DESC
					';
					break;


				case 'filtered-samples-in-log-book':
					$select = 
					'
					SELECT 	slbt.sample_log_book_id,
							slbt.possible_sample_types_id,
							slbt.first_name,
							slbt.last_name,
							slbt.soft_path_num,
							slbt.soft_lab_num,
							slbt.mol_num,
							slbt.medical_record_num,
							slbt.mpi,
							slbt.test_tissue,
							slbt.date_received,
							slbt.block,
							slbt.WBC_count,
							slbt.h_and_e_circled,
							slbt.tumor_cellularity,
							slbt.outside_case_num,
							slbt.time_stamp,
							CONCAT(ut.first_name," " ,ut.last_name) AS user_name,
							pstt.sample_type,
							slbccvw.problems,
							otpsvw.ordered_tests,
							otpsvw.test_status_concat,
							orpsvw.ordered_reflexes,
							orpsvw.reflex_status_concat
							
					FROM 	sample_log_book_table slbt 

					LEFT JOIN 	ordered_tests_per_sample_vw otpsvw 
					ON 			otpsvw.sample_log_book_id = slbt.sample_log_book_id

					LEFT JOIN 	user_table ut
					ON 			slbt.user_id = ut.user_id

					LEFT JOIN 	sample_log_book_concat_comments_vw slbccvw 
					ON 			slbccvw.sample_log_book_id = slbt.sample_log_book_id

					LEFT JOIN 	possible_sample_types_table pstt 
					ON 			pstt.possible_sample_types_id = slbt.possible_sample_types_id
					
					LEFT JOIN 	ordered_reflexes_per_sample_vw orpsvw 
					ON 			orpsvw.sample_log_book_id = slbt.sample_log_book_id

					HAVING 		test_status_concat LIKE "%'.$this->sanitize($where).'%"

					ORDER BY 	slbt.sample_log_book_id DESC
					';
					break;

				case 'in-progress-tests':
					$select = 
					'
					SELECT DISTINCT 	otst.orderable_tests_id,
										otst.test_name			
					
					FROM 		ordered_test_table ott 

					LEFT JOIN 	orderable_tests_table otst
					ON 			otst.orderable_tests_id = ott.orderable_tests_id

					WHERE 		ott.test_status = "In Progress"

					ORDER BY 	otst.test_name ASC
					';
					break;

				case 'ordered-tests-in-progress':
					$select = 
					'
					SELECT 	slbt.sample_log_book_id,
							slbt.possible_sample_types_id,
							slbt.first_name,
							slbt.last_name,
							CONCAT(SUBSTRING(slbt.first_name, 1,1), "", SUBSTRING(slbt.last_name, 1,1)) AS patient_initials,
							slbt.soft_path_num,
							slbt.soft_lab_num,
							slbt.mol_num,
							slbt.medical_record_num,
							slbt.mpi,
							slbt.test_tissue,
							slbt.date_received,
							slbt.block,
							slbt.WBC_count,
							slbt.h_and_e_circled,
							slbt.tumor_cellularity,
							slbt.outside_case_num,
							slbt.time_stamp,
							otst.orderable_tests_id,
							otst.test_name,
							vt.visit_id,
							vt.order_num
			
					
					FROM 		ordered_test_table ott 

					LEFT JOIN 	sample_log_book_table slbt 
					ON 			slbt.sample_log_book_id	= ott.sample_log_book_id

					LEFT JOIN 	orderable_tests_table otst
					ON 			otst.orderable_tests_id = ott.orderable_tests_id

					LEFT JOIN 	visit_table vt
					ON 			vt.visit_id = ott.visit_id

					WHERE 		ott.test_status = "In Progress"';

					if (isset($where))
					{
						$select.= ' AND ott.orderable_tests_id = "'.$this->sanitize($where).'"';
					}

					$select.=' 	ORDER BY 	slbt.date_received
					';
					break;

				case 'patient-tests-history-mrn':
					$select = 
					'
					SELECT 			slbt1.sample_log_book_id,
									"MRN: '.$this->sanitize($where['medical_record_num']).'" AS search_criteria,
									otpsvw1.ordered_tests,
									otpsvw1.test_status_concat,
									otpsvw1.ordered_tests_by_test_result,
									slbt1.first_name,
									slbt1.last_name,
									slbt1.soft_path_num,
									slbt1.soft_lab_num,
									slbt1.mol_num,
									slbt1.medical_record_num,
									slbt1.mpi,
									slbt1.test_tissue,
									slbt1.date_received,
									slbccvw1.problems,
									slbt1.block,
									slbt1.WBC_count,
									slbt1.h_and_e_circled,
									slbt1.tumor_cellularity,
									slbt1.outside_case_num,
									pstt1.sample_type
									
					FROM 			sample_log_book_table slbt1 

					LEFT JOIN 		ordered_tests_per_sample_vw otpsvw1
					ON 				otpsvw1.sample_log_book_id = slbt1.sample_log_book_id

					LEFT JOIN 		sample_log_book_concat_comments_vw slbccvw1 
					ON 				slbccvw1.sample_log_book_id = slbt1.sample_log_book_id

					LEFT JOIN 		possible_sample_types_table pstt1 
					ON 				pstt1.possible_sample_types_id = slbt1.possible_sample_types_id
				
					WHERE 			slbt1.medical_record_num = "'.$this->sanitize($where['medical_record_num']).'"

					UNION 

					SELECT 			slbt2.sample_log_book_id,
									"MPI: '.$this->sanitize($where['mpi']).'" AS search_criteria,
									otpsvw2.ordered_tests,
									otpsvw2.test_status_concat,
									otpsvw2.ordered_tests_by_test_result,
									slbt2.first_name,
									slbt2.last_name,
									slbt2.soft_path_num,
									slbt2.soft_lab_num,
									slbt2.mol_num,
									slbt2.medical_record_num,
									slbt2.mpi,
									slbt2.test_tissue,
									slbt2.date_received,
									slbccvw2.problems,
									slbt2.block,
									slbt2.WBC_count,
									slbt2.h_and_e_circled,
									slbt2.tumor_cellularity,
									slbt2.outside_case_num,
									pstt2.sample_type

					FROM 			sample_log_book_table slbt2 

					LEFT JOIN 		ordered_tests_per_sample_vw otpsvw2 
					ON 				otpsvw2.sample_log_book_id = slbt2.sample_log_book_id

					LEFT JOIN 		sample_log_book_concat_comments_vw slbccvw2 
					ON 				slbccvw2.sample_log_book_id = slbt2.sample_log_book_id

					LEFT JOIN 		possible_sample_types_table pstt2 
					ON 				pstt2.possible_sample_types_id = slbt2.possible_sample_types_id
				
					WHERE 			slbt2.mpi = "'.$this->sanitize($where['mpi']).'"


					UNION 

					SELECT 			slbt3.sample_log_book_id,
									"Name: '.$this->sanitize($where['first_name']).' '.$this->sanitize($where['last_name']).'" AS search_criteria,
									otpsvw3.ordered_tests,
									otpsvw3.test_status_concat,
									otpsvw3.ordered_tests_by_test_result,
									slbt3.first_name,
									slbt3.last_name,
									slbt3.soft_path_num,
									slbt3.soft_lab_num,
									slbt3.mol_num,
									slbt3.medical_record_num,
									slbt3.mpi,
									slbt3.test_tissue,
									slbt3.date_received,
									slbccvw3.problems,
									slbt3.block,
									slbt3.WBC_count,
									slbt3.h_and_e_circled,
									slbt3.tumor_cellularity,
									slbt3.outside_case_num,
									pstt3.sample_type
									
					FROM 			sample_log_book_table slbt3 

					LEFT JOIN 		ordered_tests_per_sample_vw otpsvw3 
					ON 				otpsvw3.sample_log_book_id = slbt3.sample_log_book_id

					LEFT JOIN 		sample_log_book_concat_comments_vw slbccvw3 
					ON 				slbccvw3.sample_log_book_id = slbt3.sample_log_book_id

					LEFT JOIN 		possible_sample_types_table pstt3 
					ON 				pstt3.possible_sample_types_id = slbt3.possible_sample_types_id
				
					WHERE 	slbt3.last_name = "'.$this->sanitize($where['last_name']).'" AND 
							slbt3.first_name = "'.$this->sanitize($where['first_name']).'"
					';	
											
					break;

				case 'sample-log-book-by-id':
					$select = 
					'
					SELECT 		slbt.sample_log_book_id,
							    slbt.user_id,
							    slbt.possible_sample_types_id,
							    slbt.first_name,
							    slbt.last_name,
							    slbt.soft_path_num,
							    slbt.soft_lab_num,
							    slbt.mol_num,
							    slbt.medical_record_num,
							    slbt.test_tissue,
							    slbt.date_received,
							    slbt.block,
							    slbt.WBC_count,
							    slbt.h_and_e_circled,
							    slbt.tumor_cellularity,
							    slbt.time_stamp,
							    slbt.outside_case_num,
							    slbt.outside_mrn,
							    slbt.sample_type_toggle,
							    slbt.initial_login_user_id,
							    slbt.mpi,
							    CASE
							    	WHEN slbt.dob IS NOT NULL THEN slbt.dob 
							    	WHEN pt.dob IS NOT NULL THEN pt.dob 
							    	ELSE ""
							    END AS dob,
							    CASE
							    	WHEN slbt.sex IS NOT NULL THEN slbt.sex 
							    	WHEN pt.sex IS NOT NULL THEN pt.sex 
							    	ELSE ""
							    END AS sex,
							    CASE							    	 
							    	WHEN vt.order_num IS NOT NULL THEN vt.order_num 
							    	WHEN slbt.order_num IS NOT NULL THEN slbt.order_num
							    	ELSE ""
							    END AS order_num,
							    ott.ordered_test_id,
							    ott.visit_id,
								pstt.sample_type

					FROM 		sample_log_book_table slbt 

					LEFT JOIN 	possible_sample_types_table pstt 
					ON 			pstt.possible_sample_types_id = slbt.possible_sample_types_id

					LEFT JOIN 	ordered_test_table ott 
					ON 			ott.sample_log_book_id = slbt.sample_log_book_id

					LEFT JOIN 	visit_table vt
					ON 			vt.visit_id = ott.visit_id

					LEFT JOIN 	patient_table pt
					ON 			pt.patient_id = vt.patient_id
					
					WHERE 		slbt.sample_log_book_id = "'.$this->sanitize($where['sample_log_book_id']).'" 
					';

					if (isset($where['visit_id']) && isset($where['patient_id']) && !empty($where['patient_id']) && !empty($where['visit_id']))
					{
						$select.= ' AND ott.visit_id = "'.$this->sanitize($where['visit_id']).'"
									AND pt.patient_id = "'.$this->sanitize($where['patient_id']).'"';
					}
					break;

				case 'problems-sample-log-book':
					$select =
					'
					SELECT 	ct.*,
							CONCAT(ut.first_name," " ,ut.last_name) AS user_name

					FROM 	comment_table ct

					LEFT JOIN user_table ut 
					ON 		ut.user_id = ct.user_id 

					WHERE 	ct.comment_type = "sample_log_book_table" AND 
							ct.comment_ref = "'.$this->sanitize($where).'"

					ORDER BY 	ct.time_stamp ASC
					';
					break;

				case 'previous-comments-add-visit':
					$select =
					'
					SELECT 	ct.comment,
							ct.time_stamp,
							CONCAT(ut.first_name," " ,ut.last_name) AS user_name

					FROM 	comment_table ct

					LEFT JOIN user_table ut 
					ON 		ut.user_id = ct.user_id 

					WHERE 	ct.comment_type = "add_visit" AND 
							ct.comment_ref = "'.$this->sanitize($where).'"

					ORDER BY 	ct.time_stamp ASC
					';
					break;

				case 'previous-comments':
					$select =
					'
					SELECT 	ct.comment,
							ct.time_stamp,
							CONCAT(ut.first_name," " ,ut.last_name) AS user_name

					FROM 	comment_table ct

					LEFT JOIN user_table ut 
					ON 		ut.user_id = ct.user_id 

					WHERE 	ct.comment_type = "'.$this->sanitize($where['comment_type']).'" AND 
							ct.comment_ref = "'.$this->sanitize($where['comment_ref']).'"

					ORDER BY 	ct.time_stamp ASC
					';
					break;

				case 'all-possible-sample-types':
					$select =
					'
					SELECT 	*

					FROM 	possible_sample_types_table

					WHERE 	status = "on"

					ORDER BY 	sample_type
					';
					break;

				case 'all-possible-sample-types-off-also':
					$select =
					'
					SELECT 	*

					FROM 	possible_sample_types_table

					ORDER BY 	sample_type
					';
					break;

				case 'find-data-by-id-and-table':
					$select =
					'
					SELECT 	*

					FROM 	'.$this->sanitize($where['table']).'

					WHERE  '.str_replace('_table', '_id', $this->sanitize($where['table'])).' = "'.$this->sanitize($where['id']).'"
					';					
					break;

				case 'sample-type-search':
					$select =
					'
					SELECT 	pstt.*,
							CONCAT(ut.first_name," " ,ut.last_name) AS user_name

					FROM 	possible_sample_types_table pstt 

					LEFT JOIN user_table ut 
					ON 		ut.user_id = pstt.user_id 

					WHERE 	pstt.sample_type = "'.$this->sanitize($where).'"
					';
					break;

				case 'instrument-info':
					$select =
					'
					SELECT 		*

					FROM 		instrument_table it 

					WHERE 		it.instrument_id = "'.$this->sanitize($where).'"
					';
					break;

				case 'instrument-use-report':
					$select =
					'
					SELECT 		*

					FROM 		instrument_used_table iut

					LEFT JOIN 	ordered_test_table ott 
					ON 			ott.ordered_test_id = iut.ref_id

					LEFT JOIN 	orderable_tests_table otst 
					ON 			otst.orderable_tests_id = ott.orderable_tests_id

					LEFT JOIN 	sample_log_book_table slbt
					ON 			slbt.sample_log_book_id = ott.sample_log_book_id

					WHERE 		iut.instrument_id = "'.$this->sanitize($where).'"
					';
					
					break;

				case 'all-instruments-used':
					$select =
					'
					SELECT 		iu_vw.*,
								COUNT(iut.instrument_used_id) AS num_tests

					FROM 		instruments_used_vw iu_vw 

					LEFT JOIN 	instrument_used_table iut
					ON 			iu_vw.instrument_id = iut.instrument_id

					WHERE 		iu_vw.ref_table = "ordered_test_table"

					GROUP BY 	iut.instrument_id

					ORDER BY 	iu_vw.yellow_tag_num ASC

					';
					break;

				case 'audit-turn-around-time':
					$select =
					'
					SELECT 	DATEDIFF(ott.finish_date, ott.start_date) AS turnaround,
							ott.expected_turnaround_time, 
							CASE 
								WHEN 	DATEDIFF(ott.finish_date, ott.start_date) <= ott.expected_turnaround_time 
								THEN 	"good" 
								ELSE 	"bad" 
							END AS meets_expectations,
							otst.test_name,
							ott.orderable_tests_id,
							ott.ordered_test_id 

					FROM 	ordered_test_table ott 

					LEFT JOIN orderable_tests_table otst
					ON 		otst.orderable_tests_id = ott.orderable_tests_id

					WHERE 	ott.test_status = "complete" AND
							ott.expected_turnaround_time <> 0 
					';							
					break;

				case 'audit-tissue-counts-turn-around-time':
					$select =
					'
					SELECT 	COUNT(slbt.test_tissue) AS count_test_tissue,
							YEAR(slbt.time_stamp) AS year,
							MONTHNAME(slbt.time_stamp) AS month,
							slbt.test_tissue

					FROM 	sample_log_book_table slbt';

					if ($where['filter'] === 'current_month')
					{
						$select.=' WHERE MONTH(slbt.time_stamp) = MONTH(CURRENT_DATE()) AND 
							YEAR(slbt.time_stamp) = YEAR(CURRENT_DATE())';
					}
					else if ($where['filter'] === 'months')
					{
						$select.= ' WHERE MONTH(slbt.time_stamp) = '.$this->sanitize($where['month']).' AND 
							YEAR(slbt.time_stamp) = '.$this->sanitize($where['year']);
					}

					$select.=	'

					GROUP BY 	YEAR(slbt.time_stamp), 
							MONTHNAME(slbt.time_stamp), 
							slbt.test_tissue
					';										
					break;

				case 'audit-month-counts-turn-around-time':
					$select =
					'
					SELECT 	COUNT(
								CASE 
									WHEN 	DATEDIFF(ott.finish_date, ott.start_date) > ott.expected_turnaround_time 
									THEN 	1 
									ELSE 	NULL 
								END 
							) AS failed_count,							
							COUNT(
								CASE 
									WHEN 	DATEDIFF(ott.finish_date, ott.start_date) <= ott.expected_turnaround_time 
									THEN 	1 
									ELSE 	NULL 
								END 
							) AS passed_tests,
							COUNT(*) AS total_tests,
							MONTHNAME(ott.finish_date) AS month,
							YEAR(ott.finish_date) AS year

					FROM 	ordered_test_table ott 

					LEFT JOIN orderable_tests_table otst
					ON 		otst.orderable_tests_id = ott.orderable_tests_id

					WHERE 	ott.test_status = "complete" AND
							ott.expected_turnaround_time <> 0 ';

					if ($where['filter'] === 'current_month')
					{
						$select.='AND
							MONTH(ott.finish_date) = MONTH(CURRENT_DATE()) AND 
							YEAR(ott.finish_date) = YEAR(CURRENT_DATE())';
					}
					else if ($where['filter'] === 'months')
					{
						$select.= 'AND MONTH(ott.finish_date) = '.$this->sanitize($where['month']).' AND 
							YEAR(ott.finish_date) = '.$this->sanitize($where['year']);
					}

					$select.=	'
					GROUP BY 	MONTH(ott.finish_date)
					';										
					break;

				case 'audit-month-turn-around-time-test-stats':
					$select =
					'
					SELECT 	COUNT(
								CASE 
									WHEN 	DATEDIFF(ott.finish_date, ott.start_date) > ott.expected_turnaround_time 
									THEN 	1 
									ELSE 	NULL 
								END 
							) AS failed_count,							
							COUNT(
								CASE 
									WHEN 	DATEDIFF(ott.finish_date, ott.start_date) <= ott.expected_turnaround_time 
									THEN 	1 
									ELSE 	NULL 
								END 
							) AS passed_tests,
							COUNT(*) AS total_tests,
							AVG(DATEDIFF(ott.finish_date, ott.start_date)) AS average_turnaround,
							STD(DATEDIFF(ott.finish_date, ott.start_date)) AS std_turnaround,

							MIN(DATEDIFF(ott.finish_date, ott.start_date)) AS min_turnaround,
							MAX(DATEDIFF(ott.finish_date, ott.start_date)) AS max_turnaround,
							MONTHNAME(ott.finish_date) AS month,
							YEAR(ott.finish_date) AS year,
							otst.test_name,
							ott.expected_turnaround_time

					FROM 	ordered_test_table ott 

					LEFT JOIN orderable_tests_table otst
					ON 		otst.orderable_tests_id = ott.orderable_tests_id

					WHERE 	ott.test_status = "complete" AND
							ott.expected_turnaround_time <> 0 ';

					if ($where['filter'] === 'current_month')
					{
						$select.='AND
							MONTH(ott.finish_date) = MONTH(CURRENT_DATE()) AND 
							YEAR(ott.finish_date) = YEAR(CURRENT_DATE())';
					}
					else if ($where['filter'] === 'months')
					{
						$select.= 'AND MONTH(ott.finish_date) = '.$this->sanitize($where['month']).' AND 
							YEAR(ott.finish_date) = '.$this->sanitize($where['year']);
					}

					$select.=	'
					GROUP BY 	MONTH(ott.finish_date),
								otst.orderable_tests_id
					';										
					break;


				case 'audit-tests-turnaround-all':
					$select =
					'
					SELECT 	ott.ordered_test_id,
							slbt.soft_lab_num,
							otst.test_name,
							ott.start_date,
							ott.finish_date,
							DATEDIFF(ott.finish_date, ott.start_date) AS turnaround_time, 
							ott.expected_turnaround_time,
							MONTHNAME(ott.finish_date) AS month,
							YEAR(ott.finish_date) AS year,
							CONCAT(ut.first_name," " ,ut.last_name) AS user_name,
							(
							SELECT 	GROUP_CONCAT(ut2.first_name, ":", tt2.task SEPARATOR ", ") AS users

							FROM 	ordered_test_table ott2

							LEFT JOIN users_performed_task_table uptt2 
							ON 		uptt2.ref_id = ott2.ordered_test_id AND 
									uptt2.ref_table = "ordered_test_table"
							
							LEFT JOIN user_table ut2 
							ON 		ut2.user_id = uptt2.user_id

							LEFT JOIN task_table tt2
							ON 		tt2.task_id = uptt2.task_id

							WHERE 	ott2.ordered_test_id = ott.ordered_test_id

							GROUP BY 	ott2.ordered_test_id

							) AS users_by_task

					FROM 	ordered_test_table ott 

					LEFT JOIN orderable_tests_table otst
					ON 		otst.orderable_tests_id = ott.orderable_tests_id

					LEFT JOIN user_table ut
					ON 		ut.user_id = ott.user_id

					LEFT JOIN sample_log_book_table slbt
					ON 		slbt.sample_log_book_id = ott.sample_log_book_id

					WHERE 	ott.test_status = "complete" AND
							ott.expected_turnaround_time <> 0 
							
					ORDER BY otst.test_name, YEAR(ott.finish_date) DESC, MONTHNAME(ott.finish_date) DESC 
					';	
								
					break;


				case 'audit-tests-turnaround-curr-month':
					$select =
					'
					SELECT 	ott.ordered_test_id,
							slbt.soft_lab_num,
							slbt.medical_record_num,
							CONCAT(slbt.first_name," " , slbt.last_name) AS patient_name,
							otst.test_name,
							ott.start_date,
							ott.finish_date,
							DATEDIFF(ott.finish_date, ott.start_date) AS turnaround_time, 
							ott.expected_turnaround_time,
							MONTHNAME(ott.finish_date) AS month,
							YEAR(ott.finish_date) AS year,
							CONCAT(ut.first_name," " ,ut.last_name) AS user_name,
							(
							SELECT 	GROUP_CONCAT(ut2.first_name, ":", tt2.task SEPARATOR ", ") AS users

							FROM 	ordered_test_table ott2

							LEFT JOIN users_performed_task_table uptt2 
							ON 		uptt2.ref_id = ott2.ordered_test_id AND 
									uptt2.ref_table = "ordered_test_table"
							
							LEFT JOIN user_table ut2 
							ON 		ut2.user_id = uptt2.user_id

							LEFT JOIN task_table tt2
							ON 		tt2.task_id = uptt2.task_id

							WHERE 	ott2.ordered_test_id = ott.ordered_test_id

							GROUP BY 	ott2.ordered_test_id

							) AS users_by_task

					FROM 	ordered_test_table ott 

					LEFT JOIN orderable_tests_table otst
					ON 		otst.orderable_tests_id = ott.orderable_tests_id

					LEFT JOIN user_table ut
					ON 		ut.user_id = ott.user_id

					LEFT JOIN sample_log_book_table slbt
					ON 		slbt.sample_log_book_id = ott.sample_log_book_id

					WHERE 	ott.test_status = "complete" AND
							ott.expected_turnaround_time <> 0 AND
							MONTH(ott.finish_date) = '.$this->sanitize($where['month']).' AND 
							YEAR(ott.finish_date) = '.$this->sanitize($where['year']).'
							
					ORDER BY otst.test_name, YEAR(ott.finish_date) DESC, MONTHNAME(ott.finish_date) DESC 
					';	
						
					break;					
				case 'audit-tests-failed-turnaround-curr-month':
					$select =
					'
					SELECT 	ott.ordered_test_id,
							slbt.soft_lab_num,
							slbt.medical_record_num,
							CONCAT(slbt.first_name," " , slbt.last_name) AS patient_name,
							otst.test_name,
							ott.start_date,
							ott.finish_date,
							DATEDIFF(ott.finish_date, ott.start_date) AS turnaround_time, 
							ott.expected_turnaround_time,
							MONTHNAME(ott.finish_date) AS month,
							YEAR(ott.finish_date) AS year,
							CONCAT(ut.first_name," " ,ut.last_name) AS user_name,
							(
							SELECT 	GROUP_CONCAT(ut2.first_name, ":", tt2.task SEPARATOR ", ") AS users

							FROM 	ordered_test_table ott2

							LEFT JOIN users_performed_task_table uptt2 
							ON 		uptt2.ref_id = ott2.ordered_test_id AND 
									uptt2.ref_table = "ordered_test_table"
							
							LEFT JOIN user_table ut2 
							ON 		ut2.user_id = uptt2.user_id

							LEFT JOIN task_table tt2
							ON 		tt2.task_id = uptt2.task_id

							WHERE 	ott2.ordered_test_id = ott.ordered_test_id

							GROUP BY 	ott2.ordered_test_id

							) AS users_by_task

					FROM 	ordered_test_table ott 

					LEFT JOIN orderable_tests_table otst
					ON 		otst.orderable_tests_id = ott.orderable_tests_id

					LEFT JOIN user_table ut
					ON 		ut.user_id = ott.user_id

					LEFT JOIN sample_log_book_table slbt
					ON 		slbt.sample_log_book_id = ott.sample_log_book_id

					WHERE 	ott.test_status = "complete" AND
							ott.expected_turnaround_time <> 0 AND
							MONTH(ott.finish_date) = '.$this->sanitize($where['month']).' AND 
							YEAR(ott.finish_date) = '.$this->sanitize($where['year']).'
							
					HAVING 	turnaround_time > ott.expected_turnaround_time 

					ORDER BY 	YEAR(ott.finish_date) DESC, MONTHNAME(ott.finish_date) DESC 
					';					
					break;

				case 'audit-num-samples':
					$select =
					'
					SELECT 	COUNT(slbt.sample_log_book_id) AS num_samples

					FROM 	sample_log_book_table slbt

					
					WHERE 	MONTH(slbt.time_stamp) = '.$this->sanitize($where['month']).' AND 
							YEAR(slbt.time_stamp) = '.$this->sanitize($where['year']);
					
					break;

				case 'audit-num-samples-per-months':
					$select =
					'
					SELECT 		COUNT(slbt.sample_log_book_id) AS num_samples,
								MONTHNAME(slbt.time_stamp) as month,
								YEAR(slbt.time_stamp) as year

					FROM 		sample_log_book_table slbt

					GROUP BY 	MONTH(slbt.time_stamp),
								YEAR(slbt.time_stamp)

					ORDER BY 	time_stamp ASC
					';
					
					break;

				case 'audit-samples-per-test':
					$select = 
					'
					SELECT 	COUNT(otst.test_name) AS num_tests,
							otst.test_name

					FROM 	ordered_test_table ott

					LEFT JOIN orderable_tests_table otst 
					ON 		otst.orderable_tests_id = ott.orderable_tests_id';

					if ($where['filter'] === 'current_month')
					{
						$select.=' WHERE MONTH(ott.finish_date) = MONTH(CURRENT_DATE()) AND YEAR(ott.finish_date) = YEAR(CURRENT_DATE()) AND
							otst.test_name NOT LIKE "%Extraction%"';
					}
					else if ($where['filter'] === 'months')
					{
						$select.= ' WHERE MONTH(ott.finish_date) = '.$this->sanitize($where['month']).' AND 
							YEAR(ott.finish_date) = '.$this->sanitize($where['year']).' AND
							otst.test_name NOT LIKE "%Extraction%"';
					}
					else
					{
						$select.=' WHERE otst.test_name NOT LIKE "%Extraction%"';
					}
					$select.= ' GROUP BY 	ott.orderable_tests_id
							ORDER BY 		otst.test_name';
					break;
				
				case 'cumulative-audit-samples-per-test':
					$select = 
					'
					SELECT 		COUNT(otst.test_name) AS num_tests,
								otst.test_name,
								MONTHNAME(ott.finish_date) as month,
								YEAR(ott.finish_date) as year,
								CONCAT(MONTHNAME(ott.finish_date)," " ,YEAR(ott.finish_date)) AS month_year


					FROM 		ordered_test_table ott

					LEFT JOIN 	orderable_tests_table otst 
					ON 			otst.orderable_tests_id = ott.orderable_tests_id

					WHERE 		ott.finish_date IS NOT NULL AND 
								ott.finish_date NOT LIKE "%0000-00-00%" AND
								otst.test_name NOT LIKE "%Extraction%" AND
								otst.test_type NOT LIKE "%extraction%" AND 
								otst.test_type NOT LIKE "%Quantification%" 

					GROUP BY 	MONTH(ott.finish_date),
								YEAR(ott.finish_date),
								otst.test_name,
								ott.orderable_tests_id
					
					ORDER BY 	ott.finish_date ASC
					';

					break;

				case 'cumulative-audit-samples-wo-tests':
					$select = 
					'
					SELECT 		COUNT(slbt.sample_log_book_id) AS num_samples,
								MONTHNAME(slbt.time_stamp) as month,
								YEAR(slbt.time_stamp) as year,
								CONCAT(MONTHNAME(slbt.time_stamp)," " ,YEAR(slbt.time_stamp)) AS month_year
							
					FROM 		sample_log_book_table slbt 

					LEFT JOIN 	ordered_tests_per_sample_vw otpsvw 
					ON 			otpsvw.sample_log_book_id = slbt.sample_log_book_id
					
					WHERE 		otpsvw.ordered_tests = "DNA Extraction" OR
								otpsvw.ordered_tests = "Nanodrop"

					GROUP BY 	MONTH(slbt.time_stamp),
								YEAR(slbt.time_stamp)

					ORDER BY 	slbt.time_stamp ASC

					';
					break;


				case 'audit-samples-tissues-per-test':
					$select = 
					'
					SELECT 	COUNT(otst.test_name) AS num_tests,
							otst.test_name,
							COUNT(slbt.test_tissue) AS num_tissues,
							slbt.test_tissue

					FROM 	ordered_test_table ott

					LEFT JOIN sample_log_book_table slbt 
					ON 		slbt.sample_log_book_id = ott.sample_log_book_id

					LEFT JOIN orderable_tests_table otst 
					ON 		otst.orderable_tests_id = ott.orderable_tests_id';

					if ($where['filter'] === 'current_month')
					{
						$select.=' WHERE MONTH(ott.finish_date) = MONTH(CURRENT_DATE()) AND YEAR(ott.finish_date) = YEAR(CURRENT_DATE()) AND
							otst.test_name NOT LIKE "%Extraction%"';
					}
					else if ($where['filter'] === 'months')
					{
						$select.= ' WHERE MONTH(ott.finish_date) = '.$this->sanitize($where['month']).' AND 
							YEAR(ott.finish_date) = '.$this->sanitize($where['year']).' AND
							otst.test_name NOT LIKE "%Extraction%"';
					}
					else
					{
						$select.=' WHERE otst.test_name NOT LIKE "%Extraction%"';
					}
					$select.= ' GROUP BY 	ott.orderable_tests_id,
										slbt.test_tissue
					';				
					break;

				case 'audit-num-tests':
					$select =
					'
					SELECT 	COUNT(ott.ordered_test_id) AS num_tests

					FROM 	ordered_test_table ott

					LEFT JOIN orderable_tests_table otst 
					ON 		otst.orderable_tests_id = ott.orderable_tests_id
					
					WHERE 	MONTH(ott.finish_date) = '.$this->sanitize($where['month']).' AND 
							YEAR(ott.finish_date) = '.$this->sanitize($where['year']). ' AND
							otst.test_name NOT LIKE "%Extraction%" AND
							otst.test_type NOT LIKE "%extraction%" AND 
							otst.test_type NOT LIKE "%Quantification%"';
									
					break;


				case 'audit-num-tests-per-months':
					$select =
					'
					SELECT 		COUNT(ott.ordered_test_id) AS num_tests,
								MONTHNAME(ott.finish_date) as month,
								YEAR(ott.finish_date) as year,
								ott.finish_date

					FROM 		ordered_test_table ott

					LEFT JOIN 	orderable_tests_table otst 
					ON 			otst.orderable_tests_id = ott.orderable_tests_id

					WHERE 		ott.finish_date IS NOT NULL AND 
								ott.finish_date NOT LIKE "%0000-00-00%" AND
								otst.test_name NOT LIKE "%Extraction%" AND
								otst.test_type NOT LIKE "%extraction%" AND 
								otst.test_type NOT LIKE "%Quantification%"

					GROUP BY 	MONTH(ott.finish_date),
								YEAR(ott.finish_date)

					ORDER BY 	ott.finish_date ASC
					';
				
					break;


				case 'audit-tests-failed-turnaround-all':
					$select =
					'
					SELECT 	ott.ordered_test_id,
							slbt.soft_lab_num,
							otst.test_name,
							DATEDIFF(ott.finish_date, ott.start_date) AS turnaround_time, 
							ott.expected_turnaround_time,
							ott.start_date,
							ott.finish_date,
							MONTHNAME(ott.finish_date) AS month,
							YEAR(ott.finish_date) AS year,
							CONCAT(ut.first_name," " ,ut.last_name) AS user_name,
							(
							SELECT 	GROUP_CONCAT(ut2.first_name, ":", tt2.task SEPARATOR ", ") AS users

							FROM 	ordered_test_table ott2

							LEFT JOIN users_performed_task_table uptt2 
							ON 		uptt2.ref_id = ott2.ordered_test_id AND 
									uptt2.ref_table = "ordered_test_table"
							
							LEFT JOIN user_table ut2 
							ON 		ut2.user_id = uptt2.user_id

							LEFT JOIN task_table tt2
							ON 		tt2.task_id = uptt2.task_id

							WHERE 	ott2.ordered_test_id = ott.ordered_test_id

							GROUP BY 	ott2.ordered_test_id

							) AS users_by_task

					FROM 	ordered_test_table ott 

					LEFT JOIN orderable_tests_table otst
					ON 		otst.orderable_tests_id = ott.orderable_tests_id

					LEFT JOIN user_table ut
					ON 		ut.user_id = ott.user_id

					LEFT JOIN sample_log_book_table slbt
					ON 		slbt.sample_log_book_id = ott.sample_log_book_id

					WHERE 	ott.test_status = "complete" AND
							ott.expected_turnaround_time <> 0 							
					HAVING 	turnaround_time > ott.expected_turnaround_time 

					ORDER BY 	YEAR(ott.finish_date) DESC, MONTHNAME(ott.finish_date) DESC 
					';
					break;
				case 'user-login-summary':
					$select =
					'
					SELECT 	COUNT(
								CASE 
									WHEN 	status = "passed"
									THEN 	1
									ELSE 	NULL
								END
							) AS "passed",
							COUNT(
								CASE 
									WHEN 	status = "failed"
									THEN 	1
									ELSE 	NULL
								END
							) AS "failed",
							email_address

					FROM 	login_table

					'.$this->sanitize($where).'

					GROUP BY 	email_address

					ORDER BY 	email_address
							
					';			
					break;

				case 'user-login-summary-status':
					$select =
					'
					SELECT DISTINCT 	status 

					FROM 			login_table

					'.$this->sanitize($where).'
					';
					break;

				case 'get-next-unk':
					$select =
					'
					SELECT 		MAX(CAST(SUBSTRING(slbt.'.$this->sanitize($where).', 4) AS INT)) AS last_unk_num

					FROM 		sample_log_book_table slbt 

					WHERE 		slbt.'.$this->sanitize($where).' LIKE "UNK%"
					';
					
					break;

				case 'all-sample-order-nums':
					$select =
					'
					SELECT DISTINCT	vt.order_num

					FROM 			ordered_test_table ott

					LEFT JOIN 		visit_table vt
					ON 				vt.visit_id = ott.visit_id

					WHERE 			ott.sample_log_book_id = "'.$this->sanitize($where).'" AND
									vt.order_num != ""
					';
					break;

				case 'num-tests-with-order-num':
					$select =
					'
					SELECT 			vt.visit_id

					FROM 			ordered_test_table ott

					LEFT JOIN 		visit_table vt
					ON 				vt.visit_id = ott.visit_id

					WHERE 			ott.sample_log_book_id = "'.$this->sanitize($where).'" AND
									vt.order_num != ""
					';
					break;

				case 'match-patient-by-name-sex':
					$select =
					'
					SELECT 		*

					FROM 		patient_table

					WHERE 		last_name = "'.$this->sanitize($where['last_name']).'" AND
								first_name = "'.$this->sanitize($where['first_name']).'" AND
								sex = "'.$this->sanitize($where['sex']).'" AND
								patient_id = "'.$this->sanitize($where['patient_id']).'"
					';
					break;

				case 'get-next-mol-num':
					$select =
					'
					SELECT 	MAX(CAST(SUBSTRING(vt.order_num, 7) AS INT)) AS last_mol_num
							
					FROM 	visit_table vt 

					WHERE 	vt.order_num REGEXP "[0-9]-MOL[0-9]" AND 
							vt.order_num REGEXP "'.$this->sanitize($where).'-MOL[0-9]" 
					';				
					break;

				case 'get-all-md-num':
					$select =
					'
					SELECT 	slbt.mol_num
							
					FROM 	sample_log_book_table slbt 

					WHERE 	slbt.mol_num REGEXP "[0-9]-[0-9]" AND 
							slbt.mol_num REGEXP "[0-9]-'.$this->sanitize($where).'"

					ORDER BY 	slbt.mol_num';				
					break;

				case 'hotspots-in-panel':
					$select =
					'
					SELECT 	* 

					FROM 	test_hotspots_table

					WHERE 	orderable_tests_id = "'.$this->sanitize($where).'"
					';
					break;

				case 'genes-in-panel-now':
					$select =
					'
					SELECT 	*,
							gcbpt.date_gene_added <= CURDATE() AS gene_present_in_panel							

					FROM 	genes_covered_by_panel_table gcbpt 

					WHERE 	gcbpt.ngs_panel_id = "'.$this->sanitize($where).'"
					';
					break;

				case 'one-genes-covered_by_panel':
					$select =
					'
					SELECT 		*

					FROM 		genes_covered_by_panel_table gcbpt 

					WHERE 		gcbpt.genes_covered_by_panel_id = "'.$this->sanitize($where).'"
					';
					break;

				case 'genes-in-panel-as-of-upload-ngs-date':
					$select =
					'
					SELECT 	b.genes_covered_by_panel_id,
							a.ngs_panel_id,
							a.gene,
				        	a.exon,
				        	b.accession_num,
							a.add_date
					        
					FROM 	
							(
								SELECT 	gcbpt.ngs_panel_id,
										gcbpt.gene,
										gcbpt.exon,
											MAX(gcbpt.date_gene_added) AS add_date

								FROM 		genes_covered_by_panel_table gcbpt 

								WHERE 	gcbpt.date_gene_added <= "'.$this->sanitize($where['upload_NGS_date']).'"

								GROUP BY 	gcbpt.ngs_panel_id, gcbpt.gene, gcbpt.exon
							)a

					INNER JOIN 
							(
								SELECT  gcbpt2.genes_covered_by_panel_id,
										gcbpt2.ngs_panel_id,
										gcbpt2.gene,
										gcbpt2.exon,
										gcbpt2.accession_num, 
										gcbpt2.date_gene_added

								FROM 	genes_covered_by_panel_table gcbpt2 

								WHERE 	gcbpt2.date_gene_added <= "'.$this->sanitize($where['upload_NGS_date']).'"

							) b 

					ON 		a.ngs_panel_id = b.ngs_panel_id AND a.gene = b.gene AND a.exon = b.exon AND a.add_date = b.date_gene_added

					WHERE 	a.ngs_panel_id = "'.$this->sanitize($where['ngs_panel_id']).'"

					ORDER BY 	a.gene, b.genes_covered_by_panel_id
					';
			
					break;

				case 'upload_ngs_date_pre_step':
					$select =
					'
					SELECT 	date(time_stamp) AS upload_date 	

					FROM 	pre_step_visit

					WHERE 	step = "upload_NGS_data" and visit_id = "'.$this->sanitize($where).'"
					';
					break;

				case 'unique-visit-fields':
					$select = 
					'
					SELECT 	*

					FROM 	visit_table vt

					WHERE 	vt.'.$this->sanitize($where['field_name']).' = "'.$this->sanitize($where['value']).'"
					';
					break;

				case 'unique-sample-fields':
					$select = 
					'
					SELECT 	*

					FROM 	sample_log_book_table slbt

					WHERE 	slbt.'.$this->sanitize($where['field_name']).' = "'.$this->sanitize($where['value']).'"
					';
					break;

				case 'myeloid-library-pool':
					$select =
					'
					SELECT 	lpt.*

					FROM 	pool_chip_linker_table pclt

					LEFT JOIN library_pool_table lpt 
					ON 		lpt.library_pool_id = pclt.library_pool_id_A

					WHERE 	pclt.pool_chip_linker_id = "'.$this->sanitize($where['pool_chip_linker_id']).'"					
					';
					break;

				case 'pool-chip-linker-by-id':
					$select =
					'
					SELECT 	*

					FROM 	pool_chip_linker_table pclt

					WHERE 	pclt.pool_chip_linker_id = "'.$this->sanitize($where).'"
					';
					break;

				case 'stock-dilution-by-visits-in-pool-id':
					$select =
					'
					SELECT 	*

					FROM 	stock_dilution_table

					WHERE 	visits_in_pool_id = "'.$this->sanitize($where).'"
					';
					break;

				case 'visits-in-pool-by-id':
					$select = 
					'
					SELECT 	*

					FROM 	visits_in_pool_table

					WHERE 	visits_in_pool_id = "'.$this->sanitize($where).'"
					';
					break;

				case 'min-max-barcode':
					$select =
					'
					SELECT 	rlt.start,
							rlt.stop,
							rlt.reagent_name

					FROM 	used_reagents_table urt 

					LEFT JOIN reagent_list_table rlt 
					ON 		rlt.reagent_list_id = urt.reagent_list_id 

					WHERE 	pool_chip_linker_id = "'.$this->sanitize($where).'";
					';
					break;

				case 'all-pool-complete-steps':
					$select = 
					'
					SELECT 	pst.pool_steps_id,
							pst.status,
							pst.step,
							CONCAT(ut.first_name," " ,ut.last_name) AS user_name
					
					FROM 	pool_steps_table pst 

					LEFT JOIN user_table ut 
					ON 		ut.user_id = pst.user_id

					WHERE 	pst.pool_chip_linker_id = "'.$this->sanitize($where).'"
					';
					break;

				case 'ofa-run-template-by-chip':
					$select =
					'

					SELECT 	vipt.sample_name,
							vipt.patient_name,
							vipt.order_num,
							vipt.visits_in_pool_id,
							vipt.barcode_ion_torrent,
							vt.visit_id,
							pclt.pool_chip_linker_id

					FROM 	pool_chip_linker_table pclt

					LEFT JOIN library_pool_table lpt 
					ON 		lpt.library_pool_id = pclt.library_pool_id_'.$this->sanitize($where['chip']).'

					LEFT JOIN visits_in_pool_table vipt 
					ON 		vipt.library_pool_id = lpt.library_pool_id

					LEFT JOIN visit_table vt
					ON 		vt.visit_id = vipt.visit_id

					WHERE 	pclt.pool_chip_linker_id = "'.$this->sanitize($where['pool_chip_linker_id']).'"

					ORDER BY 	vipt.order_num ASC
					
					';
					break;
				
				case 'distinct-test-results':
					$select =
					'
					SELECT DISTINCT 	test_result 

					FROM 				ordered_test_table 

					WHERE 				test_result <> "" AND 
										test_result IS NOT NULL
					';
					break;

				case 'myeloid-run-template-by-chip':
					$select =
					'

					SELECT 	vipt.sample_name,
							vipt.patient_name,
							vipt.order_num,
							vipt.visits_in_pool_id,
							vipt.i7_index_miseq,
							vipt.i5_index_miseq

					FROM 	pool_chip_linker_table pclt

					LEFT JOIN library_pool_table lpt 
					ON 		lpt.library_pool_id = pclt.library_pool_id_A

					LEFT JOIN visits_in_pool_table vipt 
					ON 		vipt.library_pool_id = lpt.library_pool_id

					LEFT JOIN visit_table vt
					ON 		vt.visit_id = vipt.visit_id

					WHERE 	pclt.pool_chip_linker_id = "'.$this->sanitize($where['pool_chip_linker_id']).'"

					ORDER BY 	vipt.order_num ASC
					
					';
					break;
				case 'visits-in-pool':
					$select = 
					'
					
					SELECT 	* 

					FROM 	library_pool_A_vw lpavw

					WHERE 	lpavw.pool_chip_linker_id = "'.$this->sanitize($where).'"
					
					UNION

					
					SELECT 	* 

					FROM 	library_pool_B_vw lpbvw

					WHERE 	lpbvw.pool_chip_linker_id = "'.$this->sanitize($where).'"

					ORDER BY 	order_num

					';		
					break;

				case 'myeloid-ntc-last-used':
					$select =
					'
					SELECT 	nt.i7_index_miseq,
							nt.i5_index_miseq,
							CONCAT(ut.first_name," " ,ut.last_name) AS user_name

					FROM 	ntc_table nt

					LEFT JOIN user_table ut
					ON 		ut.user_id = nt.user_id

					WHERE 	nt.i7_index_miseq IS NOT NULL AND nt.i5_index_miseq IS NOT NULL

					ORDER BY 	nt.time_stamp DESC

					LIMIT 	1
					';							
					break;

				case 'last-used-indexes-by-visits':
					$select =
					'
					SELECT 		vipt.i7_index_miseq,
								vipt.i5_index_miseq,
								CONCAT(ut.first_name," " ,ut.last_name) AS user_name

					FROM 		visits_in_pool_table vipt

					LEFT JOIN 	user_table ut
					ON 			ut.user_id = vipt.user_id

					LEFT JOIN 	visit_table vt
					ON 			vipt.visit_id = vt.visit_id

					WHERE 		vipt.i7_index_miseq IS NOT NULL AND 
								vipt.i5_index_miseq IS NOT NULL AND
								vt.ngs_panel_id = "'.$this->sanitize($where).'"

					ORDER BY 	vipt.time_stamp DESC

					LIMIT 	1
					';												
					break;



				case 'ntc-in-pool':
					$select = 
					'
					SELECT 	nt.*

					FROM 	ntc_table nt

					WHERE 	nt.pool_chip_linker_id = "'.$this->sanitize($where).'"
					';
					break;

				case 'dna-conc-control-quant':
					$select = 
					'
					SELECT 	pclt.dna_conc_control_quant

					FROM 	pool_chip_linker_table pclt 

					WHERE 	pclt.pool_chip_linker_id = "'.$this->sanitize($where).'"
					';
					break;

				case 'pending-pools-ngs-panel-id':
					$select =
					'
					SELECT 	pclt.status,
							pclt.time_stamp,
							pclt.library_pool_id_A,
							pclt.library_pool_id_B,
							pclt.excel_workbook_num_id,
							CONCAT(pclt.library_pool_id_A, ", ", pclt.library_pool_id_B) AS library_pools,
							np.type AS ngs_panel_type,
							np.ngs_panel_id,
							pclt.pool_chip_linker_id AS pool_number,
							CONCAT(ut.first_name," " ,ut.last_name) AS user_name,
							(
							SELECT 	COUNT(*)

							FROM 	visits_in_pool_table vipt1

							WHERE 	vipt1.library_pool_id = pclt.library_pool_id_A
							)  AS count_chip_a,
							(
							SELECT 	COUNT(*)

							FROM 	visits_in_pool_table vipt1

							WHERE 	vipt1.library_pool_id = pclt.library_pool_id_b
							)  AS count_chip_b

					FROM 	pool_chip_linker_table pclt

					LEFT JOIN ngs_panel np 
					ON 		pclt.ngs_panel_id = np.ngs_panel_id

					LEFT JOIN user_table ut
					ON 		ut.user_id = pclt.user_id

					WHERE 	np.ngs_panel_id = "'.$this->sanitize($where['ngs_panel_id']).'"
					';	

					if (isset($where['pool_view_type']) && $where['pool_view_type'] === 'recent')
					{
						$select.= ' AND pclt.time_stamp >= ( CURDATE() - INTERVAL 2 WEEK) AND pclt.status = "pending"';
					}								
					break;

				case 'pools-visit-in':
					$select = 
					'
					SELECT 	*

					FROM 	visits_in_pool_table vipt 

					WHERE 	visit_id = "'.$this->sanitize($where).'"
					';
					break;

				case 'pending-pools':
					$select =
					'
					SELECT 	pclt.status,
							pclt.time_stamp,
							pclt.library_pool_id_A,
							pclt.library_pool_id_B,
							pclt.excel_workbook_num_id,
							CONCAT(pclt.library_pool_id_A, ", ", pclt.library_pool_id_B) AS library_pools,
							np.type AS ngs_panel_type,
							np.ngs_panel_id,
							pclt.pool_chip_linker_id AS pool_number,
							CONCAT(ut.first_name," " ,ut.last_name) AS user_name,
							(
							SELECT 	COUNT(*)

							FROM 	visits_in_pool_table vipt1

							WHERE 	vipt1.library_pool_id = pclt.library_pool_id_A
							)  AS count_chip_a,
							(
							SELECT 	COUNT(*)

							FROM 	visits_in_pool_table vipt1

							WHERE 	vipt1.library_pool_id = pclt.library_pool_id_b
							)  AS count_chip_b

					FROM 	pool_chip_linker_table pclt

					LEFT JOIN ngs_panel np 
					ON 		pclt.ngs_panel_id = np.ngs_panel_id

					LEFT JOIN user_table ut
					ON 		ut.user_id = pclt.user_id
					';	

					if (isset($where['pool_view_type']) && $where['pool_view_type'] === 'recent')
					{
						$select.= ' WHERE pclt.time_stamp >= ( CURDATE() - INTERVAL 2 WEEK) AND pclt.status = "pending"';
					}
												
					break;

				case 'curr-pending-pools':
					$select =
					'
					SELECT 	pclt.status,
							pclt.time_stamp,
							pclt.library_pool_name,
							pclt.library_prep_date,
							pclt.infotrack_version,
							pclt.library_pool_id_A,
							pclt.library_pool_id_B,
							pclt.excel_workbook_num_id,
							CONCAT(pclt.library_pool_id_A, ", ", pclt.library_pool_id_B) AS library_pools,
							pclt.ngs_run_date,
							pclt.excel_workbook_num_id,
							pclt.status_excel_workbook,
							np.type AS ngs_panel_type,
							np.ngs_panel_id,
							pclt.pool_chip_linker_id AS pool_number,
							CONCAT(ut.first_name," " ,ut.last_name) AS user_name,
							(
							SELECT 	COUNT(*)

							FROM 	visits_in_pool_table vipt1

							WHERE 	vipt1.library_pool_id = pclt.library_pool_id_A
							)  AS count_chip_a,
							(
							SELECT 	COUNT(*)

							FROM 	visits_in_pool_table vipt1

							WHERE 	vipt1.library_pool_id = pclt.library_pool_id_b
							)  AS count_chip_b

					FROM 	pool_chip_linker_table pclt

					LEFT JOIN ngs_panel np 
					ON 		pclt.ngs_panel_id = np.ngs_panel_id

					LEFT JOIN user_table ut
					ON 		ut.user_id = pclt.user_id

					WHERE 	pclt.status = "pending" AND
							pclt.pool_chip_linker_id = "'.$this->sanitize($where).'"

					';				
					break;

				case 'library-pools':
					$select =
					'
					SELECT 		lpta.*,
								nsplp_vwa.num_samples_in_pool

					FROM 		pool_chip_linker_table pclta 

					LEFT JOIN 	library_pool_table lpta 
					ON 			lpta.library_pool_id = pclta.library_pool_id_A
					
					LEFT JOIN 	num_samples_per_library_pool_vw nsplp_vwa 
					ON 			nsplp_vwa.library_pool_id = lpta.library_pool_id

					WHERE 		pclta.pool_chip_linker_id = "'.$this->sanitize($where).'"

					UNION 

					SELECT 		lptb.*,
								nsplp_vwb.num_samples_in_pool

					FROM 		pool_chip_linker_table pcltb 

					LEFT JOIN 	library_pool_table lptb 
					ON 			lptb.library_pool_id = pcltb.library_pool_id_B

					LEFT JOIN 	num_samples_per_library_pool_vw nsplp_vwb 
					ON 			nsplp_vwb.library_pool_id = lptb.library_pool_id

					WHERE 		pcltb.pool_chip_linker_id = "'.$this->sanitize($where).'"			
					';							
					break;

				case 'last-used-barcode':
					$select =
					'
					SELECT 		pclt.pool_chip_linker_id AS pool_chip_linker_id_A,
								vipt.barcode_ion_torrent AS barcode_ion_torrent_A

					FROM 		pool_chip_linker_table pclt  

					LEFT JOIN 	library_pool_table lptA
					ON 			pclt.library_pool_id_A = lptA.library_pool_id 

					LEFT JOIN 	visits_in_pool_table vipt 
					ON 			lptA.library_pool_id = vipt.library_pool_id

					WHERE 		pclt.ngs_panel_id = "'.$this->sanitize($where['ngs_panel_id']).'" AND 
								pclt.pool_chip_linker_id <> "'.$this->sanitize($where['pool_chip_linker_id']).'" 

					UNION 

					SELECT 		pclt.pool_chip_linker_id AS pool_chip_linker_id_B,
								vipt.barcode_ion_torrent AS barcode_ion_torrent_B

					FROM 		pool_chip_linker_table pclt

					LEFT JOIN 	library_pool_table lptB
					ON 			pclt.library_pool_id_A = lptB.library_pool_id

					LEFT JOIN 	visits_in_pool_table vipt 
					ON 			lptB.library_pool_id = vipt.library_pool_id


					WHERE 		pclt.ngs_panel_id = "'.$this->sanitize($where['ngs_panel_id']).'" AND 
								pclt.pool_chip_linker_id <> "'.$this->sanitize($where['pool_chip_linker_id']).'" 
					

					';	
							
					break;

				case 'possible-barcodes-by-ngs-panel':
					$select = 
					'
					SELECT 	* 

					FROM 	reagent_list_table

					WHERE 	purpose = "barcode" AND 
							ngs_panel_id = "'.$this->sanitize($where).'"
					';
					break;

				case 'all-expected_control-variants-for-control':
					$select =
					'
					SELECT 		ecvt.genes,
								ecvt.coding,
								ecvt.amino_acid_change,
								ecvt.min_accepted_frequency,
								ecvt.max_accepted_frequency,
								ecvt.target_frequency_range
								
					FROM 		expected_control_variants_table ecvt 

					WHERE 		ecvt.control_type_id = "'.$this->sanitize($where).'"
					';
					break;

				case 'all-info-expected_control-variants-for-control':
					$select =
					'
					SELECT 		ecvt.*
								
					FROM 		expected_control_variants_table ecvt 

					WHERE 		ecvt.control_type_id = "'.$this->sanitize($where).'"
					';
					break;

				case 'expected-control-variant-by-id':
					$select = 
					'
					SELECT 		*

					FROM 		expected_control_variants_table ecvt 

					WHERE 		ecvt.expected_control_variants_id = "'.$this->sanitize($where).'"
					';
					break;

				case 'expected-control-variant-by-id':
					$select =
					'
					SELECT 	*

					FROM 	expected_control_variants_table

					WHERE 	expected_control_variants_id = "'.$this->sanitize($where).'"
					';
					break;

				case 'ngs-panel-info-by-id':
					$select = 
					'
					SELECT 	np.* 

					FROM 	ngs_panel np

					WHERE 	np.ngs_panel_id = "'.$this->sanitize($where).'"
					';
					break;
				case 'unexpected-control-variants':
					$select = 
					'
					SELECT 	ovt.genes, 
							ovt.coding, 
							ovt.amino_acid_change,
							ovt.frequency,
							ecvt.genes AS expected_gene
					
					FROM 	visit_table vt 

					LEFT JOIN observed_variant_table ovt 
					ON 		ovt.run_id = vt.run_id 

					LEFT JOIN expected_control_variants_table ecvt
					ON 		ovt.genes = ecvt.genes AND
							ovt.coding = ecvt.coding AND
							ovt.amino_acid_change = ecvt.amino_acid_change 

					WHERE 	vt.control_type_id = "'.$this->sanitize($where['control_type_id']).'" AND 
							ovt.run_id = "'.$this->sanitize($where['run_id']).'"

					ORDER BY 	-ecvt.genes ASC,
							ovt.genes ASC, 
							ovt.coding ASC, 
							ovt.amino_acid_change ASC;
					';
					break;

				case 'expected-variant-with-stats':
					$select = 
					'
					SELECT 	ovt.genes, 
							ovt.coding, 
							ovt.amino_acid_change, 
							AVG(ovt.frequency) AS avg_freq, 
							MAX(ovt.frequency) as max, 
							MIN(ovt.frequency) as min,
							COUNT(ovt.coding) AS observed_count,
							STDDEV(ovt.frequency) AS standard_deviation,
							ecvt.genes AS expected_gene,
							ecvt.coding AS expected_coding,
							ecvt.amino_acid_change AS expected_amino_acid_change,
							ecvt.max_accepted_frequency,
							ecvt.min_accepted_frequency
					
					FROM 	visit_table vt 

					LEFT JOIN observed_variant_table ovt 
					ON 		ovt.run_id = vt.run_id 

					LEFT JOIN expected_control_variants_table ecvt
					ON 		ovt.genes = ecvt.genes AND
							ovt.coding = ecvt.coding AND
							ovt.amino_acid_change = ecvt.amino_acid_change 

					WHERE 	vt.control_type_id = "'.$this->sanitize($where['control_type_id']).'" AND 
							ecvt.expected_control_variants_id = "'.$this->sanitize($where['expected_control_variants_id']).'"

					GROUP BY 	ovt.genes, ovt.coding, ovt.amino_acid_change 

					ORDER BY 	-ecvt.genes DESC,
							ovt.genes ASC, 
							ovt.coding ASC, 
							ovt.amino_acid_change ASC;
					';				
					break;

				case 'summary-control-vaf':
					$select = 
					'
					SELECT 	ovt.genes, 
							ovt.coding, 
							ovt.amino_acid_change, 
							AVG(ovt.frequency) AS avg_freq, 
							MAX(ovt.frequency) as max, 
							MIN(ovt.frequency) as min,
							COUNT(ovt.coding) AS observed_count,
							STDDEV(ovt.frequency) AS standard_deviation,
							ecvt.genes AS expected_gene,
							ecvt.coding AS expected_coding,
							ecvt.amino_acid_change AS expected_amino_acid_change,
							ecvt.max_accepted_frequency,
							ecvt.min_accepted_frequency
					
					FROM 	visit_table vt 

					LEFT JOIN observed_variant_table ovt 
					ON 		ovt.run_id = vt.run_id 

					LEFT JOIN expected_control_variants_table ecvt
					ON 		ovt.genes = ecvt.genes AND
							ovt.coding = ecvt.coding AND
							ovt.amino_acid_change = ecvt.amino_acid_change 

					WHERE 	vt.control_type_id = "'.$this->sanitize($where['control_type_id']).'" AND 
							vt.run_id != 0 AND
							vt.ngs_panel_id = "'.$this->sanitize($where['ngs_panel_id']).'" AND 
							ecvt.control_type_id = "'.$this->sanitize($where['control_type_id']).'"

					GROUP BY 	ovt.genes, ovt.coding, ovt.amino_acid_change 

					ORDER BY 	ecvt.genes DESC,
								ovt.genes ASC, 
								ovt.coding ASC, 
								ovt.amino_acid_change ASC;
					';
					break;

				case 'control-info':
					$select = 
					'
					SELECT 	* 
					
					FROM 	control_type_table

					WHERE 	control_type_id = "'.$this->sanitize($where).'"
					';
					break;

				case 'control-expected-by-observed':
					$select = 
					'
					SELECT 	ecvt.control_type_id,
							ecvt.expected_control_variants_id,
							ecvt.genes AS Expected_Gene,
							ecvt.coding AS Expected_Coding, 
							ecvt.amino_acid_change AS Expected_Protein,	
							ecvt.min_accepted_frequency AS Expected_Min_VAF,
							ecvt.max_accepted_frequency AS Expected_Max_VAF,
							ovt.genes AS Observed_Gene,
							ovt.coding AS Observed_Coding,
							ovt.amino_acid_change AS Observed_Protein,
							ovt.frequency AS Observed_VAF,
							sbt.ref_pos,
							sbt.ref_neg,
							sbt.variant_neg,
							sbt.variant_pos

					FROM 	expected_control_variants_table ecvt 

					LEFT JOIN observed_variant_table ovt
					ON 		ovt.run_id = "'.$this->sanitize($where['run_id']).'" AND 
							ovt.genes = ecvt.genes AND
							ovt.coding = ecvt.coding AND
							ovt.amino_acid_change = ecvt.amino_acid_change 

					LEFT JOIN strand_bias_table sbt
					ON 		sbt.observed_variant_id = ovt.observed_variant_id

					WHERE 	ecvt.control_type_id = "'.$this->sanitize($where['control_type_id']).'"

					ORDER BY 	ecvt.genes
					';
			
					break;
				
				case 'control-by-id':
					$select = 
					'
					SELECT 	* 

					FROM 	control_type_table

					WHERE 	control_type_id = "'.$this->sanitize($where).'"
					';
					break;

				case 'all-positive-controls':
					$select = 
					'
					SELECT 	ctt.*

					FROM 	control_type_table ctt

					WHERE 	ctt.type = "positive"
					';			
					break;
				
				case 'control-last-version-for-ngs-panel':
					$select =
					'
					SELECT 	vt.version, 
							vt.control_type_id,
							CONCAT(ut.first_name," " ,ut.last_name) AS user_name,
							DATE(vt.time_stamp) AS version_date

					FROM 	visit_table vt

					LEFT JOIN user_table ut
					ON 		ut.user_id = vt.user_id

					WHERE 	vt.version <>"" AND
							vt.ngs_panel_id = "'.$this->sanitize($where['ngs_panel_id']).'" AND 
							vt.control_type_id = "'.$this->sanitize($where['control_type_id']).'"

					ORDER BY 	vt.time_stamp DESC

					LIMIT 	1

					';
					break;

				case 'all-positive-controls-plus-ngs-panel':
					$select = 
					'
					SELECT DISTINCT 	np.type AS ngs_panel,
									np.ngs_panel_id,
									ct.type AS control_type,
									ct.control_type_id,
									ct.control_name

					FROM 	ngs_panel np

					LEFT JOIN visit_table vt	
					ON 		np.ngs_panel_id = vt.ngs_panel_id 

					LEFT JOIN control_type_table ct
					ON 		ct.control_type_id = vt.control_type_id 

					WHERE 	ct.control_type_id IS NOT NULL AND 
							ct.type = "positive"
					';
					break;

				case 'all-negative-controls-plus-ngs-panel':
					$select = 
					'
					SELECT DISTINCT 	np.type AS ngs_panel,
										np.ngs_panel_id,
										ct.type AS control_type,
										ct.control_type_id,
										ct.control_name

					FROM 	ngs_panel np

					LEFT JOIN visit_table vt	
					ON 		np.ngs_panel_id = vt.ngs_panel_id 

					LEFT JOIN control_type_table ct
					ON 		ct.control_type_id = vt.control_type_id 

					WHERE 	ct.control_type_id IS NOT NULL AND 
							ct.type = "negative"
					';
					break;

				case 'all-controls':
					$select = 
					'
					SELECT  			ct.*,
										CONCAT(ut.first_name," " ,ut.last_name) AS user_name			

					FROM 				control_type_table ct

					LEFT JOIN 			user_table ut
					ON 					ut.user_id = ct.user_id
					';
					break;

				case 'current-controls':
					$select = 
					'
					SELECT  			ct.*

					FROM 				control_type_table ct

					WHERE 				ct.control_type_id = "'.$this->sanitize($where).'"
					';
					break;

				case 'all-negative-controls':
					$select = 
					'
					SELECT 	* 

					FROM 	control_type_table

					WHERE 	type = "negative"
					';
					break;
				
				case 'controls-by-name':
					$select =
					'
					SELECT 		*

					FROM 		control_type_table ct 

					WHERE 		ct.control_name = "'.$this->sanitize($where).'"
					';
					break;

				case 'revisions':
					$select =
					'
					SELECT 	rt.*, 
							CONCAT(ut.first_name," " ,ut.last_name) AS user_name

					FROM 	revise_table rt

					LEFT JOIN user_table ut
					ON 		ut.user_id = rt.user_id

					WHERE 	rt.visit_id = "'.$this->sanitize($where).'"

					ORDER BY	rt.time_stamp ASC
					'
					;
					break;

				case 'num-updates-in-time-period':
					$select =
					'
					SELECT 	SUM(num_updates) as total_num_updates 

					FROM 
						(
							SELECT 	DATEDIFF(now(), time_stamp) AS days_ago, 
									COUNT(version_history_id) AS num_updates 
							FROM version_history_table 

							GROUP BY DATE(time_stamp)
						) as temp 

					WHERE days_ago <= "'.$this->sanitize($where).'"
					';
					break;

				case 'version-history-within-7-days':
					$select =
					'
					SELECT 	vht.version_comment,
							vht.version_num_a,
							vht.version_num_b,
							vht.version_num_c,
							CONCAT("v",vht.version_num_a,"." ,vht.version_num_b,".",vht.version_num_c) AS version,
							CONCAT(ut.first_name," " ,ut.last_name) AS user_name,
							vht.time_stamp

					FROM 	version_history_table vht

					LEFT JOIN user_table ut
					ON 		ut.user_id = vht.user_id

					WHERE 	vht.time_stamp > (curdate() - INTERVAL 1 WEEK)

					ORDER BY  vht.version_history_id DESC
					';
				
					break;

				case 'version-history-within-14-days':
					$select =
					'
					SELECT 	vht.version_comment,
							vht.version_num_a,
							vht.version_num_b,
							vht.version_num_c,
							CONCAT("v",vht.version_num_a,"." ,vht.version_num_b,".",vht.version_num_c) AS version,
							CONCAT(ut.first_name," " ,ut.last_name) AS user_name,
							vht.time_stamp

					FROM 	version_history_table vht

					LEFT JOIN user_table ut
					ON 		ut.user_id = vht.user_id

					WHERE 	vht.time_stamp > (curdate() - INTERVAL 2 WEEK)

					ORDER BY  vht.version_history_id DESC
					';
				
					break;
				case 'version-history-within-7-days':
					$select =
					'
					SELECT 	vht.version_comment,
							vht.version_num_a,
							vht.version_num_b,
							vht.version_num_c,
							CONCAT("v",vht.version_num_a,"." ,vht.version_num_b,".",vht.version_num_c) AS version,
							CONCAT(ut.first_name," " ,ut.last_name) AS user_name,
							vht.time_stamp

					FROM 	version_history_table vht

					LEFT JOIN user_table ut
					ON 		ut.user_id = vht.user_id

					WHERE 	vht.time_stamp > (curdate() - INTERVAL 1 WEEK)

					ORDER BY  vht.version_history_id DESC
					';
				
					break;

				case 'version-history-desc':
					$select =
					'
					SELECT 	vht.version_comment,
							vht.version_num_a,
							vht.version_num_b,
							vht.version_num_c,
							vht.version_history_id,
							CONCAT("v",vht.version_num_a,"." ,vht.version_num_b,".",vht.version_num_c) AS version,
							CONCAT(ut.first_name," " ,ut.last_name) AS user_name,
							vht.time_stamp,
							vht.version_diagram

					FROM 	version_history_table vht

					LEFT JOIN user_table ut
					ON 		ut.user_id = vht.user_id

					ORDER BY  vht.version_history_id DESC
					';		
					break;

				case 'run-sheet-general-info':
					$select =
					'
					SELECT 	* 

					FROM 	run_sheet_general_table

					WHERE 	sheet_panel_type = "'.$this->sanitize($where['sheet_panel_type']).'" AND ngs_run_date = "'.$this->sanitize($where['ngs_run_date']).'" AND version = "'.$this->sanitize($where['version']).'"
					';
					break;

				case 'run-sheet-general':
					$select = 
					'
					SELECT 	* 

					FROM 	run_sheet_general_table

					WHERE 	run_sheet_general_id = "'.$this->sanitize($where).'" 
					';
					break;
				
				case 'run-sheet-rows':
					$select = 
					'
					SELECT 	* 

					FROM 	run_sheet_rows_table

					WHERE 	run_sheet_general_id = "'.$this->sanitize($where).'" 

					ORDER BY 	library_tube ASC
					';
					break;

				case 'previous-myleoid-run-sheets':
					$select = 
					'
					SELECT 	* 

					FROM 	run_sheet_general_table

					WHERE 	sheet_panel_type = "myeloid"

					ORDER BY  run_sheet_general_id DESC

					LIMIT 	10
					';
					break;
				case 'query_ensemble_transcrpits':
					$select =
					'
					SELECT	g.*,
							t.seq_region_start AS seq_start, 
							t.seq_region_end AS seq_end, 
							t.seq_region_strand, 
							t.is_current, 
							t.stable_id AS ENST,
							t.transcript_id,
							ox.*,
							x.*,
							ed.*

					FROM 	transcript t

					LEFT JOIN gene g 
					ON 		g.gene_id = t.gene_id 

					LEFT JOIN object_xref ox 
					ON 		ox.ensembl_id = t.transcript_id AND
							ox.ensembl_object_type = "Transcript" 

					LEFT JOIN xref x
					ON 		x.xref_id = ox.xref_id

					LEFT JOIN external_db ed 
					ON 		x.external_db_id = ed.external_db_id 							

					WHERE 	t.status = "KNOWN" AND 
							t.stable_id = "'.$this->sanitize($where).'" AND
							ed.db_name = "RefSeq_mRNA"
					';				
					break;

				case 'query_ensemble_exon':
					$select =
					'
					SELECT 	E.stable_id,
							R.name,
							E.seq_region_start,
							E.seq_region_end,
							E.seq_region_strand

					FROM 	exon E

					LEFT JOIN seq_region R
					ON 		R.seq_region_id=E.seq_region_id
				
					';
				
					break;

				case 'query_ucsc_exon':
					$select =
					'
					SELECT 	*

					FROM 	refGene rg

					WHERE 	rg.name = "'.$this->sanitize($where).'"

					LIMIT 	10
					';
					break;

				case 'suggestor-from-suggest-id':
					$select = 
					'
					SELECT 	srt.*, 
							CONCAT(ut.first_name," " ,ut.last_name) AS user_name,
							ut.email_address

					FROM 	suggested_revision_table srt 

					LEFT JOIN user_table ut 
					ON 		srt.user_id = ut.user_id

					WHERE 	srt.suggested_revision_id = "'.$this->sanitize($where).'"

					';
					break;

				case 'revisions_run':
					$select =
					'
					SELECT 	as_vw.suggested_revision_id,
							as_vw.user_id,
							as_vw.suggested_revision,
							as_vw.status,
							as_vw.run_id,
							as_vw.suggestor_user_name,
							"" AS suggested_suggested_revision_id,
							"" AS reviewed_suggested_revision_id,						
							"" AS reviwer_user_name,
							"" AS reviewer_comment,
							as_vw.time_stamp  

					FROM 	active_suggestions_vw as_vw

					WHERE 	as_vw.run_id = "'.$this->sanitize($where).'"

					UNION 	

					SELECT 	rs_vw.suggested_revision_id,
							rs_vw.user_id,
							rs_vw.suggested_revision,
							rs_vw.status,
							rs_vw.run_id,
							rs_vw.suggestor_user_name,
							rs_vw.suggested_suggested_revision_id,
							rs_vw.reviewed_suggested_revision_id,						
							rs_vw.reviwer_user_name,
							rs_vw.reviewer_comment,
							rs_vw.time_stamp 

					FROM 	reviewed_suggestions_vw rs_vw 

					WHERE 	rs_vw.run_id = "'.$this->sanitize($where).'"

					ORDER BY 	time_stamp DESC
					';
			
					break;

				case 'reporter-permission-list':
					$select = 
					'
					SELECT 		*

					FROM 		permission_table

					ORDER BY 	permission
					';
					break;

				case 'permission-info-by-id':
					$select = 
					'
					SELECT 		*

					FROM 		permission_table

					WHERE 		permission_id = "'.$this->sanitize($where).'"
					';
					break;


				case 'all-users':
					$select =
					'
					SELECT  	ut.user_id,
								ut.email_address,
								ut.password_need_reset,
								CONCAT(ut.first_name," " ,ut.last_name) AS user_name

					FROM 		user_table ut
					';
					break;

				case 'all-user-failed-logins':
					$select = 
					'
					SELECT 	CONCAT(first_name," " ,last_name) AS user_name,
							email_address,
							failed_last_30 

					FROM 	(

								SELECT 		ut.first_name, ut.last_name,
											COUNT(*) AS failed_last_30,
											ut.email_address

								FROM 		user_table ut
								LEFT JOIN 	login_table lt 
								ON 			ut.email_address = lt.email_address AND 
											lt.status = "failed" AND 
											lt.time_stamp > (now() - interval 30 minute)

								GROUP BY 	ut.first_name, ut.last_name

							)	AS 	failed_counts
					';									
					break;

				case 'login-last-thirty-mins-history':
					$select = 
					'
					SELECT 	COUNT(*) 

					FROM 	login_table lt 

					WHERE 	lt.status = "failed" AND 
							lt.time_stamp > (now() - interval 30 minute)

					GROUP BY 	lt.email_address
					';
					break;


				case 'login':
					$select = 
					'
					SELECT 	*

					FROM 	user_table

					WHERE 	email_address = "'.$this->sanitize($where['email_address']).'"
					AND 		password = "'.$this->sanitize($where['password']).'"
					';								
					break;

				case 'user-by-email':
					$select =
					'
					SELECT 	* 

					FROM 	user_table 

					WHERE  	email_address = "'.$this->sanitize($where['email_address']).'"
					';
					
					break;

				case 'previous-passwords':
					$select =
					'
					SELECT 	* 

					FROM 	user_previous_password_md5s_table 

					WHERE 	user_id = "'.$this->sanitize($where['user_id']).'" AND
							password = "'.$this->sanitize($where['password']).'"
					';
					break;

				case 'user-by-email-only':
					$select =
					'
					SELECT 	* 

					FROM 	user_table 

					WHERE  	email_address = "'.$this->sanitize($where['email_address']).'"
					';
					
					break;

				case 'all_wet_bench_users':
					$select =
					'
					SELECT 		ut.*

					FROM 		user_table ut

					JOIN 		permission_user_xref pux 
					ON 			pux.user_id = ut.user_id

					JOIN 		permission_table pt 
					ON 			pux.permission_id = pt.permission_id AND 
								pt.permission = "wet_bench"

					ORDER BY 		ut.first_name ASC
					';
					break;

				case 'wet-bench-tech-run':
					$select = 
					'
					SELECT 		ut.*

					FROM 		user_run_task_xref urtx 

					JOIN 		user_table ut 
					ON 			ut.user_id = urtx.user_id AND
								urtx.run_id = "'.$this->sanitize($where).'"
					';
					break;

				case 'ip-num-failed-attempts':
					$select =
					'
					SELECT 		COUNT(lt.login_id) as login_count

					FROM 		login_table lt

					WHERE 		lt.ip_address = "'.$this->sanitize($where).'" AND
								lt.status = "failed" AND 
								lt.time_stamp > (now() - interval 30 minute)
					';
					
					break;
				
				case 'user-num-failed-attempts':
					$select =
					'
					SELECT 		COUNT(lt.login_id) as login_count

					FROM 		login_table lt

					WHERE 		lt.email_address = "'.$this->sanitize($where).'" AND
								lt.status = "failed" AND 
								lt.time_stamp > (now() - interval 30 minute)
					';
					
					break;

				case 'num-logins-today':
					$select = 
					'
					SELECT 	COUNT(login_id) AS num_logins_today 

					FROM 	login_table 
					
					WHERE 	DATE(time_stamp) = CURDATE();
					';
					break;

				case 'failed-logins-last-30-mins':
					$select =
					'
					SELECT 		COUNT(lt.login_id) as login_count

					FROM 		login_table lt

					WHERE 		lt.status = "failed" AND 
								lt.time_stamp > (now() - interval 30 minute)
					';
					
					break;

				case 'low-coverage-by-run-id':
					$select = 
					'
					SELECT 	lct.*

					FROM 	low_coverage_table lct

					WHERE 	lct.run_id =  "'.$this->sanitize($where).'"  
					';
					break;

				case 'low-coverage-by-run-id-with-gene-include-status':
					$select = 
					'
					SELECT 	lct.*,
							gcbpt.date_gene_added,
							gcbpt.date_gene_added <= "'.$this->sanitize($where['upload_NGS_date']).'" AS activate_status

					FROM 	low_coverage_table lct

					LEFT JOIN 	gene_covered_on_date_vw gcbpt 
					ON 			gcbpt.ngs_panel_id = "'.$this->sanitize($where['ngs_panel_id']).'" AND 
								gcbpt.gene = lct.gene 

					WHERE 	lct.run_id =  "'.$this->sanitize($where['run_id']).'"  
					';					
					break;					

				case 'classify-by-panel-gene-coding-protein':
					$select = 
					'
					SELECT 	ct.*

					FROM 	classification_table ct

					WHERE 	ct.genes = "'.$this->sanitize($where['genes']).'" AND 
							ct.coding = "'.$this->sanitize($where['coding']).'" AND
							ct.amino_acid_change = "'.$this->sanitize($where['amino_acid_change']).'" AND
							ct.panel = "'.$this->sanitize($where['panel']).'"
					';
					break;

				case 'classification-variant':
					$select = 
					'
					SELECT 		ct.classification

					FROM 		classification_table ct

					WHERE 		ct.genes = "'.$this->sanitize($where['genes']).'" AND
								ct.coding = "'.$this->sanitize($where['coding']).'" AND
								ct.amino_acid_change = "'.$this->sanitize($where['amino_acid_change']).'" AND
								ct.panel =  "'.$this->sanitize($where['panel']).'"	
					';
					break;

				case 'low-coverage-by-run-id-gene-amplicon':
					$select =
					'
					SELECT 	lct.*

					FROM 	low_coverage_table lct

					WHERE 	lct.gene =  "'.$this->sanitize($where['gene']).'" AND 
							lct.run_id =  "'.$this->sanitize($where['run_id']).'" AND
							lct.Amplicon =  "'.$this->sanitize($where['Amplicon']).'" 
					';
					break;
				
				case 'low-cov-status':
					$select = 
					'
					SELECT 	lcst.status,
							lcst.type,
							CONCAT(ut.first_name," " ,ut.last_name) AS user_name

					FROM 	low_coverage_status_table lcst

					JOIN  	user_table ut
					ON 		ut.user_id = lcst.user_id

					WHERE 	lcst.run_id = "'.$this->sanitize($where).'"
					';								
					break;

				case 'query-variant-tier-xref-by-observed-variant-id':
					$select =
					'
					SELECT 	vtx.* 

					FROM 	variant_tier_xref vtx 

					WHERE 	vtx.observed_variant_id = "'.$this->sanitize($where).'"
					';
					break;

				case 'all-notes':
					$select = 
					'
					SELECT 	nt.note,
							nt.time_stamp,
							CONCAT(ut.first_name," " ,ut.last_name) AS user_name

					FROM 	notes_table nt

					JOIN  	user_table ut
					ON 		ut.user_id = nt.user_id

					ORDER BY 	nt.time_stamp DESC
					';
					break;

				case 'ip-info-exist':
					// find the ip info which pertains to ip address
					$select = 
					'
					SELECT 	* 

					FROM 	ip_info_table

					WHERE 	ip_address = "'.$this->sanitize($where).'"
					';
					break;

				case 'observed-variant-by-id':
					$select =
					'
					SELECT 	*

					FROM 	observed_variant_table

					WHERE 	observed_variant_id = "'.$this->sanitize($where).'"
					';
					break;
				
				case 'observed-variant-by-run-id':
					$select =
					'
					SELECT 	*

					FROM 	observed_variant_table

					WHERE 	run_id = "'.$this->sanitize($where).'"
					';
					break;					
				
				case 'ngs-panel-info':
					$select = 
					'
					SELECT 	*

					FROM 	ngs_panel
					';
					break;

				case 'possible-diagnosis':
					$select = 
					'
					SELECT 	*

					FROM 	tumor_type_table

					ORDER BY 	tumor ASC
					';
					break;

				case 'run-info':
					$select = 
					'
					SELECT 		rit.*,
								vt.visit_id,
								vt.received,
								vt.mol_num,
								vt.order_num,
								CONCAT(pt.first_name," " ,pt.middle_name, " ", pt.last_name) AS patient_name

					FROM 		run_info_table rit

					LEFT JOIN  	visit_table vt
					ON 			vt.run_id = rit.run_id

					LEFT JOIN 	patient_table pt 
					ON 			pt.patient_id = vt.patient_id

					WHERE 		rit.run_id = "'.$this->sanitize($where).'"
					';					
					break;
				
				case 'all-possible-stop-pool-reasons':
					$select = 
					'	
					SELECT DISTINCT 	pclt.status

					FROM 			pool_chip_linker_table pclt 

					WHERE 			pclt.status != "pre completed" AND 
									pclt.status != "report complete" AND
									pclt.status != "pending" 
					';
					break;


				case 'all-possible-stop-run-reasons':
					$select = 
					'	
					SELECT DISTINCT 	rit.status

					FROM 			run_info_table rit 

					WHERE 			rit.status != "completed" AND 
									rit.status != "tech approved" AND
									rit.status != "pending" 
					';
					break;

				case 'run-info-record-only':
					$select = 
					'
					SELECT 	* 

					FROM 	run_info_table rit

					WHERE 	rit.run_id = "'.$this->sanitize($where).'"
					';
					break;

				case 'pending-tech-runs':

					$select = 
					'
					SELECT 		rit.*,
								vt.visit_id,
								vt.mol_num,
								vt.soft_path_num,
								vt.soft_lab_num,
								vt.order_num,
								CASE 
									WHEN 	DATE(vt.collected) = "1970-01-01"
									THEN 	NULL
									ELSE 	vt.collected
								END AS collected,
								CASE 
									WHEN 	DATE(vt.received) = "1970-01-01"
									THEN 	NULL
									ELSE 	vt.received
								END AS received,
								vt.test_tissue,
								pt.medical_record_num,
								CONCAT(pt.first_name," " ,pt.middle_name, " ", pt.last_name) AS patient_name,
								pt.patient_id,	
								np.type AS ngs_panel,
								pncs_vw.editors_per_step,
								pncs_vw.completed_steps,
								np.ngs_panel_id			

					FROM 		run_info_table rit

					LEFT JOIN  	visit_table vt
					ON 			vt.run_id = rit.run_id

					LEFT JOIN 	patient_table pt 
					ON 			pt.patient_id = vt.patient_id

					LEFT JOIN 	ngs_panel np
					ON 			vt.ngs_panel_id = np.ngs_panel_id

					LEFT JOIN 	pending_NGS_completed_steps_vw pncs_vw
					ON 			pncs_vw.run_id = rit.run_id

					WHERE 		(
									rit.status = "pending" OR 
									rit.status = "tech approved" OR 
									rit.status = "revising"
								) AND								
								(
									rit.run_date >= NOW() - INTERVAL 3 WEEK AND 
									vt.ngs_panel_id <> 0
								)

					ORDER BY 		vt.visit_id DESC
					';
					break;

				case 'pending-tech-runs-all':

					$select = 
					'
					SELECT 		rit.*,
								vt.visit_id,
								vt.mol_num,
								vt.soft_path_num,
								vt.soft_lab_num,
								vt.order_num,
								CASE 
									WHEN 	DATE(vt.collected) = "1970-01-01"
									THEN 	NULL
									ELSE 	vt.collected
								END AS collected,
								CASE 
									WHEN 	DATE(vt.received) = "1970-01-01"
									THEN 	NULL
									ELSE 	vt.received
								END AS received,
								vt.test_tissue,
								pt.medical_record_num,
								CONCAT(pt.first_name," " ,pt.middle_name, " ", pt.last_name) AS patient_name,
								pt.patient_id,	
								np.type AS ngs_panel,
								pncs_vw.editors_per_step,
								pncs_vw.completed_steps,
								np.ngs_panel_id			

					FROM 		run_info_table rit

					LEFT JOIN  	visit_table vt
					ON 			vt.run_id = rit.run_id

					LEFT JOIN 	patient_table pt 
					ON 			pt.patient_id = vt.patient_id

					LEFT JOIN 	ngs_panel np
					ON 			vt.ngs_panel_id = np.ngs_panel_id

					LEFT JOIN 	pending_NGS_completed_steps_vw pncs_vw
					ON 			pncs_vw.run_id = rit.run_id

					WHERE 		(
									rit.status = "pending" OR 
									rit.status = "tech approved" OR 
									rit.status = "revising"
								)  AND
								(
									vt.ngs_panel_id <> 0
								)

					ORDER BY 		vt.visit_id DESC
					';
					break;

				case 'pending-tech-runs-ngs-panel-id':

					$select = 
					'
					SELECT 		rit.*,
								vt.visit_id,
								vt.mol_num,
								vt.soft_path_num,
								vt.soft_lab_num,
								vt.order_num,
								CASE 
									WHEN 	DATE(vt.collected) = "1970-01-01"
									THEN 	NULL
									ELSE 	vt.collected
								END AS collected,
								CASE 
									WHEN 	DATE(vt.received) = "1970-01-01"
									THEN 	NULL
									ELSE 	vt.received
								END AS received,
								vt.test_tissue,
								pt.medical_record_num,
								CONCAT(pt.first_name," " ,pt.middle_name, " ", pt.last_name) AS patient_name,
								pt.patient_id,	
								np.type AS ngs_panel,
								pncs_vw.editors_per_step,
								pncs_vw.completed_steps,
								np.ngs_panel_id

					FROM 		run_info_table rit

					LEFT JOIN  	visit_table vt
					ON 			vt.run_id = rit.run_id

					LEFT JOIN 	patient_table pt 
					ON 			pt.patient_id = vt.patient_id

					LEFT JOIN 	ngs_panel np
					ON 			vt.ngs_panel_id = np.ngs_panel_id

					LEFT JOIN 	pending_NGS_completed_steps_vw pncs_vw
					ON 			pncs_vw.run_id = rit.run_id

					WHERE 		(
									vt.ngs_panel_id = "'.$this->sanitize($where).'" AND 
									rit.run_date >= NOW() - INTERVAL 3 WEEK
								) 
								AND 
								(
									rit.status = "pending" OR 
									rit.status = "tech approved" OR
									rit.status = "revising"
								)

					ORDER BY 		vt.visit_id DESC
					';		
					break;

				case 'completed-controls':

					$select = 
					'
					SELECT 		rit.*,
								vt.visit_id,
								vt.mol_num,
								vt.soft_path_num,
								vt.soft_lab_num,
								vt.order_num,
								CASE 
									WHEN 	DATE(vt.collected) = "1970-01-01"
									THEN 	NULL
									ELSE 	vt.collected
								END AS collected,
								CASE 
									WHEN 	DATE(vt.received) = "1970-01-01"
									THEN 	NULL
									ELSE 	vt.received
								END AS received,
								vt.test_tissue,
								pt.medical_record_num,
								CONCAT(pt.first_name," " ,pt.middle_name, " ", pt.last_name) AS patient_name,
								pt.patient_id,	
								np.type AS ngs_panel,
								pncs_vw.editors_per_step,
								pncs_vw.completed_steps,
								np.ngs_panel_id

					FROM 		run_info_table rit

					LEFT JOIN  	visit_table vt
					ON 			vt.run_id = rit.run_id

					LEFT JOIN 	patient_table pt 
					ON 			pt.patient_id = vt.patient_id

					LEFT JOIN 	ngs_panel np
					ON 			vt.ngs_panel_id = np.ngs_panel_id

					LEFT JOIN 	pending_NGS_completed_steps_vw pncs_vw
					ON 			pncs_vw.run_id = rit.run_id

					WHERE 		(
									vt.ngs_panel_id = "'.$this->sanitize($where['control_type_id']).'" AND 
									vt.control_type_id = "'.$this->sanitize($where['control_type_id']).'"
								) 
								AND 
								(
									vt.control_type_id <> 0 AND 
									vt.control_type_id IS NOT NULL AND 
									rit.status <> "pending"
								)

					ORDER BY 		vt.visit_id DESC
					';		
					break;

				case 'pending-tech-runs-ngs-panel-id-all':

					$select = 
					'
					SELECT 		rit.*,
								vt.visit_id,
								vt.mol_num,
								vt.soft_path_num,
								vt.soft_lab_num,
								vt.order_num,
								CASE 
									WHEN 	DATE(vt.collected) = "1970-01-01"
									THEN 	NULL
									ELSE 	vt.collected
								END AS collected,
								CASE 
									WHEN 	DATE(vt.received) = "1970-01-01"
									THEN 	NULL
									ELSE 	vt.received
								END AS received,
								vt.test_tissue,
								pt.medical_record_num,
								CONCAT(pt.first_name," " ,pt.middle_name, " ", pt.last_name) AS patient_name,
								pt.patient_id,	
								np.type AS ngs_panel,
								pncs_vw.editors_per_step,
								pncs_vw.completed_steps,
								np.ngs_panel_id

					FROM 		run_info_table rit

					LEFT JOIN  	visit_table vt
					ON 			vt.run_id = rit.run_id

					LEFT JOIN 	patient_table pt 
					ON 			pt.patient_id = vt.patient_id

					LEFT JOIN 	ngs_panel np
					ON 			vt.ngs_panel_id = np.ngs_panel_id

					LEFT JOIN 	pending_NGS_completed_steps_vw pncs_vw
					ON 			pncs_vw.run_id = rit.run_id

					WHERE 		(
									vt.ngs_panel_id = "'.$this->sanitize($where).'"
								) AND 
								(
									rit.status = "pending" OR 
									rit.status = "tech approved" OR 
									rit.status = "revising"
								)

					ORDER BY 		vt.visit_id DESC
					';		
					break;					

				case 'tech-approved-runs':

					$select = 
					'
					SELECT 	*

					FROM 	run_info_table rit

					WHERE 	rit.status = "tech approved"
					';

					break;

				case 'ngs-panel-info-orderable-tests-table':
					$select =
					'
					SELECT 		*

					FROM 		ngs_panel np 

					LEFT JOIN 	orderable_tests_table ott 
					ON 			ott.ngs_panel_id = np.ngs_panel_id

					WHERE 		np.ngs_panel_id = "'.$this->sanitize($where).'"

					';				
					break;

				case 'steps-run':
					$select = 
					'
					SELECT 	rst.step,
							srx.time_stamp,
							CONCAT(ut.first_name," " ,ut.last_name) AS user_name

					FROM 	step_run_xref srx

					JOIN 	report_steps_table rst
					ON 		srx.step_id = rst.step_id

					JOIN 	user_table ut
					ON 		ut.user_id = srx.user_id

					WHERE 	srx.run_id = "'.$this->sanitize($where).'"
					
					ORDER BY 	srx.time_stamp DESC

					';
				
					break;
				
				case 'accession-num-present-for-ngs-panel':
					$select =
					'
					SELECT 	DISTINCT accession_num

					FROM 	genes_covered_by_panel_table 

					WHERE 	ngs_panel_id = "'.$this->sanitize($where).'"
					';
					break;

				case 'accession-num-present-for-ngs-panel-by-gene':
					$select =
					'
					SELECT 	DISTINCT 	gcbpt.accession_num

					FROM 				gcbpt.genes_covered_by_panel_table 	

					WHERE 	gcbpt.ngs_panel_id = "'.$this->sanitize($where['ngs_panel_id']).'" AND 
							gcbpt.gene = "'.$this->sanitize($where['gene']).'"

					ORDER BY 	gcbpt.date_gene_added DESC
					';				
					break;

				case 'all-accession-nums-present-for-gene':
					$select =
					'
					SELECT 		gcbpt.accession_num,
								np.*

					FROM 		gcbpt.genes_covered_by_panel_table 	

					LEFT JOIN 	ngs_panel np 
					ON 			np.ngs_panel_id = gcbpt.ngs_panel_id


					WHERE 	gcbpt.gene = "'.$this->sanitize($where['gene']).'"

					ORDER BY 	gcbpt.date_gene_added DESC
					';				
					break;

				case 'steps-run-reviewer':
					$select = 
					'
					SELECT  	ut.first_name,
							ut.last_name,
							ut.email_address,
							ut.user_id

					FROM 	step_run_xref srx

					JOIN 	report_steps_table rst
					ON 		srx.step_id = rst.step_id

					JOIN 	user_table ut
					ON 		ut.user_id = srx.user_id

					WHERE 	srx.run_id = "'.$this->sanitize($where).'" AND
							rst.step = "reviewers_emailed"
					';		
					break;					


				case 'completed-pre-steps':
					$select = 
					'
					SELECT 	psv.step

					FROM 	pre_step_visit psv

					WHERE 	psv.visit_id = "'.$this->sanitize($where).'"
					';
					break;

				case 'this-step':
					$select = 
					'
					SELECT 	*

					FROM 	report_steps_table

					WHERE 	step = "'.$this->sanitize($where).'"
					';
					break;

				case 'this-pre-step':
					$select = 
					'
					SELECT 	*

					FROM 	report_steps_table

					WHERE 	step = "'.$this->sanitize($where).'"
					';
					break;

				case 'step-added':
					$select =
					'
					SELECT 	step_id 

					FROM 	'.$this->sanitize($where['table_name']).' 

					WHERE 	step="'.$this->sanitize($where['step']).'"
					';				
					break;

				case 'find-patient-by-mrn-dob':
					$select =
					'
					SELECT 	*

					FROM 	patient_table 

					WHERE 	medical_record_num = "'.$this->sanitize($where['medical_record_num']).'" AND
							dob = "'.$this->sanitize($where['dob']).'" 
					';
					break;

				case 'patient-info':
					$select =
					'	
					SELECT 		CONCAT(pt.first_name," " ,pt.middle_name, " ", pt.last_name) AS patient_name,
								rivw.*,
								pt.*,
								CONCAT(ut.first_name," " ,ut.last_name) AS user_name

					FROM 		report_ids_vw rivw

					JOIN 		patient_table pt
					ON 			pt.patient_id = rivw.patient_id

					LEFT JOIN 	user_table ut
					ON 			pt.user_id = ut.user_id

					WHERE 		rivw.run_id = "'.$this->sanitize($where).'"
					';
					break;

				case 'find-patient':
					$select =
					'
					SELECT 	*

					FROM 	patient_table

					WHERE ';

					$first_key = true;
					
					foreach($where as $key => $value)
					{	
						// if user_id is included in this query every user will create a new patient.
						if ($key === 'user_id')
						{
							continue;
						}

						if(!$first_key)
						{
							$select.= ' AND ';
						}
					
						$select .= $this->sanitize($key).' = "'.$this->sanitize($value).'"';
						$first_key = false;						
					}
					break;

				case 'select-patient-from-id':
					$select = 
					'
					SELECT 		CONCAT(pt.first_name," " ,pt.middle_name, " ", pt.last_name) AS patient_name,
								pt.*,
								CONCAT(ut.first_name," " ,ut.last_name) AS user_name

					FROM 		patient_table pt

					LEFT JOIN 	user_table ut
					ON 			pt.user_id = ut.user_id

					WHERE 		pt.patient_id = "'.$this->sanitize($where).'"
					';
					break;
					
				case 'all-patient-visits':
					$select = 
					'
					SELECT 		vt.*,
								rit.*

					FROM 		visit_table vt

					LEFT JOIN 	run_info_table rit
					ON 			rit.run_id = vt.run_id

					WHERE 		vt.patient_id = "'.$this->sanitize($where).'"
					';
				
					break;
				
				case 'test-reporting-regulation-info':
					$select = 
					'
					SELECT 		*

					FROM 		test_reporting_regulation_info_table trrit 

					WHERE 		trrit.orderable_tests_id = "'.$this->sanitize($where).'"
					';
					break;

				case 'visit-info':
					$select =
					'	
					SELECT 		vt.*,
								CONCAT(ut.first_name," " ,ut.last_name) AS user_name,
								tt.tumor AS tumor_type,
								ngp.type as panel_type,
								ngp.method,
								ngp.url,
								ngp.disclaimer,
								ngp.limitations

					FROM 		visit_table vt

					LEFT JOIN 	user_table ut
					ON 			vt.user_id = ut.user_id

					LEFT JOIN  	ngs_panel ngp
					ON 			vt.ngs_panel_id = ngp.ngs_panel_id

					LEFT JOIN 	tumor_type_table tt
					ON 			tt.tumor_id = vt.diagnosis_id

					WHERE 		vt.run_id = "'.$this->sanitize($where).'"
					';
					
					break;

				case 'completed-reports-ngs-panel-id':
					$select =
					'
					SELECT 		rit.run_id,
								vt.visit_id,
								vt.soft_path_num,
								vt.soft_lab_num,
								vt.patient_id,
								vt.order_num,
								CONCAT(pt.first_name," " , pt.middle_name," ",pt.last_name) AS patient_name,
								pt.medical_record_num,
								rit.time_stamp,
								vt.mol_num,
								np.type AS panel,
								np.ngs_panel_id

					FROM 		run_info_table rit 

					LEFT JOIN 	visit_table vt
					ON 			rit.run_id = vt.run_id

					LEFT JOIN 	patient_table pt 
					ON 			pt.patient_id = vt.patient_id

					LEFT JOIN 	ngs_panel np 
					ON 			np.ngs_panel_id = vt.ngs_panel_id

					WHERE 		rit.status = "completed" AND np.ngs_panel_id = "'.$this->sanitize($where).'"

					ORDER BY 		rit.time_stamp DESC 

					';				
					break;

				case 'completed-reports':
					$select =
					'
					SELECT 		rit.run_id,
								vt.visit_id,
								vt.soft_path_num,
								vt.soft_lab_num,
								vt.patient_id,
								vt.order_num,
								CONCAT(pt.first_name," " , pt.middle_name," ",pt.last_name) AS patient_name,
								pt.medical_record_num,
								rit.time_stamp,
								vt.mol_num,
								np.type AS panel,
								np.ngs_panel_id,
								otst.orderable_tests_id,
								otst.test_name,
								ott.ordered_test_id,
								ott.sample_log_book_id,
								rit.single_analyte_pool_id

					FROM 		run_info_table rit 

					LEFT JOIN 	visit_table vt
					ON 			rit.run_id = vt.run_id

					LEFT JOIN 	ordered_test_table ott 
					ON 			ott.visit_id = vt.visit_id

					LEFT JOIN 	orderable_tests_table otst 
					ON 			otst.orderable_tests_id = ott.orderable_tests_id

					LEFT JOIN 	patient_table pt 
					ON 			pt.patient_id = vt.patient_id

					LEFT JOIN 	ngs_panel np 
					ON 			np.ngs_panel_id = vt.ngs_panel_id

					WHERE 		rit.status = "completed"

					ORDER BY 	rit.time_stamp DESC 				
					';								
					break;

				case 'scanned-reports-by-status':
					$select =
					'
					SELECT 		rit.run_id,
								vt.visit_id,
								vt.soft_path_num,
								vt.soft_lab_num,
								vt.patient_id,
								vt.order_num,
								CONCAT(pt.first_name," " , pt.middle_name," ",pt.last_name) AS patient_name,
								pt.medical_record_num,
								rit.time_stamp,
								vt.mol_num,
								otst.test_name, 
								np.type AS panel,
								np.ngs_panel_id,
								rit.report_type,
								rit.transferred_to_patient_chart,
								uut_vw.change_log,
								otst.orderable_tests_id,
								ott.ordered_test_id,
								ott.sample_log_book_id

					FROM 		run_info_table rit 

					LEFT JOIN 	visit_table vt
					ON 			rit.run_id = vt.run_id

					LEFT JOIN 	ordered_test_table ott 
					ON 			ott.visit_id = vt.visit_id

					LEFT JOIN 	orderable_tests_table otst 
					ON 			otst.orderable_tests_id = ott.orderable_tests_id

					LEFT JOIN 	patient_table pt 
					ON 			pt.patient_id = vt.patient_id

					LEFT JOIN 	user_update_task_vw uut_vw 
					ON 			uut_vw.ref_id = rit.run_id AND 
								uut_vw.ref = "run_info_table" AND 
								uut_vw.task = "scanned_ngs_report"

					LEFT JOIN 	ngs_panel np 
					ON 			np.ngs_panel_id = vt.ngs_panel_id

					WHERE 		rit.status = "completed" AND 
								rit.report_type = "reportable" AND 
								rit.transferred_to_patient_chart = "'.$this->sanitize($where).'"

					ORDER BY 	rit.time_stamp DESC 

					';								
					break;


				case 'ajax-visit-info':
					$select 	=
					'
					SELECT 	*

					FROM 	visit_table vt

					WHERE 	vt.visit_id = "'.$this->sanitize($where).'"
					';
					break;

				case 'get-gene-interpt-by-interpt':
					$select =
					'
					SELECT 	* 

					FROM 	gene_interpt_table git 

					WHERE 	git.interpt = "'.$this->sanitize($where).'"
					';
					break;

				case 'all-gene-interpts-for-gene':
					$select =
					'
					SELECT 		git.interpt,
								git.time_stamp,
								CONCAT(ut.first_name," " ,ut.last_name) AS user_name 

					FROM 		gene_interpt_table git

					LEFT JOIN  	user_table ut 
					ON 			ut.user_id = git.user_id

					WHERE 		git.gene = "'.$this->sanitize($where).'"

					ORDER BY 		git.time_stamp ASC
					';
					break;					

				case 'run-xref-gene-interpt-by-run-id-gene':
					$select = 
					'
					SELECT 		rxgi.*,
								git.interpt

					FROM 		run_xref_gene_interpt rxgi

					LEFT JOIN 	gene_interpt_table git 
					ON 			git.gene_interpt_id = rxgi.gene_interpt_id

					WHERE 		rxgi.gene = "'.$this->sanitize($where['gene']).'" AND 
								rxgi.run_id = "'.$this->sanitize($where['run_id']).'"
					';
					break;

				case 'add-visit-info':
					$select =
					'	
					SELECT 		vt.*,
								np.type AS ngs_panel,
								tt.tumor_id,
								tt.tumor AS tumor_type,
								ttt.tissue_id,
								ott.ordered_test_id,
								ott.sample_log_book_id

					FROM 		visit_table vt

					LEFT JOIN 	ngs_panel np
					ON 			np.ngs_panel_id = vt.ngs_panel_id

					LEFT JOIN 	tumor_type_table tt
					ON 			tt.tumor_id = vt.diagnosis_id

					LEFT JOIN 	tissue_type_table ttt 
					ON 			ttt.tissue = vt.primary_tumor_site

					LEFT JOIN 	ordered_test_table ott 
					ON 			ott.visit_id = vt.visit_id

					WHERE 		vt.visit_id = "'.$this->sanitize($where).'"
					';
					break;

				case 'add-patient-info':
					$select =
					'	
					SELECT 		pt.*,
								CONCAT(pt.first_name," " , pt.middle_name," ",pt.last_name) AS patient_name

					FROM 		patient_table pt

					WHERE 		pt.patient_id = "'.$this->sanitize($where).'"
					';
					break;

				case 'pre-step-complete':
					$select = 
					'
					SELECT 	* 

					FROM 	pre_step_visit

					WHERE 	visit_id = "'.$this->sanitize($where['visit_id']).'" AND 
							step = "'.$this->sanitize($where['step']).'"
					';
					break;

				case 'all-complete-pre-steps':	
					$select =
					'
					SELECT 	*

					FROM 	pre_step_visit

					WHERE 	visit_id = "'.$this->sanitize($where).'"
					';
					break;

				case 'all-complete-variant-qc-steps':
					$select = 
					'
					SELECT 	qvst.*

					FROM 	qc_variant_step_table qvst 

					WHERE 	qvst.xref_id = "'.$this->sanitize($where).'" AND
							qvst.type_xref = "run_id"
					';
					break;

				case 'qc-variant-steps-complete':
					$select =
					'
					SELECT 	qvst.step,
							qvst.time_stamp,
							CONCAT(ut.first_name," " ,ut.last_name) AS user_name

					FROM 	qc_variant_step_table qvst

					LEFT JOIN user_table ut
					ON 		ut.user_id = qvst.user_id

					WHERE 	qvst.run_id = "'.$this->sanitize($where).'"
					';
					break;

				case 'all-complete-steps':
					$select = 
					'
					SELECT 	step,
							user_name,
							time_stamp,
							user_id
						
					FROM 	(

								SELECT 	*

								FROM
										(
											SELECT 		psv.step,
														CONCAT(ut.first_name," " ,ut.last_name) AS user_name,
														psv.time_stamp,
														ut.user_id

											FROM 		visit_table vt 

											LEFT JOIN 	pre_step_visit psv 
											ON 			vt.visit_id = psv.visit_id AND
														vt.visit_id = "'.$this->sanitize($where).'"

											LEFT JOIN		user_table ut 
											ON 			ut.user_id = psv.user_id

											WHERE 		psv.step != "NULL"

										) 	AS step

								UNION 
								SELECT 	*

								FROM
										(
											SELECT 		rst.step,
														CONCAT(ut.first_name," " ,ut.last_name) AS user_name,
														srx.time_stamp,
														ut.user_id

											FROM 		visit_table vt 

											LEFT JOIN 	run_info_table rit 
											ON 			rit.run_id = vt.run_id AND 
														vt.visit_id = "'.$this->sanitize($where).'"

											LEFT JOIN 	step_run_xref srx 
											ON 			rit.run_id = srx.run_id

											LEFT JOIN		user_table ut 
											ON 			ut.user_id = srx.user_id

											LEFT JOIN 	report_steps_table rst 
											ON 			rst.step_id = srx.step_id

											WHERE 		rst.step != "NULL"

										) 	AS step
							) 	AS step2

					ORDER BY 		time_stamp DESC
					';
					break;

				case 'all-analysis-techs':
					$select = 
					'
					SELECT DISTINCT user_name
						
					FROM 	(
								SELECT 	*

								FROM
										(
											SELECT 		rst.step,
														CONCAT(ut.first_name," " ,ut.last_name) AS user_name,
														srx.time_stamp,
														ut.user_id

											FROM 		visit_table vt 

											LEFT JOIN 	run_info_table rit 
											ON 			rit.run_id = vt.run_id AND 
														vt.visit_id = "'.$this->sanitize($where).'"

											LEFT JOIN 	step_run_xref srx 
											ON 			rit.run_id = srx.run_id

											LEFT JOIN		user_table ut 
											ON 			ut.user_id = srx.user_id

											LEFT JOIN 	report_steps_table rst 
											ON 			rst.step_id = srx.step_id

											WHERE 		rst.step != "NULL"

										) 	AS step
							) 	AS step2
					';
					break;


				case 'this-pre-step-status':
					$select =
					'
					SELECT 		psv.*,
								CONCAT(ut.first_name," " ,ut.last_name) AS user_name

					FROM 		pre_step_visit psv

					LEFT JOIN 	user_table ut
					ON 			ut.user_id = psv.user_id

					WHERE 		psv.visit_id = "'.$this->sanitize($where['visit_id']).'" AND
								psv.step = "'.$this->sanitize($where['step']).'"

					';
					break;

				case 'reports-using-variant':
					$select = 
					'
					SELECT 			COUNT(rit.status),
									rit.status 

					FROM 			observed_variant_table ovt

					JOIN 			run_info_table rit 
					ON 				rit.run_id = ovt.run_id

					WHERE 			ovt.genes = "'.$this->sanitize($where['genes']).'" AND
									ovt.coding = "'.$this->sanitize($where['coding']).'" AND
									ovt.amino_acid_change = "'.$this->sanitize($where['amino_acid_change']).'" AND
									rit.status <> "pending" AND
									rit.status <> "tech approved"

					GROUP BY 		rit.status
					';	
echo $select;										
					break;

				case 'select-knowledge-base':
					$select =
					'
					SELECT 	*

					FROM 	knowledge_base_table kbt
					
					WHERE 	knowledge_id IN ('.$this->sanitize($where).')';
					break;

				case 'all-knowledge-base':
					$select =
					'
					SELECT 	*

					FROM 	knowledge_base_table kbt
					';

					break;

				case 'last-update-gene-knowledge-base':
					$select = 
					'
					SELECT 	gut.*

					FROM 	gene_update_table gut

					WHERE 	gut.time_stamp > (NOW() - INTERVAL 15 day) AND 
							gut.gene = "'.$this->sanitize($where).'"
					
					ORDER BY 	gut.time_stamp DESC

					LIMIT 	1
					'
					;
					break;

				case 'knowledge-base-passed-filter':
					$select =
					'
					SELECT 		kbt.*,
								GROUP_CONCAT(gcvpt.accession_num) AS accession_num,
								vt.ngs_panel_id,
								COUNT(gcvpt.accession_num) AS accession_num_count,
								CONCAT("https://varsome.com/variant/hg19/", gcvpt.accession_num, "(",ovt.genes,")%3A",ovt.coding) AS varsome_accession_link,
								CONCAT("https://mutalyzer.nl/name-checker?description=",gcvpt.accession_num, ":",ovt.coding) AS mutalyzer_accession_link,
								CONCAT("https://mutalyzer.nl/name-checker") AS mutalyzer_simple_link,
								CONCAT("https://varsome.com/variant/hg19/",ovt.coding, "%20",ovt.coding) AS varsome_simple_link

					FROM		observed_variant_table ovt

					JOIN 		knowledge_base_table kbt
					ON 			ovt.genes = kbt.genes AND ovt.coding = kbt.coding AND ovt.amino_acid_change = kbt.amino_acid_change

					LEFT JOIN 	visit_table vt 
					ON 			ovt.run_id = vt.run_id

					LEFT JOIN 	genes_covered_by_panel_table gcvpt 
					ON 			ovt.genes = gcvpt.gene AND
								gcvpt.ngs_panel_id = vt.ngs_panel_id

					WHERE 		ovt.run_id = "'.$this->sanitize($where).'"

					GROUP BY 	kbt.genes, kbt.coding, kbt.amino_acid_change, gcvpt.gene 
					';
				
					break;

				case 'knowledge-base-failed-filter':
					$select =
					'
					SELECT 		kbt.*,
								GROUP_CONCAT(gcvpt.accession_num) AS accession_num,
								vt.ngs_panel_id,
								COUNT(gcvpt.accession_num) AS accession_num_count,
								CONCAT("https://varsome.com/variant/hg19/", gcvpt.accession_num, "(",ovt.genes,")%3A",ovt.coding) AS varsome_accession_link,
								CONCAT("https://mutalyzer.nl/name-checker?description=",gcvpt.accession_num, ":",ovt.coding) AS mutalyzer_accession_link,
								CONCAT("https://mutalyzer.nl/name-checker") AS mutalyzer_simple_link,
								CONCAT("https://varsome.com/variant/hg19/",ovt.coding, "%20",ovt.coding) AS varsome_simple_link

					FROM		observed_variant_table ovt

					JOIN 		knowledge_base_table kbt
					ON 			ovt.genes = kbt.genes AND ovt.coding = kbt.coding AND ovt.amino_acid_change = kbt.amino_acid_change

					LEFT JOIN 	visit_table vt 
					ON 			ovt.run_id = vt.run_id

					LEFT JOIN 	genes_covered_by_panel_table gcvpt 
					ON 			ovt.genes = gcvpt.gene AND
								gcvpt.ngs_panel_id = vt.ngs_panel_id

					WHERE 		ovt.run_id = "'.$this->sanitize($where).'" AND ovt.filter_status = "failed"

					GROUP BY 	kbt.genes, kbt.coding, kbt.amino_acid_change, gcvpt.gene
					';
					break;

				case 'knowledge-base-included-filter':
					$select =
					'
					SELECT 		kbt.*,
								GROUP_CONCAT(gcvpt.accession_num) AS accession_num,
								vt.ngs_panel_id,
								COUNT(gcvpt.accession_num) AS accession_num_count,
								CONCAT("https://varsome.com/variant/hg19/", gcvpt.accession_num, "(",ovt.genes,")%3A",ovt.coding) AS varsome_accession_link,
								CONCAT("https://mutalyzer.nl/name-checker?description=",gcvpt.accession_num, ":",ovt.coding) AS mutalyzer_accession_link,
								CONCAT("https://mutalyzer.nl/name-checker") AS mutalyzer_simple_link,
								CONCAT("https://varsome.com/variant/hg19/",ovt.coding, "%20",ovt.coding) AS varsome_simple_link

					FROM		observed_variant_table ovt

					JOIN 		knowledge_base_table kbt
					ON 			ovt.genes = kbt.genes AND ovt.coding = kbt.coding AND ovt.amino_acid_change = kbt.amino_acid_change

					LEFT JOIN 	visit_table vt 
					ON 			ovt.run_id = vt.run_id

					LEFT JOIN 	genes_covered_by_panel_table gcvpt 
					ON 			ovt.genes = gcvpt.gene AND
								gcvpt.ngs_panel_id = vt.ngs_panel_id

					WHERE 		ovt.run_id = "'.$this->sanitize($where).'" AND ovt.include_in_report = "1"

					GROUP BY 	kbt.genes, kbt.coding, kbt.amino_acid_change, gcvpt.gene
					';
					break;

				case 'all-tiers':
					$select = 
					'
					SELECT 	*

					FROM 	tier_table
					';
					break;
				case 'find-tier':
					$select = 
					'
					SELECT 	*

					FROM 	variant_tier_xref

					WHERE 	tier_id = "'.$this->sanitize($where['tier_id']).'" AND
							knowledge_id = "'.$this->sanitize($where['knowledge_id']).'" AND
							tissue_id = "'.$this->sanitize($where['tissue_id']).'" 

					';
					break;
				case 'all-tissues':
					$select = 
					'
					SELECT 	*

					FROM 	tissue_type_table

					ORDER BY 	tissue
					';
					break;

				case 'all-primary-tumor-site':
					$select = 
					'
					SELECT DISTINCT primary_tumor_site

					FROM 			visit_table
					';
					break;

				case 'search-tissues':
					$select = 
					'
					SELECT 	* 

					FROM 	tissue_type_table

					WHERE 	tissue = "'.$this->sanitize($where).'"
					';
					break;

				case 'report-interpt':
					$select = 
					'
					SELECT 		ct.comment,
								kbt.pmid,
								ovt.genes,
								ovt.coding,
								ovt.amino_acid_change,
								ovt.tier,
								kbt.exon,
								kbt.cosmic,

								CONCAT(ovt.genes, " ", ovt.coding, " (", ovt.amino_acid_change,")") AS result_mutant


					FROM 		observed_variant_table ovt

					LEFT JOIN 	comment_table ct 
					ON 			ct.comment_id = ovt.interpt_id

					LEFT JOIN 	knowledge_base_table kbt
					ON 			ovt.genes = kbt.genes AND 
								ovt.coding = kbt.coding AND 
								ovt.amino_acid_change = kbt.amino_acid_change

					WHERE 		ovt.run_id = "'.$this->sanitize($where).'"  AND 
								ovt.include_in_report = 1

					';	
					break;

				case 'run-variants-passed-filter':
					$select =
					'
					SELECT 	va.*,
							sbt.ref_pos,
							sbt.ref_neg,
							sbt.variant_neg,
							sbt.variant_pos,
							sbt.strand_bias_id,
							sbt.user_id AS sb_user_id,
							sbt.observed_variant_id AS sb_observed_variant_id,
							kbt.knowledge_id,
							kbt.cosmic,
							vt.comment AS variant_interpt,
							gcbpt.date_gene_added <= "'.$this->sanitize($where['upload_NGS_date']).'" AS activate_status,
							va.classification
					FROM	(
						SELECT 		ovt.*,
									GROUP_CONCAT(classt.classification, " (", ut.first_name, " ", ut.last_name, ")" ORDER BY classt.time_stamp DESC SEPARATOR ", ") AS classification,
									GROUP_CONCAT(ct.comment_id ORDER BY ct.comment_id ASC SEPARATOR "-") AS comment_ids

						FROM			observed_variant_table ovt

						LEFT JOIN 	comment_table ct 
						ON 			ct.comment_ref = ovt.observed_variant_id AND 
									ct.comment_type = "observed_variant"

						LEFT JOIN 	classification_table classt 
						ON 			classt.genes = ovt.genes AND
									classt.coding =  ovt.coding AND
									classt.amino_acid_change = ovt.amino_acid_change

						LEFT JOIN 	user_table ut 
						ON 			ut.user_id = classt.user_id

						WHERE 		ovt.run_id = "'.$this->sanitize($where['run_id']).'" 

						GROUP BY 		ovt.genes, 	
									ovt.observed_variant_id,
									ovt.run_id,
									classt.genes,
									classt.coding,
									classt.amino_acid_change
					) va

					LEFT JOIN 	knowledge_base_table kbt 
					ON 			va.genes = kbt.genes AND
								va.coding =  kbt.coding AND
								va.amino_acid_change = kbt.amino_acid_change 

					LEFT JOIN 	comment_table vt 
					ON 			vt.comment_id = va.interpt_id AND 
								vt.comment_type = "knowledge_base_table"

					LEFT JOIN 	strand_bias_table sbt 
					ON 			sbt.observed_variant_id = va.observed_variant_id

					LEFT JOIN 	gene_covered_on_date_vw gcbpt 
					ON 			gcbpt.ngs_panel_id = "'.$this->sanitize($where['ngs_panel_id']).'" AND 
								gcbpt.gene = va.genes

					ORDER BY 		va.tier ASC, va.genes ASC
					';			
					break;
				
				case 'variant-info-for-access-verify-variants':
					$select =
					'
					SELECT 		ovt.genes,
								ovt.coding,
								ovt.amino_acid_change,
								ovt.include_in_report,
								kbt.knowledge_id,
								ct.classification

					FROM			observed_variant_table ovt

					LEFT JOIN 	knowledge_base_table kbt 
					ON 			ovt.genes = kbt.genes AND
								ovt.coding =  kbt.coding AND
								ovt.amino_acid_change = kbt.amino_acid_change 

					LEFT JOIN 	classification_table ct
					ON 			ct.genes = ovt.genes AND
								ct.amino_acid_change = ovt.amino_acid_change AND
								ct.coding = ovt.coding

					WHERE 		ovt.run_id = "'.$this->sanitize($where).'"	
					';			
					break;


				case 'chr-gene-on':
					$select = 
					'
					SELECT DISTINCT 	kbt.chr,
									kbt.hgnc

					FROM 			knowledge_base_table kbt

					WHERE 			kbt.genes = "'.$this->sanitize($where).'"
					';
					break;
				
				case 'mutation-types':
					$select = 'SELECT DISTINCT mutation_type FROM knowledge_base_table ORDER BY mutation_type ASC';
					break;

				case 'report-status':
					$select = 
					'
					SELECT 	rit.status AS approval_status

					FROM 	run_info_table rit

					WHERE 	rit.run_id = "'.$this->sanitize($where).'"
					';
					break;

				case 'approval-steps':
					$select = 
					'
					SELECT 	rst.step,
							srx.time_stamp,
							CONCAT(ut.first_name," " ,ut.last_name) AS user_name,
							ut.credentials,
							ut.title

					FROM 	step_run_xref srx

					JOIN 	report_steps_table rst

					ON 		srx.step_id = rst.step_id AND
                    			(rst.step = "tech_approved" OR rst.step = "confirmed")

					JOIN 	user_table ut

					ON 		ut.user_id = srx.user_id

					WHERE 	srx.run_id = "'.$this->sanitize($where).'"

					ORDER BY 	srx.time_stamp DESC';
					break;

				case 'run-variants-failed-filter':
					$select =
					'
					SELECT 	va.*,
							sbt.ref_pos,
							sbt.ref_neg,
							sbt.variant_neg,
							sbt.variant_pos,
							sbt.strand_bias_id,
							sbt.user_id AS sb_user_id,
							sbt.observed_variant_id AS sb_observed_variant_id,
							kbt.knowledge_id,
							gcbpt.date_gene_added <= "'.$this->sanitize($where['upload_NGS_date']).'" AS activate_status,
					FROM	(
						SELECT 		ovt.*,
									GROUP_CONCAT(ct.comment_id ORDER BY ct.comment_id ASC SEPARATOR "-") AS comment_ids

						FROM			observed_variant_table ovt

						LEFT JOIN 	comment_table ct 
						ON 			ct.comment_ref = ovt.observed_variant_id AND 
									ct.comment_type = "observed_variant"

						WHERE 		ovt.run_id = "'.$this->sanitize($where['run_id']).'" AND ovt.filter_status = "failed"

						GROUP BY 		ovt.genes, ovt.observed_variant_id, ovt.run_id
					) va

					LEFT JOIN 	knowledge_base_table kbt 
					ON 			va.genes = kbt.genes AND
								va.coding =  kbt.coding AND
								va.amino_acid_change = kbt.amino_acid_change 
					LEFT JOIN 	strand_bias_table sbt 
					ON 			sbt.observed_variant_id = va.observed_variant_id

					LEFT JOIN 	gene_covered_on_date_vw gcbpt 
					ON 			gcbpt.ngs_panel_id = "'.$this->sanitize($where['ngs_panel_id']).'" AND 
								gcbpt.gene = va.genes

					ORDER BY 		va.tier ASC, va.genes ASC
					';
					break;

				case 'kbt-snv':
					$select =
					'
					SELECT 	kbt.*

					FROM 	knowledge_base_table kbt 

					WHERE 	kbt.genes = "'.$this->sanitize($where['genes']).'" AND
							kbt.coding = "'.$this->sanitize($where['coding']).'" AND
							kbt.amino_acid_change = "'.$this->sanitize($where['amino_acid_change']).'"
					';
					break;
				case 'observed-variant-run-id':
					$select =
					'
					SELECT 	ovt.*

					FROM 	observed_variant_table ovt 

					WHERE 	ovt.genes = "'.$this->sanitize($where['genes']).'" AND
							ovt.coding = "'.$this->sanitize($where['coding']).'" AND
							ovt.amino_acid_change = "'.$this->sanitize($where['amino_acid_change']).'" AND 
							ovt.run_id = "'.$this->sanitize($where['run_id']).'"
					';
					break;					
				case 'kbt-snv-gene-coding':
					$select =
					'
					SELECT 	kbt.*

					FROM 	knowledge_base_table kbt 

					WHERE 	kbt.genes = "'.$this->sanitize($where['genes']).'" AND
							kbt.coding = "'.$this->sanitize($where['coding']).'" 
					';
					break;
				case 'run-variants-included-filter':
					$select =
					'
					SELECT 	va.*,
							kbt.knowledge_id,
							kbt.loc,
							kbt.chr,
							sbt.ref_pos,
							sbt.ref_neg,
							sbt.variant_neg,
							sbt.variant_pos,
							sbt.strand_bias_id,
							kbt.cosmic,
							vt.comment AS variant_interpt,
							gcbpt.date_gene_added <= "'.$this->sanitize($where['upload_NGS_date']).'" AS activate_status,
							va.classification
					FROM	(
						SELECT 		ovt.*,
									GROUP_CONCAT(classt.classification, " (", ut.first_name, " ", ut.last_name, ")" ORDER BY classt.time_stamp DESC SEPARATOR ", ") AS classification,
									GROUP_CONCAT(ct.comment_id ORDER BY ct.comment_id ASC SEPARATOR "-") AS comment_ids
									
						FROM 		observed_variant_table ovt

						LEFT JOIN 	comment_table ct 
						ON 			ct.comment_ref = ovt.observed_variant_id AND 
									ct.comment_type = "observed_variant"
						
						LEFT JOIN 	classification_table classt 
						ON 			classt.genes = ovt.genes AND
									classt.coding =  ovt.coding AND
									classt.amino_acid_change = ovt.amino_acid_change

						LEFT JOIN 	user_table ut 
						ON 			ut.user_id = classt.user_id

						WHERE 		ovt.run_id = "'.$this->sanitize($where['run_id']).'" AND ovt.include_in_report = "1"

						GROUP BY 		ovt.genes, 
									ovt.observed_variant_id,
									ovt.run_id,
									classt.genes,
									classt.coding,
									classt.amino_acid_change
					) va

					LEFT JOIN 	knowledge_base_table kbt 
					ON 			va.genes = kbt.genes AND
								va.coding =  kbt.coding AND
								va.amino_acid_change = kbt.amino_acid_change 

					LEFT JOIN 	comment_table vt 
					ON 			vt.comment_id = va.interpt_id AND 
								vt.comment_type = "knowledge_base_table"
					
					LEFT JOIN 	strand_bias_table sbt 
					ON 			sbt.observed_variant_id = va.observed_variant_id

					LEFT JOIN 	gene_covered_on_date_vw gcbpt 
					ON 			gcbpt.ngs_panel_id = "'.$this->sanitize($where['ngs_panel_id']).'" AND 
								gcbpt.gene = va.genes 

					ORDER BY 		va.tier ASC, va.genes ASC		
					';						
					break;
				
				case 'pending-pre-samples-sent-out':
					$select = 
					'
					SELECT 	vt.visit_id,
							vt.patient_id,
							vt.mol_num,
							vt.time_stamp,
							vt.soft_path_num,
							vt.soft_lab_num,
							vt.order_num,
							vt.test_tissue,
							CASE 
								WHEN 	DATE(vt.collected) = "1970-01-01"
								THEN 	NULL
								ELSE 	vt.collected
							END AS collected,
							CASE 
								WHEN 	DATE(vt.received) = "1970-01-01"
								THEN 	NULL
								ELSE 	vt.received
							END AS received,
							ppsv_vw.steps_passed,
						    fpsv_vw.fail_count,
						    ott.ordered_test_id,
							ott.sample_log_book_id,
							dt.diagnosis,
							pt.medical_record_num,
							CONCAT(pt.first_name," " , pt.middle_name," ",pt.last_name) AS patient_name,
							np.type AS ngs_panel,
							np.ngs_panel_id,
							vtcc_vw.problems,
							flag_psv_vw.flagged_steps,
							pcpsv_vw.pre_complete_count,
							sopsv_vw.sent_out_count,
							epspsv_vw.editors_per_step

					FROM 		visit_table vt								
					
					LEFT JOIN 	passed_pre_step_visit_vw ppsv_vw 
					ON 			vt.visit_id = ppsv_vw.visit_id

					LEFT JOIN 	failed_pre_step_visit_vw fpsv_vw 
					ON 			vt.visit_id = fpsv_vw.visit_id 

					LEFT JOIN 	flagged_pre_step_visit_vw flag_psv_vw 
					ON 			vt.visit_id = flag_psv_vw.visit_id 

					LEFT JOIN 	sent_out_pre_step_visit_vw sopsv_vw 
					ON 			vt.visit_id = sopsv_vw.visit_id 

					LEFT JOIN 	pre_complete_pre_step_visit_vw pcpsv_vw 
					ON 			vt.visit_id = pcpsv_vw.visit_id 

					LEFT JOIN 	ordered_test_table ott 
					ON 			vt.visit_id = ott.visit_id
					
					LEFT JOIN 	diagnosis_table dt 
					ON 			vt.diagnosis_id = dt.diagnosis_id

					LEFT JOIN 	patient_table pt 
					ON 			vt.patient_id = pt.patient_id

					LEFT JOIN 	ngs_panel np 
					ON 			vt.ngs_panel_id = np.ngs_panel_id

					LEFT JOIN 	visit_table_concat_comments_vw vtcc_vw
					ON 			vt.visit_id = vtcc_vw.visit_id

					LEFT JOIN 	editors_per_step_pre_step_visit_vw epspsv_vw 
					ON 			vt.visit_id = epspsv_vw.visit_id

					WHERE 		sent_out_count <> 0 

					ORDER BY 	vt.visit_id ASC
					';
					break;

					$select = 
					'
					SELECT 	*
					FROM 	(
							
							SELECT 	vt.visit_id,
									vt.patient_id,
									vt.mol_num,
									vt.time_stamp,
									vt.soft_path_num,
									vt.soft_lab_num,
									vt.order_num,
									CASE 
										WHEN 	DATE(vt.collected) = "1970-01-01"
										THEN 	NULL
										ELSE 	vt.collected
									END AS collected,
									CASE 
										WHEN 	DATE(vt.received) = "1970-01-01"
										THEN 	NULL
										ELSE 	vt.received
									END AS received,
									vt.test_tissue,
									pt.medical_record_num,
									np.type AS ngs_panel,
									dt.diagnosis,
									CONCAT(pt.first_name," " , pt.middle_name," ",pt.last_name) AS patient_name,
									GROUP_CONCAT(ut.first_name, "->", psv.step ORDER BY psv.time_stamp ASC SEPARATOR ", ") AS editors_per_step,
									
									-- passed steps
									(
										SELECT 	GROUP_CONCAT(psv5.step ORDER BY psv5.time_stamp ASC SEPARATOR ", ") AS steps_passed1

										FROM 		pre_step_visit psv5

										WHERE 		psv5.visit_id =  vt.visit_id AND 
													psv5.status = "passed"

										GROUP BY 		vt.visit_id
									
									)	AS 			steps_passed,

									-- add failed count and remove samples which have failed
								     (
										SELECT 	COUNT(*)

										FROM 	pre_step_visit psv2

										WHERE 	psv2.visit_id =  vt.visit_id AND 
												psv2.status = "Failed"
								     
								     ) 	AS fail_count,

								     -- add flagged count

								     (
								     	SELECT 	psv3.step AS flagged_step

								     	FROM 	pre_step_visit psv3 

								     	WHERE 	psv3.visit_id = vt.visit_id AND 
								     			psv3.status = "Flagged"
								     
								     ) AS flagged_steps,

								     -- remove completed pre
								     (
										SELECT 	COUNT(*)

										FROM 	pre_step_visit psv4

										WHERE 	psv4.visit_id =  vt.visit_id AND 
												psv4.step = "pre_complete"
								     
								     ) 	AS pre_complete_count
								     ,

								     -- remove sent_out
								     (
										SELECT 	COUNT(*)

										FROM 	pre_step_visit psv5

										WHERE 	psv5.visit_id =  vt.visit_id AND 
												psv5.step = "sent_out"
								     
								     ) 	AS sent_out_count,
								     vtcc_vw.problems

							FROM 		visit_table vt
							
							INNER JOIN  	pre_step_visit psv USING(visit_id)
							
							LEFT JOIN 	diagnosis_table dt 
							ON 			vt.diagnosis_id = dt. diagnosis_id

							LEFT JOIN 	patient_table pt 
							ON 			vt.patient_id = pt.patient_id

							LEFT JOIN 	user_table ut
							ON 			ut.user_id = psv.user_id

							LEFT JOIN 	ngs_panel np 
							ON 			np.ngs_panel_id = vt.ngs_panel_id

							LEFT JOIN 	visit_table_concat_comments_vw vtcc_vw
							ON 			vt.visit_id = vtcc_vw.visit_id

							GROUP BY 		vt.visit_id

							ORDER BY 		np.type ASC, vt.visit_id ASC
					) 	as sub1

					WHERE 	sent_out_count <> 0
					';
					break;

				case 'pending_pre_runs_library_pool_visit_id':
					$select = 
					'
					SELECT 		lpt.*,
								vipt.*,
								np.type AS ngs_panel,
								vt.*,
								pt.*,
								psvp.step AS steps_passed,
								psvf.step AS flagged_steps  

					FROM 		library_pool_table lpt 

					LEFT JOIN 	visits_in_pool_table vipt 
					ON 			vipt.library_pool_id = lpt.library_pool_id

					LEFT JOIN 	ngs_panel np 
					ON 			np.ngs_panel_id = "'.$this->sanitize($where['ngs_panel_id']).'"

					LEFT JOIN 	visit_table vt
					ON 			vt.visit_id = vipt.visit_id

					LEFT JOIN 	pre_step_visit psvp
					ON 			psvp.visit_id = vt.visit_id AND 
								psvp.status = "passed" AND 
								psvp.step = "upload_NGS_data"

					LEFT JOIN 	pre_step_visit psvf
					ON 			psvf.visit_id = vt.visit_id AND 
								psvf.status = "Flagged" AND 
								psvf.step = "upload_NGS_data"

					LEFT JOIN 	patient_table pt
					ON 			pt.patient_id = vt.patient_id

					WHERE 		lpt.library_pool_id = "'.$this->sanitize($where['library_pool_id']).'" AND
								vipt.visit_id = "'.$this->sanitize($where['visit_id']).'"';
					
					break;

				case 'pending_pre_runs_library_pool':
					$select =
					'
					SELECT 		lpt.*,
								vipt.*,
								np.type AS ngs_panel,
								vt.*,
								pt.*,
								psvp.step AS steps_passed,
								psvf.step AS flagged_steps  

					FROM 		library_pool_table lpt 

					LEFT JOIN 	visits_in_pool_table vipt 
					ON 			vipt.library_pool_id = lpt.library_pool_id

					LEFT JOIN 	ngs_panel np 
					ON 			np.ngs_panel_id = "'.$this->sanitize($where['ngs_panel_id']).'"

					LEFT JOIN 	visit_table vt
					ON 			vt.visit_id = vipt.visit_id

					LEFT JOIN 	pre_step_visit psvp
					ON 			psvp.visit_id = vt.visit_id AND 
								psvp.status = "passed" AND 
								psvp.step = "upload_NGS_data"

					LEFT JOIN 	pre_step_visit psvf
					ON 			psvf.visit_id = vt.visit_id AND 
								psvf.status = "Flagged" AND 
								psvf.step = "upload_NGS_data"

					LEFT JOIN 	patient_table pt
					ON 			pt.patient_id = vt.patient_id

					WHERE 		lpt.library_pool_id = "'.$this->sanitize($where['library_pool_id']).'"

					';
					break;

				case 'pending-pre-samples':
					$select = 
					'
					SELECT 	vt.visit_id,
							vt.patient_id,
							vt.mol_num,
							vt.time_stamp,
							vt.soft_path_num,
							vt.soft_lab_num,
							vt.order_num,
							vt.test_tissue,
							CASE 
								WHEN 	DATE(vt.collected) = "1970-01-01"
								THEN 	NULL
								ELSE 	vt.collected
							END AS collected,
							CASE 
								WHEN 	DATE(vt.received) = "1970-01-01"
								THEN 	NULL
								ELSE 	vt.received
							END AS received,
							ppsv_vw.steps_passed,
						    fpsv_vw.fail_count,
						    ott.ordered_test_id,
							ott.sample_log_book_id,
							dt.diagnosis,
							pt.medical_record_num,
							CONCAT(pt.first_name," " , pt.middle_name," ",pt.last_name) AS patient_name,
							np.type AS ngs_panel,
							np.ngs_panel_id,
							vtcc_vw.problems,
							flag_psv_vw.flagged_steps,
							pcpsv_vw.pre_complete_count,
							sopsv_vw.sent_out_count,
							epspsv_vw.editors_per_step

					FROM 		visit_table vt								
					
					LEFT JOIN 	passed_pre_step_visit_vw ppsv_vw 
					ON 			vt.visit_id = ppsv_vw.visit_id

					LEFT JOIN 	failed_pre_step_visit_vw fpsv_vw 
					ON 			vt.visit_id = fpsv_vw.visit_id 

					LEFT JOIN 	flagged_pre_step_visit_vw flag_psv_vw 
					ON 			vt.visit_id = flag_psv_vw.visit_id 

					LEFT JOIN 	sent_out_pre_step_visit_vw sopsv_vw 
					ON 			vt.visit_id = sopsv_vw.visit_id 

					LEFT JOIN 	pre_complete_pre_step_visit_vw pcpsv_vw 
					ON 			vt.visit_id = pcpsv_vw.visit_id 

					LEFT JOIN 	ordered_test_table ott 
					ON 			vt.visit_id = ott.visit_id
					
					LEFT JOIN 	diagnosis_table dt 
					ON 			vt.diagnosis_id = dt.diagnosis_id

					LEFT JOIN 	patient_table pt 
					ON 			vt.patient_id = pt.patient_id

					LEFT JOIN 	ngs_panel np 
					ON 			vt.ngs_panel_id = np.ngs_panel_id

					LEFT JOIN 	visit_table_concat_comments_vw vtcc_vw
					ON 			vt.visit_id = vtcc_vw.visit_id

					LEFT JOIN 	editors_per_step_pre_step_visit_vw epspsv_vw 
					ON 			vt.visit_id = epspsv_vw.visit_id

					WHERE 		fpsv_vw.fail_count = 0 AND 
								sopsv_vw.sent_out_count = 0 AND
								pcpsv_vw.pre_complete_count = 0 

					ORDER BY 	vt.visit_id ASC
					';
					break;
				
				case 'pending-pre-samples-ngs-panel-id':
					$select = 
					'
					SELECT 	*
					FROM 	(
							
							SELECT 	vt.visit_id,
									vt.patient_id,
									vt.mol_num,
									vt.time_stamp,
									vt.soft_path_num,
									vt.soft_lab_num,
									vt.order_num,
									ott.ordered_test_id,
									ott.sample_log_book_id,
									CASE 
										WHEN 	DATE(vt.collected) = "1970-01-01"
										THEN 	NULL
										ELSE 	vt.collected
									END AS collected,
									CASE 
										WHEN 	DATE(vt.received) = "1970-01-01"
										THEN 	NULL
										ELSE 	vt.received
									END AS received,
									vt.test_tissue,
									pt.medical_record_num,
									np.type AS ngs_panel,
									dt.diagnosis,						
									CONCAT(pt.first_name," " , pt.middle_name," ",pt.last_name) AS patient_name,
									GROUP_CONCAT(ut.first_name, "->", psv.step ORDER BY psv.time_stamp ASC SEPARATOR ", ") AS editors_per_step,
									np.ngs_panel_id,
									
									
									-- passed steps
									(
										SELECT 	GROUP_CONCAT(psv5.step ORDER BY psv5.time_stamp ASC SEPARATOR ", ") AS steps_passed1

										FROM 		pre_step_visit psv5

										WHERE 		psv5.visit_id =  vt.visit_id AND 
													psv5.status = "passed"

										GROUP BY 		vt.visit_id
									
									)	AS 			steps_passed,

									-- add failed count and remove samples which have failed
								     (
										SELECT 	COUNT(*)

										FROM 	pre_step_visit psv2

										WHERE 	psv2.visit_id =  vt.visit_id AND 
												psv2.status = "Failed"
								     
								    ) 	AS fail_count,
								    (
								     	SELECT 	psv3.step AS flagged_step

								     	FROM 	pre_step_visit psv3 

								     	WHERE 	psv3.visit_id = vt.visit_id AND 
								     			psv3.status = "Flagged"								     
								    ) AS flagged_steps,								     
								    (
										SELECT 	COUNT(*)

										FROM 	pre_step_visit psv4

										WHERE 	psv4.visit_id =  vt.visit_id AND 
												psv4.step = "pre_complete"
								     
								    ) 	AS pre_complete_count,								   
								    (
										SELECT 	COUNT(*)

										FROM 	pre_step_visit psv5

										WHERE 	psv5.visit_id =  vt.visit_id AND 
												psv5.step = "sent_out"
								     
								    ) 	AS sent_out_count,
								    vtcc_vw.problems

							FROM 		visit_table vt
							
							INNER JOIN  pre_step_visit psv USING(visit_id)
												

							LEFT JOIN 	diagnosis_table dt 
							ON 			vt.diagnosis_id = dt. diagnosis_id

							LEFT JOIN 	patient_table pt 
							ON 			vt.patient_id = pt.patient_id

							LEFT JOIN 	user_table ut
							ON 			ut.user_id = psv.user_id

							LEFT JOIN 	ngs_panel np 
							ON 			np.ngs_panel_id = vt.ngs_panel_id

							LEFT JOIN 	visit_table_concat_comments_vw vtcc_vw
							ON 			vt.visit_id = vtcc_vw.visit_id

							LEFT JOIN 	ordered_test_table ott 
							ON 			vt.visit_id = ott.visit_id

							GROUP BY 		vt.visit_id

							ORDER BY 		np.type ASC, vt.visit_id ASC
					) 	as sub1

					WHERE 	fail_count = 0 AND
							sent_out_count = 0 AND
							pre_complete_count = 0 AND ngs_panel_id = "'.$this->sanitize($where).'"
					';				
					break;


				case 'pending-pre-samples-ngs-panel-id-testing':
					$select = 
					'
					SELECT 	vt.visit_id,
							vt.patient_id,
							vt.mol_num,
							vt.time_stamp,
							vt.soft_path_num,
							vt.soft_lab_num,
							vt.order_num,
							vt.test_tissue,
							CASE 
								WHEN 	DATE(vt.collected) = "1970-01-01"
								THEN 	NULL
								ELSE 	vt.collected
							END AS collected,
							CASE 
								WHEN 	DATE(vt.received) = "1970-01-01"
								THEN 	NULL
								ELSE 	vt.received
							END AS received,
							ppsv_vw.steps_passed,
						    fpsv_vw.fail_count,
						    ott.ordered_test_id,
							ott.sample_log_book_id,
							dt.diagnosis,
							pt.medical_record_num,
							CONCAT(pt.first_name," " , pt.middle_name," ",pt.last_name) AS patient_name,
							np.type AS ngs_panel,
							np.ngs_panel_id,
							vtcc_vw.problems,
							flag_psv_vw.flagged_steps,
							pcpsv_vw.pre_complete_count,
							sopsv_vw.sent_out_count,
							epspsv_vw.editors_per_step

					FROM 		visit_table vt								
					
					LEFT JOIN 	passed_pre_step_visit_vw ppsv_vw 
					ON 			vt.visit_id = ppsv_vw.visit_id

					LEFT JOIN 	failed_pre_step_visit_vw fpsv_vw 
					ON 			vt.visit_id = fpsv_vw.visit_id 

					LEFT JOIN 	flagged_pre_step_visit_vw flag_psv_vw 
					ON 			vt.visit_id = flag_psv_vw.visit_id 

					LEFT JOIN 	sent_out_pre_step_visit_vw sopsv_vw 
					ON 			vt.visit_id = sopsv_vw.visit_id 

					LEFT JOIN 	pre_complete_pre_step_visit_vw pcpsv_vw 
					ON 			vt.visit_id = pcpsv_vw.visit_id 

					LEFT JOIN 	ordered_test_table ott 
					ON 			vt.visit_id = ott.visit_id
					
					LEFT JOIN 	diagnosis_table dt 
					ON 			vt.diagnosis_id = dt.diagnosis_id

					LEFT JOIN 	patient_table pt 
					ON 			vt.patient_id = pt.patient_id

					LEFT JOIN 	ngs_panel np 
					ON 			vt.ngs_panel_id = np.ngs_panel_id

					LEFT JOIN 	visit_table_concat_comments_vw vtcc_vw
					ON 			vt.visit_id = vtcc_vw.visit_id

					LEFT JOIN 	editors_per_step_pre_step_visit_vw epspsv_vw 
					ON 			vt.visit_id = epspsv_vw.visit_id

					WHERE 		fpsv_vw.fail_count = 0 AND 
								sopsv_vw.sent_out_count = 0 AND
								pcpsv_vw.pre_complete_count = 0 AND 
								np.ngs_panel_id = "'.$this->sanitize($where).'"  

					ORDER BY 	vt.visit_id ASC
					';				
					break;

				case 'myleoid-run-sheet':
					$select = 
					'
SELECT 	* 

FROM 
(
	SELECT 	vt.visit_id,
			vt.patient_id,
			CONCAT(vt.mol_num,"_",vt.soft_lab_num) AS mol_num_soft_lab,

			CONCAT(pt.first_name," " , pt.middle_name," ",pt.last_name) AS patient_name,
			psv.concentration AS qubit,
	(
		SELECT 	GROUP_CONCAT(psv5.step ORDER BY psv5.time_stamp ASC SEPARATOR ", ") AS steps_passed1

		FROM 		pre_step_visit psv5

		WHERE 		psv5.visit_id =  vt.visit_id AND 
					psv5.status = "passed"

		GROUP BY 		vt.visit_id

	)	AS 			steps_passed

	FROM 	visit_table vt

	LEFT JOIN ngs_panel np
	ON 		np.ngs_panel_id = vt.ngs_panel_id

	LEFT JOIN patient_table pt
	ON 		pt.patient_id = vt.patient_id

	LEFT JOIN pre_step_visit psv
	ON 		psv.visit_id = vt.visit_id AND
			psv.step = "add_DNA_conc"

	WHERE 	np.ngs_panel_id = 2


) as sub


WHERE 	steps_passed LIKE "%add_DNA_conc%" AND
		steps_passed NOT LIKE "%pre_complete%"
';			
					break;	

				case 'version-lot-control-step-regulator':
					$select = 
					'
					SELECT 	*

					FROM 	control_step_regulator_table csrt

					WHERE 	csrt.step_regulator_name = "version_lot"
					';
					break;

				case 'pre-samples-not-in-library-pool-by-ngs-panel':
					$select = 
					'
SELECT 	* 

FROM 
(
	SELECT 	psson_vw.visit_id,
			psson_vw.patient_id,
			psson_vw.order_num,
			psson_vw.mol_num_soft_lab,
			psson_vw.control_type_id,
			psson_vw.sample_type,
			psson_vw.control_name,
			psson_vw.patient_name,
			psson_vw.qubit,
			psson_vw.steps_passed,
			psson_vw.steps_status

	FROM 	pending_samples_substring_order_num_vw psson_vw

	LEFT JOIN ngs_panel np
	ON 		np.ngs_panel_id = psson_vw.ngs_panel_id

	LEFT JOIN patient_table pt
	ON 		pt.patient_id = psson_vw.patient_id

	LEFT JOIN pre_step_visit psv
	ON 		psv.visit_id = psson_vw.visit_id AND
			psv.step = "add_DNA_conc"

	WHERE 	np.ngs_panel_id = "'.$this->sanitize($where).'"

) as sub


WHERE 	steps_status NOT LIKE "%Failed%" AND
		steps_passed NOT LIKE "%pre_complete%" AND
		steps_passed NOT LIKE "%sent_out%" AND
		(sample_type IS NULL OR sample_type NOT LIKE "%Control%")



UNION 	

SELECT 	* 

FROM 
(
	SELECT 	vt.visit_id,
			vt.patient_id,
			vt.order_num,
			CONCAT(vt.mol_num,"_",vt.soft_lab_num) AS mol_num_soft_lab,
			vt.control_type_id,
			vt.sample_type,
			CONCAT(pt.first_name,"_",vt.version) AS control_name,
			CONCAT(pt.first_name," " , pt.middle_name," ",pt.last_name) AS patient_name,
			psv.concentration AS qubit,
	(
		SELECT 	GROUP_CONCAT(psv5.step ORDER BY psv5.time_stamp ASC SEPARATOR ", ") AS steps_passed1

		FROM 		pre_step_visit psv5

		WHERE 		psv5.visit_id =  vt.visit_id AND 
					psv5.status = "passed"

		GROUP BY 		vt.visit_id

	)	AS 			steps_passed,
	(
		SELECT 	GROUP_CONCAT(psv6.status ORDER BY psv6.time_stamp ASC SEPARATOR ", ") AS step_status

		FROM 		pre_step_visit psv6

		WHERE 		psv6.visit_id =  vt.visit_id 

		GROUP BY 		vt.visit_id

	)	AS 			step_status

	FROM 	visit_table vt

	LEFT JOIN ngs_panel np
	ON 		np.ngs_panel_id = vt.ngs_panel_id

	LEFT JOIN patient_table pt
	ON 		pt.patient_id = vt.patient_id

	LEFT JOIN pre_step_visit psv
	ON 		psv.visit_id = vt.visit_id AND
			psv.step = "add_DNA_conc"

	WHERE 	np.ngs_panel_id = "'.$this->sanitize($where).'"

	ORDER BY 	control_name ASC
) as sub


WHERE 	step_status NOT LIKE "%Failed%" AND
		steps_passed NOT LIKE "%pre_complete%" AND
		sample_type LIKE "Negative Control" AND
		steps_passed NOT LIKE "%sent_out%"


UNION 	

SELECT 	* 

FROM 
(
	SELECT 	vt.visit_id,
			vt.patient_id,
			vt.order_num,
			CONCAT(vt.mol_num,"_",vt.soft_lab_num) AS mol_num_soft_lab,
			vt.control_type_id,
			vt.sample_type,
			CONCAT(pt.first_name,"_",vt.version) AS control_name,
			CONCAT(pt.first_name," " , pt.middle_name," ",pt.last_name) AS patient_name,
			psv.concentration AS qubit,
	(
		SELECT 	GROUP_CONCAT(psv5.step ORDER BY psv5.time_stamp ASC SEPARATOR ", ") AS steps_passed1

		FROM 		pre_step_visit psv5

		WHERE 		psv5.visit_id =  vt.visit_id AND 
					psv5.status = "passed"

		GROUP BY 		vt.visit_id

	)	AS 			steps_passed,
	(
		SELECT 	GROUP_CONCAT(psv6.status ORDER BY psv6.time_stamp ASC SEPARATOR ", ") AS step_status

		FROM 		pre_step_visit psv6

		WHERE 		psv6.visit_id =  vt.visit_id 

		GROUP BY 		vt.visit_id

	)	AS 			step_status

	FROM 	visit_table vt

	LEFT JOIN ngs_panel np
	ON 		np.ngs_panel_id = vt.ngs_panel_id

	LEFT JOIN patient_table pt
	ON 		pt.patient_id = vt.patient_id

	LEFT JOIN pre_step_visit psv
	ON 		psv.visit_id = vt.visit_id AND
			psv.step = "add_DNA_conc"

	WHERE 	np.ngs_panel_id = "'.$this->sanitize($where).'"

	ORDER BY 	control_name ASC
) as sub


WHERE 	step_status NOT LIKE "%Failed%" AND
		steps_passed NOT LIKE "%pre_complete%" AND
		steps_passed NOT LIKE "%sent_out%" AND
		sample_type = "Positive Control"

					';	
																	
					break;

				case 'get-interpts-per-ngs-panel':
					$select = 
					'
					SELECT 	ut.first_name, 
							ut.last_name, 
							ct.comment, 
							COUNT(np.ngs_panel_id) AS num_times_used,
							GROUP_CONCAT(np.type) AS ngs_panels,
							GROUP_CONCAT(np.ngs_panel_id) AS ngs_panel_ids

					FROM 	comment_table ct

					LEFT JOIN user_table ut 
					ON 		ut.user_id = ct.user_id

					LEFT JOIN observed_variant_table ovt 
					ON 		ct.comment_id = ovt.interpt_id

					LEFT JOIN run_info_table rit
					ON 		rit.run_id = ovt.run_id

					LEFT JOIN visit_table vt
					ON 		rit.run_id = vt.run_id

					LEFT JOIN ngs_panel np 
					ON 		np.ngs_panel_id = vt.ngs_panel_id

					WHERE 	ct.comment_type = "knowledge_base_table" AND 
							ct.status = "active" 

					GROUP BY 	np.ngs_panel_id,
							ct.comment_id
							
					HAVING 	ngs_panel_ids IS NULL OR
							ngs_panel_ids LIKE "%1%"
					';
			
					break;
					

				case 'pending-pre-samples-by-visit-id':
					$select = 
					'
					SELECT 		vt.visit_id,
								vt.patient_id,
								vt.mol_num,
								vt.order_num,
								vt.time_stamp,
								vt.soft_path_num,
								vt.soft_lab_num,
								vt.collected,
								vt.received,
								vt.test_tissue,
								pt.medical_record_num,
								np.type AS ngs_panel,
								dt.diagnosis,
								CONCAT(pt.first_name," " , pt.middle_name," ",pt.last_name) AS patient_name,
								epspsv_vw.editors_per_step,	
								vtcc_vw.problems, 
								ppsv_vw.steps_passed, 
								fpsv_vw.fail_count,
								flag_psv_vw.flagged_steps,
								pcpsv_vw.pre_complete_count, 
								sopsv_vw.sent_out_count, 
								vtcc_vw.problems

					FROM 		visit_table vt

					LEFT JOIN 	editors_per_step_pre_step_visit_vw epspsv_vw 
					ON 			vt.visit_id = epspsv_vw.visit_id						

					LEFT JOIN 	diagnosis_table dt 
					ON 			vt.diagnosis_id = dt. diagnosis_id

					LEFT JOIN 	patient_table pt 
					ON 			vt.patient_id = pt.patient_id

					LEFT JOIN 	ngs_panel np 
					ON 			np.ngs_panel_id = vt.ngs_panel_id

					LEFT JOIN 	visit_table_concat_comments_vw vtcc_vw 
					ON 			vt.visit_id = vtcc_vw.visit_id

					LEFT JOIN 	passed_pre_step_visit_vw ppsv_vw 
					ON 			vt.visit_id = ppsv_vw.visit_id

					LEFT JOIN 	failed_pre_step_visit_vw fpsv_vw 
					ON 			vt.visit_id = fpsv_vw.visit_id 

					LEFT JOIN 	flagged_pre_step_visit_vw flag_psv_vw 
					ON 			vt.visit_id = flag_psv_vw.visit_id 

					LEFT JOIN 	sent_out_pre_step_visit_vw sopsv_vw 
					ON 			vt.visit_id = sopsv_vw.visit_id 

					LEFT JOIN 	pre_complete_pre_step_visit_vw pcpsv_vw 
					ON 			vt.visit_id = pcpsv_vw.visit_id 


					WHERE 		vt.visit_id = "'.$this->sanitize($where).'" AND 
								fpsv_vw.fail_count = 0 AND sopsv_vw.sent_out_count = 0
					';			
					break;

				case 'variant-comments':
					$select =
					'
					SELECT 		CONCAT(ut.first_name," " ,ut.last_name) AS user_name,
								ct.comment_id,
								ct.comment,
								ct.time_stamp as comment_time_stamp

					FROM 		comment_table ct

					LEFT JOIN 	user_table ut
					ON 			ut.user_id = ct.user_id

					WHERE 		ct.comment_type = "observed_variant" AND
								ct.comment_id = "'.$this->sanitize($where).'"
					';
					break;

				case 'message-board-comments':
					$select =
					'
					SELECT 		CONCAT(ut.first_name," " ,ut.last_name) AS user_name,
								ut.user_id,
								ct.comment_id,
								ct.comment,
								ct.time_stamp as comment_time_stamp

					FROM 		comment_table ct

					LEFT JOIN 	user_table ut
					ON 			ut.user_id = ct.user_id

					WHERE 		ct.comment_type = "message_board" AND
								ct.comment_ref = "'.$this->sanitize($where).'"';
					break;

				case 'comments-with-comment-type-variable':
					$select =
					'
					SELECT 		CONCAT(ut.first_name," " ,ut.last_name) AS user_name,
								ut.user_id,
								ct.comment_id,
								ct.comment,
								ct.time_stamp as comment_time_stamp

					FROM 		comment_table ct

					LEFT JOIN 	user_table ut
					ON 			ut.user_id = ct.user_id

					WHERE 		ct.comment_type = "'.$this->sanitize($where['comment_type']).'" AND
								ct.comment_ref = "'.$this->sanitize($where['comment_ref']).'"';
					break;


				case 'message-board-comments-plus-visit-comments':
					$select =
					'
					SELECT *

					FROM 
					(
					SELECT 		CONCAT(ut.first_name," " ,ut.last_name) AS user_name,
								ut.user_id,
								ct.comment_id,
								ct.comment,
								ct.time_stamp as comment_time_stamp

					FROM 		run_info_table rit

					LEFT JOIN 	visit_table vt
					ON 			vt.run_id = rit.run_id

					LEFT JOIN 	comment_table ct
					ON 			vt.visit_id = ct.comment_ref AND 
								ct.comment_type = "add_visit"

					LEFT JOIN 	user_table ut
					ON 			ut.user_id = ct.user_id

					WHERE 		rit.run_id = "'.$this->sanitize($where).'"

					UNION 		
					
					SELECT 		CONCAT(ut.first_name," " ,ut.last_name) AS user_name,
								ut.user_id,
								ct.comment_id,
								ct.comment,
								ct.time_stamp as comment_time_stamp

					FROM 		comment_table ct

					LEFT JOIN 	user_table ut
					ON 			ut.user_id = ct.user_id

					WHERE 		ct.comment_type = "message_board" AND
								ct.comment_ref = "'.$this->sanitize($where).'"
					) AS joined
					
					ORDER BY 		joined.comment_time_stamp ';
				
					break;

				// get knowledge comments
				case 'knowledge-comments':
					$select =
					'
					SELECT 		ut.first_name, 
								ut.last_name,
								ct.comment_id,
								ct.comment,
								ct.comment_ref,
								ct.time_stamp as comment_time_stamp

					FROM 		comment_table ct

					LEFT JOIN 	user_table ut
					ON 			ut.user_id = ct.user_id

					WHERE 		ct.comment_type = "knowledge_base_table" AND
								ct.comment_ref = "'.$this->sanitize($where).'"
					';
					break;
				
				case 'knowledge-comments-active':
					$select =
					'
					SELECT 		ut.first_name, 
								ut.last_name,
								ct.comment_id,
								ct.comment,
								ct.comment_ref,
								ct.time_stamp as comment_time_stamp

					FROM 		comment_table ct

					LEFT JOIN 	user_table ut
					ON 			ut.user_id = ct.user_id

					WHERE 		ct.comment_type = "knowledge_base_table" AND
								ct.comment_ref = "'.$this->sanitize($where).'" AND 
								ct.status = "active"
					';
					break;
				case 'knowledge-comments-join-observed':
					$select =
					'
					SELECT 		ut.first_name,
								ut.last_name,
								ct.comment_id,
								ct.comment,
								ct.time_stamp as comment_time_stamp

					FROM 		observed_variant_table ovt

					JOIN 		knowledge_base_table kbt
					ON 			ovt.genes = kbt.genes AND 
								ovt.coding = kbt.coding AND 
								ovt.amino_acid_change = kbt.amino_acid_change

					LEFT JOIN	 	comment_table ct
					ON 			kbt.knowledge_id = ct.comment_ref

					LEFT JOIN 	user_table ut

					ON 			ut.user_id = ct.user_id

					WHERE 		ct.comment_type = "knowledge_base_table" AND 
								ovt.genes = "'.$this->sanitize($where['genes']).'" AND 
								ovt.coding = "'.$this->sanitize($where['coding']).'" AND 
								ovt.amino_acid_change = "'.$this->sanitize($where['amino_acid_change']).'" AND
								ovt.run_id = "'.$this->sanitize($where['run_id']).'"

					ORDER BY 		ct.time_stamp DESC
					';
					
					break;

				case 'comment-by-id':
					$select =
					'
					SELECT 	comment

					FROM 	comment_table

					WHERE 	comment_id = "'.$this->sanitize($where).'"
					';
					break;
				case 'comment-by-ref-and-comment':
					$select =
					'
					SELECT 	ct.comment,
							ct.time_stamp,
							ct.comment_id,
							CONCAT(ut.first_name," " ,ut.last_name) AS user_name

					FROM 	comment_table ct

					JOIN 	user_table ut
					ON 		ut.user_id = ct.user_id

					WHERE 	ct.comment_ref = "'.$this->sanitize($where['comment_ref']).'" AND 
							ct.comment = "'.$this->sanitize($where['comment']).'" 
					';
					break;
				case 'knowledge-tier':
					$select = 
					'
					SELECT 	
							vt_xref_id,
							tier,
							tier_summary,
							tissue				

					FROM 	tier_tissue_knowledge_vw

					WHERE 	knowledge_id = "'.$this->sanitize($where).'"
					';
					break;

				case 'summary-saved':
					$select =
					'
					SELECT 		*

					FROM 		no_snv_summary_table

					WHERE 		summary = "'.$this->sanitize($where).'"
					';

					break;

				case 'all-summary-saved':
					$select =
					'
					SELECT 		nsst.*,
								CONCAT(ut.first_name," " ,ut.last_name) AS user_name

					FROM 		no_snv_summary_table nsst

					LEFT JOIN 	user_table ut
					ON 			ut.user_id = nsst.user_id

					ORDER BY 		nsst.time_stamp DESC
					';

					break;		

				case 'tier-user':
					$select = 
					'	
					SELECT         vtx.vt_xref_id,
								ttt.tissue,
					               CONCAT(ut.first_name," " ,ut.last_name) AS user_name,
					               vtx.time_stamp,
					               tt.tier,
					               tt.tier_summary,
					               vtx.report_link

					FROM           variant_tier_xref vtx

					LEFT JOIN      tier_table tt
					ON             vtx.tier_id = tt.tier_id

					LEFT JOIN      user_table ut
					ON             ut.user_id = vtx.user_id

					LEFT JOIN      tissue_type_table ttt
					ON             ttt.tissue_id = vtx.tissue_id

					WHERE          vtx.knowledge_id = "'.$this->sanitize($where).'"

					ORDER BY 		tt.tier ASC, ttt.tissue ASC 
					';
					
					break;

				case 'tier-knowledge-id-overview':
					$select =
					'
					SELECT 		tt.tier,
								COUNT(tt.tier) AS count

					FROM           variant_tier_xref vtx

					LEFT JOIN      tier_table tt
					ON             vtx.tier_id = tt.tier_id

					WHERE          vtx.knowledge_id = "'.$this->sanitize($where).'" 

					GROUP BY 		tt.tier

					ORDER BY 		tt.tier ASC
					';
					break;

				case 'tier-by-vt-xref':
					$select = 
					'	
					SELECT         vtx.vt_xref_id,
								ttt.tissue,
					               CONCAT(ut.first_name," " ,ut.last_name) AS user_name,
					               vtx.time_stamp,
					               tt.tier,
					               tt.tier_summary

					FROM           variant_tier_xref vtx

					JOIN           tier_table tt
					ON             vtx.tier_id = tt.tier_id

					JOIN           user_table ut
					ON             ut.user_id = vtx.user_id

					JOIN           tissue_type_table ttt
					ON             ttt.tissue_id = vtx.tissue_id

					WHERE          vtx.vt_xref_id = "'.$this->sanitize($where).'"

					ORDER BY 		tt.tier ASC, ttt.tissue ASC 
					';
					break;

				case 'knowledge-by-id':
					$select = 
					'
					SELECT 	*

					FROM 	knowledge_base_table

					WHERE 	knowledge_id = "'.$this->sanitize($where).'"
					';
					break;
				
				case 'search-knowledge-for-gene-snvs':
					$select = 
					'	
					SELECT 		  	* 

					FROM 			knowledge_base_table 

					WHERE 			genes = "'.$this->sanitize($where).'"';

					break;

				case 'search-knowledge':
					$select = 
					'	
					SELECT DISTINCT  	kbt.* 

					FROM 				knowledge_base_table kbt

					WHERE 			';
					
					$select = $this->MakeWhereClauseFromArray($select, $where);
					break;

				case 'search-knowledge-by-genes-tissue':
					$select = 
					'	
					SELECT 	kbt.*,
							vt.test_tissue,
							ovt.frequency,
							ovt.genotype,
							ovt.allele_coverage,
							ovt.coverage,
							ovt.tier,
							ovt.variant_allele,
							ovt.ref_allele,
							ovt.ref_var_strand_counts,
							ovt.strand,
							ovt.genetic_call,
							vt.visit_id,
							rit.run_id,
							vt.patient_id,
							vt.ngs_panel_id

					FROM 	visit_table vt

					JOIN 	ordered_test_table ott 
					ON 		vt.visit_id = ott.visit_id

					JOIN 	sample_log_book_table slbt 
					ON 		slbt.sample_log_book_id = ott.sample_log_book_id

					JOIN 	run_info_table rit 
					ON 		rit.run_id = vt.run_id 

					JOIN 	observed_variant_table ovt 
					ON 		ovt.run_id = rit.run_id

					JOIN 	knowledge_base_table kbt 
					ON 		ovt.genes = kbt.genes AND 
							ovt.coding =  kbt.coding AND 
							ovt.amino_acid_change = kbt.amino_acid_change

					WHERE 	ovt.include_in_report = "1" AND 
							ovt.genes = "'.$this->sanitize($where['genes']).'" AND 
							slbt.test_tissue = "'.$this->sanitize($where['test_tissue']).'"
					';
					break;

				case 'knowledge-base-failed-filter':
					$select =
					'
					SELECT 		kbt.*,
								GROUP_CONCAT(gcvpt.accession_num) AS accession_num,
								vt.ngs_panel_id,
								COUNT(gcvpt.accession_num) AS accession_num_count,
								CONCAT("https://varsome.com/variant/hg19/", gcvpt.accession_num, "(",ovt.genes,")%3A",ovt.coding) AS varsome_accession_link,
								CONCAT("https://mutalyzer.nl/name-checker?description=",gcvpt.accession_num, ":",ovt.coding) AS mutalyzer_accession_link,
								CONCAT("https://mutalyzer.nl/name-checker") AS mutalyzer_simple_link,
								CONCAT("https://varsome.com/variant/hg19/",ovt.coding, "%20",ovt.coding) AS varsome_simple_link

					FROM		observed_variant_table ovt

					JOIN 		knowledge_base_table kbt
					ON 			ovt.genes = kbt.genes AND ovt.coding = kbt.coding AND ovt.amino_acid_change = kbt.amino_acid_change

					LEFT JOIN 	visit_table vt 
					ON 			ovt.run_id = vt.run_id

					LEFT JOIN 	genes_covered_by_panel_table gcvpt 
					ON 			ovt.genes = gcvpt.gene AND
								gcvpt.ngs_panel_id = vt.ngs_panel_id

					WHERE 		ovt.run_id = "'.$this->sanitize($where).'" AND ovt.filter_status = "failed"

					GROUP BY 	gcvpt.gene 
					';
					break;

				case 'search-observed-variants':
					$select =
					'
					SELECT 		ovt.run_id,
								ovt.genes,
								ovt.amino_acid_change,
								ovt.coding,
								ovt.tier,
								ovt.include_in_report,
								rit.status,
								rit.run_date_chip,
								rit.sample_name,
								vt.visit_id,
								pt.patient_id,
								vt.ngs_panel_id,
								np.type AS panel

					FROM 		observed_variant_table ovt

					JOIN 		run_info_table rit
					ON 			ovt.run_id = rit.run_id

					JOIN 		visit_table vt
					ON 			vt.run_id = ovt.run_id

					LEFT JOIN 	ngs_panel np
					ON 			np.ngs_panel_id = vt.ngs_panel_id

					JOIN 		patient_table pt 
					ON 			vt.patient_id = pt.patient_id

					WHERE 	';	

					$select = $this->MakeWhereClauseFromArray($select, $where, 'rit', 'ovt');
			
					$select .= ' AND rit.status = "completed"';		
					break;

				case 'search-single-analyte-pool-table':
					$select =
					'
					SELECT 		sapt.*,
								CONCAT(ut.first_name," " ,ut.last_name) AS user_name,
								otst.test_name,
								GROUP_CONCAT(ct.comment SEPARATOR ", ") as comments

					FROM 		single_analyte_pool_table sapt

					LEFT JOIN 	user_table ut 
					ON 			ut.user_id = sapt.user_id

					LEFT JOIN 	orderable_tests_table otst
					ON 			otst.orderable_tests_id = sapt.orderable_tests_id

					LEFT JOIN 	comment_table ct
					ON 			sapt.single_analyte_pool_id = ct.comment_ref AND 
								ct.comment_type = "single_analyte_pool_table"

					WHERE 	';	

					$select = $this->MakeWhereClauseFromArray($select, $where, 'sapt');					

					$select .= ' GROUP BY 	ct.comment_ref,
								sapt.single_analyte_pool_id';

					if (isset($where['tech']))
					{
						$select .= ' HAVING user_name LIKE "%'.$where['tech'].'%"';
					}													
					break;

				case 'search-patient-reports':
					$select =
					'
						SELECT 		cpr_vw.*, 
									otst.test_name,
									np.type AS NGS_panel,
									nrvpr_vw.num_reported_variants

						FROM 		completed_patient_reports_vw  cpr_vw

						LEFT JOIN 	ordered_test_table ott 
						ON 			ott.visit_id = cpr_vw.visit_id 

						LEFT JOIN 	orderable_tests_table otst 
						ON 			otst.orderable_tests_id = ott.orderable_tests_id

						LEFT JOIN 	ngs_panel np
						ON 			np.ngs_panel_id = cpr_vw.ngs_panel_id

						LEFT JOIN 	num_reported_variants_per_run_id_vw nrvpr_vw
						ON 			cpr_vw.run_id = nrvpr_vw.run_id

						WHERE 	';
					
					$where_keys = array_keys($where);

					$select = $this->MakeWhereClauseFromArray($select, $where);
		
					break;

				case 'tissue-gene-stats':
					$select =
					'
					SELECT slbt.test_tissue, ovt.genes, COUNT(vt.visit_id) AS count

					FROM visit_table vt

					JOIN ordered_test_table ott ON vt.visit_id = ott.visit_id

					JOIN sample_log_book_table slbt ON slbt.sample_log_book_id = ott.sample_log_book_id

					JOIN run_info_table rit ON rit.run_id = vt.run_id 

					JOIN observed_variant_table ovt ON ovt.run_id = rit.run_id

					WHERE ovt.include_in_report = "1"

					GROUP BY slbt.test_tissue, ovt.genes
					';	
					break;

				case 'search-patient-reports-without-variants':
					unset($where['no_variants_found']);

					$select =
					'	
					SELECT 		cpr_vw.*, 
								otst.test_name,
								np.type AS NGS_panel,
								rit.status, "0" AS num_reported_variants

					FROM 		completed_patient_reports_vw cpr_vw 

					LEFT JOIN 	ordered_test_table ott 
					ON 			ott.visit_id = cpr_vw.visit_id 

					LEFT JOIN 	orderable_tests_table otst 
					ON 			otst.orderable_tests_id = ott.orderable_tests_id

					LEFT JOIN 	ngs_panel np
					ON 			np.ngs_panel_id = cpr_vw.ngs_panel_id

					JOIN run_info_table rit ON rit.run_id = cpr_vw.run_id

					WHERE rit.status = "completed"  AND NOT EXISTS ( SELECT nrvpr_vw2.run_id FROM num_reported_variants_per_run_id_vw nrvpr_vw2 WHERE nrvpr_vw2.run_id = cpr_vw.run_id)
					';

					if (!empty($where))
					{

						$where_keys = array_keys($where);

						$select.=' AND ';

						$select = $this->MakeWhereClauseFromArray($select, $where);
					}
					break;

				case 'find-variant-tier-xref':
					$select =
					'
					SELECT 	vt_xref_id

					FROM 	variant_tier_xref

					WHERE 	tier_id ="'.$this->sanitize($where['tier_id']).'"	AND
							knowledge_id ="'.$this->sanitize($where['knowledge_id']).'"		
					';					
					if ($where['tissue_id'] !== '')
					{
						$select .= 
						'		AND
								tissue_id ="'.$this->sanitize($where['tissue_id']).'"';
					}
					
					break;

				case 'find-variant-tissue-xref':
					$select =
					'
					SELECT 	vt_xref_id

					FROM 	variant_tier_xref

					WHERE 	knowledge_id ="'.$this->sanitize($where['knowledge_id']).'" AND
							tissue_id ="'.$this->sanitize($where['tissue_id']).'"		
					';			
					break;

				case 'get-table-col-names':
					$select =
					'
					SELECT DISTINCT 	COLUMN_NAME 

					FROM 			information_schema.columns 

					WHERE 			table_name = "'.$this->sanitize($where).'"
					';
					
					break;

				case 'unique-gene-list':
					$select = 
					'
					SELECT DISTINCT 	genes

					FROM 			knowledge_base_table kbt

					ORDER BY 			kbt.genes ASC
					';
					break;

				case 'user-exist':
					$select = 
					'
					SELECT 			*

					FROM 			user_table ut

					WHERE 			ut.email_address = "'.$this->sanitize($where['email_address']).'" AND 
									ut.password = "'.$this->sanitize($where['password']).'"
					';
					break;					

				case 'user-permissions':
					$select = 
					'
					SELECT 		GROUP_CONCAT(pt.permission SEPARATOR ", ") AS permissions

					FROM 		user_table ut

					LEFT JOIN 	permission_user_xref pux
					ON 			pux.user_id = ut.user_id

					LEFT JOIN 	permission_table pt
					ON 			pux.permission_id = pt.permission_id

					WHERE 		ut.user_id = "'.$this->sanitize($where).'"

					GROUP BY 		ut.user_id
					';
					break;

				case 'log-book-sample-type':
					$select =
					'
					SELECT 		slbt.sample_type_toggle 

					FROM 		visit_table vt

					LEFT JOIN 	sample_log_book_table slbt 
					ON 			slbt.mol_num = vt.mol_num

					WHERE 		vt.visit_id = "'.$this->sanitize($where).'"
					';
					break;

				case 'all-users-with-permission':
					$select =
					'
					SELECT 	GROUP_CONCAT(CONCAT_WS(" ",ut.first_name, ut.last_name, ut.email_address) SEPARATOR ", ") AS users

					FROM 	permission_table pt

					LEFT JOIN permission_user_xref pux
					ON 		pux.permission_id = pt.permission_id

					LEFT JOIN user_table ut
					ON 		ut.user_id = pux.user_id

					WHERE 	pt.permission = "'.$this->sanitize($where).'"

					';				
					break;

				case 'users-review-permissions':
					$select = 
					'
					SELECT 		ut.user_id,
								ut.first_name,
								ut.last_name,
								ut.email_address

					FROM 		user_table ut

					LEFT JOIN 	permission_user_xref pux
					ON 			pux.user_id = ut.user_id

					LEFT JOIN 	permission_table pt
					ON 			pux.permission_id = pt.permission_id 

					WHERE		pt.permission = "reviewer"
					';
					break;
				
				case 'users-permissions-non-ngs-wet-bench':
					$select = 
					'
					SELECT 		ut.user_id,
								ut.first_name,
								ut.last_name,
								ut.email_address,
								CONCAT(SUBSTRING(ut.first_name, 1,1), "", SUBSTRING(ut.last_name, 1,1)) AS initials

					FROM 		user_table ut

					LEFT JOIN 	permission_user_xref pux
					ON 			pux.user_id = ut.user_id

					LEFT JOIN 	permission_table pt
					ON 			pux.permission_id = pt.permission_id 

					WHERE		pt.permission = "non_ngs_wet_ben"

					ORDER BY 		initials
					';
					break;

				case 'users-director-permissions':
					$select = 
					'
					SELECT 		ut.user_id,
								ut.first_name,
								ut.last_name,
								ut.email_address

					FROM 		user_table ut

					LEFT JOIN 	permission_user_xref pux
					ON 			pux.user_id = ut.user_id

					LEFT JOIN 	permission_table pt
					ON 			pux.permission_id = pt.permission_id 

					WHERE		pt.permission = "director"
					';
					break;

				case 'get-permission-id':
					$select = 
					'
					SELECT 	* 

					FROM 	permission_table

					WHERE 	permission = "'.$this->sanitize($where).'"
					';
					break;

				case 'get-all-users-with-a-permission':
					$select =
					'
					SELECT 	pt.permission,
							CONCAT(ut.first_name," " ,ut.last_name) AS user_name,
							ut.email_address 

					FROM 	permission_table pt

					LEFT JOIN permission_user_xref pux
					ON 		pux.permission_id = pt.permission_id

					LEFT JOIN user_table ut
					ON 		ut.user_id = pux.user_id

					WHERE 	pt.permission = "'.$this->sanitize($where).'"
					';				
					break;

				case 'user':
					$select = 
					'
					SELECT 		ut.*,
								CONCAT(first_name," " ,last_name) AS user_name

					FROM 		user_table ut

					WHERE 		ut.user_id = "'.$this->sanitize($where).'"
					';
					break;

				case 'user-initials':
					$select = 
					'
					SELECT 	CONCAT(SUBSTRING(ut.first_name, 1,1), "", SUBSTRING(ut.last_name, 1,1)) AS initials

		
					FROM 	user_table ut

					WHERE 	ut.user_id = "'.$this->sanitize($where).'"
					';				
					break;

				case 'confirmed-variants-minimum-not-met':
					$select = 
					'
					SELECT 		ovt.genes,
								ovt.coding,
								ovt.amino_acid_change,
								ovt.observed_variant_id,

								cc_vw.mutation_type,
								COALESCE(cc_vw.confirmation_count, 0) AS current_confirmation_count,
								COALESCE(cc_vw.met_minimium, "no") AS met_minimium_yes_no,	
								cc_vw.met_minimium,
								kbt.mutation_type,
								mtr.snp_or_indel
								
					FROM 		run_info_table rit

					LEFT JOIN		visit_table vt
					ON 			vt.run_id = rit.run_id

					LEFT JOIN		observed_variant_table ovt
					ON  			rit.run_id = ovt.run_id

					LEFT JOIN 	knowledge_base_table kbt
					ON 			kbt.genes = ovt.genes AND
								kbt.coding = ovt.coding AND
								kbt.amino_acid_change = ovt.amino_acid_change

					LEFT JOIN		mutation_type_ref mtr
					ON 			mtr.cosmic_mutation_type = kbt.mutation_type

					LEFT JOIN 	confirmed_count_vw cc_vw
					ON 			cc_vw.genes = ovt.genes AND
								cc_vw.mutation_type = mtr.snp_or_indel AND 
								cc_vw.ngs_panel_id = "'.$this->sanitize($where['ngs_panel_id']).'"

					WHERE 		ovt.include_in_report = 1 AND
								ovt.run_id = "'.$this->sanitize($where['run_id']).'" AND
								vt.ngs_panel_id = "'.$this->sanitize($where['ngs_panel_id']).'" AND (
								cc_vw.met_minimium = "no" OR ISNULL(cc_vw.met_minimium))

					ORDER BY 		confirm_status DESC
					';
		
					break;

				case 'get-ngs-panel-id':
					$select = 
					'
					SELECT 	ngs_panel_id

					FROM 	run_info_table rit

					JOIN 	visit_table vt
					ON 		rit.run_id = vt.run_id

					WHERE 	rit.run_id = "'.$this->sanitize($where).'"
					';
					break;

				case 'get-confirm-info-for-gene':
					$select = 
					'
					SELECT 	*

					FROM 	confirmed_count_vw cc_vw 

					WHERE 	cc_vw.ngs_panel_id = "'.$this->sanitize($where["ngs_panel_id"]).'" AND
							cc_vw.genes = "'.$this->sanitize($where["genes"]).'" AND
							cc_vw.mutation_type = "'.$this->sanitize($where["snp_or_indel"]).'"
					';					
					break;

				case 'confirmed-by-observed-variant-id':
					$select =
					'
					SELECT 	*

					FROM 	confirmed_table ct

					WHERE 	ct.observed_variant_id = "'.$this->sanitize($where).'"
					';
					break;

				case 'confirm-status-with-ovt-info':
					$select = 
					'
					SELECT 	ovt.genes,
							ovt.coding,
							ovt.amino_acid_change,
							ct.mutation_type,
							IF(ct.confirmation_status = "0", "pending", IF(ct.confirmation_status = "1", "confirmed", IF(ct.confirmation_status = "2", "not confirmed", "error") ) ) AS confirmation_status,
							CONCAT(ut.first_name," " ,ut.last_name) AS user_name

					FROM 	run_info_table rit	

					JOIN 	observed_variant_table ovt	
					ON 		rit.run_id = ovt.run_id

					JOIN 	confirmed_table ct
					ON 		ovt.observed_variant_id = ct.observed_variant_id

					JOIN 	user_table ut
					ON 		ut.user_id = ct.user_id

					WHERE 	rit.run_id = "'.$this->sanitize($where).'"
					';
					break;

				case 'pending-confirmations':
					$select = 
					'
					SELECT 		ct.confirm_id,
								ct.genes,
								ovt.coding,
								ovt.amino_acid_change,
								ct.mutation_type,
								ct.confirmation_status,				
								rit.run_id,
								pt.patient_id,
								vt.visit_id,			
								np.type,
								CONCAT(ut.first_name," " ,ut.last_name) AS user_name

					FROM 		confirmed_table ct

					JOIN 		ngs_panel np
					ON 			np.ngs_panel_id = ct.ngs_panel_id

					LEFT JOIN 	observed_variant_table ovt 
					ON 			ovt.observed_variant_id = ct.observed_variant_id

					LEFT JOIN 	run_info_table rit 
					ON 			rit.run_id = ovt.run_id

					LEFT JOIN 	visit_table vt
					ON 			vt.run_id = rit.run_id

					LEFT JOIN 	patient_table pt 
					ON 			vt.patient_id = pt.patient_id

					JOIN 		user_table ut
					ON 			ut.user_id = ct.user_id

					WHERE 		ct.confirmation_status = 0

					ORDER BY 		ct.time_stamp DESC
					';
					break;
				case 'pending-confirmations-by-id':
					$select = 
					'
					SELECT 	ct.*,
							np.type

					FROM 	confirmed_table ct

					JOIN 	ngs_panel np
					ON 		np.ngs_panel_id = ct.ngs_panel_id

					WHERE 	ct.confirmation_status = 0 AND 
							ct.confirm_id = "'.$this->sanitize($where).'"
					';
					break;
				case 'confirmed-count-vw':
					$select =
					'
					SELECT 	cc_vw.genes,
							cc_vw.mutation_type,
							cc_vw.confirmation_count,
							cc_vw.met_minimium,
							np.type 

					FROM 	confirmed_count_vw cc_vw

					JOIN 	ngs_panel np
					ON 		np.ngs_panel_id = cc_vw.ngs_panel_id

					';
					break;
				case 'variant-strand-bias':
					$select = 
					'
					SELECT 	* 

					FROM 	strand_bias_table sbt 

					WHERE 	sbt.strand_bias_id = "'.$this->sanitize($where).'"
					';	
					break;
				case 'training-log':
					$select =
					'
					SELECT 	*

					FROM 	training_table

					WHERE 	user_id = "'.$this->sanitize($where).'"
					';
					break;


				case 'qc-strand-bias':
					$select = 
					'
					SELECT 	sbt.user_id AS sb_user_id,
							sbt.strand_bias_id,
							sbt.ref_pos,
							sbt.ref_neg,
							sbt.variant_neg,
							sbt.variant_pos,
							ovt.observed_variant_id,
							ovt.genes,
							ovt.coding,
							ovt.amino_acid_change,
							ovt.variant_allele,
							ovt.ref_allele,
							ovt.ref_var_strand_counts,
							ovt.strand 

					FROM 	run_info_table rit

					LEFT JOIN observed_variant_table ovt
					ON 		ovt.run_id = rit.run_id

					LEFT JOIN strand_bias_table sbt 
					ON 		sbt.observed_variant_id = ovt.observed_variant_id

					WHERE 	rit.run_id = "'.$this->sanitize($where).'"

					';

					break;
				case 'reviewers-emailed':
					$select =
					'
					SELECT 	CONCAT(ut_r.first_name," " ,ut_r.last_name) AS recipient_name,
							CONCAT(ut_s.first_name," " ,ut_s.last_name) AS sender_name,
							seet.time_stamp

					FROM 	sent_email_table seet

					LEFT JOIN user_table ut_r 
					ON 		ut_r.user_id = seet.recipient_user_id

					LEFT JOIN user_table ut_s 
					ON 		ut_s.user_id = seet.sender_user_id

					WHERE 	seet.email_type = "reviewers_emailed" AND
							seet.ref_table = "visit_table" AND
							seet.ref_id = "'.$this->sanitize($where).'"

					ORDER BY 	seet.time_stamp ASC, 
							seet.sent_email_id ASC
					';				
					break;
				case 'directors-notifying-reviewers':
					$select =
					'
					SELECT DISTINCT seet.sender_user_id AS user_id,
								CONCAT(ut_s.first_name," " ,ut_s.last_name) AS user_name,
								ut_s.email_address

					FROM 	sent_email_table seet

					LEFT JOIN user_table ut_s 
					ON 		ut_s.user_id = seet.sender_user_id

					WHERE 	seet.email_type = "reviewers_emailed" AND
							seet.ref_table = "visit_table" AND
							seet.ref_id = "'.$this->sanitize($where).'"
					';							
					break;
				case 'all-reporting-flow-steps':
					$select =
					'
					SELECT 	*

					FROM 	reporting_flow_steps_table
					';
					break;

				case 'reporting-flow-steps-by-id':
					$select =
					'
					SELECT 		*

					FROM 		reporting_flow_steps_table 

					WHERE 		reporting_flow_steps_id = "'.$this->sanitize($where).'"
					';
					break;

				case 'reporting-flow-steps-by-test-id':
					$select =
					'
					SELECT 		rsbtt.*,
								rfst.step_name, 
								rfst.no_variant_status,
								rfst.description,
								rfst.flow_type,
								rfst.step_status

					FROM 		reporting_steps_by_test_table rsbtt 

					LEFT JOIN 	reporting_flow_steps_table rfst 
					ON 			rfst.reporting_flow_steps_id = rsbtt.reporting_flow_steps_id

					WHERE 		rsbtt.orderable_tests_id = "'.$this->sanitize($where).'"

					ORDER BY 	step_order ASC
					';
					break;

				case 'reporting-flow-steps-by-ngs-panel-id':
					$select =
					'
					SELECT 		rsbtt.*,
								rfst.step_name, 
								rfst.no_variant_status,
								rfst.description,
								rfst.flow_type,
								rfst.step_status

					FROM 		reporting_steps_by_test_table rsbtt 

					LEFT JOIN 	reporting_flow_steps_table rfst 
					ON 			rfst.reporting_flow_steps_id = rsbtt.reporting_flow_steps_id

					WHERE 		rsbtt.ngs_panel_id = "'.$this->sanitize($where).'"

					ORDER BY 	step_order ASC
					';
					break;
					


			}

			if(isset($select))
			{
				$results = array();

				// perform query
				$searchResult = mysqli_query($this->conn, $select);

				if(!$searchResult)
				{
					return false;
				}

				// make results array
				else
				{

					while($row = mysqli_fetch_array($searchResult, $result=MYSQLI_ASSOC))
					{

						$results[] = $row;
					}

					return $results;
				}
			}
		}

		private function MakeWhereClauseFromArray($select, $where, $date_table_name='', $radio_filter_table_name='')
		{
			$start_select = $select;

			$where_keys = array_keys($where);

			if ($date_table_name !== '')
			{
				$date_table_name .= '.';
			}
			if ($radio_filter_table_name !== '')
			{
				$radio_filter_table_name .= '.';
			}

			for ($i = 0; $i < sizeOf($where_keys); $i++)
			{
				$key = $where_keys[$i];
				$val = $where[$key];

				if ($date_table_name == 'sapt.' && $key === 'start_date')
				{
					$select .= $date_table_name.'start_date >=  STR_TO_DATE("'.$this->sanitize($val).'", "%Y-%m-%d")';
				}				
				else if ($key === 'start_date')
				{
					$select .= $date_table_name.'run_date >=  STR_TO_DATE("'.$this->sanitize($val).'", "%m/%d/%Y")';
				}

				else if ($date_table_name == 'sapt.' && $key === 'end_date')
				{
					$select .= $date_table_name.'start_date <= STR_TO_DATE("'.$this->sanitize($val).'", "%Y-%m-%d")';
				}

				else if ($key === 'end_date')
				{
					$select .= $date_table_name.'run_date <= STR_TO_DATE("'.$this->sanitize($val).'", "%m/%d/%Y")';
				}

				else if ($key === 'radio_filter')
				{
					if ($val !== 'all' && $val !== 'included')
					{
						$select .= $radio_filter_table_name.'filter_status = "'.$this->sanitize($val).'"';
					}
					else if ($val === 'included')
					{
						$select .= $radio_filter_table_name.'include_in_report = 1';
					}
				}

				else if ($key === 'genes' || $key === 'coding' || $key === 'amino_acid_change')
				{
					$val_l = explode(', ', $val);

					for ($j=0; $j < sizeOf($val_l); $j++)
					{
						$select .=  $this->sanitize($key).' LIKE "%'.$this->sanitize($val_l[$j]).'%"';

						if ($j < sizeOf($val_l)-1)
						{
							$select .= ' AND ';
						}
					}

				}

				else if ($key === 'tech')
				{
					continue;
				}
				else
				{
					$select .=  $this->sanitize($key).' LIKE "%'.$this->sanitize($val).'%"';
				}

				if ($i < sizeOf($where_keys) - 1 && $key !== 'page')
				{

					// make sure this key and next key is not equal to radio_filter and all
					if (! ($key === 'radio_filter' && $val === 'all')  && 
						!($where_keys[$i+1] === 'radio_filter' && $where[$where_keys[$i+1]] === 'all') )
					{
						$select .= ' AND ';
					}
					
				}
			}

			// If nothing was added remove where.
			if ($start_select === $select)
			{
				$select = str_replace('WHERE', '', $select);
			}


			return $select;
		}


		public function ExportGridCSV($query, $where=NULL)
		{
			switch($query)
			{

				case 'box-grid':
					$select =
					'
					SELECT         	ti.sampleName,
									ti.xLoc,
									ti.yLoc,
									bt.boxName,
									bt.xSize

					FROM           tube_info_vw ti

					LEFT JOIN 		boxTable bt
					ON				ti.boxID = bt.boxID

					WHERE          ti.boxID = "'.$this->sanitize($where).'"
					ORDER BY ti.xLoc, ti.yLoc
					';
					break;
			}
			if(isset($select))
			{
				// perform query
				$searchResult = mysqli_query($this->conn, $select);

				if(!$searchResult)
				{
					return false;
				}

				// make grid array
				else
				{
					$search_array = array();
					$results = array();
					$i = 0;
					while ($row = mysqli_fetch_array($searchResult))
					{
						if ($i == 0)
						{
							$box_size = (int)$row['xSize'];
							$results['box_size'] = $box_size;
							$box_name = $row['boxName'];
							$results['box_name'] = $box_name;

						}

						$i++;

						$search_array[] = $row;
					}

					// make an empty square matrix of the size of the box
					$square_matrix = array();
					$alphabet = range('A', 'Z');

					for($i=0; $i < $box_size+1; $i++)
					{
						// if $i is 0 change index make the first row list of numbers the size of the box
						if ($i === 0)
						{
							$curr_array = range(0,$box_size);
						}
						// Make empty array the size of the size of the box
						else
						{
							$curr_array = array_fill(0, $box_size+1, 0);
							$curr_array[0] = $alphabet[$i-1];

							for ($k=0; $k<sizeof($search_array); $k++)
							{
								$curr_samp = $search_array[$k];

								if ($curr_samp['xLoc'] == $i)
								{
									$curr_array[$curr_samp['yLoc']] = $curr_samp['sampleName'];
								}
							}
						}
						$square_matrix[$i] = $curr_array;
					}
					// add grid to results
					$results['grid'] = $square_matrix;

					return $results;
				}
			}
		}

		public function logoff()
		{
			session_start();
      		session_destroy();
      		mysqli_close($this->conn);
			header('Location:'.REDIRECT_URL.'?page=login');
		}

		public function close_db_conn()
		{
			mysqli_close($this->conn);
		}

		public function logLogin($user_info)
		{

			$this->addOrModifyRecord('login_table', $user_info);
		}

		public function login($inputs)
		{
			// login to app.  Make sure that the password does not need to be reset
			// and that there are not more than 5 attempts in the last 30 mins
			// user_table (password_need_reset) controls locking the user account 
			// and reseting the password.

			// password_need_reset == 0 -> (allow login)
			// password_need_reset == 1 -> (reset password)
			// password_need_reset == 2 -> (account locked)

			$login_array = array(
					'email_address' 	=> 		$inputs['email_address'],
					'password'		=> 		md5($inputs['password']),
					'ip_address'		=> 		$this->ip_loc['ip']
				);

			// Find all user information.  An empty array will equal user and 
			// email does not exist.  This is used to increase the failed
			// attempts for the user.
			$user_info = $this->listAll('user-by-email-only', $login_array);

			// Find if email address matches md5 password.  Used to login.
			$login_info = $this->listAll('login', $login_array);

			//////////////////////////////////////////////////////////////////
			// If more than 5 attempts exist in the last 30 mins.  Redirect to 
			// to_many_login_attempts
			//////////////////////////////////////////////////////////////////
			$user_login_attempts = $this->listAll('user-num-failed-attempts', $inputs['email_address']);

			// remove password from login_array.  It is not needed anymore.
			unset($login_array['password']);

			
			//////////////////////////////////////////////////////////////////
			// If email address does not exist a login attempt can not be 
			// logged however, still need to show failed login.
			//////////////////////////////////////////////////////////////////
			if (!isset($user_info) || empty($user_info))
			{			
				return False;
			}

			//////////////////////////////////////////////////////////////////
			// If password_need_reset is set to 2 this account is locked.  
			// Only users with admin rights can unlock accounts. 
			//////////////////////////////////////////////////////////////////
			else if ($user_info[0]['password_need_reset'] == 2)
			{				
				header('Location:'.REDIRECT_URL.'/templates/account_locked.php');				
			}

			//////////////////////////////////////////////////////////////////
			// If > 5 attempts exist in the last 30 mins do not allow in
			//////////////////////////////////////////////////////////////////
			else if (!empty($user_login_attempts) && $user_login_attempts[0]['login_count'] > 5)
			{

				$login_array['status'] = 'failed';
				$this->logLogin($login_array);
				header('Location:'.REDIRECT_URL.'/templates/to_many_login_attempts.php');
			}

			//////////////////////////////////////////////////////////////////
			// If password_need_reset is set to 1 redirect to password_reset
			//////////////////////////////////////////////////////////////////
			else if ($user_info[0]['password_need_reset'] == 1)
			{
				header('Location:'.REDIRECT_URL.'?page=password_reset');
			}

			//////////////////////////////////////////////////////////////////
			// If no user found.  message user of incorrect login and add failed
			// login to 
			//////////////////////////////////////////////////////////////////
			else if (empty($login_info))
			{
				$login_array['status'] = 'failed';
				$this->logLogin($login_array);

				return False;
			}			
			//////////////////////////////////////////////////////////////////
			// login and set session
			//////////////////////////////////////////////////////////////////
			else
			{
				$login_array['status'] = 'passed';
				$this->logLogin($login_array);

				// add a session
				$session_set = $this->setLoginSession($user_info[0]);			

				return $session_set;			
			}
		}

		public function unlockAccount($email_address, $random_password)
		{
			// update all failed logins for user in last 30 mins
			$update_query = 'UPDATE login_table lt SET lt.status = "update_fail" WHERE lt.email_address = "'.$this->sanitize($email_address).'" AND lt.status = "failed" AND lt.time_stamp > (now() - interval 30 minute)';
			
			$result = mysqli_query($this->conn, $update_query) or die('Oops!! Died ');

			// get the Id of the inserted record
			$lastId = mysqli_insert_id($this->conn);

			// save current password
			$this->saveCurrentUserPassword($email_address);

			// update password to random 10 chars 
			$this->updateRecord('user_table', 'email_address', $email_address, 'password', md5($random_password));

			// change password need reset to 1
			$this->updateRecord('user_table', 'email_address', $email_address, 'password_need_reset', 1);
		}

		private function saveCurrentUserPassword($email_address)
		{
			// save current password as long it isn't a password automatically generated by infotrack
			// auto generated passwords have password_need_reset == 1
			$current_user_info = $this->listAll('user-by-email', $email_address);
				
			if (!empty($current_user_info) && $current_user_info[0]['password_need_reset'] != 1)
			{
				$save_password_arr = array(
					'user_id' 	=> 	$current_user_info[0]['user_id'],
					'password' 	=> 	$current_user_info[0]['password']
				);

				$add_result = $this->addOrModifyRecord('user_previous_password_md5s_table', $save_password_arr);
			}
		}

		public function lockAccount($email_address)
		{
			// save current password
			$this->saveCurrentUserPassword($email_address);

			// change password need reset to 1
			$this->updateRecord('user_table', 'email_address', $email_address, 'password_need_reset', 2);
		}
		public function setLoginSession($user_info)
		{
			session_start();
			$user_info['site_title'] = $this->site_title;
			$user_info['root_url'] = $this->root_url;
			$user_info['last_active_time'] = time();
			$_SESSION['user'] = $user_info;
			
			define('USER_ID', $_SESSION['user']['user_id']);

			session_write_close();

		     return true;
		}

		public function findDuplicate()
		{
			$searchQuery = 'SELECT '.$this->colName.' FROM '.$this->tableName.' WHERE '.$this->colName.' = "'.$this->postInput[$this->colName].'";';

			$searchResult = mysqli_num_rows(mysqli_query($this->conn, $searchQuery));
			return $searchResult;
		}

		private function sanitize($dirty_string)
		{
			$no_ticks_string = str_replace("`","'",$dirty_string);
			$clean_string = mysqli_real_escape_string($this->conn, $no_ticks_string);

			return $clean_string;
		}

		public function TwoWayEncrypt($in_string)
		{

			//  Gets the cipher iv length
			$iv_len = openssl_cipher_iv_length($this->cipher);
			$iv = openssl_random_pseudo_bytes($iv_len);
			$cipher_text_raw = openssl_encrypt($in_string, $this->cipher, $this->cryp_key, $options=OPENSSL_RAW_DATA, $iv);

			// hmac would be nice to add to the encryption but it makes search difficult
			$hmac = hash_hmac('sha256', $cipher_text_raw, $this->cryp_key, $as_binary=true);
			$cipher_text = base64_encode( $iv.$hmac.$cipher_text_raw );

			// $cipher_text = base64_encode( $iv.$cipher_text_raw );
			return $cipher_text;
		}

		public function decrypt($cipher_text)
		{
			// Decode a base64 encoded data.
			$c = base64_decode($cipher_text);

			// Gets the cipher iv length
			$ivlen = openssl_cipher_iv_length($this->cipher);

			// separate encrypted string 
			$iv = substr($c, 0, $ivlen);

			// get hmac by separating encyrpted string
			$hmac = substr($c, $ivlen, $sha2len=32);


			$cipher_text_raw = substr($c, $ivlen+$sha2len);


			$original_plain_text = openssl_decrypt($cipher_text_raw, $this->cipher, $this->cryp_key, $options=OPENSSL_RAW_DATA, $iv);

			$calcmac = hash_hmac('sha256', $cipher_text_raw, $this->cryp_key, $as_binary=true);
			if (hash_equals($hmac, $calcmac))//PHP 5.6+ timing attack safe comparison
			{
			    return $original_plain_text;
			}
		}

		public function decrypt_array($encrypted_array)
		{
			// there might be many values to decrypt in an array.  Use this function to decrypt all values

			$decrypted_array = array();

			for ($i = 0; $i < sizeof($encrypted_array); $i++)
			{

				foreach($encrypted_array[$i] as $key => $value)
				{				
					// some variables are concatenated strings of encryption.  These need to be each decrypted separately
					if ($key === 'patient_name')
					{
						$decrypt_concat = '';
						$split_str = explode(' ', $value);
					
						for($k = 0; $k < sizeof($split_str); $k++)
						{
							$decrypt_concat .= $this->decrypt($split_str[$k]).' ';
						}

						$decrypted_array[$i][$key] = $decrypt_concat;
					
					}

					// encrypted strings will be len 88 so decrypt them
					elseif (strlen($value) === 88)
					{
						$decrypted_val = $this->decrypt($value);
						$decrypted_array[$i][$key] = $decrypted_val;
					}
					else
					{
						$decrypted_array[$i][$key] = $value;
					}
				}
			}

			return $decrypted_array;

		}

		public function version_count($time_frame)
		{
			// get count of version updates in a time_frame of days
			$count = $this->listAll('num-updates-in-time-period', $time_frame);
			
			// if ($time_frame == 60 && empty($count[0]['total_num_updates']))
			// {
			// 	header("Location: http://{$_SERVER['SERVER_NAME']}/");
			// }
			
			return empty($count[0]['total_num_updates']) ? 0 : $count[0]['total_num_updates'];
		}

		function mysqli_field_name($result, $field_offset)
		{
		    $properties = mysqli_fetch_field_direct($result, $field_offset);
		    return is_object($properties) ? $properties->name : null;
		}

		public function addControlVisitPatient($add_visit_array, $POST)
		{

				// Get info about control from control_type_table using control_type_id
				$control_info = $this->listAll('control-by-id', $add_visit_array['control_type_id']);

				// Get patient info ready to search if the patient exists or add the patient.
				$add_patient_array = array();
				$add_patient_array['last_name'] = $control_info[0]['dummy_last_name'];
				$add_patient_array['first_name'] = $control_info[0]['dummy_first_name'];
				$add_patient_array['sex'] = $control_info[0]['dummy_sex'];
				$add_patient_array['dob'] = $control_info[0]['dummy_dob'];
				$add_patient_array['medical_record_num'] = $control_info[0]['dummy_medical_record_num'];
				$add_patient_array['user_id'] = USER_ID;

				$patient_found_array = $this->listAll('find-patient', $add_patient_array);

				// if patient does not exist add patient and get patient id
				if (empty($patient_found_array))
				{
					$add_patient_result = $this->addOrModifyRecord('patient_table', $add_patient_array);

					$patient_id = $add_patient_result[1];
				}

				// check if patient already exists if so get patient id 
				else 
				{
					$patient_id = $patient_found_array[0]['patient_id'];
				}
				

				if (isset($patient_id) && !empty($patient_id))
				{
					// for updating add visit_id
					if (isset($POST['visit_id']) && !empty($POST['visit_id']))
					{
						$add_visit_array['visit_id'] = $POST['visit_id'];
					}
					// add visit 
					$add_visit_array['run_id'] = RUN_ID;
					$add_visit_array['ngs_panel_id'] = $POST['ngs_panel_id'];
					$add_visit_array['sample_type'] = $POST['sample_type'];
					$add_visit_array['user_id'] = USER_ID;
					$add_visit_array['mol_num'] = $control_info[0]['control_name'];

					$add_visit_array['version'] = $POST['version'];
					$add_visit_array['patient_id'] = $patient_id;
					$add_visit_array['soft_lab_num'] = $POST['version'];
					$visit_add_result = $this->addOrModifyRecord('visit_table', $add_visit_array);

					return array(
						'visit_id'	=> 	$visit_add_result[1],
						'patient_id'	=>	intval($patient_id)
					);
				}
				else
				{
					return 'Oops.  Something went wrong with patient entry.';
				}

		}

	}

?>
