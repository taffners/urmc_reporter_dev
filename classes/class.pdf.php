<?php
	

	class PDF extends barcode_128
	{
		protected $col = 0; // current column
		protected $y0; 	// Ordinate of column start
		private $logo = 'images/urmc_logo.png';
		private $lab_name = 'Molecular Diagnostic Laboratory';
		
		// outside data
		private $patientArray;
		private $visitArray;
		private $reportArray;
		private $tiers;
		private $runInfoArray;
		private $approval_status;
		private $confirm_array;
		private $approval_steps;
		private $download_user_initials;
		private $panel_type;
		private $reportMutantTables;
		private $full_interpt;
		private $sp_sections = 10;
		private $std_font_size = 12;

		// keep track of size of page
		private $height_of_cell = 60; // mm
		private $page_height = 275; // mm (portrait letter)

		// used for Table of Genes in CoveredGenes Function
		private $genes_table_col_1_width = '58';
		private $genes_table_col_2_width = '90';
		private $genes_table_col_3_width = '95';
		private $genes_table_num_tables = 3;

		var $widths;
		var $aligns;

		// rotation
		var $angle = 0;

		var $test_report_num = 2;

		function RotatedText($x,$y,$txt,$angle)
		{
		    //Text rotated around its origin
		    $this->Rotate($angle,$x,$y);
		    $this->Text($x,$y,$txt);
		    $this->Rotate(0);
		}

		function ordinal($number) 
		{
			// change a number to ordinal number.
			//  	(1) -> 1st
			// 	(5) -> 5th
			// 	(83) -> 83rd

			$ends = array('th','st','nd','rd','th','th','th','th','th','th');
			
			if ((($number % 100) >= 11) && (($number%100) <= 13))
			{
				return $number. 'th';
			}
			else
			{
				return $number. $ends[$number % 10];
			}
		}

		function Circle($x, $y, $r, $style='D')
		{
		    $this->Ellipse($x,$y,$r,$r,$style);
		}

		function Ellipse($x, $y, $rx, $ry, $style='D')
		{
		    if($style=='F')
		        $op='f';
		    elseif($style=='FD' || $style=='DF')
		        $op='B';
		    else
		        $op='S';
		    $lx=4/3*(M_SQRT2-1)*$rx;
		    $ly=4/3*(M_SQRT2-1)*$ry;
		    $k=$this->k;
		    $h=$this->h;
		    $this->_out(sprintf('%.2F %.2F m %.2F %.2F %.2F %.2F %.2F %.2F c',
		        ($x+$rx)*$k,($h-$y)*$k,
		        ($x+$rx)*$k,($h-($y-$ly))*$k,
		        ($x+$lx)*$k,($h-($y-$ry))*$k,
		        $x*$k,($h-($y-$ry))*$k));
		    $this->_out(sprintf('%.2F %.2F %.2F %.2F %.2F %.2F c',
		        ($x-$lx)*$k,($h-($y-$ry))*$k,
		        ($x-$rx)*$k,($h-($y-$ly))*$k,
		        ($x-$rx)*$k,($h-$y)*$k));
		    $this->_out(sprintf('%.2F %.2F %.2F %.2F %.2F %.2F c',
		        ($x-$rx)*$k,($h-($y+$ly))*$k,
		        ($x-$lx)*$k,($h-($y+$ry))*$k,
		        $x*$k,($h-($y+$ry))*$k));
		    $this->_out(sprintf('%.2F %.2F %.2F %.2F %.2F %.2F c %s',
		        ($x+$lx)*$k,($h-($y+$ry))*$k,
		        ($x+$rx)*$k,($h-($y+$ly))*$k,
		        ($x+$rx)*$k,($h-$y)*$k,
		        $op));
		}

		function Rotate($angle,$x=-1,$y=-1)
		{
			if($x==-1)
				$x=$this->x;
			if($y==-1)
				$y=$this->y;
			if($this->angle!=0)
				$this->_out('Q');
			$this->angle=$angle;
			if($angle!=0)
			{
				$angle*=M_PI/180;
				$c=cos($angle);
				$s=sin($angle);
				$cx=$x*$this->k;
				$cy=($this->h-$y)*$this->k;
				$this->_out(sprintf('q %.5F %.5F %.5F %.5F %.2F %.2F cm 1 0 0 1 %.2F %.2F cm',$c,$s,-$s,$c,$cx,$cy,-$cx,-$cy));
			}
		}

		public function addTestSheetData($PDF_Data_array)
		{
			$this->pdfType = 'Test_Sheet';
			$this->sampleLogBookArray = $PDF_Data_array['sampleLogBookArray'];
			$this->orderedTestArray = $PDF_Data_array['orderedTestArray'];
			$this->extractors = $PDF_Data_array['extractors'];
			$this->reflexArray = $PDF_Data_array['reflexArray'];
						
		}

		public function addNGSReportData($patientArray, $visitArray, $reportArray, $tiers, $runInfoArray, $approval_status, $confirm_array, $approval_steps, $download_user_initials, $reportMutantTables, $full_interpt, $num_snvs, $revisionsArray, $genesCoveredArray)
		{
			// Purpose: add all data for dynamic report
			$this->pdfType = 'NGS';
			$this->patientArray = $patientArray;
			$this->visitArray = $visitArray;
			$this->reportArray = $reportArray;
			$this->tiers =$tiers;
			$this->runInfoArray = $runInfoArray;
			$this->approval_status = $approval_status;
			$this->confirm_array = $confirm_array;	
			$this->approval_steps = $approval_steps;
			$this->download_user_initials = $download_user_initials;	
			$this->panel_type = $visitArray['panel_type'];
			$this->reportMutantTables = $reportMutantTables;
			$this->section_split_interpt = explode('VARIANTS OF UNKNOWN SIGNIFICANCE INTERPRETATION<br><br>',$full_interpt);
			$this->full_interpt = $full_interpt;
			$this->num_snvs = $num_snvs;
			$this->revisionsArray = $revisionsArray;
			$this->genesCoveredArray = $genesCoveredArray;
		}		 
		
		public function Header()
		{	

			if ($this->header_status && !empty($this->address_array))
			{
				// Adds page header on every page
				$this->SetTextColor(0);
				// Logo (img, x, y, width)
				$this->Image($this->logo, 10, 12, 60);

				// Arial bold 15
				$this->SetFont('Arial','B',$this->std_font_size);

				// Move to the right
				$this->Cell(70);

				// lab name
				$this->Cell(0,5, strtoupper($this->lab_name),'B',0,'C');

				// Line break
				$this->Ln(2);

				// add address				
				$this->SetFont('Arial','',10);

				foreach($this->address_array as $address_line)
				{
					$this->Ln(4);
					$this->Cell(70);
					$this->Cell(0,4, $address_line,'',0,'C');
				}

				// horizontal dividing line
				$this->Ln(1);
				$this->Cell(0,5, '','B',0,'');
				$this->Ln(7);	
			}

			if (isset($this->approval_steps) && $this->in_array_r('confirmed', $this->approval_steps) === False)
			{
				//Put the watermark
				$this->SetFont('Arial','B',50);
				$this->SetTextColor(255,192,203);
				$this->RotatedText(35,190,'D R A F T   R E P O R T',45);
			}
			else if (isset($this->sampleLogBookArray['consent_recieved']) && 
				strpos($this->sampleLogBookArray['consent_recieved'], 'No Consent') !== false && 
				($this->orderedTestArray['test_type'] === 'Extraction' || $this->orderedTestArray['test_type'] === 'genetic'))
			{
				//Put the watermark
				$this->SetFont('Arial','B',50);
				$this->SetTextColor(255,192,203);
				$this->RotatedText(35,190,'   N O  C O N S E N T',45);
			}	
		}

		function _endpage()
		{
			if($this->angle!=0)
			{
				$this->angle=0;
				$this->_out('Q');
			}
			parent::_endpage();
		}

		public function PatientInfo()
		{
			if ($this->header_status) 
			{
				// patient Name
				// $this->Ln($this->sp_sections);
				$this->SetFont('Arial','', $this->std_font_size);
				$this->TwoColData('Patient Name', 'row_name', 1);
				$this->TwoColData($this->patientArray['patient_name'], 'data', 1);

				// MRN
				$this->TwoColData('MRN', 'row_name', 2);
				$this->TwoColData($this->patientArray['medical_record_num'], 'data', 2);
				
				// age, dob, sex
				$this->TwoColData('Age/DOB/Sex', 'row_name', 1);
				$this->TwoColData($this->calc_age($this->patientArray['dob'], $this->visitArray['time_stamp']).' Y  '.$this->patientArray['dob'].'  '.$this->patientArray['sex'], 'data', 1);
				
				// soft path num
				$this->TwoColData('Soft Path#', 'row_name', 2);

				// append block number if not empty
				if (strtotime($this->visitArray['received']) > strtotime('2020-08-25') && isset($this->visitArray['block']) && !empty($this->visitArray['block']))
				{

					$this->TwoColData($this->visitArray['soft_path_num'].'-'.$this->visitArray['block'], 'data', 2);

				}
				else
				{
					$this->TwoColData($this->visitArray['soft_path_num'], 'data', 2);

				}
				
				// Coll, 			
				$this->TwoColData('Collected', 'row_name', 1);
				$this->TwoColData($this->printDate($this->visitArray['collected']), 'data', 1);
				
				// soft lab number
				$this->TwoColData('Soft Lab#', 'row_name', 2);
				$this->TwoColData($this->visitArray['soft_lab_num'], 'data', 2);

				// Recd
				$this->TwoColData('Received', 'row_name', 1);
				$this->TwoColData($this->printDate($this->visitArray['received']), 'data', 1);

				// molecular number
				$this->TwoColData('MD#', 'row_name', 2);
				$this->TwoColData($this->visitArray['mol_num'], 'data', 2);

				// ORD
				$this->TwoColData('Requesting Physician', 'row_name', 1.25);
				// $this->SetFont('Arial','B',50);

				// for requestion physician with more than 40 charcters change the font size to smaller
				
				// var_dump(strlen($this->visitArray['req_physician']));				
				$this->TwoColData($this->visitArray['req_physician'], 'data', 1.25);
				
				$this->SetFont('Arial','B',50);
				// Account #
				$this->TwoColData('Account#', 'row_name', 2);
				$this->TwoColData($this->visitArray['account_num'], 'data', 2);

				// loc 
				$this->TwoColData('Requesting Location', 'row_name', 1.25);
				$this->TwoColData($this->visitArray['req_loc'], 'data', 1.25);
			
				// chart#
				$this->TwoColData('Chart#', 'row_name', 2);
				$this->TwoColData($this->visitArray['chart_num'], 'data', 2);
			

				if($this->panel_type == 'Myeloid/CLL Mutation Panel' )
				{
					$this->TwoColData('Tested Tissue', 'row_name', 1.25);
					$this->TwoColData($this->visitArray['test_tissue'], 'data', 1.25);
					// order #
					$this->TwoColData('Mol#', 'row_name', 2);
					$this->TwoColData($this->visitArray['order_num'], 'data', 2);
				}
				else
				{
					// order #
					$this->TwoColData('Mol#', 'row_name', 1);
					$this->TwoColData($this->visitArray['order_num'], 'data', 1);
				}
			}

			// ordered tests
			// $this->TwoColData('Ordered tests', 'row_name', 1);
			// $this->TwoColData('EGFR MUTATION ANALYSIS, KRAS GENE MUTATION, BRAF V600E MUTATION, EGFR REVIEW, BRAF REVIEW', 'full_line', 1);
		}

		public function SampleInfo()
		{
			// This section is only necessary for Assay's which can have many different types
			// of Tissue inputs.  Current the Oncomine Focus Hotspot Assay is the only 
			// Assay which will need this.  As more assays are added add a table to cross ref 
			// Assay with pdf sections.  

			// Tissues
			if ($this->panel_type !== 'Myeloid/CLL Mutation Panel')
			{				
				$this->AddSectionHead('SAMPLE INFORMATION:');
				$this->SetFont('Arial','',10);
				$this->Ln(5);

				// Add Primary Site and tissue tested row 
				$this->TwoColData('Primary Site', 'row_name', .5);
				$this->TwoColData($this->visitArray['primary_tumor_site'], 'data', .5);
				$this->TwoColData('Tissue Tested', 'row_name', 1.5);
				$this->TwoColData($this->visitArray['test_tissue'], 'data', 1.5);
										
				$this->ln(5);

				// Add Tumor type and Neoplastic Cellularity row 
				$this->TwoColData('Tumor Type', 'row_name', .5);
				$this->TwoColData($this->visitArray['tumor_type'], 'data', .5);
				$this->TwoColData('Neoplastic Cellularity', 'row_name', .75);

				// default value of tumor_cellularity is 0 if a 0 is added do not add anything to field
				if ($this->visitArray['tumor_cellularity'] == 0)
				{
					$this->TwoColData('', 'data', .75);
				}
				else
				{
					$this->TwoColData($this->visitArray['tumor_cellularity'].'%', 'data', .75);
				}
			}
		}

		public function SampleTestSheetInfo()
		{
			$this->SetFont('Arial','', $this->std_font_size);
			$this->TwoColData('Patient Name', 'row_name', 1);
			$this->TwoColData($this->sampleLogBookArray['last_name'].', '.$this->sampleLogBookArray['first_name'], 'data', 1);

			$this->TwoColData('MRN', 'row_name', 2);
			$this->TwoColData($this->sampleLogBookArray['medical_record_num'], 'data', 2);

			// age, dob, sex
			$this->TwoColData('Age/DOB/Sex', 'row_name', 1);
			$this->TwoColData($this->calc_age($this->sampleLogBookArray['dob'], $this->sampleLogBookArray['time_stamp']).' Y  '.$this->sampleLogBookArray['dob'].'  '.$this->sampleLogBookArray['sex'], 'data', 1);


			$this->TwoColData('MD#', 'row_name', 2);
			$this->TwoColData($this->sampleLogBookArray['mol_num'], 'data', 2);

			// Recd
			$this->TwoColData('Received', 'row_name', 1);
			$this->TwoColData($this->printDate($this->sampleLogBookArray['date_received']), 'data', 1);
			// soft path num
			$this->TwoColData('Soft Path#', 'row_name', 2);
			$this->TwoColData($this->sampleLogBookArray['soft_path_num'], 'data', 2);

			$this->TwoColData('Outside#', 'row_name', 1);
			$this->TwoColData($this->sampleLogBookArray['outside_case_num'], 'data', 1);

			// soft lab num
			$this->TwoColData('Soft Lab#', 'row_name', 2);
			$this->TwoColData($this->sampleLogBookArray['soft_lab_num'], 'data', 2);	
			

			if (isset($this->orderedTestArray['order_num']) && !empty($this->orderedTestArray['order_num']))
			{
				$this->TwoColData('mol#', 'row_name', 1);
				$this->TwoColData($this->orderedTestArray['order_num'], 'data', 1);	
			}

			// $this->Ln(5);
			// $this->AddSectionHead('SAMPLE INFORMATION:');
			// $this->SetFont('Arial','',10);
			$this->Ln(5);

			// 
			$this->TwoColData('Tissue', 'row_name', 1.5);
			$this->TwoColData($this->sampleLogBookArray['test_tissue'], 'data', .5);

			$this->TwoColData('Sample Type', 'row_name', 1.5);
			$this->TwoColData($this->sampleLogBookArray['sample_type'], 'small-data', 1.5);

			$this->TwoColData('Block', 'row_name', 1.5);
			$this->TwoColData($this->sampleLogBookArray['block'], 'data', .5);

			$this->ln(5);

			$this->TwoColData('H&E Circled', 'row_name', .5);
			$this->TwoColData($this->sampleLogBookArray['h_and_e_circled'], 'data', .5);

			$this->TwoColData('Neoplastic Cellularity', 'row_name', .75);
			$this->TwoColData($this->sampleLogBookArray['tumor_cellularity'], 'data', .75);

			$this->TwoColData('WBC Count', 'row_name', 1.5);
			$this->TwoColData($this->sampleLogBookArray['WBC_count'], 'data', 1.5);

		}

		public function testOrderedHistory($histories)
		{
			$this->AddSectionHead('HISTORY INFORMATION:');
			$this->SetFont('Arial','',10);
			$this->Ln(5);

			foreach($histories as $key => $history)
			{
				$this->TwoColData('History Name', 'row_name', 1);
				$this->TwoColData($history['history_overview'], 'data', 1);

				$this->TwoColData('MD#', 'row_name', 2);
				$this->TwoColData($history['history_mol_num'], 'data', 2);

				$this->TwoColData('Soft Path#', 'row_name', 1.5);
				$this->TwoColData($history['history_soft_path_num'], 'data', .5);

				
				$this->TwoColData('Tissue', 'row_name', .75);
				$this->TwoColData($history['history_test_tissue'], 'data', .75);

				$this->TwoColData('Date', 'row_name', 2);
				$this->TwoColData($this->printDate($history['history_date']), 'data', 2);	
			}

			
		}

		public function printDate($in_date)
		{
			// Only include date if it is not 0000-00-00
			if ($in_date != '0000-00-00' && $in_date !== DEFAULT_DATE_TIME)
			{
				return $in_date;
			}
			else
			{
				return '';
			}
		}

		public function testOrderedConsent()
		{		

			$this->AddSectionHead('CONSENT INFORMATION:');
			$this->SetFont('Arial','',10);

			$this->Ln(2);
			$this->SetFont('Arial','', $this->std_font_size);
			$this->TwoColData('Consent Type', 'row_name', .5);

			// // On July 21st 2020 I was notified that Onbase was on longer being used.  However, the exact date of abandoment of Onbase was unknown.  Therefore instead of changing Onbase to soft media the user now sees Scanned Document Found.

			// if ($this->orderedTestArray['consent_info'] === 'Onbase check found')
			// {
			// 	$this->orderedTestArray['consent_info'] = 'Scanned Document Found';
			// }
	
			$this->TwoColData($this->orderedTestArray['consent_info'], 'data', .5);

			
			/////////////////////////////////
			// On March 15th 2021 consent_for_research field was discontinued from collection via 
			// infotrack because it was no longer being collected on the consent form.  Dan Bach 
			// requested for its removal.  This report will display the info if provided otherwise it will not 
			// be added to the form
			////////////////////////////////
			if (!empty($this->orderedTestArray['consent_for_research']))
			{
				$this->Ln(5);
				$this->TwoColData('Research Consented', 'row_name', .75);
				$this->TwoColData($this->orderedTestArray['consent_for_research'], 'data', .75);

				if ($this->orderedTestArray['consent_date'] != '0000-00-00')
				{
					$this->TwoColData('Consent Received', 'row_name', 1.5);
					$this->TwoColData($this->printDate('     '.$this->orderedTestArray['consent_date']), 'data', 1.5);
				}
			}

			else if ($this->orderedTestArray['consent_date'] != '0000-00-00')
			{
				$this->Ln(5);
				$this->TwoColData('Consent Received', 'row_name', .5);
				$this->TwoColData($this->printDate('     '.$this->orderedTestArray['consent_date']), 'data', .5);
			}
			$this->Ln($this->sp_sections);			
		}

		public function testOrderedBy()
		{							
			$this->reset_line();

			// horizontal line
			$this->Cell(0,5, '','B',0,'');
			$this->Ln(6);
			

			$this->formInputName('Tests Ordered', 'row_name', .5);
			$this->formInput($this->sampleLogBookArray['ordered_tests']);
			$this->Ln(6);

			if (!empty($this->sampleLogBookArray['consent_recieved']) && 
				$this->orderedTestArray['test_type'] === 'Extraction')
			{
				$this->formInputName('Consented', 'row_name', .5);
				$this->formInput($this->sampleLogBookArray['consent_recieved']);
				$this->Ln(6);
			}

			if (!empty($this->sampleLogBookArray['consent_for_research']) && 
				$this->orderedTestArray['test_type'] === 'Extraction')
			{
				$this->formInputName('Consented for Research', 'row_name', .5);
				$this->formInput($this->sampleLogBookArray['consent_for_research']);
				$this->Ln(6);
			}

			if (!empty($this->reflexArray))
			{
				$this->formInputName('Reflex Trackers', 'row_name', .5);
				foreach ($this->reflexArray as $key => $reflex)
				{					
					$this->AddHTML('<b>'.$reflex['reflex_name'].'</b>');
					$this->Ln(6);
				}
				
			}

			$this->formInputName('Test Ordered By');
			$this->formInput($this->orderedTestArray['user_name']);

			$this->formInputName('Test Ordered Time Stamp');
			$this->formInput($this->orderedTestArray['time_stamp']);

			// horizontal line
			$this->Ln(6);
			$this->Cell(0, 0, '','B',0,'');
		}

		public function testOrderedComments()
		{
			if (isset($this->sampleLogBookArray['problems']) && !empty($this->sampleLogBookArray['problems']))
			{
				$this->AddSectionHead('Sample Problem/Comments:');

				$this->addParagraphs(explode('&#*', $this->sampleLogBookArray['problems']));
			}

			if (isset($this->orderedTestArray['problems']) && !empty($this->orderedTestArray['problems']))
			{

				$this->AddSectionHead('Test Problem/Comments:');
				$this->addParagraphs(explode('&#*', $this->orderedTestArray['problems']));
			}
		}	

		public function addRevisionInfo()
		{	
			$this->ln(5);
		
			$this->SetFont('Arial','B',15);
			$this->Cell(0,6, '* * * * REVISED REPORT * * * *','',0,'C');
			$this->AddSectionHead('REVISION HISTORY:');
			
			foreach ($this->revisionsArray as $key => $revision)
			{
				$this->ln(5);
				$this->AddHTML($revision['change_summary']);
				$this->ln();

				$this->SetFont('Arial','', $this->std_font_size);
				$this->TwoColData('Revised By', 'row_name', .5);
				$this->TwoColData($revision['user_name'], 'data', .5);

				$this->TwoColData('Time Stamp', 'row_name', .75);
				$this->TwoColData($revision['time_stamp'], 'data', .75);
				$this->ln();			
			}
		}

		public function TwoColData($data, $type, $col_num)
		{
			// (str, str, int) -> write
			// Example: input Name, row_name, 1
			// place data in correct column and depending if if row_name or data
			// $this->TwoColData('Name', 'row_name', 1);
			// $this->TwoColData($this->patientArray['patient_name'], 'data', 1);
			switch($type)
			{
				case 'row_name':
					// change font to italic and font size 7
					if ($col_num === 1.5)
					{
						$this->SetFont('Arial','I', 10);
						$this->Cell(25,6, $data.':','',0,'');
					}
					elseif ($col_num === 1)
					{
						$this->SetFont('Arial','I', 10);
						$this->Cell(25,6, $data.':','',0,'');
					}			
					elseif ($col_num === 2)
					{
						$this->SetFont('Arial','I', 10);
						$this->Cell(18,6, $data.':','',0,'');
					}	
					elseif ($col_num === .5)
					{
						$this->SetFont('Arial','I', 10);
						$this->Cell(25,6, $data.':','',0,'');
					}	
					elseif ($col_num === .75)
					{
						$this->SetFont('Arial','I', 10);
						$this->Cell(35,6, $data.':','',0,'');
					}
					elseif ($col_num === 1.25)
					{
						$this->SetFont('Arial','I', 10);
						$this->Cell(40,6, $data.':','',0,'');
					}		
					break;

				case 'small-data':

					if (strlen($data)> 20)
					{
				
						$this->SetFont('Arial','B', 10);
					}
					else
					{
						// change font to italic and font size 7
						$this->SetFont('Arial','B', 12);
					}
					break;

				case 'data':
					
					// Make fonts smaller for data longer than 39 characters since there 
					// will not be enough room.  There is a maxium space of 78 characters without making it too small.
					if (strlen($data)> 39 && strlen($data) < 46)
					{
				
						$this->SetFont('Arial','B', 10);
					}
					else if (strlen($data) >= 46 && strlen($data) < 57)
					{
						$this->SetFont('Arial','B', 8);
					}
					else if (strlen($data) >= 57)
					{
						$this->SetFont('Arial','B', 6);
					}

					else
					{
						// change font to italic and font size 7
						$this->SetFont('Arial','B', 12);
					}
					

					break;
				case 'full_line':
					// $this->Ln(6);
					$this->SetFont('Arial','B',12);
					$this->write(5, $data);
					break;
			}

			// add data for a small-data field and a larger data field
			switch($type)
			{
				case 'small-data':
				case 'data':

					if ($col_num === 1)
					{
						$this->Cell(113,6, $data,'',0,'');
					}
					elseif ($col_num === 1.25)
					{
						$this->Cell(98,6, $data,'',0,'');
					}
					elseif ($col_num === .5)
					{
						$this->Cell(42,6, $data,'',0,'');
					}
					elseif ($col_num === 1.5)
					{
						$this->Cell(46,6, $data,'',0,'');
					}
					elseif ($col_num === .75)
					{
						$this->Cell(36,6, $data,'',0,'');
					}
					elseif ($col_num === 2)
					{
						$this->Cell(50,6, $data,'',0,'');
						$this->Ln(6);
					}
					break;
			}

			// change font back to normal
		}

		public function reset_line($line_size = 6)
		{
			// if x is half way down the right restart the line 
			if ($this->x > 11.00)
			{
				$this->Ln($line_size);
			}
		}

		public function Panel($panel_title)
		{
			// add section which tells what assay was run after patient info section.
			$this->reset_line();

			// horizontal line
			$this->Cell(0,5, '','B',0,'');

			// add panel name
			$this->Ln(6);
			$this->SetFont('Arial','B',$this->std_font_size);

			// The panel title was determined to not be necessary once the idylla reports were developed
			// This is still present for NGS reports made before 2021-05-04
			if  (
					strtotime($this->visitArray['received']) < strtotime('2021-05-04') 
					&& $panel_title !== 'None'
				)
			{
				$this->Cell(0,6, $panel_title,'',0,'C');
				$this->Ln(6);
			}

			$this->Cell(0,6, $this->visitArray['panel_type'],'',0,'C');

			// horizontal line
			$this->Ln(6);
			$this->Cell(0, 0, '','B',0,'');
		}

		public function AddTestInfo()
		{
			$this->reset_line();

			// horizontal line
			$this->Cell(0,5, '','B',0,'');

			// add panel name
			$this->Ln(6);
			$this->SetFont('Arial','B');
			$this->Cell(0,6, $this->orderedTestArray['test_name'].' ('.$this->orderedTestArray['full_test_name'].')','',0,'C');
			$this->Ln(6);
			$this->Cell(0,6, $this->orderedTestArray['test_type'].', '.$this->orderedTestArray['assay_name'],'',0,'C');

			// horizontal line
			$this->Ln(6);
			$this->Cell(0, 0, '','B',0,'');
		}

		public function testOrderedPreviousExtractionInfo($extraction_info)
		{
			if (sizeOf($extraction_info) > 1)
			{
				$sectionHead = 'EXTRACTION/QUANTIFICATIONS:';
			}
			else
			{
				$sectionHead = 'EXTRACTION/QUANTIFICATION:';
			}
			
			$this->AddSectionHead($sectionHead);
// 'test_name' => string 'DNA Extraction' (length=14)
//       'extraction_method' => string 'Maxwell' (length=7)
//       'stock_conc' => string '250' (length=3)
//       'dilution_exist' => string 'yes' (length=3)
//       'dilutions_conc' => null
//       'extraction_log_id' => string '11' (length=2)
//       'tasks' => string 'extraction_performed_by' (length=23)
//       'task_users' => string 'John Fitzsimmons' (length=16)
			
			$this->Ln(3);

			// There can be more than one extraction therefore I will add all extractions
			foreach ($extraction_info as $key => $extraction)
			{
				///////////////////////////////////////////////////////////////////////////////////
				// extraction example:
				// array (size=13)
				//   'test_name' => string 'DNA Extraction' (length=14)
				//   'extraction_method' => null
				//   'stock_conc' => string '80' (length=2)
				//   'dilution_exist' => string 'yes' (length=3)
				//   'dilutions_conc' => string '50' (length=2)
				//   'extraction_log_id' => string '30' (length=2)
				//   'times_dilution' => null
				//   'purity' => null
				//   'vol_dna' => string '31' (length=2)
				//   'vol_eb' => string '19' (length=2)
				//   'dilution_final_vol' => string '50' (length=2)
				//   'elution_volume' => string '100' (length=3)
				//   'user_tasks' => string 'extraction_performed_by: John Fitzsimmons' (length=41)
				///////////////////////////////////////////////////////////////////////////////////
				if ($key === 0)
				{
					$this->Ln(2);
				}
				else
				{
					$this->Ln(5);
				}

				// add Extraction number if more than one extraction
				if (sizeOf($extraction_info) > 1)
				{
					$this->Ln(2);
					$extraction_num = $key + 1;
					$extractionNumHead = $this->ordinal($extraction_num).' extraction:';
					$this->formInput($extractionNumHead);
					$this->Ln(5);
				}

				if (!empty($extraction_info[$key]['extraction_method']))
				{
					$this->formInputName('Extraction Method');
					$this->formInput($extraction_info[$key]['extraction_method']);
				}

				if (!empty($extraction_info[$key]['quantification_method']))
				{
					$this->formInputName('Quant Method');
					$this->formInput($extraction_info[$key]['quantification_method']);

					if (empty($extraction_info[$key]['dna_conc_control_quant'])  && empty($extraction_info[$key]['purity']))
					{
						$this->Ln(5);
					}
				}

				if (!empty($extraction_info[$key]['purity']))
				{
					$this->formInputName('260/280');
					$this->formInput($extraction_info[$key]['purity']);

					if (empty($extraction_info[$key]['dna_conc_control_quant']))
					{
						$this->Ln(5);
					}
				}

				if (!empty($extraction_info[$key]['dna_conc_control_quant']))
				{
					$this->formInputName('Quant control');
					$this->formInput($extraction_info[$key]['dna_conc_control_quant']);
					$this->Ln(5);
				}

				if (!empty($extraction_info[$key]['stock_conc']))
				{
					$this->formInputName('Stock Conc');
					$this->formInput($extraction_info[$key]['stock_conc']);
				}

				if (!empty($extraction_info[$key]['elution_volume']))
				{
					$this->formInputName('Elution Vol');
					$this->formInput($extraction_info[$key]['elution_volume']);
				}


				if (!empty($extraction_info[$key]['dilution_exist']))
				{
					$this->formInputName('Dilution Exist');
					$this->formInput($extraction_info[$key]['dilution_exist']);
				}

				if (!empty($extraction_info[$key]['dilutions_conc']))
				{
					$this->formInputName('Actual Dilution Conc');
					$this->formInput($extraction_info[$key]['dilutions_conc']);
				}

				if (!empty($extraction_info[$key]['times_dilution']))
				{
					$this->formInputName('X Dilution');
					$this->formInput($extraction_info[$key]['times_dilution']);
				}			

				if (	
						!empty($extraction_info[$key]['vol_dna']) && //v1
						!empty($extraction_info[$key]['stock_conc']) && // c1
						!empty($extraction_info[$key]['vol_eb']) && // 
						!empty($extraction_info[$key]['dilution_final_vol']) && // v2
						!empty($extraction_info[$key]['target_conc']) // c2
					)
				{
					$this->Ln(5);
					$this->formInputName('Dilution Calc C1 V1 = C2 V2');

					$calc = '('.$extraction_info[$key]['stock_conc'].')('.$extraction_info[$key]['vol_dna'].') = ('.$extraction_info[$key]['target_conc'].')('.$extraction_info[$key]['dilution_final_vol'].')';

					$this->formInput($calc);
					// $this->Ln(5);
					$this->formInputName('EB Volume V2 - V1');

					$calc = $extraction_info[$key]['dilution_final_vol'].' - '.$extraction_info[$key]['vol_dna'].' = '.$extraction_info[$key]['vol_eb'];

					$this->formInput($calc);
				}


				if (!empty($extraction_info[$key]['user_tasks']))
				{
					$this->Ln(6);
					$this->formInputName('Tasks:Tech');
					$this->smallTypeWordWrapped($extraction_info[$key]['user_tasks']);
				}

				// if (!empty($extraction_info[$key]['task_users']))
				// {
				// 	$this->Ln(5);
				// 	$this->formInputName('Tasks Users');
				// 	$this->smallTypeWordWrapped($extraction_info[$key]['task_users']);
				// }

				if (!empty($extraction_info[$key]['purity']))
				{
					$this->Ln(5);
					
				}
			}
		}

		public function testOrderedDNAExtraction($quantifiers)
		{
			// this function is used in DNA Extraction and Nanodrop
			// DNA Extraction only par of the form

			if ($this->orderedTestArray['extraction_info'] === 'yes')
			{
				// $this->AddSectionHead('EXTRACTION:');

				$this->SetFont('Arial','', 10);

				$this->Ln(4);
				$this->formInputName('Aliquoted for Extraction Date');
				$this->formBlank('short');
				$this->formInputName(' Initials');
				$this->formBlank('short');

				$this->Ln(8);
				$this->formInputName('If PE, PK 56C digestion Date');
				$this->formBlank('short');
				$this->formInputName(' Time');
				$this->formBlank('short');
				$this->formInputName(' Initials');
				$this->formBlank('short');

				$this->Ln(8);
				$this->formInputName('Extraction Method');
				$dna_extration_options = array('Maxwell', 'Qiagen');
				$this->addRadioInlineInput($dna_extration_options);
				// $this->SetFont('Arial','', 10);

				$sprite_options = array();

				foreach ($this->extractors as $key => $instrument)
				{
					$curr_option = $instrument['manufacturer'].' '.$instrument['model'].' '.$instrument['yellow_tag_num'].' Serial# '.$instrument['serial_num'];
					array_push($sprite_options, $curr_option);
				}

				$this->Ln(8);

				$this->formInputName('Instrument Used');
				
				$this->addRadioInput($sprite_options, 'Instrument Used');

				// $this->SetFont('Arial','', 9);
				$this->Ln(3);
				$this->formInputName('Extraction Performed by Date');
				$this->formBlank('short');
				$this->formInputName(' Initials');
				$this->formBlank('short');
			}


			if  (
					$this->orderedTestArray['quantification_info'] === 'yes' && 
					$this->orderedTestArray['extraction_info'] === 'no'
				)
			{
				// $this->AddSectionHead('QUANTIFICATIOIN:');

				// $this->SetFont('Arial','', 9);
				$this->Ln(4);
				$this->formInputName('Quantification Date');
				$this->formBlank('short');
				$this->formInputName(' Initials');
				$this->formBlank('short');
				$this->Ln(4);
			}
			else
			{
				// $this->AddSectionHead('Quantification:');			
			}

			if ($this->orderedTestArray['quantification_info'] === 'yes')
			{			
				$this->Ln(8);	
				$this->formInputName('Method');
				$quant_method = array('Nanodrop', 'Quantus', 'Qubit', 'NA');
				$this->addRadioInlineInput($quant_method);

				$this->formInputName('Quantification Control (ng/ul)');
				$this->formBlank('short');

				// $this->SetFont('Arial','', 10);

				$quantifier_options = array();

				foreach ($quantifiers as $key => $instrument)
				{
					$curr_option = $instrument['manufacturer'].' '.$instrument['model'].' '.$instrument['yellow_tag_num'].' Serial# '.$instrument['serial_num'];
					array_push($quantifier_options, $curr_option);
				}

				$this->Ln(8);

				$this->formInputName('Quantifer Used');
				
				$this->addRadioInput($quantifier_options, 'Quantifer Used');

				$this->Ln(4);
				$this->formInputName('Stock Concentration (ng/ul)');
				$this->formBlank('short');

				$this->formInputName(' Dilution');

				$y_n = array('Yes', 'No');
				$this->addRadioInlineInput($y_n);

				$this->formInputName('Concentration (ng/ul)');
				$this->formBlank('short');
				
				$this->Ln(8);
				$this->formInputName('260/280');
				$this->formBlank('short');

				$this->formInputName('Number X Dilution');
				$this->formBlank('short');	

				$this->Ln(8);
				$this->SetFont('Arial','B',$this->std_font_size);
			}

			if ($this->orderedTestArray['extraction_info'] === 'yes')
			{
				$this->AddHTML('For re-extractions please sign this extraction out and add a new test in '.SITE_TITLE_ABBR);
			}
		}

		public function addIdyllaInstrumentsWorkSheetSection($idylla_instruments)
		{
			$this->Ln(3);
			$this->formInputName('Sample Loaded');
			$this->addRadioInlineInput(array('DNA','FFPE'), 'Sample Loaded');
			$this->formInputName('If DNA, volume (ul)');
			$this->formBlank('short');
			$this->formInputName('Conc. (ng/ul)');
			$this->formBlank('short');
			$this->Ln(8);
			$this->formInputName('Cartridge ID');
			$this->formBlank('medium');
			$this->formInputName('Lot ID');
			$this->formBlank('medium');
			$this->Ln(8);

			$instruments = array();

			foreach ($idylla_instruments as $key => $instrument)
			{
				$curr_option = $instrument['manufacturer'].' '.$instrument['model'].' '.$instrument['yellow_tag_num'].' Serial# '.$instrument['serial_num'];
				array_push($instruments, $curr_option);
			}

			$this->formInputName('Instrument ID');
					
			if (sizeOf($instruments) <=2)
			{
				$this->addRadioInlineInput($instruments, 'Instrument ID');
			}
			else
			{
				$this->addRadioInput($instruments, 'Instrument ID');
			}
		}

		public function addIdyllaHotspotsWorkSheetSection($idylla_variants)
		{
			
			$vars = array();
			
			$this->AddSectionHead('RESULTS:');
			$this->Ln(3);
			$input_name_added = False;
			$num_mutants_per_line = 6;

			// provide enough space for blanks to be added for values
			if ($this->orderedTestArray['hotspot_val_type'] !== 'none')
			{
				$num_mutants_per_line = 2;
			}

			$idylla_variants[sizeOf($idylla_variants)] = array('ordered_test_genetic_call' => '', 'genes' => 'None');
			foreach ($idylla_variants as $key => $variant)
			{
				/////////////////////////////
				// Append data to the vars variable and once number of mutants is equal to num_mutants_per_line 
				// Then the writer will add it to the pdf.  Add blanks if
					//  hotspot_val_type === observed & normal size
				////////////////////////////
				if ($this->orderedTestArray['hotspot_val_type'] === 'observed & normal size' && $variant['genes'] !==  'None')
				{
					array_push($vars, $variant['genes'].' '.$variant['ordered_test_genetic_call'].' Size: _______ Normal: _______');
				}
				else
				{
					array_push($vars, $variant['genes'].' '.$variant['ordered_test_genetic_call']);
				}
				
				
				// if a gene is not provided like Idylla MSI then more mutants can fit
				if (empty($variant['genes']) && $this->orderedTestArray['hotspot_val_type'] === 'none')
				{
					$num_mutants_per_line = 7;
				}

				///////////////////////////////////////
				// Writer
				// Write data to pdf once it reaches the max number of mutants per line
				///////////////////////////////////////
				if (sizeof($vars) >= $num_mutants_per_line)	
				{
					if ($input_name_added)
					{							
						$this->addRadioInlineInput($vars, $this->orderedTestArray['hotspot_type']);
					}
					else
					{							
						$this->formInputName($this->orderedTestArray['hotspot_type']);
						$this->addRadioInlineInput($vars, $this->orderedTestArray['hotspot_type']);
						$input_name_added = True;
					}
					$vars = array();
					$this->Ln(8);
				}			
			}

			if ($input_name_added)
			{							
				$this->addRadioInlineInput($vars, $this->orderedTestArray['hotspot_type']);
				$this->Ln(8);
			}
			else
			{							
				$this->formInputName($this->orderedTestArray['hotspot_type']);
				$this->addRadioInlineInput($vars, $this->orderedTestArray['hotspot_type']);
				$this->Ln(8);
			}

			
			$this->formInputName('# of Positive');
			$this->formBlank('short');

			$this->AddSectionHead('QUALITY:');

			$this->Ln(8);
			$this->formInputName('All '.$this->orderedTestArray['hotspot_type'].' properly amplified');
			$this->addRadioInlineInput(array('Yes - Valid', 'No - Sign out this test, add new test to infotrack, and repeat run'), 'All '.$this->orderedTestArray['hotspot_type'].' properly amplified');
			$this->Ln(8);
			$this->formInputName('Any comments about QC');
			$this->formBlank('extra long');
			$this->Ln(8);
			$this->formInputName('QC Status');
			$this->addRadioInlineInput(array('Pass', 'Fail'), 'QC Status');
			$this->formInputName('Wet Bench Tech');
			$this->formBlank('short');
			$this->formInputName('Analysis Tech');
			$this->formBlank('short');
		}

		private function smallTypeWordWrapped($add_words)
		{
			
			$this->AddHTML($add_words);
		}

		private function formInputName($input_name)
		{
			$this->SetFont('Arial','I', 10);
			$input_name = $input_name.': ';
			$width = $this->GetStringWidth($input_name) + 2;
			$this->Cell($width,6, $input_name,'',0,'');
		}

		private function formInput($input_name)
		{
			$this->SetFont('Arial','B', 12);
			$width = $this->GetStringWidth($input_name) + 2;
			$this->Cell($width,6, $input_name,'',0,'');
		}

		private function formBlank($blank_length)
		{
			switch ($blank_length) 
			{
				case 'short':
					$this->Cell(30,4, '','B',0,'');
					break;
				case 'medium':
					$this->Cell(60,4, '','B',0,'');
					break;
				case 'long':
					$this->Cell(90,4, '','B',0,'');
					break;
				case 'extra long':
					$this->Cell(120,4, '','B',0,'');
					break;
			}
		}

		private function addRadioInput($options_array, $form_input_name)
		{
			$this->SetFont('Arial','B', 10);

			$form_input_name = $form_input_name.':';

			$input_name_width = $this->GetStringWidth($form_input_name) + 2;
			
			$inital_x = $this->GetX() - $input_name_width;

			// move cursor to the right 2.
			$this->SetXY($this->GetX() +2, $this->GetY());
			$circle_x_loc = $this->GetX();
			
			foreach($options_array as $opt)
			{
				$curr_y = $this->GetY()+3;
				

				$this->Circle($circle_x_loc, $curr_y,1,'');

				$this->SetXY($circle_x_loc +2, $this->GetY());

				$width = $this->GetStringWidth($opt) + 5;
				$this->Cell($width,6, $opt,'',0,'');
				$this->Ln(5);
				
				$input_name_width = $inital_x + $input_name_width;
				// move over option by width of input name
				$this->SetXY($input_name_width, $this->GetY());
			}

			$this->Ln(1);
			
		}


		private function addRadioInlineInput($options_array)
		{
			$this->SetFont('Arial','B', 10);

			// move cursor to the right 2.
			$this->SetXY($this->GetX() +2, $this->GetY());

			foreach($options_array as $opt)
			{
				$curr_y = $this->GetY()+3;
				$curr_x = $this->Getx();

				$this->Circle($curr_x, $curr_y,1,'');

				$this->SetXY($this->GetX() +2, $this->GetY());

				$width = $this->GetStringWidth($opt) + 5;
				$this->Cell($width,6, $opt,'',0,'');
			}			
		}

		public function Results()
		{	

			$results = '';

			foreach ($this->reportArray as $key => $variant)
			{

				$results .=  $variant['result_mutant'];

				if ($key < sizeof($this->reportArray) - 2)
				{
					$results .= ', ';
				}

				elseif ($key < sizeof($this->reportArray) -1)
				{
					$results .=  ' AND ';
				}
			}

			$does_it_fit = $this->DoesItFit($results);

			// add section header
			$this->AddSectionHead('RESULTS:');

			// add section contents 
			$this->AddSectionContents($results);
		}

		public function Interpretation()
		{
			$this->Interpretation_pargraphs(explode('<br><br>', $this->section_split_interpt[0]));
		}

		private function addParagraphs($paragraph_array)
		{

			foreach ($paragraph_array as $key => $paragraph)
			{
				$paragraph = str_replace( "\n", '<br><br>', $paragraph);

				$does_it_fit = $this->DoesItFit($paragraph, 12, '', 'Arial', 14);
				$this->Ln(5);
				$this->AddHTML(trim($paragraph));
			}
		}

		private function Interpretation_pargraphs($split_interpt)
		{
			// add section header

			// find if the interpretation will fit on the page

			// find out how many new lines will be added.  There should be a newline for each variant - 1
			/* I'm thinking that if an interpretation takes up an entire front page I will need to rethink this does it fit
				$num_new_lines = sizeOf($this->reportArray) - 1;
				$extra_lines = $num_new_lines * 10; // each line is about 10 

				$to_add = '';

				foreach ($this->reportArray as $key => $variant)
				{
					// add section contents 
					$to_add .= $variant['comment'];
				}

				$does_it_fit = $this->DoesItFit($this->visitArray['method'], 12, '', 'Arial', $extra_lines);
			*/

			foreach ($split_interpt as $key => $paragraph)
			{
				
				// Make sure 
				$paragraph = str_replace( "\n", '<br><br>', $paragraph);	
				// check if header
				if (preg_match("/(INTERPRETATION|VARIANTS OF UNKNOWN SIGNIFICANCE|BENIGN VARIANTS)/", $paragraph))
				{

					$does_it_fit = $this->DoesItFit($paragraph.$split_interpt[$key+1],12, '', 'Arial', 20);
					$this->Ln(2);
					$this->AddSectionHead($paragraph);
				}
				else
				{
					// add paragraph return
					if ($key <= sizeof($split_interpt) - 1 && $key > 1)
					{				
						$this->Ln($this->sp_sections);			
					}

					$does_it_fit = $this->DoesItFit($paragraph, 12, '', 'Arial', 14);

					// convert charset types to UTF-8 
					// https://stackoverflow.com/questions/2477452/%C3%A2%E2%82%AC-showing-on-page-instead-of
					$updated_paragraph = mb_convert_encoding($paragraph, 'Windows-1252', 'UTF-8');

					// add PMID links and Italic genes
					$updated_paragraph = $this->AddItalicGenes($this->AddPMIDLinkUrls($updated_paragraph));

					$this->AddHTML($updated_paragraph);
				}	
			}

			// $this->Ln(6);	
		}

		private function DoesItFit($string, $font_size=12, $font_style='', $font='Arial', $extra_lines=0)
		{
			// purpose: find if there is enough room on the page to add the input string

			// set font 
			$this->SetFont($font, $font_style, $font_size);
			$column_width = 38;			
			$total_string_width = $this->GetStringWidth($string);

			// Determine the number of lines 
			$num_lines = $total_string_width / ($column_width -1);

			if ($this->header_status)
			{
				$num_lines = ceil($num_lines + 10); // round up
			}
			else
			{
				$num_lines = ceil($num_lines); // round up
			}		

			return $this->NumLinesFit($num_lines, $extra_lines);
		}

		private function NumLinesFit($num_lines, $extra_lines=0)
		{
			// check if theres enough space for a set number of lines

			$space_left = ceil($this->page_height - $this->GetY());

			// find if this new string will fit
			if ($space_left > $num_lines + $extra_lines)
			{
				return True;
			}
			else
			{
			
				$this->AddPage();
				
				// was starting too early to add second page info
				if ($this->header_status)
				{
					// $this->Ln();
					// $this->Ln();
				}

				return False;
			}
		}

		private function AddSectionHead($section_name)
		{
			if (strpos($section_name, 'LOW-COVERAGE') !== False)
			{
				$this->SetFont('Arial','B',8);
				$this->Ln($this->sp_sections-3);				
			}
			else
			{
				$this->SetFont('Arial','B',$this->std_font_size);
				$this->Ln($this->sp_sections);
			}
			

			// // On June 18th 2020 Dr. Olvati wanted VARIANTS OF UNKNOWN SIGNIFICANCE: updated to VARIANTS OF UNKNOWN OR BENIGN SIGNIFICANCE:
			// if ($section_name === 'VARIANTS OF UNKNOWN SIGNIFICANCE' && strtotime($this->visitArray['received']) > strtotime('2018-06-18'))
			// {
			// 	$section_name = 'VARIANTS OF UNKNOWN OR BENIGN SIGNIFICANCE';
			// }


			// // On June 18th 2020 Dr. Olvati wanted VARIANTS OF UNKNOWN SIGNIFICANCE: updated to VARIANTS OF UNKNOWN OR BENIGN SIGNIFICANCE:
			// if ($section_name === 'VARIANTS OF UNKNOWN SIGNIFICANCE:' && strtotime($this->visitArray['received']) > strtotime('2018-06-18'))
			// {
			// 	$section_name = 'VARIANTS OF UNKNOWN OR BENIGN SIGNIFICANCE:';
			// }



			$this->Cell(0,0, $section_name,'',0,'');
		}

		private function AddSectionContents($content, $font_size=12)
		{
			$this->SetFont('Arial','',$font_size);
			$this->write(4, $content);
		}

		public function Confirmation()
		{
			$pending_confirmations = '';
			$confirmed = '';
			$failed_confirmation = '';

			foreach($this->confirm_array as $i => $array)
			{
				if ($array['confirmation_status'] === 'pending')
				{
					$pending_confirmations .= $array['genes'].' '.$array['coding'].' ('.$array['amino_acid_change'].'), ';
				}

				elseif ($array['confirmation_status'] === 'confirmed')
				{
					$confirmed .= $array['genes'].' '.$array['coding'].' ('.$array['amino_acid_change'].'), ';
				}
				elseif ($array['confirmation_status'] === 'not confirmed')
				{
					$failed_confirmation .= $array['genes'].' '.$array['coding'].' ('.$array['amino_acid_change'].'), ';
				}
			}
		
			$does_it_fit = $this->DoesItFit($pending_confirmations);

			if ($pending_confirmations !== '')
			{
				$this->AddSectionHead('VARIANTS REQUIRING CONFIRMATION:');
				$this->AddSectionContents(rtrim($pending_confirmations, ', '));
			}


			if ($confirmed !== '')
			{
				$this->AddSectionHead('VARIANTS CONFIRMED:');
				$this->AddSectionContents(rtrim($confirmed, ', '));
			}

			if ($failed_confirmation !== '')
			{
				$this->AddSectionHead('VARIANTS FAILED CONFIRMATION:');
				$this->AddSectionContents(rtrim($failed_confirmation, ', '));
			}
		}

		public function MutantsTableRowHeightVaries($table_type)
		{
			// This function will produce a mutants table where the length of the input does not matter.  
			// The column heights will automatically adjust. Implemented for idllya assays only on 2/25/2021
			

			if (!empty($this->reportMutantTables))
			{

				// reportMutantTables array separates reportArray into a table of MUTATIONS and a 
				// table of VARIANTS OF UNKNOWN SIGNIFICANCE
				$this->Ln();

				// add each table in reportMutantTables.  The header is the key name
				foreach($this->reportMutantTables as $sectionHead => $tableArray)
				{

					if (!empty($tableArray[0]))
					{

						// // add section header
						$this->AddSectionHead($sectionHead.':');

						// add a line between header and table
						$this->Ln(5);

						// Add table Header
						$this->SetFont('Arial','B',10);
						$this->SetWidths(array(17,74,74,25));
						$this->Row(array('Gene','Coding', 'Protein', 'Call'));

						$this->SetFont('Arial','',10);
						
						// add table contents
						foreach ($tableArray[0] as $key => $row)
						{	

							$this->Row(array(

											isset($row['Gene']) && $row['Gene'] != '' ? $row['Gene'] : 'N/A',
											
											isset($row['DNA']) && $row['DNA'] != '' ? $row['DNA'] : 'N/A', 
											isset($row['Protein']) && $row['Protein'] != '' ? $row['Protein'] : 'N/A', 
											isset($row['genetic_call']) && $row['genetic_call'] != '' ? $row['genetic_call'] : 'N/A')
									);
							
						}										
					}
				}
				
			}
				
		}


		public function Mutants($table_type='full')
		{	

			if (!empty($this->reportArray))
			{

				// find if any coding or protein is too Big for to fit in a table.  
				// Max size is 40 characters
				// reportMutantTables array separates reportArray into a table of MUTATIONS 
				// and a table of VARIANTS OF UNKNOWN SIGNIFICANCE. To set control variable 
				// $mutantReportType just look at $reportArray
				$mutantReportType = 'table';
				foreach($this->reportArray as $key => $variant)
				{
					if 	(
							$table_type === 'full' &&
							(
								strlen($variant['coding']) > 40 || 
								strlen($variant['amino_acid_change'] > 40)
							)
						)
					{
						$mutantReportType = 'list';
						break;
					}
					else if 	(
							$table_type !== 'full' &&
							(
								strlen($variant['coding']) > 60 || 
								strlen($variant['amino_acid_change'] > 60)
							)
						)
					{
						$mutantReportType = 'list';
						break;
					}
				}

				if ($table_type === 'full')
				{
					$table_header='<table border="1">
					<tr>
						<th width="70" height="30">Gene</th>
						<th width="246" height="30">DNA</th>
						<th width="246" height="30">Protein</th>
						<th width="56" height="30">VAF</th>
						<th width="3" height="30"><sup>1</sup></th>
						<th width="90" height="30">COSMIC</th>
						<th width="3" height="30"><sup>2</sup></th>
						<th width="58" height="30">Tier</th>
						<th width="3" height="30"><sup>3</sup></th>
					</tr>';
				}
				else
				{
					$table_header='<table border="1">
					<tr>
						<th width="70" height="30">Gene</th>
						<th width="290" height="30">DNA</th>
						<th width="230" height="30">Protein</th>
						
						<th width="85" height="30">COSMIC</th>
						<th width="3" height="30"><sup>1</sup></th>
						<th width="85" height="30">Call</th>
						
					</tr>';
				}
				
					
				$this->MutantTableOrList($table_header, $mutantReportType, $table_type);

				$this->MutantFootNotes($table_type);
			}
			else
			{
				// instead of no mutation in report the interpretation summary is used 
				// $this->WriteHTML('No Mutations to report.');
			}
		}

		public function MultiCellBltArray($w, $h, $blt_array, $border=0, $align='J', $fill=false)
		{
			/************************************************************
			*                                                           *
			*    MultiCell with bullet (array)                          *
			*                                                           *
			*    Requires an array with the following  keys:            *
			*                                                           *
			*        Bullet -> String or Number                         *
			*        Margin -> Number, space between bullet and text    *
			*        Indent -> Number, width from current x position    *
			*        Spacer -> Number, calls Cell(x), spacer=x          *
			*        Text -> Array, items to be bulleted                *
			*        http://www.fpdf.org/en/script/script56.php                                                   *
			************************************************************/

			if (!is_array($blt_array))
			{
				die('MultiCellBltArray requires an array with the following keys: bullet,margin,text,indent,spacer');
				exit;
			}

			//Save x
			$bak_x = $this->x;

			for ($i=0; $i<sizeof($blt_array['text']); $i++)
			{
				//Get bullet width including margin
				$blt_width = $this->GetStringWidth($blt_array['bullet'] . $blt_array['margin'])+$this->cMargin*2;

				// SetX
				$this->SetX($bak_x);

				//Output indent
				if ($blt_array['indent'] > 0)
				{
					$this->Cell($blt_array['indent']);	
				}
				

				//Output bullet
				$this->Cell($blt_width,$h,$blt_array['bullet'] . $blt_array['margin'],0,'',$fill);

				//Output text
				$this->MultiCell($w-$blt_width,$h,$blt_array['text'][$i],$border,$align,$fill);

				//Insert a spacer between items if not the last item
				if ($i != sizeof($blt_array['text'])-1)
				{
					$this->Ln($blt_array['spacer']);	
				}
				

				//Increment bullet if it's a number
				if (is_numeric($blt_array['bullet']))
				{
					$blt_array['bullet']++;
				}
			}

			//Restore x
			$this->x = $bak_x;
		}

		function MutantTableOrList($table_header, $mutantReportType, $table_type='full')
		{
			// reportMutantTables array separates reportArray into a table of MUTATIONS and a 
			// table of VARIANTS OF UNKNOWN SIGNIFICANCE
			$this->Ln();

			// add each table in reportMutantTables.  The header is the key name
			foreach($this->reportMutantTables as $sectionHead => $tableArray)
			{

				if (!empty($tableArray[0]))
				{

					// add section header
					$this->AddSectionHead($sectionHead.':');

					// Change font to table font
					$this->SetFont('Arial','',10);

					// add a line between header and table
					$this->Ln();

					
					// add table header
					if ($mutantReportType === 'table')
					{
						// $html = $table_header;
						$this->WriteHTML($table_header);
					}

					$html='';
					// add table contents
					foreach ($tableArray[0] as $key => $row)
					{	

						// since a table cell needs info in it to actually make cell add N/A to 
						// which is empty
						$gene = isset($row['Gene']) && $row['Gene'] != '' ? $row['Gene'] : 'N/A';
						$dna = isset($row['DNA']) && $row['DNA'] != '' ? $row['DNA'] : 'N/A';
						$protein = isset($row['Protein']) && $row['Protein'] != '' ? $row['Protein'] : 'N/A';

						$vaf = isset($row['VAF']) && $row['VAF'] != '' ? $row['VAF'] : 'N/A';

						$cosmic = isset($row['COSMIC']) && $row['COSMIC'] != '' ? $row['COSMIC'] : 'N/A';
						
						$tier = isset($row['Tier']) && $row['Tier'] != '' ? $row['Tier'] : 'N/A';
						
						$genetic_call = isset($row['genetic_call']) && $row['genetic_call'] != '' ? $row['genetic_call'] : 'N/A';

						if ($mutantReportType === 'table')
						{							
							$html.=$this->MutantTable($gene, $dna, $protein, $vaf, $tier, $cosmic, $html, $genetic_call, $table_type);	
						}
						else if ($mutantReportType === 'list')
						{							
							$html.=$this->MutantList($gene, $dna, $protein, $vaf, $tier, $cosmic, $html, $genetic_call, $table_type);
						} 	
					}

					if ($mutantReportType === 'table')
					{
						// close table
						$html.='</table>';
					
						// write table to pdf
						$this->WriteHTML($html);
					}										
				}				
			}			
		}

		public function MutantList($gene, $dna, $protein, $vaf, $tier, $cosmic,  $html, $genetic_call, $table_type='full')
		{
			// Add each as a separate entry in list
			$non_bullet_format_array = array(
				'bullet' 	=> 	'',
				'margin'	=> 	'  ',
				'indent' 	=> 	2,
				'spacer'	=> 	2,
				'text'	=>	array(
						0 	=> 	'Coding: '.$dna
					)
			);

			$bullet_format_array = array(
				'bullet' 	=> 	chr(149),
				'margin'	=> 	' ',
				'indent' 	=> 	2,
				'spacer'	=> 	2,
				'text' 	=> 	array(
						0 	=> 	'Gene: '.$gene
					)
			);

			$line_height = 4.5;
			// Gene
			$this->Ln(2);
			$this->MultiCellBltArray($this->w-30, $line_height, $bullet_format_array);

			// Coding
			$this->MultiCellBltArray($this->w-30, $line_height, $non_bullet_format_array);

			// protein
			$non_bullet_format_array['text'][0] = 'Protein: '.$protein;
			$this->MultiCellBltArray($this->w-30, $line_height, $non_bullet_format_array);

			// Vaf, Tier, Cosmic
			$non_bullet_format_array['text'][0] = 'VAF: '.$vaf.', Tier: '.$tier. ', Cosmic: '.$cosmic;
			$this->MultiCellBltArray($this->w-30, $line_height, $non_bullet_format_array);

		}

		public function MutantTable($gene, $dna, $protein, $vaf, $tier, $cosmic, $html, $genetic_call, $table_type='full')
		{
			// There's only enough space for 30 characters at max for columns DNA and Protein.  By shrinking font starting at 28 characters more can be fit.  More than 30 characters is to large.  This will need to changed to a list instead of a table.
			$table_html = '';

			if ($table_type==='full')
			{

				if (strlen($dna) > 28 && strlen($dna) <= 35)
				{
					$dna_row = '<td width="246" height="30"><font size="8">'.$dna.'</td>';					
				}
				elseif(strlen($dna) > 35)
				{
					$dna_row = '<td width="246" height="30"><font size="6.8">'.$dna.'</td>';
				}
				else
				{
					$dna_row = '<td width="246" height="30">'.$dna.'</td>';
				}

				// protein
				if (strlen($protein) > 28 && strlen($protein) <= 35)
				{
					$protein_row = '<td width="246" height="30"><font size="8">'.$protein.'</td>';					
				}
				elseif(strlen($protein) > 35 )
				{
					$protein_row = '<td width="246" height="30"><font size="6.8">'.$protein.'</td>';
				}
				else
				{
					$protein_row = '<td width="246" height="30">'.$protein.'</td>';
				}

				$table_html.= '<tr>
					<td width="70" height="30"><i>'.$gene.'</i></td>
					'.$dna_row.$protein_row.'
					
					<td width="56" height="30" padding="0" margin="0">'.$vaf.'</td>
					<td width="90" height="30" padding="0" margin="0"><font size="8">'.$cosmic.'</font></td>
					<td width="58" height="30" padding="0" margin="0">'.$tier.'</td>
				</tr>';
			}

			else
			{

				if (strlen($dna) > 45 && strlen($dna) <= 50)
				{
					$dna_row = '<td width="290" height="30"><font size="8">'.$dna.'</td>';					
				}
				elseif(strlen($dna) > 50)
				{
					$dna_row = '<td width="290" height="30"><font size="6.8">'.$dna.'</td>';
				}
				else
				{
					$dna_row = '<td width="290" height="30">'.$dna.'</td>';
				}

				// protein
				if (strlen($protein) > 45 && strlen($protein) <= 50)
				{
					$protein_row = '<td width="230" height="30"><font size="8">'.$protein.'</td>';					
				}
				elseif(strlen($protein) > 50)
				{
					$protein_row = '<td width="230" height="30"><font size="6.8">'.$protein.'</td>';
				}
				else
				{
					$protein_row = '<td width="230" height="30">'.$protein.'</td>';
				}

				$table_html.= '<tr>
					<td width="70" height="30"><i>'.$gene.'</i></td>
					'.$dna_row.$protein_row.'
				
					<td width="85" height="30" padding="0" margin="0"><font size="8">'.$cosmic.'</font></td>
					<td width="85" height="30" padding="0" margin="0"><font size="8">'.$genetic_call.'</font></td>
				</tr>';
			}

			return $table_html;
		}

		public function MutantFootNotes($table_type='full')
		{
			$this->Ln(8);

			if($this->panel_type == 'Myeloid/CLL Mutation Panel' || $this->panel_type == 'Archer VariantPlex Myeloid Assay')
			{
    				$VAF = 'VAF, variant allele fraction.  The VAF is the percent of reads with the mutation.  Although it will usually be a good approximation of the fraction of DNA with the mutation, errors can occur.  For FLT3-ITD mutations the VAF appears to underestimate the actual fraction of DNA with the mutation.';
    			}
    			elseif($this->panel_type == 'Oncomine Focus Hotspot Assay' )
			{
				$VAF = 'VAF, variant allele fraction.  The VAF is the percent of reads with the mutation.  Although it will usually be a good approximation of the fraction of DNA with the mutation, errors can occur.';
			}
			else
			{
				$VAF = '';
			}

			if ($table_type === 'full')
			{
				$this->SetFont('Arial','',7);
				$this->subWrite(3,'1',6,4);
				$this->write(3, $VAF);
				$this->Ln(4);
			
    			$cosmic = 'Catalogue of Somatic Mutations in Cancer (http://cancer.sanger.ac.uk/cosmic/). ';

				$this->SetFont('Arial','',7);
				$this->subWrite(3,'2',6,4);

				$this->AddHTML($this->AddPMIDLinkUrls($cosmic), 7); 
				//(<a href="http://cancer.sanger.ac.uk/cosmic" target="_blank">http://cancer.sanger.ac.uk/cosmic/</a>
				$this->Ln(5);
	    			
	    			$tier = 'Tier is an interpretation of pathogenicity (Li  et al 2017, J Mol Diag 19:4): Tier I variants have strong clinical significance (definitely pathogenic); Tier II variants have potential clinical significance (probably pathogenic); Tier III are variants of unknown significance (VUS); Tier IV variants are benign or likely benign, and will not be reported.';

				$this->SetFont('Arial','',7);
				$this->subWrite(3,'3',6,4);
				$this->write(3, $tier);
				$this->Ln(4);	
			}
			else
			{
				$cosmic = 'Catalogue of Somatic Mutations in Cancer (http://cancer.sanger.ac.uk/cosmic/). ';

				$this->SetFont('Arial','',7);
				$this->subWrite(3,'1',6,4);

				$this->AddHTML($this->AddPMIDLinkUrls($cosmic), 7); 
				
				$this->Ln(4);
			}   

			if($this->panel_type == 'Myeloid/CLL Mutation Panel' || $this->panel_type == 'Archer VariantPlex Myeloid Assay')
			{
    				$note = 'We did not detect any other mutations in the genes on this panel.  See below for a list of regions with low coverage.  A list of genes included on this panel and the analytical sensitivity for detection of mutations is described in the Methods section below.';
    			}

    			elseif($this->panel_type == 'Oncomine Focus Hotspot Assay' )
			{
				$note = 'No genomic alterations identified in any of the genes included in this panel.  See below for a list of regions with low coverage.  For a complete list of the genes assayed and performance specifications, please refer to the "Methods" section below."';
			}
			else
			{
				$note = '';
			}
			$this->SetFont('Arial','',7);
			$this->write(3, $note);
		}

		public function LowChrStartFinishCoverage($lowCoverageArray)
		{
			
			$low_cov_table = '<table border="1">
					<tr>
						<th width="70" height="30">Gene</th>
						<th width="240" height="30">Amplicon</th>
						<th width="70" height="30">Chr</th>
						<th width="145" height="30" padding="0" margin="0">Amp Start</th>	
						<th width="145" height="30" padding="0" margin="0">Amp End</th>	
						<th width="90" height="30" padding="0" margin="0">Depth</th>
					</tr>';

			// not every low coverage region is in an area that the panel covers since we are not 
			// reporting all genes on the panel.  Therefore check to see that a region is reported
			// before writing the header of the table.
			$active_region_count = 0;

			foreach ($lowCoverageArray as $key => $amp)
			{
				if ($amp['activate_status'])
				{
					$active_region_count ++;
					$low_cov_table .='<tr>';
		
						$low_cov_table .='<td width="70" height="30"><i>'.$amp['gene'].'</i></td>';
						$low_cov_table .='<td width="240" height="30">'.$amp['Amplicon'].'</td>';
						$low_cov_table .='<td width="70" height="30">'.$amp['chr'].'</td>';
						$low_cov_table .='<td width="145" height="30" padding="0" margin="0">'.$amp['amp_start'].'</td>';
						$low_cov_table .='<td width="145" height="30" padding="0" margin="0">'.$amp['amp_end'].'</td>';
						$low_cov_table .='	<td width="90" height="30" padding="0" margin="0">'.$amp['depth'].'</td>';
					$low_cov_table .='</tr>';
				}
			}
			$low_cov_table .='</table>';

			if ($active_region_count > 0)
			{
				$this->AddSectionHead('LOW-COVERAGE (<500-fold) REGIONS:');
				$this->SetFont('Arial','',7);
				$this->WriteHTML($low_cov_table);		
			}				
		}
			
		public function LowExonCodonsCoverage($lowCoverageArray)
		{
			$this->AddSectionHead('LOW-COVERAGE (<500-fold) REGIONS:');
			$this->SetFont('Arial','',7);

			$low_cov_table = '<table border="1">
					<tr>
						<th width="70" height="30">Gene</th>
						<th width="240" height="30">Amplicon</th>
						<th width="240" height="30">Exon</th>
						<th width="120" height="30" padding="0" margin="0">Codons</th>					
						<th width="90" height="30" padding="0" margin="0">Depth</th>
					</tr>';

			foreach ($lowCoverageArray as $key => $amp)
			{
				$low_cov_table .='<tr>';
	
					$low_cov_table .='<td width="70" height="30"><i>'.$amp['gene'].'</i></td>';
					$low_cov_table .='<td width="240" height="30">'.$amp['Amplicon'].'</td>';
					$low_cov_table .='<td width="240" height="30">'.$amp['exon'].'</td>';
					$low_cov_table .='<td width="120" height="30" padding="0" margin="0">'.$amp['codons'].'</td>';
					$low_cov_table .='	<td width="90" height="30" padding="0" margin="0">'.$amp['depth'].'</td>';
				$low_cov_table .='</tr>';
			}
			$low_cov_table .='</table>';
			$this->WriteHTML($low_cov_table);	
	
		}

		public function noLowCoverage()
		{
			// add no low coverage statement
			$this->AddSectionHead('LOW-COVERAGE (<500-fold) REGIONS:');
			$this->SetFont('Arial','',7);
			$this->WriteHTML('All of the targeted regions had coverage of at least 500-fold. There were no regions of low-coverage.');
		}

		public function Methodology()
		{

			$does_it_fit = $this->DoesItFit($this->visitArray['method'], 8);

			// add section header
			$this->AddSectionHead('METHODOLOGY:');
			$this->Ln(2);
			// add methodology from visitArray for this panel
			$this->AddSectionContents($this->AddHTML($this->AddItalicGenes($this->AddPMIDLinkUrls($this->visitArray['method'])),8), 8);
			
		}

		public function Limitations()
		{
			// Limitations are pulled from the table ngs panel.  If limiations is left blank 
			// nothing is added.
			$does_it_fit = $this->DoesItFit($this->visitArray['limitations'], 8);

			if ($this->visitArray['limitations'] != '')
			{
				// add section header
				$this->AddSectionHead('LIMITATIONS:');

				// add methodology from visitArray for this panel
				$this->AddSectionContents($this->AddHTML($this->AddItalicGenes($this->AddPMIDLinkUrls($this->visitArray['limitations'])),8), 8);
			}
		}

		private function GetCoveredGeneTableHeader($accession_num_present)
		{
			if ($accession_num_present)
			{
				return '<table border="1">
					<tr>
						<th width="'.$this->genes_table_col_1_width.'" height="26">Gene</th>
						<th width="'.$this->genes_table_col_2_width.'" height="26">Accession #</th>
						<th width="'.$this->genes_table_col_3_width.'" height="26">Exon(s)</th>

						<ts width="10" height="26">. </ts>

						<th width="'.$this->genes_table_col_1_width.'" height="26">Gene</th>
						<th width="'.$this->genes_table_col_2_width.'" height="26">Accession #</th>
						<th width="'.$this->genes_table_col_3_width.'" height="26">Exon(s)</th>

						<ts width="10" height="26">. </ts>

						<th width="'.$this->genes_table_col_1_width.'" height="26">Gene</th>
						<th width="'.$this->genes_table_col_2_width.'" height="26">Accession #</th>
						<th width="'.$this->genes_table_col_3_width.'" height="26">Exon(s)</th>
					</tr>	
				';
			}
			else
			{
				return '<table border="1">
					<tr>
						<th width="'.$this->genes_table_col_1_width.'" height="26">Gene</th>
						<th width="'.$this->genes_table_col_2_width.'" height="26">Exon(s)</th>

						<ts width="10" height="26">. </ts>

						<th width="'.$this->genes_table_col_1_width.'" height="26">Gene</th>
						<th width="'.$this->genes_table_col_2_width.'" height="26">Exon(s)</th>

						<ts width="10" height="26">. </ts>

						<th width="'.$this->genes_table_col_1_width.'" height="26">Gene</th>
						<th width="'.$this->genes_table_col_2_width.'" height="26">Exon(s)</th>

						<ts width="10" height="26">. </ts>

						<th width="'.$this->genes_table_col_1_width.'" height="26">Gene</th>
						<th width="'.$this->genes_table_col_2_width.'" height="26">Exon(s)</th>
					</tr>	
				';
			}
		}

		////////////////////////////////////////////////////
		// Functions below are for making tables with multiple row heights
		// Copied from here: http://www.fpdf.org/en/script/script3.php
		///////////////////////////////////////////////////
		function SetWidths($w)
		{
		    //Set the array of column widths
		    $this->widths=$w;
		}

		function SetAligns($a)
		{
		    //Set the array of column alignments
		    $this->aligns=$a;
		}

		function Row($data)
		{
		    //Calculate the height of the row
		    $nb=0;
		    for($i=0;$i<count($data);$i++)
		        $nb=max($nb,$this->NbLines($this->widths[$i],$data[$i]));
		    $h=5*$nb;
		    //Issue a page break first if needed
		    $this->CheckPageBreak($h);
		    //Draw the cells of the row
		    for($i=0;$i<count($data);$i++)
		    {
		        $w=$this->widths[$i];

		        $a=isset($this->aligns[$i]) ? $this->aligns[$i] : 'L';
		        //Save the current position
		        $x=$this->GetX();
		        $y=$this->GetY();
		        //Draw the border
		        $this->Rect($x,$y,$w,$h);
		        //Print the text
		        $this->MultiCell($w,5,$data[$i],0,$a);
		        //Put the position to the right of the cell
		        $this->SetXY($x+$w,$y);
		    }
		    //Go to the next line
		    $this->Ln($h);
		}

		function CheckPageBreak($h)
		{
		    //If the height h would cause an overflow, add a new page immediately
		    if($this->GetY()+$h>$this->PageBreakTrigger)
		        $this->AddPage($this->CurOrientation);
		}

		function NbLines($w,$txt)
		{
		    //Computes the number of lines a MultiCell of width w will take
		    $cw=&$this->CurrentFont['cw'];
		    if($w==0)
		        $w=$this->w-$this->rMargin-$this->x;
		    $wmax=($w-2*$this->cMargin)*1000/$this->FontSize;
		    $s=str_replace("\r",'',$txt);
		    $nb=strlen($s);
		    if($nb>0 and $s[$nb-1]=="\n")
		        $nb--;
		    $sep=-1;
		    $i=0;
		    $j=0;
		    $l=0;
		    $nl=1;
		    while($i<$nb)
		    {
		        $c=$s[$i];
		        if($c=="\n")
		        {
		            $i++;
		            $sep=-1;
		            $j=$i;
		            $l=0;
		            $nl++;
		            continue;
		        }
		        if($c==' ')
		            $sep=$i;
		        $l+=$cw[$c];
		        if($l>$wmax)
		        {
		            if($sep==-1)
		            {
		                if($i==$j)
		                    $i++;
		            }
		            else
		                $i=$sep+1;
		            $sep=-1;
		            $j=$i;
		            $l=0;
		            $nl++;
		        }
		        else
		            $i++;
		    }
		    return $nl;
		}

		////////////////////////////////////////////////////
		// Functions above are for making tables with multiple row heights
		// Copied from here: http://www.fpdf.org/en/script/script3.php
		///////////////////////////////////////////////////

		public function HotSpotsCovered()
		{
			$this->NumLinesFit(50);
			$this->AddSectionHead('TABLE OF HOTSPOTS:');
			$this->SetFont('Arial','B', 7.5);
			$this->Ln(5);

			// Example $this->genesCoveredArray for hotspots
			// array (size=13)
		      // 'test_hotspots_id' => string '53' (length=2)
		      // 'orderable_tests_id' => string '35' (length=2)
		      // 'user_id' => string '1' (length=1)
		      // 'genes' => string 'BRAF' (length=4)
		      // 'amino_acid_change' => string 'p.Val600Lys or p.Val600Arg' (length=26)
		      // 'coding' => string 'c.1798_1799delinsAA' (length=19)
		      // 'exon' => string '15' (length=2)
		      // 'codon' => string '600' (length=3)
		      // 'ordered_test_genetic_call' => string 'V600K/R' (length=7)
		      // 'mutation_abbr' => string '' (length=0)
		      // 'reported' => string 'yes' (length=3)
		      // 'date_name_added' => string '2020-12-22' (length=10)
		      // 'time_stamp' => string '2021-02-09 10:18:59' (length=19)

			$this->SetWidths(array(10,10,12,70,68,17));
						
			$this->Row(array('Gene','Exon','Codon','Coding', 'Protein', 'Call'));
			$this->SetFont('Arial','', 7.5);
			
			foreach ($this->genesCoveredArray as $key => $curr_hotspot) 
			{
				$this->Row(array(

						$curr_hotspot['genes'],
						$curr_hotspot['exon'],
						$curr_hotspot['codon'],
						$curr_hotspot['coding'], 
						$curr_hotspot['amino_acid_change'], 
						$curr_hotspot['ordered_test_genetic_call'])
				);
			}
		}

		private function tdheight($all_row_data, $max_num_chars_array, $default_height)
		{

			foreach($max_num_chars_array as $col_name => $max_chars)
			{
				// Increase height of row if any column is greater than the $max_chars by 15.  This means any more
				// reduction in fontsize will not help and the height of the row needs to increase currently the 
				// row will be increased by the number of the , in the data
				if (strlen($all_row_data[$col_name]) > $max_chars + 15)
				{
					return $default_height * substr_count($all_row_data[$col_name], ',');
				}
			}

			return $default_height;
		}

		private function tdSizing($td_content, $td_width, $td_height, $td_normal_font_size, $max_num_characters)
		{	
			// This function will size down columns with font size until a taller column is required

			// replace empty td_content with .
			if (empty($td_content))
			{
				$td_content = '.';
			}


			if (strlen($td_content) > $max_num_characters + 10)
			{
				$font_size = strval($td_normal_font_size - 1.5);

				return '<td width="'.strval($td_width).'" height="'.strval($td_height).'" padding="0" margin="0"><font size="'.$font_size.'">'.strval($td_content).'</font size></td>';
			}

			else if (strlen($td_content) > $max_num_characters + 5)
			{
				$font_size = strval($td_normal_font_size - 1);

				return '<td width="'.strval($td_width).'" height="'.strval($td_height).'" padding="0" margin="0"><font size="'.$font_size.'">'.strval($td_content).'</font size></td>';
			}


			else if (strlen($td_content) > $max_num_characters)
			{
				$font_size = strval($td_normal_font_size - 0.5);

				return '<td width="'.strval($td_width).'" height="'.strval($td_height).'" padding="0" margin="0"><font size="'.$font_size.'">'.strval($td_content).'</font size></td>';
			}

			else
			{
				return '<td width="'.strval($td_width).'" height="'.strval($td_height).'" padding="0" margin="0"><font size="'.strval($td_normal_font_size).'">'.strval($td_content).'</font size></td>';
			}
		}


		public function CoveredGenes($accession_num_present)
		{
			// This function puts in a table of genes for each panel.
			// All tables are html is coded here since each panel will have 
			// A specific table structure.

			// since the table of genes has two formats depending on if accession number is present 
			// or not keep track of column widths here.  Default is 3 parallel tables with 3 
			// columns each.  This is used when accession number is known.  If accession number is 
			// not known for the entire table make 4 parallel tables with 2 columns each.  The 
			// second column will also be wider. 
			
			$this->NumLinesFit(50);
			$this->AddSectionHead('TABLE OF GENES:');
			$this->SetFont('Arial','B', 7.5);
			$this->Ln();

			// set column widths
			if (!$accession_num_present)
			{
				$this->genes_table_col_1_width = '78';
				$this->genes_table_col_2_width = '105';	
				$this->genes_table_num_tables = 4;			
			}

			// add header of table of genes table
			$html= $this->GetCoveredGeneTableHeader($accession_num_present);

			// iterate over all of the genes covered for the panel at the time the assay
			// was run.  Sometimes genes are added later so this function has to be dynamic.
			$curr_table_col_num = 1;
			foreach ($this->genesCoveredArray as $key => $curr_gene) 
			{
				// theres 3 columns to this table.  Each is a little different depending on adding a new row. closing the row or adding a spacer <ts>

				// add row
				if ($curr_table_col_num === 1)
				{
					$html.= '<tr>';
				}
				
				if ($accession_num_present)
				{
					// add gene to row
					$html.='<td width="'.$this->genes_table_col_1_width.'" height="26"><i>'.$curr_gene['gene'].'</i></td>
					<td width="'.$this->genes_table_col_2_width.'" height="26">'.$curr_gene['accession_num'].'</td>
					<td width="'.$this->genes_table_col_3_width.'" height="26">'.$curr_gene['exon'].'</td>';
				}
				else
				{
					// add gene to row
					$html.='<td width="'.$this->genes_table_col_1_width.'" height="26"><i>'.$curr_gene['gene'].'</i></td>
					<td width="'.$this->genes_table_col_2_width.'" height="26">'.$curr_gene['exon'].'</td>';
				}

				// add spacers
				if ($curr_table_col_num < $this->genes_table_num_tables)
				{
					$html.='<ts width="10" height="26">. </ts>';
				}

				// close row
				if ($curr_table_col_num === $this->genes_table_num_tables)
				{
					$html.= '</tr>';
				}

				$curr_table_col_num++;

				// reset if greater than 3 colums
				if ($curr_table_col_num > $this->genes_table_num_tables)
				{
					$curr_table_col_num = 1;
				}

			}
			
			$html.='</table>';

			$this->WriteHTML($html);				
		}

		public function AddSignature()
		{
			// Add to report of whoever has signed off on the report 
			
			// allow enough space for lines, medical tech and confirmed
			$this->NumLinesFit(34);

			// horizontal dividing line
			if ($this->header_status)
			{
				$this->Ln(6);			
				$this->Cell(0,5, '','B',0,'');
			}

			// add wet bench tech
			// if ($this->header_status && isset($this->visitArray['wet_bench_tech']) && !empty($this->visitArray['wet_bench_tech']) )
			// {
			// 	$this->Ln(6);
			// 	$this->SetFont('Arial','I', 10);
			// 	$this->Cell(50,6, 'Wet Bench Techs'.': ','',0,'');
			// 	$this->SetFont('Arial','B', 12);
			// 	$this->Cell(101,6, $this->visitArray['wet_bench_tech'],'',0,'');
			// }


			// // add report helper
			// if ($this->header_status && $this->in_array_r('tech_approved', $this->approval_steps))
			// {
			// 	$tech_approved_info = $this->FindSubArrayContaining('tech_approved', $this->approval_steps);

			// 	$this->Ln(6);
			// 	$this->SetFont('Arial','I', 10);
			// 	$this->Cell(50,6, 'Report Tech: ','',0,'');
			// 	$this->SetFont('Arial','B', 12);
			// 	$this->Cell(101,6, $tech_approved_info['user_name'].', '.$tech_approved_info['credentials'],'',0,'');				
			// }
			
			$this->Ln(6);			

			// add electronic signature 
			if ($this->in_array_r('confirmed', $this->approval_steps))
			{
				$director_approved_info = $this->FindSubArrayContaining('confirmed', $this->approval_steps);

				$this->SetFont('Arial','BIU', 10);

				if ($this->header_status)
				{
					$this->Cell(50,6, 'Electronic Signature ','',0,'');
					$this->Ln(6);
					$this->SetFont('Arial','I', 10);
					$this->Cell(50,6, 'Reviewed By: ','',0,'');
				}
				else
				{
					$this->SetFont('Arial','I', 10);
					$this->Cell(50,6, 'Reviewed By: ','',0,'');
				}

				
				$this->SetFont('Arial','', 12);
				$this->Cell(101,6, $director_approved_info['user_name'].', '.$director_approved_info['credentials'],'',0,'');
				$this->Ln(6);
				$this->SetFont('Arial','I', 10);

				if ($this->header_status)
				{
					$this->Cell(50,6, 'Approved Date: ','',0,'');
				}
				else
				{
					$this->Cell(50,6, 'Date: ','',0,'');
				}
				
				$this->SetFont('Arial','', 12);
				$this->Cell(101,6, explode(' ', $director_approved_info['time_stamp'])[0],'',0,'');
			}

			// horizontal dividing line
			if ($this->header_status)
			{
				$this->Ln(1);			
				$this->Cell(0,5, '','B',0,'');
			}
		}
		
		public function Disclaimer()
		{

			$this->NumLinesFit(30);
			$this->Ln(7);
			// $this->AddSectionHead('DISCLAIMER:');
			$this->SetFont('Arial','',10);
			$this->SetFont('Arial','',8);
			$this->write(3, $this->visitArray['disclaimer']);
		}

		public function barcode($code,$width=20)
		{

			$x = $this->Getx();
			$y = $this->Gety();
			
			$this->Code128($x, $y, $code, $width,8);

			// $this->SetXY($x+20, $y+2);
			$this->SetXY($x+$width, $y+2);

			$this->SetFont('Arial','',6);
			$this->Write(5,$code);

			$x = $this->Getx();
			$y = $this->Gety();
			$this->SetXY($x+5, $y-2);

		}

		public function Footer()
		{
			$this->SetTextColor(0);
			// Add Page footer to every page

			// Position at 1.5 cm from bottom
			$this->SetY(-15);

			// Arial italic 8
			$this->SetFont('Arial','I',8);

			if ($this->footer_status)
			{					
				$curr_page_num = $this->PageNo();

				// Download date time
				$download_date_time = ' Downloaded: '.date("Y-m-d").' '.date("h:i:sa").' by: '.$this->download_user_initials;

				// add patient name and molecular number of all pages which are not page 1
				if ($this->header_status && $curr_page_num == 1)
				{
					$footer_content = $download_date_time.' Page '.$curr_page_num.'/{nb}';
					// Page number
					$this->Cell(0,10, $footer_content ,0,0,'C');
				}
				else
				{
					$footer_content = 'Name: '.$this->patientArray['patient_name'].'  DOB: '.$this->patientArray['dob'].'  AGE:  '.$this->calc_age($this->patientArray['dob'], $this->visitArray['time_stamp']).'  MRN: '.$this->patientArray['medical_record_num'].'  MD#: '.$this->visitArray['mol_num'];

					$this->Cell(0,10, $footer_content ,0,0,'C');
					$this->Ln(3);
					$footer_content = 'Soft Path #: '.$this->visitArray['soft_path_num'].' '.$download_date_time.'  Page '.$curr_page_num.'/{nb}';
					$this->Cell(0,10, $footer_content ,0,0,'C');
				}
			}
			else
			{
				// add soft lab barcode
				if (isset($this->visitArray['soft_lab_num']) && !empty($this->visitArray['soft_lab_num']))
				{
					$code = $this->visitArray['soft_lab_num'];
				}
				else if 	(
						isset($this->sampleLogBookArray['soft_lab_num']) && 
						!empty($this->sampleLogBookArray['soft_lab_num'])
						)
				{
					$code = $this->sampleLogBookArray['soft_lab_num'];
				}

				if (isset($code))
				{
					$this->barcode($code);
				}

				if (isset($this->visitArray['mol_num']) && !empty($this->visitArray['mol_num']))
				{
					$mol_code = $this->visitArray['mol_num'];
				}
				else if 	(
						isset($this->sampleLogBookArray['mol_num']) && 
						!empty($this->sampleLogBookArray['mol_num'])
						)
				{
					$mol_code = $this->sampleLogBookArray['mol_num'];
				}
				
				if (isset($mol_code))
				{
					$this->barcode($mol_code);
				}
				
				if (isset($this->patientArray['medical_record_num']) && !empty($this->patientArray['medical_record_num']))
				{
					$mrn_code = $this->patientArray['medical_record_num'];
				}
				else if 	(
						isset($this->sampleLogBookArray['medical_record_num']) && 
						!empty($this->sampleLogBookArray['medical_record_num'])
						)
				{
					$mrn_code = $this->sampleLogBookArray['medical_record_num'];
				}
				
				if (isset($mrn_code))
				{
					$this->barcode($mrn_code);
				}

				// Add sample barcode
				if (
						isset($this->sampleLogBookArray['mol_num']) && 
						!empty($this->sampleLogBookArray['mol_num']) &&
						isset($this->sampleLogBookArray['soft_lab_num']) && 
						!empty($this->sampleLogBookArray['soft_lab_num'])
					)
				{
					$sample_barcode = $this->sampleLogBookArray['mol_num'].'_'.$this->sampleLogBookArray['soft_lab_num'];
				}
				
				if (isset($sample_barcode))
				{
					$this->barcode($sample_barcode,50);
				}


				// $this->Cell(0,10, SITE_TITLE_ABBR.' '.VERSION ,0,0,'R');
			}



		}
	}
?>