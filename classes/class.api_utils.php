<?php

	class APIUtils extends utils
	{
		// public $root_url = 'devs';//explode('/', $_SERVER['REQUEST_URI'])[1];
		public $ion_torrent_backup_dir = '/media/storage/fs-ngs/';

		public $tech_called_ion_torrent_backup_dir = 'N: ';
		
		public $server = array(

			'Ion Torrent'		=> 	array(
										'model'	=> 	'S5XL',
										'instrument serial #' 	=> 	'2772816100108',
										'contacts'	=> 	
										array(
											'John Zhang'	=> 	'John.Zhan@thermofisher.com',
											'Ricardo Costa'	=> 'RicardoDallaCosta@thermofisher.com',
											'Ed Horsey' 	=> 'edward.horsey@thermofisher.com',
											'Matthew Goodman'=> 'Matt.Goodman@thermofisher.com'
										),
										'instrument service call'=> '1-800-955-6288'
									),
			'Ion_Reporter'		=>	array(
										'ip' 	=> 	'10.149.59.209',
										'api_key'	=> 	'YmUyMzU5NzA1NzlmNjBkNDYyMTk3NjVmOTlmNTdjOWFiOGQwZTgxNWFkMzg5ODMzMTJkODU2NTI4Mjg1MGRiYQ',
										'api_server' => 'https://10.149.59.209/api/v1/',
										'MAC' 	=> 	'14:18:77:2d:09:76',
										'username'=>	'mdlofa',
										'password'=> 	'Th3rm0f1sh3r',
										'name' 	=> 	'2F65182',
										'dell service tag' 	=> 	'2F65182',
										'mounted_loc' => '/media/2f65182'
									),
			'Torrent_Server'	=>	array(
										'ip' 	=> 	'10.149.58.208',
										'api_key'	=> 	'89476b3a1803cdcb1616848a21842bf37592f0d6',
										// 'username'=>	'molecdiag@urmc.edu',
										'username'=>	'mdlofa',
										'password'=> 	'Danryan_1', // API does not use this password.  It only uses key an username
										'MAC' 	=> 	'18:66:da:63:7c:50',
										'name' 	=> 	'H28TPD2',
										'dell service tag' 	=> 	'H28TPD2',
										'mounted_loc' => '/media/h28tpd2/results/analysis'


										// 'api_key'	=> 	'3f2cdec72f8b1783dfc1222cb4f4164c79de53a7',
										// // 'username'=>	'molecdiag@urmc.edu',
										// 'username'=>	'ionadmin',//'mdlofa',
										// 'password'=> 	'ionadmin',//'Danryan#1', // API does not use this password.  It only uses key an username
										// 'MAC' 	=> 	'18:66:da:63:7c:50',
										// 'name' 	=> 	'H28TPD2',
										// 'dell service tag' 	=> 	'H28TPD2',
										// 'mounted_loc' => '/media/h28tpd2/results/analysis'

									),
			'IONDataSafe'			=>	array(
										'ip' 		=> 	'172.18.126.41',
										'api_key'	=> 	'1760118d18e70786f5c5bb22ff26a9b6b7ba69b8',
										'username'	=>	'molecdiag@urmc.edu',
										'password'	=> 	'danryan1',
										'MAC' 		=> 	'00:07:b4:00:01:01',
										'name' 		=> 	'torrenttnas',
										'dell service tag' 	=> 	'8BXMMD2'
									),			
			'MiSeq_Control'	=> 	array(
										'address' =>	'\\M-M70203R\MiSeq​ Control Software',
										'username'=> 	'sbsuser',
										'password'=> 	'Danryan#1' 
									),
			'backup_locs'		=>	array(
										'ion_torrent_backup_dir'	=>	'',
										'tech_called_ion_torrent_backup_dir' 	=> ''
									)

		);
		
		public function __construct()
		{
			$this->Torrent_server_login_creditials = '?username='.$this->server['Torrent_Server']['username'].'&password='.$this->server['Torrent_Server']['password'].'&api_key='.$this->server['Torrent_Server']['api_key'];

			$this->Ion_reporter_login_creditials = '?username='.$this->server['Ion_Reporter']['username'].'&password='.$this->server['Ion_Reporter']['password'].'&api_key='.$this->server['Ion_Reporter']['api_key'];
			$this->setBackupLocs();
		}

		public function setBackupLocs()
		{
			if (ROOT_URL === 'devs')
			{
				$this->ion_torrent_backup_dir.='devs/ion_torrent';
				$this->tech_called_ion_torrent_backup_dir.='devs/ion_torrent';
			}
			else
			{
				$this->ion_torrent_backup_dir.='ion_torrent';
				$this->tech_called_ion_torrent_backup_dir.='ion_torrent';	
			}

			$this->server['backup_locs']['ion_torrent_backup_dir'] = $this->ion_torrent_backup_dir;
			$this->server['backup_locs']['tech_called_ion_torrent_backup_dir'] = $this->tech_called_ion_torrent_backup_dir;
		}

		public function checkAPIkeys($selected_server, $api_key)
		{
			return $this->server[$selected_server]['api_key'] === $api_key;
		}

		public function getAPIkey($selected_server)
		{
			if (isset($this->server[$selected_server]['api_key']))
			{
				return $this->server[$selected_server]['api_key'];
			}
			else
			{
				return 'Not Found';
			}			
		}

		public function ConvertBackupLocToTechFormat($pathlamp_backup_loc)
		{
			// replace old loc 
			$return_url = str_replace('/media/storage/fs-ngs/dev', $this->server['backup_locs']['tech_called_ion_torrent_backup_dir'], $pathlamp_backup_loc);

			$return_url = str_replace($this->server['backup_locs']['ion_torrent_backup_dir'], $this->server['backup_locs']['tech_called_ion_torrent_backup_dir'], $return_url);

			return $return_url;
		}

		public function GetTorrentServerWebSite()
		{
			return $this->server['Torrent_Server']['ip'];
		}

		public function TorrentServerResults($month, $year)
		{
			// Obtain the result names for the most recent results.  Only include status Completed results and  

			$url = 'http://'.$this->server['Torrent_Server']['ip'].'/rundb/api/v1/results/'.$this->Torrent_server_login_creditials.'&timeStamp__month='.$month.'&timeStamp__year='.$year.'&order_by=-timeStamp';

			// $url = 'http://'.$this->server['Torrent_Server']['ip'].'/rundb/api/v1/results/'.$this->Torrent_server_login_creditials;

			$response_array = $this->AccessRestAPI($url);

			$keep_run_array = array();

			if ($response_array)
			{
				foreach ($response_array['objects'] as $key => $run)
				{			
					// Make sure the plugins ran.
					if (empty($run['pluginresults']) || sizeOf($run['pluginresults']) < 2) 
					{
						continue;
					}


					// remove thumbnail results and status not complete
					// If run results are just a thumbnail it will have 
					// 'metaData' => 
						// array (size=1)
						//   'thumb' => int 1
					if (empty($run['metaData']) && $run['status'] === 'Completed')
					{
						$curr_run = array(
							'result_id' 	=>	isset($run['id']) ? $run['id'] :'',
							'time_stamp'	=> 	isset($run['timeStamp']) ? $run['timeStamp'] :'',
							'results_name'	=> 	isset($run['resultsName']) ? $run['resultsName']:'',
							'filesystempath'	=> 	isset($run['filesystempath']) ? $run['filesystempath']:'',
							'runid'		=> 	isset($run['runid']) ? $run['runid']:'',
							'status'		=> 	isset($run['status']) ? $run['status']:''
						);

						/////////////////////////////////////////////////////////////////
						// Find out what Bill is doing ofabio_api_metrics_v1.2.6a.py
						/////////////////////////////////////////////////////////////////
						
						// find all of the experiment info
						$url = 'http://'.$this->server['Torrent_Server']['ip'].$run['experiment'].$this->Torrent_server_login_creditials.'&timeStamp__month='.$month.'&timeStamp__year='.$year.'&order_by=-timeStamp';

						// $url = 'http://'.$this->server['Torrent_Server']['ip'].$run['experiment'].$this->Torrent_server_login_creditials;

						$experiment_info = $this->AccessRestAPI($url);

						// Might be all of the sample info
						// experiment_info['samples'] 
						// array (size=12)
							// 0 => string '/rundb/api/v1/sample/159/' (length=25)
							// 1 => string '/rundb/api/v1/sample/160/' (length=25)

						// Add chipBarcode to end 
						$curr_run['chipBarcode'] = isset($experiment_info['chipBarcode']) ? $experiment_info['chipBarcode']:'';

						// Add id from experiment.  This is the id which is added to the report name in the torrent server web application
						$curr_run['experiment_id'] = isset($experiment_info['id']) ? $experiment_info['id']:'';

						// Either 1 or 2 looks like chip A is 1 and Chip B is 2.  However, this possibly could be mixed when putting in iontorrent so might not be best
						$curr_run['chefSamplePos'] = isset($experiment_info['chefSamplePos']) ? $experiment_info['chefSamplePos']:'';
											
						array_push($keep_run_array, $curr_run);
					}
				}
			}
			else
			{
				return False;
			}
			
			return $keep_run_array;
		}

		public function TorrentServerRecentResults()
		{
			// Get a list of the current month results and the previous month.  Exclude _tn in reportLink and any entries without status 'Completed'.  I'm not sure what this is but it is not the correct data.  Include id, filesystempath, timeStamp, status in result.
			
			// Get current year and month results
			$curr_month = date('m');
			$curr_year = date('Y');
			$curr_month_results = $this->TorrentServerResults($curr_month, $curr_year);
			
			// tested for January and works.
			$now = new DateTime();
			$previousMonth = $now->modify('first day of previous month');
			$prev_month = $previousMonth->format('m');
			$prev_year = $previousMonth->format('Y');
			
			$prev_month_results = $this->TorrentServerResults($prev_month, $prev_year);

			// Return False so the user can be notified that something is wrong with 
			// the API
			if ($curr_month_results === False || $prev_month_results === False)
			{
				return False;
			}

			return array_merge($curr_month_results, $prev_month_results);			
		}

		public function AccessRestAPI($url)
		{
			// return json from a restAPI

			// suppress the warning by putting an error control operator (@) in from on of the file_get_contents()
			$response_json = @file_get_contents($url);

			// Exit if response is not present 
			if ($response_json === False)
			{
				return False;
			}

			return json_decode($response_json, true);
		}

		public function cutoffPassFail($qc_cutoffs)
		{
			// assess if all metrics passed cutoffs.  If all passed return true otherwise return false.

			foreach ($qc_cutoffs as $metric => $vals)
			{

				if ($vals['status'] !== 'pass')
				{
					return false;
				}
			}
			return true;
			
		}

		public function checkQC($val, $ranges)
		{
			// input array will have min and max
			// example $ranges (min=>30, max=>100, equal=>true)
			// example $ranges (max=>100)
			// example $ranges (min=>30)
			if (isset($ranges['max']) && isset($ranges['min']))
			{
				if (isset($ranges['equal']) && $ranges['equal'])
				{
					return ($val >= $ranges['min']) && ($val <= $ranges['max']);	
				}
				else
				{
					return ($val > $ranges['min']) && ($val < $ranges['max']);	
				}				
			}
			else if (isset($ranges['min']))
			{
				if (isset($ranges['equal']) && $ranges['equal'])
				{
					return $val >= $ranges['min'];
				}
				else
				{
					return $val > $ranges['min'];
				}
			}
			else if (isset($ranges['max']))
			{
				if (isset($ranges['equal']) && $ranges['equal'])
				{
					return $val <= $ranges['max'];
				}
				else
				{
					return $val < $ranges['max'];
				}
			}
		}

		public function readSepToJSON($bc_summary_f, $sep='\t')
		{
			// (str) -> JSON
			// Read a tab seperated file with a header and return a json of header by line

			$of = fopen($bc_summary_f, 'r') or die('unable to open file!');

			$header = rtrim(fgets($of));
		
			$split_header = explode($sep, $header);
			$out_json = array();

			while(!feof($of))
			{
				$line = rtrim(fgets($of));
				
				if (empty($line))
				{
					break;
				}
				$split_line = explode($sep, $line);

				$matched_array = $this->array_combine($split_header, $split_line);
				array_push($out_json, $matched_array);
			}

			fclose($of);
			return $out_json;
		}

		public function searchCivicGene($gene)
		{
			// search civic to see if a gene interpt is available.  Return gene interpt 

			$civic_url = 'https://civicdb.org/api/genes/'.urlencode($gene).'?identifier_type=entrez_symbol';
			$response_json = file_get_contents($civic_url);
			$response_array = json_decode($response_json, true);
			return isset($response_array['description'])? $response_array['description']:'';
		}

		public function searchVariantEffectPredictor($cosmic_id)
		{
			// CURRENLTY NOT BEING USED!!!!!!!!  Couldn't find useful info via the free api
			// (int) -> json
			// (COSM476) -> 

			// $vep_url = 'https://rest.ensembl.org/vep/human/id/'.urlencode($cosmic_id).'?content-type=application/json';
			$vep_url = 'https://rest.ensembl.org/vep/human/region/9:22125503-22125502:1/C?content-type=application/json';
			$response_json = file_get_contents($vep_url);
			$response_array = json_decode($response_json, true);

			// var_dump($response_array[0]['colocated_variants']);	
			var_dump($response_array[0]['transcript_consequences']);		
		}

		public function findProteinNameMutalyzer($transcript, $coding)
		{
			// https://mutalyzer.nl/json/runMutalyzer?variant=NM_000222.2:c.1698_1727delCAATTATGTTTACATAGACCCAACACAACTinsTAATTATGTTTACATAGACCCAACACAACC

			$mutalyzer_url = 'https://mutalyzer.nl/json/runMutalyzer?variant='.$transcript.':'.$coding;

			$response_json = file_get_contents($mutalyzer_url);
			$response_array = json_decode($response_json, true);


			return $response_array['proteinDescriptions'];

		}


	// response = requests.get()
	// response.raise_for_status()
	// print('###############################################\nRun mutalyzer on %s %s\n###############################################\n'%(transcript, coding))
	
	// json_response = response.json()

	
	// if response.status_code != 200:
	// 	return False
	
	// json_keys = json_response.keys()

	// if 'proteinDescriptions' in json_keys:

	// 	# proteinDescriptions example: [u'NM_000222.2(KIT_i001):p.(Leu576Pro)']
	// 	protein_descriptions = json_response['proteinDescriptions'][0]
	// 	spilt_descript = protein_descriptions.split(':')[-1].replace('(','').replace(')','')
	// 	print('mutalyzer says: %s Extracted to: %s'%(protein_descriptions, spilt_descript))

	// 	return spilt_descript
	// else:
	// 	return False


		public function searchCivicVariant($variant)
		{
			// CURRENLTY NOT BEING USED!!!!!!!!  
			// search civic to see if a variant interpt is available
			/////////////////////////////////////////
			//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
			// NOT COMPLETED !!!!!!!!!!!!!!!!!!!!!!!!!!!!!
			//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
			/////////////////////////////////////////
			$civic_url = 'https://civicdb.org/api/variants';
			$response_json = file_get_contents($civic_url);
			$response_array = json_decode($response_json, true);

			foreach ($response_array['records'] as $key => $civic_mut)
			{
				if ($civic_mut['entrez_name'] === $variant['Gene'] )
				{
					var_dump($civic_mut);
				}
			}
		}

		public function getServerLink($server_name)
		{
			// use this function to make a link for the ion reporter

			switch ($server_name) 
			{
				case 'Ion_Reporter':
					return '<a href="http://'.$this->server['Ion_Reporter']['ip'].'/ir/" target="_blank">'.$this->server['Ion_Reporter']['ip'].'/ir/</a>';
					break;
				
				case 'Torrent_Server':
					return '<a href="http://'.$this->server['Torrent_Server']['ip'].'/home/" target="_blank">'.$this->server['Torrent_Server']['ip'].'/home/</a>';
					break;
			}
		}

		public function make_directories($dir)
		{
	
			if (!is_dir($dir))
			{
				mkdir($dir, 0700);	
			}
		}

	}
	
?>