<?php
	class EmailUtils extends Utils
	{
		private $site_email_address = 'From: MD Infotrack no-reply <md_infotrack@urmc.rochester.edu>';

		public function EmailUtils($init_array)
		{
			$this->db = $init_array['db'];
		}

		public function addABugReportToForFailedEmail($body, $to, $email_admins=True)
		{
			// add suggestion/bug to notes table
			$add_array = array(
					'user_id' 	=>	USER_ID,
					'note'		=>	"There was a problem sending this email:\r\n\r\n".'"'.$body.'"'."\r\n\r\nTo:\r\n\r\n".$to
							);
			$this->db->addOrModifyRecord('notes_table', $add_array);

			// to avoid an infinite loop of of trying to email admins check if it is turned off
			if ($email_admins)
			{
				$this->emailAdmins($add_array['note'], 'URMC NGS Email Failing');
			}
		}

		public function sendEmail($to, $subject, $body, $email_info, $email_admins_of_failure=True)
		{
			// (str, str, str, array, bool) -> send email and track the sent email in sent_email_table if $email_info is not str equal to skip
			// example:
				// $email_info = array(
					// 'email_type' 		=> 	'reviewers_emailed',
					// 'recipient_user_ids' 	=>	array(1, 5, 8, 10, etc...),
					// 'sender_user_id'		=> 	1
				// )
			// send an html email

			// To send HTML mail, the Content-type header must be set
			$headers = 'MIME-Version: 1.0' . "\r\n";
			$headers.= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
			$headers.= $this->site_email_address;

			// Make sure my gmail account doesn't slip through
			$to = str_replace('taffners@gmail.com', 'samantha_taffner@urmc.rochester.edu', $to);

			$mail_result = mail($to, htmlspecialchars_decode($subject), nl2br($body), $headers);
			
			//////////////////////////////////////////////////////////////////////////////////////
			// Notify Admins that email was not sent successfully
			// Returns TRUE if the mail was successfully accepted for delivery, FALSE otherwise. It is important to note that just because the mail was accepted for delivery, it does NOT mean the mail will actually reach the intended destination.  
			// NOTE: Not sure what will actually return False.  Wrong email address and sending to gmail which does not accept these emails both returns True
			//////////////////////////////////////////////////////////////////////////////////////
			if (!$mail_result && $email_admins_of_failure)
			{
				$this->addABugReportToForFailedEmail($body, $to, True);
			}

			///////////////////////////////////////////////////////////////
			// Add all recipients of this email to sent_email_table 
			///////////////////////////////////////////////////////////////
			if 	(
					isset($email_info) && 
					$email_info != 'skip' && 
					isset($email_info['recipient_user_ids'])
				)
			{
				// make add array and just change the $recipent_user_id everytime.
				$toAdd = array();
				$toAdd['email_type'] = $email_info['email_type'];
				$toAdd['sender_user_id'] = $email_info['sender_user_id'];
				$toAdd['ref_id'] = $email_info['ref_id'];
				$toAdd['ref_table'] = $email_info['ref_table'];

				// Add each recipient as a new entry in sent_email_table
				foreach ($email_info['recipient_user_ids'] as $key => $recipient)
				{
					$toAdd['recipient_user_id'] = $recipient;
					$this->db->addOrModifyRecord('sent_email_table', $toAdd);	
				}
			}

			return $mail_result;
		}

		public function emailByUserID($user_id, $subject, $body, $email_info, $email_admins_of_failure=True)
		{
			$user_info = $this->db->listAll('user', $user_id);
			$to = $this->getEmailAddresses($user_info);
			
			$this->sendEmail($to, $subject, $body, $email_info, $email_admins_of_failure);
		}

		public function retrieveEmailTestInfo($test_data)
		{
			// Get all of the info about a test and include barcodes
			// test name: STMP
			// full test name: Oncomine Focus Hotspot Assay
			// mol#: 21-MOL77
			// MD#: 439-21
			// soft Lab#: K5278207
			// soft path#: 21-SCO28

			// Add Barcodes
			require 'vendor/autoload.php';

			// This will output the barcode as HTML output to display in the browser
			$generator = new Picqer\Barcode\BarcodeGeneratorPNG();

			$test_info = 'test name: '.$test_data['test_name']."\r\n";
			$test_info .= 'full test name: '.$test_data['full_test_name']."\r\n\r\n";
			
			if (isset($test_data['single_analyte_pool_run_number']) && !empty($test_data['single_analyte_pool_run_number']))
			{
				$test_info .= 'Single Analyte Pool Run ID: '.$test_data['single_analyte_pool_run_number']."\r\n\r\n";
			}

			if (isset($test_data['mol_num']) && !empty($test_data['mol_num']))
			{
				$test_info .= 'MD#: '.$test_data['mol_num'].'   <img src="data:image/png;base64,'.base64_encode($generator->getBarcode($test_data['mol_num'], $generator::TYPE_CODE_128)).'"> '."\r\n\r\n";
			}

			if (isset($test_data['order_num']) && !empty($test_data['order_num']))
			{
				$test_info .= 'mol#: '.$test_data['order_num'].'   <img src="data:image/png;base64,'.base64_encode($generator->getBarcode($test_data['order_num'], $generator::TYPE_CODE_128)).'"> '."\r\n\r\n";
			}

			if (isset($test_data['soft_lab_num']) && !empty($test_data['soft_lab_num']))
			{

				$test_info .= 'soft Lab#: '.$test_data['soft_lab_num'].'   <img src="data:image/png;base64,'.base64_encode($generator->getBarcode($test_data['soft_lab_num'], $generator::TYPE_CODE_128)).'"> '."\r\n\r\n";
			}

			if (isset($test_data['soft_path_num']) && !empty($test_data['soft_path_num']))
			{
				$test_info .= 'soft path#: '.$test_data['soft_path_num'].'   <img src="data:image/png;base64,'.base64_encode($generator->getBarcode($test_data['soft_path_num'], $generator::TYPE_CODE_128)).'"> '."\r\n\r\n";

				///////////////////////////
				// Convert format from the lab formated soft path number to the soft format
					// Adele says: 
						// Path likes the mask (she, ssp, etc), then the 2 digit year, then the case number as 6 digits with leading zeros to fill the spaces. For example: what we log as 20-SSP63569, Path wants to read SSP20063569
					// I investigated the number by using
						// SELECT DISTINCT soft_path_num, SUBSTRING(soft_path_num, 4, 3) AS letters FROM sample_log_book_table;
					// I replied back:
						// It doesn't seem like the number is consistent.  For instance below are some numbers.  The first column is the soft path number and the second column I tried to extract just the three letters from the number.  If I did do something like this I would have to provide the info both ways because MTP-01-21 and 21ME-01 would be completely messed up.  I would do a check to make sure the first two characters are numbers and the characters 4 - 6 are letters before providing the barcode. Also the number does not appear to be of consistent length​.  I put the info about lengths below.  I'm unsure if I could exclude the conversion based on length also at this time.
				//////////////////////////
				$converted_soft_path_num = $this->convert_soft_path_num_from_lab_to_soft($test_data['soft_path_num']);

				if ($converted_soft_path_num !== False)
				{
					$test_info .= 'soft path#: '.$converted_soft_path_num.'   <img src="data:image/png;base64,'.base64_encode($generator->getBarcode($converted_soft_path_num, $generator::TYPE_CODE_128)).'"> '."\r\n\r\n";
				}
			}	

			return $test_info;
		}

		public function bodyAssignAdminEmail($test_data, $tech_name)
		{
			return "Hello,\r\n\r\nA new report was assigned to you in ".SITE_TITLE.".\r\n\r\n".$this->retrieveEmailTestInfo($test_data)."\r\n\r\nPlease login to the ".SITE_TITLE." to obtain more information about the new report.\r\n\r\nIf you have any questions pertaining to this report please contact ".$tech_name[0]['first_name']." ".$tech_name[0]['last_name'].' ('.$tech_name[0]['email_address'].")\r\n\r\nThank you.\r\n\r\n".SITE_TITLE.' '.VERSION;
		}

		public function sendAssignAdminEmail($test_data, $tech_name, $admin_id)
		{
			$subject = 'New Report Ready '. $test_data['mol_num'];

			$body = $this->bodyAssignAdminEmail($test_data, $tech_name);

			$this->emailByUserID($admin_id, $subject, $body, 'skip', False);
		}


		public function getActiveUsersEmailAddress()
		{
			// Get all of the users with admin rights and send them an email.
			$all_admin_users = $this->db->listAll('get-all-active-users');
			return $this->getEmailAddresses($all_admin_users);
		}

		public function emailActiveUsers($subject, $body)
		{
			$to = $this->getAdminEmailAddress();
			return $this->sendEmail($to, $subject, $body, 'skip', False);
		}


		public function getAdminEmailAddress()
		{
			// Get all of the users with admin rights and send them an email.
			$all_admin_users = $this->db->listAll('get-all-users-with-a-permission', 'admin');
			return $this->getEmailAddresses($all_admin_users);
		}

		public function emailAdmins($body, $subject)
		{
			$to = $this->getAdminEmailAddress();
		

			// Do not notify admins since something is wrong with sending them an email but add to 
			// bug page
			return $this->sendEmail($to, $subject, $body, 'skip', False);
		}




		public function emailAdminsProblem($code_loc, $user)
		{
			// Put together the body of email and send to email to admins about the problem
			$subject = SITE_TITLE.' Bug!';
			$body = 'Hello,

This is an automated message from '.SITE_TITLE.'

';

			if (isset($user[0]) && !empty($user[0]))
			{
				$body.= $user[0]['first_name'].' '.$user[0]['last_name'].' encountered a problem in the following URL:

';
			}
			$body.=$_SERVER['REQUEST_URI'];

$body.='

Code location:


'.$code_loc;			

			return $this->emailAdmins($body, $subject);
		}

		public function emailAdminsDailyUpdate($message, $daily_status)
		{
			// Put together the body of email and send to email to admins about the problem
			$subject = $daily_status.' Daily Update!';
			$body = 'Hello,

This is an automated Daily Update from '.SITE_TITLE.'

';
			$body.= $message;
			
			$body.=$_SERVER['REQUEST_URI'];
		

			return $this->emailAdmins($body, $subject);
		}

		public function emailAdminsProblemURL($code_loc)
		{
			$user = $this->db->listAll('user', USER_ID);

			$adminNotificationStatus = $this->emailAdminsProblem($code_loc, $user);

			global $message;
			if ($adminNotificationStatus)
			{
				$message = 'Oops Something went wrong! '.SITE_TITLE.' admins have been notified of this issue.';
			}
			else
			{
				$message = 'Oops Something went wrong!';
			}
		}

		public function emailSoftpathsNewReport($post)
		{
			$user_info = $this->db->listAll('user', USER_ID);

			$subject = 'New '.$post['test_name'].' mol# '.$post['order_num'];
			$body = 	"Hello,\r\n\r\nA new visit was added to the ".SITE_TITLE.".\r\n\r\n".$this->retrieveEmailTestInfo($post)."\r\n\r\nPlease login to the ".SITE_TITLE." to obtain more information about the new visit.\r\n\r\nIf you have any questions pertaining to this visit please contact ".$user_info[0]['first_name']." ".$user_info[0]['last_name'].' ('.$user_info[0]['email_address'].")\r\n\r\nThank you for your help.\r\n\r\n".SITE_TITLE.' '.VERSION;
		
			$mail_result = $this->emailSoftpaths($body,$subject);

		}

		public function emailSoftpaths($body, $subject)
		{
			// email all users with softpath permissions

			// get all users with the softpath permission to email
			$all_softpath_users = $this->db->listAll('get-all-users-with-a-permission', 'softpath');
			$to = $this->getEmailAddresses($all_softpath_users);

			if ($to !== '')
			{
				return $this->sendEmail($to, $subject, $body, 'skip');
			}

		}

		public function getEmailAddresses($array_users)
		{
			// supply an array of email addresses and return a list of email addresses to send email to

			$to = '';
			foreach ($array_users as $key => $curr_user)
			{
				// Replace my gmail address with the urmc address.  I used gmail address 
				// because it is shorter but it's better to test the urmc email account
				// for emails.  Also gmail blocks these emails.  I'm not sure how to
				// get around gmail filter.
				if ($curr_user['email_address'] === 'taffners@gmail.com')
				{
					$curr_user['email_address'] = 'samantha_taffner@urmc.rochester.edu';
				}

				if ($to !== '')
				{
					$to.=',';
				}

				$curr_email = filter_var($curr_user['email_address'], FILTER_SANITIZE_EMAIL);
				$to.=$curr_email;
			}
			return $to;
		}

		public function getUserNames($array_users)
		{
			// supply an array of users with a user_name and return a string of users comma separated.

			$user_names = '';
			foreach ($array_users as $key => $curr_user)
			{				
				if ($user_names !== '')
				{
					$user_names.=', ';
				}

				$user_names.=$curr_user['user_name'] ;
			}
			return $user_names;
		}

		public function getUserIds($array_users)
		{
			// supply an linked array of user_ids for each user and return an simple array of user ids
			// example
			// Input
			// array(
			// 	0 => array('first_name' => 'Joe', 'last_name' => 'Smith', 'user_id' => 1),
			// 	2 => array('first_name' => 'Jane', 'last_name' => 'Wallace', 'user_id' => 3)
			// 	....
			// )
			// output
			// array('1','3')

			$user_ids = array();
			foreach ($array_users as $key => $curr_user)
			{
				if (is_numeric($curr_user['user_id']))
				{
					array_push($user_ids, intval($curr_user['user_id']));
				}
				else
				{
					array_push($user_ids, $curr_user['user_id']);
				}							
			}
			return $user_ids;
		}

		public function getSiteEmailAddress()
		{
			return $this->site_email_address;
		}


		public function expiringServiceContractsAutoEmail()
		{
			// send automatic emails to managers to notify them of expiring service 
			// contract on approximately 90, 60,30,20,10 days from expiration.  It is approx because it might fall on the weekend.  If so it will be sent on the next login.  

			// find all managers
			$managers_users = $this->db->listAll('get-all-users-with-a-permission', 'manager');

			// Find instruments with days until expiring <= 90 days on the contract.
			$instruments_with_expiring_contract = $this->db->listAll('all-instruments-with-service-contract-info');

			// Find which email should have been sent so far.  example 90_service_email
			// Make sure the max_expiry date equals expiration date.  I have been 
			// unable to figure out how to remove the non max expiry date service contracts
			foreach ($instruments_with_expiring_contract as $key => $contract)
			{		
				if ($contract['max_expiry'] !== $contract['expiration_date'])
				{
					continue;
				}

				$autom_email_type = $this->emailType($contract['days_till_expiry'], '_service_email');

				// find if current autom_email_type is one of the emails already sent
				// If the current num_day email has not been sent send it and add
				// to database.
				if (strpos($contract['email_types_sent'], $autom_email_type) === false)
				{
					$subject = 'Service Contract Expiring in '.$contract['days_till_expiry'].' days';
					$body = 'Hello,

The instrument '.$contract['task'].' '.$contract['manufacturer'].' yellow tag#: '.$contract['yellow_tag_num'].' Serial#: '.$contract['serial_num'].' has a service contract which will expire in '.$contract['days_till_expiry'].' days.


';
					$to = $this->getEmailAddresses($managers_users);

					if ($to !== '')
					{
						$this->sendEmail($to, $subject, $body, 'skip');

						// update automatic email table to include this email.
						$add_array = array(
							'ref_id'		=>	$contract['instrument_service_contract_id'],
							'permission_group_emailed'	=> 	'managers',
							'automatic_email_type'		=>	$autom_email_type,
							'date_sent'				=> 	date('Y-d-m'),
							'message'					=> 	$body
						);

						$add_result = $this->db->addOrModifyRecord('automatic_email_table', $add_array);
					}
				}
			}		
		}

		public function runAutoEmailAlerts()
		{
			// Send emails about service contracts expiring to everyone with manager permissions
			$this->expiringServiceContractsAutoEmail(); 
		}

		public function emailType($num_days, $email_type)
		{
			// Send an email reminder in 90,60,30,20,10,0 day increments 
			$num_days = intval($num_days);

			if ($num_days <= 0)
			{
				return '0'.$email_type;
			}
			else if 	(
						$num_days <= 90 && 
						$num_days > 60
					)
			{
				return '90'.$email_type;
			}

			else if 	(
						$num_days <= 60 && 
						$num_days > 30
					)
			{
				return '60'.$email_type;
			}

			else if 	(
						$num_days <= 30 && 
						$num_days > 20
					)
			{
				return '30'.$email_type;
			}
			else if 	(
						$num_days <= 20 && 
						$num_days > 10
					)
			{
				return '20'.$email_type;
			}
			else if 	(
						$num_days <= 10 && 
						$num_days > 0
					)
			{
				return '10'.$email_type;
			}
			else
			{
				return 'skip';
			}
		}

	}

?>