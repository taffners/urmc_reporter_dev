<?php

	class ThermoBackup extends APIUtils
	{
		// use this variable to save files so they can all be copied in the end and run md5
		private 	$files_to_backup = array();
		private		$chip;
		private		$result_name;
		private 	$page;
		private 	$bcmatrix;
		private 	$select_dump;
		private 	$Torrent_server_login_creditials;
		private 	$all_qc_data = array();
		private 	$qc_report = array('cutoffs' => array(), 'header' => array());

		private 	$md5s = array(array('Original_file', 'Original_md5', 'cp_file', 'cp_m5', 'match 1==True', 'file_type', 'sample_name'));
		private 	$coverage_dir;
		private 	$errors = array();

		public function __construct($result_name, $chip, $page, $library_pool_id, $pool_info)
		{
			$this->chip = $chip;

			$this->results_name = $result_name;
	
			$this->page = $page;

			$this->Torrent_server_login_creditials = '?username='.$this->server['Torrent_Server']['username'].'&password='.$this->server['Torrent_Server']['password'].'&api_key='.$this->server['Torrent_Server']['api_key'];
			$this->library_pool_id = $library_pool_id;	

			$this->setBackupLocs();

			$this->pool_info = $pool_info;
		}

		public function assessErrors()
		{
	
			// write an error file to out dir
			if (!empty($this->errors))
			{
				$out_dir = $this->qc_report['header']['backup_location'];
				$this->make_directories($out_dir);
				
				$err_f = fopen($out_dir.'/err.log','w');
				fwrite($err_f, json_encode($this->errors, JSON_PRETTY_PRINT));
				fclose($err_f);	
			}

			return $this->errors;
		}
		
		public function run()
		{
			$this->TorrentServerRunQualityResults();
			$this->QCReportHeader();
		
			$this->getTorrentJSONFiles();
			$this->MakeQCReportFile();
			
			// before trying to access the files in the coverage dir see if 
			// it is there and do not allow the access if it isn't there
			$this->MakeSureCoverageAPIRan();
		
			if (!isset($this->errors['Coverage_dir']))
			{
				// functions are dependent on Coverage Dir being present
				$this->getTorrentSampleQC();
				$this->getTorrentSampleCoverage();
				$this->cpFilesToBackup();
				$this->writeMd5File();
				$this->writeJSONQCs();
				$this->callPythonToGetAmpliconCov();
			}
		}

		public function backupIonReporterData()
		{
			// make sure barcode_by_sample_name is part of $this->qc_report
			if 	(	
					isset($this->qc_report['barcode_by_sample_name']) && 
					!empty($this->qc_report['barcode_by_sample_name']) && 
					isset($this->qc_report['header']['backup_location']) &&
					!empty($this->qc_report['header']['backup_location']) &&
					isset($this->server['Ion_Reporter']['api_server']) &&
					isset($this->server['Ion_Reporter']['api_key'])
				)
			{
				// I do a quick check to make sure IR completed if I can figure this out via API

				$sample_list = '';
				// read thru every sample and start backup of IR files (bam, vcf, variant tsv files)
				foreach ($this->qc_report['barcode_by_sample_name'] as $barcode => $sample_name)
				{
					if (!empty($sample_list))
					{
						$sample_list.=' ';
					}

					$sample_list.=$sample_name;
				}


				if (!empty($sample_list) )
				{
					$cmd = 'time python python/thermo_backup_ion_reporter_run.py -o '.$this->qc_report['header']['backup_location'].' -api_server '.$this->server['Ion_Reporter']['api_server'].' -api_token '.$this->server['Ion_Reporter']['api_key'].' -sample_list '.$sample_list.' -library_pool_id '.$this->library_pool_id.' 1> '.$this->qc_report['header']['backup_location'].'/ir_backup.log'.' 2> '.$this->qc_report['header']['backup_location'].'/ir_backup_err.log & echo $! >> '.$this->qc_report['header']['backup_location'].'/pid.log';

					// ' 2>&1 &';

					//////////////////////////////////////////////////////////
					// script is extremely slow when running it via php using system() however, 
					// it completes in 45 seconds submitting 
					// the python script in command line. I will try exec() instead.  8/25/2020 
					// exec() - Execute an external program (6 minutes 13 secs)
						// need to wait for it to run.  It might time out.
					// system() - Execute an external program and display the output
						// very very slow.
					// passthru() - Execute an external program and display raw output
						// need to wait for it to run.  It might time out.
					// shell_exec — Execute command via shell and return the complete output as a string
					// popen() - Run66 23 samples tested
					//////////////////////////////////////////////////////////
					// system($cmd);
					

					exec($cmd);
					// popen($cmd,'r');
				}				
			}

			else
			{
				$this->errors['IR Backup Not ERROR'] = 'There are missing variables or files necessary to start IR backup';
			}
		}

		public function MakeSureCoverageAPIRan()
		{
			$this->coverage_dir = $this->server['Torrent_Server']['mounted_loc'].$this->all_qc_data[0]['coverageAnalysis']['URL'];

			if (!file_exists($this->coverage_dir))
			{
				$this->errors['Coverage_dir'] = 'The coverage directory was not found '.$this->coverage_dir;	
			}
		}

		private function writeJSONQCs()
		{
			$qc_folder = $this->qc_report['header']['backup_location'].'/qc';
			$this->make_directories($qc_folder);
			
			$qc_dir_4_ident = $qc_folder.'/'.$this->qc_report['header']['run_five_identifiers'];
			// back up json
			$wf = fopen($qc_dir_4_ident.'Torrent_server_api_dump.json','w');
			fwrite($wf, json_encode($this->all_qc_data, JSON_PRETTY_PRINT));
			fclose($wf);

			$this->select_dump = $qc_dir_4_ident.'select_api_dump.json';
			$wf = fopen($qc_dir_4_ident.'select_api_dump.json','w');
			fwrite($wf, json_encode($this->qc_report, JSON_PRETTY_PRINT));
			fclose($wf);
		}

		private function writeMd5File()
		{
			$md5_folder = $this->qc_report['header']['backup_location'].'/md5s';
			$this->make_directories($md5_folder);

			$md_file = $md5_folder.'/'.$this->page.'_md5.tsv';

			// if sizeOf md5s is == 1 then only the header is there and do not write of a file that might already be there.
			if (sizeOf($this->md5s) > 1)
			{
				$wf = fopen($md_file, 'w') or die('unable to open file!');

				foreach ($this->md5s as $key => $md5_file_info)
				{
					fwrite($wf, $this->arrayToCharSeperated($md5_file_info, '\t'));
				}

				fclose($wf);
			}
		}

		private function cpFilesToBackup()
		{
			// copy files that were identified to backup via files_to_backup while using run function 
			foreach ($this->files_to_backup as $orginal_f => $cp_f)
			{
				if (!file_exists($cp_f))
				{
					copy($orginal_f, $cp_f);	

					$this->getMd5FileTransfer($orginal_f, $cp_f, 'run_qc');
				}
			}
		}

		private function getMd5FileTransfer($orginal_f, $cp_f, $file_type)
		{
			$original_md5 = md5_file($orginal_f);
			$cp_md5 = md5_file($cp_f);


			array_push($this->md5s, array($orginal_f, $original_md5, $cp_f, $cp_md5, $original_md5 == $cp_md5, $file_type));
		}

		public function getQCData()
		{
			return $this->qc_report;
		}

		public function TorrentServerRunQualityResults()
		{
			// Obtain data from multiple API calls pertaining to quality 

			$url = 'http://'.$this->server['Torrent_Server']['ip'].'/rundb/api/v1/results/'.$this->Torrent_server_login_creditials.'&resultsName='.$this->results_name;

			$response_array = $this->AccessRestAPI($url);

			$this->all_qc_data = array();

			foreach ($response_array['objects'] as $key => $run)
			{							
				// remove thumbnail results and status not complete
				// If run results are just a thumbnail it will have 
				// 'metaData' => 
					// array (size=1)
					//   'thumb' => int 1
				if (empty($run['metaData']) && $run['status'] === 'Completed')
				{
					$curr_run = array(
						'result_id' 		=>	isset($run['id']) ? $run['id'] :'',
						'time_stamp'		=> 	isset($run['timeStamp']) ? $run['timeStamp'] :'',
						'results_name'		=> 	isset($run['resultsName']) ? $run['resultsName']:'',
						'filesystempath'	=> 	isset($run['filesystempath']) ? $run['filesystempath']:'',
						'runid'				=> 	isset($run['runid']) ? $run['runid']:'',
						'status'			=> 	isset($run['status']) ? $run['status']:''
					);

					/////////////////////////////////////////////////////////////////
					// Find quality metrics 
					/////////////////////////////////////////////////////////////////
					$api_calls = array('analysismetrics', 'qualitymetrics', 'libmetrics', 'experiment');

					// Make calls for each type of data listed in api_calls
					foreach ($api_calls as $call)
					{			
						// some calls are nested and some are just strings.
						if (!isset($run[$call]))
						{						
							return array('error' => $call.' API Call Error');
						}

						else if (is_string($run[$call]))
						{
							$url = 'http://'.$this->server['Torrent_Server']['ip'].$run[$call].$this->Torrent_server_login_creditials.'&resultsName='.$this->results_name;

							$rest_response = $this->AccessRestAPI($url);

							if ($rest_response === False)
							{
								return array('error' => $call.' API Call Error');
							}
							else
							{
								$curr_run[$call] = $rest_response;
							}							
						}

						else
						{
							// These calls are all nested therefore pull all data.
							foreach ($run[$call] as $key => $arr)
							{
								$url = 'http://'.$this->server['Torrent_Server']['ip'].$run[$call][$key].$this->Torrent_server_login_creditials.'&resultsName='.$this->results_name;

								$rest_response = $this->AccessRestAPI($url);

								if ($rest_response === False)
								{								
									return array('error' => $call.' API Call Error');
								}
								else
								{
									$curr_run[$call][$key] = $rest_response;
								}
							}
						}
					}
							

					if (isset($run['pluginresults']))
					{
						// get coverage analysis 
						$curr_run['coverageAnalysis'] = $this->TorrentServerCoverageAnalysis($run['pluginresults']);						
					}
					else
					{
						return array('error' => 'Plug-ins are not working!!');
					}

					array_push($this->all_qc_data, $curr_run);						
				}
			}		

			// Add versions
			$url = 'http://'.$this->server['Torrent_Server']['ip'].'/rundb/api/v1/torrentsuite/'.$this->Torrent_server_login_creditials;

			$version_array = $this->AccessRestAPI($url);
		
			$this->all_qc_data['torrentsuite'] = $version_array;		
		}

		public function QCReportHeader()
		{
			/////////////////////////////////////////////////////////////
			// Add header data
				// array (size=7)
				//   'run_id' => string 'SAWBD' (length=5)
				//   'chip_barcode' => string 'DAEJ00646' (length=9)
				//   'experiment_id' => int 177
				//   'time_stamp' => string 'SAWBD' (length=5)
				//   'filesystempath' => string '/results/analysis/output/Home/Auto_user_S5XL-0108-110-Run_36_A_190828_177_020' (length=77)
				//   'results_name' => string 'Auto_user_S5XL-0108-110-Run_36_A_190828_177' (length=43)
				//   'backup_location' => string '/var/www/urmc_reporter_data/torrent_data/19691231_SAWBD_DAEJ00646_177' (length=69)			
			////////////////////////////////////////////////////////////
			$this->qc_report['header']['run_id'] = $this->all_qc_data[0]['runid'];
			$this->qc_report['header']['chip_barcode'] = $this->all_qc_data[0]['experiment']['chipBarcode'];
			$this->qc_report['header']['experiment_id'] = $this->all_qc_data[0]['experiment']['id'];
			$this->qc_report['header']['time_stamp'] = $this->all_qc_data[0]['time_stamp'];
			$this->qc_report['header']['filesystempath'] = $this->all_qc_data[0]['filesystempath'];
			$this->qc_report['header']['results_name'] = $this->all_qc_data[0]['results_name'];
			$this->qc_report['header']['library_pool_id'] = $this->library_pool_id;
			
			
			$five_identifiers = $this->fiveIdentifiersRun($this->all_qc_data);

			$this->qc_report['header']['run_five_identifiers'] = $five_identifiers;

			$backup_loc = $this->server['backup_locs']['ion_torrent_backup_dir'].'/'.$five_identifiers;
var_dump($backup_loc);
			$this->make_directories($backup_loc);
			$this->make_directories($backup_loc.'/qc');

			$this->qc_report['header']['backup_location'] = $backup_loc;
		}

		public function MakeQCReportFile()
		{
			// Extract a subset of fields from all_qc_data this array will be used to 
			// show user QC data and to add info to db.  	

			/////////////////////////////////////////////////////////////
			// Add lots to return array
			// Example:
			// array (size=25)
			//   'chefChipType1' => string '530v1' (length=5)
			//   'chefChipType2' => string '530v1' (length=5)
			//   'chefEndTime' => string '2019-08-29T13:02:15+00:00' (length=25)
			//   'chefInstrumentName' => string '242470541' (length=9)
			//   'chefKitType' => string 'Ion 510 & Ion 520 & Ion 530 Kit-Chef' (length=36)
			//   'chefLastUpdate' => string '2019-08-29T13:02:21+00:00' (length=25)
			//   'chefLogPath' => string '/results/chef_logs/242470541-000414_rc_2019-8-28_1613.tar.gz' (length=60)
			//   'chefOperationMode' => string 'Customer Mode' (length=13)
			//   'chefPackageVer' => string 'IC.5.12.0' (length=9)
			//   'chefProgress' => int 100
			//   'chefReagentsExpiration' => string '200531' (length=6)
			//   'chefReagentsLot' => string '2084063' (length=7)
			//   'chefReagentsPart' => string 'A34018C' (length=7)
			//   'chefSamplePos' => string '1' (length=1)
			//   'chefScriptVersion' => string '905' (length=3)
			//   'chefSolutionsExpiration' => string '200430' (length=6)
			//   'chefSolutionsLot' => string '2071790' (length=7)
			//   'chefSolutionsPart' => string 'A27754C' (length=7)
			//   'chefStartTime' => string '2019-08-28T20:13:01+00:00' (length=25)
			//   'chefStatus' => string 'Complete' (length=8)
			//   'chefTipRackBarcode' => string '4821601AE' (length=9)
			//   'chipBarcode' => string 'DAEJ00646' (length=9)
			//   'chipType' => string '530' (length=3)
			//   'sequencekitbarcode' => string '20088' (length=5)
			//   'sequencekitname' => string 'Ion S5 Sequencing Kit' (length=21
			/////////////////////////////////////////////////////////////
			$this->qc_report['lots'] = array();
			foreach ($this->all_qc_data[0]['experiment'] as $field => $val)
			{
				if 	(
					!empty($val) &&
					$val !== 'None' &&
					(
						$this->startsWith($field, 'chef') || 
						$this->startsWith($field, 'chip') || 
						$this->startsWith($field, 'sequencekit')
					) 
				)
				{
					$this->qc_report['lots'][$field] = $val;
				}
			}

			/////////////////////////////////////////////////////////////
			// Add versions to return array
				// array (size=18)
				//   'ion-analysis' => string '5.12.11-1' (length=9)
				//   'ion-chefupdates' => string '5.12.2' (length=6)
				//   'ion-dbreports' => string '5.12.35-1' (length=9)
				//   'ion-docs' => string '5.12.0' (length=6)
				//   'ion-gpu' => string '5.12.0-1' (length=8)
				//   'ion-onetouchupdater' => string '5.0.2-1' (length=7)
				//   'ion-pgmupdates' => string '5.10.0' (length=6)
				//   'ion-pipeline' => string '5.12.11-1' (length=9)
				//   'ion-plugins' => string '5.12.10-1' (length=9)
				//   'ion-protonupdates' => string '5.12.0' (length=6)
				//   'ion-publishers' => string '5.12.0-1' (length=8)
				//   'ion-referencelibrary' => string '2.2.0' (length=5)
				//   'ion-rsmts' => string '5.12.5-1' (length=8)
				//   'ion-s5updates' => string '5.12.0' (length=6)
				//   'ion-sampledata' => string '1.2.0-1' (length=7)
				//   'ion-torrentpy' => string '5.12.8-1' (length=8)
				//   'ion-torrentr' => string '5.12.10-1' (length=9)
				//   'ion-tsconfig' => string '5.12.7-1' (length=8)			
			/////////////////////////////////////////////////////////////
			$this->qc_report['versions'] = $this->all_qc_data['torrentsuite']['versions'];

			/////////////////////////////////////////////////////////////
			// Add all of the following metrics and cutoffs to $qc_report['cutoffs']
			// Example of output cutoff array
				// array (size=12)
				  // 'total_reads' => 
				  //   array (size=4)
				  //     'val' => float 18303818
				  //     'unit' => string '' (length=0)
				  //     'status' => string 'pass' (length=4)
				  //     'range' => string '10,000,000 - 20,000,000 ' (length=24)
				  // 'useable_reads' => 
				  //   array (size=4)
				  //     'val' => float 51.459
				  //     'unit' => string '%' (length=1)
				  //     'status' => string 'pass' (length=4)
				  //     'range' => string '> 30 %' (length=6)
			// cutoff_pass => true/false
			/////////////////////////////////////////////////////////////
			
			////////////////////////////////////////////////////////////
			// find total reads
			////////////////////////////////////////////////////////////
			if 	( 	
					isset($this->all_qc_data[0]['analysismetrics'][0]['libFinal']) &&
					!empty($this->all_qc_data[0]['analysismetrics'][0]['libFinal'])

				)
			{
				$total_reads = $this->all_qc_data[0]['analysismetrics'][0]['libFinal'];

				$this->qc_report['cutoffs']['total_reads'] = $this->fieldStatusQC($total_reads, array('min'=> 10000000, 'equal' => true, 'unit' => ''));
			}			

			////////////////////////////////////////////////////////////
			// find useable reads
			////////////////////////////////////////////////////////////
			if 	(
					isset($this->all_qc_data[0]['analysismetrics'][0]['libFinal']) &&
					!empty($this->all_qc_data[0]['analysismetrics'][0]['libFinal']) &&
					isset($this->all_qc_data[0]['analysismetrics'][0]['lib']) &&
					!empty($this->all_qc_data[0]['analysismetrics'][0]['lib'])
				)
			{

				$useable_reads = ($this->all_qc_data[0]['analysismetrics'][0]['libFinal'] * 100) / $this->all_qc_data[0]['analysismetrics'][0]['lib'];

				$this->qc_report['cutoffs']['useable_reads'] = $this->fieldStatusQC($useable_reads, array('min'=> 30, 'unit' => '%'));				
			}			

			////////////////////////////////////////////////////////////
			// bead loading == ISP loading
			///////////////////////////////////////////////////////////
			if 	(
					isset($this->all_qc_data[0]['analysismetrics'][0]['loading']) &&
					!empty($this->all_qc_data[0]['analysismetrics'][0]['loading']) 
				)
			{

				$loading = $this->all_qc_data[0]['analysismetrics'][0]['loading'];
				$this->qc_report['cutoffs']['loading'] = $this->fieldStatusQC($loading, array('min'=> 60, 'unit' => '%'));
			}

			////////////////////////////////////////////////////////////
			// Enrichment
			////////////////////////////////////////////////////////////
			if 	(
					isset($this->all_qc_data[0]['analysismetrics'][0]['live']) &&
					!empty($this->all_qc_data[0]['analysismetrics'][0]['live']) &&
					isset($this->all_qc_data[0]['analysismetrics'][0]['bead']) &&
					!empty($this->all_qc_data[0]['analysismetrics'][0]['bead'])
				)
			{
				$enrichment_ISP = $this->all_qc_data[0]['analysismetrics'][0]['live'];
				$enrichment =  ( $enrichment_ISP / $this->all_qc_data[0]['analysismetrics'][0]['bead']) * 100.0;
				
				$qc_report['cutoffs']['enrichment'] = $this->fieldStatusQC($enrichment, array('min'=> 98, 'unit' => '%'));
			}			

			////////////////////////////////////////////////////////////
			// polyclonal%, clonal%, final library %
			////////////////////////////////////////////////////////////
			if 	(
					isset($this->all_qc_data[0]['file_jsons']['Filtering']['LibraryReport']) &&
					!empty($this->all_qc_data[0]['file_jsons']['Filtering']['LibraryReport']) 
				)
			{
				// Find all of the reasons why reads were removed so we can calculate the 
				// how many reads we are left with for the final library
				$polyclonal_ISP =  $this->all_qc_data[0]['file_jsons']['Filtering']['LibraryReport']['filtered_polyclonal'];

				
				$dimer_ISP = $this->all_qc_data[0]['file_jsons']['Filtering']['LibraryReport']['filtered_primer_dimer'];

				$low_quality_ISP = $this->all_qc_data[0]['file_jsons']['Filtering']['LibraryReport']['filtered_low_quality'];

				$failed_reads = $polyclonal_ISP + $dimer_ISP + $low_quality_ISP;

				$final_library_ISP = $this->all_qc_data[0]['file_jsons']['Filtering']['LibraryReport']['final_library_reads'];

				$total_starting_reads = $failed_reads + $final_library_ISP;

				// Calculate the Percents of each read type
				$clonal = (($total_starting_reads - $polyclonal_ISP) / $total_starting_reads) * 100;
				$polyclonal = ($polyclonal_ISP / $total_starting_reads) * 100;
				$low_quality = ($low_quality_ISP / $total_starting_reads) * 100;
				$adapter_dimer = ($dimer_ISP / $total_starting_reads) * 100;
				$final_library = ($final_library_ISP / $total_starting_reads) * 100;

				// Add of the metrics to the final json
				$this->qc_report['cutoffs']['clonal'] = $this->fieldStatusQC($clonal, array('min'=> 50, 'unit' => '%'));

				$this->qc_report['cutoffs']['polyclonal'] = $this->fieldStatusQC($polyclonal, array('max'=> 45, 'unit' => '%'));

				$this->qc_report['cutoffs']['adapter_dimer'] = $this->fieldStatusQC($adapter_dimer, array('max'=> 10, 'unit' => '%'));

				$this->qc_report['cutoffs']['low_quality'] = $this->fieldStatusQC($low_quality, array('max'=> 30, 'unit' => '%'));

				$this->qc_report['cutoffs']['final_library'] = $this->fieldStatusQC($final_library, array('min'=> 40, 'unit' => '%'));
			}	

			////////////////////////////////////////////////////////////
			// test fragment
			////////////////////////////////////////////////////////////
			if 	(
					isset($this->all_qc_data[0]['analysismetrics'][0]['tf']) &&
					!empty($this->all_qc_data[0]['analysismetrics'][0]['tf']) &&
					isset($this->all_qc_data[0]['analysismetrics'][0]['live']) &&
					!empty($this->all_qc_data[0]['analysismetrics'][0]['live'])
				)
			{
				$test_fragment =  ($this->all_qc_data[0]['analysismetrics'][0]['tf'] / $this->all_qc_data[0]['analysismetrics'][0]['live']) * 100.0;
				
				$this->qc_report['cutoffs']['test_fragment'] = $this->fieldStatusQC($test_fragment, array('max'=> 3, 'unit' => '%', 'equal' => true));
			}
			
			////////////////////////////////////////////////////////////
			// median_read_len
			////////////////////////////////////////////////////////////
			if 	(
					isset($this->all_qc_data[0]['qualitymetrics'][0]['q0_median_read_length']) &&
					!empty($this->all_qc_data[0]['qualitymetrics'][0]['q0_median_read_length'])
				)
			{
				$median_read_len =  $this->all_qc_data[0]['qualitymetrics'][0]['q0_median_read_length'];
				
				$this->qc_report['cutoffs']['median_read_len'] = $this->fieldStatusQC($median_read_len, array('max'=> 130, 'min' => 100, 'unit' => 'bp', 'equal' => true));
			}
			
			////////////////////////////////////////////////////////////
			// median_read_len
			////////////////////////////////////////////////////////////
			if 	(
					isset($this->all_qc_data[0]['libmetrics'][0]['raw_accuracy']) &&
					!empty($this->all_qc_data[0]['libmetrics'][0]['raw_accuracy'])
				)
			{
				$mean_raw_accuracy =  $this->all_qc_data[0]['libmetrics'][0]['raw_accuracy'];
				
				$this->qc_report['cutoffs']['mean_raw_accuracy'] = $this->fieldStatusQC($mean_raw_accuracy, array('min' => 99, 'unit' => '%', 'equal' => true));
			}

			$this->qc_report['cutoff_pass'] = $this->cutoffPassFail($this->qc_report['cutoffs']);

			return $qc_report;
		}

		public function fieldStatusQC($val, $ranges)
		{
			// $API_Utils->fieldStatusQC(19000000, array('min'=> 10000000, 'max'=> 20000000, 'equal' => true, 'unit' => '')) => 10000000 - 20000000
			// $API_Utils->fieldStatusQC(40, array('min'=> 30, 'unit' => '%')) => > 30 %
			// $API_Utils->fieldStatusQC(2, array('max'=> 3, 'equal' => true, 'unit' => '%')) => '&lte; 3 %'

			// find if the value falls within the ranges
			$status = $this->checkQC($val, $ranges);

			$signs = array('lt' => '<', 'gt' => '>');

			// signs need to change depending on if (>= <=) or (> <)
			if (isset($ranges['equal']))
			{
				$signs = array('lt' => '&#8804;', 'gt' => '&#8805;');
			}

			///////////////////////////////////////////////////////////////
			// Make string range
			///////////////////////////////////////////////////////////////
			$range = '';
			// example 10000000 - 20000000
			if (isset($ranges['min']) && isset($ranges['max']))
			{
				$range = number_format($ranges['min']).' - '.number_format($ranges['max']).' '.$ranges['unit'];
			}

			// example > 30 %
			else if (isset($ranges['min']) && !isset($ranges['max']))
			{
				$range = $signs['gt'].' '.number_format($ranges['min']).' '.$ranges['unit'];
			}

			// example '&lte; 3 %'
			else if (!isset($ranges['min']) && isset($ranges['max']))
			{
				$range = $signs['lt'].' '.$ranges['max'].' '.$ranges['unit'];
			}

			// return an array for qc for a value
			if ($status)
			{
				return array(
					'val'	=> 	round($val, 3),
					'unit'	=> 	$ranges['unit'],
					'status'	=> 	'pass',
					'range'	=> 	$range
				);
			}
			else
			{
				return array(
					'val'	=> 	round($val, 3),
					'unit'	=> 	$ranges['unit'],
					'status'	=> 	'fail',
					'range'	=> 	$range
				);
			}			
		}

		public function fiveIdentifiersRun($chip_qc)
		{
			// Format: rundate_runid_chipBarcode_experimentid_(library_pool_id)
			// All Fields are obtained via the thermofisher API using the result name chosen the pool_select_results page
			// library_pool_id is added to make the folder unique for this pool just encase data is run again with a different pool.

			$run = 'Run'.$this->pool_info['excel_workbook_num_id'];
			$runid = $chip_qc[0]['runid'];
			$chipBarcode = $chip_qc[0]['experiment']['chipBarcode'];	
			$experiment_id = $chip_qc[0]['experiment']['id'];
			// $time_stamp = $chip_qc[0]['time_stamp'];
			$ngs_run_date = $this->pool_info['ngs_run_date'];

			// 4 Identifier backup_location.  Add pool id from infotrack also
			$run_date = date('Ymj', strtotime($ngs_run_date));
			
			return $run.'_'.$this->library_pool_id.'_'.$run_date.'_'.$this->chip.'_'.$runid.'_'.$chipBarcode.'_'.$experiment_id;
		}

		public function getTorrentJSONFiles()
		{
			$report_dir = $this->server['Torrent_Server']['mounted_loc'].$this->all_qc_data[0]['coverageAnalysis']['reportLink'].'basecaller_results';

			$basecaller_f = $report_dir.'/BaseCaller.json';

			// check if original basecaller.json exists
			if (file_exists($basecaller_f))
			{
				$this->files_to_backup[$basecaller_f] = $this->qc_report['header']['backup_location'].'/qc/'.$this->qc_report['header']['run_five_identifiers'].'BaseCaller.json';

				$this->all_qc_data[0]['file_jsons'] = json_decode($this->returnFileContents($basecaller_f), true);	
			}
			else
			{
				$this->errors['BaseCaller.json'] = 'a BaseCaller.json file has not been found in the '.$report_dir.' directory. Something went wrong in the thermo server.  Refer to the thermo help menu.';
			}
		}

		public function getTorrentSampleQC()
		{	
			// Inside the Torrent server results folder is plugin_out/coverageAnalysis_out.#number
			// Inside this folder are two coverage analysis files.  .bc_summary.xls and 
			// .bcmatrix.xls  These files need to be found since the the name of the file
			// is not predictable.  
				// Example: 
					// 190822_B_Run35_torrent-server_19.bc_summary.xls
					// 190822_B_Run35_torrent-server_19.bcmatrix.xls

			$cov_files = array();

			///////////////////////////////////////////////////////////////////
			// The .bc_summary.xls file contains sample level qc stats compare to standard cutoffs.
			// Example file
				// Barcode ID	Sample Name	Mapped Reads	On Target	Mean Depth	Uniformity
				// IonXpress_014	984.19	1287755	98.80%	4666	99.60% 
			// Example JSON
				// array (size=14)
				//   0 => 
				//     array (size=6)
				//       'Barcode ID' => string 'IonXpress_014' (length=13)
				//       'Sample Name' => string '984.19' (length=6)
				//       'Mapped Reads' => string '1287755' (length=7)
				//       'On Target' => string '98.80%' (length=6)
				//       'Mean Depth' => string '4666' (length=4)
				//       'Uniformity' => string '99.60%' (length=6)
			///////////////////////////////////////////////////////////////////
			$bc_summary = $this->get_files_with_extension($this->coverage_dir, 'bc_summary.xls');

			if (!empty($bc_summary))	
			{	
				// Back up original file
				$save_loc = $this->qc_report['header']['backup_location'].'/qc/'.$this->qc_report['header']['run_five_identifiers'];
				$this->files_to_backup[$bc_summary[0]] = $save_loc.'bc_summary.tsv';

				// Make a JSON for making a table in HTML
				$bc_summary_json = $this->BCSummaryToJSON($bc_summary[0], '	');
			}
			else
			{
				$this->errors['bc_summary.xls'] = 'a bc_summary.xls file has not been found in the '.$this->coverage_dir.' directory. Make sure the coverage Plugin ran.';
			}
		}

		public function BCSummaryToJSON($bc_summary_f, $sep='\t')
		{
			// (str) -> JSON
			// Read a tab seperated file with a header and return a json of header by line

			$of = fopen($bc_summary_f, 'r') or die('unable to open file!');

			$header = rtrim(fgets($of));
		
			$split_header = explode($sep, $header);
			$out_json = array();

			while(!feof($of))
			{
				$line = rtrim(fgets($of));
				
				if (empty($line))
				{
					break;
				}
				$split_line = explode($sep, $line);

				$matched_array = $this->array_combine($split_header, $split_line);

				$this->qc_report['barcode_by_sample_name'][$matched_array['Barcode ID']] = $matched_array['Sample Name'];

				array_push($out_json, $matched_array);
			}

			$this->qc_report['sample_qc'] = $out_json;

			fclose($of);
		}

		public function callPythonToGetAmpliconCov()
		{
			// Call the python script thermo_make_cov_files to run 
			$out_dir = $this->qc_report['header']['backup_location'].'/coverage';

			$cmd = 'python python/thermo_make_cov_files.py -select_dump '.$this->select_dump.' -bcmatrix '.$this->bcmatrix.' -o '.$out_dir.' > '.$out_dir.'/coverage.log'.' 2>&1 &'; 
	
			$this->make_directories($out_dir);
			system($cmd);
		}

		public function getTorrentSampleCoverage()
		{
			/////////////////////////////////////////////////////////////////////////
			// bcmatrix file provides coverage on an amplicon level for each sample.
			// Example File:
				// Gene	Target	IonXpress_020	IonXpress_021	IonXpress_022	IonXpress_023	IonXpress_024	IonXpress_025	IonXpress_026	IonXpress_027	IonXpress_028	IonXpress_029	IonXpress_032
			// Example JSON:
				// array(size=269)
				// array (size=15)
				//   'Gene' => string 'MTOR' (length=4)
				//   'Target' => string 'Oncomine_Focus_MTOR_1' (length=21)
				//   'IonXpress_014' => string '6734' (length=4)
				//   'IonXpress_015' => string '5385' (length=4)
				//   'IonXpress_016' => string '6101' (length=4)
				//   'IonXpress_017' => string '5123' (length=4)
				//   'IonXpress_018' => string '3867' (length=4)
				//   'IonXpress_019' => string '3685' (length=4)
				//   'IonXpress_020' => string '9543' (length=4)
				//   'IonXpress_021' => string '4514' (length=4)
				//   'IonXpress_022' => string '4621' (length=4)
				//   'IonXpress_023' => string '4988' (length=4)
				//   'IonXpress_024' => string '4824' (length=4)
				//   'IonXpress_030' => string '5899' (length=4)
				//   'IonXpress_031' => string '5572' (length=4)
			// MTOR	Oncomine_Focus_MTOR_1	6534	6312	3883	5131	6132	7240	6383	6026	7198	7446	7958
			// UNFORTUNELTY THIS FILE DOES NOT HAVE SAMPLE NAMES IT ONLY HAS BARCODE!!!
			/////////////////////////////////////////////////////////////////////////
			$bc_matrix = $this->get_files_with_extension($this->coverage_dir, 'bcmatrix.xls');

			if (!empty($bc_matrix))	
			{
				// Back up original file
				$save_loc = $this->qc_report['header']['backup_location'].'/qc/'.$this->qc_report['header']['run_five_identifiers'];
				$this->files_to_backup[$bc_matrix[0]] = $save_loc.'bcmatrix.tsv';

				$this->bcmatrix = $save_loc.'bcmatrix.tsv';
			}
			else
			{
				$this->errors['bcmatrix.xls'] = 'a bcmatrix.xls file has not been found in the '.$this->coverage_dir.' directory. Make sure the coverage Plugin ran.';
			}
		}

		public function TorrentServerCoverageAnalysis($installed_plugins)
		{
			// Find which installed plugin is the coverage analysis plugin and return the array
			// (array) -> array
			// Example $installed_plugins
				//  array (size=3)
					// 0 => string '/rundb/api/v1/pluginresult/59/' (length=30)
					// 1 => string '/rundb/api/v1/pluginresult/58/' (length=30)
					// 2 => string '/rundb/api/v1/pluginresult/57/' (length=30)

			foreach ($installed_plugins as $key => $plugin)
			{
				$url = 'http://'.$this->server['Torrent_Server']['ip'].$plugin.$this->Torrent_server_login_creditials;

				$rest_response = $this->AccessRestAPI($url);

				if (!isset($rest_response['pluginName'])) 
				{
					return array('error' => 'coverageAnalysis Plug-ins are not working!!');
				}

				else if ($rest_response['pluginName'] === 'coverageAnalysis')
				{
					return $rest_response;
				}								
			}

			return array('error' => 'coverageAnalysis Plug-ins was not found');
		}
	}
	
?>