<?php

	class ThermoCheckBackup extends APIUtils
	{

		public function __construct($library_pools, $epoch_start_time)
		{
			$this->library_pools = $library_pools;
			$this->epoch_start_time = $epoch_start_time;

			$this->time_elapsed = time() - $this->epoch_start_time;
				
			$this->errors = array();

			$this->backup_done = $this->init();	

			$this->curr_library_pool_id = 0;

			$this->curr_num_samples_in_pool = 0;		
		}

		private function init()
		{
			//////////////////////////////////////////////////////////////
			// find md5s folder
			// md5s folder should have 3 files in it 
				// ir_backup.tsv
				// ir_backup_tracker.tsv
				// pool_qc_backup_data_md5.tsv (check first this should be done already)
			//////////////////////////////////////////////////////////////
			foreach ($this->library_pools as $key => $pool)
			{

				// check to make sure the second chip is not empty.  This happens when there's only one chip
				if (empty($pool['library_pool_id']))
				{
					break;
				}

				// do not proceed if samples failed run qc
				elseif ($pool['run_qc_status'] === 'failed')
				{
					$this->errors[$pool['library_pool_id']]['failed_qc'] = 'Sample failed qc';
					break;
				}

				$this->curr_library_pool_id = $pool['library_pool_id'];

				$this->curr_num_samples_in_pool = $pool['num_samples_in_pool'];
				
				// check that md5s folder exists add to library_pool if it exists
				if (!$this->is_file_present($pool['backup_location'].'/md5s'))
				{
					return False;
				}
				else
				{
					$this->library_pools[$key]['md5_dir'] = $pool['backup_location'].'/md5s';
				}

				//////////////////////////////////////////////////////////////			
				// pool_qc_backup_data_md5.tsv
					//  3 files are copied in this check.  Make sure they all copied correctly 
				//////////////////////////////////////////////////////////////
				if (!$this->is_file_present($this->library_pools[$key]['md5_dir'].'/pool_qc_backup_data_md5.tsv'))
				{
					return False;
				}
				else
				{
					$this->check_data_transfer($this->library_pools[$key]['md5_dir'].'/pool_qc_backup_data_md5.tsv', 'pool_qc_backup_data_md5', $pool['library_pool_id']);
				}

				//////////////////////////////////////////////////////////////			
				// ir_backup.tsv
					//  Just make sure it is present and save loc
				//////////////////////////////////////////////////////////////	
				if (!$this->is_file_present($this->library_pools[$key]['md5_dir'].'/ir_backup.tsv'))
				{
					return False;
				}

				//////////////////////////////////////////////////////////////			
				// ir_backup_tracker.tsv
					//  Make sure all entries passed.  It will add more than one entry for every attempt to copy the files however,
					// 	there will be no way to know if its in the middle of backing up.
				//////////////////////////////////////////////////////////////
				if (!$this->is_file_present($this->library_pools[$key]['md5_dir'].'/ir_backup_tracker.tsv'))
				{
					return False;
				}
				else
				{
					$this->check_data_transfer($this->library_pools[$key]['md5_dir'].'/ir_backup_tracker.tsv', 'ir_backup_tracker', $pool['library_pool_id']);
				}
			}
		}

		public function is_file_present($check_file)
		{
			// check to see if a file exists.  If it doesn't exist then check to see if it has been at least 2 minutes.  If it
			// hasn't been 2 minutes provide a warning.  Otherwise all of these files checked under this function should be
			// created within 2 minutes So provide a fatal error.  

			if (!file_exists($check_file))
			{
				// $this->errors[$pool['library_pool_id']]['pool_qc_backup_data_md5.tsv'] = 'The file '.$this->library_pools[$key]['md5_dir'].'/pool_qc_backup_data_md5.tsv is missing';

				if ($this->time_elapsed > 120)
				{
					$this->errors[$this->curr_library_pool_id]['fatal_error'][$check_file] = $check_file.' missing';
				}
				else
				{
					$this->errors[$this->curr_library_pool_id]['warning'][$check_file] = $check_file.' missing';
				}

				return False;
			}
			else
			{
				return True;
			}
		}

		public function check_data_transfer($f, $file_type, $library_pool_id)
		{
			$of = fopen($f, 'r') or die('unable to open file!');

			$line = rtrim(fgets($of));
			$header = explode("\t", $line);
			$line = rtrim(fgets($of));

			switch ($file_type) 
			{
				case 'pool_qc_backup_data_md5':
					$match_col = 'match 1==True';
					$match_val = '1';
					$num_rows = 3;
					break;

				case 'ir_backup_tracker':
					$match_col = 'status';
					$match_val = 'TRUE';
					$num_rows = 1;
					break;
			}

			$count = 0; 

			// Read the file that was supplied and make sure it matches the file_type added.
			while(!feof($of))
			{
				$count++;
				$split_line = explode("\t", $line);
				$file_transfer_info = $this->array_combine($header, $split_line);

				// Make sure the column that supplies if the file was transfered properly is present
				if (!in_array($match_col, $header))
				{
					$this->errors[$library_pool_id]['fatal_error'] = 'Error found with file format for the file '.$file_type.'.<br><br>'.json_encode($file_transfer_info);
					return False;
				}

				else if 	(
								isset($file_transfer_info[$match_col]) && 
								(
									$file_transfer_info[$match_col] != $match_val &&  
									strtoupper($file_transfer_info[$match_col]) != strtoupper($match_val) // added this because PHP is changing TRUE to True
								)
							)
				{

					$this->errors[$library_pool_id]['fatal_error'] = 'Error found while copying files.  md5s do not match<br><br>'.json_encode($file_transfer_info);
					return False;
				}
				
				$line = rtrim(fgets($of));			
			}	

			fclose($of);

			// If there are not enough lines in the file then either the transfer is not done yet or an error occurred.  If the file type is pool_qc_backup_data_md5 then it is an error since this file should have been done before the ir_backup transfer started.
		
			if ($file_type === 'ir_backup_tracker' && isset($num_rows) && $count < $num_rows)
			{
				// check the ir_backup.tsv file to see if any files were copied in the last 2 mins.				
				$ir_backup_f = str_replace('ir_backup_tracker.tsv', 'ir_backup.tsv', $f);

				$this->checkIRBackupContinuing($ir_backup_f);
				return False;												
			}
			else if ($file_type !== 'ir_backup_tracker'  && isset($num_rows) && $count != $num_rows)
			{			
				$this->errors[$library_pool_id]['fatal_error'] = 'Not enough files transfered in the '.$file_type.' backup';
				return False;							
			}
			else
			{
				return True;
			}			
		}

		private function checkIRBackupContinuing($ir_backup_f)
		{
			// Check to see that the ir_backup.tsv has had a file reported as copied within 12 minutes.  In a test case the maximum amount of time between copies was 49 seconds.  So 12 minutes should be enough time to ensure it isn't still running.  Unfortunately I can't just check the stderr file because there's a permission warning being produced while unzipping file.

			// Read file and get the last line and see if the it has been more than 12 mins since transfer occurred

			if ($this->is_file_present($ir_backup_f))
			{
				$last_line_array = $this->return_last_line_file($ir_backup_f);

				if 	(
						isset($last_line_array['last_line']['time'])  && 
						is_numeric($last_line_array['last_line']['time']) &&
						
						isset($last_line_array['line_count'])  && 
						is_numeric($last_line_array['line_count'])
					)
				{
					$last_line = $last_line_array['last_line'];
					$last_line['line_count'] = $last_line_array['line_count'];
					$time_elapsed = time() - intval($last_line['time']);
				
					// two minutes was not long enough.  Increasing to 6 minutes on 3/3/2020. Increasing to 12 minutes on 4/22/2020
					if ($time_elapsed > 720)
					{
						$this->errors[$this->curr_library_pool_id]['fatal_error'] = $ir_backup_f.' Something is causing a lag in transfer. Please refer to error logs. '.json_encode($last_line);
						return False;
					}	
					else
					{
						
						$last_line['num_samples_in_pool'] = intval($this->curr_num_samples_in_pool);

						// six file transfers per sample.
						$expect_transfer_num = 14 * $last_line['num_samples_in_pool'];
						$num_done = $last_line['line_count'];
						$num_left = $expect_transfer_num - $num_done;

						$percent_done = round(($num_done / $expect_transfer_num) * 100);
						
						$last_line['percent_done'] = $percent_done;

						$this->errors[$this->curr_library_pool_id]['still_processing'] = $last_line;
						return True;
					}		
				}
				else
				{
					$this->errors[$this->curr_library_pool_id]['fatal_error'][$ir_backup_f] = $ir_backup_f.' time not found in last line ';
					return False;
				}
			}
			else
			{
				return False;
			}
		}

		public function getErrors()
		{
			return $this->errors;
		}
	}
	
?>