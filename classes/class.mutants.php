<?php

	class MutantTracker
	{
		private $diagnosis; 
		private $NGS_panel;

		private $reportType; // multiple tables, one table

		private $required_report_genes = array(

				'CLL'	=>	array('SF3B1', 'NOTCH1', 'TP53')
			);

		private $extraGenesRequired;

		private $mutant_tables = array();

		private $unknown_sig_genes = array();

		private $sig_genes = array();

		private $benign_genes = array();

		public function GetInterptOrder()
		{
			// Use this function to either make a default order of the paragraphs 
			// for the interpt or return the current order.
	
		}


		public function MutantTracker($diagnosis, $NGS_panel, $variantArray,$visit_received_date)
		{
			// initialize function
			$this->diagnosis = $diagnosis;
			$this->NGS_panel = $NGS_panel;
			$this->variantArray = $variantArray;
			$this->visit_received_date = $visit_received_date;
				
			// On July 16th 2020 Tier 4 variants were removed from the Variants of unknown significance section and placed into a BENIGN VARIANTS section
			if (strtotime($this->visit_received_date) > strtotime('2020-07-16'))
			{
				$this->addMutantsToTableWithBenign(); // old function
			}
			else
			{
				$this->addMutantsToTable(); // new function used 
			}
			
		}

		private function addSelectedMutantsToTable($snvs, $select_genes)
		{
			$snvs_genes = array();
			$table_array = array();

			// add select genes which have mutation
			foreach ($snvs as $v)
			{			
				array_push($snvs_genes, $v['genes']);

				if (in_array($v['genes'], $select_genes) && ($v['tier'] !== 'Tier III' && $v['tier'] !== 'Tier IV'))
				{
					$add_array = array(
						'Gene' 		=> 	isset($v['genes']) ? $v['genes'] : '',
						'DNA'		=>	isset($v['coding']) ? $v['coding'] : '',
						'Protein' 	=> 	isset($v['amino_acid_change']) ? $v['amino_acid_change'] : '',
						'VAF'		=> 	isset($v['frequency']) ? round($v['frequency']).'%' : '',
						'COSMIC' 		=> 	isset($v['cosmic']) ? $v['cosmic'] : '',
						'Tier'		=> 	isset($v['tier']) ? $v['tier'] : '',
						'observed_variant_id' => isset($v['observed_variant_id']) ? $v['observed_variant_id'] : '',
						'knowledge_id'	=> 	isset($v['knowledge_id']) ? $v['knowledge_id'] : ''
					);

					array_push($table_array, $add_array);
				}
			}

			// iterate over snvs and include 
			foreach ($select_genes as $gene)
			{
				// if data is in snvs for gene in select list add data to array otherwise add N/A column
				if (!in_array($gene, $snvs_genes))
				{
					$add_array = array(
						'Gene' 				=> 	$gene,
						'DNA'				=>	'No mutation',
						'Protein' 			=> 	'N/A',
						'VAF'				=> 	'N/A',
						'COSMIC' 				=> 	'N/A',
						'Tier' 				=> 	'N/A',
						'observed_variant_id' => '',
						'knowledge_id'	=> 	''
					);
			
					array_push($table_array, $add_array);
				}
			}

			return $table_array;
		}

		public function UnknowSignificanceGenes()
		{
			return $this->unknown_sig_genes;
		}

		public function GenesWhichAreOnlyUnknownSignificance()
		{
			// sometimes there can be mutations in the same gene some are significant and some are of unknown significance

			$only_unknown_sig_genes = array_diff($this->unknown_sig_genes,$this->sig_genes);
			return $only_unknown_sig_genes;

		} 

		private function addMutantsToTableWithBenign()
		{
			// Separate mutations into Mutation or VARIANTS OF UNKNOWN SIGNIFICANCE to make tables

			$mutations_array = array();
			$unknown_sig_array = array();
			$benign_array = array();


			foreach ($this->variantArray as $key => $v)
			{	
					
				if ($v['tier'] !== 'Tier III' && $v['tier'] !== 'Tier IV')
				{				
					$add_array = array(
						'Gene' 				=> 	$v['genes'],
						'DNA'				=>	$v['coding'],
						'Protein' 			=> 	$v['amino_acid_change'],
						'VAF'				=> 	round($v['frequency']).'%',
						'COSMIC'			 	=> 	$v['cosmic'],
						'Tier'		 		=> 	$v['tier'],
						'observed_variant_id' => isset($v['observed_variant_id']) ? $v['observed_variant_id'] : '',
						'knowledge_id'	=> 	isset($v['knowledge_id']) ? $v['knowledge_id'] : '',
						'variant_interpt' => isset($v['variant_interpt']) ? $v['variant_interpt'] : '',
						'activate_status' => isset($v['activate_status']) ? $v['activate_status'] : ''
												
					);
					array_push($mutations_array, $add_array);
					array_push($this->sig_genes, $v['genes']);
				}
				else if ($v['tier'] === 'Tier III')
				{		
						
					$add_array = array(
						'Gene' 				=> 	$v['genes'],
						'DNA'				=>	$v['coding'],
						'Protein' 			=> 	$v['amino_acid_change'],
						'VAF'				=> 	round($v['frequency']).'%',
						'COSMIC'			 	=> 	$v['cosmic'],
						'Tier'		 		=> 	$v['tier'],
						'observed_variant_id' => isset($v['observed_variant_id']) ? $v['observed_variant_id'] : '',
						'knowledge_id'	=> 	isset($v['knowledge_id']) ? $v['knowledge_id'] : '',
						'variant_interpt' => isset($v['variant_interpt']) ? $v['variant_interpt'] : '',
						'activate_status' => isset($v['activate_status']) ? $v['activate_status'] : ''
												
					);
					array_push($unknown_sig_array, $add_array);
					array_push($this->unknown_sig_genes, $v['genes']);
				}
				else if ($v['tier'] === 'Tier IV')
				{		
						
					$add_array = array(
						'Gene' 				=> 	$v['genes'],
						'DNA'				=>	$v['coding'],
						'Protein' 			=> 	$v['amino_acid_change'],
						'VAF'				=> 	round($v['frequency']).'%',
						'COSMIC'			 	=> 	$v['cosmic'],
						'Tier'		 		=> 	$v['tier'],
						'observed_variant_id' => isset($v['observed_variant_id']) ? $v['observed_variant_id'] : '',
						'knowledge_id'	=> 	isset($v['knowledge_id']) ? $v['knowledge_id'] : '',
						'variant_interpt' => isset($v['variant_interpt']) ? $v['variant_interpt'] : '',
						'activate_status' => isset($v['activate_status']) ? $v['activate_status'] : ''
												
					);
					array_push($benign_array, $add_array);
					array_push($this->benign_genes, $v['genes']);
				}				
			}

			// add to mutant_table array 
			$this->mutant_tables['MUTATIONS'] = array();
			array_push($this->mutant_tables['MUTATIONS'], $mutations_array);

			$this->mutant_tables['VARIANTS OF UNKNOWN SIGNIFICANCE'] = array();
			array_push($this->mutant_tables['VARIANTS OF UNKNOWN SIGNIFICANCE'], $unknown_sig_array);

			$this->mutant_tables['BENIGN VARIANTS'] = array();
			array_push($this->mutant_tables['BENIGN VARIANTS'], $benign_array);

		}

		private function addMutantsToTable()
		{
			// Separate mutations into Mutation or VARIANTS OF UNKNOWN SIGNIFICANCE to make tables

			$mutations_array = array();
			$unknown_sig_array = array();


			foreach ($this->variantArray as $key => $v)
			{	
					
				if ($v['tier'] !== 'Tier III' && $v['tier'] !== 'Tier IV')
				{				
					$add_array = array(
						'Gene' 				=> 	$v['genes'],
						'DNA'				=>	$v['coding'],
						'Protein' 			=> 	$v['amino_acid_change'],
						'VAF'				=> 	round($v['frequency']).'%',
						'COSMIC'			 	=> 	$v['cosmic'],
						'Tier'		 		=> 	$v['tier'],
						'observed_variant_id' => isset($v['observed_variant_id']) ? $v['observed_variant_id'] : '',
						'knowledge_id'	=> 	isset($v['knowledge_id']) ? $v['knowledge_id'] : '',
						'variant_interpt' => isset($v['variant_interpt']) ? $v['variant_interpt'] : '',
						'activate_status' => isset($v['activate_status']) ? $v['activate_status'] : '',
						'genetic_call' 		=> isset($v['genetic_call']) ? $v['genetic_call'] : ''
												
					);
					array_push($mutations_array, $add_array);
					array_push($this->sig_genes, $v['genes']);
				}
				else if ($v['tier'] === 'Tier III' || $v['tier'] === 'Tier IV')
				{		
						
					$add_array = array(
						'Gene' 				=> 	$v['genes'],
						'DNA'				=>	$v['coding'],
						'Protein' 			=> 	$v['amino_acid_change'],
						'VAF'				=> 	round($v['frequency']).'%',
						'COSMIC'			 	=> 	$v['cosmic'],
						'Tier'		 		=> 	$v['tier'],
						'observed_variant_id' => isset($v['observed_variant_id']) ? $v['observed_variant_id'] : '',
						'knowledge_id'	=> 	isset($v['knowledge_id']) ? $v['knowledge_id'] : '',
						'variant_interpt' => isset($v['variant_interpt']) ? $v['variant_interpt'] : '',
						'activate_status' => isset($v['activate_status']) ? $v['activate_status'] : '',
						'genetic_call' 		=> isset($v['genetic_call']) ? $v['genetic_call'] : ''
												
					);
					array_push($unknown_sig_array, $add_array);
					array_push($this->unknown_sig_genes, $v['genes']);
				}				
			}

			// add to mutant_table array 
			$this->mutant_tables['MUTATIONS'] = array();
			array_push($this->mutant_tables['MUTATIONS'], $mutations_array);

			$this->mutant_tables['VARIANTS OF UNKNOWN SIGNIFICANCE'] = array();
			array_push($this->mutant_tables['VARIANTS OF UNKNOWN SIGNIFICANCE'], $unknown_sig_array);

		}

		public function getReportTable()
		{	
			return $this->mutant_tables;
		}

		public function getReportGeneOrder()
		{
			// find gene order of genes which have mutations which will be included in the report
			$gene_order = array();

			foreach ($this->mutant_tables as $table)
			{
				foreach ($table[0] as $variant)
				{
					if ($variant['DNA'] !== 'No mutation' && !in_array($variant['Gene'], $gene_order))
					{
						array_push($gene_order, $variant['Gene']);	
					}							
				}				
			}
			return $gene_order;
		}

		public function getReportVariantOrder()
		{
			$variant_order = array();
			foreach ($this->mutant_tables as $table)
			{			

				foreach ($table[0] as $variant)
				{
					if ($variant['DNA'] !== 'No mutation')
					{
						$curr_var_array = array(
							'Gene' 	=> 	$variant['Gene'],
							'DNA' 	=> 	$variant['DNA'],
							'Protein' 	=> 	$variant['Protein'],
							'observed_variant_id' => isset($variant['observed_variant_id']) ? $variant['observed_variant_id'] : '',
							'knowledge_id'	=> 	isset($variant['knowledge_id']) ? $variant['knowledge_id'] : '',
							'variant_interpt' => isset($variant['variant_interpt']) ? $variant['variant_interpt'] : '',
							'tier'		 		=> 	isset($variant['Tier']) ? $variant['Tier'] : '',
							'genetic_call'		=> 	isset($variant['genetic_call']) ? $variant['genetic_call'] : ''
						);
						if (!isset($variant_order[$variant['Gene']]))
						{
							$variant_order[$variant['Gene']] = array();
						}
						array_push($variant_order[$variant['Gene']], $curr_var_array);
					}							
				}				
			}

			return $variant_order;
		}

		public function getExtraGenesRequired()
		{
			return $this->extraGenesRequired;
		}
	}
?>
