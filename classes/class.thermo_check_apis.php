<?php

	class ThermoCheckAPIs extends APIUtils
	{
		private $errors = array();
		private $sample_names = array();

		public function run($full_run=True)
		{
			////////////////////////////////////////////////////
			// Check that IPs are correct for the tocheckservers
			///////////////////////////////////////////////////
			$toCheckServers = array('Torrent_Server', 'Ion_Reporter');
			foreach ($toCheckServers as $key => $curr_server)
			{
				$this->checkIPs($curr_server);
			}

			////////////////////////////////////////////////////
			// Make sure the backup location exists.
			///////////////////////////////////////////////////
			$this->checkBackLocExists();

			////////////////////////////////////////////////////
			// Make TS mounted location 
			///////////////////////////////////////////////////
			$this->checkTSMounted();

			// ////////////////////////////////////////////////////
			// // Check all API calls used in class.thermo_backup->TorrentServerRunQualityResults()
			// ///////////////////////////////////////////////////
			if ($full_run)
			{
				$this->checkTSRunQualityResultAPIs();	
			}
		}

		public function getErrors()
		{
			return $this->errors;
		}

		public function getIRCommandlineStatement()
		{
			if (!empty($this->sample_names))
			{
				return 'python python/check_IR_API.py -o /media/storage/fs-ngs/devs/ion_torrent/check_ir_api -api_server '.$this->server['Ion_Reporter']['api_server'].' -api_token '.$this->server['Ion_Reporter']['api_key'].' -sample '.$this->sample_names[0];
			}

			
			else
			{
				return False;
			}
		}

		public function checkTSRunQualityResultAPIs()
		{
			// This function will ensure class.thermo_backup->TorrentServerRunQualityResults() APIs are set up as expected.
			if (empty($this->errors))
			{
				$this->checkTSConnection();
				$this->checkTSresultsAPINotEmpty();
				
				if (empty($this->errors))
				{
					$this->checkTSresultsAPI();
				}

				if (empty($this->errors))
				{
					$this->checkTStorrentsuiteAPI();
				}
			}
		}

		public function checkBackLocExists()
		{
			
			// Make sure the backup location still exists 
			if (!file_exists($this->server['backup_locs']['ion_torrent_backup_dir']))
			{
				$this->errors['backup location does not exist'] = 'The backup location does not exist: '.$this->server['backup_locs']['ion_torrent_backup_dir'];
			}
		}

		public function checkTSMounted()
		{
		
			if (!file_exists($this->server['Torrent_Server']['mounted_loc']))
			{
				$this->errors['TS mounted error'] = 'The TS Mounted location does not exist: '.$this->server['Torrent_Server']['mounted_loc'];
			}			
		}

		public function checkIPs($curr_server)
		{
			if (!$this->pingAddress($this->server[$curr_server]['ip']))
			{
				$this->errors['ip'] = $curr_server.' is not live.  IP address does not ping: '.$this->server[$curr_server]['ip'];
			}		
		}

		public function checkTSConnection()
		{
			// since the api call torrentsuite should return an array even if there's no data use this to see if user name and api key are correct.  Password is not used at all for API.
			
			$url = 'http://'.$this->server['Torrent_Server']['ip'].'/rundb/api/v1/torrentsuite/'.$this->Torrent_server_login_creditials;

			$response_array = $this->AccessRestAPI($url);

			$response_type = gettype($response_array);

			if ($response_type === 'boolean')
			{
				$this->errors['login credentials'] = 'Something is wrong with the TS login credentials. The password does not get used in this API so something is wrong with Key or username. Login credentials are: '.$this->Torrent_server_login_creditials;				
			}
		}

		public function checkTSresultsAPINotEmpty()
		{
			// check that an array is returned and that it isn't empty
			$url = 'http://'.$this->server['Torrent_Server']['ip'].'/rundb/api/v1/results/'.$this->Torrent_server_login_creditials;
			
			// Then find a result without _tn in name to see if all necessary fields are present

			$response_array = $this->AccessRestAPI($url);

			if (gettype($response_array) === 'boolean')
			{
				$this->errors['result API'] = 'Something is wrong with the results API.  A bool was returned instead of result array';
			}
			else if (!isset($response_array['objects']) || empty($response_array['objects']))
			{
				$this->errors['result API'] = 'No data could be found.';
			}
		}

		public function checkTSresultsAPI()
		{
			// Make a call to the results API this is the main call that all other calls run thru.
			$url = 'http://'.$this->server['Torrent_Server']['ip'].'/rundb/api/v1/results/'.$this->Torrent_server_login_creditials;
			

			// Then find a result without _tn in name to see if all necessary fields are present
			$response_array = $this->AccessRestAPI($url);


			// since it is unknown what data set will be find the first resultsName without a _tn 
			$objects = $response_array['objects'];

			$format_correct = true;
			$found_at_least_one = false;
			foreach ($objects as $key => $obj)
			{
				if (empty($obj['pluginresults']) || sizeOf($obj['pluginresults']) < 2) 
				{
					var_dump($obj['resultsName']);
					continue;
				}

				if (isset($obj['resultsName']) && isset($obj['status']) && strpos($obj['resultsName'], '_tn') === false && $obj['status'] === 'Completed')
				{
					$found_at_least_one = true;

					$required_fields = array('id', 'timeStamp', 'filesystempath', 'runid', 'analysismetrics', 'qualitymetrics', 'libmetrics', 'experiment', 'pluginresults');

					/////////////////////////////////////////////////////////////////
					// make sure all necessary fields are present in the API call to results
					/////////////////////////////////////////////////////////////////
					foreach($required_fields as $k => $field)
					{
						if (!isset($obj[$field]) || empty($obj[$field]))
						{
							$this->errors['results format error'] = 'ResultName: '.$obj['resultsName'].' Does not have a '.$field.' '.json_encode($obj);
							$format_correct = false;
						}
					}				
				}

				/////////////////////////////////////////////////////////////////
				// Make sure quality metrics are as expected
				/////////////////////////////////////////////////////////////////
				$api_calls = array('analysismetrics', 'qualitymetrics', 'libmetrics', 'experiment');

				// Make calls for each type of data listed in api_calls
				foreach ($api_calls as $call)
				{			

					// call each API that is not nested (experiment only)
					if (is_string($obj[$call]))
					{
						$url = 'http://'.$this->server['Torrent_Server']['ip'].$obj[$call].$this->Torrent_server_login_creditials.'&resultsName='.$obj['resultsName'];

						$rest_response = $this->AccessRestAPI($url);
					
						if ($rest_response === False)
						{							
							$this->errors['API Call '.$call] = 'Error with API Call '.$call.'  False was returned as ';
							$format_correct = false;
						}

						/////////////////////////////////////////////////////////////
						// experiment is an array and in in class.thermo_backup->QCReportHeader() chipBarcode and id are accessed.  Make sure these are present
						/////////////////////////////////////////////////////////////
						if ($call === 'experiment')
						{
							
							if (!isset($rest_response['chipBarcode']) || empty($rest_response['chipBarcode']))
							{
								$this->errors['results format error'] = 'experiment does not have chipBarcode. '.json_encode($rest_response);
								$format_correct = false;
							}

							if (!isset($rest_response['id']) || empty($rest_response['id']))
							{								
								$this->errors['results format error'] = 'experiment does not have id. '.json_encode($rest_response);
								$format_correct = false;
							}
						}						
					}

					// some calls are nested and some are just strings.
					else
					{
						// These calls are all nested therefore pull all data.
						foreach ($obj[$call] as $j => $arr)
						{
							$url = 'http://'.$this->server['Torrent_Server']['ip'].$obj[$call][$j].$this->Torrent_server_login_creditials.'&resultsName='.$obj['resultsName'];

							$rest_response = $this->AccessRestAPI($url);
						
							if ($rest_response === False)
							{							
								$this->errors['API Call '.$call] = 'Error with API Call '.$call.'  False was returned as ';
								$format_correct = false;
							}	

							/////////////////////////////////////////////////////////////
							// analysismetrics is an array and in in class.thermo_backup->MakeQCReportFile() chipBarcode and id are accessed.  Make sure these are present
							/////////////////////////////////////////////////////////////
							else if ($call === 'analysismetrics')
							{
								
								$analysismetrics_required = array('libFinal', 'lib', 'loading','live', 'bead', 'tf');

								foreach ($analysismetrics_required as $key => $field)
								{
									if (!isset($rest_response[$field]) || empty($rest_response[$field]) || !is_numeric($rest_response[$field]))
									{
										$this->errors['analysismetrics format error '.$field] = 'analysismetrics does not have '.$field.'. '.json_encode($rest_response);
										$format_correct = false;
									}

								}														
							}

							else if ($call === 'qualitymetrics')
							{
							
								if 	(
										!isset($rest_response['q0_median_read_length']) || 
										empty($rest_response['q0_median_read_length']) || 
										!is_numeric($rest_response['q0_median_read_length'])
									)
								{
									$this->errors['qualitymetrics format error q0_median_read_length'] = 'qualitymetrics does not have q0_median_read_length. '.json_encode($rest_response);
									$format_correct = false;
								}										
							}

							else if ($call === 'libmetrics')
							{
							
								if 	(
										!isset($rest_response['raw_accuracy']) || 
										empty($rest_response['raw_accuracy']) || 
										!is_numeric($rest_response['raw_accuracy'])
									)
								{
									$this->errors['libmetrics format error raw_accuracy'] = 'libmetrics does not have raw_accuracy. '.json_encode($rest_response);
									$format_correct = false;
								}										
							}

							
						}
					}					
				}
						
				/////////////////////////////////////////////////////////////////
				// See if coverage analysis is working correctly
				/////////////////////////////////////////////////////////////////
				// Find which installed plugin is the coverage analysis plugin and return the array.  I'm only interested in the 
				// Coverage analysis plugin
				// (array) -> array
				// Example $installed_plugins
					//  array (size=3)
						// 0 => string '/rundb/api/v1/pluginresult/59/' (length=30)
						// 1 => string '/rundb/api/v1/pluginresult/58/' (length=30)
						// 2 => string '/rundb/api/v1/pluginresult/57/' (length=30)

				$cov_analysis_found = False;
				foreach ($obj['pluginresults'] as $l => $plugin)
				{
					$url = 'http://'.$this->server['Torrent_Server']['ip'].$plugin.$this->Torrent_server_login_creditials;

					$rest_response = $this->AccessRestAPI($url);

					if (!isset($rest_response['pluginName'])) 
					{
						$this->errors['plugin error pluginName'] = 'Error with plugin.  Plugin name not found.';
						$format_correct = false;
					}

					else if ($rest_response['pluginName'] === 'coverageAnalysis')
					{
						$cov_analysis_found = True;

						/////////////////////////////////////////////////////////////////
						// Make sure URLs present in CoverageAnalysis JSON actually exist
						/////////////////////////////////////////////////////////////////
						if (!isset($rest_response['URL']) || empty($rest_response['URL']) || !file_exists($this->server['Torrent_Server']['mounted_loc'].$rest_response['URL']))
						{
							$this->errors['plugin error URL'] = 'CoverageAnalysis URL not set';
							$format_correct = false;
						}

						else
						{
							$coverage_dir = $this->server['Torrent_Server']['mounted_loc'].$rest_response['URL'];
							$bc_summary = $this->get_files_with_extension($coverage_dir, 'bc_summary.xls');

							$bc_matrix = $this->get_files_with_extension($coverage_dir, 'bcmatrix.xls');

							if (empty($bc_summary) || !isset($bc_summary[0]))
							{
								$this->errors['bc_summary.xls not found'] = 'bc_summary.xls not found at: '.$coverage_dir;
							}

							// Make sure file exists
							else if (!file_exists($bc_summary[0]))
							{
								$this->errors['bc_summary.xls does not exist'] = 'bc_summary.xls does not exist at '.$bc_summary[0];
							}

							// Make sure format of the bc_summary is the same as expected
							else
							{
								$this->checkbc_summary_file($bc_summary[0]);
							}	

							// Make sure bc_matrix file exists 
							if (empty($bc_matrix) || !isset($bc_matrix[0]))	
							{
								// Back up original file
								$this->errors['bcmatrix.xls not found'] = 'bcmatrix.xls not found at: '.$coverage_dir;
							}	
							// Make sure file exists
							else if (!file_exists($bc_matrix[0]))
							{
								$this->errors['bc_summary.xls does not exist'] = 'bc_summary.xls does not exist at '.$bc_summary[0];
							}
							else
							{
								$this->checkbc_matrix_file($bc_matrix[0]);
							}

						}
					

						/////////////////////////////////////////////////////////////////
						// Make sure json files made by coverageAnalysis are present
							// BaseCaller -> JSON is used for QC. for polyclonal%, clonal%, final library %
						/////////////////////////////////////////////////////////////////
						if (isset($rest_response['reportLink']) && !empty($rest_response['reportLink']))
						{
							$report_dir = $this->server['Torrent_Server']['mounted_loc'].$rest_response['reportLink'].'basecaller_results';
							$basecaller_f = $report_dir.'/BaseCaller.json';							

							if (!file_exists($basecaller_f))
							{	
								$this->errors['plugin error BaseCaller'] = 'BaseCaller.json on found at: '.$basecaller_f;
								$format_correct = false;
							}

							// open json if it exists and ensure everything is present that is necessary.
							else
							{
			
								$baseCallerJSON = json_decode($this->returnFileContents($basecaller_f), true);

								// Make sure all necessary fields are present
								if 	( 
										!isset($baseCallerJSON['Filtering']['LibraryReport']['filtered_polyclonal']) || 
										empty($baseCallerJSON['Filtering']['LibraryReport']['filtered_polyclonal']) || 
										!is_numeric($baseCallerJSON['Filtering']['LibraryReport']['filtered_polyclonal']) ||

										!isset($baseCallerJSON['Filtering']['LibraryReport']['filtered_primer_dimer']) || 
										empty($baseCallerJSON['Filtering']['LibraryReport']['filtered_primer_dimer']) ||
										!is_numeric($baseCallerJSON['Filtering']['LibraryReport']['filtered_primer_dimer']) ||

										!isset($baseCallerJSON['Filtering']['LibraryReport']['filtered_low_quality']) || 
										empty($baseCallerJSON['Filtering']['LibraryReport']['filtered_low_quality']) ||
										!is_numeric($baseCallerJSON['Filtering']['LibraryReport']['filtered_low_quality']) ||

										!isset($baseCallerJSON['Filtering']['LibraryReport']['final_library_reads']) || 
										empty($baseCallerJSON['Filtering']['LibraryReport']['final_library_reads']) ||
										!is_numeric($baseCallerJSON['Filtering']['LibraryReport']['final_library_reads']) 
									)
								{
									$this->errors['BaseCaller Missing Numeric fields'] = 'BaseCaller.json on found at: '.$basecaller_f;
									$format_correct = false;
								}
							}
						}
						else
						{
							$this->errors['plugin error reportLink'] = 'CoverageAnalysis plugin result JSON does not have a reportLink';
						}
						break;
					}								
				}

				// Report error if analysis plugin was never found
				if (!$cov_analysis_found)
				{
					$this->errors['plugin error'] = 'Error with plugin. coverageAnalysis plugin not found.';
						$format_correct = false;
				}
				break;
			}
			
		}

		public function checkbc_matrix_file($bc_matrix_file)
		{
			// Check bc_matrix_file used by python/thermo_make_cov_files.py


			$of = fopen($bc_matrix_file, 'r') or die('unable to open file!');

			$header = rtrim(fgets($of));
		
			/////////////////////////////////////////////////////////////////
			// The JSON made from bc_summary file is used in pool_run_qc_tables.  Make sure all necessary columns are present and at least
			// one sample is listed
			/////////////////////////////////////////////////////////////////
			if (	
					strpos($header, 'Gene') === false ||
					strpos($header, 'Target') === false 					 
				)
			{
				$this->errors['bc_matrix_file.xls header Error'] = 'bc_matrix_file.xls header does not contain Gene and/or Target.  Header is '.$header;
			}

			$split_header = explode('	', $header);

			$out_json = array();

			$samp_count = 0;

			while(!feof($of))
			{
				$line = rtrim(fgets($of));
				$samp_count ++;

				if (empty($line))
				{
					break;
				}

				$split_line = explode('	', $line);

				$matched_array = $this->array_combine($split_header, $split_line);

				if (	
					!isset($matched_array['Gene']) ||
					empty($matched_array['Gene']) ||

					!isset($matched_array['Target']) ||
					empty($matched_array['Target']) 
				)
				{
					$this->errors['bc_matrix_file.xls matched_array Error'] = 'bc_matrix_file.xls matched_array does not contain Gene and/or Target. '.$line;
				}

			}

			fclose($of);
		}

		public function checkbc_summary_file($bc_summary_f)
		{
			$of = fopen($bc_summary_f, 'r') or die('unable to open file!');

			$header = rtrim(fgets($of));

			/////////////////////////////////////////////////////////////////
			// The JSON made from bc_summary file is used in pool_run_qc_tables.  Make sure all necessary columns are present and at least
			// one sample is listed
			/////////////////////////////////////////////////////////////////
			if (	
					strpos($header, 'Sample Name') === false ||
					strpos($header, 'On Target') === false ||
					strpos($header, 'Mean Depth') === false ||
					strpos($header, 'Uniformity') === false ||
					strpos($header, 'Barcode ID') === false 
					 
				)
			{
				$this->errors['bc_summary.xls header Error'] = 'bc_summary.xls header does not contain Sample Name, On Target, Mean Depth, Uniformity, Barcode ID.  Header is '.$header;
			}


			$split_header = explode('	', $header);

			$out_json = array();

			$samp_count = 0;

			while(!feof($of))
			{
				$line = rtrim(fgets($of));
				$samp_count ++;

				if (empty($line))
				{
					break;
				}

				$split_line = explode('	', $line);

				$matched_array = $this->array_combine($split_header, $split_line);
				
				////////////////////////////////////////////////////////////////////
				// Make sure all of the required fields are present for each sample
				////////////////////////////////////////////////////////////////////
				if (	
						!isset($matched_array['Sample Name']) ||
						empty($matched_array['Sample Name']) 
					)
				{
					$this->errors['bc_summary.xls matched_array Error Sample Name'] = 'bc_summary.xls matched_array does not contain Sample Name. '.$line;
				}

				if (	
						!isset($matched_array['On Target']) ||
						empty($matched_array['On Target']) 
					)
				{
					$this->errors['bc_summary.xls matched_array Error Target'] = 'bc_summary.xls matched_array does not contain On Target. '.$line;
				}
				if (	
					
						!isset($matched_array['Mean Depth']) ||
						empty($matched_array['Mean Depth']) 
					)
				{
					$this->errors['bc_summary.xls matched_array Error Mean Depth'] = 'bc_summary.xls matched_array does not contain Mean Depth. '.$line;
				}
				if (	
					
						!isset($matched_array['Uniformity']) ||
						empty($matched_array['Uniformity']) 
					)
				{
					$this->errors['bc_summary.xls matched_array Error Uniformity'] = 'bc_summary.xls matched_array does not contain Uniformity. '.$line;
				}
				if (	
					
						!isset($matched_array['Barcode ID']) ||
						empty($matched_array['Barcode ID'])
					) 

				{
					$this->errors['bc_summary.xls matched_array Error Barcode ID'] = 'bc_summary.xls matched_array does not contain Barcode ID. '.$line;
				}

				////////////////////////////////////////////////////////////////////
				// Add Sample name to $this->sample_names array.  This will be used to assess the Ion Report API
				////////////////////////////////////////////////////////////////////
				if (isset($matched_array['Sample Name']) && !empty($matched_array['Sample Name']))
				{
					array_push($this->sample_names, $matched_array['Sample Name']);

				}
			}

			fclose($of);
		}

		public function checkTStorrentsuiteAPI()
		{
			// Make sure all versions can be obtained for run.

			// Add versions
			$url = 'http://'.$this->server['Torrent_Server']['ip'].'/rundb/api/v1/torrentsuite/'.$this->Torrent_server_login_creditials;

			$version_array = $this->AccessRestAPI($url);
		
			if (gettype($version_array) === 'boolean')	
			{
				$this->errors['Error torrentsuite API call'] = 'Error with torrentsuite API call.  This API call should supply all version information';
			}
		}
	}
	
?>