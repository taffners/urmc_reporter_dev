<?php


class html extends FPDF
{
	//variables of html parser
	protected $B;
	protected $I;
	protected $U;
	protected $HREF;
	protected $fontList;
	protected $issetfont;
	protected $issetcolor;
	protected $subFontSizeold;

	function __construct($orientation='P', $unit='mm', $format='A4', $header_footer_array)
	{
		//Call parent constructor
		parent::__construct($orientation,$unit,$format,$header_footer_array['header_status'],$header_footer_array['footer_status'],$header_footer_array['address_array']);

		//Initialization
		$this->B=0;
		$this->I=0;
		$this->U=0;
		$this->HREF='';

		$this->tableborder=0;
		$this->tdbegin=false;
		$this->tdwidth=0;
		$this->tdheight=0;
		$this->tdalign="L";
		$this->tdbgcolor=false;

		$this->oldx=0;
		$this->oldy=0;

		$this->fontlist=array("arial","times","courier","helvetica","symbol");
		$this->issetfont=false;
		$this->issetcolor=false;

	    	$this->is_a_table=false;
	    	// toggle header on and off
		$this->header_status = $header_footer_array['header_status'];

		// toggle footer on and off
		$this->footer_status= $header_footer_array['footer_status'];

		// set address
		$this->address_array = $header_footer_array['address_array'];
	}

	//////////////////////////////////////
	//html parser
		
	function AddHTML($content, $font_size=12)
	{
		// change font size
		$this->SetFont('Arial','',$font_size);
		$this->WriteHTML($content);
	}

	public function WriteHTML($html)
	{
		// (html) -> pdf with clickable links
		// HTML parser
		
		$this->subFontSizeold = $this->FontSizePt;

		$html=strip_tags($html,"<b><u><i><a><img><p><br><strong><em><font><tr><blockquote><hr><td><tr><table><sup><th><ts>"); //remove all unsupported tags
		$html=str_replace("\n",'',$html); //replace carriage returns with spaces
		$html=str_replace("\t",'',$html); //replace carriage returns with spaces
		$a=preg_split('/<(.*)>/U',$html,-1,PREG_SPLIT_DELIM_CAPTURE); //explode the string

		// splits html where an html tag is found
		// example $a
			// 0 => string 'CTNNB-1 c.122C>A mutations have been characterized in a number of different tumors, such tumors of the liver ,skin, cervix, and hematopoetic system. This particular mutation has not been characterized in lung adenocarcinomas and the clinical significance is currently unknown. (' (length=278)
			// 1 => string 'a href="http://cancer.sanger.ac.uk/cosmic/mutation/overview?id=5730" target="_blank"' (length=84)
			// 2 => string 'http://cancer.sanger.ac.uk/cosmic/mutation/overview?id=5730' (length=59)
			// 3 => string '/a' (length=2)
			// 4 => string ')' (length=1)


		
		foreach($a as $i=>$e)
		{			
			// why only even numbers?
			if($i%2==0)
			{				
		
				// Text
				if($this->HREF)
				{
	
					$this->PutLink($this->HREF,$e);
				}
				elseif($this->tdbegin) 
				{
                		if(trim($e)!='' && $e!="&nbsp;") 
                		{
	
                			// add text to table
                			if($this->is_a_table)
                			{            
                				$this->Cell($this->tdwidth,$this->tdheight,$e,$this->tableborder,'',$this->tdalign,$this->tdbgcolor);
                			}

                			// add All text to page Interpretations also
                			else
                			{                        				
							$line_spacing = $this->findLineSpacing();
                				$this->Write($line_spacing, $e);
                			}
                    		
                		}
                		elseif($e=="&nbsp;") 
                		{
                			if($this->is_a_table)
                			{

                    			$this->Cell($this->tdwidth,$this->tdheight,'',$this->tableborder,'',$this->tdalign,$this->tdbgcolor);
                    		}
                			else
                			{
                				$this->Write(5, $e);
                			}
                		}
            		}				
				else
				{				
					 $this->Write(5,stripslashes($this->txtentities($e)));

				}
			}
			else
			{

				// Tag
				if($e[0]=='/') // closing tag
				{

					$this->CloseTag(strtoupper(substr($e,1)));
				}
				else
				{
						
					// Extract attributes
					// example $a2
						// 0 => string 'a' (length=1)
						// 1 => string 'href="http://cancer.sanger.ac.uk/cosmic/mutation/overview?id=5730"' (length=66)
						// 2 => string 'target="_blank"' (length=15)
					$a2 = explode(' ',$e);

					$tag = strtoupper(array_shift($a2));
					
					$attr = array();

					// css
					foreach($a2 as $v)
					{

						if(preg_match('/([^=]*)=["\']?([^"\']*)/',$v,$a3))
						{
							$attr[strtoupper($a3[1])] = $a3[2];
						}
					}
					
					$this->OpenTag($tag,$attr);
				}
			}
		}
	}

	function findLineSpacing()
	{
		if ($this->FontSizePt == 8)
		{
			return 3.5;
		}  
		elseif ($this->FontSizePt == 7)
		{
			return 3.5;
		} 

		else
		{
			return 5;
		}
	}


	function subWrite($h, $txt, $subFontSize=12, $subOffset=0)
	{
	    // resize font
	    $subFontSizeold = $this->FontSizePt;
	    $this->SetFontSize($subFontSize);
	    
	    // reposition y
	    $subOffset = ((($subFontSize - $subFontSizeold) / $this->k) * 0.3) + ($subOffset / $this->k);
	    $subX        = $this->x;
	    $subY        = $this->y;
	    $this->SetXY($subX, $subY - $subOffset);

	    //Output text
	    $this->Write($h, $txt);

	    // restore y position
	    $subX        = $this->x;
	    $subY        = $this->y;
	    $this->SetXY($subX,  $subY + $subOffset);

	    // restore font size
	    $this->SetFontSize($subFontSizeold);
	}

	private function OpenTag($tag, $attr)
	{
		switch($tag)
		{
			case 'SUP':

				$subFontSize=6;
				$subOffset=4;
				$this->tableborder = '';
				// resize font
				$subFontSizeold = $this->FontSizePt;
				$this->SetFontSize($subFontSize);

				// reposition y
				$subOffset = ((($subFontSize - $subFontSizeold) / $this->k) * 0.3) + ($subOffset / $this->k);
				if ($this->is_a_table)
				{
					$subX        = $this->x-3;
				}
				else
				{
					$subX        = $this->x;
				}
				
				$subY        = $this->y;
				$this->SetXY($subX, $subY - $subOffset);

				break;
			
			case 'B':
			case 'I':
        		case 'U':
        			$this->SetStyle($tag,true);
        			break;
		     
		     case 'A':
		     	$this->tableborder = '';
            		$this->HREF=$attr['HREF'];
            		break;
			
			case 'BLOCKQUOTE':
			case 'BR':
				$this->Ln(4);
				break;  

			case 'TABLE': // TABLE-BEGIN

				$this->is_a_table = true;

				if( !empty($attr['BORDER']) ) 
				{
					$this->tableborder=$attr['BORDER'];
				}
				else 
				{
					$this->tableborder=0;
				}
				break;

			case 'TR': //TR-BEGIN
				$this->tdalign = 'c';

				// line spacing is off for tables with small print adjust it
				if ($this->subFontSizeold == 7.5)
				{
					$this->Ln(4.5);
				}
				else
				{
					$this->Ln(5);	
				}
				
				break;
			
			case 'TS':
			
				if( !empty($attr['WIDTH']) ) 
				{
					$this->tdwidth=($attr['WIDTH']/4);
				}
				else 
				{
					$this->tdwidth=40; // Set to your own width if you need bigger fixed cells
				}
			
				if( !empty($attr['HEIGHT']) ) 
				{
					$this->tdheight=($attr['HEIGHT']/6);
				}
				else 
				{
					$this->tdheight=6; // Set to your own height if you need bigger fixed cells
				}

				$this->SetTextColor(255,255,255);
				$this->tableborder = '';
				break;

			case 'TD': // TD-BEGIN
				
				if( !empty($attr['WIDTH']) ) 
				{
					$this->tdwidth=($attr['WIDTH']/4);
				}
				else 
				{
					$this->tdwidth=40; // Set to your own width if you need bigger fixed cells
				}
			
				if( !empty($attr['HEIGHT']) ) 
				{
					$this->tdheight=($attr['HEIGHT']/6);
				}
				else 
				{
					$this->tdheight=6; // Set to your own height if you need bigger fixed cells
				}

				if( !empty($attr['ALIGN']) ) 
				{
					$align=$attr['ALIGN'];        
					if($align=='LEFT') 
					{
						$this->tdalign='L';
					}					
					
					if($align=='CENTER') 
					{	
						$this->tdalign='C';
					}
					
					if($align=='RIGHT') 
					{
						$this->tdalign='R';
					}
				}
				else 
				{
					$this->tdalign='C'; // Set to your own
				}			

				if( !empty($attr['BGCOLOR']) ) 
				{
					$coul=$this->hex2dec($attr['BGCOLOR']);
					$this->SetFillColor($coul['R'],$coul['G'],$coul['B']);
					$this->tdbgcolor=true;
				}
				$this->SetFont('','',$this->subFontSizeold);
				$this->tdbegin=true;
				break;				          		
			
			case 'TH': // TD-BEGIN
				$this->SetFont('','B',$this->subFontSizeold);
				if( !empty($attr['WIDTH']) ) 
				{
					$this->tdwidth=($attr['WIDTH']/4);
				}
				else 
				{
					$this->tdwidth=40; // Set to your own width if you need bigger fixed cells
				}
			
				if( !empty($attr['HEIGHT']) ) 
				{
					$this->tdheight=($attr['HEIGHT']/6);
				}
				else 
				{
					$this->tdheight=6; // Set to your own height if you need bigger fixed cells
				}

				if( !empty($attr['ALIGN']) ) 
				{
					$align=$attr['ALIGN'];        
					if($align=='LEFT') 
					{
						$this->tdalign='L';
					}					
					
					if($align=='CENTER') 
					{	
						$this->tdalign='C';
					}
					
					if($align=='RIGHT') 
					{
						$this->tdalign='R';
					}
				}
				else 
				{
					$this->tdalign='C'; // Set to your own
				}			

				if( !empty($attr['BGCOLOR']) ) 
				{
					$coul=$this->hex2dec($attr['BGCOLOR']);
					$this->SetFillColor($coul['R'],$coul['G'],$coul['B']);
					$this->tdbgcolor=true;
				}
				
				$this->tdbegin=true;
				break;          
			
			case 'HR':
				if( !empty($attr['WIDTH']) )
				{
					$Width = $attr['WIDTH'];	
				}
				
				else
				{
					$Width = $this->w - $this->lMargin-$this->rMargin;	
				}
				
				$x = $this->GetX();
				$y = $this->GetY();
				$this->SetLineWidth(0.2);
				$this->Line($x,$y,$x+$Width,$y);
				$this->SetLineWidth(0.2);
				$this->Ln(1);
				break;

			case 'STRONG':
            		$this->SetStyle('B',true);
            		break;
            	
            	case 'EM':
            		$this->SetStyle('I',true);
            		break;
			
			case 'IMG':
				if(isset($attr['SRC']) && (isset($attr['WIDTH']) || isset($attr['HEIGHT']))) 
				{
					if(!isset($attr['WIDTH']))
					{
						$attr['WIDTH'] = 0;	
					}
					
					if(!isset($attr['HEIGHT']))
					{
						$attr['HEIGHT'] = 0;	
					}
					
					$this->Image($attr['SRC'], $this->GetX(), $this->GetY(), px2mm($attr['WIDTH']), px2mm($attr['HEIGHT']));
				}
				break;    
			
			case 'P':
            		$this->Ln(10);
            		break;

        		case 'FONT':
			
				if (isset($attr['COLOR']) && $attr['COLOR']!='') 
				{
					$coul=hex2dec($attr['COLOR']);
					$this->SetTextColor($coul['R'],$coul['G'],$coul['B']);
					$this->issetcolor=true;
				}
				
				if (isset($attr['FACE']) && in_array(strtolower($attr['FACE']), $this->fontlist)) 
				{
					$this->SetFont(strtolower($attr['FACE']));
					$this->issetfont=true;
				}
				
				if (isset($attr['FACE']) && in_array(strtolower($attr['FACE']), $this->fontlist) && isset($attr['SIZE']) && $attr['SIZE']!='') 
				{
					$this->SetFont(strtolower($attr['FACE']),'',$attr['SIZE']);
					$this->issetfont=true;
				}

				elseif (isset($attr['SIZE']) && $attr['SIZE']!='')
				{
					$this->SetFont('','',$attr['SIZE']);
					$this->issetfont=true;
				}

				break;				        		
		}

	}


	private function CloseTag($tag)
	{
		// Closing tag
		if($tag=='B' || $tag=='I' || $tag=='U')
		{
			$this->SetStyle($tag,false);
		}
		if($tag=='A')
		{
			$this->HREF = '';
		}
		if($tag=='FONT')
		{
			if ($this->issetcolor==true) 
			{
				$this->SetTextColor(0);
			}
			if ($this->issetfont) 
			{
				$this->SetFont('arial');
				$this->issetfont=false;
			}
	    	}
		if($tag == 'SUP')
		{
			$subOffset=4;
			// restore y position
			$subX        = $this->x+2.28;
			$subY        = $this->y-3.1;

			$this->SetXY($subX,  $subY + $subOffset);

			// restore font size
			$this->SetFontSize($this->subFontSizeold);	
			$this->tableborder = 'TBLR';
		}	

		if ($tag == 'TH')
		{
			$this->SetFont('','','');
		}	

		if ($tag == 'TABLE')
		{
			$this->is_a_table = false;
			$this->tableborder= '';
		}

		if ($tag == 'TS')
		{
			$this->SetTextColor(0);
			$this->tableborder= 'TBRL';
		}

	}

	private function SetStyle($tag, $enable)
	{
		// Modify style and select corresponding font
		$this->$tag += ($enable ? 1 : -1);
		$style = '';
		
		foreach(array('B', 'I', 'U') as $s)
		{
			if($this->$s>0)
			{
				$style .= $s;
			}
		}
		
		$this->SetFont('',$style);
	}

	private function PutLink($URL, $txt)
	{
	    // Put a hyperlink
	    $this->SetTextColor(0,0,255);
	    $this->SetStyle('U',true);
	    $line_spacing = $this->findLineSpacing();
	    $this->Write($line_spacing,$txt,$URL);
	    $this->SetStyle('U',false);
	    $this->SetTextColor(0);
	}
}
?>
