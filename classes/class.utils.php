<?php

	class Utils
	{
		private $ignore_ids = 	array(
								'visit_id',
								'report_id',
								'patient_id',
								'ngs_panel_id',
								'user_id',
								'run_id',
								'run_ids',
								'observed_variant_id',
								'interpt_id',
								'knowledge_id',
								'tier_id',
								'tissue_id',
								'vt_xref_id',
								'vtx_user_id',
								'confirm_id',
								'diagnosis_id'
							);					
		private $ignore_fields = 	array(
									'first_name',
									'last_name',
									'middle_name',
									'method',
									'panel_type',
									'url',
									'disclaimer',
									'limitations'
								);

		private $halmark_gene = array(
			'ABL1'=> 'yes',
			'AKT1'=> 'no',
			'AKT3'=> 'no',
			'ALK'=> 'yes',
			'APC'=> 'yes',
			'AR'=> 'yes',
			'AXL'=> 'no',
			'BRAF'=> 'yes',
			'CCND1'=> 'yes',
			'CDH1'=> 'yes',
			'CDK4'=> 'yes',
			'CDK6'=> 'yes',
			'CTNNB1'=> 'no',
			'DDR2'=> 'yes',
			'EGFR'=> 'yes',
			'ERBB2'=> 'yes',
			'ERBB3'=> 'yes',
			'ERBB4'=> 'yes',
			'ERG'=> 'yes',
			'ESR1'=> 'yes',
			'ETV1'=> 'no',
			'ETV4'=> 'no',
			'ETV5'=> 'no',
			'FBXW7'=> 'yes',
			'FGFR1'=> 'yes',
			'FGFR2'=> 'yes',
			'FGFR3'=> 'yes',
			'FGFR4'=> 'yes',
			'FOXL2'=> 'no',
			'GNA11'=> 'no',
			'GNAQ'=> 'no',
			'GNAS'=> 'no',
			'HRAS'=> 'yes',
			'IDH1'=> 'no',
			'IDH2'=> 'no',
			'JAK1'=> 'yes',
			'JAK2'=> 'yes',
			'JAK3'=> 'yes',
			'KIT'=> 'no',
			'KRAS'=> 'yes',
			'MAP2K1'=> 'no',
			'MAP2K2'=> 'no',
			'MET'=> 'no',
			'MTOR'=> 'yes',
			'MYC'=> 'yes',
			'MYCN'=> 'no',
			'NRAS'=> 'yes',
			'NTRK1'=> 'no',
			'NTRK2'=> 'no',
			'NTRK3'=> 'no',
			'NOTCH1' => 'no',
			'PDGFRA'=> 'no',
			'PIK3CA'=> 'yes',
			'PPARG'=> 'yes',
			'RAF1'=> 'yes',
			'RET'=> 'no',
			'ROS1'=> 'yes',
			'SMAD4'=> 'yes',
			'SMO'=> 'yes',
			'SRC'=> 'no',
			'STK11'=> 'no',
			'TP53'=> 'yes',
			'ASXL1'=> 'yes',
			'BCOR'=> 'no',
			'CSF3R' => 'no',
			'DNMT3a' => 'no',
			'ETV6' => 'yes',
			'EZH2' => 'no',
			'FLT3' => 'yes',
			'GATA2' => 'no',
			'MPL' => 'no',
			'MYD88' => 'no',
			'NPM1' => 'no',
			'PHF6' => 'no',
			'PTPN11' => 'no',
			'RUNX1' => 'no',
			'SETBP1' => 'no',
			'SF3B1' => 'no',
			'SRSF2' => 'no',
			'STAG2' => 'no',
			'TET2' => 'no',
			'U2AF1' => 'no',
			'WT1' => 'no',
			'ZRSR2' => 'ZRSR2'


			// http://cancer.sanger.ac.uk/census#cl_search
		);

	// 	public function FileHasHeader($headers, $check_file)
	// 	{
	// 		// check if file has rows which start with ## that contain all of the strings in the array $headers
	// var_dump($headers);
	// var_dump($check_file);

	// 		$of = fopen($check_file, 'r') or die('unable to open file!');

	// 		while (!feof($of) && $checked_lines < 5)
	// 		{
	// 			$line = fgets($of);

	// 			if(strpos($line, '#') !== 0 && $line != '')
	// 			{

	// 			}
	// 		}



	// 		return True;			
	// 	}
		function pingAddress($ip) 
		{
		    exec(sprintf('ping -c 1 -W 5 %s', escapeshellarg($ip)), $res, $rval);
        	
        	return $rval === 0;
		}

	function colorTdBKGRDGreen($val, $expected_val='yes')
	{
		// color a table td green if value equal to expected_val

		if ($val === $expected_val)
		{
			return '<td class="alert-passed-qc">'.$val.'</td>';
		}

		else
		{
			return '<td>'.$val.'</td>';
		}
	}
	

	function num_occurances_substring_in_array($in_arr, $string) 
	{
		// (array('usa-ny-nyc','usa-fl-ftl', 'usa-nj-b', 'usa-ny-wch', 'usa-ny-li'), 'usa-ny-') => 3

	    $count = 0;

	    foreach  ($in_arr as $v) 
	    {
	        if (strpos($v, $string)===0) 
	        {
	            ++$count;
	        }
	    }
	    return $count;

	}

	function soft_path_num_format_correct($soft_path_num)
	{
		// soft_path_number in the molecular lab should be formated as two digit num hyphen 3 letters and then a number up to 6 digits long
		// ('21-SHE727') -> True
		// ('21-SSP727') -> True
		// ('21-SHE4') -> True
		// ('21SHE727') -> False
		// ('19-SHE') -> False		
		// ('21-SH727') -> False
		// ('21ME-07') -> False
		// ('MTP-03-21') -> False

		if 	(
			strlen($soft_path_num) >= 7 && 
			preg_match("/^[0-9]{2}-[a-zA-Z]{3}\d{1,6}/", $soft_path_num)
		)
		{
			return True;
		}
		return False;
	}

	
	function convert_soft_path_num_from_lab_to_soft($soft_path_num)
	{
		// Path likes the mask (she, ssp, etc), then the 2 digit year, then the case number as 6 digits with leading zeros to fill the spaces. For example: what we log as 20-SSP63569, Path wants to read SSP20063569

		// tested on array('21-SHE727', '21SHE738', '21-SHE758', '21ME-07', 'MTP-03-21', '21-SSP61', '19-SHE', '21-SHE4', '21-SH727', '21-SHE758758', '21-SHE75', '21-SHE758', '21-SHE7587', '21-SHE75875')

		// check that the format is the molecular lab format.  If not do not proceed.
		if ($this->soft_path_num_format_correct($soft_path_num))
		{
			$two_digit_year = substr($soft_path_num, 0, 2);
			$mask = substr($soft_path_num, 3, 3);
			$nums = substr($soft_path_num, 6);
			$six_digit_num = sprintf("%06s", $nums);

			return $mask.$two_digit_year.$six_digit_num;
		}
		else
		{
			return False;
		}
	}

	function myErrorHandler($errno, $errstr, $errfile, $errline)
	{
		if (!(error_reporting() & $errno)) 
		{
			// This error code is not included in error_reporting
			return;
		}
		switch ($errno) 
		{
			case E_USER_ERROR:
			case E_ERROR:
			case E_COMPILE_ERROR:

				echo "<b>My ERROR</b> [$errno] $errstr<br />\n";
				echo "  Fatal error on line $errline in file $errfile";
				echo ", PHP " . PHP_VERSION . " (" . PHP_OS . ")<br />\n";
				echo "Aborting...<br />\n";
				// emailErrorFunction($errno,$erstr,$errfile,$errline);
				exit(1);
				break;

			default:

				var_dump($errno);
				var_dump($errstr);
				var_dump($errfile);
				var_dump($errline);
				printError($errno, $errstr, $errfile, $errline);
				echo "Unknown error type: [$errno] $errstr<br />\n";
				break;
		}

		/* Don't execute PHP internal error handler */
		return true;
	}

	function printError($errno, $errstr, $errfile, $errline)
	{
		$e = new \Exception;
		$err_trace_array = $e->getTrace();
		var_dump($err_trace_array);
?>
		<font size="1">
			<table class="xdebug-error xe-notice" dir="ltr" border="1" cellspacing="0" cellpadding="1">
				<tbody>
					<tr>
						<th align="left" bgcolor="#f57900" colspan="5"><span style="background-color: #cc0000; color: #fce94f; font-size: x-large;">( ! )</span> Notice: <?= $errstr;?> in <?= $errfile;?> on line <i><?= $errline;?></i>
						</th>
					</tr>
					<tr>
						<th align="left" bgcolor="#e9b96e" colspan="5">Call Stack</th>
					</tr>
					<tr>
						<th align="center" bgcolor="#eeeeec">#</th>
						<th align="left" bgcolor="#eeeeec">Function</th>
					</tr>
	<?php
		$email_admin_err_str = 'Notice: '.$errstr.' in '.$errfile.' on line '.$errline;
		foreach ($err_trace_array as $key => $trace)
		{
			$args_array = $trace['args'];
			$args_str = '';

			if ($trace['function'] !== 'printError' && $trace['function'] !== 'myErrorHandler')
			{
				$email_admin_err_str.='

';
				foreach ($args_array as $k => $arg)
				{
					$arg_type = gettype($arg);
					
					if ($args_str !== '')
					{
						$args_str.=', ';
					}

					if ($arg_type !== 'array')
					{
						$args_str.=$arg;
					}
					else
					{
						$args_str.='array';
					}					
				}	

				$args_str = $trace['function'].'(<span style="color:green;">'.$args_str.'</span>)';
				$email_admin_err_str = $trace['function'].'('.$email_admin_err_str.')'; 
			
	?>
					<tr>
						<td bgcolor="#eeeeec" align="center"><?= $key+1;?></td>
						<td bgcolor="#eeeeec"><?= $args_str;?></td>
					</tr>
	<?php
			}
		}
	?>					
				</tbody>
			</table>
		</font>
<?php
var_dump($email_admin_err_str);		
		$email_utils->emailAdminsProblemURL($email_admin_err_str);

	}


		public function countKeysThatStartWithStrWithSubArrayKey($in_array, $startwith_str, $sub_array_key)
		{
			// (arr_below, visit, include_in_pool) -> int 2 (num of include_in_pool)
			// array (size=18)
			  // 'ngs_panel_id' => string '1' (length=1)
			  // 'num_chips' => string '1' (length=1)
			  // 'barcodes_1' => 
			  //   array (size=1)
			  //     'reagent_list_id' => string '1' (length=1)
			  // 'library_pool_name' => string 'OFA_5-10_complex_v3_filter_updated_2020_02_14' (length=45)
			  // 'library_prep_date' => string '2020-02-14' (length=10)
			  // 'visit_0' => 
			  //   array (size=6)
			  //     'include_in_pool' => string '1' (length=1)
			  //     'visit_id' => string '194' (length=3)
			  //     'sample_name' => string 'test3_test3' (length=11)
			  //     'patient_name' => string '' (length=0)
			  //     'order_num' => string '1' (length=1)
			  //     'chip_flow_cell' => string 'A' (length=1)
			  // 'visit_1' => 
			  //   array (size=6)
			  //     'include_in_pool' => string '1' (length=1)
			  //     'visit_id' => string '211' (length=3)
			  //     'sample_name' => string '5413-19_Devils-35' (length=17)
			  //     'patient_name' => string 'Martin  Brodeur' (length=15)
			  //     'order_num' => string '2' (length=1)
			  //     'chip_flow_cell' => string 'A' (length=1)
			  // 'visit_2' => 
			  //   array (size=6)
			  //     'visit_id' => string '202' (length=3)
			  //     'sample_name' => string '5414-19_Sabers-9-softlab' (length=24)
			  //     'patient_name' => string 'Jack  Eichel' (length=12)
			  //     'order_num' => string '3' (length=1)
			  //     'chip_flow_cell' => string 'A' (length=1)

			$arr_keys = array_keys($in_array);

			$count = 0;
			foreach ($arr_keys as $i => $key)
			{

				if (preg_match("/^".$startwith_str."/", $key))
				{			

					if (is_array($in_array[$key]) && $this->countKeysThatStartWithStr($in_array[$key], $sub_array_key) === 1)
					{
						$count++;
					}
				}

			}
			return $count;
		}

		public function findIfVisitIncludedIsNTC($in_array)
		{
			// (arr_below, visit, include_in_pool) -> bool
			// If NTC found 
			// array (size=18)
			  // 'ngs_panel_id' => string '1' (length=1)
			  // 'num_chips' => string '1' (length=1)
			  // 'barcodes_1' => 
			  //   array (size=1)
			  //     'reagent_list_id' => string '1' (length=1)
			  // 'library_pool_name' => string 'OFA_5-10_complex_v3_filter_updated_2020_02_14' (length=45)
			  // 'library_prep_date' => string '2020-02-14' (length=10)
			  // 'visit_0' => 
			  //   array (size=6)
			  //     'include_in_pool' => string '1' (length=1)
			  //     'visit_id' => string '194' (length=3)
			  //     'sample_name' => string 'test3_test3' (length=11)
			  //     'patient_name' => string '' (length=0)
			  //     'order_num' => string '1' (length=1)
			  //     'chip_flow_cell' => string 'A' (length=1)
			  // 'visit_1' => 
			  //   array (size=6)
			  //     'include_in_pool' => string '1' (length=1)
			  //     'visit_id' => string '211' (length=3)
			  //     'sample_name' => string '5413-19_Devils-35' (length=17)
			  //     'patient_name' => string 'Martin  Brodeur' (length=15)
			  //     'order_num' => string '2' (length=1)
			  //     'chip_flow_cell' => string 'A' (length=1)
			  // 'visit_2' => 
			  //   array (size=6)
			  //     'visit_id' => string '202' (length=3)
			  //     'sample_name' => string '5414-19_Sabers-9-softlab' (length=24)
			  //     'patient_name' => string 'Jack  Eichel' (length=12)
			  //     'order_num' => string '3' (length=1)
			  //     'chip_flow_cell' => string 'A' (length=1)
			  // ......
			  //  'visit_19' => 
			  //   array (size=6)
			  //     'include_in_pool' => string '1' (length=1)
			  //     'visit_id' => string '288' (length=3)
			  //     'sample_name' => string 'NTC_1' (length=5)
			  //     'patient_name' => string 'NTC  NTC' (length=8)
			  //     'order_num' => string '20' (length=2)
			  //     'chip_flow_cell' => string 'B' (length=1)

			$arr_keys = array_keys($in_array);

			$ntc_added = False;
			foreach ($arr_keys as $i => $key)
			{

				if (preg_match("/^visit/", $key))
				{			

					if (is_array($in_array[$key]) && $this->countKeysThatStartWithStr($in_array[$key], 'include_in_pool') === 1)
					{
						if ($in_array[$key]['patient_name'] === 'NTC  NTC')
						{
							$ntc_added = True;
						}
					}
				}

			}
			return $ntc_added;
		}


		public function findVarsomeMutalyzerLink($conditions)
		{
			// since this template is used in multiple places ngs_panel is not always available.  Only show specific accession link if ngs_panel_id is available and only one accession number is found

			// if NGS panel is available only provide the link for that panel.  However, there is two genes that have two accession numbers.
			// SMO for OFA and BCOR for myeloid
				// | SMO    | NM_005631.4,NM_005631.4     |            1 |						
				// | BCOR   | NM_017745.5,NM_001123385.1  |            2 |


			if (isset($conditions['ngs_panel_id']))
			{
				$search_array = array(
					'ngs_panel_id'	=>	$conditions['ngs_panel_id'],
					'gene' 			=> 	$conditions['genes']
				);

				$accession_number = $conditions['db']->listAll('accession-num-present-for-ngs-panel-by-gene', $search_array);

				if (sizeOf($accession_number) == 1)
				{
					$varsome_link = 'https://varsome.com/variant/hg19/'.$accession_number[0]['accession_num'].'('.$conditions['genes'].')%3A'.$conditions['coding'];
					$mutalyzer_link = 'https://mutalyzer.nl/name-checker?description='.$accession_number[0]['accession_num'].':'.$conditions['coding'];

					return array('varsome' => $varsome_link, 'mutalyzer' => $mutalyzer_link);
				}
				else
				{	
					$mutalyzer_link = 'https://mutalyzer.nl/name-checker';
					return array('varsome' => $this->makeVarsomeLinkWithoutAccessionNum($conditions), 'mutalyzer' => $mutalyzer_link);
				}				
			}
			else
			{
				$mutalyzer_link = 'https://mutalyzer.nl/name-checker';
				return array('varsome' => $this->makeVarsomeLinkWithoutAccessionNum($conditions), 'mutalyzer' => $mutalyzer_link);
			}
		}

		public function makeVarsomeLinkWithoutAccessionNum($conditions)
		{
			return 'https://varsome.com/variant/hg19/'.$conditions['genes'].'%20'.$conditions['coding'];
		}

		public function countKeysThatStartWithStr($in_array, $startwith_str)
		{
			// array (size=18)
			  // 'ngs_panel_id' => string '1' (length=1)
			  // 'num_chips' => string '1' (length=1)
			  // 'barcodes_1' => 
			  //   array (size=1)
			  //     'reagent_list_id' => string '1' (length=1)
			  // 'library_pool_name' => string 'OFA_5-10_complex_v3_filter_updated_2020_02_14' (length=45)
			  // 'library_prep_date' => string '2020-02-14' (length=10)
			  // 'visit_0' => 
			  //   array (size=6)
			  //     'include_in_pool' => string '1' (length=1)
			  //     'visit_id' => string '194' (length=3)
			  //     'sample_name' => string 'test3_test3' (length=11)
			  //     'patient_name' => string '' (length=0)
			  //     'order_num' => string '1' (length=1)
			  //     'chip_flow_cell' => string 'A' (length=1)
			  // 'visit_1' => 
			  //   array (size=6)
			  //     'include_in_pool' => string '1' (length=1)
			  //     'visit_id' => string '211' (length=3)
			  //     'sample_name' => string '5413-19_Devils-35' (length=17)
			  //     'patient_name' => string 'Martin  Brodeur' (length=15)
			  //     'order_num' => string '2' (length=1)
			  //     'chip_flow_cell' => string 'A' (length=1)
			  // 'visit_2' => 
			  //   array (size=6)
			  //     'include_in_pool' => string '1' (length=1)
			  //     'visit_id' => string '202' (length=3)
			  //     'sample_name' => string '5414-19_Sabers-9-softlab' (length=24)
			  //     'patient_name' => string 'Jack  Eichel' (length=12)
			  //     'order_num' => string '3' (length=1)
			  //     'chip_flow_cell' => string 'A' (length=1)

			$arr_keys = array_keys($in_array);

			$count = 0;
			foreach ($arr_keys as $i => $key)
			{

				if (preg_match("/^".$startwith_str."/", $key))
				{				
					$count++;
				}

			}
			return $count;
		}

		public function returnFileContents($file)
		{
			// Open file and read if it exists otherwise return False 		

			if (file_exists($file))
			{
				$of = fopen($file, 'r') or die('Unable to open file!');
				
				$file_contents = fread($of, filesize($file));

				fclose($of);	

				return $file_contents;
			}
			else
			{
				return False;
			}					
		}

		public function return_last_line_file($in_file, $header_present=True)
		{
			// return the last line of a file.  
			// (file address, bool) -> array(line_count => int, last_line => array/bool/str)

			if (!file_exists($in_file))
			{
				return array('line_count' => 0, 'last_line' => False);
			}

			$of = fopen($in_file, 'r') or die('unable to open file!');

			$line = rtrim(fgets($of));
			$count = 1;
			if ($header_present)
			{
				$header = explode("\t", $line);
				$line = rtrim(fgets($of));
				$count++;
			}

			// get last line of file
			while(!feof($of))
			{
				$prevline = $line;
				$line = rtrim(fgets($of));
				
				// ignore empty lines.
				if (empty($line))
				{
					$line = $prevline;
					break;
				}
				else
				{
					$count++;
				}
			}
			
			fclose($of);

			if ($header_present)
			{
				$split_line = explode("\t", $line);
				return array('line_count' => $count, 'last_line' => $this->array_combine($header, $split_line));
			}
			else
			{
				return array('line_count' => $count, 'last_line' => $line);
			}			
		}

		public function arrayToCharSeperated($in_array, $char, $newline=True)
		{
			
			// (array, str, bool) -> str
			// Purpose:  Accept an array and return a char separated str

			// Newline is turned to False the list char will not be a newline

			// >>> list_to_char_seperated((0 => 'hello', 1 => 'world'], ' ', True) 
			// 'hello world\n'
			// >>> list_to_char_seperated((0 => 1, 1 => 'world'), '\t', False) 
			// '1\tworld'
						
			$char_line = "";
			$len_array = sizeOf($in_array);

			for ($i=0; $i < $len_array; $i++)
			{
				if ($i === $len_array -1 && !$newline)
				{
					$char_line.= $in_array[$i];
				}
				else if ($i === $len_array -1 && $newline)
				{
					$char_line.= $in_array[$i]."\n";
				}
				else
				{
					$char_line.= $in_array[$i];

					if ($char === '\t')
					{
						$char_line.="\t";
					}
					else 
					{
						$char_line.=$char;
					}
				}
			} 
			return $char_line;
		}

		public function allGetsInStr($array_ignore_values=array())
		{
			// Return all values in the $_GET array ignoring key names in $array_ignore_values
			// $_GET =
				// array (size=5)
				//   'page' => string 'show_edit_report' (length=16)
				//   'run_id' => string '247' (length=3)
				//   'visit_id' => string '239' (length=3)
				//   'patient_id' => string '171' (length=3)
				//   'ngs_panel_id' => string '1' (length=1))
			// $array_ignore_values = array('page');
			// return run_id=247&visit_id=239&patient_id=171&ngs_panel_id=1

			$return_str = '';

			foreach ($_GET as $val_name => $get_val)
			{
				if (in_array($val_name, $array_ignore_values))
				{
					continue;
				}

				if (!empty($return_str))
				{
					$return_str.='&';
				}

				$return_str.=$val_name.'='.$get_val;
			}

			return $return_str;
		}

		public function nestedArrayToCharSeperated($in_array, $char, $nested_key, $newline=True)
		{
			
			// (array, str, bool) -> str
			// Purpose:  Accept an array and return a char separated str

			// Newline is turned to False the list char will not be a newline

			// $in_array = array (size=2)
			//   0 => 
			//     array (size=1)
			//       'ordered_test_genetic_call' => string 'BAT-25' (length=6)
			//   1 => 
			//     array (size=1)
			//       'ordered_test_genetic_call' => string 'BAT-26' (length=6)
						
			$char_line = "";
			$len_array = sizeOf($in_array);

			for ($i=0; $i < $len_array; $i++)
			{
				if ($i === $len_array -1 && !$newline)
				{
					$char_line.= $in_array[$i][$nested_key];
				}
				else if ($i === $len_array -1 && $newline)
				{
					$char_line.= $in_array[$i][$nested_key]."<br>";
				}
				else
				{
					$char_line.= $in_array[$i][$nested_key];

					if ($char === '\t')
					{
						$char_line.="\t";
					}
					else 
					{
						$char_line.=$char;
					}
				}
			} 
			return $char_line;
		}


		public function validateGetInt($get_name)
		{
			// Make sure a Get parameter is not malicious.
			// Make sure isset, not empty, and an integer
			// (str) -> bool


			if (isset($_GET[$get_name]) && !empty($_GET[$get_name]) && is_numeric($_GET[$get_name]))
			{
				return True;
			}
			else
			{
				return False;
			}
		}

		public function validateGetStr()
		{
			// Make sure a Get parameter is not malicious.
			// Make sure isset, not empty, does not contain a blackListFileType
			// does not contain a blackListCharacters
			// (str) -> bool
		}

		public function validatePageGet()
		{
			// Make sure a page is a validateGetStr and is an available page
		}

		public function validateDateGet()
		{
			// Make sure a Get parameter is not malicious.
			// Make sure isset, not empty, and is a date
			// (str) -> bool
		}

		public function binaryPresent($check_str)
		{
			// check if string contains non-printable characters which is
			// most likely binary
			// (str) -> bool

			// A hacky solution would be to search for NUL \0 chars.
			if (strpos($check_str, '\0') !== False)
			{
				return True;
			}
			
			else
			{
				return False;
			}

		}

		public function BlackListCharacters($check_str)
		{
			// This function will make sure the check_str does not contain any
			// characters which are blacklisted.  Black listed characters
			// could be added because they are malicious. 
			// All the control characters and Unicode ones should be removed from the filenames and their extensions without any exception. Also, the special characters such as ";", ":", ">", "<", "/" ,"\", additional ".", "*", "%", "$", and so on should be discarded as well. 
			// (':abc') -> False
			// ('abc>') -> False
			// ('..') -> False
			// (file.asax:.jpg) -> False

			$forbidden_chars = array(':', ';', '>', '<', '$', '..', '*', '?', '|', '"', "'", '%', '`');
		}

		public function get_files_with_extension($dir, $extension)
		{
			$files = array();
			
			foreach (glob($dir.'*.'.$extension) as $file) 
			{
				$files[] = $file;
			}

			return $files;
		}

		function uploadMultipleFiles($ref_id, $ref_table, $file_array, $save_folder_name, $md5_hash_post_array)
		{
			// Upload multiple files to SERVER_SAVE_LOC.  Checking that they 
			// transfered completely.
			$return_array = array();

			foreach ($file_array['name'] as $key => $filename)
			{
				//////////////////////////////////////////////////////
				// Move file to server in a folder associated with save_folder_name and ROOT_URL
				//////////////////////////////////////////////////////
				$save_folder = SERVER_SAVE_LOC.$save_folder_name.'/'.ROOT_URL;

				if (!file_exists($save_folder))
				{
					mkdir($save_folder);
				}

				$save_folder.= '/'.$ref_id;
				
				if (!file_exists($save_folder))
				{
					mkdir($save_folder);
				}

				// Move file to server
				$destination = $save_folder.'/'.$filename;
				$temp_file = $file_array['tmp_name'][$key];

				// make sure not to replace a file.
				if (file_exists($destination))
				{
					if (!isset($message))
					{
						$return_array[$key] = array('message' => 'The following file(s) are already uploaded.','file_name' => $filename);						
					}
				}

				else if (move_uploaded_file($temp_file, $destination))
				{
					//////////////////////////////////////////////////////
					// Check if file transfered correctly
					//////////////////////////////////////////////////////

					// get md5 hash of file moved to server
					$server_md5 = md5_file($destination);

					// Check md5 hash against hashes made in javascript 
					if ($server_md5 === $md5_hash_post_array[$filename])
					{
						///////////////////////////////////////////////////
						// Return document to either add info to  instrument_validation_doc_table or doc_table  
						///////////////////////////////////////////////////
						$return_array[$key] = array(
							'ref_id' 				=> 	$ref_id,
							'user_id'				=> 	USER_ID,
							'file_loc'			=> 	$destination,
							'name_uploaded_as'		=> 	$filename,
							'md5_hash'			=> 	$server_md5,
							'file_size'			=> 	$file_array['size'][$key]
						);
					}
					else
					{
						$return_array[$key] =  array(
								'admin_message' =>	 __FILE__.' Error uploading file!! MD5 hashes do not match!! Line #: '.__LINE__.' $server_md5: '.$server_md5.' $md5_hash_post_array[$filename]: '.$md5_hash_post_array[$filename],
								'message'		=> 	'File upload problem: '.$filename
						);
					}					
				}
				else
				{
					$return_array[$key] = array(
						'admin_message' => __FILE__.' Error uploading validation file!! Line #: '.__LINE__.' $destination: '.$destination.' $temp_file: '.$temp_file,
						'message'		=> 	'File upload problem: '.$filename

					);
				}
			}
			
			return $return_array;

		}

		public function BlackListFileTypes($file_array)
		{
			// Check if file type is a type which is black listed from upload
			// These file types can be used for attacks on the server or program
			// File types and why?
				// Upload .jsp file into web tree - jsp code executed as the web user
				// Upload .gif file to be resized - image library flaw exploited
				// Upload huge files - file space denial of service
				// Upload file using malicious path or name - overwrite a critical file
				// Upload file containing personal data - other users access it
				// Upload file containing "tags" - tags get executed as part of being "included" in a web page
				// Upload .rar file to be scanned by antivirus - command executed on a server running the vulnerable antivirus software
				// Upload .exe file into web tree - victims download trojaned executable
				// Upload .html file containing script - victim experiences Cross-site Scripting (XSS)
				// Upload .jpg file containing a Flash object - victim experiences Cross-site Content Hijacking
				// Finding missed extensions that can be executed on the server side or can be dangerous on the client side (e.g. ".php5", ".pht", ".phtml", ".shtml", ".asa", ".cer", ".asax", ".swf", or ".xap").
				// The list of permitted extensions should be reviewed as it can contain malicious extensions as well. For instance, in case of having ".shtml" in the list, the application can be vulnerable to SSI attacks.
			// refs
				// https://www.owasp.org/index.php/Unrestricted_File_Upload
				// https://cheatsheetseries.owasp.org/
			$blocked_file_types = array('jsp', 'gif', 'rar', 'exe', 'dmg', 'html', 'jpg', 'pht', 'phtml', 'shtml', 'asa', 'cer', 'asax', 'swf', 'xap', 'php', 'xml', 'asp', 'php3', 'php5', 'php7');

			foreach ($file_array['name'] as $key => $filename)
			{
				/////////////////////////////////////////////////////////////////
				// Make sure files do not have two extensions (ex. image.jpg.php, .., ...php)
				/////////////////////////////////////////////////////////////////
				if (substr_count($filename, '.') > 1)
				{
					return False;
				}

				/////////////////////////////////////////////////////////////////
				// Make sure file type not a blocked type
				// blocked examples (file.aSp, file.asp, test.php)
				/////////////////////////////////////////////////////////////////
				$ext = strtolower(pathinfo($filename, PATHINFO_EXTENSION));
					
				if (in_array($ext,$blocked_file_types))
				{
					return False;								
				}
			}

			return True;
		}

		public function allowedFileTypes($file_array, $allowed_types_array)
		{
			foreach ($file_array['name'] as $key => $filename)
			{
				$ext = strtolower(pathinfo($filename, PATHINFO_EXTENSION));
					
				if (in_array($ext,$allowed_types_array))
				{
					return True;								
				}
			}

			return False;
		}

		public function CheckSecurityOfFile($file_array, $allowed_types_array='skip')
		{
			// check all files in file array for a malicious file upload
			// phpMussel is automatically activated to check all uploads 
			// and performs a virus scan.
			// (array) -> bool
			// in_array
				// allowed_types_array
			// refs
				// https://www.computerweekly.com/answer/File-upload-security-best-practices-Block-a-malicious-file-upload

			/////////////////////////////////////////////////////////////////
			// check if file is part of the Black list file types
			/////////////////////////////////////////////////////////////////
			if (!$this->BlackListFileTypes($file_array))
			{			
				return False;
			}
			
			///////////////////////////////////////////////////////////////
			// if allowed_types_array is present check that file type is allowed
			// if it isn't present use default allowed_types_array
			///////////////////////////////////////////////////////////////
			if ($allowed_types_array !== 'skip' && !$this->allowedFileTypes($file_array,$allowed_types_array))
			{
				return False;
			}	

			return True;		
		}

		public function textareaEmpty($textArea)
		{
			if (trim($textArea) == '' && strlen(trim($textArea)) == 0)
			{
				return True;
			}
			else
			{
				return False;
			}
		}

		public function GetValueForUpdateInput($in_array, $val_name, $default_val='')
		{
			// (array, str, str/int/date) -> str
			// when updating forms inputs previous values need to be entered into form.  
			// This function return the value for input

			if (isset($_POST[$val_name]) && !empty($_POST[$val_name]))
			{
				return $_POST[$val_name];
			}

			else if (isset($in_array[0][$val_name]))
			{
				return strval($in_array[0][$val_name]);
			}
			else
			{
				return $default_val;
			}

		}

		public function techCheckbox($ckbx_name, $input_array, $settings_array=False)
		{
			// (str, array) -> check box html
			// if settings_array is not supplied a general required check box will be returned
			// $settings_array example
			// {
			// 	'required-linked-fields-status' => True,
			//   'required_linked_field'  => extraction_aliquoted_date,
			//	'required' => False
			// }

			if (!$settings_array)
			{
				$settings_array = array(
					'required-linked-field-status' => False,
			  		'required_linked_field'  => False,
					'required' 			=> True,
					'column-count'			=> 1
				);
			}

			$ckbx_html = '';

			if (isset($settings_array['column-count']) && $settings_array['column-count'] !== 1)
			{
				$ckbx_html.= '<ul style="column-count:'.$settings_array['column-count'].';column-gap:2rem;list-style:none;">';
			}
			
			foreach ($input_array as $key => $user)
			{		

				if (isset($settings_array['column-count']) && $settings_array['column-count'] !== 1)
				{
					$ckbx_html.= '<li>';
				}

				$ckbx_html.='<div class="form-check">';
				
				$ckbx_html.='<input type="checkbox" ';

				if ($settings_array['required-linked-field-status'])
				{
					$ckbx_html.=' class="required-linked-fields" data-required_linked_field="'.$settings_array['required_linked_field'].'" ';
				}
				

				// if a settings_array was supplied add class to activate making a field required if this check box is selected.

				$ckbx_html.='value="'.$user['user_id'].'" name="'.$ckbx_name.'" ';

				if ($settings_array['required'])
				{
					$ckbx_html.='" required="required"';
				}

				$ckbx_html.='>';
				$ckbx_html.='<label class="checkbox">';
				

				// highlight current users name
				if ($user['user_id'] === USER_ID)
				{

					$ckbx_html.='<span class="yellow_background">';
							
				}
	
				$ckbx_html.=$user['first_name'].' '.$user['last_name'].' ('.$user['initials'].')';
							
				if ($user['user_id'] === USER_ID)
				{
							
					$ckbx_html.='</span>';
							
				}
	
				$ckbx_html.='</label>';
				$ckbx_html.='</div>';
				if (isset($settings_array['column-count']) && $settings_array['column-count'] !== 1)
				{
					$ckbx_html.= '</li>';
				}
			}

			if (isset($settings_array['column-count']) && $settings_array['column-count'] !== 1)
			{
				$ckbx_html.= '</ul>';
			}

			return $ckbx_html;			
		}

		public function instrumentInputName($instrument)
		{
			// array is obtained by iterating over an array produced from listAll 'instrument-groups-description'

			$logo = $this->ManufacturerLogo($instrument['manufacturer']);
			
			$input = '';

			// find if a manufacturerlogo exisits
			if ($logo)
			{
				$input.= '<img src="'.$logo.'" height="30px;">';	
			}					
			else
			{
				$input.= $instrument['manufacturer'];
			}
								
			$input.= ' '.$instrument['model']; 
								
			$icon = $this->InstrumentDescriptionIcon($instrument['description']);
			
			if ($icon)
			{
				$input.= ' '.$instrument['description'].' <img src="'.$icon.'" height="30px;">';
			}
			else
			{
				$input.= ' '.$instrument['description'];
			}
			
			$input.= ' <span class="yellow_background">Yellow Tag: '.$instrument['yellow_tag_num'].'</span>';

			if (!empty($instrument['serial_num']))
			{
				$input.= ' Serial#:'.$instrument['serial_num'];
			}

			if (!empty($instrument['clinical_engineering_num']))
			{
				$input.= ' Clin Eng#:'.$instrument['clinical_engineering_num'];
			}
			return $input;
		}

		public function ManufacturerLogo($manufacturer)
		{
			switch ($manufacturer) 
			{
				case 'Applied Biosystems':
					return 'images/ABI_logo.png';
					break;

				case 'Eppendorf':
					return 'images/logoEppendorf.png';
					break;
				
				case 'Roche':
					return 'images/roche_icon.png';
					break;

				case 'Beckman':
					return 'images/beckman.jpg';
					break;

				default:
					return False;
					break;
			}
		}

		public function InstrumentDescriptionIcon($description)
		{
			switch ($description) 
			{
				case 'Mars':
					return 'images/mars-planet-png-8.png';
					break;
				case 'Venus':
					return 'images/venus.jpg';
					break;
				case 'Pluto':
					return 'images/pluto.jpg';
					break;			
				default:
					return False;
					break;
			}
		}

		public function GetValueForUpdateSelect($in_array, $val_name, $expected_val)
		{
			// (array, str, str/int/date) -> str
			// when updating forms selects previous values need to be selected in the form.  
			// This function return selected if value equals expected value equals 

			if (isset($_POST[$val_name]) && !empty($_POST[$val_name]) && $_POST[$val_name] == strval($expected_val))
			{
				return 'selected';
			}


			else if (isset($in_array[0][$val_name]) && strval($in_array[0][$val_name]) == strval($expected_val))
			{
				return 'selected';
			}

			else if (isset($in_array[$val_name]) && strval($in_array[$val_name]) == strval($expected_val))
			{
				return 'selected';
			}
			else
			{
				return '';
			}
		}

		public function getStatusDotColor($status)
		{
			if ($status === 'pending')
			{
				return '<span class="dot dot-right dot-yellow" title="Test Pending"></span>';
			}

			else if ($status === 'complete')
			{
				return '<span class="dot dot-right dot-green" title="Test Complete"></span>';
			}
			else if ($status === 'stop')
			{
				return '<span class="dot dot-right dot-red" title="Test Canceled"></span>';
			}
			else if ($status === 'revising')
			{
				return '<span class="dot dot-right dot-blue" title="Revising Report"></span>';
			}
			else if ($status === 'waiting')
			{
				return '<span class="dot dot-right dot-purple" title="Test Waiting/On Hold"></span>';
			}
			else if ($status === 'Sent Out')
			{
				return '<span class="dot dot-right dot-grey" title="Test Sent Out"></span>';
			}
			else if ($status === 'In Progress')
			{
				return '<span class="dot dot-right dot-yellow-green-gradient" title="Test in progress"></span>';
			}
			return '';
		}

		public function GetValueForUpdateRadioCBX($in_array, $val_name, $expected_val)
		{
			// (array, str, str/int/date) -> str
			// when updating forms radio buttons or cbx previous values need to be checked in the form.  
			// This function return checked if value equals expected value equals 
			// LIMIATATION ONLY WORKS IF ONE CBX IS CHECKED USE GetValueForUpdateCBX INSTEAD

			if (isset($_POST[$val_name]) && !empty($_POST[$val_name]) && $_POST[$val_name] == strval($expected_val))
			{
				return 'checked';
			}

			else if (isset($in_array[0][$val_name]) && strval($in_array[0][$val_name]) == strval($expected_val))
			{
				return 'checked';
			}

			else if (isset($in_array[$val_name]) && strval($in_array[$val_name]) == strval($expected_val))
			{
				return 'checked';
			}
			else
			{
				return '';
			}

		}

		public function DefaultRadioBtn($in_array, $val_name, $expected_val)
		{
			// (array, str, str/int/date) -> str
			// Return checked for two reasons if expected_val is found or if the the val_name is not found or empty since this is the default radio button option

			// expected value found
			if (isset($in_array[0][$val_name]) && strval($in_array[0][$val_name]) == strval($expected_val))
			{
				return 'checked';
			} 

			// No value found
			else if (!isset($in_array[0][$val_name]) || empty($in_array[0][$val_name]))
			{
				return 'checked';
			}

			// a Value besides expected value found
			else
			{
				return '';
			}
		}

		public function mean($input_array)
		{
			// $arr = array of all values to take mean
			$total = 0;
			
			foreach ($input_array as $value)
			{
				$total += $value;
			}
			
			return ($total / count($input_array));
		}

		public function standard_deviation($arr)
		{
			// $arr = array of all values to take standard Deviation over

			if (!count($arr))
			{
				return 0;
			}
			
			$mean = $this->mean($arr);
			$sos = 0; // Sum of squares
			
			for ($i = 0; $i < count($arr); $i++)
			{
				$sos += ($arr[$i] - $mean) * ($arr[$i] - $mean);
			}
			
			return sqrt($sos / (count($arr) - 1));
		}

		public function z_score($var, $arr)
		{
			// Z-score Number of standard deviations a given data point ($var) lies from the mean.  

			return ($var - $this->mean($arr)) / $this->standard_deviation($arr);
		}

		public function toggleMoreLess($cell_val, $col_name, $row_id)
		{
			// Takes a string and strings larger than 20 length will be returned 
			// with a toggle teaser.  Needs to be activated in JavaScript.
			// Entire cell is returned. place in a row in a table

	          ////////////////////////////////////////////////////
	          // Larger strings need to be toggled so more information
	          // can fit in smaller places
	          ////////////////////////////////////////////////////
               if (strlen($cell_val) >11)
               {
                                   
                    // get the first 20 char of string
                    $substr_cell_val = substr($cell_val, 0, 11);

                    $end_id = $col_name.'_'.$row_id;

                    $td = '<td>';
                    	$td.= '<span id="teaser_'.$end_id.'" class="teaser">'.$substr_cell_val.'</span>';
                    	$td.= '<span id="complete_'.$end_id.'" class="complete d-none">'.$cell_val.'</span>';
                         $td.= '<br><span id="show_complete_'.$end_id.'" class="show-complete">more ...</span>';    
                        	$td.= '<span id="show_teaser_'.$end_id.'" class="show-teaser d-none">less ...</span>'; 
                    $td.= '</td>';           

                    return $td;
               }

               ////////////////////////////////////////////////////
               // Smaller strings can just be added directly to the cell
               ////////////////////////////////////////////////////
               else
               {
               	return '<td>'.$cell_val.'</td>';
               }
		}

		public function toggleDropDownMenu($cell_val, $col_name, $row_id, $dropDownHTML)
		{
			// (str, str, int, html str) -> html str
			// This function will make the html to toggle a dropdown menu.
			// It will have to be activated in javascript.		
               $end_id = $col_name.'_'.$row_id;

               $td = '<td>';
               	$td.= '<span id="teaser_'.$end_id.'" class="teaser">'.$cell_val.'</span>';
               	$td.= '<span id="complete_'.$end_id.'" class="complete d-none">'.$cell_val.'<br><span>'.$dropDownHTML.'</span></span>';
                    $td.= '<br><span id="show_complete_'.$end_id.'" class="show-complete"><span class="fas fa-plus"></span></span>';    
                   	$td.= '<span id="show_teaser_'.$end_id.'" class="show-teaser d-none"><span class="fas fa-minus"></span></span>'; 
               $td.= '</td>';

               return $td;
		}

		public function readExcelRow($row)
		{
			// Rows from phpexcel seem to be split between two entries in a row
			// combine into one string and then split into array.
			// 1 => 
				//   array (size=2)
				//     'A' => string 'contig_id	contig_srt	contig_end	region_id	attributes	gc_count	overlaps	fwd_e2e	rev_e2e	total_reads	fwd_reads	rev_reads	cov20x	cov100x	cov500x' (length=141)
				//     'B' => null
			// 11 => 
				// array (size=2)
				//  'A' => string 'chr7	55227951	55228057	ON_EGFR_2A	GENE_ID=EGFR;PURPOSE=CNV' (length=58)
				//  'B' => string 'Hotspot;CNV_ID=EGFR;CNV_HS=1	41	937	277	319	937	402	535	107	107	107' (length=67)
			// $elems = array();

			$elems = array();
			foreach ($row as $key => $row_elem)
			{
				$curr_elems = explode("\t",$row_elem);

				if (!empty($row_elem))
				{
					$elems = array_merge($elems, $curr_elems);				
				}
			}

			return $elems;
		}

		public function generateCaptcha($captcha_file)
		{
			$captcha = $this->randomStr(8, False);
			session_start();
			$_SESSION['code'] = $captcha;
			session_write_close(); 

			$font_size = 60;
			$img_width = 500;
			$img_height = 100;

			$image = imagecreate($img_width, $img_height); // create background image with dimensions
			imagecolorallocate($image, 255, 255, 255); // set background color
			$text_color = imagecolorallocate($image, 255, 0, 0); // set captcha text color

			imagettftext($image, $font_size, 0, 30, 80, $text_color, 'fonts/V-de-vacia-deFharo.ttf', $captcha);
		
			imagejpeg($image, $captcha_file);
		}

		public function strandBiasCellClass($sb)
		{
			$sb = round($sb,4);
                    
               // strand bias color
               if ($sb == 0)
               {
                    return array(
                    	'sb_class'	=>	'alert-na-qc',
                    	'sb'			=>	'QC not done'
                    );
               }

               else if ($sb > 4.0)
               {
                    return array(
                    	'sb_class'	=>	'alert-flagged-qc',
                    	'sb'			=>	$sb
                    );
               }                        
               else
               {
                    return array(
                    	'sb_class'	=>	'alert-passed-qc',
                    	'sb'			=>	$sb,
                    );
               }
		}

		public function strandOddsRatio($allele_counts)
		{
			// https://gatkforums.broadinstitute.org/gatk/discussion/5533/strandoddsratio-computation
 
			// https://gatkforums.broadinstitute.org/gatk/discussion/9316/how-to-calculate-annotation-sor
			
			// https://software.broadinstitute.org/gatk/documentation/tooldocs/3.8-0/org_broadinstitute_gatk_tools_walkers_annotator_StrandOddsRatio.php
			// Pasted
			// The dev of strandOddsRatio recommends throwing out SNPs with SOR > 4.0 and indels with SOR > 10.0. These are meant to be conservative, in line with the rest of the recommendations. For more stringency, you'll want to lower the values.

			// calculate strandOddsRatio
			// a = ref_pos	+ 1
			// b = ref_neg + 1
			// c = variant_pos + 1
			// d = variant_neg + 1

			// R = (a * d ) / ( b * c )
			// sum_R = ( R + ( 1 / R ) )
			// ref_ratio = ( min { a , b } ) / (max { a, b })
			// alt_ratio = ( min { c , d } ) / (max { c , d })
			// scale_factor = ( ref_ratio / alt_ratio )
			// scaled_sor = ( sum_R * scale_factor )
			// LN_scaled = (ln(scaled_sor)) 

			if (isset($allele_counts['ref_pos']) && isset($allele_counts['ref_neg']) && isset($allele_counts['variant_neg']) && isset($allele_counts['variant_pos']) && !empty($allele_counts['ref_neg']) && !empty($allele_counts['ref_pos']) && !empty($allele_counts['variant_neg']) && !empty($allele_counts['variant_pos']))
			{
				$a = (int)$allele_counts['ref_pos'] + 1;	
				$b = (int)$allele_counts['ref_neg']  + 1;
				$c = (int)$allele_counts['variant_pos'] + 1;
				$d = (int)$allele_counts['variant_neg'] + 1;

				$R = ($a * $d ) / ( $b * $c );

				$sum_R = ( $R + ( 1 / $R ) );

				$ref_ratio = ( min ($a , $b) ) / (max($a, $b));
				$alt_ratio = ( min ($c , $d) ) / (max($c , $d));
				$scale_factor = ( $ref_ratio / $alt_ratio );
				$scaled_sor = ( $sum_R * $scale_factor );
				$LN_scaled = (log($scaled_sor));

				$allele_counts['p'] = $scaled_sor;
				$allele_counts['strand_bias'] = $LN_scaled;

				///////////////////////////////////////////////////////////
				// Add status of strand bias
				// greater than 4 flagged
				// need to add later indel cut off LN_scaled > 10.0 flagged
				///////////////////////////////////////////////////////////

				if ($LN_scaled > 4.0)
				{
					$allele_counts['status'] = 'flagged';
				}
				else
				{
					$allele_counts['status'] = 'passed';
				}
			}
			else
			{
				$allele_counts['p'] = '';
				$allele_counts['strand_bias'] = '';
				$allele_counts['status'] = 'na';
			}

			return $allele_counts;
		}	

		public function strandBias($allele_counts)
		{
			// calculate strandBias
			// a = ref_pos	
			// b = ref_neg
			// c = variant_neg
			// d = variant_pos

			if (isset($allele_counts['ref_pos']) && isset($allele_counts['ref_neg']) && isset($allele_counts['variant_neg']) && isset($allele_counts['variant_pos']) && !empty($allele_counts['ref_neg']) && !empty($allele_counts['ref_pos']) && !empty($allele_counts['variant_neg']) && !empty($allele_counts['variant_pos']))
			{
				$a = (int)$allele_counts['ref_pos'];	
				$b = (int)$allele_counts['ref_neg'];
				$c = (int)$allele_counts['variant_neg'];
				$d = (int)$allele_counts['variant_pos'];

				// ref_ratio = b/(a+b) 
				$ref_ratio = $b/($a+$b);
				
				// variant_ratio = d/(c+d)	
				$variant_ratio = $d/($c + $d);
				
				// abs_var = abs(ref_ratio - variant_ratio)	
				$abs_var = abs($ref_ratio - $variant_ratio);
			
				// $every_val_ratio = (b+d)/(a+b+c+d)	
				$every_val_ratio = ($b+$d) / ($a+$b+$c+$d);
							
				// test statistic P = (abs_var) / $every_val_ratio
				$p = $abs_var / $every_val_ratio;
				$allele_counts['p'] = $p;

				// strand bias = 10*log10(P)
				$strand_bias = 10 * log10($p);
				$allele_counts['strand_bias'] = $strand_bias;
			}
			else
			{
				$allele_counts['p'] = '';
				$allele_counts['strand_bias'] = '';

			}

			return $allele_counts;

		}

		public function addEditableTableCell($table_val, $col_name, $id, $id2_nam=False, $id2=False)
		{
			//

			$cell = '<div class="row_data" edit_type="click" col_name="';
			$cell.= $col_name.'" id="'.$col_name.'_'.$id.'"';

			if ($id2_nam != False && $id2 != False)
			{
				$cell.= $id2_nam.'="'.$id2.'">';
			}
			else
			{
				$cell.='>';
			}

			if (empty($table_val))
			{
				$cell.='__';
			}
			else
			{
				$cell.=$table_val;
			}
			
			$cell.='</div>';

			return $cell;
		}

		public function getVirtualImageSRC($img_file)
		{
			// This function will read an image file and produce a src to include in an src
			// <img src="">
			// This is useful for files outside of the web directory but also useful to d-none
			// location of file. 
			// https://stackoverflow.com/questions/4286677/show-image-using-file-get-contents

			$img_data = base64_encode(file_get_contents($img_file));
			$src = 'data: '.mime_content_type($img_file).';base64,'.$img_data;
			return $src;
		}


		public function countInDir($dir)
		{
			// Count the number of files or directories in a folder

			// get everything inside the directory.  This will return . and ..
			// example of empty dir 
				// 0 => string '.' (length=1)
				// 1 => string '..' (length=2)
			$dir_contents = scandir($dir);
			
			return count($dir_contents)-2;
		}

		public function getAllFilesInDir($dir)
		{
			// Get only files in dir ignoring . and ..
			
			// get everything in a dir
			$dir_contents = scandir($dir);

			$result = array();

			foreach ($dir_contents as $key => $content)
			{
				// skip . .. files and skip directories
				if (!in_array($content, array('.', '..')) && !is_dir($dir.DIRECTORY_SEPARATOR.$content))
				{
					// add file name only.
					$result[$key] = $content;
				}
			}

			return $result;
		}

		public function randomStr($len_str, $withSpecialChrs=True)
		{
			if ($withSpecialChrs)
			{
				$possible_chrs = 'abcdefghijklmnopqrstuvwxyz'
			.'ABCDEFGHIJKLMNOPQRSTUVWXYZ'.'0123456789!@#$%&*';
			}
			else
			{
				$possible_chrs = 'abdefghijkmnqrstxyz'.'ABDEFGHIJKLMNQRSTUVWXYZ'.'123456789!@#$%&*';
			}


			$seed = str_split(
			$possible_chrs); // and any other characters
			shuffle($seed); // probably optional since array_is randomized; this may be redundant
			$rand = '';
			foreach (array_rand($seed, $len_str) as $k) 
			{
				$rand .= $seed[$k];
			}
			return $rand;
		}

		public function possibleAlternateCoding($coding)
		{
			// Sometimes the input of the coding is not the same as cosmic
			// check if coding meets a regex which should be checked
			// For example:
				// c.1900_1922delAGAGAGGCGGCCACCACTGCCAT found in db is c.1900_1922del23
			
			////////////////////////////////////////////////////////////
			// find if variant includes a del followed by letters
			////////////////////////////////////////////////////////////
			$regex = '(del[ATCGNatcgn]{6,})';
			preg_match($regex, $coding, $dels);

			if (!empty($dels))
			{
				$num_nucleotides = strval(strlen($dels[0])-3);
				return str_replace($dels[0], 'del'.$num_nucleotides, $coding);
			}



			else
			{
				return False;
			}
		}

		public function varSetNotEmpty($var)
		{
			// check if a variable is set and it is not empty

			if (isset($var) && !empty($var))
			{				
				return true;
			}
			else
			{
				return false;
			}
		}

		public function array_combine($key_array, $value_array)
		{
			// combines two arrays making into key value pairs.
			// If key_array is longer than value array empty string 
			// is included as value.`
			// >>> (['A', 'B', 'C'], ['D', 'E']) -> 
			// { 	
			// 		'A' 	=> 	'D',
			//		'B' 	=> 	'E',
			// 		'C' 	=> 	''
			// }
			$combined = array();

			foreach ($key_array as $i => $key)
			{
				$combined[$key] = isset($value_array[$i]) ? $value_array[$i] :'';
			}

			return $combined;			
		}
		
		public function has_prefix($string, $prefix) 
		{	
			return ((substr($string, 0, strlen($prefix)) == $prefix) ? true : false);
		}
		public function convert_AA_to_long_hand($aa)
		{
			// purpose: Convert short amino acid nomenclature to long form
			// (p.F751fs*28) -> p.Phe751fsTer28

			$aa_table = array(
				'A' => 'Ala', 
				'C' => 'Cys', 
				'D' => 'Asp', 
				'E' => 'Glu', 
				'F' => 'Phe', 
				'G' => 'Gly', 
				'H' => 'His', 
				'I' => 'Ile', 
				'K' => 'Lys', 
				'L' => 'Leu', 
				'M' => 'Met', 
				'N' => 'Asn', 
				'P' => 'Pro', 
				'Q' => 'Gln', 
				'R' => 'Arg', 
				'S' => 'Ser', 
				'T' => 'Thr', 
				'V' => 'Val', 
				'W' => 'Trp',
				'Y' => 'Tyr', 
				'*' => 'Ter'
			);
			
			// iterate over the entire short hand AA
			for ($i=0; $i < strlen($aa); $i++)
			{
				// if the current position in short hand AA is in the $aa_table as a key
				// it needs to be updated to long hand
				if (array_key_exists($aa[$i], $aa_table))
				{
					$long_aa = $aa_table[$aa[$i]];
					$aa = substr_replace($aa, $long_aa, $i, 1);				
				}
			}
			return $aa;			
		}	

		public function convert_AA_to_short_hand($aa)
		{
			$aa_table = array(
				'Ala'=> 'A',
				'Cys'=> 'C',
				'Asp'=> 'D',
				'Glu'=> 'E',
				'Phe'=> 'F',
				'Gly'=> 'G',
				'His'=> 'H',
				'Ile'=> 'I',
				'Lys'=> 'K',
				'Leu'=> 'L',
				'Met'=> 'M',
				'Asn'=> 'N',
				'Pro'=> 'P',
				'Gln'=> 'Q',
				'Arg'=> 'R',
				'Ser'=> 'S',
				'Thr'=> 'T',
				'Val'=> 'V',
				'Trp'=> 'W',
				'Tyr'=> 'Y',
				'Ter'=> '*'
			);
			
			foreach ($aa_table as $key => $val)
			{
				if (strpos($aa, $key) !== false)
				{
					$aa = str_replace($key, $val, $aa);
				}
			}
			return $aa;
		}

		public function IsFileDelimiterSeperated($delimiter, $check_file, $file_type=NULL, $min_num_cols_expected=3)
		{
			// Purpose: check if file is separated by delimiter excluding any lines that start with #

			// read thru file and skip all lines that start with #
			$of = fopen($check_file, 'r') or die('unable to open file!');

			$checked_lines = 0;

			$num_delimiter_last_time = 0;

			while (!feof($of) && $checked_lines < 5)
			{
				// read 5 lines or until the end of the file after the # lines
				$line = fgets($of);

				if(strpos($line, '#') !== 0 && $line != '' && !empty($line) && str_replace(PHP_EOL, '', $line) != '')
				{
					$curr_num_cols = substr_count($line, $delimiter);	

					$checked_lines += 1;

					// Make sure $curr_num_cols != 0 and if $num_delimiter_last_time != 0 make sure they are the same number otherwise return False
					if ($curr_num_cols === 0 || $curr_num_cols < $min_num_cols_expected)
					{
						return False;
					}

					else if ($num_delimiter_last_time === 0)
					{
						$num_delimiter_last_time = $curr_num_cols;
					}
				}
			}
			
			fclose($of);
			return True;
		}

		public function CovFileTypeCorrect($check_file, $possible_genes, $sample_name)
		{
			// (depth_coverage file, array, name sample)
			// check that format of coverage file is correct
			// for myleoid panel
			$of = fopen($check_file, 'r') or die('unable to open file!');
			// get the header of the file
			$line = rtrim(fgets($of));
			$header = explode("\t", $line); 

			///////////////////////////////////////////////////////
			// Check that the header has at least 5 columns
			///////////////////////////////////////////////////////
			if (!sizeOf($header) >= 5)
			{
				return 'Header has too few columns';
			}

			///////////////////////////////////////////////////////
			// Check header contains all columns necessary
			///////////////////////////////////////////////////////
			$necessary_cols_bills = array('Gene', 'Amplicon', 'Exon', 'Coding Region', 'Depth', 'Sample');
			
			$bills_cols_present = $this->allValuesPresentInArray($header, $necessary_cols_bills);

			$necessary_cols_torrent = array("Gene", "Amplicon", "Sample", "Coverage", "chrom", "start", "stop");
			$torrent_cols_present = $this->allValuesPresentInArray($header, $necessary_cols_torrent);

			// Since Bills code has problems and does not give a consistent output check many things in file before uploading file
			if ($bills_cols_present)
			{
				$sample_found = False;
				$sample_names_found = '';

				while(!feof($of))
				{
					$line = rtrim(fgets($of));
					
					$split_line = explode("\t", $line);

					if ($line != '' && sizeOf($split_line) >= 5)
					{
						$amp_linked_array = $this->array_combine($header, $split_line);

						///////////////////////////////////////////////////////
						// Check that sample_name starts with input sample name if not make 
						// a list of possible names 
						///////////////////////////////////////////////////////
						if (strpos($amp_linked_array['Sample'], $sample_name) !== False)
						{
							$sample_found = True;
						}
						else if (strpos($sample_names_found, $amp_linked_array['Sample'])=== False)
						{
							if ($sample_names_found !== '')
							{
								$sample_names_found.= ', ';	
							}
							
							$sample_names_found.= $amp_linked_array['Sample'];
						}
						///////////////////////////////////////////////////////
						// Check if gene is in possible_genes array
						///////////////////////////////////////////////////////
						if (!$this->checkStrInArrayStartsWithStr($amp_linked_array['Gene'], $possible_genes))
						{					
							return 'The Gene '.$amp_linked_array['Gene'].' is not an expected gene';
						}

						///////////////////////////////////////////////////////
						// Check that amplicon starts with gene in $possible_genes
						///////////////////////////////////////////////////////
						if (!$this->checkStrInArrayStartsWithStr($amp_linked_array['Amplicon'], $possible_genes))
						{
							
							return 'The Amplicon '.$amp_linked_array['Amplicon'].' does not contain an expected gene';
						}

						///////////////////////////////////////////////////////
						// Check that exon and Depth is an integer
						///////////////////////////////////////////////////////
						if (!preg_match("/^\d+$/", $amp_linked_array['Exon']))
						{				
							return 'Exon is expected to be an integer.  This was found as an exon '. $amp_linked_array['Exon'];
						}
						
						if (!preg_match("/^\d+$/", $amp_linked_array['Depth']))
						{				
							return 'Depth is expected to be an integer.  This was found as a Depth '. $amp_linked_array['Depth'];
						}

						///////////////////////////////////////////////////////
						// Check that Coding Region has the format num-num
						///////////////////////////////////////////////////////
						if (!preg_match("/^\d+-\d+$/", $amp_linked_array['Coding Region']))
						{				
							return 'Coding Region is expected to be of the format number-number.  This was found as a Coding Region '. $amp_linked_array['Coding Region'];
						}
					}				
				}

				// return error if sample name was never found
				if (!$sample_found)
				{
					return 'The sample name '.$sample_name.' was not found in your coverage file.  Here are possible sample names in your coverage file: '.$sample_names_found;
				}
			}


			if (!$bills_cols_present && !$torrent_cols_present)
			{
				return "Header does not contain expected columns.";
			}

			return True;
		}

		public function startsWith ($string, $startString) 
		{ 
			// function to see if string starts with a substring
			$len = strlen($startString); 
			return (substr($string, 0, $len) === $startString); 
		} 


		public function checkStrInArrayStartsWithStr($in_str, $in_array)
		{
			// ( 'ABC_23', (1 => 'ABC', 2 => 'DEF') ) -> True
			// ( 'BC_23', (1 => 'ABC', 2 => 'DEF') ) -> False

			foreach ($in_array as $key => $a_str)
			{
				if (preg_match("/^".$a_str."/", $in_str))
				{				
					return True;
				}
			}
			return False;
		}

		public function allValuesPresentInArray($check_array, $required_array)
		{
			// ( ('A', 'B', 'C'), ('B', 'C')) -> True 
			// ( ('A', 'B', 'C'), ('B', 'D')) -> False

			foreach ($required_array as $req)
			{
				if (!in_array($req, $check_array))
				{
					return False;					
				}				
			}
			return True;
		}

		public function MakeNestedInputs($group_num, $field_name, $val, $visibility='d-none')
		{
			// Purpose:  Sometimes hidden inputs need to be automatically generated. This function will return a formated input which makes a nested post.  
			// (int, str, str) -> <input type="text" class="d-none" id="Group1_field_name" name="Group1[field_name]" value="val">

			$input = '<input type="text" class="'.$visibility.'" id="Group'.$group_num.'_'.$field_name.'" name="Group'.$group_num.'['.$field_name.']" value="'.$val.'">';
			return $input;
		}

		public function ConvertDatabaseVar($row_name, $value, $extra_val=Null)
		{
			// (str, str) -> str
			// Purpose: depending on the row_name some formating might be needed.  
			// Example:
			// There might be a row_name of refs convert this to include https://www.ncbi.nlm.nih.gov/pubmed/27070783,23777544


			switch($row_name)
			{

				case 'loc':
					return '<a href="http://www.ensembl.org/Homo_sapiens/Location/View?r='.$extra_val.':'.$value.'" target="_blank">'.$value.'</a>';

					break;

				case 'cosmic':
					// input     [cosmic] => 12366:24268:13553:26129
					// get each cosmic id and add url
					$split_value = explode(':', $value);
					$to_return = '';

					foreach($split_value as $cosmic_id)
					{
						$to_return .= '<a href="http://cancer.sanger.ac.uk/cosmic/mutation/overview?id='.$cosmic_id.'"target="_blank">'.$cosmic_id.'</a> ';
					}
					return $to_return;
					break;

				case 'dbsnp':

					// input rs397517129:rs121434568:rs121913443

					$split_value = explode(':', $value);
					$to_return = '';

					foreach($split_value as $rs)
					{
						$to_return .= '<a href="https://www.ncbi.nlm.nih.gov/projects/SNP/snp_ref.cgi?rs='.str_replace('rs', '',$rs).'"target="_blank">'.$rs.'</a> ';
					}
					return $to_return;

					break;

				case 'pmid':


					// most of the time there are a lot of pmids change the shown data to a count of number of references
					$count = sizeOf(explode(',', $value));

					if ($value != '')
					{
						return '<a href="https://www.ncbi.nlm.nih.gov/pubmed/'.$value.'"target="_blank">'.(string)$count.'  references</a>';
					}
					else
					{
						return '';
					}
					break;

				case 'genes':
				
					$link = $value.': <a href="http://www.genecards.org/cgi-bin/carddisp.pl?gene='.$value.'"target="_blank" title="gene cards"><img src="images/logo_genecards.png" alt="logo genecards"></a>';
					if (isset($this->halmark_gene[$value]) && $this->halmark_gene[$value] == 'yes')
					{
						$link .= '<a href="http://cancer.sanger.ac.uk/cosmic/census-page/'.$value.'"target="_blank" title="comsic halmark"><img src="images/gene_cosmic_logo.png" alt="gene cosmic logo"></a>';
					}
					
					$link .= ' <a href="https://www.genenames.org/cgi-bin/gene_symbol_report?hgnc_id=HGNC:'.$extra_val.'" target="_blank" title="HUGO Gene Nomenclature Committee"><img src="images/hugo.png" alt="hugo gene nomenclature link" ></a>';

					return $link;
					break;

				case 'pfam':

					$split_value = explode(':', $value);
					$to_return = '';

					foreach($split_value as $pfam)
					{
						$to_return .= '<a href="http://pfam.xfam.org/family/'.$pfam.'"target="_blank">'.$pfam.'</a> ';
					}
					return $to_return;
					break;

				case 'transcript':
					// check if an ensembl or NM number
					if (strpos($value, 'NM_') !== false)
					{
						return '<a href="https://www.ncbi.nlm.nih.gov/nuccore/'.$value.'"target="_blank">'.$value.'</a>';
					}
					elseif (strpos($value, 'ENST') !== false)
					{
						return '<a href="http://www.ensembl.org/Homo_sapiens/Transcript/Summary?t='.$value.'"target="_blank">'.$value.'</a>';
					}
					break;

				default:
					return $value;
			}
		}

		public function DoubleAscSort($row_1_nam, $row_2_nam, $in_array)
		{
			if (!empty($in_array))
			{
				// sort an array of arrays
				foreach ($in_array as $key => $row)
				{
				    $row_1[$key] = $row[$row_1_nam];
				    $row_2[$key] = $row[$row_2_nam];
				}

				array_multisort($row_1, SORT_ASC, $row_2, SORT_ASC, $in_array);
			}

			return $in_array;			
		}

		public function TimeDayGreeting()
		{
			// () -> str
			// purpose: get random greeting

			$hour = date('H');

			/* If the time is less than 1200 hours, show good morning */
			if ($hour < '12')
			{
				$time = 'Good morning';
			}

			/* If the time is grater than or equal to 1200 hours, but less than 1700 hours, so good afternoon */
			elseif ($hour >= '12' && $hour < '17')
			{
				$time = 'Good afternoon';
			}

			/* Should the time be between or equal to 1700 and 1900 hours, show good evening */
			elseif ($hour >= '17' && $hour < '19')
			{
			   $time = 'Good evening';
			}

			/* Finally, show good night if the time is greater than or equal to 1900 hours */
			elseif ($hour >= '19')
			{
				$time = 'Good night';
			}

			// pick one of the possible greetings below.
			$poss_greetings = array('Welcome to Turbo Storage', 'Howdy', 'Hello there,', 'Bonjour', 'Hello! Welcome to Turbo Storage', 'Hi there', 'Nice to see you', $time);
			$i = rand(0, count($poss_greetings)-1);
			$greeting = $poss_greetings[$i];

			return $greeting;
		}

		function isRunning($pid)
		{
			// When running a commandline script in the background it is nice to 
			// monitor if it is done yet.  It can be done by passing the pid to this function
			// Example of how to start the 
			// exec(sprintf("%s > %s 2>&1 & echo $! >> %s", $cmd, $outputfile, $pidfile));
			// This launches the command $cmd, redirects the command output to $outputfile, and writes the process id to $pidfile.

			// exec(sprintf("$s > $s 2>&1 & echo $1", $cmd, $outputfile),$pidArr); 
			try
			{
			    $result = shell_exec(sprintf("ps %d", $pid));
			    if( count(preg_split("/\n/", $result)) > 2)
			    {
			        return true;
			    }
			}
			catch(Exception $e){}

			return false;
		}

		public function MySQLTimeStampToDate($time_stamp)
		{
			// convert a mysql time stamp to a date
			// (str) -> str
			// (2017-09-26 10:15:29) -> Sep 26 2017

			return date('M j Y', strtotime($time_stamp));
		}

		public function US_DateToYYYYMMDD($us_date)
		{
			return date('Ymd', strtotime($us_date));
		}


		public function TextToUrls($string)
		{
			// (str) -> str
			// take a string of text and convert string urls to html links

			$regex = '/\b(https:\/\/|ftp:\/\/|http:\/\/|www\.)[-A-Z0-9+&@#\/%?=~_|$!:,.;]*[A-Z0-9+&@#\/%=~_|$]/i';
			preg_match_all($regex, $string, $matches);
			$urls = $matches[0];

			if (!empty($urls))
			{
				// go over all links and change all links in string to an external link
				foreach($urls as $url) 
				{
					// add http to links to make an external links 
					if (strpos($url, 'http') === false)
					{
						$updated_url = 'http://'.$url;
						$string = str_replace($url, '<a href="'.rtrim($updated_url, '/).$/').'" target="_blank">'.$url.'</a>', $string);
					}
					else
					{
						$string = str_replace($url, '<a href="'.rtrim($url, '/).$/').'" target="_blank">'.$url.'</a>', $string);
					}					
				}
				return $string;
			}
			else
			{
				return $string;
			}
		}

		public function AddPMIDUrls($string)
		{
			// (string) -> string
			// Find in a large string PMID: then take whatever is after it as the number
			// Example: PMID:20008640
			$regex = '/(PMID: |PMID :)[0-9]*/';
			preg_match_all($regex, $string, $matches);
			$pmids = $matches[0];

			if (!empty($pmids))
			{
				foreach($pmids as $i=>$pmid)
				{
					$pmid = explode(':',str_replace(' ', '', $pmid))[1];

					$url = 'https://www.ncbi.nlm.nih.gov/pubmed/'.$pmid;

					$string = str_replace($pmid, '<a href="'.rtrim($url, '/).$/').'" target="_blank">'.$pmid.'</a>', $string);

					return $string;
				}
			}
			else
			{
				return $string;
			}
		}

		public function AddItalicGenes($string)
		{
			// () -> 

			$regex = '/(';
			// make regex of all genes
			foreach($this->halmark_gene as $gene=>$halmark)
			{
				$regex.=$gene.' |';
			}

			$regex .= ')*/';
			
			preg_match_all($regex, $string, $matches);
			$genes = $matches[0];

			if (!empty($genes))
			{
				// make a unique list of all genes found in string
				$unique_genes = array();

				foreach($genes as $i=>$gene)
				{
					if (!in_array($gene, $unique_genes) && $gene !== '')	
					{
						array_push($unique_genes, $gene);
					}					
				}			

				foreach($unique_genes as $gene)
				{
					$italic_gene = '<i>'.$gene.'</i>';

					$string = str_replace($gene, $italic_gene, $string);	
				}

				return $string;				
			}
			else
			{
				return $string;
			}
			return $string;
		}

		public function AddPMIDLinkUrls($string)
		{		
			$string = $this->TextToUrls($string);
			$string = $this->AddPMIDUrls($string);
		
			return nl2br($string);
		}

		public function RunDateToUSDate($run_date_chip)
		{
			// (str) -> str
			// purpose: 170809B -> 08/09/2017

			// remove letters
			$date_string = $this->ReplaceLetters($run_date_chip);

			$return_string = substr($date_string, 4,5);
			$return_string .='/';
			$return_string .= substr($date_string, 2,2);
			$return_string .= '/20'.substr($date_string, 0,2);

			return $return_string;
		}

		public function PickRandomFileFromFolder($folder_address)
		{
			// (str) -> str
			// Purpose: pick a random file from a folder.  Use this to pick a a random img from a folder of img

			// get all files in folder removing hidden folders and files
			$files = array_diff(scandir($folder_address), array('.', '..'));

			// pick random number starting from the first index in array
			$i = rand(array_keys($files)[0], count($files)-1);

			// provide random address
			return $folder_address.'/'.$files[$i];
		}

		public function oddEvenSectionMark($num)
		{
			// (int) -> str
			// if the number entered is odd return a thick blue line 
			// if the number entered is even return a think red line

			if ($this->isEven($num))
			{
				return '<hr class="thick-red-line" title="Color does not imply anything it just makes distinction between variants easier to see.">';
			}
			else
			{
				return '<hr class="thick-blue-line title="Color does not imply anything it just makes distinction between variants easier to see."">';
			}
		}

		public function oddEvenBackground($num)
		{
			if ($this->isEven($num))
			{
				return 'style="background-color:#fbeded;"';
			}
			else
			{
				return 'style="background-color:#eaf1f7;"';				
			}
			
		}

		public function isEven($num)
		{
			if ($num % 2 == 0)
			{
				return True;
			}
			else
			{
				return False;
			}
		}

		public function zipFolderDownload($in_folder, $zip_file)
		{
			// zip a folder into a file and download it.

			// Get real path for our folder
			$rootPath = realpath($in_folder);

			// Initialize archive object
			$zip = new ZipArchive();
			$zip->open($zip_file, ZipArchive::CREATE | ZipArchive::OVERWRITE);

			// Create recursive directory iterator
			/** @var SplFileInfo[] $files */
			$files = new RecursiveIteratorIterator(
			    new RecursiveDirectoryIterator($rootPath),
			    RecursiveIteratorIterator::LEAVES_ONLY
			);

			foreach ($files as $name => $file)
			{
			    // Skip directories (they would be added automatically)
			    if (!$file->isDir())
			    {
			        // Get real and relative path for current file
			        $filePath = $file->getRealPath();
			        $relativePath = substr($filePath, strlen($rootPath) + 1);

			        // Add current file to archive
			        $zip->addFile($filePath, $relativePath);
			    }
			}

			// Zip archive will be created only after closing object
			$zip->close();

			header('Content-Description: File Transfer');
			header('Content-Type: application/octet-stream');
			header('Content-Disposition: attachment; filename='.basename($zip_file));
			header('Content-Transfer-Encoding: binary');
			header('Expires: 0');
			header('Cache-Control: must-revalidate');
			header('Pragma: public');
			header('Content-Length: ' . filesize($zip_file));
			readfile($zip_file);
		}

		public function UnderscoreCaseToHumanReadable($in_str)
		{
			// str -> str
			// last_name -> Last Name

			return ucwords(str_replace('_', ' ', $in_str));
		}

		public function ReplaceSpecialChars($string)
		{
			$string = preg_replace('/[^a-zA-Z0-9_.]/', '', $string);

			$string = str_replace('.', '_', $string);

			return $string;
		}

		public function ReplaceLetters($string)
		{
			return preg_replace("/[^0-9,.]/", "", $string);
		}


		public function InputAID($in_key)
		{
			// if given a key see if it is an id.  Return TRUE if it is an id otherwise return false

			return in_array($in_key, $this->ignore_ids);
		}

		public function InputNotIgnore($in_key)
		{
			return in_array($in_key, $this->ignore_fields);
		}

		public function SearchPostArray($redirect_page, $post_array)
		{

			$url = 'Location:'.REDIRECT_URL.'?page='.$redirect_page;

			// remove submit and all empty searches.  If array ends up empty notify user with message
			foreach($post_array as $key => $val)
			{
				if ($val === '' || $val === 'Submit')
				{
					unset($post_array[$key]);
				}

				else
				{
					$url .= '&'.$key.'='.$val;

				}
			}

			if (empty($post_array))
			{
				return 'OOPS! Please enter a search parameter.';
			}

			else
			{
				// add all remaining $post_array to url
				return $url;
			}
		}
		
		public function in_array_r($needle, $haystack, $strict=false) 
		{
			// (str or int, multi dimensional array, bool) -> bool

    			foreach ($haystack as $item) 
    			{
        			if (($strict ? $item === $needle : $item == $needle) || (is_array($item) && $this->in_array_r($needle, $item, $strict))) 
        			{
            			return true;
            		}
	        	}
	    

    			return false;
		}

		public function FindSubArrayContaining($needle, $haystack, $strict=false)
		{
			// (str or int, multi dimensional array, bool) -> bool or array

    			foreach ($haystack as $item) 
    			{
        			if (($strict ? $item === $needle : $item == $needle) || (is_array($item) && $this->in_array_r($needle, $item, $strict))) 
        			{
            			return $item;
            		}
	        	}
	    

    			return false;
		}

		public function calc_age($dob, $today)
		{
			// (date, date) -> int
			// input in yyyy-mm-dd 
			$dob = date_create($dob);
			$today = date_create($today);
			$diff = date_diff($dob, $today);
			return $diff->format('%y');

		}

		public function days_past($start_date, $end_date)
		{
			// (date, date) -> int
			// input in yyyy-mm-dd 
			// Calculating the difference in timestamps
			$diff = strtotime($start_date) - strtotime($end_date);

			// 1 day = 24 hours 
			// 24 * 60 * 60 = 86400 seconds 
			return abs(round($diff / 86400));
		}

		public function doesUserHavePermission($user_permissions, $permission)
		{
			// redirect user to home page if they do not have permission
			if (strpos($user_permissions, $permission) === false ) 
			{
				header('Location:'.REDIRECT_URL.'?page=home');
			}
		}

		public function ConvertAllUsersWithAPermissionToStr($all_users_with_a_permission_array, $sep_type='or', $include_email=False)
		{
			// Example
				// Example db search to produce the $all_users_with_a_permission_array
					// $db->listAll('get-all-users-with-a-permission', 'login_enhanced');

				// $all_users_with_a_permission_array
					// array (size=3)
					//   0 => 
					//     array (size=3)
					//       'permission' => string 'login_enhanced' (length=14)
					//       'user_name' => string 'Samantha Taffner' (length=16)
					//       'email_address' => string 'taffners@gmail.com' (length=18)
					//   1 => 
					//     array (size=3)
					//       'permission' => string 'login_enhanced' (length=14)
					//       'user_name' => string 'Sounpheth Thammavong' (length=20)
					//       'email_address' => string 'sounpheth_thammavong@urmc.rochester.edu' (length=39)
					//   2 => 
					//     array (size=3)
					//       'permission' => string 'login_enhanced' (length=14)
					//       'user_name' => string 'Paige Elliot' (length=12)
					//       'email_address' => string 'paige_elliott@urmc.rochester.edu' (length=32)

				// Example output
					// Samantha Taffner, Sounpheth Thammavong, or Paige Elliot
					// Samantha Taffner taffners@gmail.com, Sounpheth Thammavong sounpheth_thammavong@urmc.rochester.edu, or Paige Elliot paige_elliott@urmc.rochester.edu
			$out_str = '';

			foreach ($all_users_with_a_permission_array as $key => $user)
			{
				if ($out_str !== '' && sizeOf($all_users_with_a_permission_array) > 2)
				{
					$out_str.=', ';
				}
				else if ($out_str !== '' && sizeOf($all_users_with_a_permission_array) <= 2)
				{
					$out_str.=' ';
				}

				if ($key === sizeOf($all_users_with_a_permission_array) - 1 && sizeOf($all_users_with_a_permission_array) > 1)
				{
					$out_str.= $sep_type.' ';
				}

				$out_str.= $user['user_name'];

				if ($include_email)
				{
					$out_str.= ' '.$user['email_address'];
				}
			}
			return $out_str;

		} 

		

     }
?>
