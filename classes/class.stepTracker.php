<?php

	class StepTracker
	{
		private $steps; 
		private $step_table;


		private $db;

		public function StepTracker($db, $steps, $step_table)
		{
			// initialize function
			$this->db = $db;

			$this->steps = $steps;

			$this->step_table = $step_table;	

			$this->no_update_steps = array('pool_download_excel');						
		}

		public function SkipStep($completed_steps, $page, $run_id, $user_id)
		{
			// only update if the step has not been completed 
			$step_status = $this->StepStatus($completed_steps, $page);

			if ($step_status != 'completed')
			{
				// add step completed to step_run_xref if not already completed
	     		$url = $this->UpdateStepInDb($page, $completed_steps, $run_id, $user_id);
			}  

			return $this->FindNextStep($page);
		}

		public function AddStepsToStepTable($table_name)
		{

			// add steps to report steps which do not need to be a full step in the report making
			$all_steps_in_report_steps_table = $this->steps;
		
			array_push($all_steps_in_report_steps_table, 'confirmed', 'tech_approved');


			// check if all the steps in $steps are in report_steps_table
			foreach ($all_steps_in_report_steps_table as $val)
			{
				$where = array(
						'table_name' => $table_name, 
						'step' => $val
				);

				// find if step has been added to report steps
				$step_id = $this->db->listAll('step-added', $where);

				if (empty($step_id))
				{
					$add_array = array();
					$add_array['step'] = $val;

					$this->db->addOrModifyRecord($table_name, $add_array);
				}			
			}
		}

		public function StepStatus($completed_steps, $step)
		{
			$found = false;
			$btn_class = 'not_complete';

			if (isset($completed_steps)  && !empty($completed_steps))
			{
				foreach ($completed_steps as $key => $value)
				{					
					if ($value['step'] === $step)
					{

						$btn_class = 'completed';
						$found = true;
					}								
				}
			}

			return $btn_class;
		}

		public function terminalStepBtnStatus($completed_steps, $curr_step, $terminal_step)
		{
			// return show or not_show on adding a submit button depending on if the terminal_step was reached.
			
			// find if current step is complete if so see if it is in no_update_steps
			$curr_step_status = $this->StepStatus($completed_steps, $curr_step);		
			
			if ($curr_step_status === 'completed' && array_search($curr_step, $this->no_update_steps) !== false)
			{
				return 'not_show';
			}

			// find if terminal_step is complete
			$terminal_step_status = $this->StepStatus($completed_steps, $terminal_step);

			if ($terminal_step_status === 'not_complete')
			{
				return 'show';
			}			

			$terminal_step_num = array_search($terminal_step, $this->steps);

			$current_step_num = array_search($curr_step, $this->steps);

			if ($current_step_num >= $terminal_step_status)
			{
				return 'show';
			}

			return 'not_show';
		}

		public function lockPageStatus($completed_steps, $page)
		{
			// return show or a description of how much longer the page will be locked.

			////////////////////////////////////////////////////
			// find if the current page has a lock in the the lock_page_table
				// If page already complete auto fill will not be necessary so 
					// return show 
				// If no lock_page_info found 
					// return show
				// lock_page found but Get restrict field (ref_id_name, ref_id) filled out and not equal to current get
				
				// If lock_page_info found, step not completed, lock_record not found	
					// return show
					// Add a lock_record equal to the lock_seconds_length in lock_page_table
				// If lock_page_info found, step not completed, lock_record found
					// return lock_record info including a time when the lock will deactivate and the user that activated the lock
					// This will deactivate the submit button
			////////////////////////////////////////////////////
			$curr_step_status = $this->StepStatus($completed_steps, $page);		
			
			// If page already complete auto fill will not be necessary so 
			if ($curr_step_status === 'completed')
			{
				return 'show';
			}

			$lock_page_info = $this->db->listAll('lock-page-info', $page);

			// If no lock_page_info found 
			if (empty($lock_page_info))
			{
				return 'show';
			}

			// lock_page found but Get restrict field (ref_id_name, ref_id) filled out and not equal to current get
			else if (
						isset($lock_page_info[0]['ref_id_name']) && 
						isset($_GET[$lock_page_info[0]['ref_id_name']]) &&
						$_GET[$lock_page_info[0]['ref_id_name']] != $lock_page_info[0]['ref_id']
					)
			{
				return 'show';
			}

			// find if lock_record is found within the time period listed in lock_page_table under
			// lock_seconds_length.
			if (!isset($lock_page_info[0]['lock_seconds_length']) || !isset($lock_page_info[0]['lock_page_id']))
			{
				return 'show';
			}

			$get_ref_id = '';
			if (isset($_GET[$lock_page_info[0]['ref_id_name']]))
			{
				$get_ref_id = $_GET[$lock_page_info[0]['ref_id_name']];
			}


			$search_array = array(
				'lock_page_id' 			=> 	$lock_page_info[0]['lock_page_id'],
				'lock_seconds_length'	=> 	$lock_page_info[0]['lock_seconds_length'],
				'ref_id'				=> 	$get_ref_id
			);

			$current_lock_records = $this->db->listAll('current-lock-page-records', $search_array);		

			// If lock_page_info found, step not completed, lock_record not found	
				// return show
				// Add a lock_record equal to the lock_seconds_length in lock_page_table
			if (empty($current_lock_records))
			{
				$addLockArray = array(
					'ref_id' 		=> 	$_GET[$lock_page_info[0]['ref_id_name']],
					'lock_page_id'	=> 	$lock_page_info[0]['lock_page_id'],
					'user_id'		=> 	USER_ID
				);

				$lock = $this->db->addOrModifyRecord('lock_record_table', $addLockArray);
				
				return 'show';
			}

			// If lock_page_info found, step not completed, lock_record found
				// return lock_record info including a time when the lock will deactivate and the user that activated the lock
				// This will deactivate the submit button
			else if (isset($current_lock_records[0]['user_name']) && isset($current_lock_records[0]['unlock_time']))
			{
				return $current_lock_records[0]['user_name'].' is currently using this page or recently used it.  To ensure no duplicate, automatically generated data this page will be locked until '.$current_lock_records[0]['unlock_time'].'.';
			}
		}

		public function activeButtonStatus($step, $color_type)
		{
			if ($_GET['page'] === $step)
			{
				return ' btn-'.$color_type.'-active';
			}
			else 
			{
				return '';
			}
		}

		public function ButtonStatus($completed_steps, $step)
		{
			$step_status = $this->StepStatus($completed_steps, $step);

			switch ($step_status)
			{
				case 'completed':
					$btn_class = ' btn-success btn-success-hover';

					$btn_class.= $this->activeButtonStatus($step, 'success');
					break;

				case 'not_complete':
					

					// find if the previous step is complete
					$previous_step_complete = $this->FindPreviousStepComplete($completed_steps, $step);

					if (!$previous_step_complete)
					{
						$btn_class = ' btn-warning btn-warning-hover disabled-link';
					}
					else if ($previous_step_complete === 'first step')
					{
						$btn_class = ' btn-warning btn-warning-hover';
					}
					else
					{
						$btn_class = ' btn-warning btn-warning-hover';
					}
					$btn_class.= $this->activeButtonStatus($step, 'warning');

					break;
			}	
			return $btn_class;
		}

		public function BtnFlaggedPassPendingStatus($passed_steps, $flagged_steps, $curr_step)
		{
			// (str, str, str) -> str
			//  >>> BtnFlaggedPassPendingStatus('add_visit, add_patient', 'add_DNA_conc', 'add_DNA_conc') 
			//  ' btn-danger btn-danger-hover'
			//  >>> BtnFlaggedPassPendingStatus('add_visit, add_patient', 'add_DNA_conc', 'add_amplicon_conc') 
			//  ' btn-warning btn-warning-hover'
			//  >>> BtnFlaggedPassPendingStatus('add_visit, add_patient', 'add_DNA_conc', 'add_visit') 
			//  ' btn-success btn-success-hover'

			//////////////////////////////////////////////////////////////////////
			// find if the step was completed successfully
			// ' btn-success btn-success-hover'
			//////////////////////////////////////////////////////////////////////
			if (strpos($passed_steps, $curr_step) !== false)
			{
				return ' btn-success btn-success-hover';
			}

			//////////////////////////////////////////////////////////////////////
			// find if the step was flagged
			// ' btn-danger btn-danger-hover'
			//////////////////////////////////////////////////////////////////////
			else if (strpos($flagged_steps, $curr_step) !== false)
			{
				return ' btn-danger btn-danger-hover';
			}

			//////////////////////////////////////////////////////////////////////
			// find if otherwise return 
			// ' btn-warning btn-warning-hover'
			//////////////////////////////////////////////////////////////////////
			else
			{
				return ' btn-warning btn-warning-hover';
			}
		}

		public function FindNextStep($curr_step)
		{
			// get the next step after the current one
			$curr_i = array_search($curr_step, $this->steps);

			if ($curr_i < count($this->steps)-1)
			{
				return $this->steps[$curr_i + 1]; 
			}
			else
			{
				return 'last step';
			}			
		}

		public function FindPreviousStep($curr_step)
		{
			$curr_i = array_search($curr_step, $this->steps);
			if ($curr_i !== 0  && $curr_i != False)
			{
				return $this->steps[$curr_i - 1]; 
			}
			else
			{
				return 'first step';
			}
		}

		public function FindLastStep()
		{
			return end($this->steps);
		}

		public function FindPreviousStepComplete($completed_steps, $curr_step)
		{
			// Find if the previous step is complete
			$curr_i = array_search($curr_step, $this->steps);

			if ($curr_i !== 0  && $curr_i != False)
			{
				$previous_step = $this->steps[$curr_i - 1]; 

				// Find if step is completed
				$previous_step_status = $this->StepStatus($completed_steps, $previous_step);
				if ($previous_step_status === 'completed')
				{				
					return True;
				}
				else
				{				
					return False;
				}
			}
			elseif ($curr_i === 0)
			{
				return 'first step';
			}

			else
			{					
				return False;
			}	
		}

		public function UpdateStepInDb($page, $completed_steps, $run_id, $user_id)
		{
			// (db class, str, str) -> redirect
			// Add the newly completed step to database

			$step_completed = $this->StepStatus($completed_steps, $page);

			// add step completed to step_run_xref if not already completed
			// used in review report
          	if ($page === 'tech_approved' || $step_completed === 'not_complete')
          	{
				// Find step id
	          	$curr_step = $this->db->listAll('this-step', $page);
	          	$stepArray = array();
	          	
	          	if (empty($curr_step))
	          	{
	          		$stepArray['step'] = $page;
	          	}
	          	else
	          	{
	          		$stepArray['step_id'] = $curr_step[0]['step_id'];
	          	}

	          	
	          	$stepArray['run_id'] = $run_id;	          	
	          	$stepArray['user_id'] = $user_id;
	          	
	          	// add completed step
	          	$step_result = $this->db->addOrModifyRecord($this->step_table, $stepArray);
			}

			// get the next step
	        $next_step = $this->FindNextStep($page);

			$url = '?page='.$next_step.'&'.EXTRA_GETS;
     	
          	return $url;
		}

		public function UpdatePreStepInDb($page, $completed_steps, $visit_id, $user_id, $run_id, $step, $status, $conc=NULL)
		{
			// (db class, str, str) -> redirect
			// Add the newly completed step to database
			// NOTE: It would be best to merge this function and UpdateStepInDb into one function.  However, column names are hardcoded in UpdateStepInDb and it is already used in the entire report building part of app.  Therefore making a new function here for Pre.

			$step_completed = $this->StepStatus($completed_steps, $page);
		
			// add step completed to step_run_xref if not already completed
          	if ($step_completed === 'not_complete')
          	{

				$insertPreStepArray = array(
						'user_id' 	=> 	$user_id,
						'visit_id' 	=> 	$visit_id,
						'step' 		=> 	$step,
						'status' 		=> 	$status,
						'concentration'=>	$conc
					);
	          	
	          	// add completed step
	          	$step_result = $this->db->addOrModifyRecord($this->step_table, $insertPreStepArray);
			}

			// get the next step
	          $next_step = $this->FindNextStep($page);

			$url = '?page='.$next_step.'&visit_id='.$visit_id;
     	
			if ($run_id != 0)
			{
				$url .='&run_id='.$run_id;
			}

          	return $url;
		}

		public function UpdatePoolStepInDb($page, $completed_steps, $pool_chip_linker_id, $user_id, $ngs_panel_id)
		{
			$step_completed = $this->StepStatus($completed_steps, $page);
	
			// add step completed to step_run_xref if not already completed
          	if ($step_completed === 'not_complete')
          	{

				$insertPoolStepArray = array(
						'user_id' 			=> 	$user_id,
						'pool_chip_linker_id' 	=> 	$pool_chip_linker_id,
						'step' 				=> 	$page,
						'status'			=> 'completed'
					);
	          	
	          	// add completed step
	          	$step_result = $this->db->addOrModifyRecord($this->step_table, $insertPoolStepArray);
			}

			// get the next step
	          $next_step = $this->FindNextStep($page);

	          // redirect to home page if last step
	          if ($next_step === 'last step')
	          {
	          	$url = '?page=home';
	          }
	          else
	          {
	          	$url = '?page='.$next_step.'&pool_chip_linker_id='.$pool_chip_linker_id.'&ngs_panel_id='.$ngs_panel_id;
	          }
			
          	return $url;
		}

		public function GetSteps()
		{
			return $this->steps;
		}
	}
?>