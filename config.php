<?php
	// ini_set('display_errors', 0);
	// ini_set('display_startup_errors', 0);
	// ini_set('expose_php','Off');
	// ini_set('error_reporting','E_ALL');
	// ini_set('display_errors','Off');
	// ini_set('display_startup_errors','Off');
	// ini_set('log_errors','On');
	// ini_set('error_log','/valid_path/PHP-logs/php_error.log');
	// ini_set('ignore_repeated_errors','Off');

	ini_set('display_errors', 1);
	ini_set('display_startup_errors', 1);
	error_reporting(E_ALL);

	define('JS_UPGRADE_TESTING',True);

	if (!defined('ROOT_URL'))
	{
		define('ROOT_URL',explode('/', $_SERVER['REQUEST_URI'])[1]);
	}
	
	// backup dev db
	// mysqldump -u urmc_app_user -p --databases urmc_reporter_dev_db > /var/www/html/devs/backup_urmc_reporter_dev_db_06_13_18.sql 

	// import into live db
	// mysql -u urmc_app_user -p urmc_reporter_db < backup_urmc_reporter_dev_db_06_13_18.sql

	// get session info and keep open for the least amount of time to reduce 
	// open session collisions
	session_start();
	$copy_session = $_SESSION;

	session_write_close(); 

	define('SITE_TITLE', 'Molecular Diagnostics Infotrack');
	define('SITE_TITLE_ABBR', 'MD Infotrack');
	define('SERVER_SAVE_LOC', '/var/www/urmc_reporter_data/');

	define('OFA_WORKFLOW', 'OFA_p200611');//'OFA_5-10_complex_v3_filter_updated'); //copy of OFA_WF_v5-12_p200402
	define('OFA_PLANNAME', 'OFA_DNA_530_p200611');
	define('OFA_FILTER_CHAIN', 'OFA_Main_v3');

	define('CURR_YEAR', intval(date('Y')));
	define('CURR_MONTH', intval(date('m')));

	// set location of class dir & redirect address for dev vs live code
	if (ROOT_URL === 'devs')
	{
		define('CLASS_DIR', ($_SERVER['DOCUMENT_ROOT'].'/devs/urmc_reporter/classes/'));
		define('REDIRECT_URL', '/devs/urmc_reporter');
		define('DB_PASSWORD', 'DefaultPasswordMySQL123456'); // DefaultPasswordMySQL123456
		define('DB_NAME', 'urmc_reporter_dev_db');
		
		// User a custom error handler to print output
		// set_error_handler( 'myErrorHandler' );
		// define('DB_NAME', 'urmc_reporter_db');
	}
	elseif (ROOT_URL === 'urmc_reporter')
	{
		define('CLASS_DIR', ($_SERVER['DOCUMENT_ROOT'].'/urmc_reporter/classes/'));
		define('REDIRECT_URL', '/urmc_reporter');
		define('DB_PASSWORD', 'DefaultPasswordMySQL123456'); // DefaultPasswordMySQL123456
		define('DB_NAME', 'urmc_reporter_db');
	}

	define('DEFAULT_DATE_TIME', '1970-01-01 12:00:00');

	/*  Database Information - Required!!  */
	/* -- Configure the Variables Below --*/
	define('DB_HOST', 'localhost');
	define('DB_USER', 'urmc_app_user');

	/* Database Stuff, do not modify below this line */
	$conn = mysqli_connect(DB_HOST,DB_USER,DB_PASSWORD) or die ("Couldn't connect to server.");

	$db = mysqli_select_db($conn, DB_NAME) or die("Couldn't select database.");

	$qc_conn = new mysqli(DB_HOST,DB_USER,DB_PASSWORD, 'qc') or die ("Couldn't connect to server.");

	// import fpdf for making pdfs
	require_once('classes/class.utils.php');
	require_once('php_libraries/fpdf.php');
	require_once('classes/class.html.php');
	require_once('classes/class.barcode.php');
	require_once('classes/class.api_utils.php');

	// Add every class in the class dir
	if($handle = opendir(CLASS_DIR))
	{
		while(false !== ($file = readdir($handle)))
		{
	    		if($file === '.' || $file === '..' || is_dir(CLASS_DIR.$file))
	    		{
	        		continue;
	        	}

			require_once(CLASS_DIR.$file);
	    }

    		closedir($handle);
	}

	$db = new DataBase($conn, md5('md5 code not active'), SITE_TITLE, ROOT_URL);
	$qc_db = new DataBase($qc_conn, md5('md5 code not active'), SITE_TITLE, ROOT_URL);
	
	// set ip
	$db->SetIP();

	// make a date functions object
	$dfs = new DateFuncs();

	// Make a utils object
	$utils = new Utils();

	// Make api object
	$API_Utils = new APIUtils();	

	// Make emailerHandler Object
	$email_init_array = array(
		'db'	=> 	$db
	);
	$email_utils = new EmailUtils($email_init_array);

	$versionArray = $db->listAll('version-history-desc');

	define('VERSION', $versionArray[0]['version']);

	// since pages use RUN_ID instead of $_GET['run_id'] set if for all pages
	if (isset($_GET) && isset($_GET['run_id']))
	{
		define ('RUN_ID', $_GET['run_id']);
		$runInfoArray = $db->listAll('run-info', RUN_ID);
		
	}
	else
	{
		define ('RUN_ID', 0);
	}

	////////////////////////////////////////////////////
	// Find if the website should be down for maintenance.  If the site is the live site redirect to maintenance page
	////////////////////////////////////////////////////
	$maintenance_now = $qc_db->listAll('qc_db-pending-maintenance-now', SITE_TITLE);

	if (ROOT_URL === 'urmc_reporter' && isset($maintenance_now) && !empty($maintenance_now))
	{

		header('Location:'.REDIRECT_URL.'/maintenance.html');

	}


	//////////////////////////////////////////////////
	// Ajax calls will come the utils folder.  These will not have a $_GET['page']
	// set page to ajax_call
	//////////////////////////////////////////////////
	if (in_array('utils', explode('/', $_SERVER['REQUEST_URI'])))
	{
		$_GET['page'] = 'ajax_call';
	}

	//////////////////////////////////////////////////
	// redirect if page is not defined to login page
	//////////////////////////////////////////////////
	else if (!isset($_GET['page']))
	{
		header('Location:'.REDIRECT_URL.'?page=login');
	}

	//////////////////////////////////////////////////
	// define page if page is login or password_reset
	//////////////////////////////////////////////////
	if ($_GET['page'] == 'login' || $_GET['page'] == 'password_reset')
	{
		$page = $_GET['page'];

		//////////////////////////////////////////////////////////////////
		// Automatic emails are triggered at logins and recorded in 
		// automatic_email_table.  It is done this way to only run this once
		// and I was unable to figure out how to run a chron job on this server
		//////////////////////////////////////////////////////////////////
		$email_utils->runAutoEmailAlerts();
	}

	//////////////////////////////////////////////////
	// If page not equal to login or password_reset and $_SESSION[user]['user_empty'] 
	// redirect to login or  $_SESSION['user']['last_active_time'] empty
	//////////////////////////////////////////////////
	else if (($_GET['page'] != 'login' || $_GET['page'] != 'password_reset') && (empty($copy_session['user']['user_id']) ) ) 
	{
		header('Location:'.REDIRECT_URL.'?page=login&error=redirect_empty_user');	
	}

	//////////////////////////////////////////////////
	// If user has been inactive for more than 1400 minutes.  
	// redirect user to login and log them out. 
	//////////////////////////////////////////////////
	else if ( time() - $copy_session['user']['last_active_time'] >= 1400)
	{
		$inactive_time = time() - $copy_session['user']['last_active_time'];
		$db->logoff();
		header('Location:'.REDIRECT_URL.'?page=login&inactive_time='.$inactive_time);		
	}

	//////////////////////////////////////////////////
	// Make sure the user is logged into the correct app.
	//////////////////////////////////////////////////
	else if (
				$_GET['page'] !== 'login' &&
				(
					!isset($copy_session['user']['site_title']) || 
					SITE_TITLE !== $copy_session['user']['site_title'] ||
								
					!isset($copy_session['user']['root_url']) ||
					ROOT_URL !== $copy_session['user']['root_url']
				)
			)
	{	
		$db->logoff();
		header('Location:'.REDIRECT_URL.'?page=login&error=redirect_wrong_app');
	}

	//////////////////////////////////////////////////
	// Set up environment to use app
	//////////////////////////////////////////////////
	else if(!empty($copy_session['user']['user_id']) && isset($_GET['page']))
	{
		////////////////////////////////////////////////////////////////
		// set page so correct page template and logic can be imported in
		// the index page
		////////////////////////////////////////////////////////////////
		$page = $_GET['page'];
		

		// find if qc info is available for this page.  If so an icon will be added to tool bar
		$qc_page = $qc_db->listAll('qc_db-qc-page-info-by-name', $page);

		///////////////////////////////////////////////////////////////
		// load phpMussel-1
			// Designed to detect trojans, viruses, malware and other threats 
			// phpMussel activates when $_FILES not empty.
			// Turned off for upload_NGS_data since the tsv from the ion torrent triggers an Office chameleon attack.  Most likely this happens because the file is named with multiple extensions (.amplicon.cov.xls)  Since the files uploaded on upload_NGS_data page are not being stored on the server and the files are checked for a specific format this should be ok.  Another possible solution to this is to change phpMussel config.ini file by turning of allow_leading_trailing_dots or chamelon_to_doc.  I will try this first because turning off these checks seems more hazardous because it will be turned off for files which are actually stored on the server.
		///////////////////////////////////////////////////////////////		  
		if ($page !== 'upload_NGS_data')
		{			
			require_once('/var/www/phpMussel-1/loader.php');
		}

		////////////////////////////////////////////////////////////////
		// Set user id for application
		////////////////////////////////////////////////////////////////
		define('USER_ID', $copy_session['user']['user_id']);

		////////////////////////////////////////////////////////////////
		// Update last active time
		////////////////////////////////////////////////////////////////
		session_start();
		$_SESSION['user']['last_active_time'] = time();
		session_write_close(); 

		////////////////////////////////////////////////////////////////
		// Log current session WORKING ON THIS
		////////////////////////////////////////////////////////////////
		// $add_session_array = array();
		// $add_session_array['user_id'] = USER_ID;
		// $add_session_array['page'] = $page;
		// $db->addOrModifyRecord('session_table', );

		// get users permissions
		$user_permssions = $db->listAll('user-permissions', USER_ID);

		// for users that do not have any advanced permissions
		if (!isset($user_permssions[0]['permissions']))
		{
			$user_permssions = '';
		}
		else
		{

			$user_permssions = $user_permssions[0]['permissions'];
		}
		
		////////////////////////////////////////////////////////////////
		// There are permissions like admins that people are notified of everyone with this permission 
		// throughout the APP.  Below a list of names will be provided
		////////////////////////////////////////////////////////////////
		// get admins
		$all_admins_str = $utils->ConvertAllUsersWithAPermissionToStr($db->listAll('get-all-users-with-a-permission', 'admin'), 'and');
		define('ADMINS', $all_admins_str);

		/////////////////////////////////////////////////////////////////////////////
		// some pages do not need the stepsTracker loaded.  It is only necessary for 
		// report making not searching.  Pre steps needs different steps than report
		// building.  
		/////////////////////////////////////////////////////////////////////////////

		/////////////////////////////////////////////////////////////////////////////
		// Report building Steps
		/////////////////////////////////////////////////////////////////////////////	

		$stepsForSNVReporting = array();
		$stepsNoSNVReporting = array();

		// Make a string of all values in the GET array to make links to other pages.
		define('EXTRA_GETS', $utils->allGetsInStr(array('page', 'ngs_stage', 'variant_filter', 'previous_page', 'genes', 'coding', 'amino_acid_change', 'header_status', 'revise')));	
		
		if (isset($_GET['orderable_tests_id']))
		{
			$curr_test_reporting_flow_steps = $db->listAll('reporting-flow-steps-by-test-id',$_GET['orderable_tests_id']);

			foreach ($curr_test_reporting_flow_steps as $key => $step)
			{
				if ($step['no_variant_status'] === 'show')
				{
					array_push($stepsNoSNVReporting, $step['step_name']);
				}

				array_push($stepsForSNVReporting, $step['step_name']);
			}
		}
		else if (isset($_GET['ngs_panel_id']))
		{
			$curr_test_reporting_flow_steps = $db->listAll('reporting-flow-steps-by-ngs-panel-id',$_GET['ngs_panel_id']);
			
			foreach ($curr_test_reporting_flow_steps as $key => $step)
			{
				if ($step['no_variant_status'] === 'show')
				{
					array_push($stepsNoSNVReporting, $step['step_name']);
				}

				array_push($stepsForSNVReporting, $step['step_name']);
			}
		}
		
		$controlSteps = array(		
			'review_control'
		);

		$pre_steps = array(
				'add_visit',
				'add_patient',
				'pre_pending',
				'add_DNA_conc',
				'add_amplicon_conc',
				'upload_NGS_data',
				'pre_complete',
				'check_tsv_upload'										
			);

		$qc_variant_steps = array(
				'qc_strand_bias',
				'QC_variant_status'
				// 'name_checker',
				// 'hapMap_background'
				// 'IDs',
				// 'frequency_in_DB',
				// 'review_qc_variant'
		);

		// $onPanelStepRegulators is used for ngs panel flow of the reporting system.  All steps that are on will be concatenated in $onPanelStepRegulators[0]['on_panel_steps']
		if (isset($_GET['visit_id']))
		{
			$visitArray = $db->listAll('add-visit-info', $_GET['visit_id']);

			// Find all the step regulators for the ngs_panel
			if (isset($visitArray[0]['ngs_panel_id']) && !empty($visitArray[0]['ngs_panel_id']))
			{
				$onPanelStepRegulators = $db->listAll('on-panel-step-regulators', $visitArray[0]['ngs_panel_id']);
			}			
		}

		// pool building steps do not have visit id but do have ngs_panel_id so step regulators can be pull this way
		else if (isset($_GET['ngs_panel_id']) && !empty($_GET['ngs_panel_id']))
		{
			$onPanelStepRegulators = $db->listAll('on-panel-step-regulators', $_GET['ngs_panel_id']);
		}

		if (isset($_GET['patient_id']))
		{
			$patientArray = $db->listAll('add-patient-info', $_GET['patient_id']);
		}

		if 	(
				(
					in_array($page, $stepsForSNVReporting) || 
					$page === 'show_edit_report' || 
					$page === 'generate_report' || 
					$page === 'stop_reporting' || 
					$page === 'qc'|| 
					$page === 'confirmations' ||
					$page === 'send_confirmation_email' ||
					$page === 'message_board' || 
					$page === 'QC_variant_status' || 
					$page === 'review_control' || 
					$page === 'qc_strand_bias' || 
					$page === 'change_report_type' ||
					$page === 'download_report'
				)  && isset($_GET['run_id'])
			)
		{
			$ttk_vw_cols = $db->listAll('get-table-col-names', 'knowledge_base_table');		
			
			if (empty($visitArray))
			{
				header('Location:'.REDIRECT_URL.'?page=add_visit');
			}

			//////////////////////////////////////////////////////////////////////
			// Get genes used for the report as of the date the data was uploaded.
			// pre_step_visit table has a step called upload_NGS_data	
			//////////////////////////////////////////////////////////////////////
			// Get upload_NGS_data_date.  If visit_id not found use today's date			
			if (isset($visitArray[0]['visit_id']))
			{
				// Get upload_NGS_data.  If not found use time_stamp in visitArray
				$upload_NGS_data_date = $db->listAll('upload_ngs_date_pre_step', $visitArray[0]['visit_id']);
				
				// If upload date not found use time_stamp in visitArray
				if (isset($upload_NGS_data_date[0]['upload_date']))
				{
					$upload_NGS_date = $upload_NGS_data_date[0]['upload_date'];
				}
				else
				{
					$dt = new DateTime($visitArray[0]['time_stamp']);
					$upload_NGS_date = $dt->format("Y-m-d");
				}		
			}
			else
			{
				$upload_NGS_date = date("Y-m-d");
			}
			

			/////////////////////////////////////////////////////////////////////////////
			// If no variants are found then remove some steps which are not required
			/////////////////////////////////////////////////////////////////////////////
			if (isset($_GET['variant_filter']) )
			{
				switch ($_GET['variant_filter'])
				{
					// This is the the same as All variants button.
					case 'passed':
						$variantArray = $db->listAll('run-variants-passed-filter', array('run_id'=>$_GET['run_id'], 'upload_NGS_date' =>$upload_NGS_date, 'ngs_panel_id'=>$visitArray[0]['ngs_panel_id']));
						$knowledge_base = $db->listAll('knowledge-base-passed-filter', $_GET['run_id']);	
						break;

					// I do not think this is being used anymore 2019-09-03 ST
					case 'failed':
						$variantArray = $db->listAll('run-variants-failed-filter', array('run_id'=>$_GET['run_id'], 'upload_NGS_date' =>$upload_NGS_date, 'ngs_panel_id'=>$visitArray[0]['ngs_panel_id']));
						$knowledge_base = $db->listAll('knowledge-base-failed-filter', $_GET['run_id']);
						break;

					// This is default on verify variants page under the included button
					case 'included':				
						$variantArray = $db->listAll('run-variants-included-filter', array('run_id'=>$_GET['run_id'], 'upload_NGS_date' =>$upload_NGS_date, 'ngs_panel_id'=>$visitArray[0]['ngs_panel_id']));
						$knowledge_base = $db->listAll('knowledge-base-included-filter', $_GET['run_id']);
						break;
				}
			}
			else
			{

				$variantArray = $db->listAll('run-variants-included-filter', array('run_id'=>$_GET['run_id'], 'upload_NGS_date' =>$upload_NGS_date, 'ngs_panel_id'=>$visitArray[0]['ngs_panel_id']));

				$knowledge_base = $db->listAll('knowledge-base-included-filter', $_GET['run_id']);			
			}
	
			////////////////////////////////////////////////////////////////////////
			// Get info required to report MUTANTS
			////////////////////////////////////////////////////////////////////////
			// find required genes to report.  Reports with diagnosis with key it $required_report_genes require a table to include
			$mutantTracker = new MutantTracker($visitArray[0]['tumor_type'], $visitArray[0]['ngs_panel'], $variantArray, $visitArray[0]['received']);
			$reportMutantTables = $mutantTracker->getReportTable();
			$extraGenesRequired = $mutantTracker->getExtraGenesRequired();
			$reportGeneOrder = $mutantTracker->getReportGeneOrder();
			$reportVariantOrder = $mutantTracker->getReportVariantOrder();
			$unknownSigGenes = $mutantTracker->GenesWhichAreOnlyUnknownSignificance();	

			///////////////////////////////////////////////////////////////////////
			// Set everything up depending on if snvs are present or not
			///////////////////////////////////////////////////////////////////////
			$num_snvs = sizeof($variantArray);

			if (isset($visitArray) && !empty($visitArray[0]['sample_type']) && ($visitArray[0]['sample_type'] === 'Positive Control' || $visitArray[0]['sample_type'] === 'Negative Control'))
			{
				$stepTracker = new StepTracker($db, $controlSteps, 'step_run_xref');			
			}

			else if ($num_snvs === 0)
			{
				$stepTracker = new StepTracker($db, $stepsNoSNVReporting, 'step_run_xref');
			}

			// there are SNVs to be included in report
			else
			{
				$stepTracker = new StepTracker($db, $stepsForSNVReporting, 'step_run_xref');				
			}		

			$stepTracker->AddStepsToStepTable('report_steps_table');
		}	

		/////////////////////////////////////////////////////////////////////////////
		// pre Steps.  Some of the steps are shared between both.  Differing layout is 
		// taken care of in template.
			// The steptracker is used in add_test to keep track of ngs samples both add_visit and add_patient needs to be tracked.
		////////////////////////////////////////////////////////////////////////////	
		if 	(
				in_array($page, $pre_steps)  || $page === 'home' || $page === 'patient_found' ||
				$page === 'add_test' || $page === 'pool_upload_data'
			)
		{		
			$preStepTracker = new StepTracker($db, $pre_steps, 'pre_step_visit');
		}
	}

?>
