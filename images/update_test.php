<?php
	
	$page_title = 'Update Test Status';

	// find everyone with reviewer permission
	$log_book_users = $db->listAll('users-permissions-log-book');

	$users_log_book = $db->listAll('users-permissions-non-ngs-wet-bench');

	if (isset($_GET['ordered_test_id']) && isset($_GET['sample_log_book_id']))
	{
		$sampleLogBookArray = $db->listAll('all-samples-in-log-book-by-sample-log-book-id',$_GET['sample_log_book_id']);

		// Get info about data entered about the test
		$orderedTestArray = $db->listAll('ordered-test-info', $_GET['ordered_test_id']);

		if (isset($orderedTestArray[0]['orderable_tests_id']))
		{
			// Get info about the test that was ordered
			$testInfo = $db->listAll('orderable-tests', $orderedTestArray[0]['orderable_tests_id']);	

			if (isset($testInfo[0]['test_name']) && $testInfo[0]['test_name'] === 'DNA Extraction')
			{
				$sprite_instruments = $db->listAll('instrument-groups-description', 'gDNA Extractor');
			}
			else
			{
				$thermal_cyclers = $db->listAll('instrument-groups-description', 'Thermal cycler');
			}

			// find all of the sanger sequencing instruments for if Sanger was used
			if (isset($testInfo[0]['assay_name']) && strpos($testInfo[0]['assay_name'], 'Sanger') !== false)
			{
				$sanger_sequencers = $db->listAll('instrument-groups-description', 'Sanger Sequencer');
			}				
		}
			
			
	}

	// submit extraction tests
	if 	(
			isset($_POST['update_test_submit']) && 
			isset($testInfo[0]['test_name']) && 
			$testInfo[0]['test_name'] === 'DNA Extraction' && 
			isset($_GET['ordered_test_id']) && 
			isset($_GET['sample_log_book_id'])
		)
	{
		
		// Example POST
			// 'task_date' => 
			//   array (size=3)
			//     'extraction_aliquoted' => string '2019-04-29' (length=10)
			//     'pk_digestion' => string '2019-05-06' (length=10)
			//     'extraction_performed_by' => string '2019-05-06' (length=10)
			// 'task_initials' => 
				// array (size=3)
				//  'extraction_aliquoted' => 
				//    array (size=3)
				//      0 => string '12' (length=2)
				//      1 => string '8' (length=1)
				//      2 => string '1' (length=1)
				//  'pk_digestion' => 
				//    array (size=1)
				//      0 => string '12' (length=2)
				//  'extraction_performed_by' => 
				//    array (size=1)
				//      0 => string '12' (length=2)
			// 'task_time' => 
			//   array (size=1)
			//     'pk_digestion' => string '13:25' (length=5)
			// 'extraction_method' => string 'SPRI-TE' (length=7)
			// 'sprite_instruments' => string '1' (length=1)
			// 'stock_conc' => string '10.2' (length=4)
			// 'dilution_exist' => string 'yes' (length=3)
			// 'dilution_conc' => string '2.5' (length=3)
			// 'update_test_submit' => string 'Submit' (length=6)

		///////////////////////////////////////////////////////////
		// This data is going to be put into 4 different tables
			// extraction_log_table
				// extraction_method
				// stock_conc
				// dilution_exist
				// dilution_conc
			// users_performed_task_table
				// data in task arrays.  Start with task_date array and find corresponding data in task_initials and task_time
					// user_id -> task_initials array
					// task_id -> find in db task_table
					// ref_id  -> extraction_log_id
					// ref_table -> extraction_log_table
					// date_performed -> task_date array
					// time_performed -> task_time array
			// task_table Search to use in users_performed_task_table
				// +---------+-------------------------+
				// | task_id | task                    |
				// +---------+-------------------------+
				// |       1 | extraction_aliquoted    |
				// |       2 | pk_digestion            |
				// |       3 | extraction_performed_by |
				// +---------+-------------------------+
			// instrument_used_table
				// use sprite_instruments 
					// user_id -> user of web app
					// instrument_id -> sprite_instruments val
					// ref_id -> extraction_log_id
					// ref_table -> extraction_log_table
			// ordered_test_table
				// Mark test as finished
					// search for ordered_test
					// update:
						// finish_date
						// test_status
		///////////////////////////////////////////////////////////

		///////////////////////////////////////////////////////////
		// Add extraction_log_table
				// user_id -> web app user
				// extraction_method
				// stock_conc
				// dilution_exist
				// dilution_conc
		///////////////////////////////////////////////////////////
		$add_extraction_array = array(
			'ordered_test_id'	=> 	$_GET['ordered_test_id'],
			'user_id'			=>  	USER_ID,
			'extraction_method'	=> 	$_POST['extraction_method'],
			'stock_conc'		=>	$_POST['stock_conc'],
			'dilution_exist'	=>	$_POST['dilution_exist'],
			'dilutions_conc'	=>	$_POST['dilution_conc'],
			'times_dilution'	=> 	$_POST['times_dilution']
		);

		$add_extraction_result = $db->addOrModifyRecord('extraction_log_table',$add_extraction_array);

		if ($add_extraction_result[0])
		{	
			///////////////////////////////////////////////////////////
			// users_performed_task_table
				// data in task arrays.  Start with task_date array and find corresponding data in task_initials and task_time
					// user_id -> task_initials array
					// task_id -> find in db task_table
					// ref_id  -> extraction_log_id
					// ref_table -> extraction_log_table
					// date_performed -> task_date array
					// time_performed -> task_time array

				// 'task_date' => 
				//   array (size=3)
				//     'extraction_aliquoted' => string '2019-04-29' (length=10)
				//     'pk_digestion' => string '2019-05-06' (length=10)
				//     'extraction_performed_by' => string '2019-05-06' (length=10)
				// 'task_initials' => 
					// array (size=3)
					//  'extraction_aliquoted' => 
					//    array (size=3)
					//      0 => string '12' (length=2)
					//      1 => string '8' (length=1)
					//      2 => string '1' (length=1)
					//  'pk_digestion' => 
					//    array (size=1)
					//      0 => string '12' (length=2)
					//  'extraction_performed_by' => 
					//    array (size=1)
					//      0 => string '12' (length=2)
				// 'task_time' => 
				//   array (size=1)
				//     'pk_digestion' => string '13:25' (length=5)
			///////////////////////////////////////////////////////////

			// iterate over all tasks in task_date array and find corresponding info in 
			// task_initials and task_time array
			foreach ($_POST['task_date'] as $task => $date_performed)
			{
				// skip adding task if data performed not entered
				if (!empty($date_performed))
				{
					// find task id
					// task_table Search to use in users_performed_task_table
					// +---------+-------------------------+
					// | task_id | task                    |
					// +---------+-------------------------+
					// |       1 | extraction_aliquoted    |
					// |       2 | pk_digestion            |
					// |       3 | extraction_performed_by |
					// +---------+-------------------------+
					$task_id = $db->listAll('task-id-by-task', $task);
					$task_id = isset($task_id[0]['task_id']) ? $task_id[0]['task_id'] : 0;
					
					$time_performed = null;

					// time_performed -> task_time array
					if (isset($_POST['task_time'][$task]))
					{
						$time_performed = $_POST['task_time'][$task];
					}

					// Add a task per user in users_performed_task_table
					if (isset($_POST['task_initials'][$task]))
					{
						foreach ($_POST['task_initials'][$task] as $key => $user_id)
						{
							// if no task_initials are selected add task 
							$add_performed_task_array = array(
								'user_id'			=>	$user_id,
								'task_id' 		=>	$task_id,
								'ref_id'  		=>	$add_extraction_result[1],
								'ref_table' 		=> 	'extraction_log_table',
								'date_performed' 	=>	$date_performed,
								'time_performed' 	=>	$time_performed
							);
							
							$performed_task_result = $db->addOrModifyRecord('users_performed_task_table',$add_performed_task_array);
						
						}
					}

					// Add a task with out a user
					else
					{
						// if no task_initials are selected add task 
						$add_performed_task_array = array(
							'user_id'			=>	0,
							'task_id' 		=>	$task_id,
							'ref_id'  		=>	$add_extraction_result[1],
							'ref_table' 		=> 	'extraction_log_table',
							'date_performed' 	=>	$date_performed,
							'time_performed' 	=>	$time_performed
						);
						
						$performed_task_result = $db->addOrModifyRecord('users_performed_task_table',$add_performed_task_array);
					}
				}				
			}

			///////////////////////////////////////////////////////////
			// instrument_used_table ONLY APPLYS IF EXTRACTION METHOD IS SPRITE
				// use sprite_instruments 
					// user_id -> user of web app
					// instrument_id -> sprite_instruments val
					// ref_id -> extraction_log_id
					// ref_table -> extraction_log_table
			///////////////////////////////////////////////////////////

			if (isset($_POST['sprite_instruments']))
			{
				$instrument_add_array = array(
					'user_id'			=> 	USER_ID,
					'instrument_id'	=> 	$_POST['sprite_instruments'],
					'ref_id'  		=>	$add_extraction_result[1],
					'ref_table' 		=> 	'extraction_log_table'
				);

				$instrument_result = $db->addOrModifyRecord('instrument_used_table',$instrument_add_array);		
			}

			///////////////////////////////////////////////////////////
			// ordered_test_table
				// Mark test as finished
					// search for ordered_test
					// update:
						// finish_date
						// test_status
			///////////////////////////////////////////////////////////
			$orderedTestResult = $db->listAll('ordered-test-only-by-id', $_GET['ordered_test_id']);

			if (isset($orderedTestResult) && !empty($orderedTestResult))
			{
				$update_array = $orderedTestResult[0];
				$update_array['finish_date'] = date('Y-m-d');
				$update_array['test_status'] = 'complete';	
				$instrument_result = $db->addOrModifyRecord('ordered_test_table',$update_array);
			}
			
			// redirect to log book
			header('Location:'.REDIRECT_URL.'?page=all_sample_tests&sample_log_book_id='.$_GET['sample_log_book_id']);
		}
	}

	// submit all other types of tests except NGS and extraction
	else if 	(
				isset($_POST['update_test_submit']) && 
				isset($testInfo[0]['test_name']) && 
				$testInfo[0]['test_name'] !== 'DNA Extraction' && 
				isset($_GET['ordered_test_id']) &&
				isset($_GET['sample_log_book_id'])
			)
	{
		////////////////////////////////////////////////////////////////////	
		// This data is going to be put into 3 different tables
		// ordered_test_table 
			// NOTE: make sure the test start date is not dependent on the end of another test orderable_tests_table (turnaround_time_depends_on_ordered_test == yes)
				// start_date
				// finish_date
				// expected_turnaround_time
				// test_status
				// test_result
		// users_performed_task_table
			// data in task_initials array.
				// user_id -> task_initials array
				// task_id -> find in db task_table
				// ref_id  -> ordered_test_id
				// ref_table -> ordered_test_table
				// date_performed -> empty
				// time_performed -> empty
		// instrument_used_table
				// use $_POST[instruments] array
					// 'instruments' => 
					//     array (size=2)
					//       'thermal_cyclers' => 
					//         array (size=2)
					//           0 => string '3' (length=1)
					//           1 => string '4' (length=1)
					//       'sanger_sequencers' => 
					//         array (size=1)
					//           0 => string '14' (length=2)
				// fields
					// user_id -> user of web app
					// instrument_id -> val of nested array example $_POST[instruments][thermal_cyclers][0]
					// ref_id -> ordered_test_id
					// ref_table -> ordered_test_table
		////////////////////////////////////////////////////////////////////


		////////////////////////////////////////////////////////////////////
		// ordered_test_table 
		// NOTE: make sure the test start date is not dependent on the end of another test orderable_tests_table (turnaround_time_depends_on_ordered_test == yes)
			// start_date
			// finish_date
			// expected_turnaround_time
			// test_status
			// test_result
		////////////////////////////////////////////////////////////////////

		$orderedTestResult = $db->listAll('ordered-test-only-by-id', $_GET['ordered_test_id']);

		if (isset($orderedTestResult) && !empty($orderedTestResult))
		{
			$update_array = $orderedTestResult[0];
			$update_array['finish_date'] = date('Y-m-d');
			$update_array['test_status'] = 'complete';	

			if (isset($_POST['test_result']))
			{
				$update_array['test_result'] = $_POST['test_result'];
			}
			
			////////////////////////////////////////////////////////////////////	
			// set expected_turnaround_time from test_turnaround_table				
				// time_depends_on_result
			////////////////////////////////////////////////////////////////////
			$turn_around_time = $db->listAll('turn-around-time', $update_array['orderable_tests_id']);
			
			$expected_turnaround_time = 0;
			if 	(	
					isset($testInfo[0]['previous_positive_required']) && 
					$testInfo[0]['time_depends_on_result'] === 'yes' 
				)
			{				
				// find if test_result is in $turn_around_time[key]['dependent_result']
				foreach ($turn_around_time as $key => $times)
				{
					if (strpos($_POST['test_result'], $times['dependent_result']) !== false)
					{
						$update_array['expected_turnaround_time'] = $times['turnaround_time'];
					}
				}
			}
			else if (sizeOf($turn_around_time) == 1)
			{
				$update_array['expected_turnaround_time'] = $turn_around_time[0]['turnaround_time'];
			}


			////////////////////////////////////////////////////////////////////
			// update start_date 
				// table abbreviations
					// orderable_tests_table otst
					// sample_log_book_table slbt
				// case
					// - otst.turnaround_time_depends_on_ordered_test == yes
					// - a test ordered for otst.dependency_orderable_tests_id with this slbt.sample_log_book_id within 1 day of this tests start_date
					// - if test ordered for otst.dependency_orderable_tests_id with this slbt.sample_log_book_id is completed
				// result
					// If meets case above change start_date to otst.dependency_orderable_tests_id with this slbt.sample_log_book_id end_date  
				// current tests otst.turnaround_time_depends_on_ordered_test == yes
					// BRAF depends on EGFR 
			////////////////////////////////////////////////////////////////////
			if (isset($testInfo[0]['previous_positive_required']) && 
				$testInfo[0]['time_depends_on_result'] === 'yes')
			{
				$searchArray = array(
					'sample_log_book_id' => $_GET['sample_log_book_id'],
					'dependency_orderable_tests_id' => $testInfo[0]['dependency_orderable_tests_id'],
					'test_start_date' => $orderedTestArray[0]['start_date']
				);			
				
				$dependencySearch = $db->listAll('dependency-ordered-within-day-completed', $searchArray);

				// if a result is return it means all of the cases are met and start date
				// should be updated to dependency_test finish date
				if (!empty($dependencySearch))
				{			
					$update_array['start_date'] = $dependencySearch[0]['finish_date'];
				}
			}

			$orderedTestResult = $db->addOrModifyRecord('ordered_test_table',$update_array);

			////////////////////////////////////////////////////////////////////
			// users_performed_task_table
			// data in task_initials array.
				// user_id -> task_initials array
				// task_id -> find in db task_table
				// ref_id  -> ordered_test_id
				// ref_table -> ordered_test_table
				// date_performed -> empty
				// time_performed -> empty
			// iterate over task_initials adding each user and task to 
			// user_performed_task_table
				// 'task_initials' => 
				//     array (size=2)
				//       'wet_bench' => 
				//         array (size=1)
				//           0 => string '12' (length=2)
				//       'analysis' => 
				//         array (size=1)
				//           0 => string '12' (length=2)
			////////////////////////////////////////////////////////////////////
			if (isset($_POST['task_initials']))
			{
				foreach ($_POST['task_initials'] as $task => $users)
				{
					$task_id = $db->listAll('task-id-by-task', $task);
					$task_id = isset($task_id[0]['task_id']) ? $task_id[0]['task_id'] : 0;
					foreach ($users as $key => $user_id)
					{
						$add_performed_task_array = array(
							'user_id'		=> 	$user_id,
							'task_id'		=> 	$task_id,
							'ref_id'		=>	$_GET['ordered_test_id'],
							'ref_table'	=>	'ordered_test_table'
						);

						$performedTaskResult = $db->addOrModifyRecord('users_performed_task_table',$add_performed_task_array);
					}
				}
			}

			////////////////////////////////////////////////////////////////////
			// instrument_used_table
				// use $_POST[instruments] array
					// 'instruments' => 
					//     array (size=2)
					//       'thermal_cyclers' => 
					//         array (size=2)
					//           0 => string '3' (length=1)
					//           1 => string '4' (length=1)
					//       'sanger_sequencers' => 
					//         array (size=1)
					//           0 => string '14' (length=2)
				// fields
					// user_id -> user of web app
					// instrument_id -> val of nested array example $_POST[instruments][thermal_cyclers][0]
					// ref_id -> ordered_test_id
					// ref_table -> ordered_test_table
			////////////////////////////////////////////////////////////////////
			
			// add thermal_cyclers
			if (isset($_POST['instruments']))
			{
				foreach ($_POST['instruments'] as $instrument_type => $instruments)
				{
					
					foreach ($instruments as $key => $instrument_id)
					{
						$add_instrument_used_array = array(
							'user_id'		=> 	USER_ID,
							'instrument_id'=> 	$instrument_id,
							'ref_id'		=>	$_GET['ordered_test_id'],
							'ref_table'	=>	'ordered_test_table'
						);
						
						$usedInstrumentResult = $db->addOrModifyRecord('instrument_used_table',$add_instrument_used_array);
					}
				}
			}

			header('Location:'.REDIRECT_URL.'?page=all_sample_tests&sample_log_book_id='.$_GET['sample_log_book_id']);
		}
		else
		{
			$message = 'Something went wrong';
		}
	}
?>