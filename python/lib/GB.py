import os
import sys
import sqlite3


def init():
	'''
		Initialize all global variables
	'''
	global author, maintainer, email, status, fs_ngs_save_loc, ofa_amplicon_bed_file

	author = 'Samantha Taffner'
	maintainer = 'Samantha Taffner'
	email = 'samantha_taffner@urmc.rochester.edu'
	status = 'Development'

	fs_ngs_save_loc = '//bhw-vm001.circ.rochester.edu/Storage/fs-ngs/dev'

	ofa_amplicon_bed_file = '/var/www/urmc_reporter_data/ofa_bed_files/ofa_amplicons_db_v1.1.1.txt'
