from __future__ import print_function
import re
import os
import errno 
import datetime
import time
import fnmatch
from time import gmtime, strftime, sleep
import io
import json
import shutil
import subprocess
import sqlite3
from sqlite3 import Error
import string
import random
import hashlib
import math

###########################################################
# slurm functions
###########################################################
def run_list_sbatches(all_sbatches):
	'''
		(list) -> run sbatch

		a list of sbatch files can be run using this function.
	'''

	for sb in all_sbatches: 
		# run sbatch file
		# start individual runs on own core
		proc = subprocess.Popen(['sbatch %s'%(sb), '/etc/services'], stdout=subprocess.PIPE, shell=True)
		out, err = proc.communicate()
		
		# get jobid
		jobid = int(out.rstrip().split(' ')[-1])
		print('Started %s as an Sbatch under jobid %s'%(sb, jobid))


###########################################################
# mutalyzer functions
###########################################################
def findProteinNameMutalyzer(transcript, coding):
	# https://mutalyzer.nl/json/runMutalyzer?variant=NM_000222.2:c.1698_1727delCAATTATGTTTACATAGACCCAACACAACTinsTAATTATGTTTACATAGACCCAACACAACC
	import json
	import requests

	print('https://mutalyzer.nl/json/runMutalyzer?variant=%s:%s'%(transcript, coding))
	response = requests.get('https://mutalyzer.nl/json/runMutalyzer?variant=%s:%s'%(transcript, coding), verify = False)
	response.raise_for_status()
	print('###############################################\nRun mutalyzer on %s %s\n###############################################\n'%(transcript, coding))
	
	json_response = response.json()

	
	if response.status_code != 200:
		return False
	
	json_keys = json_response.keys()

	if 'proteinDescriptions' in json_keys:

		# proteinDescriptions example: [u'NM_000222.2(KIT_i001):p.(Leu576Pro)']
		protein_descriptions = json_response['proteinDescriptions'][0]
		spilt_descript = protein_descriptions.split(':')[-1].replace('(','').replace(')','')
		print('mutalyzer says: %s Extracted to: %s'%(protein_descriptions, spilt_descript))

		return spilt_descript
	else:
		return False


###########################################################
# misc
###########################################################
def validate_date_full_month_name_date(date_text):
	'''
		Thermofisher makes dates like this
			u'June 18, 2020'
			u'February 14, 2020'

		Make sure that the date has the full month name
	'''
	try:
		datetime.datetime.strptime(date_text, '%B %d, %Y')
	except ValueError:
		return False
	return True

def convert_full_month_date_YYYYMMDD(date_text):
	'''
		Thermofisher makes dates like this
			u'June 18, 2020'
			u'February 14, 2020'

		convert to YYYYMMDD
	'''

	if validate_date_full_month_name_date(date_text):
		datetime_obj = datetime.datetime.strptime(date_text, '%B %d, %Y')

		YYYYMMDD = datetime_obj.strftime('%Y%m%d')
		return YYYYMMDD

	else:
		return False

def get_date_time_for_file_name():
	'''
	'''
	return datetime.datetime.fromtimestamp(time.time()).strftime('%Y_%m_%d_%H%M%S')

def get_date_for_file_name():
	'''
	'''
	return datetime.datetime.fromtimestamp(time.time()).strftime('%Y_%m_%d')

def system_call_get_stdout(command):
	'''
		(os command) -> stdout
	'''
	p = subprocess.Popen([command], stdout=subprocess.PIPE, shell=True)
	return p.stdout.read().rstrip().lstrip()

def unzip(zipped_dir, output_dir):
	'''
		(directory, folder) -> unzipped
	'''

	# check if output dir exists make it if it does not exists
	make_directories(output_dir)

	command = 'unzip %s -d %s'%(zipped_dir, output_dir)

	print('Update: Unzipping %s \n'%(zipped_dir))
	print(command)

	# unzip dir
	os.system(command)

def quiet_unzip(zipped_dir, output_dir):
	'''
		(directory, folder) -> unzipped
	'''

	# check if output dir exists make it if it does not exists
	make_directories(output_dir)

	command = 'unzip -qqo %s -d %s'%(zipped_dir, output_dir)

	# unzip dir
	os.system(command)


def num_task_manager(file_name):
	'''
		For samples running in parallel use this function to find if all samples completed a task.  

		returns True if complete
		returns False if not all samples are completed
	'''
	# add complete num to task_file
	with open(file_name, 'a') as af:
		af.write(' 1')

	# figure out if complete task num is equal to total tasks
	with open(file_name, 'r') as rf:
		total_expected = int(rf.readline().rstrip())
		tally = rf.readline().rstrip().split(' ')
		total_tally = reduce(lambda x, y: int(x) + int(y), tally ) 

		return total_expected == total_tally

def tracker_to_num_samples(file_name):
	with open(file_name, 'r') as rf:
		return int(rf.readline().rstrip())

###########################################################
# list functions
###########################################################

def list_to_char_seperated(in_list, char, newline=True):
	'''
	(list, str, bool) -> str
	Purpose:  Accept a list and return a char separated str

	Newline is turned to False the list char will not be a newline

	>>> list_to_char_seperated(['hello', 'world'], ' ', False) 
	'hello world'
	>>> list_to_char_seperated([1, 'world'], ' ', False) 
	'1 world'
	
	'''
	char_line = ''
	len_list = len(in_list)

	for i in range(len_list):
		if i == len_list - 1 and not newline:
			char_line += '%s'%(in_list[i])
		elif i == len_list -1 and newline:
			char_line += '%s\n'%(in_list[i])
		else:
			char_line += '%s%s'%(in_list[i], char)
	return char_line

def unique_line(in_list):
	'''
		Find unique entries in a list.

		Steps:
			1. convert to a set
			2. convert back to a list

		>>> sorted(unique_line([1,2,1,4]))
		[1, 2, 4]
		>>> sorted(unique_line(['a','b','a']))
		['a', 'b']
		>>> sorted(unique_line(['a','b','a', 1, 2, 1, 4]))
		[1, 2, 4, 'a', 'b']
		
	'''
	mySet = set(in_list)
	return list(mySet)

def list_to_dict(in_list, sep):
	'''
		(list) -> dict
		sep (separator) example (=)
		example input list
			['ID=cds4910', 'Name=AEG99850.1', 'Parent=gene5019', 'Note=COG2197 Response regulator containing a CheY-like receiver domain and an HTH DNA-binding domain', 'Dbxref=NCBI_GP:AEG99850.1', 'gbkey=CDS', 'product=hypothetical protein', 'protein_id=AEG99850.1', 'transl_table=11\n']
		example output dict
			{
				'ID':'cds4910', 
				'Name':'AEG99850.1', 
				'Parent':'gene5019', 
				'Note':'COG2197 Response regulator containing a CheY-like receiver domain and an HTH DNA-binding domain', 
				'Dbxref':'NCBI_GP:AEG99850.1', 
				'gbkey':'CDS', 
				'product':'hypothetical protein', 
				'protein_id':'AEG99850.1', 
				'transl_table':'11\n'
			}
		>>> list_to_dict(['ID=cds4910', 'Name=AEG99850.1'], '=') == {'ID': 'cds4910', 'Name':'AEG99850.1'}
		True

	'''
	out_dict = {}

	for l in in_list:
		split_l = l.split(sep)
		out_dict[split_l[0]] = split_l[1]

	return out_dict

def list_to_chunks(in_list, num_chunks):
	'''
		
		>>> list_to_chunks([1,2,3,4,5,6,7,8,9,10], 2)
		[[1, 2, 3, 4, 5], [6, 7, 8, 9, 10]]
		>>> list_to_chunks([1,2,3,4,5,6,7,8,9], 2)
		[[1, 2, 3, 4, 5], [6, 7, 8, 9]]
		>>> list_to_chunks([1,2,3,4,5,6,7,8,9,10], 3)
		[[1, 2, 3, 4], [5, 6, 7, 8], [9, 10]]
		>>> list_to_chunks([1,2,3,4,5,6,7,8,9], 3)
		[[1, 2, 3], [4, 5, 6], [7, 8, 9]]
		>>> list_to_chunks([393, 438, 988, 1011, 2920, 2941], 4)
		[[393], [438], [988], [1011, 2920, 2941]]
		>>> list_to_chunks([393, 438, 988, 1011, 2920, 2941], 8)
		[[393], [438], [988], [1011], [2920], [2941]]
		>>> list_to_chunks([393, 438, 988, 1011, 2920, 2941], 6)
		[[393], [438], [988], [1011], [2920], [2941]]
	'''
	chunks = []
	k = 0
	chunk_size = len(in_list) / num_chunks
	if len(in_list) % num_chunks == 1:
		chunk_size += 1

	# if num_chunks is longer than in_list just separate each 
	# part of the list into it's own chunk
	if len(in_list) < num_chunks:

		return list_to_chunks(in_list, len(in_list))


	for i in xrange(0,num_chunks):

		# if there are extra vars in in_list which are not yet added and i is equal to num_chunks add to last list
		if i == num_chunks-1:
			chunks.append(in_list[k:len(in_list)])

		else:
			chunks.append(in_list[k:k+chunk_size])
		k = k+chunk_size

	return chunks

def two_lists_to_dict(key_list, value_list):
	'''
		>>> two_lists_to_dict(['key1', 'key2', 'key3'], ['val1', 'val2', 'val3']) == {'key1':'val1', 'key2':'val2', 'key3':'val3'}
		True
		>>> two_lists_to_dict(['key1', 'key2', 'key3'], ['val1', 'val2']) == {'key1':'val1', 'key2':'val2', 'key3':''}
		True
	'''
	out_dict = {}
	for i in range(0, len(key_list)):

		# make sure value list is as long as key_list.  If value missing add '' to out_dict
		if len(value_list) - 1 >= i:
			out_dict[key_list[i]] = value_list[i]  
		else:
			out_dict[key_list[i]] = ''

	return out_dict

###########################################################
# string functions
###########################################################

def index_first_cap_letter(in_string):
	'''
		(string) -> int

		>>> index_first_cap_letter('131_Escherichia_coli_JJ1886_uid226103_NC_022648')
		4
		>>> index_first_cap_letter('Klebsiella_oxytoca_strain_M1,Klebsiella_oxytoca_KONIH1')
		0
	'''
	cap_letters = string.ascii_uppercase

	for i in range(0, len(in_string)-1):

		if in_string[i] in cap_letters:
			return i

def represents_int(in_string):
	'''
		>>> represents_int('1')
		True
		>>> represents_int(1)
		True
		>>> represents_int(0.5)
		True
		>>> represents_int('0.5')
		False
		>>> represents_int('string')
		False
	'''
	try: 
		int(in_string)
		return True
	except ValueError:
		return False

def replace_matching_chars(in_str):
	'''
		(str) -> str
		Accept a str which is used in pattern Matching and replace
		all of the special chars

		>>> replace_matching_chars('*junctions.bed')
		'junctions.bed'
		>>> replace_matching_chars('!@#$%^&*()hello')
		'hello'
		>>> replace_matching_chars('hel!@#$%^&*()lo!@#$%^&*()')
		'hello'
	'''

	output = re.sub(r'[^a-zA-Z0-9 \n\.]', '', in_str)
	return output

def is_number(in_var):
	'''
		>>> is_number('123')    # positive integer number
		True

		>>> is_number('123.4')  # positive float number
		True

		>>> is_number('-123')   # negative integer number
		True

		>>> is_number('-123.4') # negative `float` number
		True

		>>> is_number('abc')    # `False` for "some random" string
		False
	'''
	# Type-casting the string to `float`.
	# If string is not a valid `float`, 
	# it'll raise `ValueError` exception
	try:
		float(in_var)

	except ValueError:
		return False

	return True

def is_float(in_var):
	'''
		>>> is_float('1.5')
		True
		>>> is_float('1.0')
		False
		>>> is_float(1.5)
		True
		>>> is_float('1')
		False
		>>> is_float(1)
		False
	'''
	try:
		a = float(in_var)

		if a.is_integer():
			return False

	except ValueError:
		return False

	else:
		return True

def is_int(in_var):
	'''
		>>> is_int('1.5')
		False
		>>> is_int('1.0')
		True
		>>> is_int(1.5)
		False
		>>> is_int('1')
		True
		>>> is_int(1)
		True
	'''
	try:
		a = float(in_var)
		b = int(a)

	except ValueError:
		return False

	else:
		return a == b

###########################################################
# dict functions
###########################################################

def dict_to_json(dict_to_dump, json_file, variable_name):
	'''
		(dict, str, str) -> write json to file
	'''
	of = open(json_file, 'w')
	of.write('var %s='%(variable_name))

	j = json.dumps(dict_to_dump)
	of.write(j)
	of.close()

###########################################################
# Search functions
###########################################################

def divde_and_conquer(in_list, search_term):
	'''
		(sorted list, int) -> bool

		odd len list
		>>> divde_and_conquer([1,2,5,7,20], 5)
		True
		>>> divde_and_conquer([1,2,5,7,20], 6)
		False

		even len list 
		>>> divde_and_conquer([1,2,5,20], 5)
		True
		>>> divde_and_conquer([1,2,5,20], 6)
		False

		find at end of list
		>>> divde_and_conquer([1,2,5,20], 20) 
		True
		>>> divde_and_conquer([1,2,5,7,20], 20)
		True

		find at beginning of list
		>>> divde_and_conquer([1,2,5,20], 1)
		True
		>>> divde_and_conquer([1,2,5,7,20], 1)
		True
	'''
	# make sure in_list is a list.  If not return False
	if not isinstance(in_list, list):
		return False

	j = 0
	k = len(in_list) -1
	
	# make sure not at the ends of list
	if in_list[j] == search_term:
		return True

	elif in_list[k] == search_term:
		return True

	while k > j:
		
		avg_i = (j + k) // 2

		if (j+k)%2 == 1:
			avg_i += 1

		# check to see if search_term greater or less
		curr_search_val = in_list[avg_i]

		if search_term > curr_search_val:
			j = avg_i

		elif search_term < curr_search_val:
			k = avg_i

		elif search_term == curr_search_val:
			return True

		if k - j == 1:
			k = j - 1

	return False			

###########################################################
# db functions
###########################################################
def db_connect(db_file):
	""" create a database connection to a SQLite database """
	
	return sqlite3.connect(db_file)

###########################################################
# update user functions
###########################################################
def update_user(msg):
	'''
		(str) -> print message

		This function just prints an update in a standard format
	'''

	print('\n###############################\nUPDATE:\n\t%s\n\t%s\n###############################\n\n'%(long_form_time_now(), msg))

def cmd_update_user(cmd):
	'''
		(str) -> print message
	'''

	print('\nRUNNING COMMAND:\n\t%s\n\t%s\n\n'%(long_form_time_now(), cmd))

###########################################################
# Time functions
###########################################################

def long_form_time_now():
	'''
		() -> str

		This function will return current date and time

		format: year_month_date_hour_minute_second
	'''
	return strftime('%Y_%m_%d %H:%M:%S')

def wait_until(interval, function, stop_condition, *args, **kwargs):
	'''
	    (int, function, ?) ->
	    call a function until a conditions is met at a set interval
	'''
	stop_condition_met = False

	# wait a time equal to interval
	now = time.ctime()
	future = time.ctime(time.time() + interval)
	count = 0

	while not stop_condition_met:

		# wait until the interval of time has passed
		if now > future:
			count += 1
		if count == 10:
			count = 0
			
		# check if function returns stop condition
		curr_condtion = function(*args, **kwargs)

		# if stop condition stop loop
		if curr_condtion == stop_condition:
			stop_condition_met = True

		# reset conditions
		else:
			future = time.ctime(time.time() + interval)
			now = time.ctime()

def time_now():
	'''
		() -> time
	'''

	return time.time()

def time_passed(old_time):
	'''
		(time) -> time
	'''
	return time.time() - old_time

###########################################################
# files and directory functions
###########################################################	
def check_files_same(f_1, f_2):
	'''
		(file, file) -> bool
		check if two files are the same
	'''
	md5_1 = hashlib.md5(open(f_1,'rb').read()).hexdigest()
	md5_2 = hashlib.md5(open(f_2,'rb').read()).hexdigest()
	return md5_1 == md5_2

def md5_large_file(in_file):
	'''
		Large files need to be broken up to take an md5.
	'''

	if not os.path.isfile(in_file):
		return False

	# make a md5 hash object
	hash_md5 = hashlib.md5()

	# open file as binary and read only
	with open(in_file, 'rb') as f:
		i = 0

		# read 4096 bytes at a time and take the md5 hash of it and add it to the hash total
		# b converts string literal to bytes
		for chunk in iter(lambda: f.read(4096), b''):
			i += 1

			# get sum of md5 hashes
			hash_md5.update(chunk)

		# check if md5 correct
		md5_chunk_file = hash_md5.hexdigest()

	return md5_chunk_file


def tab_sep_file_to_col_list(in_file, col_num, sep):
	'''
		(file, int, str) -> list
		
		Get a unique sorted list of everything in a col from a (sep) separated file

		>>> tab_sep_file_to_col_list('lib/test_files/remove_phage_test.txt', 1, '\\t')
		[393, 438, 988, 1011, 2920, 2941]
	'''

	with open(in_file, 'r') as of:
		
		out_list = []

		line = of.readline().rstrip()

		while line != '':

			split_line = line.split(sep)

			# if column string is int or float convert
			if is_int(split_line[col_num]):
				out_list.append(int(split_line[col_num]))

			elif is_float(split_line[col_num]):
				out_list.append(float(split_line[col_num]))

			else:
				out_list.append(split_line[col_num])

			line = of.readline().rstrip()

	# get unique list
	out_list = list(set(out_list))

	return sorted(out_list)

def file_col_exclude_line_nums(in_file, col_num, sep, not_in_list):
	'''
		(file, int, str) -> list
		
		Take a separated file (sep) find the column associated with col_num.  If the line number of the file is in the not_in_list do not include in final list. 
		
		line number starts with 0

		>>> file_col_exclude_line_nums('lib/test_files/remove_phage_test.txt', 1, '\\t', [2,4])
		[393, 438, 1011, 2941]

	'''

	with open(in_file, 'r') as of:
		
		out_list = []

		line = of.readline().rstrip()

		line_num = 0
	
		while line != '':

			# make sure the line number isn't in the not_in_list if it is ignore and continue
			if not_in_list == [] or not divde_and_conquer(not_in_list, line_num):

				split_line = line.split(sep)

				# if column string is int or float convert
				if is_int(split_line[col_num]):
					out_list.append(int(split_line[col_num]))

				elif is_float(split_line[col_num]):
					out_list.append(float(split_line[col_num]))

				else:
					out_list.append(split_line[col_num])

			line = of.readline().rstrip()
			line_num += 1

	# get unique list
	out_list = list(set(out_list))

	return sorted(out_list)

def files_in_dir(in_dir):
	'''
		(str) -> list
		Take an input directory and return all files directly inside the directory ignoring other directories

		 sorted(files_in_dir(os.getcwd()))
		['README.md', 'make_snplist_matrix.py', 'mapping_stats.py', 'micro_pipeline.sql', 'micro_pipeline_SNP_Calling_v2.py', 'micro_pipeline_v2.py', 'micro_workbook.sql', 'put_fastq_in_folders.py', 'taffner_snp_matrix.py', 'test_doctests.py']

	'''
	list_of_files = []
	for file in os.listdir(in_dir):
		if os.path.isfile(os.path.join(in_dir,file)):
			list_of_files.append(file)

	return list_of_files

def dirs_in_dir(in_dir):
	'''
		(str) -> list
		Take an input directory and return all dirs directly inside the directory ignoring files
	'''
	list_of_dirs = []
	for inner_dir in os.listdir(in_dir):
		if os.path.isdir(os.path.join(in_dir,inner_dir)):
			list_of_dirs.append(os.path.join(in_dir,inner_dir))

	return list_of_dirs

def find_fastq_files_inside_dir(in_dir):
	'''
		(list, int, dir) -> str, str

		[staffner@bluehive 6_S6]$ ls -1
			6_S6_contigs.fasta
			6_S6_L001_R1_001.fastq.gz
			6_S6_L001_R2_001.fastq.gz
			consensus.fasta
			consensus.vcf
			duplicate_reads_metrics.txt
			metrics
			reads.all.pileup
			reads.sam
			reads.sorted.bam
			reads.sorted.deduped.bam
			reads.unsorted.bam
			var.flt.vcf

		(['.fastq', '.fastq.gz'], '6_S6') -> '/scratch/staffner/test_micro_pipeline/orginal/Escherichia_coli/6_S6/6_S6_L001_R2_001.fastq.gz', '/scratch/staffner/test_micro_pipeline/orginal/Escherichia_coli/6_S6/6_S6_L001_R1_001.fastq.gz'

	'''
	list_file_extension = ['.fastq', '.fastq.gz', '.fq']
	for_file = ''
	rev_file = ''
	
	# iterate over all the files in the in_dir and see if they end with any of the supplied extensions
	# This only works because hg19_filtered_R1_trimmed.fq.gz does not end with an extension found here.
	for file in os.listdir(in_dir):
		for exten in list_file_extension:
			if file.endswith(exten):
				if '_R1' in file:
					for_file = in_dir + '/' + file
				elif '_R2' in file:
					rev_file = in_dir + '/' + file
				break

	return for_file, rev_file

def force_rebuild_steps(force_tag, sample_directory):
	'''
		Certain steps save intermediate files.  If force tags are turned on 
		remove these files from the input directory.

		tags
			-force_retrim_spades
				removes _contigs.fasta
				removes _plasmid_contigs.fasta
				removes hg19_filtered_R1_trimmed.fq.gz
				removes hg19_filtered_R2_trimmed.fq.gz

			-force_spades
				removes _contigs.fasta
				removes _plasmid_contigs.fasta	

			-force_remap
				removes folder and all of its contents where intermediate
				mapping data was saved.		
	'''
	if force_tag == 'force_retrim_spades':
		# find and remove assembly files find plasmid contigs first so it can be deleted first.  Since _contigs.fast is found in the _plasmid_contigs.fasta name
		del_file(sample_directory, '_plasmid_contigs.fasta')
		del_file(sample_directory, '_contigs.fasta')
		
		# find and remove hg19_filtered and trimmed files
		del_file(sample_directory, 'hg19_filtered_R1_trimmed.fq.gz')
		del_file(sample_directory, 'hg19_filtered_R2_trimmed.fq.gz')

	elif force_tag == 'force_spades':
		# find and remove assembly files find plasmid contigs first so it can be deleted first.  Since _contigs.fast is found in the _plasmid_contigs.fasta name
		del_file(sample_directory, '_plasmid_contigs.fasta')
		del_file(sample_directory, '_contigs.fasta')

	elif force_tag == 'force_remap':
		delete_directory(sample_directory)

def del_file(sample_directory, match):
	'''	
		Remove a file with a match if only one file with match is found
	'''
	rm_file = get_files_with_match(sample_directory, match)
	
	if len(rm_file) == 1:
		cmd = 'rm %s'%(rm_file[0])
		os.system(cmd)

def ensure_files_inside_dir(list_file_extension, num_appearances, in_dir):
	'''
		(list, int, dir) -> bool

		[staffner@bluehive 6_S6]$ ls -1
			6_S6_contigs.fasta
			6_S6_L001_R1_001.fastq.gz
			6_S6_L001_R2_001.fastq.gz
			consensus.fasta
			consensus.vcf
			duplicate_reads_metrics.txt
			metrics
			reads.all.pileup
			reads.sam
			reads.sorted.bam
			reads.sorted.deduped.bam
			reads.unsorted.bam
			var.flt.vcf

		(['.fastq', '.fastq.gz'], 2, '6_S6') -> True
		(['.fastq', '.fastq.gz'], 3, '6_S6') -> False
	'''

	count_found = 0
	
	# iterate over all the files in the in_dir and see if they end with any of the supplied extensions
	for file in os.listdir(in_dir):
		for exten in list_file_extension:
			if file.endswith(exten):
				count_found += 1
				break

	return count_found == num_appearances

def ensure_folder_in_dir(in_dir, folder_name):
	'''
		(str, str) -> bool, str
		find a folder in a directory.  If found return True and folder full address otherwise return False and ''
	'''

	all_dirs = dirs_in_dir(in_dir)

	for d in all_dirs:

		if os.path.basename(d) == folder_name:
			return True, d

	return False, ''

def file_name_with_extension(input_file_path):
	'''
		(file with path) -> file name with extension

		>>> file_name_with_extension('path/to/file/foobar.txt')
		'foobar.txt'
		>>> file_name_with_extension('path/to/file/foobar.test.txt')
		'foobar.test.txt'

	'''
	return os.path.basename(input_file_path)

def file_checker(file_address, file_type):
	'''
		Check to see if the input file contains everything necessary

		File types:
			1. junctions
				- check if a header is present
				- Check if file is tab seperated
				- check if the first 2 Junction lines contains 12 inputs
				chr1	4774433	4776458	JUNC00000001	2	-	4774433	4776458	255,0,0	2	83,49	0,1976

			2. gtf
				- Check to make sure gene_id is present
				- Make sure tab seperated
				- At least 9 tab sperated inputs exists
				- That the attribute is ; seperated
	'''

	# Open file
	f = open(file_address, 'r')

	# vars
	header = False
	correct_type = False

	# check the first 3 lines
	for i in range(3):
		line = f.readline()
		tab_line = line.split('\t')

		# Check for junctions file requirments
		if file_type == 'junctions':

			# Check if header is present
			if i == 0:

				# check if a header is present in the file
				if len(tab_line) == 1 and 'track' in tab_line[0]:
					header = True

			# Check line
			else:
				if len(tab_line) == 12:
					correct_type = True

				# Just in case the 1st line is correct and the 2nd isn't
				else:
					correct_type = False

		# Check gtf file
		if file_type == 'gtf':

			# check if tab seperated and a least 9 inputs
			# Check if attribute is ; seperated
			attribute = tab_line[8]

			if len(tab_line) >= 9 and len(attribute.split(';')) > 2:

				# Check if attribute contains gene_id
				if 'gene_id' in attribute:
					correct_type = True

				# Just in case the 1st line is correct and the 2nd isn't
				else:
					correct_type = False



	return header, correct_type

	# Close File
	f.close()

def make_new_file_name(file_dir, add_name, root_name, file_type):

	# remove any file extensions
	root_name = root_name.split('.')[0]
	return '%s/%s%s.%s'%(file_dir, root_name, add_name, file_type)	

def make_bed_header(nam):
	'''
		make_bed_header('foo')
		'track name=foo description="foo"\n'
	'''
	return 'track name=%s description="%s"\n'%(nam, nam)	

def make_directories(make_dir):
	
	# it seems like sometimes another file makes the directory at the same time and then this errors out.  if not os.path.exists(make_dir):

	try:
		os.makedirs(make_dir)
	except OSError:
		pass

def put_fastq_in_folders(run_folder):
	'''
		urmc miseq output puts all fastq.gz files in Data/Intensities/BaseCalls along with other 
		files and folders.  
	
		take all .fastq.gz files and place them in folders

		Input
			run_folder (Pecora121417)
				[staffner@bluehive Pecora121417]$ ls -1
				10_S10_L001_R1_001.fastq.gz
				10_S10_L001_R2_001.fastq.gz
				11_S11_L001_R1_001.fastq.gz
				11_S11_L001_R2_001.fastq.gz

		Out put
			run_folder (Pecora121417)
				10_S10
					10_S10_L001_R1_001.fastq.gz
					10_S10_L001_R2_001.fastq.gz
				11_S11
					11_S11_L001_R1_001.fastq.gz
					11_S11_L001_R2_001.fastq.gz

		Assumptions:
			L001 is in every sample

	'''
	# get all fastq files with .fastq.gz extension
	fastq_files = get_files_with_extension(run_folder, 'fastq.gz')

	# find common sample names
	# iterate over list of files split at _L001_
	for i in range(0, len(fastq_files)):
		f = fastq_files[i].split(run_folder)[-1].replace('/', '')

		# find sample name before _L001_
		sample_name = f.split('_L001_')[0]

		# See if sample name has a folder in run_folder or make a new one 
		sample_folder = run_folder + '/' + sample_name
		make_directories(sample_folder)

		# add sample to sample_folder
		cmd = 'mv %s %s'%(fastq_files[i], run_folder + '/' + sample_name + '/' + f)
		os.system(cmd)

def delete_directory(dir_address):
	'''
		(str) -> remove dir

		Purpose: take an address to a dir and delete all files
		and delete directory
	'''
	# make sure directory exists
	if os.path.exists(dir_address):
		# list all files in directory
		files_in_dir = os.listdir(dir_address)

		# delete each file in the directory
		for f in files_in_dir:
			if os.path.isdir(dir_address + '/' + f):
				delete_directory(dir_address + '/' + f)
			else:
				os.remove(dir_address + '/' + f)

		# Delete directory
		os.rmdir(dir_address)

def get_files_with_extension(directory, extension):
	'''
		(directory, str) -> list of full address to files which match extensions
	'''
	result_list = []

	for f in os.listdir(directory):

		if f.endswith('.' + extension):
			result_list.append(os.path.join(directory, f))

	return result_list

def get_files_with_match(directory, match):
	'''
		finds all files within directory with a name containing the match

		 get_files_with_match('/scratch/staffner/test_micro_pipeline/orginal/Enterobacter_aerogenes/URMC_200_E_aer', '_contigs.fasta')
		['/scratch/staffner/test_micro_pipeline/orginal/Enterobacter_aerogenes/URMC_200_E_aer/URMC_200_E_aer_contigs.fasta']
	'''
	matches = []

	for filename in fnmatch.filter(os.listdir(directory), '*%s*'%(match)):

		matches.append(os.path.join(directory, filename))

	return matches

def recursively_find_files(directory, match):
	'''
		Finds all files within all subdirectories with a name containing the match

		 recursively_find_files('/scratch/staffner/test_micro_pipeline/orginal/cre_project', '_contigs.fasta')
		[]
	'''
	matches = []
	for root, dirnames, filenames in os.walk(directory):
		for filename in fnmatch.filter(filenames, '*%s*'%(match)):
			matches.append(os.path.join(root, filename))
	return matches

def move_files(files_to_move, directory):
	'''
		(list, directory) -> move files
	'''
	for f in files_to_move:
		shutil.move(f, os.path.join(directory, f.split('/')[-1]))

def get_root_name(file_type, file_name):

	return file_name.split('/')[-1].replace(file_type, '').split('.')[0]

def check_list_files_exist(list_files):
	'''
		(list) -> bool

		Find if all files in a list exist.  If they do return True otherwise return False
	'''

	for f in list_files:
		# if a file is not found return false
		if not os.path.isfile(f):
			return False

	return True

def file_last_line(in_file):
	'''
		(file) -> str

		Get the last line of a file
	'''

	with open(in_file, 'rb') as f:

		# Jump to the second last byte.
		f.seek(-2, os.SEEK_END)

		# Until the end of the line is found
		while f.read(1) != b'\n':
			# backup until a newline charcter is found
			f.seek(-2, os.SEEK_CUR)

		# now go forward to get the last line
		last = f.readline()
	return last

def backup(backup_file, backup_dir):
	'''
		(file, dir) -> copy file

		Example use: Copy micro_pipeline.db to home directory (/home/staffner/Documents/micro_pipeline_db_backups)
	'''

	##########################################################
	# make new file name for backup including current date, time,random number 2018_03_06_113116_201
	##########################################################
	# get directory get extension
	root, ext = os.path.splitext(backup_file)

	# get file name without extension of backup_file
	file_root = root.split('/')[-1]

	# get current time
	curr_time = datetime.datetime.fromtimestamp(time.time()).strftime('%Y_%m_%d_%H%M%S')

	# random number
	random_num = random.randint(1,500)

	# new file name 
	new_file = '%s/%s_%s_%s%s'%(backup_dir.rstrip('/'), file_root, curr_time, random_num, ext)
	
	##########################################################
	# copy file
	##########################################################
	cmd = 'cp %s %s'%(backup_file, new_file)
	os.system(cmd)

	manange_backups(backup_dir, file_root, 5)

def manange_backups(backup_dir, root_name, num_files=5):
	'''
		Keep number of backups equal to num_files.
		Default 5
	'''

	all_backups = get_files_with_match(backup_dir, root_name)

	# if there are more than 5 backups delete files older than days old
	if len(all_backups) > 5:

		new_files = newest_files(all_backups, num_files)

		current_time = time.time()

		for f in all_backups:

			# make sure file is not one of the newest files
			if f not in new_files:

				os.unlink(f)

def newest_files(list_files, num_files):
	'''
		(list, int) -> list

		return a list of the newest files based on time of creation.
	'''
	newest_files = []			

	# find all the newest files
	for f in range(0,num_files):

		# find the current newest file
		curr_newest = max(list_files, key=os.path.getctime)

		# add to newest_files list 
		newest_files.append(curr_newest)

		# remove curr_newest file from list of files
		list_files.remove(curr_newest)

	return newest_files

def make_file_or_append_to_file(file_name, add_text):
	'''
		Either make a new file if it doesn't exist or append to an existing
		file
	'''
	# check if directory exists.  If it doesn't make the directory
	make_directories(os.path.dirname(file_name))

	if not os.path.isfile(file_name):
		of = open(file_name, 'w')

	else:
		of = open(file_name, 'a')
		add_text = '\n\n' + add_text

	of.write(add_text)

	of.close()

def num_lines_in_file(file_name):
	'''
		(str) -> int
		Return how many lines are in the file
	'''

	with open(file_name) as f:
		for i, l in enumerate(f):
			pass
	return i + 1

###########################################################
# biology functions
###########################################################	

def gff_line_loc_name(line):
	'''
		Example line: 
			NC_000913.3	FIG	CDS	337	2799	.	+	1	ID=fig|6666666.296471.peg.1;Name=Aspartokinase (EC 2.7.2.4) / Homoserine dehydrogenase (EC 1.1.1.3);Ontology_term=KEGG_ENZYME:2.7.2.4,KEGG_ENZYME:1.1.1.3

		>>> gff_line_loc_name("NC_000913.3\\tFIG\\tCDS\\t337\\t2799\\t.\\t+\\t1\\tID=fig|6666666.296471.peg.1;Name=Aspartokinase (EC 2.7.2.4) / Homoserine dehydrogenase (EC 1.1.1.3);Ontology_term=KEGG_ENZYME:2.7.2.4,KEGG_ENZYME:1.1.1.3\\n")
		('337', '2799', 'Aspartokinase (EC 2.7.2.4) / Homoserine dehydrogenase (EC 1.1.1.3)')

		>>> gff_line_loc_name("NC_000913.3\\tFIG\\tCDS\\t337\\t2799\\t.\\t+\\t1\\tName=Aspartokinase (EC 2.7.2.4) / Homoserine dehydrogenase (EC 1.1.1.3);Ontology_term=KEGG_ENZYME:2.7.2.4,KEGG_ENZYME:1.1.1.3;ID=fig|6666666.296471.peg.1;\\n")
		('337', '2799', 'Aspartokinase (EC 2.7.2.4) / Homoserine dehydrogenase (EC 1.1.1.3)')

		>>> gff_line_loc_name("NC_000913.3\\tFIG\\tCDS\\t337\\t2799\\t.\\t+\\t1\\tOntology_term=KEGG_ENZYME:2.7.2.4,KEGG_ENZYME:1.1.1.3;ID=fig|6666666.296471.peg.1;\\n")
		('337', '2799', '')
	'''

	tab_split = line.rstrip().split('\t')

	split_elms = tab_split[-1].split(';')

	curr_name = ''
	
	for elm in split_elms:

		if 'Name=' in elm:

			curr_name = elm.replace('Name=', '')

	return tab_split[3], tab_split[4], curr_name
