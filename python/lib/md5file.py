import os
import time

import utils

class md5file():
	'''
		This class will make a md5 file.
	'''
	def __init__(self, out_dir, library_pool_id, GB):

		self.out_dir = out_dir
		self.start_time = time.time()
		self.library_pool_id = library_pool_id
		self.md5s_all_match = True

		print('sample backing up: %s'%(self.out_dir))
		self.md5_dir = '%s/md5s'%(self.out_dir)

		utils.make_directories(self.md5_dir)

		# make out files
		self.md5_file = '%s/ir_backup.tsv'%(self.md5_dir)
		self.md5_of = self.init_md5_f() 

		self.tracker_f = '%s/ir_backup_tracker.tsv'%(self.md5_dir)
		self.tracker_of = self.init_tracker() 		
		self.GB = GB

	def init_md5_f(self):
		
		if os.path.exists(self.md5_file):

			of = open(self.md5_file, 'a', 0)

		else:
			of = open(self.md5_file, 'w+', 0)
			header = ['Original_file', 'Original_md5', 'cp_file', 'cp_md5', 'match', 'file_type', 'sample_name', 'time']
			of.write(utils.list_to_char_seperated(header, '\t'))
		return of

	def init_tracker(self):
		# the tracker file keeps track of how long it took to successfully transfer all files and records it along with library_pool_id 
		
		# If the file is already made the header does not need to be made again
		if not os.path.exists(self.tracker_f):

			# 0 should write to file instantly
			# https://stackoverflow.com/questions/18984092/python-2-7-write-to-file-instantly
			of = open(self.tracker_f, 'w+', 0)
			header = ['library_pool_id', 'start_time', 'finsh_time', 'status', 'elapsed_time']
			of.write(utils.list_to_char_seperated(header, '\t'))

		else:
			of = open(self.tracker_f, 'a', 0)
		
		return of

	def checkIfExistsOrError(self, check_loc):

		if not os.path.exists(check_loc):
			err = 'file does not exist %s'%(check_loc)
			self.write_tracking_error(err) 
			self.GB.parser.error(err)
		else:
			return True

	def recordNewTransfer(self, original_loc, new_loc, sample_name, transfer_type):
		'''
			Check that original location exists
			Copy original to new location
			Check that MD5s match.  
			If not delete copy and give five tries before reporting an error
		'''

		if not os.path.exists(original_loc):
			print('\n!!!!!!!!!!!!!!!!!!!!!!!\nERROR: The original_loc file %s does not exist'%(original_loc))
			orginial_md5 = 'None'

		match = False
		count = 0

		while not match:
			count +=1
			# This will keep track of all 
			if count > 5:
				print('\n!!!!!!!!!!!!!!!!!!!!!!!\nERROR: MD5s do not match. After 5 trys giving up.')
				self.md5s_all_match = False
				break

			print('\n###########################\n\tCopying \n\tsample_name: %s\n\ttransfer_type: %s\n\ttry #: %s\n###########################\n'%(sample_name, transfer_type, count))
			# check that original location exists
			self.checkIfExistsOrError(original_loc)

			# copy original to new location
			cmd = 'cp %s %s'%(original_loc, new_loc)
			print('%s\n'%(cmd))
			os.system(cmd)

			# For some reason sometimes the cp does not work. Therefore check before removing 
			if not os.path.exists(new_loc) or not os.path.exists(original_loc):				
				print('\n!!!!!!!!!!!!!!!!!!!!!!!\nERROR: the new_loc file did not copy %s or the original_loc file %s does not exist'%(new_loc, original_loc))
				new_md5 = 'None'
				orginial_md5 = 'None'
				continue

			orginial_md5 = utils.md5_large_file(original_loc)

			new_md5 = utils.md5_large_file(new_loc)

			match = orginial_md5 == new_md5

			# delete file and try again
			if not match:

				print('\n!!!!!!!!!!!!!!!!!!!!!!!\nERROR: MD5s do not match.  Deleting new file and trying again. \n\torginial_md5: %s new_md5: %s'%(orginial_md5, new_md5))

				os.remove(new_loc)

		to_write = utils.list_to_char_seperated([original_loc, orginial_md5, new_loc, new_md5, match, transfer_type, sample_name, time.time()], '\t')

		print('Record Transfer: %s'%(to_write))

		self.md5_of.write(to_write)
		
		return match

	def finish_tracking(self):
		# Tracker file should be at same loc so just append
		end_time = time.time()

		self.tracker_of.write(utils.list_to_char_seperated([self.library_pool_id, self.start_time, end_time, self.md5s_all_match, '%.2f'%(end_time - self.start_time)], '\t'))

		self.tracker_of.close()
		self.md5_of.close()

	def write_tracking_error(self, err):
		'''
			Call this function to report an error that occurred since the error log is being filled with warnings from unzipping.
		'''
		print('ERROR found: %s'%(err))
		end_time = time.time()

		self.tracker_of.write(utils.list_to_char_seperated([self.library_pool_id, self.start_time, end_time, err, '%.2f'%(end_time - self.start_time)], '\t'))

		self.tracker_of.close()
		self.md5_of.close()