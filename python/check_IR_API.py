from __future__ import print_function
#!/usr/bin/python

# Libraries
import argparse
import os
import sys
import subprocess
import textwrap as _textwrap
import json
from datetime import date

from lib import GB
# call GB init to make globals
GB.init()

from lib import utils
from lib import md5file

def ir_api_per_sample():
	'''
		(Mayo13, Mjk5OWY1YmJlODc1Y2FkYTgzMGVkZDJiZjdhNDI4MjA3MmYwMmIxN2IyOWZlYzVhMThmNzAzYTQ4OTliOWRjMg, https://10.149.59.209/api/v1/) -> 2 zip files are made and 
	'''
	import subprocess
	import json
	
	api_curl = 'curl -s --request GET -k -H "Authorization:'
	
	header = '--header "Content-Type:application/x-www-form-urlencoded" '
	 
	api_cmd = 'analysis?format=json&name='

	#######################################################################################
	# Access the samples via the Ion Reporter API and analysis command
	# example sample successfully IR complete: Mayo13
		# curl command used via python's subprocess module:
			# curl -s --request GET -k -H "Authorization: Mjk5OWY1YmJlODc1Y2FkYTgzMGVkZDJiZjdhNDI4MjA3MmYwMmIxN2IyOWZlYzVhMThmNzAzYTQ4OTliOWRjMg " --header "Content-Type:application/x-www-form-urlencoded" "https://10.149.59.209/api/v1/analysis?format=json&name=Mayo13"
		# Before running this analysis command there were no .zip files present for this sample
		# After running the analysis command two zip files now are available.
			# Mayo13_v1_c2580_2019-12-18-13-38-37-379_2020-01-13_13-35-47-228_Filtered.zip
			# Mayo13_v1_c2580_2019-12-18-13-38-37-379_2020-01-13_13-35-44-502_All.zip
		# The API returns a JS like the one below with addresses to these files
		# [
		#      {
		#           "data_links": {
		#                "filtered_variants": "https://smh-o1-1-2150-2f65182.urmc-sh.rochester.edu:443/api/v1/download?filePath=/data/IR/data/IR_Org/molecdiag@urmc.edu/Mayo13_v1/Mayo13_v1_20191218133854182/Mayo13_v1_c2580_2019-12-18-13-38-37-379_2020-01-13_13-59-36-442_Filtered.zip",
		#                "unfiltered_variants": "https://smh-o1-1-2150-2f65182.urmc-sh.rochester.edu:443/api/v1/download?filePath=/data/IR/data/IR_Org/molecdiag@urmc.edu/Mayo13_v1/Mayo13_v1_20191218133854182/Mayo13_v1_c2580_2019-12-18-13-38-37-379_2020-01-13_13-59-34-041_All.zip"
		#           },
		#           "flagged": false,
		#           "id": "ff8081816baf0c96016f1a4e47021021",
		#           "ion_reporter_version": "5.10",
		#           "name": "Mayo13_v1_c2580_2019-12-18-13-38-37-379",
		#           "report_published": "",
		#           "reports": {
		#                "final": {},
		#                "qc": {
		#                     "link": "https://smh-o1-1-2150-2f65182.urmc-sh.rochester.edu:443/api/v1/download?type=pdf&filePath=/scratch/tmp/IR_Org/download/pdf/3905945b-25be-4180-9d2b-fbdd995c110d/ff8081816baf0c96016f1a4e47021021_QC.pdf"
		#                }
		#           },
		#           "samples": {
		#                "PROBAND": "Mayo13_v1"
		#           },
		#           "shared_with": [],
		#           "stage": "Send for Report Generation",
		#           "start_date": "December 18, 2019",
		#           "started_by": "Molec Diag",
		#           "status": "SUCCESSFUL",
		#           "variants_saved": "",
		#           "workflow": "OFA_5-10_complex_v3_filter_updated"
		#      }
		# ]

	# Response if sample is still finishing up in Torrent server and not start analysis yet
	# {
 	#     "error": "CONTAINER with given name: Analysis doesn't exists. does not exist"
	# }

	######################################################################################
	# Example: curl -s --request GET -k -H "Authorization: Mjk5OWY1YmJlODc1Y2FkYTgzMGVkZDJiZjdhNDI4MjA3MmYwMmIxN2IyOWZlYzVhMThmNzAzYTQ4OTliOWRjMg " --header "Content-Type:application/x-www-form-urlencoded" "https://10.149.59.209/api/v1/analysis?format=json&name=A_4306-19_J0053557_1009_921_100"

	os_cmd = '%s %s " %s"%s%s%s"'%(api_curl, GB.args.api_token, header, GB.args.api_server, api_cmd, GB.args.sample)
		
	output = subprocess.check_output(os_cmd, shell=True)
	analysis_json = json.loads(output)

	if len(analysis_json) > 0 and 'samples' in analysis_json[0].keys() and 'PROBAND' in analysis_json[0]['samples'].keys() and 'data_links' in analysis_json[0].keys() and 'filtered_variants' in analysis_json[0]['data_links'].keys():
		
		# Thermofisher replaces spaces with _
		proband_sample = analysis_json[0]['samples']['PROBAND'].replace(' ', '_')
		print('########################\nCurl API call worked\nproband_sample:\n\t%s\ndata_links:\n\t%s\n########################'%(proband_sample, analysis_json[0]['data_links']['filtered_variants']))

		backupIRSampleData(analysis_json[0]['data_links']['filtered_variants'], GB.args.sample, proband_sample)

	else:
		GB.parser.error('Sample %s had an error with the api %s\n\n\nos_cmd = %s'%(GB.args.sample, json.dumps(analysis_json, indent=5, sort_keys=True), os_cmd))

def backupIRSampleData(zipped_file, sample_name, proband):
	'''
		Run this function on each sample after the ir_api run

		This function will copy the tsv, vcf, and bam file so the out_dir and return a list of tuples of md5s for each file copied.

		Example list of tuples returned:
		[('Original_file', 'Original_md5', 'cp_file', 'cp_m5', 'match', 'file_type', 'sample_name'), ('/data/IR/data/IR_Org/molecdiag@urmc.edu/Mayo13_v1/Mayo13_v1_20191218133854182/Mayo13_v1_c2580_2019-12-18-13-38-37-379_2020-01-13_13-59-36-442_Filtered.zip', 'e896bed86e77ad3e9f3c51ca9f7e9ae1', '/share/backup_data/test_cp_dir/temp/Mayo13.zip', 'e896bed86e77ad3e9f3c51ca9f7e9ae1', True, 'zip_variant_dir', 'Mayo13'), ('/share/backup_data/test_cp_dir/temp/Mayo13/Variants/Mayo13_v1/Mayo13_v1-full.tsv', '6d0d4fd43dd4f8e087f696a700f6b7d4', '/share/backup_data/test_cp_dir/variant_folder/Mayo13_filtered.tsv', '6d0d4fd43dd4f8e087f696a700f6b7d4', True, 'variant_tsv', 'Mayo13'), ('/share/backup_data/test_cp_dir/temp/Mayo13/Workflow_Settings/Analysis_Settings/analysis_settings.txt', '9c28a8b5a99af8fd7c68c2ab19768c93', '/share/backup_data/test_cp_dir/temp/Mayo13/Variants/Mayo13_v1/analysis_settings/Mayo13_analysis_settings.txt', '9c28a8b5a99af8fd7c68c2ab19768c93', True, 'variant_settings', 'Mayo13'), ('/data/IR/data/IR_Org/molecdiag@urmc.edu/Mayo13_v1/Mayo13_v1_20191218133854182/outputs/VariantCallerActor-00/VariantOutput.filtered.vcf', 'e29b1562f2f41baaf86dad21cf4ef850', '/share/backup_data/test_cp_dir/vcf/Mayo13.vcf', 'e29b1562f2f41baaf86dad21cf4ef850', True, 'vcf', 'Mayo13'), ('/data/IR/data/IR_Org/molecdiag@urmc.edu/Mayo13_v1/Mayo13_v1_20191218133854182/outputs/VariantCallerActor-00/merged.bam.ptrim.bam', 'ad86f0fca8da588a45fe26aca5df1546', '/share/backup_data/test_cp_dir/bam/Mayo13.bam.ptrim.bam', 'ad86f0fca8da588a45fe26aca5df1546', True, 'bam', 'Mayo13'), ('/data/IR/data/IR_Org/molecdiag@urmc.edu/Mayo13_v1/Mayo13_v1_20191218133854182/outputs/VariantCallerActor-00/merged.bam.ptrim.bam.bai', '63200d28a19b45707eb532a923c3cd5f', '/share/backup_data/test_cp_dir/bam/Mayo13.bam.ptrim.bam.bai', '63200d28a19b45707eb532a923c3cd5f', True, 'bai', 'Mayo13')
	'''

	# Move to a temporary location and unzip folder because of permission settings on orginal parent
	# folder it needs to be outside of that folder
	cp_zip = '%s/%s.zip'%(GB.args.o, sample_name)

	# the drive is mounted one directly to far in therefore remove /data
	# Example: https://smh-o1-1-2150-2f65182.urmc-sh.rochester.edu:443/api/v1/download?filePath=/data/IR/data/IR_Org/molecdiag@urmc.edu/A_4068-19_SSP33609_v1/A_4068-19_SSP33609_v1_20191218130002841/A_4068-19_SSP33609_v1_3573af2a-0fa4-41e0-acd3-af9dd1589137_2020-01-21_16-42-25-560_Filtered.zip
	# Hopefully this is a good solution.
	zipped_file = '%s/%s'%(GB.args.mounted_loc, zipped_file.split('=/data')[1])

	checkIfExistsOrError(zipped_file)

	###################################################################
	# Variant TSV is nested inside a zipped folder.  Therefore copy 
	# The zipped folder to a temporary location and check CP occurred correctly
	###################################################################
	cmd = 'cp %s %s'%(zipped_file, cp_zip)
	os.system(cmd)

	###################################################################
	# unzip folder and copy the -full.tsv file and the Workflow_Settings/Analysis_Settings/analysis_settings.txt to the out_dir/variant_folder and out_dir/variant_folder/analysis_settings respectively 
	###################################################################
	unzipped_folder = '%s/%s'%(GB.args.o, sample_name)

	# utils.unzip(zipped_file, unzipped_folder)
	utils.quiet_unzip(zipped_file, unzipped_folder)

	# Find the full.tsv file
	variants_dir = '%s/Variants/%s'%(unzipped_folder,proband)

	# This is the filtered file eventhough it is labeled full.  It is inside a filtered folder
	tsv_file = utils.get_files_with_match(variants_dir, '-full.tsv')

	vcf_file = utils.get_files_with_match(variants_dir, 'Filtered*.vcf') 
	
	if len(tsv_file) > 0 and len(vcf_file) > 0:
		variant_folder = '%s/variant_folder'%(GB.args.o)
		utils.make_directories(variant_folder)

		cp_tsv = '%s/%s_filtered.tsv'%(variant_folder, sample_name)

		cmd = 'cp %s %s'%(tsv_file[0], cp_tsv) 

		os.system(cmd)

		addStrandInfoToTSV(vcf_file[0], cp_tsv)

		###################################################################
		# cp analysis_settings file
		###################################################################
		analysis_settings_dir = '%s/analysis_settings'%(variants_dir)
		utils.make_directories(analysis_settings_dir)

		orginal_settings_f = '%s/Workflow_Settings/Analysis_Settings/analysis_settings.txt'%(unzipped_folder)

		checkIfExistsOrError(orginal_settings_f)

		cp_settings_f = '%s/%s_analysis_settings.txt'%(analysis_settings_dir, sample_name)

		cmd = 'cp %s %s'%(orginal_settings_f, cp_settings_f) 
		os.system(cmd)

		###################################################################
		# Copy the vcf, bam, and bai files from outputs/VariantCallerActor-00
		# I'm unsure if the folder is always called VariantCallerActor-00 For now I will 
		# assume that it is.
		###################################################################
		
		# the main folder is where the IR stores all of the analysis for this sample
		# Example: /data/IR/data/IR_Org/molecdiag@urmc.edu/Mayo13_v1/Mayo13_v1_20191218133854182
		VC_actor_dir = '%s/outputs/VariantCallerActor-00'%(os.path.dirname(os.path.realpath(zipped_file)))

		########################################################################
		# Copy VCF files
		########################################################################
		orginal_vcf = '%s/VariantOutput.filtered.vcf'%(VC_actor_dir)
		
		checkIfExistsOrError(orginal_vcf)

		# cp
		vcf_folder = '%s/vcf'%(GB.args.o)
		utils.make_directories(vcf_folder)
		cp_vcf = '%s/%s.vcf'%(vcf_folder, sample_name)

		cmd = 'cp %s %s'%(orginal_vcf, cp_vcf)
		os.system(cmd)

		########################################################################
		# Copy bam files
		########################################################################
		orginal_bam = '%s/merged.bam.ptrim.bam'%(VC_actor_dir)

		# cp
		bam_folder = '%s/bam'%(GB.args.o)
		utils.make_directories(bam_folder)
		cp_bam = '%s/%s.bam.ptrim.bam'%(bam_folder, sample_name)

		checkIfExistsOrError(bam_folder)

		cmd = 'cp %s %s'%(orginal_bam, cp_bam)
		os.system(cmd)

		########################################################################
		# Copy bai files
		########################################################################
		orginal_bai = '%s/merged.bam.ptrim.bam.bai'%(VC_actor_dir)

		checkIfExistsOrError(orginal_bai)

		# cp
		cp_bai = '%s/%s.bam.ptrim.bam.bai'%(bam_folder, sample_name)

		cmd = 'cp %s %s'%(orginal_bai, cp_bai)
		os.system(cmd)

	else:
		err = 'tsv file was not found for the sample %s at %s with extension -full.tsv or the vcf file with format *Filtered*.vcf'%(sample_name, variants_dir)

		GB.parser.error(err)

def addStrandInfoToTSV(vcf_file, tsv_file):
	'''
	(file_address, file_address, dir_address) -> merge_filtered.tsv file
	'''
	######################################################################
	# Find the filtered VCF file to add strand info to the tsv file.
		# There might be more than one file of interest with a different date.  Just take the first one Example:
			# A_97-19_I1086077_v2/A_97-19_I1086077_v2_Filtered_2020-02-12_15:58:24.vcf 
			# A_97-19_I1086077_v2/A_97-19_I1086077_v2_Filtered_2020-02-12_16:00:39.vcf
		# Example of vcf data
			# chr7	55241677	COSM12988;COSM116882;COSM12428	GAA	AAA,CAA,CAT,GAAA	10.3606	PASS	AF=2.18579E-4,0.0,0.0,0.0242623;AO=3,0,0,316;DP=4590;FAO=1,0,0,111;FDP=4575;FDVR=5,5,10,0;FR=.,.,.,.;FRO=4463;FSAF=0,0,0,5;FSAR=1,0,0,106;FSRF=2332;FSRR=2131;FWDB=-0.0880023,0.0226692,0.00624706,-0.106117;FXX=0.00391901;HRUN=2,2,2,3;HS_ONLY=0;LEN=1,1,3,1;MLLD=56.2754,200.289,392.929,22.0858;OALT=A,C,CAT,A;OID=COSM12988,COSM116882,COSM12428,.;OMAPALT=AAA,CAA,CAT,GAAA;OPOS=55241677,55241677,55241677,55241678;OREF=G,G,GAA,-;PB=.,.,.,.;PBP=.,.,.,.;QD=0.00905842;RBI=0.0881522,0.0240901,0.00632361,0.109927;REFB=-0.00404961,0.00311704,0.00101144,-0.0122493;REVB=-0.00513877,0.00815104,9.8096E-4,-0.0286877;RO=4221;SAF=0,0,0,97;SAR=3,0,0,219;SRF=2217;SRR=2004;SSEN=0,0,0,0;SSEP=0,0,0,0;SSSB=-0.0161083,-3.52971E-8,-3.52971E-8,-0.244259;STB=0.990647,0.5,0.5,0.956719;STBP=0.299,1.0,1.0,0.0;TYPE=snp,snp,mnp,ins;VARB=0.0660333,0.0,0.0,0.117016;HS;FUNC=[{'origPos':'55241677','origRef':'GAA','normalizedRef':'G','gene':'EGFR','normalizedPos':'55241677','normalizedAlt':'GA','gt':'pos','codon':'AAC','coding':'c.2128_2129insA','transcript':'NM_005228.4','function':'frameshiftInsertion','protein':'p.Thr710fs','location':'exonic','origAlt':'GAAA','exon':'18'}]	GT:GQ:DP:FDP:RO:FRO:AO:FAO:AF:SAR:SAF:SRF:SRR:FSAR:FSAF:FSRF:FSRR	0/4:10:4590:4575:4221:4463:3,0,0,316:1,0,0,111:2.18579E-4,0.0,0.0,0.0242623:3,0,0,219:0,0,0,97:2217:2004:1,0,0,106:0,0,0,5:2332:2131
		# Desired data format
			# GAA=2332/2131, AAA=0/1, CAA=0/0, CAT=0/0, GAAA=5/106
	# add ALT to variant_allele column 			
	######################################################################

	print('\n################################################\nAdding Strand bias info from:\n\t%s'%(vcf_file))

	out_file =  '%s_filtered_SB_added.tsv'%(tsv_file.split('_filtered.tsv')[0])

	vcf_dict = {}

	with open(vcf_file, 'r') as vf, open(tsv_file, 'r') as tf, open(out_file, 'w') as wf:

		################################################################################
		# Obtain all info from the vcf and save in a dictionary
			# FSRF - Flow Evaluator reference observations on the forward strand
			# FSRR - Flow Evaluator reference observations on the reverse strand
			# Third column has the REF: GAA
			# Fourth column has ALT: AAA,CAA,CAT,GAAA (add as observed_variant)
			# combining Third and fourth column will provide order
			# FSAF - Flow Evaluator reference observations on the forward strand
			# FSAR - Flow Evaluator Alternate allele observations on the reverse strand
		################################################################################
		line = vf.readline().rstrip()
		info_header_from_vcf = ''

		while line:

			##########################################################
			# Add all of the info section from the vcf file that does not start with ##FILTER= ##INFO= ##FORMAT=.  This info
			# only pertains to the vcf file formats 
			##########################################################
			if line.startswith('##') and not line.startswith('##FILTER=') and not line.startswith('##INFO=') and not line.startswith('##FORMAT='):
				info_header_from_vcf+='%s\n'%(line) 

			# Skip Format info that starts with ##
			# unfortunately Thermofisher has altering column headers I will have to use position 
			elif not line.startswith('##') and not line.startswith('#'):
				split_line = line.split('\t')
				
				# If there are not at least 7 columns most likely it is an empty line skip
				if len(split_line) < 7:
					break

				ref = split_line[3]
				alt = split_line[4]
				split_alt = alt.split(',')
				info_str = split_line[7].split(';')
				
				# Find all of the strand coverage counts for strand bias in the huge mess of a string in info_str
				for v in info_str:
					if v.startswith('FSRF='):
						fsrf = v.replace('FSRF=', '')

					elif v.startswith('FSRR='):
						fsrr = v.replace('FSRR=', '')

					elif v.startswith('FSAF='):
						fsaf = v.replace('FSAF=', '').split(',')

					elif v.startswith('FSAR='):
						fsar = v.replace('FSAR=', '').split(',')

					elif v.startswith('FUNC='):
						func = v.replace('FUNC=', '')

						func_json = json.loads(func.replace("'", '"'))[0]

						samp_name = '%s_%s_%s'%(func_json['gene'], func_json['coding'], func_json['protein'])

				#############################################################			
				# Make sure sample name was generated before proceeding.
				#############################################################		
				if 'samp_name' not in locals():
					err = '\n!!!!!!!!!!!!!!!!\nERROR OCCURED sample name was not found in:\n\t%s'%(line)

				#############################################################			
				# add ref strand bias info
				#############################################################		
				if 'ref' not in locals() or 'fsrf' not in locals() or 'fsrr' not in locals():

					# err = '\n!!!!!!!!!!!!!!!!\nERROR OCCURED while making strand bias string.  Ref variables not set.:\n\t"ref" not in locals() => %s\n\t"fsrf" not in locals() => %s\n\t"fsrr" not in locals() => %s'%('ref' not in locals(), 'fsrf' not in locals(),'fsrr' not in locals())

					# GB.parser.error(err)
					
					# check the issue described under check alt strand bias in thermo_backup_ion_reporter_run.py for why this is here.
					ref = 'missing'
					fsrf = 1
					fsrr = 1

					strand_basis = '%s=%s/%s'%(ref, fsrf, fsrr)
				else:

					strand_basis = '%s=%s/%s'%(ref, fsrf, fsrr)
				
				#############################################################			
				# Check alt strand bias info
				#############################################################				
				if 'split_alt' not in locals() or 'fsaf' not in locals() or 'fsar' not in locals():

					# check the issue described under check alt strand bias in thermo_backup_ion_reporter_run.py for why this is here.
					if 'split_alt' in locals():
						
						fsaf = ['1'] * len(split_alt)
						fsar = ['1'] * len(split_alt)

					else:
						err = '\n!!!!!!!!!!!!!!!!\nERROR OCCURED while making strand bias string.  ALT variables not set.:\n\t"split_alt" not in locals() => %s\n\t"fsaf" not in locals() => %s\n\t"fsar" not in locals() => %s'%('split_alt' not in locals(), 'fsaf' not in locals(),'fsar' not in locals())

						GB.parser.error(err)

				# Make sure all alt lists are the same length
				alt_count = len(split_alt)

				if len(fsaf) != alt_count or len(fsar) != alt_count:

					err = '\n!!!!!!!!!!!!!!!!\nERROR OCCURED while making strand bias string.  Lengths are not equal.:\n\tALT len(%s)= %s\n\tFSAF len(%s)= %s\n\tFSAR len(%s)= %s'%(alt_count, alt, len(fsaf), utils.list_to_char_seperated(fsaf, ' '), len(fsar), utils.list_to_char_seperated(fsar, ' '))
					GB.parser.error(err)
				
				#############################################################			
				# add alt strand bias info
					# strand_basis => GAA=2332/2131
					# fsaf => ['0', '0', '0', '5']
					# fsar => ['1', '0', '0', '106']
					# split_alt => ['AAA', 'CAA', 'CAT', 'GAAA']
					# desired result => GAA=2332/2131, AAA=0/1, CAA=0/0, CAT=0/0, GAAA=5/106
				#############################################################
				for i in range(0, alt_count):

					strand_basis+= ', '

					# add current variant
					strand_basis+= '%s=%s/%s'%(split_alt[i], fsaf[i], fsar[i])

				#############################################################
				# Add current variant to the vcf_dict to link to the tsv file later
				#############################################################
				vcf_dict[samp_name] = {'strand_basis':strand_basis, 'variant_allele':alt}

				print('\nSample Name:\n\n\t%s\n\nOriginal variant:\n\n\t%s\n\nExtracted Info:\n\n\tstrand_basis: %s\n\n\tvariant_allele: %s'%(samp_name, line, strand_basis, alt))


			line = vf.readline().rstrip()

		#############################################################
		# Iterate over the tsv file and add strand bias info from the vcf_dict
			# write header info from vcf_file info_header_from_vcf
			# tsv file has header that starts with ## => Write to wf
			# tsv file has column headers that starts with # 
				# add strand bias column (Ref+/Ref-/Var+/Var-) and variant_allele
				# write to wf 
				# set split_header variable
			# tsv file has variant rows that do not start with # 
				# make sure split_header exists.
				# find the variant by gene_coding_protein 
				# match strand bias info from vcf_dict
				# append strand bias info and write info to wf
		#############################################################
		
		# write header info from vcf_file info_header_from_vcf
		if info_header_from_vcf != '':
			wf.write(info_header_from_vcf)

		line = tf.readline().rstrip()

		while line:

			# tsv file has header that starts with ## => Write to wf
			if line.startswith('##'):
				wf.write('%s\n'%(line))

			############################################################
			# tsv file has column headers that starts with # 
				# add strand bias column (Ref+/Ref-/Var+/Var-) and variant_allele
				# write to wf 
				# set split_header variable
			############################################################
			elif line.startswith('#') and not line.startswith('##'):
				header = '%s\tRef+/Ref-/Var+/Var-\tvariant_allele'%(line)
				wf.write('%s\n'%(header))
				split_header = header.split('\t')

			############################################################
			# tsv file has variant rows that do not start with # 
				# make sure split_header exists.
				# find the variant by gene_coding_protein 
				# match strand bias info from vcf_dict
				# append strand bias info and write info to wf
			############################################################
			elif 'split_line' in locals():
				split_line = line.split('\t')

				match_dict = utils.two_lists_to_dict(split_header, split_line)

				######################################################
				# Thermofisher formating problem found in tsv sample 4896-18.  
					# Problem: Sometimes more than one gene name is found
						# Example:
							# gene: EGFR|EGFR-AS1
							# coding: c.2317_2318insGTC|
							# protein: p.Pro772_His773insArg|
					# Solution split at |
				######################################################

				curr_samp_name = '%s_%s_%s'%(match_dict['gene'].split('|')[0], match_dict['coding'].split('|')[0], match_dict['protein'].split('|')[0])

				print('\n\nUpdating TSV Current Sample:\n\n\t%s\n'%(curr_samp_name))
				if curr_samp_name in vcf_dict.keys():
					
					curr_vcf_info = vcf_dict[curr_samp_name]

					match_dict['Ref+/Ref-/Var+/Var-'] = curr_vcf_info['strand_basis']
					match_dict['variant_allele'] = curr_vcf_info['variant_allele']

					row = ''
					for col in split_header:

						if row != '':
							row+='\t'
						
						row+= '%s'%(match_dict[col])

					print('\n\nUpdated Row: \n\n\t%s'%(row))
					wf.write('%s\n'%(row))

				else:
					wf.write('%s\t\n'%(line))

			line = tf.readline().rstrip()

def checkIfExistsOrError(check_loc):

	if not os.path.exists(check_loc):
		err = '!!!!!!!!!!!!!!!!!!!!!!!!!!!!\nError Occurred: \n\tZipped file does not exist. \n\t%s\n!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n'%(check_loc)

		GB.parser.error(err)
	else:
		return True

def writeTsvFromNestedTuples(nested_tuples, out_dir, of_name):
	'''
		([(),()]) -> tsv
	'''
	utils.make_directories(out_dir)
	out_file = '%s/%s.tsv'%(out_dir, of_name)

	with open(out_file, 'w') as of:

		for t in nested_tuples:
			of.write(utils.list_to_char_seperated(t, '\t'))

def control_center():

	# Make output dir
	utils.make_directories(GB.args.o)

	today = date.today()
	GB.args.o = '%s/IR_check_%s'%(GB.args.o, today.strftime('%Y-%m-%d'))

	utils.make_directories(GB.args.o)

	ir_api_per_sample()


	# for samp in GB.args.sample_list:

	# 	print(samp)

	# 	ir_api_per_sample(samp, temp_dir)

	# md5file_curr.finish_tracking()

	# ###################################################################
	# # Delete temp dir
	# ###################################################################
	# cmd = 'rm -r %s'%(temp_dir)
	# os.system(cmd)

class LineWrapRawTextHelpFormatter(argparse.RawDescriptionHelpFormatter):
	def _split_lines(self, text, width):
		text = self._whitespace_matcher.sub(' ', text).strip()
		return _textwrap.wrap(text, width)

def arguments():
    # intialize transfer
	GB.parser = argparse.ArgumentParser(description='-----------------------------------------------------------------------------------\n'
		'\nWelcome to the check_IR_API.py Python script\n\n'
		'-----------------------------------------------------------------------------------\n'
		'The purpose of this program is check if the IR API is the same after upgrade.  This will ensure Infotrack can talk to  the IR API correctly\n\n\n'		
		'-----------------------------------------------------------------------------------\n',
		formatter_class=LineWrapRawTextHelpFormatter, epilog='-----------------------------------------------------------------------------------\n'
		'Example run:\n\n'
		'python python/check_IR_API.py -o devs/ion_torrent/check_ir_api -api_server https://10.149.59.209/api/v1/ -api_token Mjk5OWY1YmJlODc1Y2FkYTgzMGVkZDJiZjdhNDI4MjA3MmYwMmIxN2IyOWZlYzVhMThmNzAzYTQ4OTliOWRjMg -sample B_1163-20_HD802_1093_1068_109\n\n'
		'-----------------------------------------------------------------------------------\n')

	GB.parser.add_argument('-sample', nargs='?', help='<string> Add a sample name.  Required to be a full sample name.  The best place to acquire the correct full name via the check_thermo_apis page in Infotrack however, it can also be found in the bc_summary file from the coverage analysis plugin.', required=True)
	
	GB.parser.add_argument('-o', nargs='?', help='<String>  output directory')
	

	GB.parser.add_argument('-api_server', nargs='?', help='<String> link to api_server')
	

	GB.parser.add_argument('-api_token', nargs='?', help='<String> api_token.  Refer to Ion Reporter settings page.')

	GB.parser.add_argument('-mounted_loc', nargs='?', help='<String> default /media/2f65182', default='/media/2f65182')

	GB.args = GB.parser.parse_args()

	# add run_command to args
	GB.args.run_command = utils.list_to_char_seperated(sys.argv, ' ', False)

	check_user_input()

def check_user_input():
	'''
		(namespace, ) ->
	'''
	pass
if __name__ == "__main__":

	print('Input Command: %s'%(sys.argv))

	arguments()

	control_center()
