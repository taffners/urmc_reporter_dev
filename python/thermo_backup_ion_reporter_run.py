from __future__ import print_function
#!/usr/bin/python

# Libraries
import argparse
import os
import sys
import subprocess
import textwrap as _textwrap
import json

from lib import GB
# call GB init to make globals
GB.init()

from lib import utils
from lib import md5file

def ir_api_per_sample(sample_name, temp_dir):
	'''
		(Mayo13, Mjk5OWY1YmJlODc1Y2FkYTgzMGVkZDJiZjdhNDI4MjA3MmYwMmIxN2IyOWZlYzVhMThmNzAzYTQ4OTliOWRjMg, https://10.149.59.209/api/v1/) -> 2 zip files are made and 
	'''
	api_curl = 'curl -s --request GET -k -H "Authorization:'
	
	header = '--header "Content-Type:application/x-www-form-urlencoded" '
	 
	api_cmd = 'analysis?format=json&name='

	#######################################################################################
	# Access the samples via the Ion Reporter API and analysis command
	# example sample successfully IR complete: Mayo13
		# curl command used via python's subprocess module:
			# curl -s --request GET -k -H "Authorization: Mjk5OWY1YmJlODc1Y2FkYTgzMGVkZDJiZjdhNDI4MjA3MmYwMmIxN2IyOWZlYzVhMThmNzAzYTQ4OTliOWRjMg " --header "Content-Type:application/x-www-form-urlencoded" "https://10.149.59.209/api/v1/analysis?format=json&name=Mayo13"
		# Before running this analysis command there were no .zip files present for this sample
		# After running the analysis command two zip files now are available.
			# Mayo13_v1_c2580_2019-12-18-13-38-37-379_2020-01-13_13-35-47-228_Filtered.zip
			# Mayo13_v1_c2580_2019-12-18-13-38-37-379_2020-01-13_13-35-44-502_All.zip
		# The API returns a JS like the one below with addresses to these files
		# [
		#      {
		#           "data_links": {
		#                "filtered_variants": "https://smh-o1-1-2150-2f65182.urmc-sh.rochester.edu:443/api/v1/download?filePath=/data/IR/data/IR_Org/molecdiag@urmc.edu/Mayo13_v1/Mayo13_v1_20191218133854182/Mayo13_v1_c2580_2019-12-18-13-38-37-379_2020-01-13_13-59-36-442_Filtered.zip",
		#                "unfiltered_variants": "https://smh-o1-1-2150-2f65182.urmc-sh.rochester.edu:443/api/v1/download?filePath=/data/IR/data/IR_Org/molecdiag@urmc.edu/Mayo13_v1/Mayo13_v1_20191218133854182/Mayo13_v1_c2580_2019-12-18-13-38-37-379_2020-01-13_13-59-34-041_All.zip"
		#           },
		#           "flagged": false,
		#           "id": "ff8081816baf0c96016f1a4e47021021",
		#           "ion_reporter_version": "5.10",
		#           "name": "Mayo13_v1_c2580_2019-12-18-13-38-37-379",
		#           "report_published": "",
		#           "reports": {
		#                "final": {},
		#                "qc": {
		#                     "link": "https://smh-o1-1-2150-2f65182.urmc-sh.rochester.edu:443/api/v1/download?type=pdf&filePath=/scratch/tmp/IR_Org/download/pdf/3905945b-25be-4180-9d2b-fbdd995c110d/ff8081816baf0c96016f1a4e47021021_QC.pdf"
		#                }
		#           },
		#           "samples": {
		#                "PROBAND": "Mayo13_v1"
		#           },
		#           "shared_with": [],
		#           "stage": "Send for Report Generation",
		#           "start_date": "December 18, 2019",
		#           "started_by": "Molec Diag",
		#           "status": "SUCCESSFUL",
		#           "variants_saved": "",
		#           "workflow": "OFA_5-10_complex_v3_filter_updated"
		#      }
		# ]

	# Response if sample is still finishing up in Torrent server and not start analysis yet
	# {
 	#     "error": "CONTAINER with given name: Analysis doesn't exists. does not exist"
	# }

	######################################################################################
	# Example: curl -s --request GET -k -H "Authorization: Mjk5OWY1YmJlODc1Y2FkYTgzMGVkZDJiZjdhNDI4MjA3MmYwMmIxN2IyOWZlYzVhMThmNzAzYTQ4OTliOWRjMg " --header "Content-Type:application/x-www-form-urlencoded" "https://10.149.59.209/api/v1/analysis?format=json&name=A_4306-19_J0053557_1009_921_100"

	os_cmd = '%s %s " %s"%s%s%s"'%(api_curl, GB.args.api_token, header, GB.args.api_server, api_cmd, sample_name)
	print(os_cmd)
	output = subprocess.check_output(os_cmd, shell=True)

	print(output)
	analysis_json = json.loads(output)

	print(analysis_json)

	#######################################################################################
	# After Bill messed with the filter settings now variants that should be in the Positive control are 
	# being filtered out.  
	# Example: analysis JSON of AcroMetrix control
	# [
	# 	{
	# 		u'status': u'SUCCESSFUL', 
	# 		u'name': u'A_AcroMetrix_187619_959_805_91_v2_d6eba1cc-8023-49b6-b7e7-f05596b302be', 
	# 		u'workflow': u'OFA_p200611', 
	# 		u'report_published': u'', 
	# 		u'reports': 
	# 		{
	# 			u'qc': 
	# 				{
	# 					u'link': u'https://2f65182.urmc-sh.rochester.edu:443/api/v1/download?type=pdf&filePath=/scratch/tmp/IR_Org/download/pdf/4c7c0ec1-d322-4b74-b2af-5af9a6f64e40/A_AcroMetrix_187619_959_805_91_v2_d6eba1cc-8023-49b6-b7e7-f05596b302be_QC.pdf'
	# 				},
	# 				u'final': {}
	# 		}, 
	# 		u'shared_with': [], 
	# 		u'data_links': 
	# 		{
	# 			u'unfiltered_variants': u'https://2f65182.urmc-sh.rochester.edu:443/api/v1/download?filePath=/data/IR/data/IR_Org/molecdiag@urmc.edu/A_AcroMetrix_187619_959_805_91_v2/A_AcroMetrix_187619_959_805_91_v2_20200618150400958/A_AcroMetrix_187619_959_805_91_v2_d6eba1cc-8023-49b6-b7e7-f05596b302be_2020-07-02_13-47-38-836_All.zip', 
	# 			u'filtered_variants': u'https://2f65182.urmc-sh.rochester.edu:443/api/v1/download?filePath=/data/IR/data/IR_Org/molecdiag@urmc.edu/A_AcroMetrix_187619_959_805_91_v2/A_AcroMetrix_187619_959_805_91_v2_20200618150400958/A_AcroMetrix_187619_959_805_91_v2_d6eba1cc-8023-49b6-b7e7-f05596b302be_2020-07-02_13-47-40-780_Filtered.zip'
	# 		}, 
	# 		u'ion_reporter_version': u'5.12', 
	# 		u'start_date': u'June 18, 2020', 
	# 		u'samples': 
	# 		{
	# 			u'PROBAND': u'A_AcroMetrix_187619_959_805_91_v2'
	# 		}, 
	# 		u'id': u'ff808181725c2c6b0172c8d10a1e0cc9', 
	# 		u'started_by': u'Diag,Molec', 
	# 		u'variants_saved': u'', 
	# 		u'flagged': False, 
	# 		u'stage': u'Review Variants'
	# 	}, 
	# 	{
	# 		u'status': u'SUCCESSFUL', 
	# 		u'name': u'A_AcroMetrix_187619_959_805_91_v1_939efb97-19ea-487e-9d4a-640193b32de4', 
	# 		u'workflow': u'OFA_5-10_complex_v3_filter_updated', 
	# 		u'report_published': u'', 
	# 		u'reports': 
	# 		{
	# 			u'qc': 
	# 			{
	# 				u'link': u'https://2f65182.urmc-sh.rochester.edu:443/api/v1/download?type=pdf&filePath=/scratch/tmp/IR_Org/download/pdf/88bb3075-4d5c-424f-bdd4-2bf840e7f5b0/A_AcroMetrix_187619_959_805_91_v1_939efb97-19ea-487e-9d4a-640193b32de4_QC.pdf'
	# 			}, 
	# 			u'final': {}
	# 		}, 
	# 		u'shared_with': [], 
	# 		u'data_links': 
	# 		{
	# 			u'unfiltered_variants': u'https://2f65182.urmc-sh.rochester.edu:443/api/v1/download?filePath=/data/IR/data/IR_Org/molecdiag@urmc.edu/A_AcroMetrix_187619_959_805_91_v1/A_AcroMetrix_187619_959_805_91_v1_20200214124744168/A_AcroMetrix_187619_959_805_91_v1_939efb97-19ea-487e-9d4a-640193b32de4_2020-07-02_13-47-42-362_All.zip', 
	# 			u'filtered_variants': u'https://2f65182.urmc-sh.rochester.edu:443/api/v1/download?filePath=/data/IR/data/IR_Org/molecdiag@urmc.edu/A_AcroMetrix_187619_959_805_91_v1/A_AcroMetrix_187619_959_805_91_v1_20200214124744168/A_AcroMetrix_187619_959_805_91_v1_939efb97-19ea-487e-9d4a-640193b32de4_2020-07-02_13-47-44-711_Filtered.zip'
	# 		}, 
	# 		u'ion_reporter_version': u'5.10', 
	# 		u'start_date': u'February 14, 2020', 
	# 		u'samples': 
	# 		{
	# 			u'PROBAND': u'A_AcroMetrix_187619_959_805_91_v1'
	# 		}, 
	# 		u'id': u'ff8081816fb08575017044d046c81791',
	# 		u'started_by': u'Diag,Molec', 
	# 		u'variants_saved': u'', 
	# 		u'flagged': False, 
	# 		u'stage': u'Send for Report Generation'
	# 	}
	# ]
	#######################################################################################
	if 'AcroMetrix' in sample_name and len(analysis_json) > 0 and 'samples' in analysis_json[0].keys() and 'PROBAND' in analysis_json[0]['samples'].keys() and 'data_links' in analysis_json[0].keys() and 'unfiltered_variants' in analysis_json[0]['data_links'].keys():
	
		# Thermofisher replaces spaces with _
		proband_sample = analysis_json[0]['samples']['PROBAND'].replace(' ', '_')

		backupIRSampleData(analysis_json[0]['data_links']['unfiltered_variants'], sample_name, GB.args.o, proband_sample, temp_dir, analysis_json[0], 'unfiltered')

	# Pull filtered and non filtered for all samples except AcroMetrix
	elif len(analysis_json) > 0 and 'samples' in analysis_json[0].keys() and 'PROBAND' in analysis_json[0]['samples'].keys() and 'data_links' in analysis_json[0].keys() and 'filtered_variants' in analysis_json[0]['data_links'].keys():
		
		# Thermofisher replaces spaces with _
		proband_sample = analysis_json[0]['samples']['PROBAND'].replace(' ', '_')

		backupIRSampleData(analysis_json[0]['data_links']['filtered_variants'], sample_name, GB.args.o, proband_sample, temp_dir, analysis_json[0], 'filtered')

		backupIRSampleData(analysis_json[0]['data_links']['unfiltered_variants'], sample_name, GB.args.o, proband_sample, temp_dir, analysis_json[0], 'unfiltered', False,False)

	else:
		GB.parser.error('Sample %s had an error with the api %s\n\n\nos_cmd = %s'%(sample_name, json.dumps(analysis_json, indent=5, sort_keys=True), os_cmd))

def backupIRSampleData(zipped_file, sample_name, out_dir, proband, temp_dir, json_parms, filter_type, infotrack_cp=True, bam_backup=True):
	'''
		Run this function on each sample after the ir_api run

		If infotrack_cp is True then copy contents to a folder called infotrack_workflow_ion_reporter_version_analysis_start_date

		bam_backup will only backup bam files if True.  For files that were already backed up it is not necessary to do it again.  This happens because we are pulling both filtered and unfiltered files.

		This function will copy the tsv, vcf, and bam file so the out_dir and return a list of tuples of md5s for each file copied.

		Example list of tuples returned:
		[('Original_file', 'Original_md5', 'cp_file', 'cp_m5', 'match', 'file_type', 'sample_name'), ('/data/IR/data/IR_Org/molecdiag@urmc.edu/Mayo13_v1/Mayo13_v1_20191218133854182/Mayo13_v1_c2580_2019-12-18-13-38-37-379_2020-01-13_13-59-36-442_Filtered.zip', 'e896bed86e77ad3e9f3c51ca9f7e9ae1', '/share/backup_data/test_cp_dir/temp/Mayo13.zip', 'e896bed86e77ad3e9f3c51ca9f7e9ae1', True, 'zip_variant_dir', 'Mayo13'), ('/share/backup_data/test_cp_dir/temp/Mayo13/Variants/Mayo13_v1/Mayo13_v1-full.tsv', '6d0d4fd43dd4f8e087f696a700f6b7d4', '/share/backup_data/test_cp_dir/variant_folder/Mayo13_filtered.tsv', '6d0d4fd43dd4f8e087f696a700f6b7d4', True, 'variant_tsv', 'Mayo13'), ('/share/backup_data/test_cp_dir/temp/Mayo13/Workflow_Settings/Analysis_Settings/analysis_settings.txt', '9c28a8b5a99af8fd7c68c2ab19768c93', '/share/backup_data/test_cp_dir/temp/Mayo13/Variants/Mayo13_v1/analysis_settings/Mayo13_analysis_settings.txt', '9c28a8b5a99af8fd7c68c2ab19768c93', True, 'variant_settings', 'Mayo13'), ('/data/IR/data/IR_Org/molecdiag@urmc.edu/Mayo13_v1/Mayo13_v1_20191218133854182/outputs/VariantCallerActor-00/VariantOutput.filtered.vcf', 'e29b1562f2f41baaf86dad21cf4ef850', '/share/backup_data/test_cp_dir/vcf/Mayo13.vcf', 'e29b1562f2f41baaf86dad21cf4ef850', True, 'vcf', 'Mayo13'), ('/data/IR/data/IR_Org/molecdiag@urmc.edu/Mayo13_v1/Mayo13_v1_20191218133854182/outputs/VariantCallerActor-00/merged.bam.ptrim.bam', 'ad86f0fca8da588a45fe26aca5df1546', '/share/backup_data/test_cp_dir/bam/Mayo13.bam.ptrim.bam', 'ad86f0fca8da588a45fe26aca5df1546', True, 'bam', 'Mayo13'), ('/data/IR/data/IR_Org/molecdiag@urmc.edu/Mayo13_v1/Mayo13_v1_20191218133854182/outputs/VariantCallerActor-00/merged.bam.ptrim.bam.bai', '63200d28a19b45707eb532a923c3cd5f', '/share/backup_data/test_cp_dir/bam/Mayo13.bam.ptrim.bam.bai', '63200d28a19b45707eb532a923c3cd5f', True, 'bai', 'Mayo13')
	'''

	# Move to a temporary location and unzip folder because of permission settings on orginal parent
	# folder it needs to be outside of that folder
	utils.make_directories(out_dir)

	cp_zip = '%s/%s.zip'%(temp_dir, sample_name)

	# the drive is mounted one directly to far in therefore remove /data
	# Example: https://smh-o1-1-2150-2f65182.urmc-sh.rochester.edu:443/api/v1/download?filePath=/data/IR/data/IR_Org/molecdiag@urmc.edu/A_4068-19_SSP33609_v1/A_4068-19_SSP33609_v1_20191218130002841/A_4068-19_SSP33609_v1_3573af2a-0fa4-41e0-acd3-af9dd1589137_2020-01-21_16-42-25-560_Filtered.zip
	# Hopefully this is a good solution.
	zipped_file = '%s/%s'%(GB.args.mounted_loc, zipped_file.split('=/data')[1])

	###################################################################
	# Variant TSV is nested inside a zipped folder.  Therefore copy 
	# The zipped folder to a temporary location and check CP occurred correctly
	###################################################################
	transfer_ok = md5file_curr.recordNewTransfer(zipped_file, cp_zip, sample_name, 'cp zip dir')

	# if transfer happened correctly unzip folder
	if transfer_ok:

		###################################################################
		# unzip folder and copy the -full.tsv file and the Workflow_Settings/Analysis_Settings/analysis_settings.txt to the out_dir/variant_folder and out_dir/variant_folder/analysis_settings respectively 
		###################################################################
		unzipped_folder = '%s/%s'%(temp_dir, sample_name)

		# utils.unzip(zipped_file, unzipped_folder)
		utils.quiet_unzip(zipped_file, unzipped_folder)
	
		# Find the full.tsv file
		variants_dir = '%s/Variants/%s'%(unzipped_folder,proband)

		###################################################################
		# Nomencalture for files in this folder is weird and does not have to do with the filter set chosen 
		# for variants filtered using the chosen filter set in the the workflow use 
		# analysis_json[0]['data_links']['filtered_variants']. Filtered here I think means SNVs which are not
		# real are filtered out.
		# Example of files in this folder:
			# A_AcroMetrix_187619_959_805_91_v2_Non-Filtered_2020-07-02_141120.vcf
			# A_AcroMetrix_187619_959_805_91_v2_Non-Filtered_2020-07-02_141120-oncomine.tsv
			# A_AcroMetrix_187619_959_805_91_v2-full.tsv
			# SmallVariants.filtered.vcf
			# SmallVariants.vcf
		###################################################################
		# This is the filtered file eventhough it is labeled full.  It is inside a filtered folder
		tsv_file = utils.get_files_with_match(variants_dir, '-full.tsv')

		# vcf file examples
		#[u'/media/storage/fs-ngs/devs/ion_torrent//Run59_179_20200714_A_KOEG5_DAFG00010_246/temp/A_2500-20_J9027447_1421_1500_156/Variants/A_2500-20_J9027447_1421_1500_156_v1/A_2500-20_J9027447_1421_1500_156_v1_Filtered_2020-07-21_13:03:14.vcf', u'/media/storage/fs-ngs/devs/ion_torrent//Run59_179_20200714_A_KOEG5_DAFG00010_246/temp/A_2500-20_J9027447_1421_1500_156/Variants/A_2500-20_J9027447_1421_1500_156_v1/A_2500-20_J9027447_1421_1500_156_v1_Non-Filtered_2020-07-21_13:03:12.vcf']

		if filter_type == 'unfiltered':
			vcf_file = utils.get_files_with_match(variants_dir, 'Non-Filtered*.vcf') 	
		else:
			vcf_file = utils.get_files_with_match(variants_dir, '_Filtered*.vcf') 	
		
		if len(tsv_file) > 0 and len(vcf_file) > 0:

			################################################
			# Make an out dir to reflect ion reporter analysis
			# 	{
			# 		u'workflow': u'OFA_p200611', 
			# 		u'ion_reporter_version': u'5.12', 
			# 		u'start_date': u'June 18, 2020', 
			# 	}, 
			################################################
			
			# check to see if ion_report_version, analysis start date, and analysis start date is in the expected format before proceeding. 
			if 'workflow' in json_parms.keys() and 'ion_reporter_version' in json_parms.keys() and 'start_date' in json_parms.keys() and utils.validate_date_full_month_name_date(json_parms['start_date']):

				analysis_start_date = utils.convert_full_month_date_YYYYMMDD(json_parms['start_date'])

				variant_folder = '%s/vars_%s_%s_%s'%(out_dir, json_parms['workflow'].replace(' ', '_'), json_parms['ion_reporter_version'].replace(' ', '').replace('.', '_'), analysis_start_date)

				vcf_folder = '%s/vcf_%s_%s_%s'%(out_dir, json_parms['workflow'].replace(' ', '_'), json_parms['ion_reporter_version'].replace(' ', '').replace('.', '_'), analysis_start_date)

				infotrack_folder = '%s/infotrack_%s_%s_%s'%(out_dir, json_parms['workflow'].replace(' ', '_'), json_parms['ion_reporter_version'].replace(' ', '').replace('.', '_'), analysis_start_date)
			else:
				variant_folder = '%s/variant_folder'%(out_dir)
				vcf_folder = '%s/vcf'%(out_dir)
				infotrack_folder = '%s/infotrack'%(out_dir)

			utils.make_directories(variant_folder)
			utils.make_directories(infotrack_folder)

			################################################
			# To make it easier to upload all necessary files to infotrack place all files necessary to 
			# infotrack_folder This includes the coverage/all_coverage_amplicons.tsv file.  This can't be done
			# else where because the folder name is not known.
			################################################			
			orginal_cov_file = '%s/coverage/all_coverage_amplicons.tsv'%(out_dir)
			cp_cov = '%s/all_coverage_amplicons.tsv'%(infotrack_folder)
			if os.path.exists(orginal_cov_file) and not os.path.exists(cp_cov):
				transfer_ok = md5file_curr.recordNewTransfer(orginal_cov_file, cp_cov, sample_name, 'coverage')

			################################################
			# Name file depending on if it is workflow filtered or not.  Acrometrix controls pulls 
			################################################		
			cp_tsv = '%s/%s_%s.tsv'%(variant_folder, sample_name, filter_type)

			transfer_ok = md5file_curr.recordNewTransfer(tsv_file[0], cp_tsv, sample_name, 'tsv')
			
			SB_file = addStrandInfoToTSV(vcf_file[0], cp_tsv)

			# copy SB file to infotrack folder if turned on  
			if infotrack_cp:
				cp_sb_file = '%s/%s'%(infotrack_folder, os.path.basename(SB_file))

				transfer_ok = md5file_curr.recordNewTransfer(SB_file, cp_sb_file, sample_name, 'infotrack')
			
			###################################################################
			# cp analysis_settings file this is a temp file
			###################################################################
			analysis_settings_dir = '%s/analysis_settings'%(variants_dir)
			utils.make_directories(analysis_settings_dir)

			orginal_settings_f = '%s/Workflow_Settings/Analysis_Settings/analysis_settings.txt'%(unzipped_folder)			

			cp_settings_f = '%s/%s_analysis_settings.txt'%(analysis_settings_dir, sample_name)

			transfer_ok = md5file_curr.recordNewTransfer(orginal_settings_f, cp_settings_f, sample_name, 'analysis_settings')
			
			###################################################################
			# Copy the vcf, bam, and bai files from outputs/VariantCallerActor-00
			# I'm unsure if the folder is always called VariantCallerActor-00 For now I will 
			# assume that it is.  
			###################################################################
		
			# the main folder is where the IR stores all of the analysis for this sample
			# Example: /data/IR/data/IR_Org/molecdiag@urmc.edu/Mayo13_v1/Mayo13_v1_20191218133854182
			# contents: 
				# depth.txt
				# merged.bam.ptrim.bam
				# merged.bam.ptrim.bam.bai
				# SmallVariants.filtered.vcf
				# SmallVariants.vcf
				# variant.call.complete
				# VariantOutput.filtered.vcf
			VC_actor_dir = '%s/outputs/VariantCallerActor-00'%(os.path.dirname(os.path.realpath(zipped_file)))
			
			########################################################################
			# Copy VCF files.  There are many types of vcf files.  The one related to filtering which will be saved in the main vcf folder and the one Part of VariantCallerActor. The variant vcf that is 
			# dependent on the workflow should be pull all the the time.
			########################################################################
			# cp
			utils.make_directories(vcf_folder)

			# copy vcf used to make strain basis.  This will have be filtered or not filtered
			cp_main_vcf = '%s/%s_%s.vcf'%(vcf_folder, sample_name, filter_type)

			transfer_ok = md5file_curr.recordNewTransfer(vcf_file[0], cp_main_vcf, sample_name, '%s main vcf'%(filter_type))

			if bam_backup:

				########################################################################
				# Copy VCF files.  There are many types of vcf files.  The one related to filtering which will be saved in the main vcf folder and the one Part of VariantCallerActor.  The variantCallerActor
				# vcf should only be pulled when the bam file is also being pulled
				########################################################################
				orginal_vcf = '%s/VariantOutput.filtered.vcf'%(VC_actor_dir)

				variantCallerDir = '%s/vcf_VariantCaller'%(out_dir)
				utils.make_directories(variantCallerDir)

				cp_vcf = '%s/%s.vcf'%(variantCallerDir, sample_name)
				transfer_ok = md5file_curr.recordNewTransfer(orginal_vcf, cp_vcf, sample_name, 'VariantCaller vcf')

				########################################################################
				# Copy bam files
				########################################################################
				orginal_bam = '%s/merged.bam.ptrim.bam'%(VC_actor_dir)

				# cp
				bam_folder = '%s/bam'%(out_dir)
				utils.make_directories(bam_folder)
				cp_bam = '%s/%s.bam.ptrim.bam'%(bam_folder, sample_name)

				transfer_ok = md5file_curr.recordNewTransfer(orginal_bam, cp_bam, sample_name, 'bam')

				########################################################################
				# Copy bai files
				########################################################################
				orginal_bai = '%s/merged.bam.ptrim.bam.bai'%(VC_actor_dir)

				checkIfExistsOrError(orginal_bai)

				# cp
				cp_bai = '%s/%s.bam.ptrim.bam.bai'%(bam_folder, sample_name)

				transfer_ok = md5file_curr.recordNewTransfer(orginal_bai, cp_bai, sample_name, 'bai')

		else:
			# I found this error but I'm unsure what caused it was in the ir_backup_tracker.tsv file.  However, this file did transfer fine and it was after 
				#  not found for the sample A_429-19_I1306534_1187_1177_121 at /media/storage/fs-ngs/ion_torrent//Run50_140_20200425_A_TKUVK_DAFG00037_232/temp/A_429-19_I1306534_1187_1177_121/Variants/A_429-19_I1306534_1187_1177_121_v1 with extension -full.tsv or the vcf file with format *Filtered*.vcf	14.20
			err = 'tsv file was not found for the sample %s at %s with extension -full.tsv or the vcf file with format *Filtered*.vcf'%(sample_name, variants_dir)
			md5file_curr.write_tracking_error(err)
			GB.parser.error(err)
		
	else:
		err = 'orginal_zip_md5 %s not equal to cp_zip_md5 %s'%(zipped_file, cp_zip)
		md5file_curr.write_tracking_error(err)
		GB.parser.error(err)
		return 

def addStrandInfoToTSV(vcf_file, tsv_file):
	'''
	(file_address, file_address, dir_address) -> merge_filtered.tsv file
	'''
	######################################################################
	# Find the filtered VCF file to add strand info to the tsv file.
		# There might be more than one file of interest with a different date.  Just take the first one Example:
			# A_97-19_I1086077_v2/A_97-19_I1086077_v2_Filtered_2020-02-12_15:58:24.vcf 
			# A_97-19_I1086077_v2/A_97-19_I1086077_v2_Filtered_2020-02-12_16:00:39.vcf
		# Example of vcf data
			# chr7	55241677	COSM12988;COSM116882;COSM12428	GAA	AAA,CAA,CAT,GAAA	10.3606	PASS	AF=2.18579E-4,0.0,0.0,0.0242623;AO=3,0,0,316;DP=4590;FAO=1,0,0,111;FDP=4575;FDVR=5,5,10,0;FR=.,.,.,.;FRO=4463;FSAF=0,0,0,5;FSAR=1,0,0,106;FSRF=2332;FSRR=2131;FWDB=-0.0880023,0.0226692,0.00624706,-0.106117;FXX=0.00391901;HRUN=2,2,2,3;HS_ONLY=0;LEN=1,1,3,1;MLLD=56.2754,200.289,392.929,22.0858;OALT=A,C,CAT,A;OID=COSM12988,COSM116882,COSM12428,.;OMAPALT=AAA,CAA,CAT,GAAA;OPOS=55241677,55241677,55241677,55241678;OREF=G,G,GAA,-;PB=.,.,.,.;PBP=.,.,.,.;QD=0.00905842;RBI=0.0881522,0.0240901,0.00632361,0.109927;REFB=-0.00404961,0.00311704,0.00101144,-0.0122493;REVB=-0.00513877,0.00815104,9.8096E-4,-0.0286877;RO=4221;SAF=0,0,0,97;SAR=3,0,0,219;SRF=2217;SRR=2004;SSEN=0,0,0,0;SSEP=0,0,0,0;SSSB=-0.0161083,-3.52971E-8,-3.52971E-8,-0.244259;STB=0.990647,0.5,0.5,0.956719;STBP=0.299,1.0,1.0,0.0;TYPE=snp,snp,mnp,ins;VARB=0.0660333,0.0,0.0,0.117016;HS;FUNC=[{'origPos':'55241677','origRef':'GAA','normalizedRef':'G','gene':'EGFR','normalizedPos':'55241677','normalizedAlt':'GA','gt':'pos','codon':'AAC','coding':'c.2128_2129insA','transcript':'NM_005228.4','function':'frameshiftInsertion','protein':'p.Thr710fs','location':'exonic','origAlt':'GAAA','exon':'18'}]	GT:GQ:DP:FDP:RO:FRO:AO:FAO:AF:SAR:SAF:SRF:SRR:FSAR:FSAF:FSRF:FSRR	0/4:10:4590:4575:4221:4463:3,0,0,316:1,0,0,111:2.18579E-4,0.0,0.0,0.0242623:3,0,0,219:0,0,0,97:2217:2004:1,0,0,106:0,0,0,5:2332:2131
		# Desired data format
			# GAA=2332/2131, AAA=0/1, CAA=0/0, CAT=0/0, GAAA=5/106
	# add ALT to variant_allele column 			
	######################################################################

	print('\n################################################\nAdding Strand bias info from:\n\t%s'%(vcf_file))

	################################################
	# Name file depending on if it is workflow filtered or not.  Acrometrix controls pulls unfiltered
	################################################		
	if '_unfiltered.tsv' in tsv_file:
		out_file =  '%s_unfiltered_SB_added.tsv'%(tsv_file.split('_unfiltered.tsv')[0])

	else:
		out_file =  '%s_filtered_SB_added.tsv'%(tsv_file.split('_filtered.tsv')[0])

	vcf_dict = {}

	with open(vcf_file, 'r') as vf, open(tsv_file, 'r') as tf, open(out_file, 'w') as wf:

		################################################################################
		# Obtain all info from the vcf and save in a dictionary
			# FSRF - Flow Evaluator reference observations on the forward strand
			# FSRR - Flow Evaluator reference observations on the reverse strand
			# Third column has the REF: GAA
			# Fourth column has ALT: AAA,CAA,CAT,GAAA (add as observed_variant)
			# combining Third and fourth column will provide order
			# FSAF - Flow Evaluator reference observations on the forward strand
			# FSAR - Flow Evaluator Alternate allele observations on the reverse strand
		################################################################################
		line = vf.readline().rstrip()
		info_header_from_vcf = ''

		mutalyzer_proteins = {}

		while line:

			if 'fsar' in locals():

				del fsar

			if 'fsaf' in locals():

				del fsaf

			if 'split_alt' in locals():

				del split_alt


			if 'ref' in locals():

				del ref


			if 'fsrf' in locals():

				del fsrf


			if 'fsrr' in locals():

				del fsrr

			##########################################################
			# Add all of the info section from the vcf file that does not start with ##FILTER= ##INFO= ##FORMAT=.  This info
			# only pertains to the vcf file formats 
			##########################################################
			if line.startswith('##') and not line.startswith('##FILTER=') and not line.startswith('##INFO=') and not line.startswith('##FORMAT='):
				info_header_from_vcf+='%s\n'%(line) 

			# Skip Format info that starts with ##
			# unfortunately Thermofisher has altering column headers I will have to use position 
			elif not line.startswith('##') and not line.startswith('#'):
				split_line = line.split('\t')
				
				# If there are not at least 7 columns most likely it is an empty line skip
				if len(split_line) < 7:
					break

				ref = split_line[3]
				alt = split_line[4]
				filter_col = split_line[6]

				######################################
				# Add variant NOCALL to final tsv file.
				######################################
				if filter_col == 'NOCALL':
					samp_name = '%s:%s'%(split_line[0], split_line[1])

				elif filter_col != 'PASS':
					err = 'Only NOCALL and PASS are allowed.  Go and check filter column in the VCF file: %s '%(vcf_file)
					GB.parser.error(err)

				split_alt = alt.split(',')
				info_str = split_line[7].split(';')
				
				# Find all of the strand coverage counts for strand bias in the huge mess of a string in info_str
				len_info_str = len(info_str)
				for i in range(0, len_info_str):
					v = info_str[i]

					if v.startswith('FSRF='):
						fsrf = v.replace('FSRF=', '')

					# For no calls FR will contain a reason for instance .,.&PREDICTIONSHIFTx1.30003,.
					elif v.startswith('FR='):
						fr = v.replace('FR=', '')

					elif v.startswith('FSRR='):
						fsrr = v.replace('FSRR=', '')

					elif v.startswith('FSAF='):
						fsaf = v.replace('FSAF=', '').split(',')

					elif v.startswith('FSAR='):
						fsar = v.replace('FSAR=', '').split(',')

					# Thermofisher decided to add sometimes add ; to the middle of the FUNC= JSON like this
						# FUNC=[{'origPos':'55593632','origRef':'CAATTATGTTTACATAGACCCAACACAACT','normalizedRef':'CAATTATGTTTACATAGACCCAACACAACT','gene':'KIT','normalizedPos':'55593632','normalizedAlt':'TAATTATGTTTACATAGACCCAACACAACC','polyphen':'0.256','gt':'pos','coding':'c.1698_1727delCAATTATGTTTACATAGACCCAACACAACTinsTAATTATGTTTACATAGACCCAACACAACC','sift':'0.02','grantham':'98.0','transcript':'NM_000222.2','function':['synonymous','refAllele','refAllele','refAllele','refAllele','refAllele','refAllele','refAllele','refAllele','refAllele','missense'],'protein':'p.[Asn566=;Asn567=;Tyr568=;Val569=;Tyr570=;Ile571=;Asp572=;Pro573=;Thr574=;Gln575=;Leu576Pro]','location':'exonic','origAlt':'TAATTATGTTTACATAGACCCAACACAACC','exon':'11','oncomineGeneClass':'Gain-of-Function','oncomineVariantClass':'Hotspot'}]
					# or this
						# FUNC=[{'origPos':'55593632','origRef':'CAATTATGTTTACATAGACCCAACACAACT','normalizedRef':'CAATTATGTTTACATAGACCCAACACAACT','gene':'KIT','normalizedPos':'55593632','normalizedAlt':'TAATTATGTTTACATAGACCCAACACAACC','polyphen':'0.256','gt':'pos','coding':'c.1698_1727delCAATTATGTTTACATAGACCCAACACAACTinsTAATTATGTTTACATAGACCCAACACAACC','sift':'0.02','grantham':'98.0','transcript':'NM_000222.2','function':['synonymous','refAllele','refAllele','refAllele','refAllele','refAllele','refAllele','refAllele','refAllele','refAllele','missense'],'protein':'p.[Met227Ser;Phe228Ile]','location':'exonic','origAlt':'TAATTATGTTTACATAGACCCAACACAACC','exon':'11','oncomineGeneClass':'Gain-of-Function','oncomineVariantClass':'Hotspot'}]
					# causes the FUNC JSON to be cut prematurely.  Therefore the rest needs to be added back on.  So first I will test if the FUNC= ends with }] this means Thermofisher did not add ; in the str
					elif v.startswith('FUNC=') and v.endswith('}]') and filter_col != 'NOCALL':
						func = v.replace('FUNC=', '')
											
						func_json = json.loads(func.replace("'", '"'))[0]

						if 'coding' in func_json.keys() and 'protein' in func_json.keys():

							samp_name = '%s_%s_%s'%(func_json['gene'], func_json['coding'], func_json['protein'])
						else:
							samp_name = 'skip'

					elif v.startswith('FUNC=') and filter_col != 'NOCALL':
						func = v.replace('FUNC=', '')
						if 'coding' in func and 'protein' in func:
							# Since this json string is not complete now add back the rest of the string until the
							# closing brackets are found

							while True:

								i+=1
								
								if i > len_info_str:
									break

								v = info_str[i]
								
								if v.endswith('}]'):
									func += ';%s'%(v)
									break
								else:
									func += ';%s'%(v)

							func_json = json.loads(func.replace("'", '"'))[0]
							
							# Since the protein name is likely garbage run mutalyzer to find protein name
							mutalyzer_protein = utils.findProteinNameMutalyzer(func_json['transcript'], func_json['coding'])						

							samp_name = '%s_%s_%s'%(func_json['gene'], func_json['coding'], func_json['protein'])

							# Save all mutalyzer proteins to be used later
							mutalyzer_proteins[samp_name] = mutalyzer_protein

						else:
							samp_name = 'skip'

				#############################################################			
				# Make sure sample name was generated before proceeding.
				#############################################################		
				if 'samp_name' not in locals():
					err = '\n!!!!!!!!!!!!!!!!\nERROR OCCURED sample name was not found in:\n\t%s'%(line)

				#############################################################			
				# add ref strand bias info
				#############################################################		
				if 'ref' not in locals() or 'fsrf' not in locals() or 'fsrr' not in locals():

					# err = '\n!!!!!!!!!!!!!!!!\nERROR OCCURED while making strand bias string.  Ref variables not set.:\n\t"ref" not in locals() => %s\n\t"fsrf" not in locals() => %s\n\t"fsrr" not in locals() => %s'%('ref' not in locals(), 'fsrf' not in locals(),'fsrr' not in locals())

					# GB.parser.error(err)

					# see the issue described under check alt strand bias for why this is here.
					ref = 'missing'
					fsrf = 1
					fsrr = 1

					strand_basis = '%s=%s/%s'%(ref, fsrf, fsrr)

				else:
					strand_basis = '%s=%s/%s'%(ref, fsrf, fsrr)
				
				#############################################################			
				# Check alt strand bias info
				#############################################################				
				if 'split_alt' not in locals() or 'fsaf' not in locals() or 'fsar' not in locals():

					# I added an error of fsaf and fsar missing.  I wasn't sure what was causing it at this point.  This sample is 
					# A_2090-21_K816A960_2840_3310_308
					# it has a large indel 
					###################################################################################
					# I looked into the issue.  Infotrack actually behaved exactly like I wanted it to.  

					# Since Theromofisher changes up how there vcf files are formatted randomly I made it error out completely if the format was different than we saw before.  I did this so it wasn't overlooked and caused unknown issues.

					# Below are the two variants in sample A_2090-21_K816A960_2840_3310_308. The chr4 mutation does not have the fields FSAF,FSAR,FSRF, and FSRR (highlighted in yellow in chr8 variant).  This is causing the whole thing to error out.  Infotrack uses these to calculate the strand bias.

					# Every mutation infotrack has ever seen has this info in it.  Any cause why this has changed?

					# I'm unsure why this strand bias info is not being provided for chr4 but is provided for chr8.  The chr4 type is del. While the chr4 type is snp.  I assume we have seen del's before so I do not think this would be the issue.

					# Do you know of another reason?  I need to know why it is happening before I can write anything to handle it. 

					# Were settings in the theromofisher software changed?  

					# VCF Variants for A_2090-21_K816A960_2840_3310_308
					# chr4 55593597 . GTACAGTGGAAGGTTGTTGAGGAGATAAATGGAAACAATTATGTTTACATAGACCCAACACAAC G 50.0 PASS AF=0.232979;AO=3203;DP=13748;FR=.;HS_ONLY=0;LEN=63;OALT=-;OID=.;OMAPALT=G;OPOS=55593598;OREF=TACAGTGGAAGGTTGTTGAGGAGATAAATGGAAACAATTATGTTTACATAGACCCAACACAAC;RO=10545;SAF=1618;SAR=1585;SRF=5356;SRR=5189;TYPE=del;FUNC=[{'origPos':'55593597','origRef':'GTACAGTGGAAGGTTGTTGAGGAGATAAATGGAAACAATTATGTTTACATAGACCCAACACAAC','normalizedRef':'GTACAGTGGAAGGTTGTTGAGGAGATAAATGGAAACAATTATGTTTACATAGACCCAACACAAC','gene':'KIT','normalizedPos':'55593597','normalizedAlt':'G','gt':'pos','coding':'c.1665_1727delACAGTGGAAGGTTGTTGAGGAGATAAATGGAAACAATTATGTTTACATAGACCCAACACAACT','transcript':'NM_000222.2','function':'nonframeshiftDeletion','protein':'p.Gln556_Leu576del','location':'exonic','origAlt':'G','exon':'11'}] GT:GQ:DP:RO:AO:SAF:SAR:SRF:SRR:AF 0/1:99:13748:10545:3203:1618:1585:5356:5189:0.232979
					# chr8 128753248 . G A 24508.1 PASS AF=0.485862;AO=2240;DP=4633;FAO=2251;FDP=4633;FDVR=10;FR=.;FRO=2382;FSAF=1044;FSAR=1207;FSRF=1012;FSRR=1370;FWDB=-0.00459952;FXX=0.00107805;HRUN=1;HS_ONLY=0;LEN=1;MLLD=558.041;OALT=A;OID=.;OMAPALT=A;OPOS=128753248;OREF=G;PB=.;PBP=.;PPA=.;PPD=0;QD=21.1596;RBI=0.00716776;REFB=-0.00287365;REVB=-0.00549737;RO=2357;SAF=1042;SAR=1198;SPD=0;SRF=1000;SRR=1357;SSEN=0;SSEP=0;SSSB=0.0386753;STB=0.520185;STBP=0.007;TYPE=snp;VARB=0.00311559;FUNC=[{'origPos':'128753248','origRef':'G','normalizedRef':'G','gene':'MYC','normalizedPos':'128753248','normalizedAlt':'A','gt':'pos','coding':'c.*44G>A','transcript':'NM_002467.5','function':'unknown','protein':'p.?','location':'utr_3','origAlt':'A','exon':'3'}] GT:GQ:DP:FDP:RO:FRO:AO:FAO:AF:SAR:SAF:SRF:SRR:FSAR:FSAF:FSRF:FSRR 0/1:24508:4633:4633:2357:2382:2240:2251:0.485862:1198:1042:1000:1357:1207:1044:1012:1370

					######################################################################################
					if 'split_alt' in locals():
						
						fsaf = ['1'] * len(split_alt)
						fsar = ['1'] * len(split_alt)
						print('#######################\n!!!!!!!!!!!!!!!!\n')
						print(fsaf)
						print(fsar)
						print(split_alt)
					else:
						err = '\n!!!!!!!!!!!!!!!!\nERROR OCCURED while making strand bias string.  ALT variables not set.:\n\t"split_alt" not in locals() => %s\n\t"fsaf" not in locals() => %s\n\t"fsar" not in locals() => %s'%('split_alt' not in locals(), 'fsaf' not in locals(),'fsar' not in locals())

						GB.parser.error(err)

				# Make sure all alt lists are the same length
				alt_count = len(split_alt)

				if len(fsaf) != alt_count or len(fsar) != alt_count:



					err = '\n!!!!!!!!!!!!!!!!\nERROR OCCURED while making strand bias string.  Lengths are not equal.:\n\tALT len(%s)= %s\n\tFSAF len(%s)= %s\n\tFSAR len(%s)= %s'%(alt_count, alt, len(fsaf), utils.list_to_char_seperated(fsaf, ' '), len(fsar), utils.list_to_char_seperated(fsar, ' '))
					GB.parser.error(err)
				
				#############################################################			
				# add alt strand bias info
					# strand_basis => GAA=2332/2131
					# fsaf => ['0', '0', '0', '5']
					# fsar => ['1', '0', '0', '106']
					# split_alt => ['AAA', 'CAA', 'CAT', 'GAAA']
					# desired result => GAA=2332/2131, AAA=0/1, CAA=0/0, CAT=0/0, GAAA=5/106
				#############################################################
				for i in range(0, alt_count):

					strand_basis+= ', '

					# add current variant
					strand_basis+= '%s=%s/%s'%(split_alt[i], fsaf[i], fsar[i])

				#############################################################
				# Add current variant to the vcf_dict to link to the tsv file later
				#############################################################
				if samp_name in mutalyzer_proteins.keys() and mutalyzer_proteins[samp_name] != False:

					vcf_dict[samp_name.replace(' ', '')] = {'strand_basis':strand_basis, 'variant_allele':alt, 'mutalyzer_protein':mutalyzer_protein, 'filter':filter_col, 'fr':fr}

					# Sometimes Thermofisher decides to drop the [ ] from the protein name so add this version to vcf_dict also
					# Example:
						# p.[Asn566=;Asn567=;Tyr568=;Val569=;Tyr570=;Ile571=;Asp572=;Pro573=;Thr574=;Gln575=;Leu576Pro] becomes
						# p.Asn566=;Asn567=;Tyr568=;Val569=;Tyr570=;Ile571=;Asp572=;Pro573=;Thr574=;Gln575=;Leu576Pro
					vcf_dict[samp_name.replace('[','').replace(']','').replace(' ', '')] = {'strand_basis':strand_basis, 'variant_allele':alt, 'mutalyzer_protein':mutalyzer_protein}	
					
				else:
					vcf_dict[samp_name] = {'strand_basis':strand_basis, 'variant_allele':alt, 'filter':filter_col, 'fr':fr}
				
				print('\nSample Name:\n\n\t%s\n\nOriginal variant:\n\n\t%s\n\nExtracted Info:\n\n\tstrand_basis: %s\n\n\tvariant_allele: %s'%(samp_name, line, strand_basis, alt))

			line = vf.readline().rstrip()

		#############################################################
		# Iterate over the tsv file and add strand bias info from the vcf_dict
			# write header info from vcf_file info_header_from_vcf
			# tsv file has header that starts with ## => Write to wf
			# tsv file has column headers that starts with # 
				# add strand bias column (Ref+/Ref-/Var+/Var-) and variant_allele
				# write to wf 
				# set split_header variable
			# tsv file has variant rows that do not start with # 
				# make sure split_header exists.
				# find the variant by gene_coding_protein 
				# match strand bias info from vcf_dict
				# append strand bias info and write info to wf
		#############################################################

		# write header info from vcf_file info_header_from_vcf
		if info_header_from_vcf != '':
			wf.write(info_header_from_vcf)

		line = tf.readline().rstrip()
		all_curr_samples = []

		while line:

			# tsv file has header that starts with ## => Write to wf
			if line.startswith('##'):
				wf.write('%s\n'%(line))

			############################################################
			# tsv file has column headers that starts with # 
				# add strand bias column (Ref+/Ref-/Var+/Var-) and variant_allele
				# write to wf 
				# set split_header variable
			############################################################
			elif line.startswith('#') and not line.startswith('##'):
				header = '%s\tRef+/Ref-/Var+/Var-\tvariant_allele'%(line)
				wf.write('%s\n'%(header))
				split_header = header.split('\t')

			############################################################
			# tsv file has variant rows that do not start with # 
				# make sure split_header exists.
				# find the variant by gene_coding_protein 
				# match strand bias info from vcf_dict
				# append strand bias info and write info to wf
			############################################################
			elif 'split_line' in locals():
				split_line = line.split('\t')

				match_dict = utils.two_lists_to_dict(split_header, split_line)

				######################################################
				# Thermofisher formating problem found in tsv sample 4896-18.  
					# Problem: Sometimes more than one gene name is found
						# Example:
							# gene: EGFR|EGFR-AS1
							# coding: c.2317_2318insGTC|
							# protein: p.Pro772_His773insArg|
					# Solution split at |
				######################################################

				curr_samp_name = '%s_%s_%s'%(match_dict['gene'].split('|')[0], match_dict['coding'].split('|')[0], match_dict['protein'].split('|')[0])
				all_curr_samples.append(curr_samp_name)

				
				if curr_samp_name in vcf_dict.keys():
					
					curr_vcf_info = vcf_dict[curr_samp_name]

					match_dict['Ref+/Ref-/Var+/Var-'] = curr_vcf_info['strand_basis']
					match_dict['variant_allele'] = curr_vcf_info['variant_allele']

					# add mutalyzer protein if exists
					if 'mutalyzer_protein' in curr_vcf_info.keys():

						print('\nMutalyzer Changed Protein from %s to %s'%(match_dict['protein'], curr_vcf_info['mutalyzer_protein']))


						match_dict['protein'] = curr_vcf_info['mutalyzer_protein']


					row = ''
					for col in split_header:

						if row != '':
							row+='\t'
						
						row+= '%s'%(match_dict[col])

					wf.write('%s\n'%(row))

				# Handle NOCALL
				elif match_dict['filter'] == 'NOCALL' and match_dict['# locus'] in vcf_dict.keys():

					curr_vcf_info = vcf_dict[match_dict['# locus']]

					match_dict['Ref+/Ref-/Var+/Var-'] = curr_vcf_info['strand_basis']
					match_dict['variant_allele'] = curr_vcf_info['variant_allele']

					row = ''
					for col in split_header:

						if row != '':
							row+='\t'
						
						if col == 'coding' or col == 'protein':
							row+= 'NOCALL'

						else:
							row+= '%s'%(match_dict[col])

					wf.write('%s\n'%(row))


				else:
					wf.write('%s\t\n'%(line))

			line = tf.readline().rstrip()


	return out_file

def checkIfExistsOrError(check_loc):

	if not os.path.exists(check_loc):
		err = 'file does not exist %s'%(check_loc)
		md5file_curr.write_tracking_error(err) 
		GB.parser.error(err)
	else:
		return True

def writeTsvFromNestedTuples(nested_tuples, out_dir, of_name):
	'''
		([(),()]) -> tsv
	'''
	utils.make_directories(out_dir)
	out_file = '%s/%s.tsv'%(out_dir, of_name)

	with open(out_file, 'w') as of:

		for t in nested_tuples:
			of.write(utils.list_to_char_seperated(t, '\t'))

def control_center():

	global md5file_curr 
	# md5file class makes md5 file and writes all md5 info
	md5file_curr = md5file.md5file(GB.args.o, GB.args.library_pool_id, GB)

	temp_dir = '%s/temp'%(GB.args.o)

	utils.make_directories(temp_dir)

	for samp in GB.args.sample_list:

		print(samp)

		ir_api_per_sample(samp, temp_dir)

	md5file_curr.finish_tracking()

	###################################################################
	# Delete temp dir
	###################################################################
	cmd = 'rm -r %s'%(temp_dir)
	os.system(cmd)

class LineWrapRawTextHelpFormatter(argparse.RawDescriptionHelpFormatter):
	def _split_lines(self, text, width):
		text = self._whitespace_matcher.sub(' ', text).strip()
		return _textwrap.wrap(text, width)

def arguments():
    # intialize transfer
	GB.parser = argparse.ArgumentParser(description='-----------------------------------------------------------------------------------\n'
		'\nWelcome to the thermo_backup_ion_reporter_run.py Python script\n\n'
		'-----------------------------------------------------------------------------------\n'
		'The purpose of this program is to transfer bam, vcf, varaint tsv files from the Ion reporter to fs-ngs.\n\n\n'
		'These files will be used for backup purpoces but will also be used by infotrack to generate reports.\n'
		'	dependencies: python2, ion reporter version \n\n'		
		'-----------------------------------------------------------------------------------\n',
		formatter_class=LineWrapRawTextHelpFormatter, epilog='-----------------------------------------------------------------------------------\n'
		'Example run:\n\n'
		'-----------------------------------------------------------------------------------\n')

	GB.parser.add_argument('-sample_list', nargs='+', help='<Required list> Add a list of sample names.  These are required to be full names.  The best place to acqurie the correct full name is from the bc_summary file from the covearge analysis plugin.', required=True)
	
	GB.parser.add_argument('-o', nargs='?', help='<String>  output directory')
	

	GB.parser.add_argument('-api_server', nargs='?', help='<String> link to api_server')
	

	GB.parser.add_argument('-api_token', nargs='?', help='<String> api_token.  Refer to Ion Reporter settings page.')

	GB.parser.add_argument('-mounted_loc', nargs='?', help='<String> default /media/2f65182', default='/media/2f65182')

	GB.parser.add_argument('-library_pool_id', nargs='?', help='<int> This id will be used to add to tracker file.')

	GB.args = GB.parser.parse_args()

	# add run_command to args
	GB.args.run_command = utils.list_to_char_seperated(sys.argv, ' ', False)

	check_user_input()

def check_user_input():
	'''
		(namespace, ) ->
	'''
	pass
if __name__ == "__main__":

	print('\n********************\nInput Command: \n\t%s********************\n'%(utils.list_to_char_seperated(sys.argv, ' ')))

	arguments()

	control_center()
