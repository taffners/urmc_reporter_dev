import os
import requests
import json
from lib import utils


def ir_api_per_sample(sample_name):
	'''
		(Mayo13, Mjk5OWY1YmJlODc1Y2FkYTgzMGVkZDJiZjdhNDI4MjA3MmYwMmIxN2IyOWZlYzVhMThmNzAzYTQ4OTliOWRjMg, https://10.149.59.209/api/v1/) -> 2 zip files are made and 
	'''
	import subprocess
	import json
	
	api_curl = 'curl -s --request GET -k -H "Authorization:'
	
	header = '--header "Content-Type:application/x-www-form-urlencoded" '
	 
	api_cmd = 'analysis?format=json&name='

	#######################################################################################
	# Access the samples via the Ion Reporter API and analysis command
	# example sample: Mayo13
		# curl command used via python's subprocess module:
			# curl -s --request GET -k -H "Authorization: Mjk5OWY1YmJlODc1Y2FkYTgzMGVkZDJiZjdhNDI4MjA3MmYwMmIxN2IyOWZlYzVhMThmNzAzYTQ4OTliOWRjMg " --header "Content-Type:application/x-www-form-urlencoded" "https://10.149.59.209/api/v1/analysis?format=json&name=Mayo13"
		# Before running this analysis command there were no .zip files present for this sample
		# After running the analysis command two zip files now are available.
			# Mayo13_v1_c2580_2019-12-18-13-38-37-379_2020-01-13_13-35-47-228_Filtered.zip
			# Mayo13_v1_c2580_2019-12-18-13-38-37-379_2020-01-13_13-35-44-502_All.zip
		# The API returns a JS like the one below with addresses to these files
		# [
		#      {
		#           "data_links": {
		#                "filtered_variants": "https://smh-o1-1-2150-2f65182.urmc-sh.rochester.edu:443/api/v1/download?filePath=/data/IR/data/IR_Org/molecdiag@urmc.edu/Mayo13_v1/Mayo13_v1_20191218133854182/Mayo13_v1_c2580_2019-12-18-13-38-37-379_2020-01-13_13-59-36-442_Filtered.zip",
		#                "unfiltered_variants": "https://smh-o1-1-2150-2f65182.urmc-sh.rochester.edu:443/api/v1/download?filePath=/data/IR/data/IR_Org/molecdiag@urmc.edu/Mayo13_v1/Mayo13_v1_20191218133854182/Mayo13_v1_c2580_2019-12-18-13-38-37-379_2020-01-13_13-59-34-041_All.zip"
		#           },
		#           "flagged": false,
		#           "id": "ff8081816baf0c96016f1a4e47021021",
		#           "ion_reporter_version": "5.10",
		#           "name": "Mayo13_v1_c2580_2019-12-18-13-38-37-379",
		#           "report_published": "",
		#           "reports": {
		#                "final": {},
		#                "qc": {
		#                     "link": "https://smh-o1-1-2150-2f65182.urmc-sh.rochester.edu:443/api/v1/download?type=pdf&filePath=/scratch/tmp/IR_Org/download/pdf/3905945b-25be-4180-9d2b-fbdd995c110d/ff8081816baf0c96016f1a4e47021021_QC.pdf"
		#                }
		#           },
		#           "samples": {
		#                "PROBAND": "Mayo13_v1"
		#           },
		#           "shared_with": [],
		#           "stage": "Send for Report Generation",
		#           "start_date": "December 18, 2019",
		#           "started_by": "Molec Diag",
		#           "status": "SUCCESSFUL",
		#           "variants_saved": "",
		#           "workflow": "OFA_5-10_complex_v3_filter_updated"
		#      }
		# ]
	######################################################################################
	api_token = 'Mjk5OWY1YmJlODc1Y2FkYTgzMGVkZDJiZjdhNDI4MjA3MmYwMmIxN2IyOWZlYzVhMThmNzAzYTQ4OTliOWRjMg'
	api_server = 'https://10.149.59.209/api/v1/'
	os_cmd = '%s %s " %s"%s%s%s"'%(api_curl, api_token, header, api_server, api_cmd, sample_name)

	output = subprocess.check_output(os_cmd, shell=True)
	analysis_json = json.loads(output)

	return json.dumps(analysis_json, indent=5, sort_keys=True)

	# if len(analysis_json) > 0 and 'samples' in analysis_json[0].keys() and 'PROBAND' in analysis_json[0]['samples'].keys() and 'data_links' in analysis_json[0].keys() and 'filtered_variants' in analysis_json[0]['data_links'].keys():
		
	# 	# Thermofisher replaces spaces with _
	# 	proband_sample = analysis_json[0]['samples']['PROBAND'].replace(' ', '_')

	# 	# backupIRSampleData(analysis_json[0]['data_links']['filtered_variants'], sample_name, GB.args.o, proband_sample)

	# else:
	# 	GB.parser.error('Sample %s had an error with the api %s\n\n\nos_cmd = %s'%(sample_name, json.dumps(analysis_json, indent=5, sort_keys=True), os_cmd))

def match_acrometrix_variants_with_vaf_standards(sop_vars_f, acro_vars_f, out_dir):
	'''
		
	'''
	utils.make_directories(out_dir)
	acro_dict = {}
	
	# make a dict of all acrometrix variants with key mutation cds
	with open(acro_vars_f, 'r') as af:

		header = af.readline().rstrip()	

		line = af.readline().rstrip()
		
		while line != '':
			split_line = line.split('\t')

			if split_line[3] != 'genomic':

				acro_dict[split_line[1]] = {'gene': split_line[0], 'aa':split_line[2], 'freq': split_line[3]}

			line = af.readline().rstrip()
	

	acro_keys = acro_dict.keys()

	# find match in sop variants
	with open(sop_vars_f, 'r') as sf, open(out_dir + '/SOP_vars_with_acrometrix_standard.txt', 'w') as wf:
		line = sf.readline().rstrip()

		to_write = 'Gene\tCoding\tProtein\tAcrometrix Standard Target Frequency\tAccepted Range\tGene Match\tacro gene\tProtein Match\tacro protein\n'
		wf.write(to_write)

		while line != '':
			split_line = line.split('\t')

			gene = split_line[0]
			mutatant = split_line[1].replace(')', '').split('(')
			cds = mutatant[0].rstrip()
			aa = mutatant[1].rstrip()

			if cds in acro_keys:
				curr_match = acro_dict[cds]

				if curr_match['freq'] == '5-15%':					
					curr_accepted_range = '<40%'

				elif curr_match['freq'] == '15-35%':
					curr_accepted_range = '>5 and <50%'
				
				else:
					curr_accepted_range = 'unknown'

				to_write = '%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\n'%(gene, cds, aa, curr_match['freq'], curr_accepted_range, gene == curr_match['gene'], curr_match['gene'], aa == curr_match['aa'], curr_match['aa'])
				wf.write(to_write)
			
			else:
				print 'not found %s'%(cds)
			
			line = sf.readline().rstrip()			

def make_sure_sop_var_name_correct(sop_vars_with_vaf_f, control_run_dir, out_dir):

	utils.make_directories(out_dir)

	all_files = utils.get_files_with_extension(control_run_dir, 'tsv')

	for control_run_f in all_files:

		all_control_vars = []

		control_var_by_freq = {}

		print '\n######################\nSample: %s\n\n'%(control_run_f)
		with open(control_run_f, 'r') as cf:

			line = cf.readline()
			while line != '':

				if not line.startswith('#'): 

					split_line = line.split('\t')
					observed_genes = split_line[0].replace('CSDE1,', '').replace(',EGFR-AS1', '')
					observed_coding = split_line[6]
					observed_aa = split_line[7]
					observed_freq = split_line[8]

					observed = observed_genes+' '+observed_coding+' '+observed_aa

					if observed_genes != 'Genes':
						all_control_vars.append(observed)
						control_var_by_freq[observed] = observed_freq

				line = cf.readline()

		with open(sop_vars_with_vaf_f, 'r') as sf:

			header = sf.readline().rstrip()
			line = sf.readline().rstrip()

			while line != '':

				split_line = line.split(',')
				sop_var = split_line[0]+' '+split_line[1]+' '+split_line[2]

				if sop_var not in all_control_vars:
					print 'Variant found in SOP but not found in sample ran%s'%(sop_var)

				else:
					all_control_vars.remove(sop_var)

				line = sf.readline().rstrip()

		print '\nVariants Found in sample %s but not in sop expected variants\n'%(control_run_f)

		for var in all_control_vars:

			print var, control_var_by_freq[var]

def find_control_passed(sop_vars_with_vaf_f, control_run_dir, out_dir):

	utils.make_directories(out_dir)

	all_files = utils.get_files_with_extension(control_run_dir, 'tsv')

	# Make a dictionary of all variants excepted 
	with open(sop_vars_with_vaf_f, 'r') as sf:

		header = sf.readline().rstrip()
		line = sf.readline().rstrip()

		expected_vars = {}

		while line != '':

			gene, coding, protein, acrometrix_range, accepted_range = line.split(',')

			expected_vars[gene+coding+protein] = {'accepted_range': accepted_range}

			line = sf.readline().rstrip()

def ir_api(sample_name, api_token, api_server):
	'''
		(Mayo13, Mjk5OWY1YmJlODc1Y2FkYTgzMGVkZDJiZjdhNDI4MjA3MmYwMmIxN2IyOWZlYzVhMThmNzAzYTQ4OTliOWRjMg, https://10.149.59.209/api/v1/) -> 2 zip files are made and 
	'''

	import subprocess
	import json
	
	api_curl = 'curl -s --request GET -k -H "Authorization:'
	
	header = '--header "Content-Type:application/x-www-form-urlencoded" '
	 
	api_cmd = 'analysis?format=json&name='

	#######################################################################################
	# Access the samples via the Ion Reporter API and analysis command
	# example sample: Mayo13
		# curl command used via python's subprocess module:
			# curl -s --request GET -k -H "Authorization: Mjk5OWY1YmJlODc1Y2FkYTgzMGVkZDJiZjdhNDI4MjA3MmYwMmIxN2IyOWZlYzVhMThmNzAzYTQ4OTliOWRjMg " --header "Content-Type:application/x-www-form-urlencoded" "https://10.149.59.209/api/v1/analysis?format=json&name=Mayo13"
		# Before running this analysis command there were no .zip files present for this sample
		# After running the analysis command two zip files now are available.
			# Mayo13_v1_c2580_2019-12-18-13-38-37-379_2020-01-13_13-35-47-228_Filtered.zip
			# Mayo13_v1_c2580_2019-12-18-13-38-37-379_2020-01-13_13-35-44-502_All.zip
		# The API returns a JS like the one below with addresses to these files
		# [
		#      {
		#           "data_links": {
		#                "filtered_variants": "https://smh-o1-1-2150-2f65182.urmc-sh.rochester.edu:443/api/v1/download?filePath=/data/IR/data/IR_Org/molecdiag@urmc.edu/Mayo13_v1/Mayo13_v1_20191218133854182/Mayo13_v1_c2580_2019-12-18-13-38-37-379_2020-01-13_13-59-36-442_Filtered.zip",
		#                "unfiltered_variants": "https://smh-o1-1-2150-2f65182.urmc-sh.rochester.edu:443/api/v1/download?filePath=/data/IR/data/IR_Org/molecdiag@urmc.edu/Mayo13_v1/Mayo13_v1_20191218133854182/Mayo13_v1_c2580_2019-12-18-13-38-37-379_2020-01-13_13-59-34-041_All.zip"
		#           },
		#           "flagged": false,
		#           "id": "ff8081816baf0c96016f1a4e47021021",
		#           "ion_reporter_version": "5.10",
		#           "name": "Mayo13_v1_c2580_2019-12-18-13-38-37-379",
		#           "report_published": "",
		#           "reports": {
		#                "final": {},
		#                "qc": {
		#                     "link": "https://smh-o1-1-2150-2f65182.urmc-sh.rochester.edu:443/api/v1/download?type=pdf&filePath=/scratch/tmp/IR_Org/download/pdf/3905945b-25be-4180-9d2b-fbdd995c110d/ff8081816baf0c96016f1a4e47021021_QC.pdf"
		#                }
		#           },
		#           "samples": {
		#                "PROBAND": "Mayo13_v1"
		#           },
		#           "shared_with": [],
		#           "stage": "Send for Report Generation",
		#           "start_date": "December 18, 2019",
		#           "started_by": "Molec Diag",
		#           "status": "SUCCESSFUL",
		#           "variants_saved": "",
		#           "workflow": "OFA_5-10_complex_v3_filter_updated"
		#      }
		# ]
	######################################################################################
	os_cmd = '%s %s " %s"%s%s%s"'%(api_curl, api_token, header, api_server, api_cmd, sample_name)

	output = subprocess.check_output(os_cmd, shell=True)
	analysis_json = json.loads(output)

	print json.dumps(analysis_json, indent=5, sort_keys=True)

def backupIRSampleData(zipped_file, sample_name, out_dir, proband):
	'''
		file is placed at ionadmin@2F65182:/share/backup_data

		Run this function on each sample after the ir_api run

		This function will copy the tsv, vcf, and bam file so the out_dir and return a list of tuples of md5s for each file copied.

		Example list of tuples returned:
		[('Original_file', 'Original_md5', 'cp_file', 'cp_m5', 'match', 'file_type', 'sample_name'), ('/data/IR/data/IR_Org/molecdiag@urmc.edu/Mayo13_v1/Mayo13_v1_20191218133854182/Mayo13_v1_c2580_2019-12-18-13-38-37-379_2020-01-13_13-59-36-442_Filtered.zip', 'e896bed86e77ad3e9f3c51ca9f7e9ae1', '/share/backup_data/test_cp_dir/temp/Mayo13.zip', 'e896bed86e77ad3e9f3c51ca9f7e9ae1', True, 'zip_variant_dir', 'Mayo13'), ('/share/backup_data/test_cp_dir/temp/Mayo13/Variants/Mayo13_v1/Mayo13_v1-full.tsv', '6d0d4fd43dd4f8e087f696a700f6b7d4', '/share/backup_data/test_cp_dir/variant_folder/Mayo13_filtered.tsv', '6d0d4fd43dd4f8e087f696a700f6b7d4', True, 'variant_tsv', 'Mayo13'), ('/share/backup_data/test_cp_dir/temp/Mayo13/Workflow_Settings/Analysis_Settings/analysis_settings.txt', '9c28a8b5a99af8fd7c68c2ab19768c93', '/share/backup_data/test_cp_dir/temp/Mayo13/Variants/Mayo13_v1/analysis_settings/Mayo13_analysis_settings.txt', '9c28a8b5a99af8fd7c68c2ab19768c93', True, 'variant_settings', 'Mayo13'), ('/data/IR/data/IR_Org/molecdiag@urmc.edu/Mayo13_v1/Mayo13_v1_20191218133854182/outputs/VariantCallerActor-00/VariantOutput.filtered.vcf', 'e29b1562f2f41baaf86dad21cf4ef850', '/share/backup_data/test_cp_dir/vcf/Mayo13.vcf', 'e29b1562f2f41baaf86dad21cf4ef850', True, 'vcf', 'Mayo13'), ('/data/IR/data/IR_Org/molecdiag@urmc.edu/Mayo13_v1/Mayo13_v1_20191218133854182/outputs/VariantCallerActor-00/merged.bam.ptrim.bam', 'ad86f0fca8da588a45fe26aca5df1546', '/share/backup_data/test_cp_dir/bam/Mayo13.bam.ptrim.bam', 'ad86f0fca8da588a45fe26aca5df1546', True, 'bam', 'Mayo13'), ('/data/IR/data/IR_Org/molecdiag@urmc.edu/Mayo13_v1/Mayo13_v1_20191218133854182/outputs/VariantCallerActor-00/merged.bam.ptrim.bam.bai', '63200d28a19b45707eb532a923c3cd5f', '/share/backup_data/test_cp_dir/bam/Mayo13.bam.ptrim.bam.bai', '63200d28a19b45707eb532a923c3cd5f', True, 'bai', 'Mayo13')
	'''

	# Move to a temporary location and unzip folder because of permission settings on orginal parent
	# folder it needs to be outside of that folder
	md5s = [('Original_file', 'Original_md5', 'cp_file', 'cp_m5', 'match', 'file_type', 'sample_name')]

	utils.make_directories(out_dir)

	temp_dir = '%s/temp'%(out_dir)

	utils.make_directories(temp_dir)

	cp_zip = '%s/%s.zip'%(temp_dir, sample_name)
	cmd = 'cp %s %s'%(zipped_file, cp_zip)
	
	os.system(cmd)

	###################################################################
	# Variant TSV is nested inside a zipped folder.  Therefore copy 
	# The zipped folder to a temporary location and check CP occurred correctly
	###################################################################

	orginial_zip_md5 = utils.md5_large_file(zipped_file)

	cp_zip_md5 = utils.md5_large_file(cp_zip)

	md5_tuple = returnCompareMd5List(zipped_file, cp_zip, sample_name, 'zip_variant_dir')

	md5s.append(md5_tuple)

	# if transfer happened correctly unzip folder
	if md5_tuple[4]:

		###################################################################
		# unzip folder and copy the -full.tsv file and the Workflow_Settings/Analysis_Settings/analysis_settings.txt to the out_dir/variant_folder and out_dir/variant_folder/analysis_settings respectively 
		###################################################################
		unzipped_folder = '%s/%s'%(temp_dir, sample_name)

		utils.unzip(zipped_file, unzipped_folder)

		# Find the full.tsv file
		variants_dir = '%s/Variants/%s'%(unzipped_folder,proband)

		tsv_file = utils.get_files_with_match(variants_dir, '-full.tsv')

		variant_folder = '%s/variant_folder'%(out_dir)
		utils.make_directories(variant_folder)

		cp_tsv = '%s/%s_filtered.tsv'%(variant_folder, sample_name)

		cmd = 'cp %s %s'%(tsv_file[0], cp_tsv) 

		os.system(cmd)

		md5s.append(returnCompareMd5List(tsv_file[0], cp_tsv, sample_name, 'variant_tsv'))

		###################################################################
		# cp analysis_settings file
		###################################################################
		analysis_settings_dir = '%s/analysis_settings'%(variants_dir)
		utils.make_directories(analysis_settings_dir)

		orginal_settings_f = '%s/Workflow_Settings/Analysis_Settings/analysis_settings.txt'%(unzipped_folder)
		cp_settings_f = '%s/%s_analysis_settings.txt'%(analysis_settings_dir, sample_name)

		cmd = 'cp %s %s'%(orginal_settings_f, cp_settings_f) 
		os.system(cmd)

		md5s.append(returnCompareMd5List(orginal_settings_f, cp_settings_f, sample_name, 'variant_settings'))
		
		###################################################################
		# Delete temp dir
		###################################################################
		cmd = 'rm -r %s'%(temp_dir)
		os.system(cmd)

		###################################################################
		# Copy the vcf, bam, and bai files from outputs/VariantCallerActor-00
		# I'm unsure if the folder is always called VariantCallerActor-00 For now I will 
		# assume that it is.
		###################################################################
		
		# the main folder is where the IR stores all of the analysis for this sample
		# Example: /data/IR/data/IR_Org/molecdiag@urmc.edu/Mayo13_v1/Mayo13_v1_20191218133854182
		VC_actor_dir = '%s/outputs/VariantCallerActor-00'%(os.path.dirname(os.path.realpath(zipped_file)))

		########################################################################
		# Copy VCF files
		########################################################################
		orginal_vcf = '%s/VariantOutput.filtered.vcf'%(VC_actor_dir)
		
		# cp
		vcf_folder = '%s/vcf'%(out_dir)
		utils.make_directories(vcf_folder)
		cp_vcf = '%s/%s.vcf'%(vcf_folder, sample_name)

		cmd = 'cp %s %s'%(orginal_vcf, cp_vcf)
		os.system(cmd)

		md5s.append(returnCompareMd5List(orginal_vcf, cp_vcf, sample_name, 'vcf'))

		########################################################################
		# Copy bam files
		########################################################################
		orginal_bam = '%s/merged.bam.ptrim.bam'%(VC_actor_dir)

		# cp
		bam_folder = '%s/bam'%(out_dir)
		utils.make_directories(bam_folder)
		cp_bam = '%s/%s.bam.ptrim.bam'%(bam_folder, sample_name)

		cmd = 'cp %s %s'%(orginal_bam, cp_bam)
		os.system(cmd)

		md5s.append(returnCompareMd5List(orginal_bam, cp_bam, sample_name, 'bam'))

		########################################################################
		# Copy bai files
		########################################################################
		orginal_bai = '%s/merged.bam.ptrim.bam.bai'%(VC_actor_dir)

		# cp
		cp_bai = '%s/%s.bam.ptrim.bam.bai'%(bam_folder, sample_name)

		cmd = 'cp %s %s'%(orginal_bai, cp_bai)
		os.system(cmd)

		md5s.append(returnCompareMd5List(orginal_bai, cp_bai, sample_name, 'bai'))
		
	else:
		return 'orginal_zip_md5 %s not equal to cp_zip_md5 %s'

def returnCompareMd5List(original_f, cp_f, samp_name, file_type):
	'''
		(str,str,str,str) -> tuple
		return a tuple to keep track of transfer
	'''

	original_md5 = utils.md5_large_file(original_f)

	cp_md5 = utils.md5_large_file(cp_f)

	return (original_f, original_md5, cp_f, cp_md5, original_md5 == cp_md5, file_type, samp_name)

def writeTsvFromNestedTuples(nested_tuples, out_dir, of_name):
	'''
		([(),()]) -> tsv
	'''
	utils.make_directories(out_dir)
	out_file = '%s/%s.tsv'%(out_dir, of_name)

	with open(out_file, 'w') as of:

		for t in nested_tuples:
			of.write(utils.list_to_char_seperated(t, '\t'))


def planned_experiment_resource_example(USERNAME, API_KEY, BASE_URL):
	headers = {"Authorization": "ApiKey " + USERNAME + ":" + API_KEY}
	simple_plan_json = {
	    	'library': 'hg19',
		'planName': 'test_kits_barcode',
		'sample': 'my_sample',
		'chipType': '530',
		'sequencekitname': 'Ion S5 Sequencing Kit',
		'librarykitname': 'Ion AmpliSeq 2.0 Library Kit',
		'templatingKitName': 'Ion Chef S530 V2'
	}

	relevant_run = {
	    	'library': 'hg19',
		'planName': 'test_kits_barcode',
		'sample': 'my_sample',
		'chipType': '530',
		'sequencekitname': 'Ion S5 Sequencing Kit',
		'librarykitname': 'Ion AmpliSeq 2.0 Library Kit',
		'templatingKitName': 'Ion Chef S530 V2',
		'barcodeId':'IonXpress',
		"barcodedSamples": {
			'2291-18_H3245719': {
				'barcodeSampleInfo': {
				'IonXpress_022': {
					'description': 'description here',
					'hotSpotRegionBedFile': '/results/uploads/BED/9/hg19/unmerged/detail/Oncomine_Focus.20160219.hotspots.bed',
					'nucleotideType': 'DNA',
					'reference': 'hg19',
					'targetRegionBedFile': '/results/uploads/BED/2/hg19/unmerged/detail/Oncomine_Focus_DNA_20160219_designed.bed'
					}
				},
				'barcodes': ['IonXpress_022']
			}
		}
	}

	plan_json = {
		"library": "hg19",
		"planName": "DOCS_my_plan",
		"sample": "my_sample",
		"chipType": "520",
		"sequencekitname": "Ion S5 Sequencing Kit",
		"librarykitname": "Ion Xpress Plus Fragment Library Kit",
		"templatingKitName": "Ion 520/530 Kit-OT2",
		"barcodeId": "IonXpress",
		"barcodedSamples": {
			'demo sample 1': {
			'barcodeSampleInfo': {
				'IonXpress_003': {
				'description': 'description here',
				'hotSpotRegionBedFile': '',
				'nucleotideType': 'DNA',
				'reference': 'hg19',
				'targetRegionBedFile': ''
				}
			},
			'barcodes': ['IonXpress_003']
		}
			}
		}
	


	# simple plan works and returns a 201 however, the librarykitname is not set.
	# response = requests.post(BASE_URL + "/rundb/api/v1/plannedexperiment/", headers=headers, data=json.dumps(simple_plan_json))
	# response.raise_for_status()
	# print '###############################################\nNon-barcoded Run Default\n###############################################\n'
	# print response.status_code


	response = requests.post(BASE_URL + "/rundb/api/v1/plannedexperiment/", headers=headers, data=json.dumps(plan_json))
	response.raise_for_status()
	print '###############################################\nbarcoded Run Default\n###############################################\n'
	print response.status_code

	# # barcode_plan_json does not work and returns the following error
	# # 	  File "test.py", line 199, in planned_experiment_resource_example
	# #     response.raise_for_status()
	# #   File "/usr/lib/python2.7/site-packages/requests/models.py", line 834, in raise_for_status
	# #     raise HTTPError(http_error_msg, response=self)
	# # requests.exceptions.HTTPError: 400 Client Error: BAD REQUEST
	# response = requests.post(BASE_URL + "/rundb/api/v1/plannedexperiment/", headers=headers, data=json.dumps(relevant_run))
	# response.raise_for_status()
	# print '###############################################\nNon-barcoded Run Relevant\n###############################################\n'
	# print response.status_code


################################################################################
# feb 28 2020
################################################################################
sample_name = 'B_3643-19_I8217661_1014_926_100'
out_dir = 'test_ir'
utils.make_directories(out_dir)
out_f = '%s/api_response.txt'%(out_dir)

with open(out_f,'w',0) as of:
	import time

	starttime = time.time()

	while True:
		of.write('%s\n\n'%(time.time()))
		of.write('%s\n\n'%(time.time() - starttime))
		time.sleep(300.0 - ((time.time() - starttime) % 300.0))
		# time.sleep(10.0 - ((time.time() - starttime) % 10.0))
		response = ir_api_per_sample(sample_name)
		of.write('%s\n\n'%(response))
		of.write('\n#########################################\n\n')

################################################################################
# test planned experiement from docs (1/15/2020) working zoom meeting
# https://ion-torrent-sdk.readthedocs.io/en/latest/api/examples.html#planning-a-barcoded-run
################################################################################

# USERNAME = 'ionadmin'
# API_KEY = '3f2cdec72f8b1783dfc1222cb4f4164c79de53a7'
# BASE_URL = 'http://10.149.58.208'

# planned_experiment_resource_example(USERNAME, API_KEY, BASE_URL)


################################################################################
# unzip folder and contents test jan 13, 2020					STEP 2 !!!
################################################################################
# sample_name = 'Mayo13'
# zipped_file = '/data/IR/data/IR_Org/molecdiag@urmc.edu/Mayo13_v1/Mayo13_v1_20191218133854182/Mayo13_v1_c2580_2019-12-18-13-38-37-379_2020-01-13_13-59-36-442_Filtered.zip'
# out_dir = '/share/backup_data/test_cp_dir'
# proband = 'Mayo13_v1'

# # md5s = backupIRSampleData(zipped_file, sample_name, out_dir, proband)

# md5_dir = '%s/md5s'%(out_dir) 

# md5s = [('Original_file', 'Original_md5', 'cp_file', 'cp_m5', 'match', 'file_type', 'sample_name'), ('/data/IR/data/IR_Org/molecdiag@urmc.edu/Mayo13_v1/Mayo13_v1_20191218133854182/Mayo13_v1_c2580_2019-12-18-13-38-37-379_2020-01-13_13-59-36-442_Filtered.zip', 'e896bed86e77ad3e9f3c51ca9f7e9ae1', '/share/backup_data/test_cp_dir/temp/Mayo13.zip', 'e896bed86e77ad3e9f3c51ca9f7e9ae1', True, 'zip_variant_dir', 'Mayo13'), ('/share/backup_data/test_cp_dir/temp/Mayo13/Variants/Mayo13_v1/Mayo13_v1-full.tsv', '6d0d4fd43dd4f8e087f696a700f6b7d4', '/share/backup_data/test_cp_dir/variant_folder/Mayo13_filtered.tsv', '6d0d4fd43dd4f8e087f696a700f6b7d4', True, 'variant_tsv', 'Mayo13'), ('/share/backup_data/test_cp_dir/temp/Mayo13/Workflow_Settings/Analysis_Settings/analysis_settings.txt', '9c28a8b5a99af8fd7c68c2ab19768c93', '/share/backup_data/test_cp_dir/temp/Mayo13/Variants/Mayo13_v1/analysis_settings/Mayo13_analysis_settings.txt', '9c28a8b5a99af8fd7c68c2ab19768c93', True, 'variant_settings', 'Mayo13'), ('/data/IR/data/IR_Org/molecdiag@urmc.edu/Mayo13_v1/Mayo13_v1_20191218133854182/outputs/VariantCallerActor-00/VariantOutput.filtered.vcf', 'e29b1562f2f41baaf86dad21cf4ef850', '/share/backup_data/test_cp_dir/vcf/Mayo13.vcf', 'e29b1562f2f41baaf86dad21cf4ef850', True, 'vcf', 'Mayo13'), ('/data/IR/data/IR_Org/molecdiag@urmc.edu/Mayo13_v1/Mayo13_v1_20191218133854182/outputs/VariantCallerActor-00/merged.bam.ptrim.bam', 'ad86f0fca8da588a45fe26aca5df1546', '/share/backup_data/test_cp_dir/bam/Mayo13.bam.ptrim.bam', 'ad86f0fca8da588a45fe26aca5df1546', True, 'bam', 'Mayo13'), ('/data/IR/data/IR_Org/molecdiag@urmc.edu/Mayo13_v1/Mayo13_v1_20191218133854182/outputs/VariantCallerActor-00/merged.bam.ptrim.bam.bai', '63200d28a19b45707eb532a923c3cd5f', '/share/backup_data/test_cp_dir/bam/Mayo13.bam.ptrim.bam.bai', '63200d28a19b45707eb532a923c3cd5f', True, 'bai', 'Mayo13')]

# write md5 check sum file for sample
# writeTsvFromNestedTuples(md5s, md5_dir, sample_name)

################################################################################
# test ir api Jan 13, 2020 									STEP 1 !!!
################################################################################
# sample_name = 'Mayo13'
# api_token = 'Mjk5OWY1YmJlODc1Y2FkYTgzMGVkZDJiZjdhNDI4MjA3MmYwMmIxN2IyOWZlYzVhMThmNzAzYTQ4OTliOWRjMg'
# api_server = 'https://10.149.59.209/api/v1/'
# ir_api(sample_name, api_token, api_server)

################################################################################
# test planned experiement from docs (1/6/2020)
# https://ion-torrent-sdk.readthedocs.io/en/latest/api/examples.html#planning-a-barcoded-run
################################################################################

# USERNAME = 'ionadmin'
# API_KEY = '3f2cdec72f8b1783dfc1222cb4f4164c79de53a7'
# BASE_URL = 'http://10.149.58.208'

# planned_experiment_resource_example(USERNAME, API_KEY, BASE_URL)


################################################################################
# find_control_passed('SOP_vars_with_acrometrix_standard.csv', 'acrometrix_samples_ran', 'out')
# match_acrometrix_variants_with_vaf_standards('variants_in_SOP_acrometrix.txt', 'acrometrix_control_abrev.txt', 'out')


