from __future__ import print_function
#!/usr/bin/python

# Libraries
import argparse
import os
import sys
import subprocess
import textwrap as _textwrap

from lib import GB
# call GB init to make globals
GB.init()

from lib import utils

class LineWrapRawTextHelpFormatter(argparse.RawDescriptionHelpFormatter):
	def _split_lines(self, text, width):
		text = self._whitespace_matcher.sub(' ', text).strip()
		return _textwrap.wrap(text, width)

def arguments():
    # intialize transfer
	GB.parser = argparse.ArgumentParser(description='-----------------------------------------------------------------------------------\n'
		'\nWelcome to the Ion Torrent File Transfer Python script\n\n'
		'-----------------------------------------------------------------------------------\n'
		'The purpose of this program is to transfer files from the Ion torrent to the fs-ofa then fs-ngs\n\n\n'
		'ion_torrent_file_transfer.py uses the following software, languages, database\n'
		'	python2, ion torrent\n\n'		
		'-----------------------------------------------------------------------------------\n',
		formatter_class=LineWrapRawTextHelpFormatter, epilog='-----------------------------------------------------------------------------------\n'
		'Example run:\n\n'
		'-----------------------------------------------------------------------------------\n')

	GB.parser.add_argument('-r','--report_name', nargs='?', help='<String>  Enter the report Name Example:	Auto_user_S5XL-0108-116-200102A_OFA_DNA_530_p190806_183')

	GB.parser.add_argument('-infotrack_version', nargs='?', help='<String>  Add version on infotrack so it can be tracked in log file.')
	
	GB.args = GB.parser.parse_args()

	# add run_command to args
	GB.args.run_command = utils.list_to_char_seperated(sys.argv, ' ', False)

	check_user_input()

def check_user_input():
	'''
		(namespace, ) ->
	'''
	pass

if __name__ == "__main__":

	arguments()

	# make directory 
	utils.make_directories('%s/test'%(GB.fs_ngs_save_loc))
