from __future__ import print_function
#!/usr/bin/python

# Libraries
import argparse
import os
import sys
import subprocess
import textwrap as _textwrap
import json

from lib import GB
# call GB init to make globals
GB.init()

from lib import utils

def control_center():
	utils.make_directories(GB.args.o)

	# copy bed file on this server to out folder
	cp_ofa_bed_file = '%s/%s'%(GB.args.o, os.path.basename(GB.ofa_amplicon_bed_file))

	cmd = 'cp %s %s'%(GB.ofa_amplicon_bed_file, cp_ofa_bed_file)
	os.system(cmd)

	utils.update_user('Starting to create coverage files by merging\n\t%s\n\t%s\n\t%s'%(cp_ofa_bed_file, GB.args.select_dump, GB.args.bcmatrix))

	all_amp_coverage_file = '%s/all_coverage_amplicons.tsv'%(GB.args.o)
	low_amp_coverage_file = '%s/Low_coverage_amplicons.tsv'%(GB.args.o)

	# load select_dump json file
	with open(GB.args.select_dump, 'r') as jsf, open(GB.args.bcmatrix, 'r') as mf, open(low_amp_coverage_file, 'w') as lwf, open(all_amp_coverage_file, 'w') as awf, open(GB.ofa_amplicon_bed_file,'r') as bedf:


		# find locations of all amplicons in the amp_bed_file
		# chrom	contig_srt	contig_end	region_id	attributes	chr	gene	hotspot	last_column
		# chr1	11174373	11174495	Oncomine_Focus_MTOR_1	GENE_ID=MTOR;PURPOSE=Hotspot;CNV_ID=MTOR;CNV_HS=0	1	MTOR	1
		bed_header = bedf.readline().rstrip()

		line = bedf.readline().rstrip()

		amp_dict = {}

		while line:

			split_line = line.split('\t')

			amp_dict[split_line[3]] = [split_line[0], split_line[1], split_line[2]]

			line = bedf.readline().rstrip()

		# This select dump json is made in infotrack and it matches sample name with barcode 
		# since the the bcmatrix file does not contain sample names only barcodes
		select_dump_json = json.load(jsf)
		select_dump_json = select_dump_json['barcode_by_sample_name']

		header = mf.readline().rstrip()

		barcode_split_header = header.split('	')
		split_header = ['Gene', 'Amp']
		
		lwf.write('Gene\tAmplicon\tSample\tCoverage\tchrom\tstart\tstop\n')
		awf.write('Gene\tAmplicon\tSample\tCoverage\tchrom\tstart\tstop\n')

		# The header includes barcodes and I want to replace them with sample names from select_dump_json
		barcodes = select_dump_json.keys()

		for col in barcode_split_header:

			if col in barcodes:

				split_header.append(select_dump_json[col])

		line = mf.readline().rstrip()

		while line:

			split_line = line.split('	')

			match_dict = utils.two_lists_to_dict(split_header, split_line)

			# write to the file any amplicon lower than 500x cov
			amp_keys = match_dict.keys()

			gene = match_dict['Gene']
			amp = match_dict['Amp']

			loc_amp = utils.list_to_char_seperated(amp_dict[amp], '\t', False)

			for samp in amp_keys:

				if samp != 'Gene' and samp != 'Amp':
					
					if match_dict[samp].isdigit() and int(match_dict[samp]) < GB.args.coverage_cut_off:

						lwf.write('%s\t%s\t%s\t%s\t%s\n'%(gene, amp, samp, match_dict[samp], loc_amp))
					awf.write('%s\t%s\t%s\t%s\t%s\n'%(gene, amp, samp, match_dict[samp], loc_amp))
			line = mf.readline().rstrip()

	utils.update_user('Done creating the following coverage files\n\t%s\n\t%s'%(all_amp_coverage_file,low_amp_coverage_file ))

class LineWrapRawTextHelpFormatter(argparse.RawDescriptionHelpFormatter):
	def _split_lines(self, text, width):
		text = self._whitespace_matcher.sub(' ', text).strip()
		return _textwrap.wrap(text, width)

def arguments():
    # intialize transfer
	GB.parser = argparse.ArgumentParser(description='-----------------------------------------------------------------------------------\n'
		'\nWelcome to the Torrent Server Coverage file Python script\n\n'
		'-----------------------------------------------------------------------------------\n'
		'For each chip this script uses the bcmatrix file, the a select_dump json file, and a bed file of all of the amplicon genome locations (built into Infotrack) to make two coverage files.  all_coverage_amplicons and low_coverage_amplicons.  \n'
		'-----------------------------------------------------------------------------------\n',
		formatter_class=LineWrapRawTextHelpFormatter, epilog='-----------------------------------------------------------------------------------\n'
		'Example run:\n\n'
		'python python/thermo_make_cov_files.py -select_dump /media/storage/fs-ngs/dev/20190829_R7DUF_DAEJ00664_178/qc/20190829_R7DUF_DAEJ00664_178select_api_dump.json -bcmatrix /media/storage/fs-ngs/dev/20190829_R7DUF_DAEJ00664_178/qc/20190829_R7DUF_DAEJ00664_178bcmatrix.tsv -o /media/storage/fs-ngs/dev/20190829_R7DUF_DAEJ00664_178/coverage\n'
		'-----------------------------------------------------------------------------------\n')
	
	GB.parser.add_argument('-bcmatrix', nargs='?', help='<file>  Address to the copied bcmatrix.tsv file which is made on the torrent server in the Coverage plugin')

	GB.parser.add_argument('-select_dump', nargs='?', help='<file>  Address to the json file made by Infotrack by gathering information from the Torrent server API and some json files.')

	GB.parser.add_argument('-coverage_cut_off', nargs='?', type=int, required=False, default=500, help="<int> \t\tSet the coverage cut off. Any amplicons that have a coverage less than this toggle with be added to the low_coverage_amplicons file.  This file is used for uploading to Infotrack.  Default is 500")

	GB.parser.add_argument('-o', nargs='?', help='<String>  output directory')

	GB.args = GB.parser.parse_args()

	# add run_command to args
	GB.args.run_command = utils.list_to_char_seperated(sys.argv, ' ', False)

	check_user_input()

def check_user_input():
	'''
		(namespace, ) ->
	'''
	pass

if __name__ == "__main__":

	print('\n********************\nInput Command: \n\t%s********************\n'%(utils.list_to_char_seperated(sys.argv, ' ')))


	arguments()

	control_center()
	
