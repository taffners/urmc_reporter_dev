<?php
	
	// set up db connection
	if (explode('/', $_SERVER['REQUEST_URI'])[1] === 'devs')
	{
		require_once($_SERVER['DOCUMENT_ROOT'].'/devs/urmc_reporter/config.php');
	}
	elseif (explode('/', $_SERVER['REQUEST_URI'])[1] === 'urmc_reporter')
	{
		require_once($_SERVER['DOCUMENT_ROOT'].'/urmc_reporter/config.php');
	}

	// Query database to see if the tier is already present for this variant in the variant_tier_xref table

	$add_xref = array();

	$add_xref['tier_id'] = $_GET['tier_id'];

	if ($_GET['tissue_id'] !== '')
	{
		$add_xref['tissue_id'] = $_GET['tissue_id'];
	}
	
	$add_xref['knowledge_id'] = $_GET['knowledge_id'];
	$add_xref['user_id'] = $_GET['user_id'];


	$result = $db->addOrModifyRecord('variant_tier_xref', $add_xref);
	
	print_r($result);

	exit();
?>