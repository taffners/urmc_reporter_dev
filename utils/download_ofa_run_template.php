<?php
	$_GET['page'] = 'Get OFA Run Template';
	// set up db connection
	if (explode('/', $_SERVER['REQUEST_URI'])[1] === 'devs')
	{
		require_once($_SERVER['DOCUMENT_ROOT'].'/devs/urmc_reporter/config.php');
	}
	elseif (explode('/', $_SERVER['REQUEST_URI'])[1] === 'urmc_reporter')
	{
		require_once($_SERVER['DOCUMENT_ROOT'].'/urmc_reporter/config.php');
	}

	// get data from database for run template.  This will only return data related 
	// to the chip currently downloading.
	$chip_data = $db->listAll('ofa-run-template-by-chip', $_POST);
	
	// All the columns expected by the ion torrent for the run template
	$col_header_str = 'Barcode,Control Type,Sample Name (required),Sample ID,Sample Description,DNA/RNA/Fusions,Reference library,Target regions BED file,Hotspot regions BED file,Cancer Type,Cellularity %,Biopsy Days,Couple ID,Embryo ID,IR Workflow,IR Relation,IR Gender,IR Set ID';

	// locations of files so data can be analyzed 
	$target_regions_file = '/results/uploads/BED/2/hg19/unmerged/detail/Oncomine_Focus_DNA_20160219_designed.bed';
	$hotspot_regions_file = '/results/uploads/BED/9/hg19/unmerged/detail/Oncomine_Focus.20160219.hotspots.bed';

	// array to save all csv output data
	$csv = array(
		'chip' 	=> 	$_POST['chip'],
		'pool_chip_linker_id' 	=> 	$_POST['pool_chip_linker_id'],
		'CSV Version (required),2',
		$col_header_str
	);

	// iterate of the data for this chip and add it to the csv output array.
	foreach ($chip_data as $key => $row)
	{
		$barcode = intval($row['barcode_ion_torrent']) > 9 ? 'IonXpress_0'.$row['barcode_ion_torrent'] : 'IonXpress_00'.$row['barcode_ion_torrent'];

		$curr_row = $barcode.','.','.$_POST['chip'].'_'.$row['sample_name'].'_'.$row['visit_id'].'_'.$row['visits_in_pool_id'].'_'.$row['pool_chip_linker_id'].','.$_POST['chip'].'_'.$row['sample_name'].'_'.$row['visit_id'].'_'.$row['visits_in_pool_id'].'_'.$row['pool_chip_linker_id'];

		$curr_row.= ',,'.'DNA,'.'hg19,'.$target_regions_file.','.$hotspot_regions_file.',,';
		$curr_row.= '100,,,,'.OFA_WORKFLOW.',Self,,'.strval($key+1);

		array_push($csv, $curr_row);
	}

	echo json_encode($csv);

	exit();
?>
