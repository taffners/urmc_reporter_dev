<?php

	// set up db connection
	if (explode('/', $_SERVER['REQUEST_URI'])[1] === 'devs')
	{
		require_once($_SERVER['DOCUMENT_ROOT'].'/devs/urmc_reporter/config.php');
	}
	elseif (explode('/', $_SERVER['REQUEST_URI'])[1] === 'urmc_reporter')
	{
		require_once($_SERVER['DOCUMENT_ROOT'].'/urmc_reporter/config.php');
	}

	
	if (isset($_POST['gene']) && $_POST['gene'] !== '')
	{
		// find all of variants which are equal to the input gene
		$variants_to_del = $db->listAll('search-knowledge-for-gene-snvs', $_POST['gene']);	

		foreach ($variants_to_del as $key => $del_array)
		{
			$db->deleteRecordById('knowledge_base_table', 'knowledge_id', $del_array['knowledge_id']);
			
		}
		echo json_encode($del_array);
	}

	



	exit();
?>