<?php
	$_GET['page'] = 'get_user_access_info';
	// set up db connection
	if (explode('/', $_SERVER['REQUEST_URI'])[1] === 'devs')
	{
		require_once($_SERVER['DOCUMENT_ROOT'].'/devs/urmc_reporter/config.php');
	}
	elseif (explode('/', $_SERVER['REQUEST_URI'])[1] === 'urmc_reporter')
	{
		require_once($_SERVER['DOCUMENT_ROOT'].'/urmc_reporter/config.php');
	}

	#########################################################

	$search_array = array(
		'filter'	=> 	$_POST['filter'],
		'month'	=>	$_POST['month'],
		'year'	=> 	$_POST['year']
	);

	if (isset($_POST['query_filter']) && $_POST['query_filter'] === 'month_counts')
	{
		echo json_encode($db->listAll('audit-month-counts-turn-around-time',$search_array));
	}

	else if (isset($_POST['query_filter']) && $_POST['query_filter'] === 'tissue_counts')
	{
		echo json_encode($db->listAll('audit-tissue-counts-turn-around-time', $search_array));
	}

	else if (isset($_POST['query_filter']) && $_POST['query_filter'] === 'sampes_per_test')
	{
		echo json_encode($db->listAll('audit-samples-per-test', $search_array));
	}

	else if (isset($_POST['query_filter']) && $_POST['query_filter'] === 'samples_tissues_per_test')
	{
		echo json_encode($db->listAll('audit-samples-tissues-per-test', $search_array));
	}


	else if (isset($_POST['query_filter']) && $_POST['query_filter'] === 'samples_tissues_per_test')
	{
		echo json_encode($db->listAll('audit-samples-tissues-per-test', $search_array));
	}

	else if (isset($_POST['query_filter']) && $_POST['query_filter'] === 'cummulative_test_counts')
	{
		echo json_encode($db->listAll('audit-num-tests-per-months'));
	}

	else if (isset($_POST['query_filter']) && $_POST['query_filter'] === 'cummulative_sample_counts')
	{
		echo json_encode($db->listAll('audit-num-samples-per-months'));
	}

	else if (isset($_POST['query_filter']) && $_POST['query_filter'] === 'cummulative_counts_per_test')
	{
		echo json_encode($db->listAll('cumulative-audit-samples-per-test'));

	}


	else
	{
		return 'Post Does not contain query_filter';
	}

	exit();
?>