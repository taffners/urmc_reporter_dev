<?php
	$_GET['page'] = 'add_comments';
	// set up db connection
	if (explode('/', $_SERVER['REQUEST_URI'])[1] === 'devs')
	{
		require_once($_SERVER['DOCUMENT_ROOT'].'/devs/urmc_reporter/config.php');
	}
	elseif (explode('/', $_SERVER['REQUEST_URI'])[1] === 'urmc_reporter')
	{
		require_once($_SERVER['DOCUMENT_ROOT'].'/urmc_reporter/config.php');
	}

	// if strand bias id is present that means everything in the database needs to be
	// found and added to the add_array
	if (isset($_POST['strand_bias_id']) && !empty($_POST['strand_bias_id']))
	{
		$pervious_data = $db->listAll('variant-strand-bias', $_POST['strand_bias_id']);
		$add_array = $pervious_data[0];
	}
	else
	{
		$add_array = array();
	}

	$add_array['user_id'] = USER_ID;
	$add_array[$_POST['col_name']] = $_POST['col_val'];
	$add_array['observed_variant_id'] = $_POST['observed_variant_id'];

	$response = $db->addOrModifyRecord('strand_bias_table',$add_array);

	// get all data related to response id
	$sb = $db->listAll('variant-strand-bias', $response[1]);

	// // if all ref -, ref +, variant -, and variant + are all not null calculate strand bias.  Otherwise return empty value array
	$sb_calculated = $utils->strandBias($sb[0]);

	echo json_encode($sb_calculated);

	exit();
?>