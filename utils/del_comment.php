<?php
	
	// set up db connection
	if (explode('/', $_SERVER['REQUEST_URI'])[1] === 'devs')
	{
		require_once($_SERVER['DOCUMENT_ROOT'].'/devs/urmc_reporter/config.php');
	}
	elseif (explode('/', $_SERVER['REQUEST_URI'])[1] === 'urmc_reporter')
	{
		require_once($_SERVER['DOCUMENT_ROOT'].'/urmc_reporter/config.php');
	}


	$delete_array = array(
		'comment_id' 		=> $_POST['comment_id'],
		'comment_type'			=> $_POST['comment_type'],
		'comment_ref'			=> $_POST['comment_ref']
	);


	$db->deleteRecord('comment_table', $delete_array);

	exit();
?>