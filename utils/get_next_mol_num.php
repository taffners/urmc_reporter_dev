<?php
	$_GET['page'] = 'add_comments';
	// set up db connection
	if (explode('/', $_SERVER['REQUEST_URI'])[1] === 'devs')
	{
		require_once($_SERVER['DOCUMENT_ROOT'].'/devs/urmc_reporter/config.php');
	}
	elseif (explode('/', $_SERVER['REQUEST_URI'])[1] === 'urmc_reporter')
	{
		require_once($_SERVER['DOCUMENT_ROOT'].'/urmc_reporter/config.php');
	}

	// echo json_encode($db->listAll('get-next-mol-num', $_POST['year']));

	if (isset($_POST['sample_log_book_id']))
	{
		// see if sample log book or visit_table already has an order_num if so return it as the order_num to use
		// The original design requirements had the order_num linked to the test and not the sample.  Therefore every new test would get its own order_num.  Now (2020-11-24) they changed the design to have only one order number per sample.  For backwards compatibility that is why the number is stored in two places. It would be ideal to have a mol_num table however, this is a larger update than I have time for.  
		$previous_order_nums = $db->listAll('all-sample-order-nums', $_POST['sample_log_book_id']);

		if (sizeOf($previous_order_nums) === 1 && $_POST['force_next'] === 'false')
		{
	
			echo json_encode($previous_order_nums);
		}

		else if (empty($previous_order_nums) || $_POST['force_next'] === 'true')
		{
			echo json_encode($db->listAll('get-next-mol-num', $_POST['year']));
		}
		
		else
		{
			$all_order_nums = '';
			foreach ($previous_order_nums as $key => $order_nums)
			{
				if ($all_order_nums != '')
				{
					$all_order_nums .=', ';
				}
				$all_order_nums = $all_order_nums.$order_nums['order_num'];
			}

			$email_utils->emailAdminsProblemURL(__FILE__.' Error!! Line #: '.__LINE__.' Error: There is more than one Mol Number already assigned to this sample.  sample_log_book_id:'.$_POST['sample_log_book_id'].' User id: '.USER_ID.'

'.$all_order_nums);
			echo 'Error: There is more than one Mol Number already assigned to this sample. Current order numbers are: '.$all_order_nums.' Admins have been notified to fix the issue. Tests that require a mol number can not be added to this sample until this is fixed.';
		}

	}
?>