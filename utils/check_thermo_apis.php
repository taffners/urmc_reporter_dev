<?php
	$_GET['page'] = 'check_thermo_apis';
	// set up db connection
	if (explode('/', $_SERVER['REQUEST_URI'])[1] === 'devs')
	{
		require_once($_SERVER['DOCUMENT_ROOT'].'/devs/urmc_reporter/config.php');
	}
	elseif (explode('/', $_SERVER['REQUEST_URI'])[1] === 'urmc_reporter')
	{
		require_once($_SERVER['DOCUMENT_ROOT'].'/urmc_reporter/config.php');
	}

	// echo json_encode($_POST);

	if (isset($_POST['check_type']) && $_POST['check_type'] === 'api_key')
	{
		// For some reason bool will not return.  with echo true is 1 and false is empty 
		if ($API_Utils->checkAPIkeys($_POST['selected_server'], $_POST['curr_key']))
		{
			echo $_POST['selected_server'].' API Key same';
		}
		else
		{
			$curr_key = $API_Utils->getAPIkey($_POST['selected_server']);

			echo $_POST['selected_server'].' API Key is not the same. <br><br>Current Key is: '.$curr_key;
		}
	}		


	exit();
?>