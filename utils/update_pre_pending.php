<?php

	// set up db connection
	if (explode('/', $_SERVER['REQUEST_URI'])[1] === 'devs')
	{
		require_once($_SERVER['DOCUMENT_ROOT'].'/devs/urmc_reporter/config.php');
	}
	elseif (explode('/', $_SERVER['REQUEST_URI'])[1] === 'urmc_reporter')
	{
		require_once($_SERVER['DOCUMENT_ROOT'].'/urmc_reporter/config.php');
	}

	// Query is to see if visit_id and step pre_pending is in pre_step_visit table.  

	$where_array = array(
			'visit_id' 	=> 	$_POST['visit_id'],
			'step' 		=> 	'pre_pending',
			'status' 		=>	'passed',
			'user_id'		=> 	USER_ID
		);

	$pre_step_status = $db->listAll('pre-step-complete', $where_array);

	/////////////////////////////////////////////////////////////////////////////
     // If visit_id not in pre_step_visit table toggle to pending.
          // Add to db
          // return ??
	/////////////////////////////////////////////////////////////////////////////
	if (empty($pre_step_status))
	{	
		$result = $db->addOrModifyRecord('pre_step_visit', $where_array);
		echo 'on';
	}
	/////////////////////////////////////////////////////////////////////////////	
     // If visit_id in pre_step_visit table toggle to not pending.
          // Delete from db
          // return ??
	/////////////////////////////////////////////////////////////////////////////	          	
	else
	{
		// use data just found in database because things might be different than where_array
		$result = $db->deleteRecordById('pre_step_visit', 'pre_step_visit_id', $pre_step_status[0]['pre_step_visit_id']);
		echo 'off';
	}

	exit();







?>