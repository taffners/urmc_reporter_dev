<?php
	$_GET['page'] = 'get_comments';
	// set up db connection
	if (explode('/', $_SERVER['REQUEST_URI'])[1] === 'devs')
	{
		require_once($_SERVER['DOCUMENT_ROOT'].'/devs/urmc_reporter/config.php');
	}
	elseif (explode('/', $_SERVER['REQUEST_URI'])[1] === 'urmc_reporter')
	{
		require_once($_SERVER['DOCUMENT_ROOT'].'/urmc_reporter/config.php');
	}

	// variant-comments

	if (isset($_GET['comment_type']) && isset($_GET['comment_id']) && $_GET['comment_type'] === 'observed_variant')
	{
		$comment = $db->listAll('variant-comments', $_GET['comment_id']);
		print_r(json_encode($comment[0]));
	}
	exit();
?>