<?php
	$_GET['page'] = 'get_user_access_info';
	// set up db connection
	if (explode('/', $_SERVER['REQUEST_URI'])[1] === 'devs')
	{
		require_once($_SERVER['DOCUMENT_ROOT'].'/devs/urmc_reporter/config.php');
	}
	elseif (explode('/', $_SERVER['REQUEST_URI'])[1] === 'urmc_reporter')
	{
		require_once($_SERVER['DOCUMENT_ROOT'].'/urmc_reporter/config.php');
	}

	#########################################################


	if (isset($_POST['query_filter']) && $_POST['query_filter'] === 'pending-counts')
	{
		echo json_encode($db->listAll('pending-test-counts'));
	}

	else if (isset($_POST['query_filter']) && $_POST['query_filter'] === 'in-progress-test-counts')
	{
		echo json_encode($db->listAll('in-progress-test-counts'));
	} 

	else
	{
		return 'Post Does not contain query_filter';
	}

	exit();
?>