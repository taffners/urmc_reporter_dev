<?php
	$_GET['page'] = 'add_comments';
	// set up db connection
	if (explode('/', $_SERVER['REQUEST_URI'])[1] === 'devs')
	{
		require_once($_SERVER['DOCUMENT_ROOT'].'/devs/urmc_reporter/config.php');
	}
	elseif (explode('/', $_SERVER['REQUEST_URI'])[1] === 'urmc_reporter')
	{
		require_once($_SERVER['DOCUMENT_ROOT'].'/urmc_reporter/config.php');
	}

	///////////////////////////////////////////
	// request new number from excel_workbook_num_table.  Request a new one now to have the least amount of time that two users are going to check out the same number
	///////////////////////////////////////////
	
	if (isset($_POST['pool_chip_linker_id']))
	{
		$new_num_request = array('user_id' => USER_ID);
		$new_num = $db->addOrModifyRecord('excel_workbook_num_table', $new_num_request);

		if ($new_num[0])
		{
			$excel_workbook_num_id = $new_num[1];

			///////////////////////////////////////////
			// Add new number to pool_chip_linker_table
			///////////////////////////////////////////
			$pool_chip_linker_array = $db->listAll('pool-chip-linker-by-id', $_POST['pool_chip_linker_id']);
			$update_array = $pool_chip_linker_array[0];
			$update_array['excel_workbook_num_id'] = $excel_workbook_num_id;

			$update_result = $db->addOrModifyRecord('pool_chip_linker_table', $update_array);
			
			
			///////////////////////////////////////////
			// Download blank workbook and Change name to include excel_workbook_num
			///////////////////////////////////////////

			$file = 'downloads/Run_OFA_Library_Prep.xlsm';
			header('Content-Disposition: attachment; filename="'.$excel_workbook_num_id.'_Run_OFA_Library_Prep.xlsm"');

			header('Content-type: application/vnd.ms-excel.sheet.macroEnabled.12');
			
			header('Content-Length: ' . filesize($file));

			header('Content-Transfer-Encoding: binary');
			
			header('Cache-Control: must-revalidate');
			
			header('Pragma: public');
			ob_clean();
			flush(); 

			// read the original file.
			readfile($file);

			echo 'it worked';
		}
		else
		{
			echo 'it failed';
		}

	}

	
	exit();
?>