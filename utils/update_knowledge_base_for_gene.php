<?php

	// set up db connection
	if (explode('/', $_SERVER['REQUEST_URI'])[1] === 'devs')
	{
		require_once($_SERVER['DOCUMENT_ROOT'].'/devs/urmc_reporter/config.php');
	}
	elseif (explode('/', $_SERVER['REQUEST_URI'])[1] === 'urmc_reporter')
	{
		require_once($_SERVER['DOCUMENT_ROOT'].'/urmc_reporter/config.php');
	}


	ini_set('max_execution_time', '600');
	//////////////////////////////////////////////////////////////////////////
	// Find cosmic file otherwise throw error
	//////////////////////////////////////////////////////////////////////////
	$cosmic_tsv = '/var/www/html/cosmic_tsvs/cosmic_per_gene/'.$_POST['gene'].'.tsv';
	if (file_exists($cosmic_tsv))
	{
		//////////////////////////////////////////////////////////////////////////
		// read thru cosmic tsv file and find all variants which equal $_POST['gene'] if none found throw error
		//////////////////////////////////////////////////////////////////////////
		$file_reader_array = array(
			'db' 		=> 	$db,
			'file'   		=>	$cosmic_tsv,
			'type'		=> 	'update_cosmic_knowledge_base',
			'gene_name'	=> 	$_POST['gene']
		);


		$genesFileReader = new FileReader($file_reader_array);
		echo json_encode($genesFileReader->updateCosmicKnowledgeBase());

		$genesFileReader->close_file();
	}

	else
	{
		// send error
		$err_array = array('err'	=> 	'File Missing: /var/www/html/cosmic_tsvs/CosmicMutantExport.tsv');
		echo json_encode($err_array);
	}

	exit();
?>