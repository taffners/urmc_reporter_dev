<?php
	$_GET['page'] = 'Get Myeloid Run Template';
	// set up db connection
	if (explode('/', $_SERVER['REQUEST_URI'])[1] === 'devs')
	{
		require_once($_SERVER['DOCUMENT_ROOT'].'/devs/urmc_reporter/config.php');
	}
	elseif (explode('/', $_SERVER['REQUEST_URI'])[1] === 'urmc_reporter')
	{
		require_once($_SERVER['DOCUMENT_ROOT'].'/urmc_reporter/config.php');
	}

	// find if existing flow cell and reagent cartridge for run_template
	$library_pool = $db->listAll('myeloid-library-pool', $_POST);
	$update_pool = $library_pool[0];
	$update_pool['user_id'] = USER_ID;

	if (empty($library_pool[0]['chip_flow_cell']) || $library_pool[0]['chip_flow_cell'] !== $_POST['chip_flow_cell'])
	{
		$update_pool['chip_flow_cell'] = $_POST['chip_flow_cell'];
	}

	if (empty($library_pool[0]['reagent_cartidge']) || $library_pool[0]['reagent_cartidge'] !== $_POST['reagent_cartidge'])
	{
		$update_pool['reagent_cartidge'] = $_POST['reagent_cartidge'];
	}

	$update_result = $db->addOrModifyRecord('library_pool_table', $update_pool);

	// get data from database for run template.  This will only return data related 
	// to the chip currently downloading.

	$chip_data = $db->listAll('myeloid-run-template-by-chip', $_POST);
	
	$user_initials = $db->listAll('user-initials', USER_ID);

	$indexes_by_sequence = array(
		'A701'	=> 'ATCACGAC',
		'A702'	=> 'ACAGTGGT',
		'A703'	=> 'CAGATCCA',
		'A704'	=> 'ACAAACGG',
		'A705'	=> 'ACCCAGCA',
		'A706'	=> 'AACCCCTC',
		'A707'	=> 'CCCAACCT',
		'A708'	=> 'CACCACAC',
		'A709'	=> 'GAAACCCA',
		'A710'	=> 'TGTGACCA',
		'A711'	=> 'AGGGTCAA',
		'A712'	=> 'AGGAGTGG',
		
		'A501'	=> 'TGAACCTT',
		'A502'	=> 'TGCTAAGT',
		'A503'	=> 'TGTTCTCT',
		'A504'	=> 'TAAGACAC',
		'A505'	=> 'CTAATCGA',
		'A506'	=> 'CTAGAACA',
		'A507'	=> 'TAAGTTCC',
		'A508'	=> 'TAGACCTA'

	);

	// array to save all csv output data
	$csv_data = array(
		'reagent_cartidge' 		=> $_POST['reagent_cartidge'],
		'pool_chip_linker_id'	=> $_POST['pool_chip_linker_id'],
		'[Header],',
		'Local Run Manager Analysis Id,4005',
		'Experiment Name,'.date("ymd").'_Myeloid2',
		'Date,'.date("Y/m/d"),
		'Module,DNA Amplicon - 2.1.0',
		'Workflow,DNA Amplicon',
		'Library Prep Kit,TruSeq Amplicon',
		'Chemistry,Amplicon',
		'',
		'[Manifests],',
		'A,TruSight-Myeloid-Manifest.txt',
		'',
		'[Reads],',
		'301,',
		'301,',
		'',
		'[Settings],',
		'VariantCallerUserScylla,0',
		'VariantCallPhasedSNP,1',
		'VariantCaller,PiscesSomatic',
		'VariantFrequencyFilterCutoff,0.0005',
		'VariantFrequencyEmitCutoff,0.00025',
		'VariantFilterQualityCutoff,30',
		'transcriptsource,RefSeq',
		'variantannotation,Nirvana',
		'minimumcoveragedepth,200',
		'Aligner,Amplicon',
		'VariantCallerRealignIndels,0',
		'StitchReads,1',
		'outputgenomevcf,True',
		'',
		'[Data],',
		'Sample_ID,Sample_Name,Description,index,I7_Index_ID,index2,I5_Index_ID,Sample_Project,Manifest,GenomeFolder'
	);

	// iterate of the data for this chip and add it to the csv output array.  Each 
	// iteration is a different sample.

	// New
		// Sample_ID,Sample_Name,Description,index,I7_Index_ID,index2,I5_Index_ID,Sample_Project,Manifest,GenomeFolder
	// Old
		// Sample_ID,Sample_Name,Sample_Plate,Sample_Well,I7_Index_ID,index,I5_Index_ID,index2,Manifest,GenomeFolder,Sample_Project,Description


	foreach ($chip_data as $key => $row)
	{
		/////////////////////////////////////////////////////////////////
		// Get indexes for i7 and i5
		/////////////////////////////////////////////////////////////////
		// get I7 name and index
		if (intval($row['i7_index_miseq']) > 9)
		{
			$I7_Index_ID = 'A7'.$row['i7_index_miseq'];
		}
		else
		{
			$I7_Index_ID = 'A70'.$row['i7_index_miseq'];
		}

		$i7_index = $indexes_by_sequence[$I7_Index_ID];

		// get I5 name and index
		$I5_Index_ID = 'A50'.$row['i5_index_miseq'];		
		$i5_index = $indexes_by_sequence[$I5_Index_ID];

		$curr_row = $row['sample_name'].','.$_POST['chip_flow_cell'].'_'.$I7_Index_ID.',';
		$curr_row.= ','.$i7_index.','.$I7_Index_ID.','.$i5_index.','.$I5_Index_ID.',,';
		$curr_row.= 'A,Homo_sapiens\UCSC\hg19\Sequence\WholeGenomeFasta';

		array_push($csv_data, $curr_row);
	}

	echo json_encode($csv_data);

	// exit();
?>
