<?php
	
	// set up db connection
	if (explode('/', $_SERVER['REQUEST_URI'])[1] === 'devs')
	{
		require_once($_SERVER['DOCUMENT_ROOT'].'/devs/urmc_reporter/config.php');
	}
	elseif (explode('/', $_SERVER['REQUEST_URI'])[1] === 'urmc_reporter')
	{
		require_once($_SERVER['DOCUMENT_ROOT'].'/urmc_reporter/config.php');
	}

	if (isset($_POST['run_id']) && isset($_POST['val']))
	{
		$runInfoArray = $db->listAll('run-info', $_POST['run_id']);

		if (!empty($runInfoArray))
		{
			$update_array = $runInfoArray[0];
			$update_array['transferred_to_patient_chart'] = $_POST['val'];	
			$result = $db->addOrModifyRecord('run_info_table', $update_array);

			$user_update_task_array = array();
			$user_update_task_array['ref_id'] = $result[1];
			$user_update_task_array['user_id'] = USER_ID;
			$user_update_task_array['task'] = 'scanned_ngs_report';
			$user_update_task_array['ref'] = 'run_info_table';
			$user_update_task_array['before_val'] = $_POST['val'] === 'yes' ? 'pending' : 'yes';

			$user_update_task_array['after_val'] = $_POST['val'];

			$result = $db->addOrModifyRecord('user_update_task_table', $user_update_task_array);
			echo 'done';
		}
		else
		{
			echo 'error run not found';
		}
	}
	else
	{
		echo 'error no info';
	}
	


	exit();
?>