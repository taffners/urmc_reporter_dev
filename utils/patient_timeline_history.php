<?php
	$_GET['page'] = 'patient_timeline_history';
	// set up db connection

	if (explode('/', $_SERVER['REQUEST_URI'])[1] === 'devs')
	{
		require_once($_SERVER['DOCUMENT_ROOT'].'/devs/urmc_reporter/config.php');
	}
	elseif (explode('/', $_SERVER['REQUEST_URI'])[1] === 'urmc_reporter')
	{
		require_once($_SERVER['DOCUMENT_ROOT'].'/urmc_reporter/config.php');
	}

	///////////////////////////////////////////////////////////////////////
	// Make sure sample has more than one visit and sample is not a control.
	///////////////////////////////////////////////////////////////////////

	$num_visits_patient = $db->listAll('patient-visit-history', $_POST['patient_id']);

	$patient_history = $db->listAll('patient-timeline-history', $_POST['patient_id']);

	if 	(
			empty($patient_history) || 
			empty($num_visits_patient) || 
			(
				!empty($num_visits_patient) &&
				isset($num_visits_patient[0]['visit_count']) &&
				$num_visits_patient[0]['visit_count'] == 1
			)
		)
	{
		echo 'skip';
	}

	else
	{
		echo json_encode($patient_history);
	}
	

	exit();
?>