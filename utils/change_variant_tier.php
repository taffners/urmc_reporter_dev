<?php
	// set up db connection
	if (explode('/', $_SERVER['REQUEST_URI'])[1] === 'devs')
	{
		require_once($_SERVER['DOCUMENT_ROOT'].'/devs/urmc_reporter/config.php');
	}
	elseif (explode('/', $_SERVER['REQUEST_URI'])[1] === 'urmc_reporter')
	{
		require_once($_SERVER['DOCUMENT_ROOT'].'/urmc_reporter/config.php');
	}

	///////////////////////////////////////////////////////////////////////////////
	// Remove all previous tier information associated with this variant
	///////////////////////////////////////////////////////////////////////////////
	$variant_tiers = $db->listAll('query-variant-tier-xref-by-observed-variant-id', $_POST['observed_variant_id']);

	if (sizeof($variant_tiers) > 0)
	{
		foreach ($variant_tiers as $row)
		{
			$db->deleteRecordById('variant_tier_xref','vt_xref_id', $row['vt_xref_id']);
		}
	}

	///////////////////////////////////////////////////////////////////////////////
	// update tier name with observed variant row
	///////////////////////////////////////////////////////////////////////////////

	# find previous information in the database for observed_variant_table
	$observed_record = $db->listAll('observed-variant-by-id', $_POST['observed_variant_id']);

	# edit the column to include_in_report
	$observed_record[0]['tier'] = $_POST['tier'];

	$role_response = $db->addOrModifyRecord('observed_variant_table',$observed_record[0]);

	///////////////////////////////////////////////////////////////////////////////
	// Add this variant to variant_tier_xref
	// tier_id, knowledge_id, tissue_id, user_id, observed_variant_id
	///////////////////////////////////////////////////////////////////////////////


	$addTierArray = array(	
			'tier_id'				=> 	$_POST['tier_id'],
			'knowledge_id'			=> 	$_POST['knowledge_id'],
			'tissue_id'			=> 	$_POST['tissue_id'] != '' ? $_POST['tissue_id'] : 0,
			'user_id'				=> 	$_POST['user_id'],
			'observed_variant_id'	=> 	$_POST['observed_variant_id'],
			'report_link'			=> 	$_POST['report_link']	
		);
var_dump($addTierArray);	
	$tier_response = $db->addOrModifyRecord('variant_tier_xref',$addTierArray);

?>