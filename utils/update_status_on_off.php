<?php
	
	// set up db connection
	if (explode('/', $_SERVER['REQUEST_URI'])[1] === 'devs')
	{
		require_once($_SERVER['DOCUMENT_ROOT'].'/devs/urmc_reporter/config.php');
	}
	elseif (explode('/', $_SERVER['REQUEST_URI'])[1] === 'urmc_reporter')
	{
		require_once($_SERVER['DOCUMENT_ROOT'].'/urmc_reporter/config.php');
	}

	// for permissions already set remove 
	$search_array = array(
		'table' => $_POST['table'],
		'id' => $_POST['id']
	);

	$update_record = $db->listAll('find-data-by-id-and-table', $search_array);


	if (!empty($update_record) && isset($update_record[0]) && isset($_POST['curr_status']) && $_POST['curr_status'] === 'on')
	{
		$update_record[0]['status'] = 'off';
		$update_result = $db->addOrModifyRecord('possible_sample_types_table', $update_record[0]);
	}


	if (!empty($update_record) && isset($update_record[0]) && isset($_POST['curr_status']) && $_POST['curr_status'] === 'off')
	{
		$update_record[0]['status'] = 'on';
		$update_result = $db->addOrModifyRecord('possible_sample_types_table', $update_record[0]);
	}

	exit();
?>