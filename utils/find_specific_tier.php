<?php
	
	// set up db connection
	if (explode('/', $_SERVER['REQUEST_URI'])[1] === 'devs')
	{
		require_once($_SERVER['DOCUMENT_ROOT'].'/devs/urmc_reporter/config.php');
	}
	elseif (explode('/', $_SERVER['REQUEST_URI'])[1] === 'urmc_reporter')
	{
		require_once($_SERVER['DOCUMENT_ROOT'].'/urmc_reporter/config.php');
	}

	// Query database to see if the tier is already present for this variant in the variant_tier_xref table

	$xref_found = $db->listAll('find-variant-tier-xref', $_GET);

	// Make sure the tissue is not already in xref for this variant if tissue is not equal to ''
	if ($_GET['tissue_id'] !== '')
	{
		$tissue_found = $db->listAll('find-variant-tissue-xref', $_GET);
		if (empty($tissue_found))
		{
			$tissue_found = 'passed';
		}
		else
		{
			$tissue_found = 'failed';
		}
	}
	else
	{
		$tissue_found = 'passed';
	}

	if (empty($xref_found) && $tissue_found === 'passed')
	{
		print_r('not_found');
	}
	elseif ($tissue_found === 'failed') 
	{
		rint_r('OOPS: Only one tier allowed per tissue!');
	}
	else
	{
		print_r('OOPS: This tier was already added for this variant!');
	}

	exit();
?>