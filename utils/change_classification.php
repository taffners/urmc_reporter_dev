<?php
	
	// set up db connection
	if (explode('/', $_SERVER['REQUEST_URI'])[1] === 'devs')
	{
		require_once($_SERVER['DOCUMENT_ROOT'].'/devs/urmc_reporter/config.php');
	}
	elseif (explode('/', $_SERVER['REQUEST_URI'])[1] === 'urmc_reporter')
	{
		require_once($_SERVER['DOCUMENT_ROOT'].'/urmc_reporter/config.php');
	}

	////////////////////////////////////////////////////////////////
	// Change or add Classification for variant based on gene, coding, protein, panel
	////////////////////////////////////////////////////////////////

	if(isset($_POST) && isset($_POST['classification']) && isset($_POST['genes']) && isset($_POST['coding']) && isset($_POST['panel']))
	{
		$classify_array = array(
               'classification'	=> 	$_POST['classification'],
               'genes'			=> 	$_POST['genes'],
               'coding'			=> 	$_POST['coding'],
               'amino_acid_change'	=> 	$_POST['amino_acid_change'],
               'panel'			=> 	$_POST['panel'],
               'user_id'			=> 	USER_ID
		);

		$previous_classification = $db->listAll('classification-variant', $classify_array);

		// Since any user can add a classification this will just add new classifications and not modify the already existing classification.  It will also take note which user added the classification and when it occurred. 
		$classify_result = $db->addOrModifyRecord('classification_table',$classify_array);
	}



	exit();
?>

