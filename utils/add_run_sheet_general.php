<?php
	$_GET['page'] = 'make_myeloid_run_sheet';
	// set up db connection
	if (explode('/', $_SERVER['REQUEST_URI'])[1] === 'devs')
	{
		require_once($_SERVER['DOCUMENT_ROOT'].'/devs/urmc_reporter/config.php');
	}
	elseif (explode('/', $_SERVER['REQUEST_URI'])[1] === 'urmc_reporter')
	{
		require_once($_SERVER['DOCUMENT_ROOT'].'/urmc_reporter/config.php');
	}

	// check if ngs_run_date and sheet panel_type already added.  If so notify user by returning already added under error
	$search_array = array(
		'sheet_panel_type' 	=> 	$_POST['sheet_panel_type'],
		'ngs_run_date' 	=> 	$_POST['ngs_run_date'],
		'version'			=> 	$_POST['version']
	);


	$search_result =$db->listAll('run-sheet-general-info', $search_array);

	// if not already added add and return result id
	if (empty($search_result))
	{
		$add_array = $_POST;
		$add_array['user_id'] = USER_ID;

		$response = $db->addOrModifyRecord('run_sheet_general_table',$add_array);
		$return_array = array('err' => '', 'run_sheet_general_id' =>  $response[1]);
		echo json_encode($return_array);
	}
	else
	{
		$return_array = array('err' => 'already added');
		echo json_encode($return_array);
	}

	exit();
?>