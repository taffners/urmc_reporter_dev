<?php    
     // set up db connection
     if (explode('/', $_SERVER['REQUEST_URI'])[1] === 'devs')
     {
          require_once($_SERVER['DOCUMENT_ROOT'].'/devs/urmc_reporter/config.php');
     }
     elseif (explode('/', $_SERVER['REQUEST_URI'])[1] === 'urmc_reporter')
     {
          require_once($_SERVER['DOCUMENT_ROOT'].'/urmc_reporter/config.php');
     }

     if(isset($_GET) && $_GET['vt_xref_id'])
     {

          $tier_result = $db->deleteRecordById('variant_tier_xref','vt_xref_id', $_GET['vt_xref_id']);

          print_r(json_encode($tier_result));
     }

     exit();
?>