<?php
	// set up db connection
	if (explode('/', $_SERVER['REQUEST_URI'])[1] === 'devs')
	{
		require_once($_SERVER['DOCUMENT_ROOT'].'/devs/urmc_reporter/config.php');
	}
	elseif (explode('/', $_SERVER['REQUEST_URI'])[1] === 'urmc_reporter')
	{
		require_once($_SERVER['DOCUMENT_ROOT'].'/urmc_reporter/config.php');
	}

	// Query db to get visit info
	$visitInfo = $db->listAll('ajax-visit-info', $_POST['visit_id']);

	$visitInfoNew = $visitInfo[0];

	$visitInfoNew['patient_id'] = $_POST['patient_id'];

	$result = $db->addOrModifyRecord('visit_table', $visitInfoNew);	
	
	// find completed steps depending on if a pre step or not
	if (isset($_POST['pre_step']) && isset($_POST['visit_id']) && $_POST['pre_step'] == 1 )
	{
		$completed_steps = $db->listAll('pre-steps-visit', $_POST['visit_id']);

		// add step completed to step_run_xref if not already completed
		$url = $preStepTracker->UpdatePreStepInDb($page, $completed_steps, $_POST['visit_id'], USER_ID, 0, 'add_patient', 'passed'); 

		echo '?page=home';		
	}
	else if (isset($_POST['pre_step']) && isset($_POST['visit_id']) && $_POST['pre_step'] == 0 )
	{
		$completed_steps = $db->listAll('steps-run', $_POST['run_id']);

		// add step completed to step_run_xref if not already completed
		$redirect_url = $stepTracker->UpdateStepInDb($db, 'add_patient', $completed_steps, $_POST['run_id'], USER_ID);

		echo $redirect_url;
	}
	exit();
?>
