<?php
	$_GET['page'] = '';
	// set up db connection
	if (explode('/', $_SERVER['REQUEST_URI'])[1] === 'devs')
	{
		require_once($_SERVER['DOCUMENT_ROOT'].'/devs/urmc_reporter/config.php');
	}
	elseif (explode('/', $_SERVER['REQUEST_URI'])[1] === 'urmc_reporter')
	{
		require_once($_SERVER['DOCUMENT_ROOT'].'/urmc_reporter/config.php');
	}

	$search_array = array(
		'ngs_panel_id'		=> 	$_POST['ngs_panel_id'],
		'control_type_id'	=> 	$_POST['control_type_id']
	);

	$all_controls = $db->listAll('control-last-version-for-ngs-panel',$search_array);

	echo json_encode($all_controls);

	exit();
?>