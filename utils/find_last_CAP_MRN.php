<?php
	$_GET['page'] = 'ajax call';
	// set up db connection
	if (explode('/', $_SERVER['REQUEST_URI'])[1] === 'devs')
	{
		require_once($_SERVER['DOCUMENT_ROOT'].'/devs/urmc_reporter/config.php');
	}
	elseif (explode('/', $_SERVER['REQUEST_URI'])[1] === 'urmc_reporter')
	{
		require_once($_SERVER['DOCUMENT_ROOT'].'/urmc_reporter/config.php');
	}

	$cap_mrns = $db->listAll('CAP-medical-record-nums');

	if (isset($cap_mrns[0]['medical_record_num']))
	{
		echo $cap_mrns[0]['medical_record_num'];
	}

	else
	{
		echo '';
	}

	exit();
?>