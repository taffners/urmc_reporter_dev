<?php
	
	// set up db connection
	if (explode('/', $_SERVER['REQUEST_URI'])[1] === 'devs')
	{
		require_once($_SERVER['DOCUMENT_ROOT'].'/devs/urmc_reporter/config.php');
	}
	elseif (explode('/', $_SERVER['REQUEST_URI'])[1] === 'urmc_reporter')
	{
		require_once($_SERVER['DOCUMENT_ROOT'].'/urmc_reporter/config.php');
	}

	// get info in the confirmed_count_vw pertaining to a specific gene and ngs_panel

	$query_vals = array(
			'ngs_panel_id' 	=> $_GET['ngs_panel_id'],
			'genes'			=> $_GET['genes'],
			'snp_or_indel'		=> $_GET['snp_or_indel']
		);


	print_r(json_encode($db->listAll('get-confirm-info-for-gene', $query_vals)));

	exit();
?>