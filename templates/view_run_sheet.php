<div class="container">
	<div class="row">
<?php
	if (isset($run_sheet_general))
	{
?>	
		Library Prep Date: <?= $run_sheet_general[0]['library_prep_date'];?>
		<br>
		run date Date: <?= $run_sheet_general[0]['ngs_run_date'];?>
		<br>
		panel type: <?= $run_sheet_general[0]['sheet_panel_type'];?>
		<br>
		version: <?= $run_sheet_general[0]['version'];?>
<?php
	}
?>	
	</div>

	<div class="row">
<?php
	if (isset($run_sheet_data))
	{
?>	
	<table class="formated_table sort_table dataTable">
		<thead>	                    
               <th>MD# / soft lab#</th>                        
          	<th>Patient Name</th>
          	<th>Qubit</th>
          	<th>Dil total vol</th>
          	<th>DNA vol</th>
          	<th>AE vol</th>
          	<th>Library Tube</th>
          	<th>Index 1 i7</th>
          	<th>Index 2 i5</th>
		</thead>
		<tbody>
<?php

	foreach($run_sheet_data as $key => $row)
	{		
?>	
			<tr>
				<td><?= $row['sample_ids'];?></td>
				<td><?= $row['patient_name'];?></td>
				<td><?= $row['qubit'];?></td>
				<td><?= $row['dil_total_vol'];?></td>
				<td><?= $row['dna_vol'];?></td>
				<td><?= $row['ae_vol'];?></td>
				<td><?= $row['library_tube'];?></td>
				<td><?= $row['index_i7'];?></td>
				<td><?= $row['index_i5'];?></td>
			</tr>

<?php
	}
?>		

		</tbody>
	</table>

<?php
	}
?>
	</div>
</div>


