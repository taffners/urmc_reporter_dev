<div id="tier_modal" class="modal fade" role="dialog">
     <div class="modal-dialog modal-md">

          <!-- Modal content-->
          <div class="modal-content">
               <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 id="tier-modal-title">Modal Header</h4>
               </div>
               <div class="modal-body">
                    <div class="row">
                         <div id="tier-modal-body" class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                              <div class="row" id="tier-tissue-delete-table">
                                   <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                        <div class="row">
                                             <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                  <div class="alert alert-danger">
                                                       <h4>Click on the rows below to delete them</h4>
                                                  </div>
                                             </div>
                                        </div>
                                        <table id="tier-tissue-del-table" class="formated_table">

                                             <thead>
                                                  <th class="hide">vt_xref_id</th>
                                                  <th>Tier</th>
                                                  <th>Tier Summary</th>
                                                  <th>Tissue</th>                    
                                             </thead>

                                             <tbody id="tier-tissue-delete-table-body">

                                             </tbody>
                                        </table>
                                        <div id="confirm_del_tier" class="hide">
                                             <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 alert alert-danger">
                                                  <h4 id="confirm_del_tier_msg"></h4>
                                                  <div id="confirm_del_tier_buttons" class="row">
                                                       <div class="col-xs-10 col-sm-10 col-md-10 col-lg-10">
                                                            <button type="button" id="confirm_del_tier_buttons_yes" class="btn btn-danger">
                                                                 Yes
                                                            </button>
                                                       </div>
                                                       <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
                                                            <button type="button" id="confirm_del_tier_buttons_no" class="btn btn-success">
                                                                 No
                                                            </button>
                                                       </div>
                                                  </div>
                                             </div>
                                        </div>
                                   </div>
                              </div>
                              <div class="row" id="tier-tissue-add-table">
                                   <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                        <div class="container-fluid" style="margin-top:10px;">
                                             <div class="row">
                                                  <form id="create_variant_tier" class="form" method="post" class="form-horizontal">
                                                       <fieldset id="tier-tissue-add-table-fieldset">
                                                            <legend id='tier-tissue-add-table-legend'>
                                                                 Add a new tier to this variant
                                                            </legend>
                                                            <div class="form-group row">

                                                                 <div id="tier-tissue-add-table-msg-center" class="alert alert-danger hide">
                                                                      
                                                                 </div>

                                                            </div>
                                                            <div class="form-group row">
                                                                 <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                                      <label for="tier">Tier<span class="required-field">*</span></label>
                                                                 </div>
                                                                 <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                                      <select class="form-control" id="tier-tissue-add-table-tier-select" name="tier" data-knowledge_id="">
                                                                      </select>
                                                                 </div>
                                                                 <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                                      <label for="tier">Tissue<span class="required-field">*</span></label>
                                                                 </div>

                                                                 
                                                                 <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                                      <select class="form-control" id="tier-tissue-add-table-tissue-select" name="tissue" data-knowledge_id="">
                                                                      </select>
                                                                 </div>
                                                                 <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="margin-top:10px;">
                                                                      <button type="button" id="submit-new-tier" class="btn btn-primary">
                                                                           submit
                                                                      </button>
                                                                 </div>
                                                            </div>
                                                       </fieldset>
                                                  </form>
                                             </div>
                                        </div>
                                   </div>
                              </div>
                         </div>
                    </div>
                    <div class="modal-footer">
                         <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
               </div>
          </div>

     </div>
</div>