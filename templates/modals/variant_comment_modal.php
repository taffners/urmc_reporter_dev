<div id="variant_comment_modal" class="modal fade" role="dialog">
     <div class="modal-dialog modal-lg">

          <!-- Modal content-->
          <div class="modal-content">
               <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 id="variant-comment-modal-title">Variant Name</h4>
               </div>
               <div class="modal-body">
                    <div class="row">
                         <div id="variant-comment-modal-body" class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                              <div class="row" id="variant-comment-table">
                                   <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                                        <table id="variant-comment-table" class="formated_table">

                                             <thead>
                                                  <th>Comment</th>
                                                  <th>User Name</th>
                                                  <th>Time</th>                   
                                             </thead>

                                             <tbody id="variant-comment-table-body">
                                                  <tr>
                                                       <td colspan="3">No comments made</td>
                                                  </tr>
                                             </tbody>
                                        </table>
                                   </div>
                              </div>                             
                              <div class="row" style="margin-top:20px;">
                                   <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                        <div id="add-variant-msg-center" class="alert alert-danger hide">
                                             <h4>Please enter a comment before submitting.</h4>
                                        </div>
                                        <div class="form-group">
                                       
                                             <textarea id="variant-comment-modal-textarea" class="form-control" name="comment" rows="5" maxlength="65535" placeholder="Add a new comment here ......"></textarea>
                                             
                                             <!-- add a button to submit new comment -->
                                             <button id="submit_variant_comment" type="button" class="btn btn-primary btn-primary-hover submit-variant-comment" style="margin-top:10px;">
                                                  Submit
                                             </button>
                                        </div>

                                   </div>
                              </div>                              
                         </div>
                    </div>
                    <div class="modal-footer">
                         <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
               </div>
          </div>

     </div>
</div>