<!-- this is the summary for the report -->

<?php
	require_once('templates/lists/run_info_list.php');
?>

<div class="container-fluid">
	<div class="row">
		<?php
			require_once('templates/shared_layouts/report_progress_bar.php');
		?>
		<!-- update area -->
		<div id="toggle-report-progress-bar" class="col-xs-10 col-sm-10 col-md-10 col-lg-10">
			
			<fieldset>
				<legend>
					<?= $utils->UnderscoreCaseToHumanReadable($page);?>
				</legend>

				<div class="alert alert-danger" style="font-size:20px;">
					<strong>This will remove patient report from the pending and completed list.  Use this option if the report will no longer be reported out.  For example if the patient is now deceased.</strong> 
				</div>

				<form id="add_visit_info" class="form" method="post" class="form-horizontal">		
					<div class="form-group row">
						<div class="col-xs-12 col-sm-4 col-md-4">
							<label for="status" class="form-control-label">Reason to Stop reporting:</label>
						</div>
						<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
							<input type="text" class="form-control" name="status" maxlength="30" list="status"/>
							<datalist id="status">
<?php
						if (isset($stopReportingOptions) && !empty($stopReportingOptions))
						{
							foreach ($stopReportingOptions as $key => $option)
							{
							
?>
								<option><?= $option['status'];?></option>
<?php
							}
						}	
?>								

							</datalist>
						</div>
					</div>
					<?php
						// add the common hidden fields for every form
						require_once('templates/shared_layouts/form_hidden_fields.php');
					?>					
<?php					
					// Add submit and navigate buttons for reporting.
						if (isset($_GET) && isset($_GET['run_id']) && isset($_GET['visit_id']))
						{	
							require_once('templates/shared_layouts/form_submit_nav_buttons.php');
						}
?>					
				</form>







				<?php
					if (isset($patientArray)  && isset($visitArray))
					{
						require_once('templates/tables/patient_visit_report_table.php');
						require_once('templates/shared_layouts/panel_info_report.php');	
						require_once('templates/shared_layouts/sample_information.php');
					}

					// MUTANTS
					require_once('templates/tables/mutant_tables.php');

					// Low Coverage 					
					require_once('templates/tables/low_cov_tables.php');

					// INTERPRETATION

					if (isset($num_snvs)  && $num_snvs > 0)
					{						
						require_once('templates/shared_layouts/interpretation_summary_report.php');
					}				

					// confirmation
					require_once('templates/shared_layouts/confirmation_info.php');

					// Wet Bench tech info
					if (isset($wet_bench_tech) && !empty($wet_bench_tech))
					{
						require_once('templates/shared_layouts/wet_bench_techs.php');
					}
				?>

			</fieldset>

		</div>
	</div>
</div>