
<div class="container-fluid">

	<?php
		if (isset($_GET) && isset($_GET['run_id']))
		{		
			require_once('templates/lists/run_info_list.php');
		}
		else if (isset($visitArray))
		{
			require_once('templates/tables/patient_visit_report_table.php');
		}		
	?>

	<div class="row">
		<?php
			if (isset($_GET) && isset($_GET['run_id']))
			{
				require_once('templates/shared_layouts/report_progress_bar.php');
		?>
		<div id="toggle-report-progress-bar" class="col-xs-12 col-sm-10 col-md-10 col-lg-10" style="height:500px;">
		<?php
			}
			else
			{
		?>
		<div id="toggle-report-progress-bar" class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="height:500px;">
		<?php
			}
		?>
			<form id="add_patient_info" class="form" method="post" class="form-horizontal">
				<fieldset>
					<legend id="new_legend">
						Add Patient Info
					</legend>
					<legend id="update_legend" class="d-none">
						Update Patient Info
					</legend>

					<div class="form-group row">
						<div class="col-xs-12 col-sm-4 col-md-4">
							<label for="first_name" class="form-control-label">First Name: <span class="required-field">*</span></label>
						</div>
						<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
							<input type="text" class="form-control capitalize" id="first_name" name="first_name" maxlength="50" <?php 
							if(isset($patientArray[0]['first_name']))
							{
								echo 'value="'.$patientArray[0]['first_name'].'"';
								echo ' readonly="readonly" ';
							}
						?> required/>
						</div>
					</div>
					<div class="form-group row">
						<div class="col-xs-12 col-sm-4 col-md-4">
							<label for="middle_name" class="form-control-label">Middle Name: </label>
						</div>
						<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
							<input type="text" class="form-control capitalize" id="middle_name" name="middle_name" maxlength="50" <?php 
							if(isset($patientArray[0]['middle_name']))
							{
								echo 'value="'.$patientArray[0]['middle_name'].'"';
								echo ' readonly="readonly" ';
							}
						?>/>
						</div>
					</div>
					<div class="form-group row">
						<div class="col-xs-12 col-sm-4 col-md-4">
							<label for="last_name" class="form-control-label capitalize">Last Name: <span class="required-field">*</span></label>
						</div>
						<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
							<input type="text" class="form-control" id="last_name" name="last_name" maxlength="50" <?php 
							if(isset($patientArray[0]['last_name']))
							{;
								echo 'value="'.$patientArray[0]['last_name'].'"';
								echo ' readonly="readonly" ';
							}
						?> required/>
						</div>
					</div>
					<div class="form-group row">
						<div class="col-xs-12 col-sm-4 col-md-4">
							<label for="dob" class="form-control-label">Date of Birth: <span class="required-field">*</span></label>
						</div>
						<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
							<input class="form-control <?php 
							if(!isset($patientArray[0]['dob']))
							{
								echo 'date-picker" type="date"';
							}

							?> id="dob" name="dob" <?php
							if(isset($patientArray[0]['dob']))
							{
								echo 'value="'.$dfs->ChangeDateFormatUS($patientArray[0]['dob'], FALSE).'"'; 
								echo ' readonly="readonly" type="text"';
							}
							?> placeholder="MM/DD/YYYY" pattern="\d{1,2}/\d{1,2}/\d{4}" />
						</div>
					</div>
					<div class="form-group row">
						<div class="col-xs-12 col-sm-4 col-md-4">
							<label for="sex" class="form-control-label">Sex: <span class="required-field">*</span></label>
						</div>
						<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
							<div class="radio">
								<label class="radio-inline"><input type="radio" name="sex" required="required" value="M" <?php
								if(isset($patientArray[0]['sex']) && $patientArray[0]['sex'] == 'M')
								{
									echo 'checked'; 								
								}

								if(isset($patientArray[0]['sex']))
								{
									echo ' onclick="return false;"';
								}
								?>>Male</label>
							
								<label class="radio-inline"><input type="radio" name="sex" value="F" <?php
								if(isset($patientArray[0]['sex']) && $patientArray[0]['sex'] == 'F')
								{
									echo 'checked'; 
								}

								if(isset($patientArray[0]['sex']))
								{
									echo ' onclick="return false;"';
								}
								?>>Female</label>
							</div>
						</div>
					</div>
					<div class="form-group row">
						<div class="col-xs-12 col-sm-4 col-md-4">
							<label for="medical_record_num" class="form-control-label">Medical Record #: <span class="required-field">*</span></label>
						</div>
						<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
							<input type="text" class="form-control" id="medical_record_num" name="medical_record_num" maxlength="50" <?php 
							if(isset($patientArray[0]['medical_record_num']))
							{
								echo 'value="'.$patientArray[0]['medical_record_num'].'"';
								echo ' readonly="readonly" ';
							}
						?> required/>
						</div>
					</div>
       				<div class="row">

						<!-- add the common hidden fields for every form -->
						<?php
							require_once('templates/shared_layouts/form_hidden_fields.php')
						?>
					</div>
						<?php
						// Add submit and home button if this is a pre-step.  Pre steps do not have a run_id.

						if (isset($_GET) && !isset($_GET['run_id']) && isset($_GET['visit_id']))
						{
							require_once('templates/shared_layouts/form_pre_submit_nav_buttons.php');
						}

						// Add submit and navigate buttons for 
						else if (isset($_GET) && isset($_GET['run_id']) && isset($_GET['visit_id']))
						{	
							require_once('templates/shared_layouts/form_submit_nav_buttons.php');
						}
					?>
					
				</fieldset>
			</form>
		</div>
	</div>
</div>