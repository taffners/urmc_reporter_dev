<div class="container-fluid">

	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			
			<fieldset>
				<legend>
					Sample Information for <?= $sampleLogBookArray[0]['mol_num'];?>
				</legend>
<?php
				require_once('templates/shared_layouts/sample_overview.php');
				
?>
			</fieldset>
		</div>
	</div>
	
	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			
			<fieldset>
				<legend>
					Sample Tests for <?= $sampleLogBookArray[0]['mol_num'];?>
				</legend>

				<div class="row d-print-none">
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" >
						<a href="?page=add_test&amp;sample_log_book_id=<?= $sampleLogBookArray[0]['sample_log_book_id'];?>" class="btn btn-primary btn-primary-hover" style="float:right;margin:10px;">
							Add Test to <?= $sampleLogBookArray[0]['mol_num'];?>
						</a>
						<a href="?page=add_single_analyte_pool" class="btn btn-primary btn-primary-hover">Add Single Analyte Pool</a>
					</div>					
				</div>
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
						<table id="sample-tests-overview-table" class="formated_table sort_table_no_inital_sort_no_paging" style="overflow-x:auto;margin-bottom: 10px;">
							<thead>
								<th>Test Name</th>
								<th>Test Status</th>
								<th>Start Date</th>
								<th>Finish Date</th>
								<th class="d-print-none">Days Since Start</th>
								<th class="d-print-none">Min Turnaround Time</th>
								<th class="d-print-none">Max Turnaround Time</th>
								<th>Assay Type</th>
								<th class="d-print-none">Change Test Status</th>
								<th class="d-print-none">Edit Ordered Test</th>
								
								<th class="d-print-none">Print Ordered Test Sheet</th>
								<th class="d-print-none" style="background:#d9534f;">Cancel Test/Test Summary</th>
								<th class="d-none d-print-block">Test Summary</th>
								
               					<th class="d-print-none" style="background:#d9534f;">Update Signed Out Test</th>
               					
							</thead>
							<tbody>
<?php						
							foreach ($testArray as $key => $test)
							{		
?>
								<tr>
									<td><?= $test['test_name'];?></td>
									<td><?= $test['test_status'];?></td>
									<td><?= $test['start_date'];?></td>
									<td><?= $test['finish_date'];?></td>
									<td
<?php 
									$days_past = $utils->days_past($test['start_date'], date('Y-m-d'));
									// make flashing background if past num days allowed
									if 	($test['max_turnaround_time'] != 0 && intval($test['max_turnaround_time']) < intval($days_past) && 
										isset($test['test_status']) && 
										$test['test_status'] != 'complete' && 
										$test['test_status'] != 'stop')
									{
										echo 'class="alert-flagged-qc d-print-none"';
									}
									else
									{
										echo 'class="d-print-none"';
									}
								
										if 	(
												isset($test['test_status']) && 
												$test['test_status'] != 'complete' && 
												$test['test_status'] != 'stop'
											)
										{
											echo $days_past;
										}
?>		
									></td>
									<td class="d-print-none"><?= $test['min_turnaround_time'];?></td>
									<td class="d-print-none"><?= $test['max_turnaround_time'];?></td>
									<td><?= $test['assay_name'];?></td>
									<td class="d-print-none">
<?php
									if (isset($test['test_status']) && $test['test_status'] == 'complete')
									{
?>								
										<span class="order-3 dot dot-green" title="Test Complete"></span>
<?php										
									}
									else if (isset($test['test_status']) && $test['test_status'] == 'stop')
									{
?>
										<span class="order-3 dot dot-red" title="Test Canceled"></span>
<?php										
									}
									else if (isset($test['test_status']) && $test['test_status'] == 'Sent Out')
									{
?>
										<span class="order-3 dot dot-grey" title="Test Sent Out"></span>
<?php										
									}
									else if(isset($test['assay_name']) && $test['assay_name'] === 'ngs' && isset($test['visit_id']) && !empty($test['visit_id']))
									{
										echo 'See NGS report Section';
									}
									else if(isset($test['infotrack_report']) && $test['infotrack_report'] === 'yes')
									{
										echo 'See single analyte pool to build report';
									}
									else
									{
?>										
										<a href="?page=update_test&sample_log_book_id=<?= $test['sample_log_book_id'];?>&ordered_test_id=<?= $test['ordered_test_id'];?>" class="btn btn-primary btn-primary-hover" title="Change Test Status from pending to complete"> 
											<span class="order-1 dot dot-yellow" ></span>&nbsp;
											<span class="order-2 fas fa-arrow-right"></span>&nbsp;
											<span class="order-3 dot dot-green" ></span>

										</a>
<?php										
									}
?>										

									</td>
									<td class="d-print-none">
<?php
									if (isset($test['test_status']) && $test['test_status'] == 'complete')
									{
?>								
										<span class="order-3 dot dot-green" title="Test <?= $test['test_name'];?> Complete"></span>
<?php										
									}
									else if (isset($test['test_status']) && $test['test_status'] == 'stop')
									{
?>
										<span class="order-3 dot dot-red" title="Test <?= $test['test_name'];?> Canceled"></span>
<?php										
									}
									else if (isset($test['test_status']) && $test['test_status'] == 'Sent Out')
									{
?>
										<span class="order-3 dot dot-grey" title="Test Sent Out"></span>
<?php										
									}
									else if (isset($test['visit_id']) && !empty($test['visit_id']))
									{
?>
										<a href="?page=add_test&sample_log_book_id=<?= $test['sample_log_book_id'];?>&ordered_test_id=<?= $test['ordered_test_id'];?>&visit_id=<?= $test['visit_id'];?><?= isset($test['run_id']) && !empty($test['run_id']) ? '&run_id='.$test['run_id'] : '' ;?><?= isset($test['patient_id']) && !empty($test['patient_id']) ? '&patient_id='.$test['patient_id'] : '' ;?>" class="btn btn-primary btn-primary-hover" title="Edit <?= $test['test_name'];?>"> Edit</a>
<?php
									}
									else
									{
?>										
										<a href="?page=add_test&sample_log_book_id=<?= $test['sample_log_book_id'];?>&ordered_test_id=<?= $test['ordered_test_id'];?>" class="btn btn-primary btn-primary-hover" title="Edit <?= $test['test_name'];?>">Edit</a>
<?php										
									}
?>										
									</td>
									<td class="d-print-none">
<?php
									
									if (isset($test['test_status']) && $test['test_status'] == 'stop')
									{
?>
										<span class="order-3 dot dot-red" title="Test <?= $test['test_name'];?> Canceled"></span>
<?php										
									}
									else if (isset($test['test_status']) && $test['test_status'] == 'Sent Out')
									{
?>
										<span class="order-3 dot dot-grey" title="Test Sent Out"></span>
<?php										
									}
									else
									{
?>																			
										<a href="?page=generate_test_sheet&sample_log_book_id=<?= $test['sample_log_book_id'];?>&ordered_test_id=<?= $test['ordered_test_id'];?>&visit_id=<?= $test['visit_id'];?>" class="btn btn-primary btn-primary-hover" target="_blank" title="Show PDF of Work Sheet for <?= $test['test_name'];?>"><img src="images/adobe-pdf-icon.png" alt="pdf icon" style="height:30px;width:30px"></a>
<?php
									}
?>										
									
									</td>
<!-- /////////////////////////////////////////////////////////////////////////////// 
	Cancel Test/Test Summary Column 
	/////////////////////////////////////////////////////////////////////////////// -->									
									<td>
<?php
									if (isset($test['test_status']) && $test['test_status'] == 'Sent Out')
									{
?>
										<span class="order-3 dot dot-grey" title="Test Sent Out"></span>
<?php										
									}
									else if (isset($test['test_status']) && $test['test_status'] != 'complete' && $test['test_status'] != 'stop' && $test['assay_name'] !== 'ngs')
									{
?>
										<a href="?page=stop_test_in_log_book&sample_log_book_id=<?= $test['sample_log_book_id'];?>&ordered_test_id=<?= $test['ordered_test_id'];?>" class="btn btn-primary btn-primary-hover d-print-none" title="Cancel Test <?= $test['test_name'];?>"> Cancel <span class="order-1 dot dot-yellow" ></span>&nbsp;
											<span class="order-2 fas fa-arrow-right"></span>&nbsp;
											<span class="order-3 dot dot-red" ></span></a>
<?php										
									}
									else if (isset($test['test_status']) && $test['test_status'] != 'complete' && $test['test_status'] != 'stop' && $test['assay_name'] === 'ngs' && isset($user_permssions) && strpos($user_permssions, 'NGS') !== false)
									{
?>
										<a href="?page=home#visit_id_<?= $test['visit_id']; ?>" class="btn btn-primary btn-primary-hover d-print-none" title="Cancel Test <?= $test['test_name'];?>"> Canceling via NGS reporting</a>									
<?php										
									}
									else if (isset($test['test_status']) && $test['test_status'] === 'stop')
									{
										// add any reason for canceling test
?>
										<?=$test['comments'];?>
<?php
									}
	/////////////////////////////////////////////////////////////////////////////// 
	// Add test summary info
	///////////////////////////////////////////////////////////////////////////////
									else if (isset($test['test_status']) && $test['test_status'] === 'complete' )
									{
										require('templates/shared_layouts/test_summary_info.php');
									}
?>								
									</td>
									<td class="d-print-none">
<?php
									// Allow any user to add a comment after the test was signed out.
									if 	(
											isset($test['ordered_test_id']) &&
											$test['test_status'] == 'complete' && 
									 		$test['assay_name'] !== 'ngs'  &&
									 		$test['infotrack_report'] !== 'yes' 
									 	)
									{
										$redirect = str_replace('page=', 'redirect_page=', $_SERVER['QUERY_STRING']);
?>

										<a href="?page=add_comment&comment_ref=<?= $test['ordered_test_id'];?>&comment_type=ordered_test_table&<?= $redirect;?>" class="btn btn-danger btn-danger-hover d-print-none" title="Add a comment to a this signed out test">Add Comment</a>			

<?php
									}
?>

<?php
									// Allow some users to ability to update extraction results
									if 	(
											isset($user_permssions) && 
											(
												strpos($user_permssions, 'edit_data') !== false ||
												strpos($user_permssions, 'admin') !== false
											) &&
											(
												$test['extraction_info'] === 'yes' || 
												$test['quantification_info'] === 'yes'
											)
										)
									{
?>
										<a href="?page=update_extraction_info_home&ordered_test_id=<?= $test['ordered_test_id'];?>&update_type=test_result&sample_log_book_id=<?= $_GET['sample_log_book_id'];?>" class="mt-2 btn btn-danger btn-danger-hover d-print-none" title="update extraction info">&Delta; Extraction Info</a>						
<?php
									}
?>


<?php
									// Allow some users to ability to update results
									if 	(
											isset($user_permssions) && 
											(
												strpos($user_permssions, 'edit_data') !== false ||
												strpos($user_permssions, 'admin') !== false
											) &&
											isset($test['test_status']) && 
											isset($test['infotrack_report']) &&
											isset($test['assay_name']) &&
											isset($test['test_result']) &&
											isset($test['ordered_test_id']) &&
											$test['test_status'] == 'complete' && 
									 		$test['assay_name'] !== 'ngs'  &&
									 		$test['infotrack_report'] !== 'yes' &&
									 		!empty($test['test_result']) 
									 	)
									{
										
?>
										<a href="?page=update_completed_test&ordered_test_id=<?= $test['ordered_test_id'];?>&update_type=test_result&sample_log_book_id=<?= $_GET['sample_log_book_id'];?>" class="mt-2 btn btn-danger btn-danger-hover d-print-none" title="update completed results">&Delta; Result Info</a>						
<?php
									}
?>

									</td>
								</tr>
<?php
							}	
								
?>
							</tbody>
						</table>
					</div>
				</div>

				

			</fieldset>

		</div>
	</div>
	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" >
			
			<fieldset>
				<legend>
					Sample Reflex Trackers for <?= $sampleLogBookArray[0]['mol_num'];?>
				</legend>

				<div class="row d-print-none">
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" >
						<a href="?page=add_reflex_tracker&sample_log_book_id=<?= $sampleLogBookArray[0]['sample_log_book_id'];?>" class="btn btn-primary btn-primary-hover" style="float: right;">
							Add Reflex Tracker to <?= $sampleLogBookArray[0]['mol_num'];?>
						</a>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
						<table class="formated_table sort_table_no_inital_sort_no_paging" style="overflow-x:auto;margin-bottom: 10px;">
							<thead>
								<th>Reflex Tracker</th>
								<th>Reflex Tracker status</th>
								<th>Reflex Link Status</th>
								<th>Added By</th>

							</thead>
							<tbody>
						<?php
							foreach($reflexArray as $key => $reflex)
							{
						?>
								<tr>
									<td><?=$reflex['reflex_name'];?></td>
									<td><?=$reflex['reflex_status'];?></td>
									<td><?=$reflex['link_status'];?></td>
									<td><?=$reflex['user_name'];?></td>

								</tr>
						<?php
							}
						?>
							</tbody>
						</table>
					</div>
				</div>
			</fieldset>
		</div>
	</div>
</div>