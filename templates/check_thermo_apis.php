<div class="container-fluid">
	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			<fieldset>
				<legend>Check that API Keys are still Valid</legend>

				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
						<h2>Check Torrent server API Key by</h2>
						<ul>
							<li><b>API Key Already set</b> 
								<ul>
									<li>Go to Torrent server website</li>
									<li><img src="images/torrent_server_check_API_1.png" width="100"></li>
									<li><img src="images/torrent_server_check_API.png" width="600"></li>
									<li>Copy and paste API key into field below</li>
									<li>Press submit to find if the API is correct</li>
									
								</ul>
							</li>
							<li><b>If no API Key is set</b>
								<ul>
									<li>..... Can't find right now</li>
									<li></li>
								</ul>
							</li>	
						</ul> 											
					</div>
				</div>
				<div id="Torrent_Server-api-key-msg-check" class="row alert">

				</div>
				<div class="form-group row">
					<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
							<label for="ts_api_key" class="form-control-label">Torrent Server API Key: <span class="required-field">*</span></label>
						</div>
					<div class="col-xs-12 col-sm-12 col-md-5 col-md-5">
						<input id="ts_api_key" class="form-control" type="text" name="ts_api_key">
					</div>
					<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
						<button type="button" data-selected_server="Torrent_Server" data-input_field_id="ts_api_key" class="btn btn-primary btn-primary-hover check-api-key-matches">Submit</button>
					</div>	
				</div>
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
						<h2>Check Ion Reporter API Key by</h2>
						<ul>
							<li>Go to Ion Reporter website</li>
							<li><img src="images/ion_reporter_check_API_key.png" width="200"></li>
							<li><img src="images/ion_reporter_check_API_key_modal.png" width="600"></li>
							<li>DO NOT CLICK Generate unless there isn't an api token</li>
							<li>Press copy to clipboard</li>
							<li>Paste in input field below</li>
							<li>Press submit to find if the API is correct</li>
						</ul> 											
					</div>
				</div>
				<div id="Ion_Reporter-api-key-msg-check" class="row alert">

				</div>
				<div class="form-group row">
					<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
							<label for="ir_api_key" class="form-control-label">Torrent Server API Key: <span class="required-field">*</span></label>
						</div>
					<div class="col-xs-12 col-sm-12 col-md-5 col-md-5">
						<input id="ir_api_key" class="form-control" type="text" name="ir_api_key">
					</div>
					<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
						<button type="button" data-selected_server="Ion_Reporter" data-input_field_id="ir_api_key" class="btn btn-primary btn-primary-hover check-api-key-matches">Submit</button>
					</div>	
				</div>
			</fieldset>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			<fieldset>
				<legend>Check Torrent Server APis</legend>

		<?php
			if (isset($errs) && empty($errs))
			{
		?>
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 alert alert-success">
						<h2>No Errors found</h2>
					</div>
				</div>
		<?php
			}

			else if (isset($errs) && !empty($errs))
			{
		?>
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 alert alert-danger">
						<h2>Errors found</h2>
		<?php
						var_dump($errs);
		?>
					</div>
				</div>
		<?php
			}

		?>		
				
			</fieldset>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			<fieldset>
				<legend>Check Ion Reporter APIs via python commandline</legend>

		<?php
			if (isset($IR_command) && !empty($IR_command))
			{
		?>
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 alert alert-success">
						<h2>Run the the python check_IR_API.py program with the following commands</h2>
			
						<ul>
							<li>On the pathlamp server navigate to location where <?= SITE_TITLE;?> is saved.  (With admin privileges you should know where this is)</li>
							<li>The run this command:
								<ul>
									<li><?= $IR_command;?></li>
								</ul>

							</li>
						</ul>								
					</div>
				</div>
		<?php
			}

		
			else if (isset($IR_command) && empty($IR_command))
			{
		?>
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 alert alert-danger">
						<h2>No Samples are found</h2>
		
					</div>
				</div>
		<?php
			}
			
		?>		
				
			</fieldset>
		</div>
	</div>
</div>
