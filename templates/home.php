<!-- reports outstanding -->
<div class="container-fluid ml-2" id="home-page-id">

<?php
if (isset($user_permssions) && strpos($user_permssions, 'NGS') !== false)
{
?>

	<div class="row d-print-none" id="toggle-samples-div">
		<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6"style="margin-bottom:5px;">
			<?php require('templates/shared_layouts/ngs_page_links.php');?>
		</div>

		<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6"style="margin-bottom:5px;">
			<hr class="plain-section-mark">
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			
					
					<?php
						require('templates/shared_layouts/ngs_add_links.php');
					?>
					
				</div>
			</div>
			<hr class="plain-section-mark">
		</div>
		
	</div>

	
	<!-- IF user was granted the permission of View_Only then hide all the tables which edit happens. This is accomplished in table html-->
	<div class="row" id="pre-ngs-samples-list-div">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		     <?php
		          require_once('templates/lists/pre_ngs_samples_list.php');
		     ?>
		</div>
	</div>
<?php
     if (isset($user_permssions) &&  strpos($user_permssions, 'View_Only') === false)
     {
     		$baseUrl = basename($_SERVER['REQUEST_URI']);
     	    // find if most recent pools or historic pools are selected
			if (isset($_GET['pool_view_type']) && $_GET['pool_view_type'] === 'recent')
			{
			   $recentClass = 'btn btn-success btn-success-hover pull-right';
			   $historicClass = 'btn btn-warning btn-warning-hover pull-right';
			   $baseUrl = str_replace('&pool_view_type=historic', '', $baseUrl);
			}
			elseif (isset($_GET['pool_view_type']) && $_GET['pool_view_type'] === 'historic')
			{
			   $recentClass = 'btn btn-warning btn-warning-hover pull-right';
			   $historicClass = 'btn btn-success btn-success-hover pull-right';
			   $baseUrl = str_replace('&pool_view_type=recent', '', $baseUrl);
			}
			else
			{
				$recentClass = 'btn btn-success btn-success-hover pull-right';
			   	$historicClass = 'btn btn-warning btn-warning-hover pull-right';
			}

?>	
	<div class="row" id="pre-ngs-library-pool-row">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			<fieldset>
				<legend id="pre-ngs-library-pool-legend">Pre NGS Library Pools</legend>				
				<div class="row">
					<div class="col-md-6 col-lg-6">

					</div>
					<div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
						<a href="<?= $baseUrl;?>&pool_view_type=recent#pre-ngs-library-pool-row" type="button" class="<?= $recentClass;?>">
							Recent Pools
						</a>
					</div>

					<div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
						<a id="historic-pools-btn" href="<?= $baseUrl;?>&pool_view_type=historic#pre-ngs-library-pool-row" type="button" class="<?= $historicClass;?>">
							Historic Pools
						</a>
					</div>
			     </div>
				<?php
			          require_once('templates/tables/pending_ngs_library_pool_table.php');
			     ?>
			</fieldset>
		</div>
	</div>
<?php
	}
?>
	<div class="row" id="outstanding-reports-row-dummy">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		    
		</div>
	</div>
	<div class="row" id="outstanding-reports-row">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		     <?php
		          require_once('templates/lists/reports_list.php');
		     ?>
		</div>
	</div>
	
	<div class="row d-print-none" id="toggle-samples-div">
		<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6"style="margin-bottom:5px;">
			<?php require('templates/shared_layouts/ngs_page_links.php');?>
		</div>

		<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6"style="margin-bottom:5px;">
			<hr class="plain-section-mark">
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			
					
					<?php
						require('templates/shared_layouts/ngs_add_links.php');
					?>
					
				</div>
			</div>
			<hr class="plain-section-mark">
		</div>
		
	</div>
<?php
} // NGS permission required

// if the user does not have NGS permission just add log book to home page
else if (isset($user_permssions) && strpos($user_permssions, 'log_book') !== false)
{
	// Show all pending tests
	$_GET['filter'] = 'pending';
	require_once('logic/sample_log_book.php');
	require_once('templates/sample_log_book.php');
}

?>	
</div>
<div id="browser_warning" class="d-none">
	<div class="col-xs-12 col-sm-12 col-md-12 pull-left alert alert-danger">
		Currently only the <a href="https://www.google.com/chrome/browser/desktop/"> Chrome web browser </a> is supported.  Please switch your browser.
		<div class="row">
			<div class="col-xs-2 col-sm-2 col-md-2">
				<a href="https://www.google.com/chrome/browser/desktop/">
					<img src="images/Chrome.png" alt="chrome" height="50" width="50">
				</a>
			</div>
		</div>
	</div>
</div>
