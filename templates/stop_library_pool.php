<div class="container-fluid">
	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			<?php
			     require_once('templates/tables/pending_ngs_library_pool_table.php');
			?>
		</div>
	</div>
	<div class="row">
<?php
if (isset($_GET['pool_chip_linker_id']) && isset($poolStepTracker) && isset($_GET['ngs_panel_id']))
{

?>
	<!-- report progress bar toggles -->	
	<?php
	require_once('templates/shared_layouts/report_toggle_show_status.php');
	?>

	<!-- Progress bar -->
	<?php
	require_once('templates/shared_layouts/pool_step_nav_bar.php')
	?>
	<!-- update area -->
	<div id="toggle-report-progress-bar" class="col-xs-10 col-sm-10 col-md-10 col-lg-10">
		
		<fieldset>
			<legend>
				<?= $utils->UnderscoreCaseToHumanReadable($page);?>
			</legend>

			<div class="alert alert-danger" style="font-size:20px;">
				<strong>This will remove this Library Pool from the Pre NGS Library Pools list.  Use this option if you would like to change which samples are in the library pool.</strong> 
			</div>

			<form id="add_visit_info" class="form" method="post" class="form-horizontal">		
				<div class="form-group row">
					<div class="col-xs-12 col-sm-4 col-md-4">
						<label for="status" class="form-control-label">Reason to Stop reporting:</label>
					</div>
					<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
						<input type="text" class="form-control" name="status" maxlength="30" list="status"/>
						<datalist id="status">
<?php
					if (isset($stopReportingOptions) && !empty($stopReportingOptions))
					{
						foreach ($stopReportingOptions as $key => $option)
						{
						
?>
							<option><?= $option['status'];?></option>
<?php
						}
					}	
?>								

						</datalist>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-12 col-sm-4 col-md-4">

					</div>
					<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
	
						<button type="submit" id="submit-stop-library-pool" name="submit-stop-library-pool" value="Submit" class="btn btn-danger btn-danger-hover">
							Submit
						</button>	
					</div>
				</div>
			</form>


			<h3 class="raised-card-subheader">Visits in Library Pool</h3>

			<table class="formated_table">
				<thead>
					<th>Reporter#</th>
					<th>Chip</th>
					<th>Sample Name</th>
					<th>Patient Name</th>
					<th>Version</th>
					<th>MRN</th>
				</thead>
				<tbody>
<?php
	if (isset($visits_in_pool))
	{

		foreach ($visits_in_pool as $key => $visit)
		{
			
?>
					<tr>
						<td><?= $visit['visit_id'];?></td>
						<td><?= $visit['chip_flow_cell'];?></td>
						<td><?= $visit['sample_name'];?></td>
						<td><?= $visit['patient_name'];?></td>
						<td><?= $visit['version'];?></td>
						<td><?= $visit['medical_record_num'];?></td>
					</tr>
<?php
		}
	}
?>			
				</tbody>
			</table>
		</fieldset>
	</div>

<?php
}
?>
	</div>
</div>