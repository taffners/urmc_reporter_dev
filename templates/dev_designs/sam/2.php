<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>

<script type="text/javascript">
  google.charts.load("current", {packages:["timeline"]});
  google.charts.setOnLoadCallback(drawChart);
  function drawChart() {

    var container = document.getElementById('example5.1');
    var chart = new google.visualization.Timeline(container);
    var dataTable = new google.visualization.DataTable();
    dataTable.addColumn({ type: 'string', id: 'Room' });
    dataTable.addColumn({ type: 'string', id: 'Name' });
    dataTable.addColumn({ type: 'date', id: 'Start' });
    dataTable.addColumn({ type: 'date', id: 'End' });
    dataTable.addRows([
      [ 'Treatment',   'hypomethylating agenets', new Date(2016, 5, 1), new Date(2017, 7, 1) ],
      [ 'Treatment',   'SQ cytarabine',     new Date(2017, 8, 1), new Date(2017, 11, 1) ],
      [ 'Treatment',   '7+3 w midostaurin',     new Date(2018, 0, 1), new Date(2018, 1, 1) ],
      [ 'Treatment',   'IDAC + midostaurin',     new Date(2018, 1, 1), new Date(2018, 2, 1) ],
      [ 'Treatment',   'Venetoclax + cytarabine',     new Date(2018, 3, 1), new Date(2018, 5, 1) ],
      [ 'Treatment',   'Ivosidenib',     new Date(2018, 7, 1), new Date(2019, 2, 1) ],

      [ 'Diagnosis', 'Leukopenia',       new Date(2015, 11, 1),  new Date(2015, 11, 1) ],
      [ 'Diagnosis', 'RAEB-2/AML-MDS',    new Date(2016, 4, 1),  new Date(2016, 4, 1) ],
      [ 'Diagnosis', 'AML with FLT-3 ITD',        new Date(2017, 7, 1),  new Date(2017, 7, 1) ],
      [ 'Diagnosis',   'AML with IDH R132C',    new Date(2018, 2, 1), new Date(2018, 2, 1) ],
      [ 'NGS',   '',     new Date(2016, 5, 15), new Date(2016, 5, 15) ],
      [ 'NGS',   '',     new Date(2018, 2, 9), new Date(2018, 2, 9) ],
      [ 'NGS',   '',     new Date(2018, 3, 17), new Date(2018, 3, 17) ],
      [ 'NGS',   '',     new Date(2018, 4, 30), new Date(2018, 4, 30) ],
      [ 'NGS',   '',     new Date(2018, 6, 26), new Date(2018, 6, 26) ],
      [ 'NGS',   '',     new Date(2018, 9, 23), new Date(2018, 9, 23) ],
      [ 'NGS',   '',     new Date(2018, 11, 14), new Date(2018, 11, 14) ],
      [ 'NGS',   '',     new Date(2019, 2, 11), new Date(2019, 2, 14) ],
      
      
      ]);


    var options = {
      timeline: { colorByRowLabel: true, barLabelStyle: { fontName: 'sans-serif', fontSize: 16 } }
    };

    chart.draw(dataTable, options);
  }

</script>

<div id="example5.1" style="margin:20px;height: 300px;"></div>
<div class="row">
     <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
          <a type="button" class="btn btn-info" href="https://pathlamp.urmc-sh.rochester.edu/devs/epi/?page=bed_trace_timeline">bed trace</a>
     </div>
</div>