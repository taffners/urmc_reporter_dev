			<tr class="completed">
				<td>5/6/2020</td>
				<td>Feature Add:  Add a toggle for sample_log_book page of identity testing.  Make it like anonymous donor except keep last name and first name same as default. Make sure it is updatable.</td>
				<td>
					
				</td>
				<td>					
					<ol>
						<li>
							<ul>
								<li></li>
							</ul>
						</li>
						<li></li>
						<li></li>
						<li></li>
					</ol>

				</td>
				<td><span class="dot dot-green"></span></td>
				<td></td>
			</tr>
			<tr class="completed">
				<td>5/5/2020</td>
				<td>Bug Fix: When updating an anoymous donor log book (add_sample_log_book) the MRN is required and the other settings are also not accurate.</td>
				<td>
					
				</td>
				<td>					
					
				</td>
				<td><span class="dot dot-green"></span></td>
				<td></td>
			</tr>
			<tr class="completed">
				<td>5/5/2020</td>
				<td>Feature Add: Add two lists to the Download Excel page of the sample name and the patient name so it can be copied and pasted into the excel sheet.</td>
				<td>
					
				</td>
				<td>					
					
				</td>
				<td><span class="dot dot-yellow"></span></td>
				<td></td>
			</tr>

			<tr class="completed">
				<td>4/29/2020</td>
				<td>Feature Add: Add the new Cosmic number and legacy Cosmic number.  Update database not just add new cosmic variants.  This new Cosmic number is causing the automatic update to error out since a column is not available any longer.</td>
				<td>
					
				</td>
				<td>					
					
				</td>
				<td><span class="dot dot-green"></span></td>
				<td></td>
			</tr>

			<tr class="completed">
				<td>4/28/2020</td>
				<td>Feature Add: Add an FAQ to the training section of infotrack</td>
				<td>
					
				</td>
				<td>					
					
				</td>
				<td><span class="dot dot-green"></span></td>
				<td></td>
			</tr>

			<tr class="completed">
				<td>4/28/2020</td>
				<td>Feature Add: Hide the error which appears when adding FLT3</td>
				<td>
					
				</td>
				<td>					
					
				</td>
				<td><span class="dot dot-green"></span></td>
				<td></td>
			</tr>

			<tr class="completed">
				<td>4/28/2020</td>
				<td>Feature Add: Make Mutalzer automatically rename OFA complex variants such as p.[Met227Ser;Phe228Ile] becomes p.Met227_Phe228delinsSerIle automatically. </td>
				<td>
					
				</td>
				<td>					
					
				</td>
				<td><span class="dot dot-green"></span></td>
				<td></td>
			</tr>

			<tr class="completed">
				<td>4/24/2020</td>
				<td>Feature Add: Make a way to handle the OFA transfer now has the ability to find protein names with names like: p.[Met227Ser;Phe228Ile] and p.[Asn566=;Asn567=;Tyr568=;Val569=;Tyr570=;Ile571=;Asp572=;Pro573=;Thr574=;Gln575=;Leu576Pro]. </td>
				<td>
					
				</td>
				<td>					
					
				</td>
				<td><span class="dot dot-green"></span></td>
				<td></td>
			</tr>

			<tr class="completed">
				<td></td>
				<td>Feature Add: Add automatic testing to ensure APIs for Torrent server and Ion Reporter are still active and work the same way and mounted drives still are present.  Make a feature for auto testing of strand bias generator also</td>
				<td>
					<ol>
						<li>Design:<span class="dot dot-purple"></span></li>
						<li>List Risks:<span class="dot dot-purple"></span></li>
						<li>Implementation:<span class="dot dot-purple"></span></li>
						<li>Testing:<span class="dot dot-purple"></span></li>
					</ol>
				</td>
				<td>					
					<ul>
						
						<li>
							<ol>
								<li>This will help with Torrent server, Ion Reporter, and pathlamp updates.</li>
								<li>Use to check before every test.</li>
								<li>Add this to the admin page.  This will be the start to many automated tests to come.</li>
							</ol>
							
						</li>
						<li>
							<ol>
								<li>Check API connections
									<ul>
										<li>Check API keys are still valid by having user copy and paste key from Torrent server and ion reporter <span class="dot dot-green"></span></li>
										<li>class.thermo_backup->TorrentServerRunQualityResults()</li>
									</ul>
								</li>
								<li>Check the format is the same as expected</li>
								<li>Take MD5 of all files once everything is transfered.  This can be used to to check any data set for testing. </li>
							</ol>
						</li>
						<li></li>
						<li></li>
					</ul>
				</td>
				<td><span class="dot dot-green"></span></td>
				<td></td>
			</tr>

			<tr class="completed">
				<td>4/16/20</td>
				<td>Feature Add: OFA Feature Update: synchronised naming of TS plan and backup folder. Included a less error prone way of naming plan with barcodes. Add excel run file to infotrack. Requested by Bill</td>
				<td>
					
				</td>
				<td>					
					
				</td>
				<td><span class="dot dot-green"></span></td>
				<td></td>
			</tr>
			<tr class="completed">
				<td>3/17/2020</td>
				<td>
					Data fix:  I need a patient’s results changed in infotrack. Principle, James MD1252-20 J5233938. I put negative for their IgH and it needs to be positive.
				</td>
				<td>
					sudo su<br>
					sudo mysqldump -u root urmc_reporter_db --single-transaction --quick --lock-tables=false > /var/www/urmc_reporter_data/db_backups/urmc_reporter_db_2020_03_31.sql<br>

					MariaDB [urmc_reporter_db]> SELECT slbt.sample_log_book_id, slbt.first_name, slbt.last_name, slbt.soft_lab_num, slbt.mol_num, slbt.medical_record_num AS MRN FROM sample_log_book_table slbt WHERE slbt.soft_lab_num = 'J5233938' AND slbt.mol_num = '1252-20';<br>
					+--------------------+------------+-----------+--------------+---------+--------+<br>
					| sample_log_book_id | first_name | last_name | soft_lab_num | mol_num | MRN    |<br>
					+--------------------+------------+-----------+--------------+---------+--------+<br>
					|               4369 | James      | Principe  | J5233938     | 1252-20 | 674685 |<br>
					+--------------------+------------+-----------+--------------+---------+--------+<br>
					1 row in set (0.00 sec)<br>


					<br><br>
					Find all tests ordered on sample<br>
					MariaDB [urmc_reporter_db]> SSELECT ott.ordered_test_id, ott.orderable_tests_id, ott.test_status, ott.test_result, otst.test_name FROM ordered_test_table ott LEFT JOIN orderable_tests_table otst ON otst.orderable_tests_id = ott.orderable_tests_id WHERE ott.sample_log_book_id = 4369;<br>
					+-----------------+--------------------+-------------+---------------+----------------+<br>
					| ordered_test_id | orderable_tests_id | test_status | test_result   | test_name      |<br>
					+-----------------+--------------------+-------------+---------------+----------------+<br>
					|            8892 |                 11 | complete    |               | DNA Extraction |<br>
					|            8893 |                  6 | complete    | Negative      | TCRG           |<br>
					|            8894 |                  7 | complete    | Weak Negative | IGH            |<br>
					|            8895 |                  8 | complete    | Positive      | IGK            |<br>
					+-----------------+--------------------+-------------+---------------+----------------+<br>
					4 rows in set (0.01 sec)<br>


					<br><br>
					MariaDB [urmc_reporter_db]> SELECT DISTINCT test_result FROM ordered_test_table;<br>
					+---------------+<br>
					| test_result   |<br>
					+---------------+<br>
					|               |<br>
					| Negative      |<br>
					| No Signal     |<br>
					| Positive      |<br>
					| Weak Negative |<br>
					| NULL          |<br>
					| Weak Positive |<br>
					| See Interpt.  |<br>
					+---------------+<br>
					8 rows in set (0.00 sec)<br>



					UPDATE ordered_test_table SET test_result = 'Positive' WHERE sample_log_book_id = 4369 and ordered_test_id = 8894 and orderable_tests_id = 7 AND test_result = 'Weak Negative';

				</td>
				<td>					
					
				</td>
				<td><span class="dot dot-green"></span></td>
				<td></td>
			</tr>
			<tr class="completed">
				<td>3/18/2020</td>
				<td>
					Hi Samantha,
 
					It was brought to my attention that everyone is still using the nanodrop book instead of just having the record be through Infotrack.  I asked why they have not discontinued using the paper records and they said because there isn't an elution volume field and they would like dilution calculations recorded.  Would it be fairly simple to add those things?  A calculator like the one that Nufatt made (attached) would be nice if it isn't a lot of trouble..or like the ngs one you made for us. This one just does 50 ng/ul and it would be better if it were able to calculate for other concentrations because we regularly have some samples at 25 ng/ul. 
					 
					Thanks so much,
					Paige
				</td>
				<td>
					Sure I can add this.  This would probably take about 2 days to complete.  However, I have a lot questions.

					With the OFA needing to go live ASAP how does this fit in?  
						OFA is the priority.  We can do this after.

					Currently the test sign out forms are not updatable.  It is a big task to make them updatable and I'm unsure we should make them updatable anyway.  Will this be an issue? 
						 I'm not sure what you mean by this. 

					I have plans to add a comment box to all finished tests.  This is a medium size task.  I could add a calculator to the comment field if necessary.  This is not part of the 2 day estimate.

					Are these the fields I will be adding?
					    elution volume 
					    		Yes
					    
					    For calculator:
					    vol DNA 
					    vol EB
					    dilution final volume (Default: 50)
					    final concentration (Default: 50)

					    		Yes. This seems fine.  We input the stock DNA concentration and default to final volume of 50 and concentration of 50ng/ul.   It gives us the vol of stock DNA to use and vol of buffer to add.   Final volume and concentration should be able to be adjusted from 50 if necessary.  See chart under below.

					How many digits after the decimal point should I allow for the above fields?   Is 2 enough?
						1 Digit after the decimal is fine because it will be hard to measure accurately smaller than 10ths.

					How does elution volume fit into the calculator?  DNA volume can not be greater than elution volume??
						Correct

					Can I make elution volume a required field?  Should I add a default value for elution volume?  Is elution volume always a whole number or is there decimal points?  If so how many?  

						Elution volume can be a required field.  Elution volumes are always whole numbers.  Typically 50ul but sometimes 100ul. We should probably allow for 1 decimal place however.  Sometimes we will have a stock DNA that is close to 50ng/ul and no dilution is made.  Elution buffer is added to bring the concentration to 50ng/ul.  So if we added 15.5ul we would but the elution volume in as 65.5 in such case unless you want to make a separate field to record the 15.5 added. 


					When will this calculator be active?  After data is entered in the stock concentration?
						Sounds fine.

					Can I make all the calculator fields required if stock concentration is added? 
						I wouldn't.  If the DNA is close to 50ng/ul or is less than 50ng/ul we don't make a dilution and wouldn't use the calculator.  

					Can I remove the concentration for dilution as an independent field and tie it to the calculator?
						Yes

					Should default be 50 ng/ul with the option of 25 ng/ul?​  Or should this be a user editable field?  If so is there a max and min I can set?
						 It should default to 50ng/ul but I would like it to be editable to anything. 

					Will dilution final volume always be 50 ul?
						Usually, but not always.

					Can I remove any of the following fields like Number X dilution?
						 Yes. We will assume a dilution was made if there is a calculation.  
				</td>
				<td>					
					Sent email with Infotrack New Feature Review Request subject to lab and Phil for approval before rolling out. 
				</td>
				<td><span class="dot dot-green"></span></td>
				<td></td>
			</tr>
			<tr class="completed">
				<td>3/17/2019</td>
				<td>Fix Data entry: remove comment Pre-tx donor from sample log book table 1148-20, MRN 1894544, J5162523</td>
				<td> 
				</td>
				<td>					
					SELECT slbt.sample_log_book_id, slbt.medical_record_num, slbt.mol_num, slbt.soft_lab_num  FROM sample_log_book_table slbt WHERE slbt.medical_record_num = '1894544' AND slbt.mol_num = '1148-20' AND slbt.soft_lab_num = 'J5162523';<br>
					+--------------------+--------------------+---------+--------------+<br>
					| sample_log_book_id | medical_record_num | mol_num | soft_lab_num |<br>
					+--------------------+--------------------+---------+--------------+<br>
					|               4265 | 1894544            | 1148-20 | J5162523     |<br>
					+--------------------+--------------------+---------+--------------+<br>
					1 row in set (0.00 sec)<br>
					<br><br>

					describe comment_table;<br>
					+--------------+-------------+------+-----+-------------------+-----------------------------+<br>
					| Field        | Type        | Null | Key | Default           | Extra                       |<br>
					+--------------+-------------+------+-----+-------------------+-----------------------------+<br>
					| comment_id   | int(11)     | NO   | PRI | NULL              | auto_increment              |<br>
					| user_id      | int(11)     | YES  | MUL | NULL              |                             |<br>
					| comment_ref  | int(11)     | YES  | MUL | NULL              |                             |<br>
					| comment_type | varchar(30) | NO   | MUL | NULL              |                             |<br>
					| comment      | text        | NO   |     | NULL              |                             |<br>
					| time_stamp   | timestamp   | NO   |     | CURRENT_TIMESTAMP | on update CURRENT_TIMESTAMP |<br>
					| status       | varchar(30) | YES  |     | active            |                             |<br>
					+--------------+-------------+------+-----+-------------------+-----------------------------+<br>
					7 rows in set (0.00 sec)<br>


					MariaDB [urmc_reporter_db]> SELECT DISTINCT comment_type FROM comment_table;<br>
					+-----------------------+<br>
					| comment_type          |<br>
					+-----------------------+<br>
					| add_visit             |<br>
					| knowledge_base_table  |<br>
					| message_board         |<br>
					| observed_variant      |<br>
					| ordered_test_table    |<br>
					| sample_log_book_table |<br>
					| visit_table           |<br>
					+-----------------------+<br>
					7 rows in set (0.00 sec)<br>
					<br><br>

					MariaDB [urmc_reporter_db]> SELECT * FROM comment_table ct WHERE comment_type = 'sample_log_book_table' AND comment_ref = 4265;<br>
					+------------+---------+-------------+-----------------------+-------------------+---------------------+--------+<br>
					| comment_id | user_id | comment_ref | comment_type          | comment           | time_stamp          | status |<br>
					+------------+---------+-------------+-----------------------+-------------------+---------------------+--------+<br>
					|       3428 |      11 |        4265 | sample_log_book_table |  Pre-tx donor     | 2020-03-16 14:11:44 | active |<br>
					|       3429 |      11 |        4265 | sample_log_book_table | Pre-tx recipient  | 2020-03-17 12:01:19 | active |<br>
					+------------+---------+-------------+-----------------------+-------------------+---------------------+--------+<br>
					2 rows in set (0.00 sec)<br>
					<br><br>

					SELECT * FROM comment_table WHERE comment_id = 3428 AND comment_type = 'sample_log_book_table' AND comment_ref = 4265 AND comment = ' Pre-tx donor';<br>
					<br>
					DELETE FROM comment_table WHERE comment_id = 3428 AND comment_type = 'sample_log_book_table' AND comment_ref = 4265 AND comment = ' Pre-tx donor';<br>
				</td>
				<td><span class="dot dot-green"></span></td>
				<td></td>
			</tr>
			<tr class="completed">
				<td>3/17/2020</td>
				<td>
					Data fix:  I ran a TCRG on Specht J5096903 MD1065-20. I entered the result in Infotrack as negative, but it really should have been weak positive. Can you please change the result for me?
				</td>
				<td>
					sudo su<br>
					sudo mysqldump -u root urmc_reporter_db --single-transaction --quick --lock-tables=false > /var/www/urmc_reporter_data/db_backups/urmc_reporter_db_2020_03_17.sql<br>

					MariaDB [urmc_reporter_db]> SELECT slbt.sample_log_book_id, slbt.first_name, slbt.last_name, slbt.soft_lab_num, slbt.mol_num, slbt.medical_record_num AS MRN FROM sample_log_book_table slbt WHERE slbt.soft_lab_num = 'J5096903' AND slbt.mol_num = '1056-20';<br>
					+--------------------+------------+-----------+--------------+---------+--------+<br>
					| sample_log_book_id | first_name | last_name | soft_lab_num | mol_num | MRN    |<br>
					+--------------------+------------+-----------+--------------+---------+--------+<br>
					|               4173 | Thomas     | Specht    | J5096903     | 1056-20 | 832279 |<br>
					+--------------------+------------+-----------+--------------+---------+--------+<br>
					1 row in set (0.00 sec)<br>

					<br><br>
					Find all tests ordered on sample<br>
					MariaDB [urmc_reporter_db]> SELECT ott.ordered_test_id, ott.orderable_tests_id, ott.test_status, ott.test_result, otst.test_name FROM ordered_test_table ott LEFT JOIN orderable_tests_table otst ON otst.orderable_tests_id = ott.orderable_tests_id WHERE ott.sample_log_book_id = 4173;<br>
					+-----------------+--------------------+-------------+-------------+----------------+<br>
					| ordered_test_id | orderable_tests_id | test_status | test_result | test_name      |<br>
					+-----------------+--------------------+-------------+-------------+----------------+<br>
					|            8511 |                 11 | complete    |             | DNA Extraction |<br>
					|            8512 |                  6 | complete    | Negative    | TCRG           |<br>
					+-----------------+--------------------+-------------+-------------+----------------+<br>

					<br><br>
					MariaDB [urmc_reporter_db]> SELECT DISTINCT test_result FROM ordered_test_table;<br>
					+---------------+<br>
					| test_result   |<br>
					+---------------+<br>
					|               |<br>
					| Negative      |<br>
					| No Signal     |<br>
					| Positive      |<br>
					| Weak Negative |<br>
					| NULL          |<br>
					| Weak Positive |<br>
					| See Interpt.  |<br>
					+---------------+<br>
					8 rows in set (0.00 sec)<br>



					UPDATE ordered_test_table SET test_result = 'Positive' WHERE sample_log_book_id = 4173 and ordered_test_id = 8512 and orderable_tests_id = 6 AND test_result = 'Negative';

				</td>
				<td>					
					
				</td>
				<td><span class="dot dot-green"></span></td>
				<td></td>
			</tr>
			<tr class="completed">
				<td>3/13/2020</td>
				<td>Urgent: Revert back report 1043 which was accidentally signed out by Zoltan</td>
				<td>
					$ sudo su<br>
					# sudo mysqldump -u root urmc_reporter_db --single-transaction --quick --lock-tables=false > /var/www/urmc_reporter_data/db_backups/urmc_reporter_db_2020_03_13.sql<br>
					<br><br>
					# exit
					<br><br>
					$ sudo mysql -u root
					MariaDB [(none)]> use urmc_reporter_db;	
					
					MariaDB [urmc_reporter_db]> SELECT visit_id, run_id, user_id, soft_path_num, soft_lab_num, mol_num FROM visit_table WHERE visit_id = 1043;
					+----------+--------+---------+---------------+--------------+---------+
					| visit_id | run_id | user_id | soft_path_num | soft_lab_num | mol_num |
					+----------+--------+---------+---------------+--------------+---------+
					|     1043 |    991 |      11 | 20-SHE442     | J4106536     | 603-20  |
					+----------+--------+---------+---------------+--------------+---------+
					1 row in set (0.00 sec)
					<br><br>

					MariaDB [urmc_reporter_db]> SELECT DISTINCT status FROM run_info_table;
					+--------------------------------+
					| status                         |
					+--------------------------------+
					| Sent to Doctors via word       |
					| completed                      |
					| Deceased Patient               |
					| pending                        |
					| pass_control                   |
					| Repeat for confirmation.       |
					| Normal control for validation. |
					| used complex variant calling   |
					| fail_control                   |
					| Normal Control                 |
					| old                            |
					| New positive control           |
					| tech approved                  |
					| Blood reported out.            |
					| Test done to confirm a mutatio |
					| Wrong assay ordered.           |
					| We repeated this specimen.     |
					| incorrect data input by ST     |
					+--------------------------------+
					18 rows in set (0.01 sec)
					<br><br>	
					MariaDB [urmc_reporter_db]> SELECT run_id, status FROM run_info_table WHERE run_id = 991;
					+--------+-----------+
					| run_id | status    |
					+--------+-----------+
					|    991 | completed |
					+--------+-----------+
					1 row in set (0.00 sec)
					<br><br>		

					MariaDB [urmc_reporter_db]> SELECT srx.*, rst.step, ut.first_name FROM step_run_xref srx LEFT JOIN user_table ut ON ut.user_id = srx.user_id LEFT JOIN report_steps_table rst ON rst.step_id = srx.step_id WHERE srx.run_id = 991;
					+------------------+--------+---------+---------+---------------------+-------------------+------------+
					| step_run_xref_id | run_id | step_id | user_id | time_stamp          | step              | first_name |
					+------------------+--------+---------+---------+---------------------+-------------------+------------+
					|             6461 |    991 |       5 |      24 | 2020-03-13 09:55:10 | qc                | Saide      |
					|             6462 |    991 |      22 |      24 | 2020-03-13 09:57:50 | qc_variant        | Saide      |
					|             6468 |    991 |       4 |       3 | 2020-03-13 12:07:39 | verify_variants   | Paul       |
					|             6469 |    991 |       8 |       3 | 2020-03-13 12:07:50 | add_summary       | Paul       |
					|             6470 |    991 |      20 |       3 | 2020-03-13 12:08:13 | reviewers_emailed | Paul       |
					|             6471 |    991 |      14 |      24 | 2020-03-13 12:13:42 | tech_approved     | Saide      |
					|             6473 |    991 |       3 |      26 | 2020-03-13 12:18:17 | confirmed         | Zoltan     |
					+------------------+--------+---------+---------+---------------------+-------------------+------------+
					7 rows in set (0.00 sec)

					Need to remove the confirmed step and change run_info_table status back to tech approved.

					DELETE FROM step_run_xref WHERE step_run_xref_id = 6473 AND run_id = 991 AND step_id = 3 AND user_id = 26;

					UPDATE run_info_table SET status = 'tech approved'  WHERE run_id = 991 AND status = 'completed' AND sample_name = '603-20_J4106536';
				</td>
				<td>					
					
				</td>
				<td><span class="dot dot-green"></span></td>
				<td></td>
			</tr>
			<tr class="completed">
				<td>3/11/2020</td>
				<td>Feature Add: Just wondering if we can make it so that canceled tests are not listed in the Test Ordered section on the DNA extraction sheet?  Requested by Dan
				</td>
				<td>
					
				</td>
				<td>					
					 
				</td>
				<td><span class="dot dot-green"></span></td>
				<td></td>
			</tr>

			<tr class="completed" >
				<td>3/11/2020</td>
				<td>Testing once switched live: Test 4 identifiers are working once switched to live server.  The SQL view can not be updated until code is added to live. </td>
				<td>
					
				</td>
				<td>					
					<ol>
						<li>Test 5/6 identifiers are working once switched to live server. <span class="dot dot-green"></li>
						<li>
							Switch Backup location depending on live vs dev<span class="dot dot-green">
						</li>
						<li>
							-- DROP AND re add pending_samples_substring_order_num_vw <span class="dot dot-green"></span>
						</li>
						<li>
							make sure I didn't forget to add any database tables or fields <span class="dot dot-green">
						</li>
						<li>2 chips are causing error.  Try with correct number of samples.  Seems like err is caused by to many rows in ir_backup_tracker </li>
						<li>Need to add full sample name does not contain chip A/B <span class="dot dot-green">  This was causing coverage data not being found.</li>
						<li>pool_upload_data make yellow btn target=_blank</li>
						<li>Change backup locationg in legend to tech loc.</li>
						<li>
							Check that nothing is affecting I/O wait.<br>
							https://bencane.com/2012/08/06/troubleshooting-high-io-wait-in-linux/<span class="dot dot-green"></span>
						</li>						
						<li>Test that low qc data is being up loaded</li>
					</ol>
				</td>
				<td><span class="dot dot-green"></span></td>
				<td></td>
			</tr>
			<tr class="completed">
				<td>3/11/2020</td>
				<td>Feature Add:  When uploading data for OFA.  Make sure it doesn't send the user to the homepage but back to the pool</td>
				<td>
					
				</td>
				<td>					
					
				</td>
				<td><span class="dot dot-green"></span></td>
				<td></td>
			</tr>
			<tr class="completed">
				<td>3/11/20</td>
				<td>Feature Add: Add Stat metrics for each test to the audit such as standard deviation and average turnaround time.  Requested by Zoltan 2/12/2020
				</td>
				<td>
					Feature add: Audit Thanks Bonnie; However, I would like to see this in more granularity, broken out to individual tests. For example, the number of JAK2 tests and their average+SD TAT for December, 2019.
				</td>
				<td>					
					 
				</td>
				<td><span class="dot dot-green"></span></td>
				<td></td>
			</tr>
			<tr class="completed">
				<td>3/10/2020</td>
				<td>Feature Add: Add a timeline of mutations CAP samples should be ignored.  Remove VAF from color picker and order by highest to lowest VAF
				</td>
				<td>
					
				</td>
				<td>					
					
				</td>
				<td><span class="dot dot-green"></span></td>
				<td></td>
			</tr>
			<tr class="completed">
				<td>3/6/2020</td>
				<td>Urgent: I found a COSMIC number for the DNMT3A mutation. It is COSM5878749. Can you put this into the database? Thanks, Paul New Message for Report # 1039</td>
				<td>
					DNMT3A c.2479-2A>C p.? 
				</td>
				<td>		
					Backup database first
					<br><br>
					$ sudo su<br>
					# sudo mysqldump -u root urmc_reporter_db --single-transaction --quick --lock-tables=false > /var/www/urmc_reporter_data/db_backups/urmc_reporter_db_2020_03_06.sql<br>
					# exit
					<br><br>
					Access database<br>
					$ sudo mysql -u root<br>
					MariaDB [(none)]> use urmc_reporter_db;<br>
					<br><br>			
					Since reports are generated every time from the database we need to make sure we are not going to change a report that was already signed out.  So we need to check to see if the variant is was already used for a different run_id.
					<br><br>
					SELECT ovt.observed_variant_id AS ov_id, ovt.run_id, vt.visit_id, ovt.genes, ovt.coding, ovt.amino_acid_change AS aa_change FROM observed_variant_table ovt LEFT JOIN visit_table vt ON vt.run_id = ovt.run_id WHERE ovt.genes ="TP53" AND ovt.coding = "c.736A>G" AND ovt.amino_acid_change = "p.Met246Val";
					<br><br>
					+-------+--------+----------+-------+----------+-------------+<br>
					| ov_id | run_id | visit_id | genes | coding   | aa_change   |<br>
					+-------+--------+----------+-------+----------+-------------+<br>
					| 16854 |    979 |     1035 | TP53  | c.736A>G | p.Met246Val |<br>
					+-------+--------+----------+-------+----------+-------------+<br>
					1 row in set (0.00 sec)<br>

					<br><br>
					Since we only have one row the cosmic ID in the knowledge_base_table can be updated without causing any harm to previously signed out reports.  (DO NOT CONTINUE IF FOUND IN MULTIPLE REPORTS.  All of the reports will have to be sent to revise mode so the original report will be save in a hard copy.)Now find the variant in the knowledge_base_table
					<br><br>
					SELECT kbt.knowledge_id AS k_id, kbt.genes, kbt.coding, kbt.amino_acid_change AS aa_change, kbt.cosmic FROM knowledge_base_table kbt WHERE kbt.genes ="TP53" AND kbt.coding = "c.736A>G" AND kbt.amino_acid_change = "p.Met246Val";
					<br><br>
					+------+-------+----------+-------------+--------+<br>
					| k_id | genes | coding   | aa_change   | cosmic |<br>
					+------+-------+----------+-------------+--------+<br>
					|  380 | TP53  | c.736A>G | p.Met246Val | 43555  |<br>
					+------+-------+----------+-------------+--------+<br>
					1 row in set (0.00 sec)<br>

					<br><br>
					Ensure there's only one.  Then change the cosmic number.  Be very certain that you are selecting the correct variant in the knowlege_base_table.  You could end up inadvertently changing many or all entries.
					<br><br>
					UPDATE knowledge_base_table SET cosmic = 3958808 WHERE genes ="TP53" AND coding = "c.736A>G" AND amino_acid_change = "p.Met246Val" AND knowledge_id = 380; 
				</td>
				<td><span class="dot dot-green"></span></td>
				<td></td>
			</tr>
			<tr class="completed">
				<td>3/6/2020</td>
				<td>Urgent: I found a COSMIC number for the DNMT3A mutation. It is COSM5878749. Can you put this into the database? Thanks, Paul New Message for Report # 1039</td>
				<td>
					DNMT3A c.2479-2A>C p.? 
				</td>
				<td>		
					Backup database first
					<br><br>
					$ sudo su<br>
					# sudo mysqldump -u root urmc_reporter_db --single-transaction --quick --lock-tables=false > /var/www/urmc_reporter_data/db_backups/urmc_reporter_db_2020_03_06.sql<br>
					# exit
					<br><br>
					Access database<br>
					$ sudo mysql -u root<br>
					MariaDB [(none)]> use urmc_reporter_db;<br>
					<br><br>			
					Since reports are generated every time from the database we need to make sure we are not going to change a report that was already signed out.  So we need to check to see if the variant is was already used for a different run_id.
					<br><br>
					SELECT ovt.observed_variant_id AS ov_id, ovt.run_id, vt.visit_id, ovt.genes, ovt.coding, ovt.amino_acid_change AS aa_change FROM observed_variant_table ovt LEFT JOIN visit_table vt ON vt.run_id = ovt.run_id WHERE ovt.genes ="DNMT3A" AND ovt.coding = "c.2479-2A>C" AND ovt.amino_acid_change = "p.?";
					<br><br>
					+-------+--------+----------+--------+-------------+-----------+<br>
					| ov_id | run_id | visit_id | genes  | coding      | aa_change |<br>
					+-------+--------+----------+--------+-------------+-----------+<br>
					| 16847 |    974 |     1039 | DNMT3A | c.2479-2A>C | p.?       |<br>
					+-------+--------+----------+--------+-------------+-----------+<br>
					1 row in set (0.00 sec)<br>

					<br><br>
					Since we only have one row the cosmic ID in the knowledge_base_table can be updated without causing any harm to previously signed out reports.  (DO NOT CONTINUE IF FOUND IN MULTIPLE REPORTS.  All of the reports will have to be sent to revise mode so the original report will be save in a hard copy.)Now find the variant in the knowledge_base_table
					<br><br>
					SELECT kbt.knowledge_id AS k_id, kbt.genes, kbt.coding, kbt.amino_acid_change AS aa_change, kbt.cosmic FROM knowledge_base_table kbt WHERE kbt.genes ="DNMT3A" AND kbt.coding = "c.2479-2A>C" AND kbt.amino_acid_change = "p.?";
					<br><br>
					+--------+--------+-------------+-----------+--------+<br>
					| k_id   | genes  | coding      | aa_change | cosmic |<br>
					+--------+--------+-------------+-----------+--------+<br>
					| 109074 | DNMT3A | c.2479-2A>C | p.?       |        |<br>
					+--------+--------+-------------+-----------+--------+<br>
					1 row in set (0.00 sec)<br>
					<br><br>
					Ensure there's only one.  Then change the cosmic number.  Be very certain that you are selecting the correct variant in the knowlege_base_table.  You could end up inadvertently changing many or all entries.
					<br><br>
					UPDATE knowledge_base_table SET cosmic = 5878749 WHERE genes ="DNMT3A" AND coding = "c.2479-2A>C" AND amino_acid_change = "p.?" AND knowledge_id = 109074; 
				</td>
				<td><span class="dot dot-green"></span></td>
				<td></td>
			</tr>
			<tr class="completed">
				<td>2/12/2020</td>
				<td>Feature Add: Sometimes we do not have soft lab order numbers but we have all the other information.  Since soft lab order number needs to be unique. Make a button to increment UNK#.
				</td>
				<td>
					
				</td>
				<td>					
					 
				</td>
				<td><span class="dot dot-green"></span></td>
				<td>a49de63..2f4ca76</td>
			</tr>
			<tr class="completed">
				<td>2/12/2020</td>
				<td>Feature Add: Add UNK# increment to the MRN num also
				</td>
				<td>
					
				</td>
				<td>					
					 
				</td>
				<td><span class="dot dot-green"></span></td>
				<td>fd66d94</td>
			</tr>
			<tr class="completed">
				<td>2/12/2020</td>
				<td>Feature Add: Add VAF to patient variant timeline.  Requested by Zoltan 2/12/20
				</td>
				<td>
					
				</td>
				<td>					
					 
				</td>
				<td><span class="dot dot-green"></span></td>
				<td>a49de63..2f4ca76</td>
			</tr>
			<tr class="completed">
				<td>2/18/2020</td>
				<td>Bug Fix: Rainbow loader added twice for add_SNV_to_knowledge_base reported by Paul. (commit 2f5edd8)
				</td>
				<td>
					
				</td>
				<td>					
					 
				</td>
				<td><span class="dot dot-green"></span></td>
				<td>2f5edd8</td>
			</tr>
			<tr class="completed">
				<td>2/24/2020</td>
				<td>I goofed up and put the wrong data in the etv6 SNV. Can you undo this one and I will put in the correct data?
				Thanks,Paul
				</td>
				<td>
					DB [urmc_reporter_db]> SELECT * FROM knowledge_base_table WHERE genes='ETV6' AND coding = "c.725delA" and amino_acid_change = 'p.Asn242IlefsTer24';
					+--------------+-------+-----------+--------------------+--------------------------+------+-------------------+------                                                                                                                        ------------------+------+--------+------------+-------------------+--------------+------+-------+------+-------+----                                                                                                                        --+---------------------+
					| knowledge_id | genes | coding    | amino_acid_change  | amino_acid_change_abbrev | chr  | loc               | mutat                                                                                                                        ion_type          | pmid | cosmic | transcript | fathmm_prediction | fathmm_score | hgnc | notes | exon | dbsnp | pfa                                                                                                                        m | time_stamp          |
					+--------------+-------+-----------+--------------------+--------------------------+------+-------------------+------                                                                                                                        ------------------+------+--------+------------+-------------------+--------------+------+-------+------+-------+----                                                                                                                        --+---------------------+
					|       109065 | ETV6  | c.725delA | p.Asn242IlefsTer24 | p.N242Ifs*24             | 12   | 15841000-15841000 | Inser                                                                                                                        tion - Frameshift |      |        | NULL       | NULL              | NULL         | 3495 | NULL  | NULL | NULL  | NUL                                                                                                                        L | 2020-02-24 15:02:17 |
					+--------------+-------+-----------+--------------------+--------------------------+------+-------------------+------                                                                                                                        ------------------+------+--------+------------+-------------------+--------------+------+-------+------+-------+----                                                                                                                        --+---------------------+
					1 row in set (0.00 sec)

					DELETE FROM knowledge_base_table WHERE genes='ETV6' AND coding = "c.725delA" and amino_acid_change = 'p.Asn242IlefsTer24' AND knowledge_id = 109065;

				</td>
				<td>					
					
				</td>
				<td><span class="dot dot-green"></span></td>
				<td></td>
			</tr>
			<tr class="completed not-discussed">
				<td>2/25/2020</td>
				<td>Feature Add: Increase length of MRN field in add_sample_log_book.  Issue reported by Bonnie</td>
				<td>
					previously length is at 12 however, outside MRN #s can have different MRN lengths.  Increase length to 20
				</td>
				<td>					
					
				</td>
				<td><span class="dot dot-green"></span></td>
				<td></td>
			</tr>
			<tr class="completed not-discussed">
				<td></td>
				<td>Feature Add: Add MPI field to Infotrack and the sample log book.</td>
				<td>
					
				</td>
				<td>					
					
				</td>
				<td><span class="dot dot-green"></span></td>
				<td></td>
			</tr>

						<tr class="completed not-discussed">
				<td>2/25/2020</td>
				<td>Bug Fix:  Why are there so many samples that show up in the make pool?</td>
				<td>
					The send out samples were also showing up.
				</td>
				<td>					
					
				</td>
				<td><span class="dot dot-green"></span></td>
				<td></td>
			</tr>
			<tr class="completed">
				<td></td>
				<td>It looks like sometimes OFA data transfer is initialized twice and then sometimes an error is written to md5 file</td>
				<td>
					string 'Error found while copying files.  md5s do not match<br><br>{"library_pool_id":"not found for the sample A_709-20_H1975_969_897_99 at \/media\/storage\/fs-ngs\/ion_torrent\/\/20200221_UPVFT_DAEJ00655_205_111\/temp\/A_709-20_H1975_969_897_99\/Variants\/A_709-20_H1975_969_897_99_v3 with extension -full.tsv or the vcf file with format *Filtered*.vcf","start_time":"47.96","finsh_time":"","status":"","elapsed_time":""}' (length=416)​
				</td>
				<td>					
					
				</td>
				<td><span class="dot dot-green"></span></td>
				<td></td>
			</tr>
			<tr class="completed not-discussed">
				<td>2/25/2020</td>
				<td>Feature Add: Remove done pool button.  Add a filter on homepage to hide or show based on 2 weeks.</td>
				<td>
					
				</td>
				<td>					
					
				</td>
				<td><span class="dot dot-green"></span></td>
				<td>f4689eb</td>
			</tr>
						<tr class="completed">
				<td>2/15/2020</td>
				<td>Feature Add: Finish pool_upload_data page. </td>
				<td>
					<ol>
						<li>Design:<span class="dot dot-green"></span></li>
						<li>List Risks:<span class="dot dot-yellow"></span></li>
						<li>Implementation:<span class="dot dot-yellow"></span></li>
						<li>Testing:<span class="dot dot-yellow"></span></li>
					</ol>
				</td>
				<td>					
					<ul>						
						<li>
							<ol>								
								<li>Need a class that loads log files and md5 files and checks everything is present and transfered fully. <span class="dot dot-green"></span></li>
								<li>Once transfered mark in library_pool_table (backup_status, backup_tsv_status??)<span class="dot dot-green"></span></li>
								<li>Decide if this should be manual or automatic data upload<span class="dot dot-green"></span></li>
								<li>Create run_info entry for each sample. <span class="dot dot-green"></span> Manual For now.  Need to test on live data first.</li>
								<li>Add a progress bar to update user of backup progress<span class="dot dot-green"></span> </li>								
								<li>If all md5s have passed for data backup show all samples so they can me uploaded use same format as homepage NGS table<span class="dot dot-green"></span>.</li>
								<li>Deactivate homepage for OFA upload<span class="dot dot-green"></span></li>
								<li>Update link to upload_NGS_data page including pool_chip_linker_id and library_pool_id<span class="dot dot-green"></span></li>
								<li>Update files types which will be uploaded in the upload_NGS_data page.  Update sample name of files.<span class="dot dot-green"></span></li>
								<li>Add ts_time_stamp to library_pool_table so it can be used in run_date<span class="dot dot-green"></span></li>
								<li>Before reporting fatal errors give the transfer at least 30 seconds.  Hide page for 30 seconds if $_GET loader_only=1 the page will never show.  The rainbow loader will be present instead.<span class="dot dot-green"></span></li>
								<li>Automatically fill in run_date and sample name in upload_NGS_data<span class="dot dot-green"></span></li>
								<li>Backup status bar is exceeding 100%. (this is occuring experiment 177 because of the space in the sample name)  It does not occur when the number of samples in the pool equals the number of samples being transfered.<span class="dot dot-green"></span></li>
								<li>Activate rainbow waiting page on all pages just made.  This will prevent errors from the user being impatient during API calls. <span class="dot dot-green"></span></li>
								<li>update upload_NGS_data page with new template.<span class="dot dot-green"></span></li>

								<li>update upload_NGS_data page for uploading of NGS tsv.<span class="dot dot-yellow"></span>
									<ul>
										<li>Unfortunately the tsv file downloaded from the IR website has different headers than the file pulled directly from the IR API. The TSV downloaded from the IR website will no longer be accepted.</li>
										<li>JS will now accept the IR variant tsv ['gene', 'coding', 'protein', '%_frequency', 'genotype', 'allele_coverage', 'coverage'].  File no longer has a header also.</li>
										<li>The sample name needs to match the file name.  This check also happens for the myeloid panel.</li>
										<li>Test with no variants found and low coverage found<span class="dot dot-purple"></span></li>
									</ul>
								</li>

								<li>update upload_NGS_data page for uploading of NGS coverage tsv.<span class="dot dot-yellow"></span>
									<ul>
										<li>The format of the coverage file is different.  Since I made it.  All coverage data is now available in one file.</li>
										<li>Columns are now "Gene", "Amplicon", "Sample", "Coverage", "chrom", "start", "stop" and accepted in JS and PHP (utils.CovFileTypeCorrect)</li>
										<li>JS filters to find if < 500 <span class="dot dot-purple"></span>  and sample name matches <span class="dot dot-purple"></span> low_amp_present it sets $_POST['not_found'] === not_found  </li>
									</ul>
								</li>
								<li>Include pool_chip_linker_id and library_pool_id in run_info_table<span class="dot dot-green"></span></li>	

								<li>Mark pool status as complete this will prevent double upload.<span class="dot dot-yellow"></span></li>
								
								<li>Mark QC as completed by user that submitted run_qc_status as a step in report building. <span class="dot dot-green"></span></li>
								
								<li>Show QC run stats in update report<span class="dot dot-green"></span></li>
								
								<li>Write a javascript function to keep on rechecking by refreshing page every 30 seconds if isset($_GET['reload']), $_GET['reload_count'] <= 10 times, and increment count $_GET['reload_count']++ .<span class="dot dot-yellow"></span></li>	

								<li>MD5 log ir_backup.tsv has a formating issue sometimes.  Example pool 74.  /media/storage/fs-ngs/dev/20190829_SAWBD_DAEJ00646_177_74/md5s/ir_backup.tsv Something is causing a lag in transfer. Please refer to error logs. However, a line with just one entry was added.  (experiment 177 has a sample called new because a sample is called 4450-18 new. With a space. recheck on multiple <span class="dot dot-yellow"></span></li>
								<li></li>
								<li></li>											
							</ol>							
						</li>
						<li>
							<ul>
								<li>while waiting for backup it needs a timeout.  I do not want to continually ping server which would cause a server crash</li>
								<li>Currently there will be no method if the user links the incorrect file.  Add checks to prevent them from linking the incorrect file. </li>
								<li>Any Spaces in sample name will cause problems in progress bar and maybe the md5 file.</li>
							</ul>
						</li>
						<li>
							<span class="dot dot-yellow"></span>
						</li>
						<li>Testing:<span class="dot dot-purple"></span>  Test with two chips.  Progress bar has numbers over 100% appearing because data does not match.  Need to test on real data</li>
					</ul>
				</td>
				<td><span class="dot dot-green"></span></td>
				<td></td>
			</tr>
			<tr class="completed">
				<td>3/4/2020</td>
				<td>redirect to https</td>
				<td>
					
				</td>
				<td>					
					
				</td>
				<td><span class="dot dot-green"></span></td>
				<td>92ba3cc</td>
			</tr>
			<tr class="completed">
				<td>3/4/2020</td>
				<td>I hate to ask because I know you have way too much on your plate...  but Bonnie suggested that we add two things to the new ngs visit email notifications.  She thinks it would be very helpful to have which test was ordered, and the Soft Path #.  I think she is working around not having this info on the notification, but we will be adding people to this task eventually and she's thinking about making it easier for them.  I probably don't want this at the end of your task list, but it doesn't have to be at the top either. </td>
				<td>
					3/4/2020 - sent an email to bonnie to see if new visit and report finished email works.<br>
				</td>
				<td>					
					
				</td>
				<td><span class="dot dot-green"></span></td>
				<td></td>
			</tr>
			<tr class="completed">
				<td></td>
				<td>Bug Fix: ?page=add_SNV_to_knowledge_base&genes=EGFR&coding=c.2235_2249delGGAATTAAGAGAAGC&amino_acid_change=p.Glu746_Ala750del&run_id=916&visit_id=945&patient_id=718&variant_filter=included (live page) loader will not turn off.</td>
				<td>
					
				</td>
				<td>					
					
				</td>
				<td><span class="dot dot-yellow"></span></td>
				<td></td>
			</tr>
			<tr class="completed">
				<td>2/6/2020</td>
				<td>Feature Add: Disable data entry for pool once library_pool_table has been filled out </td>
				<td>
					<ol>
						<li>Design:<span class="dot dot-green"></span></li>
						<li>List Risks:<span class="dot dot-green"></span></li>
						<li>Implementation:<span class="dot dot-green"></span></li>
						<li>Testing:<span class="dot dot-green"></span></li>
					</ol>
				</td>
				<td>					
					<ol>
						
						<li>
							Forms will only provide submit buttons if $step_status = $poolStepTracker->StepStatus($completed_steps, 'pool_qc_backup_data');<br>
							<br>
							if ($page === 'pool_upload_data' || $step_status === 'not_complete')
							
						</li>
						<li>
							If the user does not submit qc data correctly for pool qc backup then they will not be able to go back and edit that.
						</li>
						<li><span class="dot dot-green"></span></li>
						<li>Tested on pool 74</li>
					</ol>
				</td>
				<td><span class="dot dot-green"></span></td>
				<td></td>
			</tr>
			
			<tr class="completed">
				<td></td>
				<td>Feature Add:  Add at least 4 Identifiers to backup folder name and sample name</td>
				<td>
					
				</td>
				<td>					
					<ol>
						<li>
							backup folder format (5 identifiers)
							<ul>
								<li>Format: rundate_runid_chipBarcode_experimentid_(library_pool_id)</li>
								<li>All Fields are obtained via the thermofisher API using the result name chosen the pool_select_results page</li>
								<li>library_pool_id is added to make the folder unique for this pool just encase data is run again with a different pool.</li>
							</ul>
						</li>
						<li>
							sample name (6 identifiers)
							<ul>
								<li>Format: chip_MD#_softlab#_(visit_id)_(visit_in_pool_id)_(pool_chip_linker_id)</li>
								<li>chip: Chip is added because Yang Wang from thermofisher mentioned on March 5th, 2019 make sure the sample name always starts with a letter rather than a digit. (2 per pool)</li>
								<li>MD#: assigned by infotrack at login (Unique)</li>
								<li>softlab#: assigned by soft added to infotrack at login (Unique)</li>
								<li>visit_id: Assigned by infotrack (Unique)</li>
								<li>visits_in_pool_id: Assigned by infotrack (Unique)</li>
								<li>pool_chip_linker_id:Assigned by infotrack (1 per pool) </li>
							</ul>
						</li>
						<li>
							control name (5 identifiers)
							<ul>
								<li>Format: chip_ControlName_version-use_(visit_id)_(visit_in_pool_id)_(pool_chip_linker_id)</li>
								<li>chip: Chip is added because Yang Wang from thermofisher mentioned on March 5th, 2019 make sure the sample name always starts with a letter rather than a digit. (2 per pool)</li>
								<li>controlName: assigned by infotrack at login (NOT Unique)</li>
								<li>version-use: Added by the user.  This is why I do not like it as a unquie field.  Need to put in measures to make sure it is unquie. (possibly unique but night have format issues)</li>
								<li>visit_id: Assigned by infotrack (Unique)</li>
								<li>visits_in_pool_id: Assigned by infotrack (Unique)</li>
								<li>pool_chip_linker_id:Assigned by infotrack (1 per pool)</li>
							</ul>
						</li>
					</ol>
				</td>
				<td><span class="dot dot-green"></span></td>
				<td></td>
			</tr>
			<tr class="completed">
				<td></td>
				<td>Bug Fix: pool_add_barcode if barcode 30 used as starting val then there's errors refer to pool 44.  Check if it restarts between first barcode set and second barcode set.</td>
				<td>
					<ol>
						<li>Design:<span class="dot dot-green"></span></li>
						<li>List Risks:<span class="dot dot-yellow"></span></li>
						<li>Implementation:<span class="dot dot-yellow"></span></li>
						<li>Testing:<span class="dot dot-yellow"></span></li>
					</ol>
				</td>
				<td>					
					<ul>
						<li>Changed to only have IonXpress 1-32.  This way it is easy to have a start and stop since the lot numbers are recorded by the chef anyway.<span class="dot dot-green"></span></li>
						<li>Add last used barcode 
							<br><br>
							SELECT * FROM pool_chip_linker_table pclt WHERE pclt.ngs_panel_id = 1 AND pclt.pool_chip_linker_id <> 63 ORDER BY pclt.time_stamp DESC LIMIT 1;
						</li>
					</ul>
				</td>
				<td><span class="dot dot-green"></span></td>
				<td></td>
			</tr>

			<tr class="completed not-discussed">
				<td>2/14/2020</td>
				<td>Bug Fix: add_library_pool form allows empty to be submitted and more than max to be submitted.</td/>
				<td>
					
				</td>
				<td>					
					
				</td>
				<td><span class="dot dot-green"></span></td>
				<td>eefdead</td>
			</tr>
			<tr class="completed not-discussed">
				<td>2/14/2020</td>
				<td>Bug Fix: chip A being added to myeloid panel. Reported by John.</td/>
				<td>
					
				</td>
				<td>					
					
				</td>
				<td><span class="dot dot-green"></span></td>
				<td>eefdead</td>
			</tr>
			<tr class="completed">
				<td>3/2/2020</td>
				<td>Urgent: New Message for Report # 997 "For the U2AF1 mutation please change the cosmic number to 1235015"</td>
				<td>
					T
				</td>
				<td>		
					Backup database first
					<br><br>
					$ sudo su<br>
					# sudo mysqldump -u root urmc_reporter_db --single-transaction --quick --lock-tables=false > /var/www/urmc_reporter_data/db_backups/urmc_reporter_db_2020_03_02.sql<br>
					# exit
					<br><br>
					Access database<br>
					$ sudo mysql -u root<br>
					MariaDB [(none)]> use urmc_reporter_db;<br>
					<br><br>			
					Since reports are generated every time from the database we need to make sure we are not going to change a report that was already signed out.  So we need to check to see if the variant is was already used for a different run_id.
					<br><br>
					SELECT ovt.observed_variant_id AS ov_id, ovt.run_id, vt.visit_id, ovt.genes, ovt.coding, ovt.amino_acid_change AS aa_change FROM observed_variant_table ovt LEFT JOIN visit_table vt ON vt.run_id = ovt.run_id WHERE ovt.genes ="U2AF1" AND ovt.coding = "c.467G>A" AND ovt.amino_acid_change = "p.Arg156His";
					<br><br>
					+-------+--------+----------+-------+----------+-------------+<br>
					| ov_id | run_id | visit_id | genes | coding   | aa_change   |<br>
					+-------+--------+----------+-------+----------+-------------+<br>
					| 16668 |    967 |      997 | U2AF1 | c.467G>A | p.Arg156His |<br>
					+-------+--------+----------+-------+----------+-------------+<br>
					1 row in set (0.00 sec)<br>

					<br><br>
					Since we only have one row the cosmic ID in the knowledge_base_table can be updated without causing any harm to previously signed out reports.  (DO NOT CONTINUE IF FOUND IN MULTIPLE REPORTS.  All of the reports will have to be sent to revise mode so the original report will be save in a hard copy.)Now find the variant in the knowledge_base_table
					<br><br>
					SELECT kbt.knowledge_id AS k_id, kbt.genes, kbt.coding, kbt.amino_acid_change AS aa_change, kbt.cosmic FROM knowledge_base_table kbt WHERE kbt.genes ="U2AF1" AND kbt.coding = "c.467G>A" AND kbt.amino_acid_change = "p.Arg156His";
					<br><br>
					+-------+-------+----------+-------------+---------+<br>
					| k_id  | genes | coding   | aa_change   | cosmic  |<br>
					+-------+-------+----------+-------------+---------+<br>
					| 25037 | U2AF1 | c.467G>A | p.Arg156His | 1235014 |<br>
					+-------+-------+----------+-------------+---------+<br>
					1 row in set (0.01 sec)<br>
					<br><br>
					Ensure there's only one.  Then change the cosmic number.  Be very certain that you are selecting the correct variant in the knowlege_base_table.  You could end up inadvertently changing many or all entries.
					<br><br>
					UPDATE knowledge_base_table SET cosmic = 1235015 WHERE genes ="U2AF1" AND coding = "c.467G>A" AND amino_acid_change = "p.Arg156His" AND knowledge_id = 25037 AND cosmic = 1235014; 
				</td>
				<td><span class="dot dot-green"></span></td>
				<td></td>
			</tr>
			<tr class="completed">
				<td></td>
				<td>Feature Add: add_visit form.  Add a control_step_regulator_table to switch field name depending on control.  For instance AcroMetrix should be control lot # and the rest should be control version.  Since version is now a requirement for all controls make it required if a control is being added to the add_visit form.</td>
				<td>
					<ol>
						<li>Design:<span class="dot dot-green"></span></li>
						<li>List Risks:<span class="dot dot-green"></span></li>
						<li>Implementation:<span class="dot dot-green"></span></li>
						<li>Testing:<span class="dot dot-green"></span></li>
					</ol>
				</td>
				<td>					
					<ol>
						<li>
							
						</li>
						<li>
							There might be versions already being used.  Do Not display last used version first for lot # types if it starts with a v. 
						</li>
						<li></li>
						<li></li>
					</ol>
				</td>
				<td><span class="dot dot-green"></span></td>
				<td></td>
			</tr>
			<tr class="completed">
				<td></td>
				<td>Feature Add: Write a python script that starts running when pool_qc_backup_data pages starts called thermo_make_cov_files.py. To copy coverage files from the torrent server to fs-ngs drive.</td>
				<td>
					<ol>
						<li>Design:<span class="dot dot-green"></span></li>
						<li>List Risks:<span class="dot dot-green"></span></li>
						<li>Implementation:<span class="dot dot-green"></span></li>
						<li>Testing:<span class="dot dot-green"></span></li>
					</ol>
				</td>
				<td>					
					<ol>
						
						<li>
							<ul>
								<li>For each chip this script uses the bcmatrix file, the a select_dump json file, and a bed file of all of the amplicon genome locations (built into Infotrack) to make two coverage files.  all_coverage_amplicons and low_coverage_amplicons.  These files will be used in pool_upload_data page.</li>
								<li>Activate python script thermo_make_cov_files.py to run in the background under class function callPythonToGetAmpliconCov</li>
								
							</ul>							
						</li>
						<li>
							<ul>
								<li>Make sure the Coverage Plugin ran if it didn't run report error directly to user.</li>
								<li>Make sure bcmatrix.xls and the newly written select_dump json exists.  Select dump consists of all the API calls made to the Torrent Server in the class.thermo_backup</li>
							</ul>
						</li>
						<li>Implementation:<span class="dot dot-green"></span></li>
						<li>Tested on multiple runs including ones where coveragePlugin didn't run and error reporting works.</li>
					</ol>
				</td>
				<td><span class="dot dot-green"></span></td>
				<td></td>
			</tr>
			<tr class="completed">
				<td>1/29/2020</td>
				<td>Bug Fix reported by Saide.  When I updated pushed the new feature all sample turnaround in audit I pushed a little too much and the live database did not match the view present.  This resulted in myeloid pool looking for a field not present in the view pending_samples_substring_order_num_vw.  I updated the view and added a button to unselect all samples in the add_library_pool since there's such a backlog in the myeloid samples.</td>
				<td>
					
				</td>
				<td>					
					
				</td>
				<td><span class="dot dot-green"></span></td>
				<td>b35185a</td>
			</tr>
			<tr class="completed">
				<td>1/28/2020</td>
				<td>Feature add: Requested by Zoltan provide turnaround time for each sample in the audit </td>
				<td>
					
				</td>
				<td>					
					
				</td>
				<td><span class="dot dot-green"></span></td>
				<td></td>
			</tr>
			<tr class="completed">
				<td></td>
				<td>Feature Add: Write a python script that starts after qc is passed for chip to transfer bam, vcf, and variant tsv files to fs-ngs drive.  Script called thermo_backup_ion_reporter_run.py</td>
				<td>
					<ol>
						<li>Design:<span class="dot dot-green"></span></li>
						<li>List Risks:<span class="dot dot-green"></span></li>
						<li>Implementation:<span class="dot dot-green"></span></li>
						<li>Testing:<span class="dot dot-green"></span></li>
					</ol>
				</td>
				<td>					
					<ol>
						
						<li>
							<ul>
								<li>Run thermo_backup_ion_reporter_run.py per chip.</li>
								<li>Make md5file class to check md5s and write an output file of md5s.  This file will be used in the pool_upload_data page to see if data copying correctly and if everything is finished.</li>
							</ul>							
						</li>
						<li>
							<ul>
								<li>A list of samples will be provided to the script.  These names are pulled from the coverage plugin because names might not be historically useful in infotrack and this will ensure the most accurate data.  If the coverage plugin is not run the user will be notified and the pool_select_results will not be able to be submitted.</li>
								<li>Make sure bcmatrix.xls and the newly written select_dump json exists.  Select dump consists of all the API calls made to the Torrent Server in the class.thermo_backup</li>
							</ul>
						</li>
						<li>
							<ul>
								<li>
									There's an error unziping from APP but not command line via my login in. Maybe the APP does not have same access that I do.  (warning) cannot set modif./access times
          						Operation not permitted <span class="dot dot-purple"></span>.  Since this is just a warning I made a work around to just test for md5s and added a tracker file to keep track of errors outside of the log file. 
          						</li>
          						<li>It is absolutely necessary to make a unique sample name otherwise the sample will not be able to found in the IR.  Some how technicians were by passing using my software and naming samples Pos and Neg.  Huijie continues to do this because she doesn't like the way the plate is set up.  I was told from the Myeloid team that they need the plate set up Negative control then positive control.  This doesn't really make sense to me experimentally but the myeloid and OFA needs to be the same and the myeloid is the live test. <span class="dot dot-purple"></span></li>
          						<li>Any spaces in the sample name really messes up the IR and we might me linking samples that do not belong to each other.  I put in a fix to replace spaces with _ if found at this stage.  If my software is used there is no way to introduce spaces however, if the technicians continue to go rogue there might be problems. <span class="dot dot-purple"></span></li>
          						<li>ir_backup_tracker.tsv file:  This file keeps track of start time status and elapsed time per chip.  Write errors to tracker file.</li>
							</ul>
						</li>
						<li>Add as a variable library_pool_id so it can be added to tracker file that keeps track of start time and appends end time and if everything passed.  Place in md5 folder.</li>
						<li>After testing data backup and md5 checking takes about 2.5 - 8 minutes per chip.</li>
					</ol>
				</td>
				<td><span class="dot dot-green"></span></td>
				<td></td>
			</tr>
			<tr class="completed">
				<td></td>
				<td>Feature Add: Finish submit of pool_qc_backup_data</td>
				<td>
					<ol>
						<li>Design:<span class="dot dot-green"></span></li>
						<li>List Risks:<span class="dot dot-green"></span></li>
						<li>Implementation:<span class="dot dot-green"></span></li>
						<li>Testing:<span class="dot dot-green"></span></li>
					</ol>
				</td>
				<td>					
					<ol>
						
						<li>
							<ul>
								<li>Activate python script thermo_make_cov_files.py to run in the background.<span class="dot dot-green"></span> (running under class function callPythonToGetAmpliconCov)</li>
								<li>Write python script thermo_backup_ion_reporter_run.py This program will pull the bam and vcf files from IR and add md5s to the backup folder. <span class="dot dot-green"></span></li>
								<li>Upon submit start thermo_backup_ion_reporter_run.py.</li>
								<li>Keep track separately of user that approved qc in library_pool_table run_qc_status_user_id.  (CREATE FIELD)</li>
								<li>Adjust form if only one chip is being used with pool</li>
							</ul>
							
						</li>
						<li>
							<ul>
								<li>This assumes IR is completed.  Need to figure out a way to check if this is true.</li>

							</ul>
						</li>
						<li>Implementation:<span class="dot dot-green"></span></li>
						<li>
							Tested pool select results and pool_qc_backup on pool 23 and reports 177, 178.<br>
							Tested pool select results and pool_qc_backup on pool 24 and reports 171, 173.
						</li>
					</ol>
				</td>
				<td><span class="dot dot-green"></span></td>
				<td></td>
			</tr>
			<tr class="completed">
				<td></td>
				<td>Edit request from Nufatt: Please correct the diluted concentration on 355.20 to 25.7ng/ul.  When or if ever are we going to be able to correct on our own?</td>
				<td>
					
				</td>
				<td>					
					Unfortunately I'm swamped with tasks and only work for molecular part-time.  This is on my pending list however, it is towards the bottom of the list because of priority of tasks.  My task list is currently 15 months long and it increases at a greater rate than I can remove tasks.  So possibly I will never get to this if they do not get me help.

					<b>Why are there multiple additions of extraction to extraction_log_table for the same extraction log id?</b><br><br>
					MariaDB [urmc_reporter_db]> SELECT * FROM ordered_test_table WHERE sample_log_book_id = 3451;
					+-----------------+---------+--------------------+----------------------+----------+---------------+-----------------                                                                                                                        -+--------------+--------------+----------------------+------------+-------------+--------------------------+--------                                                                                                                        -----+---------------------+--------------------+-------------+
					| ordered_test_id | user_id | orderable_tests_id | previous_positive_id | visit_id | soft_path_num | outside_case_num                                                                                                                         | consent_info | consent_date | consent_for_research | start_date | finish_date | expected_turnaround_time | test_st                                                                                                                        atus | time_stamp          | sample_log_book_id | test_result |
					+-----------------+---------+--------------------+----------------------+----------+---------------+-----------------                                                                                                                        -+--------------+--------------+----------------------+------------+-------------+--------------------------+--------                                                                                                                        -----+---------------------+--------------------+-------------+
					|            6979 |      21 |                 11 |                 NULL |     NULL |               |                                                                                                                                          |              | 0000-00-00   |                      | 2020-01-27 | 2020-01-27  |                     NULL | complet                                                                                                                        e    | 2020-01-27 12:21:02 |               3451 |             |
					+-----------------+---------+--------------------+----------------------+----------+---------------+-----------------                                                                                                                        -+--------------+--------------+----------------------+------------+-------------+--------------------------+--------                                                                                                                        -----+---------------------+--------------------+-------------+
					1 row in set (0.01 sec)

					MariaDB [urmc_reporter_db]> SELECT * FROM extraction_log_table WHERE ordered_test_id = 6979;
					+-------------------+---------+-----------------+-------------------+------------+----------------+----------------+-                                                                                                                        --------------------+----------------+--------+
					| extraction_log_id | user_id | ordered_test_id | extraction_method | stock_conc | dilution_exist | dilutions_conc |                                                                                                                         time_stamp          | times_dilution | purity |
					+-------------------+---------+-----------------+-------------------+------------+----------------+----------------+-                                                                                                                        --------------------+----------------+--------+
					|              3215 |       9 |            6979 | NULL              |      834.8 | yes            |           35.7 |                                                                                                                         2020-01-27 15:59:43 |           NULL |      2 |
					|              3216 |       9 |            6979 | NULL              |      834.8 | yes            |           25.7 |                                                                                                                         2020-01-27 16:00:19 |           NULL |      2 |
					|              3217 |       9 |            6979 | NULL              |      834.8 | yes            |           25.7 |                                                                                                                         2020-01-27 16:01:01 |           NULL |      2 |
					+-------------------+---------+-----------------+-------------------+------------+----------------+----------------+-                                                                                                                        --------------------+----------------+--------+
					3 rows in set (0.00 sec)

					MariaDB [urmc_reporter_db]> SELECT * FROM users_performed_task_table WHERE ref_table = 'extraction_log_table' AND ref_id = 3215;
					+-------------------------+---------+---------+--------+----------------------+----------------+----------------+---------------------+
					| users_performed_task_id | user_id | task_id | ref_id | ref_table            | date_performed | time_performed | time_stamp          |
					+-------------------------+---------+---------+--------+----------------------+----------------+----------------+---------------------+
					|                   10430 |       9 |       3 |   3215 | extraction_log_table | 2020-01-27     | 00:00:00       | 2020-01-27 15:59:43 |
					+-------------------------+---------+---------+--------+----------------------+----------------+----------------+---------------------+
					1 row in set (0.00 sec)

					MariaDB [urmc_reporter_db]> SELECT * FROM users_performed_task_table WHERE ref_table = 'extraction_log_table' AND ref_id = 3216;
					+-------------------------+---------+---------+--------+----------------------+----------------+----------------+---------------------+
					| users_performed_task_id | user_id | task_id | ref_id | ref_table            | date_performed | time_performed | time_stamp          |
					+-------------------------+---------+---------+--------+----------------------+----------------+----------------+---------------------+
					|                   10431 |       9 |       3 |   3216 | extraction_log_table | 2020-01-27     | 00:00:00       | 2020-01-27 16:00:19 |
					+-------------------------+---------+---------+--------+----------------------+----------------+----------------+---------------------+
					1 row in set (0.01 sec)

					MariaDB [urmc_reporter_db]> SELECT * FROM users_performed_task_table WHERE ref_table = 'extraction_log_table' AND ref_id = 3217;
					+-------------------------+---------+---------+--------+----------------------+----------------+----------------+---------------------+
					| users_performed_task_id | user_id | task_id | ref_id | ref_table            | date_performed | time_performed | time_stamp          |
					+-------------------------+---------+---------+--------+----------------------+----------------+----------------+---------------------+
					|                   10432 |      21 |       3 |   3217 | extraction_log_table | 2020-01-27     | 00:00:00       | 2020-01-27 16:01:01 |
					|                   10433 |       9 |       3 |   3217 | extraction_log_table | 2020-01-27     | 00:00:00       | 2020-01-27 16:01:01 |
					+-------------------------+---------+---------+--------+----------------------+----------------+----------------+---------------------+

					MariaDB [urmc_reporter_db]> SELECT * FROM task_table;
					+---------+---------+-------------------------+---------------------+
					| task_id | user_id | task                    | time_stamp          |
					+---------+---------+-------------------------+---------------------+
					|       1 |       1 | extraction_aliquoted    | 2019-05-20 14:51:04 |
					|       2 |       1 | pk_digestion            | 2019-05-20 14:51:16 |
					|       3 |       1 | extraction_performed_by | 2019-05-20 14:51:09 |
					|       4 |       1 | wet_bench               | 2019-05-21 14:45:40 |
					|       5 |       1 | analysis                | 2019-05-21 14:45:40 |
					|       6 |       1 | Nanodrop                | 2019-11-05 16:01:10 |
					+---------+---------+-------------------------+---------------------+

					MariaDB [urmc_reporter_db]> SELECT * FROM users_performed_task_table WHERE ref_table = 'ordered_test_table' AND ref_id = 6979;
					Empty set (0.00 sec)


				</td>
				<td><span class="dot dot-green"></span></td>
				<td></td>
			</tr>
			<tr class="completed">
				<td></td>
				<td>Feature Add: Add full_sample_name to visits_in_pool_table.</td>
				<td>
					<ol>
						<li>Design:<span class="dot dot-green"></span></li>
						<li>List Risks:<span class="dot dot-green"></span></li>
						<li>Implementation:<span class="dot dot-green"></span></li>
						<li>Testing:<span class="dot dot-green"></span></li>
					</ol>
				</td>
				<td>					
					<ol>
						
						<li>
							<ul>
								<li>This will be useful to ensure sample is correct for automatic upload.</li>
								<li>Added in the while creating the pool under add_library_pool</li>
								
							</ul>							
						</li>
						<li>
							Need to set varchar higher than necessary with numbers increasing for ids this will get longer and longer.  Set varchar to 100.
						</li>
						<li>Implementation:<span class="dot dot-green"></span></li>
						<li>Testing:<span class="dot dot-green"></span> Tested using pool 73</li>
					</ol>
				</td>
				<td><span class="dot dot-green"></span></td>
				<td></td>
			</tr>
			<tr class="completed needs-documentation">
				<td>2/6/2020</td>
				<td>Urgent: New Message for Report # 889 "Samantha, Can you correct the COSMIC number for TP53 c.422G>A mutation? Please change 43708 to 131470. Thanks, Paul"</td>
				<td>
					This process needs to be documented
				</td>
				<td>		
					Backup database first
					<br><br>
					$ sudo su<br>
					# sudo mysqldump -u root urmc_reporter_db --single-transaction --quick --lock-tables=false > /var/www/urmc_reporter_data/db_backups/urmc_reporter_db_2020_02_06.sql<br>
					# exit
					<br><br>
					Access database<br>
					$ sudo mysql -u root<br>
					MariaDB [(none)]> use urmc_reporter_db;<br>
					<br><br>			
					Since reports are generated every time from the database we need to make sure we are not going to change a report that was already signed out.  So we need to check to see if the variant is was already used for a different run_id.
					<br><br>
					SELECT ovt.observed_variant_id AS ov_id, ovt.run_id, vt.visit_id, ovt.genes, ovt.coding, ovt.amino_acid_change AS aa_change FROM observed_variant_table ovt LEFT JOIN visit_table vt ON vt.run_id = ovt.run_id WHERE ovt.genes ="TP53" AND ovt.coding = "c.422G>A" AND ovt.amino_acid_change = "p.Cys141Tyr";
					<br><br>
					+-------+--------+----------+-------+----------+-------------+<br>
					| ov_id | run_id | visit_id | genes | coding   | aa_change   |<br>
					+-------+--------+----------+-------+----------+-------------+<br>
					| 14683 |    884 |      889 | TP53  | c.422G>A | p.Cys141Tyr |<br>
					+-------+--------+----------+-------+----------+-------------+<br>
					1 row in set (0.00 sec)<br>
					<br><br>
					Since we only have one row the cosmic ID in the knowledge_base_table can be updated without causing any harm to previously signed out reports.  (DO NOT CONTINUE IF FOUND IN MULTIPLE REPORTS.  All of the reports will have to be sent to revise mode so the original report will be save in a hard copy.)Now find the variant in the knowledge_base_table
					<br><br>
					SELECT kbt.knowledge_id AS k_id, kbt.genes, kbt.coding, kbt.amino_acid_change AS aa_change, kbt.cosmic FROM knowledge_base_table kbt WHERE kbt.genes ="TP53" AND kbt.coding = "c.422G>A" AND kbt.amino_acid_change = "p.Cys141Tyr";
					<br><br>
					+------+-------+----------+-------------+--------+<br>
					| k_id | genes | coding   | aa_change   | cosmic |<br>
					+------+-------+----------+-------------+--------+<br>
					|  447 | TP53  | c.422G>A | p.Cys141Tyr | 43708  |<br>
					+------+-------+----------+-------------+--------+<br>
					1 row in set (0.00 sec)<br>
					<br><br>
					Ensure there's only one.  Then change the cosmic number.  Be very certain that you are selecting the correct variant in the knowlege_base_table.  You could end up inadvertently changing many or all entries.
					<br><br>
					UPDATE knowledge_base_table SET cosmic = 131470 WHERE genes ="TP53" AND coding = "c.422G>A" AND amino_acid_change = "p.Cys141Tyr" AND knowledge_id = 447 AND cosmic = 43708; 
				</td>
				<td><span class="dot dot-green"></span></td>
				<td>30 mins</td>
			</tr>
			<tr class="completed">
				<td>2/18/2020</td>
				<td>Urgent: "For the TP53 mutation the COSMIC number is changed to 308311" Report # 938</td>
				<td>
					
				</td>
				<td>		
					Backup database first
					<br><br>
					$ sudo su<br>
					# sudo mysqldump -u root urmc_reporter_db --single-transaction --quick --lock-tables=false > /var/www/urmc_reporter_data/db_backups/urmc_reporter_db_2020_02_07.sql<br>
					# exit
					<br><br>
					Access database<br>
					$ sudo mysql -u root<br>
					MariaDB [(none)]> use urmc_reporter_db;<br>
					<br><br>			
					Since reports are generated every time from the database we need to make sure we are not going to change a report that was already signed out.  So we need to check to see if the variant is was already used for a different run_id.
					<br><br>
					SELECT ovt.observed_variant_id AS ov_id, ovt.run_id, vt.visit_id, ovt.genes, ovt.coding, ovt.amino_acid_change AS aa_change FROM observed_variant_table ovt LEFT JOIN visit_table vt ON vt.run_id = ovt.run_id WHERE ovt.genes ="TP53" AND ovt.coding = "c.395A>G" AND ovt.amino_acid_change = "p.Lys132Arg";
					<br><br>
					+-------+--------+----------+-------+----------+-------------+<br>
					| ov_id | run_id | visit_id | genes | coding   | aa_change   |<br>
					+-------+--------+----------+-------+----------+-------------+<br>
					| 15723 |    929 |      938 | TP53  | c.395A>G | p.Lys132Arg |<br>
					+-------+--------+----------+-------+----------+-------------+<br>
					1 row in set (0.00 sec)

					<br><br>
					Now change the cosmic id int he knowledge base
					<br><br>
					SELECT kbt.knowledge_id AS k_id, kbt.genes, kbt.coding, kbt.amino_acid_change AS aa_change, kbt.cosmic FROM knowledge_base_table kbt WHERE kbt.genes ="TP53" AND kbt.coding = "c.395A>G" AND kbt.amino_acid_change = "p.Lys132Arg";
					<br><br>
					+------+-------+----------+-------------+--------+<br>
					| k_id | genes | coding   | aa_change   | cosmic |<br>
					+------+-------+----------+-------------+--------+<br>
					|  533 | TP53  | c.395A>G | p.Lys132Arg | 11582  |<br>
					+------+-------+----------+-------------+--------+<br>
					1 row in set (0.01 sec)<br>


					<br><br>
					Ensure there's only one.  Then change the cosmic number.  Be very certain that you are selecting the correct variant in the knowlege_base_table.  You could end up inadvertently changing many or all entries.
					<br><br>
					UPDATE knowledge_base_table SET cosmic = 308311 WHERE genes ="TP53" AND coding = "c.395A>G" AND amino_acid_change = "p.Lys132Arg" AND knowledge_id = 533; 
				</td>
				<td><span class="dot dot-green"></span></td>
				<td>30 mins</td>
			</tr>
			<tr class="completed">
				<td>2/7/2020</td>
				<td>Urgent: New Message for Report # 906 "The STAG2 mutation does have a COSMIC entry, COSM4167077. Can we add this? Thanks, Paul"</td>
				<td>
					
				</td>
				<td>		
					Backup database first
					<br><br>
					$ sudo su<br>
					# sudo mysqldump -u root urmc_reporter_db --single-transaction --quick --lock-tables=false > /var/www/urmc_reporter_data/db_backups/urmc_reporter_db_2020_02_07.sql<br>
					# exit
					<br><br>
					Access database<br>
					$ sudo mysql -u root<br>
					MariaDB [(none)]> use urmc_reporter_db;<br>
					<br><br>			
					Since reports are generated every time from the database we need to make sure we are not going to change a report that was already signed out.  So we need to check to see if the variant is was already used for a different run_id.
					<br><br>
					SELECT ovt.observed_variant_id AS ov_id, ovt.run_id, vt.visit_id, ovt.genes, ovt.coding, ovt.amino_acid_change AS aa_change FROM observed_variant_table ovt LEFT JOIN visit_table vt ON vt.run_id = ovt.run_id WHERE ovt.genes ="STAG2" AND ovt.coding = "c.3407_3410dupATAG" AND ovt.amino_acid_change = "p.Ser1137ArgfsTer2";
					<br><br>
				+-------+--------+----------+-------+--------------------+--------------------+<br>
				| ov_id | run_id | visit_id | genes | coding             | aa_change          |<br>
				+-------+--------+----------+-------+--------------------+--------------------+<br>
				|   648 |    207 |      144 | STAG2 | c.3407_3410dupATAG | p.Ser1137ArgfsTer2 |<br>
				| 14699 |    891 |      906 | STAG2 | c.3407_3410dupATAG | p.Ser1137ArgfsTer2 |<br>
				+-------+--------+----------+-------+--------------------+--------------------+<br>

					<br><br>
					Since we have two reports with this mutation changing this mutation without revising the report with visit id 144 will cause the change to occur in that report.  This would result in us having a report in infotrack that differs from a report in e record.  Now we need to search for this signed out report using NGS -> search for previously observed variants.  Once the report is found go to the completed reports page with revise permissions and find the report and revise the report.
					<br><br>
					Add as comment something like: This report is was revised because on 2/7/2020 the cosmic number for the STAG2 mutation was found and this report was already signed out without the updated COSMIC number.  By revising the report the 1st report will be the report that was added to the patient record and the newest report will include the updated cosmic ID
					<br><br>
					Now change the cosmic id int he knowledge base
					<br><br>
					SELECT kbt.knowledge_id AS k_id, kbt.genes, kbt.coding, kbt.amino_acid_change AS aa_change, kbt.cosmic FROM knowledge_base_table kbt WHERE kbt.genes ="STAG2" AND kbt.coding = "c.3407_3410dupATAG" AND kbt.amino_acid_change = "p.Ser1137ArgfsTer2";
					<br><br>
					+-------+-------+--------------------+--------------------+--------+
					| k_id  | genes | coding             | aa_change          | cosmic |
					+-------+-------+--------------------+--------------------+--------+
					| 85472 | STAG2 | c.3407_3410dupATAG | p.Ser1137ArgfsTer2 |        |
					+-------+-------+--------------------+--------------------+--------+

					<br><br>
					Ensure there's only one.  Then change the cosmic number.  Be very certain that you are selecting the correct variant in the knowlege_base_table.  You could end up inadvertently changing many or all entries.
					<br><br>
					UPDATE knowledge_base_table SET cosmic = 4167077 WHERE genes ="STAG2" AND coding = "c.3407_3410dupATAG" AND amino_acid_change = "p.Ser1137ArgfsTer2" AND knowledge_id = 85472; 
				</td>
				<td><span class="dot dot-green"></span></td>
				<td>30 mins</td>
			</tr>
			<tr class="completed">
				<td>2/6/2020</td>
				<td>Feature Add: pool_qc_backup_data provides a backup location via the mapped location on pathlamp for instance  /media/storage/fs-ngs/dev/20190829_SAWBD_DAEJ00646_177_63.  Switch this to a format the tech will understand N: ... </td>
				<td>
					<ol>
						<li>Design:<span class="dot dot-green"></span></li>
						<li>List Risks:<span class="dot dot-green"></span></li>
						<li>Implementation:<span class="dot dot-green"></span></li>
						<li>Testing:<span class="dot dot-green"></span></li>
					</ol>
				</td>
				<td>					
					The backup locations now change depending on devs or live.  Then I added a function to the api_utils class to convert from pathlamp location to N: drive location when I display the location to the user.
				</td>
				<td><span class="dot dot-green"></span></td>
				<td></td>
			</tr>
			<tr class="completed">
				<td>2/10/2020</td>
				<td>Data Update:Sam,
				<br><br>
				Can you remove the following whenever you get a moment off the pre ngs samples on the myeloid -
				<br><br>
				#866 sensd v3-5
				<br><br>
				#909 sensd v4-5
				<br><br>
				#910 na19240 v1-1
				<br><br>
				I don't know why I did these duplicates. I don't even remember doing it.
				<br><br>
				Thank you.
				<br><br>
				Suni
				</td>
				<td>
					
				</td>
				<td>					
					
				</td>
				<td><span class="dot dot-green"></span></td>
				<td></td>
			</tr>
			<tr class="completed">
				<td>2/10/2020</td>
				<td>Feature Add: Add a timeline of mutations if found patient was already seen for NGS report
				</td>
				<td>
					
				</td>
				<td>					
					SELECT vt.patient_id, COUNT(*), pt.first_name, pt.last_name FROM visit_table vt LEFT JOIN patient_table pt ON pt.patient_id = vt.patient_id WHERE vt.control_type_id = 0 OR vt.control_type_id IS NULL GROUP BY vt.patient_id HAVING COUNT(*) > 1;
					<br><br>
					SELECT ovt.*, vt.time_stamp FROM visit_table vt LEFT JOIN observed_variant_table ovt ON vt.run_id = ovt.run_id WHERE vt.patient_id = 30;
					<br><br>
					Link to previous reports
					<br><br>
					?page=download_report&visit_id=906&patient_id=127&run_id=891
				</td>
				<td><span class="dot dot-green"></span></td>
				<td>7fea729</td>
			</tr>

			<tr class="completed">				

				<td>1/2/2020</td>
				<td>Fix a bug where MOl number did not restart at 1 with the new year</td>
				<td>
					Md# is automatically generated in utils.getNextMolNum().  NaN was not accounted for.  	
				</td>
				<td></td>
				<td><span class="dot dot-green"></span></td>
				<td></td>
			</tr>
			<tr class="completed">				

				<td>10/31/2019</td>
				<td>Add Sample Log Book has sample type toggle CAP sample. Adds first name: Cap, last name: Sample. It will make soft path number now equal to a required field of CAP ID. Also the last MRN number used for a CAP sample will be added to MRN. </td>
				<td>	
				</td>
				<td></td>
				<td><span class="dot dot-green"></span></td>
				<td></td>
			</tr>
			<tr class="completed">				

				<td>11/5/2019</td>
				<td>Fixed bug forms not unlocking for date-time-picker. Reported by Suni.</td>
				<td>	
				</td>
				<td></td>
				<td><span class="dot dot-green"></span></td>
				<td></td>
			</tr>
			<tr class="completed">				

				<td>11/5/2019</td>
				<td>To reduce data entry errors. Multi select lists are now removed. Users name now highlights in yellow.</td>
				<td>	
				</td>
				<td></td>
				<td><span class="dot dot-green"></span></td>
				<td>069c8e0</td>
			</tr>
			<tr class="completed">				

				<td>11/6/2019</td>
				<td>New test Nanodrop now added. Suggested by Nufatt. Discussed in 5 Nov 2019 lab meeting.</td>
				<td>	
				</td>
				<td></td>
				<td><span class="dot dot-green"></span></td>
				<td>6adb590</td>
			</tr>
			<tr class="completed">				

				<td>11/7/2019</td>
				<td>Message board now visible after NGS sample completed. Requested by Bil</td>
				<td>	
				</td>
				<td></td>
				<td><span class="dot dot-green"></span></td>
				<td>f370696</td>
			</tr>
			<tr class="completed">				

				<td>11/7/2019</td>
				<td>Tracer audit update. Included NGS tests to tracer audit.</td>
				<td>	
				</td>
				<td></td>
				<td><span class="dot dot-green"></span></td>
				<td>6c13d69</td>
			</tr>
			<tr class="completed">				

				<td>11/7/2019</td>
				<td>Tracer-audit update. Log in person is now tracked for the tracer audit. Requested by Paige in 5 Nov 2019 lab meeting.</td>
				<td>	
				</td>
				<td></td>
				<td><span class="dot dot-green"></span></td>
				<td>328014e</td>
			</tr>
			<tr class="completed">				

				<td>11/12/2019</td>
				<td>Add Development Progress report</td>
				<td>	<ol>
						<li>Requirements:<span class="dot dot-green"></span></li>
						<li>Design:<span class="dot dot-green"></span></li>
						<li>List Risks:<span class="dot dot-green"></span></li>
						<li>Implementation:<span class="dot dot-green"></span></li>
						<li>Testing:<span class="dot dot-green"></span></li>

					</ol>
				</td>
				<td>Need to keep up to date.</td>
				<td><span class="dot dot-green"></span></td>
				<td>e5448e6</td>
			</tr>
			<tr class="completed">

				<td>11/13/2019</td>
				<td>Add Comment Field to Checkout Sample.  Requested 5 Nov 2019 lab meeting.</td>
				<td>
					<ol>
						<li>Requirements:<span class="dot dot-green"></span></li>
						<li>Design:<span class="dot dot-green"></span></li>
						<li>List Risks:<span class="dot dot-green"></span></li>
						<li>Implementation:<span class="dot dot-green"></span></li>
						<li>Testing:<span class="dot dot-green"></span></li>

					</ol>
				</td>
				<td>
					<ol>
						<li>Add a comment field to update_test which will show up in tracer audit.</li>
						<li>NGS add_test adds comments comment_ref = visit_id, comment_type = visit_table.  Otherwise comment_ref = ordered_test_id, comment_type = ordered_test_table</li>
						<li>I will only implement this for Single analyte tests.  The message board will take care of any sign out comments.  It will comments will be closed after sample is signed out.  I should make a way to continue to add comments.</li>
						<li>Added to ordered_test_table_concat_comments_vw</li>
						<li>Test samples: 5414-19 NGS, 5423-19 Nanodrop</li>
					</ol>
				</td>
				<td><span class="dot dot-green"></span></td>
				<td>740b33c</td>
			</tr>
			
			<tr class="completed">

				<td>11/14/2019</td>
				<td>Add add_test comments to NGS message board. Add time to message board.</td>
				<td>
					<ol>
						<li>Requirements:<span class="dot dot-green"></span></li>
						<li>Design:<span class="dot dot-green"></span></li>
						<li>List Risks:<span class="dot dot-green"></span></li>
						<li>Implementation:<span class="dot dot-green"></span></li>
						<li>Testing:<span class="dot dot-green"></span></li>
					</ol>
				</td>
				<td>
					<ol>
						<li>NGS add_test adds comments comment_ref = visit_id, comment_type = visit_table.  Pull this info into message board so everyone can see this info while signing sample out.  Add comments to NGS worksheet also.</li>
						<li>Query: 'message-board-comments' using RUN ID UNION visit_id comments.</li>
						<li>Make sure the time works for right and left justified messages</li>
						<li></li>
						<li>visit_id:116,222</li>
					</ol>
				</td>
				<td><span class="dot dot-green"></span> ASAP</td>
				<td>e662d43</td>
			</tr>
			<tr class="completed">

				<td>11/12/2019</td>
				<td>BUG: Logs are only adding to detailed_log_table and not log_table</td>
				<td>
					<ol>
						<li>Identify cause of bug:<span class="dot dot-green"></span></li>
						
					</ol>
				</td>
				<td>
					<ol>
						<li>There's a bug in insert statement.  IP address does not have quotes or ,</li>
						
					</ol>
				</td>
				<td><span class="dot dot-green"></span> ASAP</td>
				<td>3790c7d</td>
			</tr>
			<tr class="completed">

				<td>11/14/2019</td>
				<td>Add add_test comments to NGS worksheet.</td>
				<td>
					<ol>
						<li>Requirements:<span class="dot dot-green"></span></li>
						<li>Design:<span class="dot dot-green"></span></li>
						<li>List Risks:<span class="dot dot-green"></span></li>
						<li>Implementation:<span class="dot dot-green"></span></li>
						<li>Testing:<span class="dot dot-green"></span></li>
					</ol>
				</td>
				<td>
					<ol>
						<li>NGS test comments are stored under run_id message_board and visit_id add_visit.  Add the visit_id comments to the NGS worksheet.  Do not add message_board comments.  </li>
						<li>Single analyte test comments are added under ordered_test_table and ordered_test_id.  Therefore I can have two different queries one for NGS and one for Single analyte. It is not necessary to query both because they do not coexist.
						update 'ordered-test-info' query
						</li>
						<li>If in the future I add ordered_test_table comments to NGS tests then this will not work however, this is unlikely since the visit_table powers this test.</li>
						<li>Add views: visit_table_concat_comments_vw</li>
						<li>5423-19</li>
					</ol>
				</td>
				<td><span class="dot dot-green"></span> ASAP</td>
				<td></td>
			</tr>
			<tr class="completed">

				<td></td>
				<td>Add message board comments to pre pending sample.</td>
				<td>
					<ol>
						<li>Requirements:<span class="dot dot-green"></span></li>
						<li>Design:<span class="dot dot-green"></span></li>
						<li>List Risks:<span class="dot dot-green"></span></li>
						<li>Implementation:<span class="dot dot-green"></span></li>
						<li>Testing:<span class="dot dot-green"></span></li>
					</ol>
				</td>
				<td>
					<ol>
						<li>Can not add comments from message board since run_id has not been set yet for pre pending samples.  Instead show what comments in pre ngs samples table</span></li>
						<li>edited 'pending-pre-samples' update for upload_NGS_data also using 'pending-pre-samples-by-visit-id',  Show send outs also uses the pre_ngs_samples_list via 'pending-pre-samples-sent-out'</li>
						<li>Space is limited in table. </li>
						
						
					</ol>

				</td>
				<td><span class="dot dot-green"></span> ASAP</td>
				<td>9739e87</td>
			</tr>
			<tr class="completed">

				<td>11/19/2019</td>
				<td>Make sure in donor id a 19 digit varchar including ints and spaces can be included in the donor id field. Requested by Nufatt in November 19th, lab meeting.</td>
				<td>
					<ol>
						
						<li>Design:<span class="dot dot-green"></span></li>
						
						<li>Testing:<span class="dot dot-green"></span></li>
					</ol>
				</td>
				<td>
					<ol>
						<li>Remove Auto Formating Button will allow for entry of this format.  SQL field types varchar(100), restrictions on field are not implemented upon submission. </li>
						<li>1234 5A67 8912 345</li>
					</ol>

				</td>
				<td><span class="dot dot-green"></span> ASAP</td>
				<td>a561f41</td>
			</tr>
			<tr class="completed">

				<td>11/19/2019</td>
				<td>Add a switch to turn off auto lowercase on all letters except first letter on first and last name in sample log book.  Requested by Nufatt and Suni in November 19th, lab meeting.</td>
				<td>
					<ol>
						<li>Requirements:<span class="dot dot-green"></span></li>
						
						<li>Implementation:<span class="dot dot-green"></span></li>
						
					</ol>
				</td>
				<td>
					<ol>
						<li>class capitalize controls reformatting in add_sample_log_book.js.  Make a button which removes this class.</li>
						<li>use jquery .unbind() to remov the bound event-listeners of capitalize of the elements.  Use event.preventDefault() to prevent submission of the form.</li>
					</ol>

				</td>
				<td><span class="dot dot-green"></span></td>
				<td>a561f41</td>
			</tr>
			<tr class="completed">

				<td>11/19/2019</td>
				<td>Make a diagnosis and treatment timeline for Dr. Oltvai</td>
				<td>
					
				</td>
				<td>
					<a href="?page=dev_designs&dev=sam&design_num=2&task=Dr_Oltvai_Timeline" type="button" class="btn btn-info btn-info-hover">
		             		Time line
		            </a>

				</td>
				<td><span class="dot dot-green"></span></td>
				<td>a561f41</td>
			</tr>
						<tr class="completed">

				<td>11/26/2019</td>
				<td>Bug Fix: Changed format of working NGS report builder to add new lines when entered into interpretations.  Reported by Zoltan. </td>
				<td>
					
				</td>
				<td>
					

				</td>
				<td><span class="dot dot-green"></span></td>
				<td>d8c8733</td>
			</tr>
			<tr class="completed issues">

				<td>11/26/2019</td>
				<td>Edited Sex of patient form and run_id in visit_table reverted back to 0.  I do not know how this happened.  I was able to find the correct run_id by sample_name.  I saved the db before editing patient_table and the visit_table run_id is 0 there also.  Therefore it unlinked somewhere else.</td>
				<td>
					
				</td>
				<td>
					

				</td>
				<td><span class="dot dot-green"></span></td>
				<td></td>
			</tr>
			<tr class="completed">
				<td></td>
				<td>Read Dr. Oltvai's Paper on Rhapsody.</td>
				<td>
					<ul>
						<li>Test online interface <span class="dot dot-green"></span></li>
						<li>Read paper <span class="dot dot-green"></li>
						<li>Read Git hub <span class="dot dot-green"></li>
						<li>Read Website</li>
						<li>Install and test locally</li>
						<li>Test Via API</li>
					</ul>
				</td>
				<td>
					Canceled task 11/27/2019

				</td>
				<td><span class="dot dot-red"></span></td>
				<td></td>
			</tr>
			<tr class="completed">
				<td></td>
				<td>Paul would like to have all the information of steps completed in samples with outstanding reports like the editors/completed steps column in pre ngs samples table.  However, he would to know who was notified with reviewers_emailed and when.</td>
				<td>
					<ol>
						<li>Design:<span class="dot dot-green"></span></li>
						<li>List Risks:<span class="dot dot-green"></span></li>
						<li>Implementation:<span class="dot dot-yellow"></span></li>
						<li>Testing:<span class="dot dot-purple"></span></li>

					</ol>
				</td>
				<td>					
					<ol>
						<li>
							<a href="?page=dev_designs&dev=sam&design_num=3&task=provide_more_NGS_step_tracking" type="button" class="btn btn-info btn-info-hover">
			                    	Design
			               	</a>
		               	</li>						
						<li>Since sent_email_table is a new table make sure reports older than today do not break when there's no info of the person emailed.  This is accomplished by querying sent_email_table to see if there were only notifications sent.  If not then Notified reviewers is not displayed.<span class="dot dot-green"></span>
						</li>
						<li>After submitting the Notified reviewers box does not display only message.  This was fixed by refreshing the page and scrolling the the id #notified-reviewers-box <span class="dot dot-green"></span></li>
						<li>Mostly implemented just need to add email_info to every time sendEmail is called.  Then test all instances of sending emails. <span class="dot dot-green"></span></li>
						<li>Testing notified reviewers on visit_id 124<span class="dot dot-green"></span></li>
					</ol>
				</td>
				<td><span class="dot dot-green"></span></td>
				<td>3e10eca, 71bf383</td>
			</tr>
			<tr class="completed">
				<td>12/10/2019</td>
				<td>Add OFA transcripts to Gene table</td>
				<td>
					This was already implemented.
				</td>
				<td>	
					<h2>Current OFA Genes</h2>				
					<img src="templates/dev_designs/sam/ofa_current_gene_chart.png" width="600px;">
					<h2>All OFA Genes</h2>
					<img src="templates/dev_designs/sam/ofa_all_gene_chart.png" width="600px;">
				</td>
				<td><span class="dot dot-green"></span></td>
				<td></td>
			</tr>
			<tr class="completed">
				<td>12/9/2019 - MONDAY</td>
				<td>Can you please change the DOB for William Luther, MRN E827852, softpath 19-SHE3487, J1205116 to 1946-07-26.  Thanks Bonnie</td>
				<td>
					
				</td>
				<td>					
					SELECT * FROM patient_table WHERE last_name = "Luther" and first_name = "William";
					<br>
					UPDATE patient_table SET dob = "1946-07-26" WHERE last_name = "Luther" and first_name = "William" and patient_id = 632;
				</td>
				<td><span class="dot dot-green"></span></td>
				<td></td>
			</tr>
			<tr class="completed">
				<td>12/9/2019 - MONDAY</td>
				<td>Can you please change the Stock concentration and 260/280 results on patient Simmons, Bernard
				J2063726 MD5160.19: 41.8ng/ul and 1.92. If you have any questions let me know. Thanks Sharon</td>
				<td>
					
				</td>
				<td>					
					sample_log_book_id = 2769<br>
					There are two ordered tests 5617 = DNA Extraction
					<br>
					SELECT ott.ordered_test_id, otst.test_name FROM ordered_test_table ott LEFT JOIN orderable_tests_table otst ON otst.orderable_tests_id = ott.orderable_tests_id  WHERE sample_log_book_id = 2769;
					<br>
					SELECT * FROM extraction_log_table WHERE ordered_test_id = 5617; <br>

					UPDATE extraction_log_table SET stock_conc = 41.8, purity = 1.92 WHERE extraction_log_id = 2551;

				</td>
				<td><span class="dot dot-green"></span></td>
				<td></td>
			</tr>
			<tr class="completed">
				<td>12/10/19</td>
				<td>
					Turn Saide's myeloid NGS pool back on.
				</td>
				<td>
					
				</td>
				<td>					
					SELECT * FROM pool_chip_linker_table WHERE ngs_panel_id = 2;<br>
					SELECT * FROM pool_steps_table WHERE pool_chip_linker_id = 67;<br>
					Change status of pool_chip_linker_table pool_chip_linker_id 67 to pending<br>
					UPDATE pool_chip_linker_table SET status = "pending" WHERE pool_chip_linker_id = 67;
				</td>
				<td><span class="dot dot-green"></span></td>
				<td></td>
			</tr>
			<tr class="completed">
				<td></td>
				<td>Fix email subject for creating a new account.  Current message is New URMC Reporter Account</td>
				<td>
					
				</td>
				<td></td>
				<td><span class="dot dot-green"></span></td>
				<td>71bf383</td>
			</tr>
			<tr class="completed">
				<td>Canceled Bill decided he has does not want any help.</td>
				<td>Automate transfer of data from Torrent server, Ion Torrent, and MiSeq</td>
				<td>
					<ol>
						<li>Requirements:<span class="dot dot-yellow"></span></li>
						<li>Design:<span class="dot dot-yellow"></span></li>
						<li>List Risks:<span class="dot dot-purple"></span></li>
						<li>Implementation:<span class="dot dot-purple"></span></li>
						<li>Testing:<span class="dot dot-purple"></span></li>

					</ol>
				</td>
				<td>
					<ol>
						<li>	
							<ul>
								<li>Data needed: Run QC, Coverage Data, variants</li>
								<li>Add a table to store Run QC data</li>
							</ul>							
						</li>
						<li>
							<a href="?page=dev_designs&dev=sam&design_num=1&task=automate_data_transfer" type="button" class="btn btn-info btn-info-hover">
		                    		Design
		               		</a>
		               	</li>
		               	<li>
							<ul>
								<li>One of the Servers crashes</li>
								<li>User picks incorrect run for QC run</li>
							</ul>
						</li>
						<li></li>
					</ol>
				</td>				
				<td><span class="dot dot-red"></span> </td>
				<td></td>
			</tr>
			<tr class="completed">
				<td>1/3/2019</td>
				<td>Feature add:  Audits were only working for current year.  Added in two select boxes one for month and one for year.  Requested by Bonnie.</td>
				<td>
					
				</td>
				<td>
					
				</td>				
				<td><span class="dot dot-green"></span></td>
				<td>9e4d971</td>
			</tr>