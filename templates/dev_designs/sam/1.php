<h1>Servers</h1>
<div class="row">
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		<img src="templates/dev_designs/sam/1_flowdiagram.png">
	</div>
</div>
<h1>Transfer Plan</h1>
<div class="row">
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		<img src="templates/dev_designs/sam/1_flowdiagram2.png">
	</div>
</div>
<h1>General</h1>

<div class="row alert alert-info">
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		<b>Overview:</b> Make a scalable design to transfer NGS data from the instruments to CIRC and then to MD Infotrack.  Data includes Run QC data, Coverage Data, BAM, and Variant Data.  By scalable I mean we expect to have multiple instruments and new panels.  
		<br>
		<br>
		<h3>CIRC</h3>
		We need to come up with a scalable folder structure for circ.  Possibly this will work.  Probably it would be best to add panel info via the pre library pool step in MD Infotrack.   
		<ul>
			<li>location -> instrument -> run_date -> folders: run_qc, cov_data, bam, variant_data</li>
		</ul>
		<h3>MD Infotrack design changes required:</h3>
		<ul>
			<li>Use ngs_panel_step_regulator_table for steps that specific to the panel.</li>
			<li>Use instrument_step_regulator_table for </li>
			
		</ul>
		<br>		
	</div>
</div>

<h1>Run QC Data</h1>

<div class="row alert alert-info">
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		<b>Overview of requirement:</b>  Every time the Ion Torrent or the MiSeq are run it needs to pass a list of metrics which are different for both instruments.  Ion Torrent has two chips that need to pass Run QC while the MiSeq has one flow cell to pass qc.  The goal is to automatically pull the data from each instrument and transfer it to MD Infotrack.  Using a cronjob data will be automatically pulled from both instruments and placed in ____________.  Make sure there are at least two identifiers and instrument id included in file name.  Then from the Pre NGS library Pool the user will enter all identifiers to match data to Run OC info.  
		<br>
		<br>
		<b>Current:</b> Currently there's no link to pre ngs library pool and the run.  The user is checking if Run QC meets the standards manually and just clicking a button saying it passed on every sample in the run.  For the Ion Torrent the run metrics currently are only saved on the error prone torrent server.  I'm unsure of the protocol for myeloid panel.  I would like a standard for all panels to streamline. The run metrics should be saved in two locations for safe keeping (_____, MD Infotrack).  
		<br>

		<h3>MD Infotrack design changes required:</h3>		
		Need to add a standard run qc table and link test to instrument the instrument table<br>
		
		<h3>Risks:</h3>
		What should we do if the chip/flowcell fails.  Should MD Infotrack not allow to proceed?<br>
		Will the Run QC metrics be the same for all tests run on this instrument?  
	</div>
</div>
<ul>
	<li><b>Torrent Server - Run QC Data:</b>
		<ul>
			<li>Default_Report.php provides an HTML overview.
				<ul>
					<li><a href="http://10.149.58.208/output/Home/Auto_user_S5XL-0108-103-CEPH_RUN_7_18_19_Sample_2_163_004/Default_Report.php"> Example: http://10.149.58.208/output/Home/Auto_user_S5XL-0108-103-CEPH_RUN_7_18_19_Sample_2_163_004/Default_Report.php</a>
					</li>
				</ul>
			</li>
			<li><b>LOCATION:</b> 10.149.58.208/output/Home/Auto_user_S5XL-0108-104-TEMPLATE_NAME_166_006/basecaller_results/ionstats_basecaller.json<br></li>
			<li><u>Data Needed / JSON format / MySQL </u>
				<ul>
					<li> : JSON/MySQL column name</li>
					<li>$ionstats['full']['num_reads']: total_reads (INT UNSIGNED)</li>
					<li>Usable reads: usable_reads (TINYINT UNSIGNED)</li>
					<li>ISP loading: isp_loading (TINYINT UNSIGNED)
						<ul>
							<li>$wells_with_isp = $bf["Bead Wells"];
            print try_number_format($wells_with_isp);</li>
						</ul>
					</li>	
					<li>ISP loading: isp_loading (TINYINT UNSIGNED)</li>
					<li>Enrichment:enrichment (TINYINT UNSIGNED)</li>
					<li>Clonal:clonal (TINYINT UNSIGNED)</li>
					<li>Polyclonal:polyclonal (TINYINT UNSIGNED)</li>
					<li>Final Library:final_library (TINYINT UNSIGNED)</li>
					<li>Test Fragment:test_fragment (TINYINT UNSIGNED)</li>
					<li>Adapter Dimer:adapter_dimer (TINYINT UNSIGNED)</li>
					<li>Low Quality:low_quality (TINYINT UNSIGNED)</li>
					<li>Median Read Length:median_read_length (TINYINT UNSIGNED)</li>
					<li>Mean Raw Accuracy 1X: mean_raw_accuracy (TINYINT UNSIGNED)</li>
				</ul>
			</li>
			<li>Samantha will link the data using user_input with another step in the pre-library-pool-step (run_qc)</li>
		</ul>

	</li>
	<li><b>MiSeq - Run QC data</b>
		<ul>
			<li>Using a well documented script Bill will pull run qc data from Torrent server and place in a JSON in ______ folder.  Name will be of format ____________</li>
			<li><u>Data Needed / JSON format / MySQL table</u>
				<ul>
					<li>density (K/mm2):density (INT UNSIGNED)</li>
					<li>Clusters passing filter (%): clusters_passing_filter (TINYINT UNSIGNED)</li>
					<li>Reads passing filter (M): reads_passing_filter (18 <=)</li>
					<li>% >= Q30 total: percent_q30 (70 - 100)</li>
					<li>% Aligned - Total: percent_aligned (2.5 - 20)</li>
					<li>Error Rate Total %: error_rate_total (0 - 4)</li>
				</ul>
			</li>
		</ul>
	</li>

</ul>

<h1>Coverage Data</h1>
<div class="row alert alert-info">
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		<b>Overview:</b> Pull coverage data from the Torrent server and Bills Custom script and put on CIRC.  The data will be uploaded to MD Infotrack during the pre-library-pool-step (run_qc)  

		<br>
		The Ion torrent uses produces a file from each sample currently and the MiSeq uses one file.  It would be best to choose a consistent format for both MiSeq and Ion Torrent.
		<br>

		<b>Location:</b>10.149.58.208/output/Home/Auto_user_S5XL-0108-103-CEPH_RUN_7_18_19_Sample_2_163_004/plugin_out/coverageAnalysis_out.76/IonXpress_001/IonXpress_001_Auto_191009A_Oncomine_Focus_DNA_for_S5_08062019_torrent-server_28.amplicon.cov.xls
		<br><br>What does _out.76 mean

		Name: ???
/Auto_user_S5XL-0108-115-191111_A_Oncomine_Focus_DNA_for_S5_08062019_182_030/
	</div>
</div>
<ul>
	<li><b>Torrent Server - Coverage Data</b>
		<ul>
			<li>How will this data be?</li>
			<li>How is it being pulled? coverageAnalysis API??</li>
			<li>
				<ul>
					<li>Torrent Server / MD Infotrack</li>
					<li>GENE_ID / gene</li>
					<li>region_id / Amplicon</li>
					<li>total_reads / depth</li>
					<li>contig_id / chr</li>
					<li>contig_srt / amp_start</li>
					<li>contig_end / amp_end</li>						
				</ul>
			</li>
		</ul>
	</li>
	<li><b>?? (miseq) - Coverage Data (we need documentation on how this file is created. And where the scripts are.)</b>

	</li>
</ul>
<h1>BAM</h1>
<div class="row alert alert-info">
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		<b>Overview:</b> Pull BAM files and place on CIRC VM1 for backup storage<br>

		<br>
		<b>LOCATION:</b> 10.149.58.208/output/Home/Auto_user_S5XL-0108-104-TEMPLATE_NAME_166_006/download_links<br>
		<br>
		Only transfer .bam<br>
		Do Not transfer .bam.bai, .basecaller.bam, .rawtf.bam<br>
		<br>
	</div>
</div>
<ul>
	<li><b>Torrent Server - BAM</b></li>
	<li><b>?? (miseq) - BAM</b></li>
</ul>

<h1>Variant Data</h1>
<ul>
	<li><b>Ion Torrent - Variant Data</b>
		<ul>
			<li><b>Location:</b>10.149.58.208/results/analysis/output/Home/Auto_user_S5XL-0108-115-191111_A_Oncomine_Focus_DNA_for_S5_08062019_182_030/plugin_out/variantCaller_out.83/R_2019_11_12_09_07_27_user_S5XL-0108-115-191111_A_Oncomine_Focus_DNA_for_S5_08062019.vcf.zip
				<ul>
					<li>Move zip folder</li>
					<li>Unzip folder and keep VCF files
						<ul>
							Example: file names
							<li> TSVC_variants_IonXpress_020.vcf</li>
							<li> TSVC_variants_IonXpress_009.vcf</li>
						</ul>
					</li>
					<li>Ignore .vcf.gz.tbi files</li>
				</ul>
			</li>
		</ul>
	</li>
	
	
	<li><b>?? (miseq) - Variant Data</b>

	</li>
</ul>

<img src="templates/dev_designs/sam/1_db.png">	
