<h1>Completed Step Info:</h1>
<div class="row">
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		<ul>
			<li>
				report_steps_table -> names of steps
			</li>
			<li>
				step_run_xref -> run_id, step_id, user_id
			</li>
		</ul>
	</div>
</div>
<h1>Query Database to include data in Samples with Outstanding reports</h1>
<div class="row">
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		<ul>
			<li>
				Two queries to update 'pending-tech-runs-ngs-panel-id' and 'pending-tech-runs'
			</li>
			<li>
				Add a SQL view which merges step_run_xref and GROUP_CONCATS all completed steps.<br>
				<br>
				CREATE VIEW pending_NGS_completed_steps_vw AS<br>
				SELECT srx.run_id, GROUP_CONCAT(ut.first_name, "->", rst.step ORDER BY srx.time_stamp ASC SEPARATOR ", ") AS editors_per_step, GROUP_CONCAT(rst.step ORDER BY srx.time_stamp ASC SEPARATOR ", ") AS completed_steps <br>
				FROM step_run_xref srx<br>
				LEFT JOIN report_steps_table rst ON rst.step_id = srx.step_id<br>
				LEFT JOIN user_table ut ON ut.user_id = srx.user_id<br>

				GROUP BY srx.run_id<br>
			</li>
			<li>
				pncs_vw.editors_per_step,
				pncs_vw.completed_steps

				LEFT JOIN 	pending_NGS_completed_steps_vw pncs_vw
				ON 			pncs_vw.run_id = rit.run_id
			</li>
		</ul>
	</div>
</div>
<h1>Reviewers emailed info:</h1>
<div class="row">
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		<ul>
			<li>
				<h3><b>Add table sent_email_table</b></h3>
				<ul>
					<li>Add to this every time an email is sent.</li>
					<li>
						An a new entry will be added for every user and email is sent to thru the email_utils class via sendEmail function.
					</li>
					<li>
						<h3><b>SQL Schema</b></h3>
						<img src="templates/dev_designs/sam/3_db.png">
					</li>
				</ul>

			</li>
			<li>
				<h3><b>email_utils->sendEmail class info</b></h3>
				Add to email_utils->sendEmail array $email_info.  This will allow for adding to sent_email_table.  
				<br>
				<h3>Example email_info</h3>
				$email_info = array(
					'email_type' 			=> 	'reviewers_emailed',
					'recipient_user_ids' 	=>	array(1, 5, 8, 10, etc...),
					'sender_user_id'		=> 	1,
					'ref_id'				=> 	example would be visit_id,
					'ref_table'			=> 	'visit_table'
				)
			</li>
			<li>Edit all sendEmail() calls to include the new email_info array.</li>
		</ul>


							
	</div>
</div>
