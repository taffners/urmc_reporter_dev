<div class="container-fluid">
	<div class="row" style="padding:0px;margin:0px;">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="padding:0px;margin:0px;">
			<div id="monthly-count-graphs-chart"></div>
			
		</div>

	</div>
</div>
<div class="container-fluid">
	<div class="row page-break-after" style="padding:0px;margin:0px;">
		
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="padding:0px;margin:0px;">
			
			<div id="monthly-count-graphs-table"></div>
		</div>

	</div>
</div>

<div class="container-fluid">
	<div class="row" style="padding:0px;margin:0px;">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="padding:0px;margin:0px;">
			<div id="monthly-count-samples-chart"></div>
			
		</div>

	</div>
</div>
<div class="container-fluid">
	<div class="row page-break-after" style="padding:0px;margin:0px;">
		
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="padding:0px;margin:0px;">
			
			<div id="monthly-count-samples-table"></div>
		</div>

	</div>
</div>

<div class="container-fluid">
	<div class="row" style="padding:0px;margin:0px;">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="padding:0px;margin:0px;">
			<div id="monthly-test-counts-chart"></div>
			
		</div>

	</div>
</div>
<div class="container-fluid">
	<div class="row page-break-after" style="padding:0px;margin:0px;">
		
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="padding:0px;margin:0px;">
			
			<div id="monthly-test-counts-table"></div>
		</div>

	</div>
</div>


<div class="container-fluid">
	<div class="row" style="padding:0px;margin:0px;">
		
<?php
	if (isset($samples_wo_tests_per_month) && !empty($samples_wo_tests_per_month))
	{
?>	
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="padding:0px;margin:0px;">
			<h1>Number of Samples per month without tests</h1>
		</div>
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="padding:0px;margin:0px;">
			<table class="formated_table sort_table_no_inital_sort dataTable">
				<thead>
					<th>Month Year</th>
					<th>Count</th>
				</thead>
				<tbody>
		<?php
		foreach($samples_wo_tests_per_month as $key => $month)
		{
		?>
					<tr>
						<td><?=$month['month_year'];?></td>
						<td><?=$month['num_samples'];?></td>
					</tr>
		<?php
		}
		?>
				</tbody>
			</table>
		</div>
<?php
	}
?>
			
		
	</div>
</div>








