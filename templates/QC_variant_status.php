<?php
	require_once('templates/lists/run_info_list.php');
?>

<div class="container-fluid">
	<div class="row">
		<?php
			require_once('templates/shared_layouts/qc_variant_progress_bar.php');
		?>
		<!-- update area -->
		<div id="toggle-report-progress-bar" class="col-xs-10 col-sm-10 col-md-10 col-lg-10">
			
			<fieldset>
				<legend>
					<?= $utils->UnderscoreCaseToHumanReadable($page);?>
				</legend>
<?php	
		
		if (isset($step_status) && $step_status == 'completed')
		{
			
				// find who approved the qc
				foreach ($completed_steps as $key => $step)
				{
					if ($step['step'] === $page)
					{
?>
				<div class="alert alert-success" style="font-size:20px;">
					QC Confirmed Completed by <?= $step['user_name'];?> at <?= $step['time_stamp'];?>
				</div> 
<?php
					}
				}
		}
?>
				<div class="row" style="overflow-x:auto;">
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
						<table class="formated_table sort_table">
							<thead>
								<th>Gene</th>
								<th>Coding</th>
								<th>Protein</th>
								<th>Check Name <br>(Not in knowledge base)</th>
								<th>Classification</th>
								<th>Past Tiers</th>
								<th>Check Population Data<br>(Unclassified and no past tiers)</th>
								<th>Cosmic</th>
								<th>VAF</th>
								<th>HapMap Alt Variant Freq</th>
							</thead>
							<tbody>
<?php

     $knowledge_ids_for_all = True;
     if (isset($variantArray) && $num_snvs !== 0)
     {  
 	
     	$text_message_form = '';
          for ($i = 0; $i < sizeOf($variantArray); $i++)
          {
        	
               /////////////////////////////////////////////////////////
               // If knowledge ID was not found for any sample remove nav buttons
               /////////////////////////////////////////////////////////   
               if ($variantArray[$i]['knowledge_id'] == '')
               {
                    $knowledge_ids_for_all = False;
               }

               // find Accession number if added to genes_covered_by_panel_table
               $search_array = array(
               	'ngs_panel_id'	=>	$visitArray[0]['ngs_panel_id'],
               	'gene' 		=> 	$variantArray[$i]['genes']
               );

               $accession_number = $db->listAll('accession-num-present-for-ngs-panel-by-gene', $search_array);


               /////////////////////////////////////////////////////////////////
               // Get Varsome link if accession number present
               /////////////////////////////////////////////////////////////////
               if (!empty($accession_number))
               {
               	$varsome_link = 'https://varsome.com/variant/hg19/'.$accession_number[0]['accession_num'].'('.$variantArray[$i]['genes'].')%3A'.$variantArray[$i]['coding'];
               	$mutalyzer_link = 'https://mutalyzer.nl/name-checker?description='.$accession_number[0]['accession_num'].':'.$variantArray[$i]['coding'];
               }
               else
               {
               	$varsome_link = 'https://varsome.com/variant/hg19/'.$variantArray[$i]['genes'].'%20'.$variantArray[$i]['coding'];
               	$mutalyzer_link = 'https://mutalyzer.nl/name-checker';
               }
?>
				               <tr id ="tr_var_id_<?= $variantArray[$i]['observed_variant_id'];?>" data-scroll-knowledge-id="knowledge_id_<?= $variantArray[$i]['knowledge_id']; ?>">
				               	<?=$utils->toggleMoreLess($variantArray[$i]['genes'], 'genes', $i);?>
				               	<?=$utils->toggleMoreLess($variantArray[$i]['coding'], 'coding', $i);?>
				               	<?=$utils->toggleMoreLess($variantArray[$i]['amino_acid_change'], 'amino_acid_change', $i);?>				               	
	<?php
								if ($variantArray[$i]['knowledge_id'] == null)
								{
									// Add Name checker data to message area
$text_message_form .= '
Variant: '.$variantArray[$i]['genes'].' '. $variantArray[$i]['coding'].' '.$variantArray[$i]['amino_acid_change'].'
	Varsome (Variant):
	Varsome (Cosmic ID):
	Varsome (Position):
	Mutalyzer (Errors and Correction):

	';
	?>									
									<td class="alert-flagged-qc">Check Names <a href="<?= $varsome_link; ?>" target="blank">varsome</a> and <a href="<?= $mutalyzer_link;?>" target="blank"> mutalyzer</a></td>
	<?php									
								}
								else
								{
	?>
									<td class="alert-passed-qc">NA</td>
	<?php									
								}               
                    ////////////////////////////////////////////////////
                    // Add the columns of classification and tiers
                    ////////////////////////////////////////////////////
                    require('templates/shared_layouts/classification_past_tiers.php');			
?>                    
									<td> <a href="https://cancer.sanger.ac.uk/cosmic/mutation/overview?id=<?= $variantArray[$i]['cosmic']; ?>" target="_blank"><?= $variantArray[$i]['cosmic']; ?></a></td>

									
									<td <?php
									if (is_numeric($variantArray[$i]['frequency']) && (float)$variantArray[$i]['frequency'] < 5)
									{
										echo 'class="alert-flagged-qc"';
									}
									else
									{
										echo 'class="alert-passed-qc"';
									}

									?>> <?= $variantArray[$i]['frequency']; ?></td>

									<td>
<?php
				///////////////////////////////////////////////////
				// Add Hap Map Info
				///////////////////////////////////////////////////
				
				if (isset($variantArray[$i]['loc']) && !empty($variantArray[$i]['loc']))
				{
					$pos = explode('-', $variantArray[$i]['loc']);
					$pos = $pos[0];

					$search_array = array(
						'ngs_panel_id' 	=>  	$visitArray[0]['ngs_panel_id'],
						'chr'			=> 	$variantArray[$i]['chr'],
						'pos'			=> 	$pos
					);


					$hapmap = $db->listAll('hapmap-vaf', $search_array);

					// add hap map info 
					if (!isset($hapmap[0]) || empty($hapmap[0]))
					{
						echo 'No Data';
					}
					else if ($hapmap[0]['hapmap_regulator_staus'] === 'off')
					{
						echo 'Function Turned Off';
					}
					else if ($hapmap[0]['hapmap_regulator_staus'] === 'on' && empty($hapmap[0]['hapmap_sample']))
					{
						echo '<1%';
					}					
					else
					{
						$td_hapmap = '';
						foreach ($hapmap as $key => $pos_samp)
						{
							$td_hapmap.=$pos_samp['hapmap_sample'].':'.$pos_samp['vaf'].'%<br>';
						}
						echo $td_hapmap;

$text_message_form .= '
Variant: '.$variantArray[$i]['genes'].' '. $variantArray[$i]['coding'].' '.$variantArray[$i]['amino_acid_change'].'
	HapMap Alt Variant Freq:'.$td_hapmap;						
					}
				}
				else
				{
					echo 'Function does not run for variants not in knowledge base or if location is not entered into the knowledge base.  Check manually.';
				}


?>										

									</td>
				               	</tr>
	<?php
			}
		}

	?>				              
				          	</tbody>
				     	</table>
				    </div>
				</div>
				<div class="alert alert-success" style="font-size:20px;">
					Please fill out the message board below and send to Director and everyone else working on this report.
				</div>
<?php
		require_once('templates/shared_layouts/message_board.php')
?>				
				<div class="alert alert-success" style="font-size:20px;">
					<strong>Clicking the QC Variant Steps Completed button below confirms that you looked at all variant QC.  You confirm that QC data was assessed for all qc variant steps and added to the <?= SITE_TITLE;?> or the Excel workbook related to this run.</strong>
					<br><br>
					Including:
					<ul>
						<li>IGV: 
				<?php
					if (isset($visitArray[0]['ngs_panel_id']) && $visitArray[0]['ngs_panel_id'] === '2')
					{
				?>
						Use Excel Workbook to open IGV 
				<?php
					}
					else if (isset($visitArray[0]['ngs_panel_id']) && $visitArray[0]['ngs_panel_id'] === '1')
					{
				?>
						Use built in IGVR in Ion Reporter or download .jnlp from IGRV <br>
						<img src="images/igv_jnlp.png">
				<?php
					}
				?>
						</li>
						<li>Check name for mutants not found in <?= SITE_TITLE;?> with <a href="https://varsome.com/" target="blank">varsome</a> and <a href="https://mutalyzer.nl/name-checker" target="blank"> mutalyzer</a></li>
						<li>For variants which are unclassified and have no past reported tiers check <a href="https://varsome.com/">varsome</a>. </li>
					</ul> 
				</div>

				<div class="row">
					<form id="add_visit_info" class="form" method="post" class="form-horizontal">		
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
							<button type="submit" id="<?= $page;?>_submit" name="<?= $page;?>_submit" value="Submit"  class="btn-lg btn-primary btn-primary-hover">
								QC Variant Steps Completed				
							</button>
						</div>
					</form>
				</div>
			</fieldset>
		</div>
	</div>
</div>