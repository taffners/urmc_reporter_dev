<div class="container-fluid">
	
	<div class="row d-print-none" >
<?php	
	if (!isset($_GET['filter']) || $_GET['filter'] === 'current_month')
	{
		$all_class_turnaround_filter = 'btn btn-primary btn-primary-hover';
		$curr_month_class_turnaround_filter = 'btn btn-success btn-success-hover';
	}
	else if (!isset($_GET['filter']) || $_GET['filter'] === 'all')
	{
		$all_class_turnaround_filter = 'btn btn-success btn-success-hover';
		$curr_month_class_turnaround_filter = 'btn btn-primary btn-primary-hover';
	}
	else
	{
		$all_class_turnaround_filter = 'btn btn-primary btn-primary-hover';
		$curr_month_class_turnaround_filter = 'btn btn-primary btn-primary-hover';
	}
?>
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 d-print-none">
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 alert alert-info">
					To print or save as a PDF right click on page and select print.  
		          </div>		          
		     </div>
		</div>
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 d-print-none">
			<div class="form-group row">
				<div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
					<label for="month" class="form-control-label">
						Month: <span class="required-field">*</span>
					</label>
				</div>
				<div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
					<select class="form-control" name="month" id="month" required="required">					
	<?php
		$months = array(1 => 'Jan', 2 => 'Feb', 3 => 'Mar', 4 => 'Apr', 5 => 'May', 6 => 'Jun', 7 => 'Jul', 8 => 'Aug', 9 => 'Sep', 10 => 'Oct', 11 => 'Nov', 12 => 'Dec');	

		// Add all months selecting current month
		foreach ($months as $month_num => $month_name)
		{
	?>
						<option value="<?= $month_num;?>" <?php
							// select the month which was selected on reload
							if (isset($_GET['month']) && $_GET['month'] == $month_num)
							{
								echo 'selected';
							}

							// default selection to current month.  Useful for selecting all.
							else if (!isset($_GET['month']) && CURR_MONTH === $month_num)
							{
								echo 'selected';
							}
						?>>
							<?= $month_name;?>
						</option>
	<?php
		}
	?>
					</select>			
				</div>
				<div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
					<label for="year" class="form-control-label">
						Year: <span class="required-field">*</span>
					</label>
				</div>
				
				<div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
					<select class="form-control" name="year" id="year" required="required">					
	<?php
		// Add possible years selecting current year
		for ($y=2019; $y <= CURR_YEAR; $y++)
		{
	?>
						<option value="<?= $y;?>" <?php
							// select the month which was selected on reload
							if (isset($_GET['year']) && $_GET['year'] == $y)
							{
								echo 'selected';
							}

							// default selection to current month.  Useful for selecting all.
							else if (!isset($_GET['year']) && CURR_YEAR === $y)
							{
								echo 'selected';
							}
						?>>
							<?= $y;?>
						</option>
	<?php
		}
	?>
					</select>					
				</div>
				<div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
					<a class="btn btn-primary btn-primary-hover" href="?page=performance_audit_worksheet&filter=months&month=<?=CURR_MONTH;?>&year=<?=CURR_YEAR;?>&tracked_tests=1" title="Use this button to edit turn around times" style="padding-top:5px;padding-bottom:5px;">
                       Turn around time worksheet
                  	</a>
				</div>
				<div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
					<a class="btn btn-primary btn-primary-hover" href="?page=alter_default_tracked_tests" title="Use this button to edit which tests are checked automatically to be included in the tracked tests view." style="padding-top:5px;padding-bottom:5px;">
	                   Alter Default Tracked Tests
	              	</a>
				</div>
			</div>

		</div>
		<div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
			<label for="tracked_tests" class="form-control-label">Turn around time checked tests: <span class="mulitple-field">*</span></label>
		</div>
		<div id="track-tests-ckbx-div" class="col-xs-12 col-sm-8 col-md-8
<?php
		// only show if tracked tests btn is clicked
		if (!isset($_GET['tracked_tests']))
		{
			echo ' d-none';
		}
?>
		">
<?php
		$tests_default_tracked = array();
		foreach ($tests as $key => $test)
		{

?>
			<div class="form-check">
				<input class="form-check-input tracked_tests-ckbx" type="checkbox" value="<?= $test['orderable_tests_id'];?>" name="tracked_tests[]" required="required" data-orderable_tests_id="<?= $test['orderable_tests_id'];?>" data-test_name="test_<?= str_replace(' ', '_', $test['test_name']);?>"
				<?php
				if ($test['turnaround_time_tracked'] === 'yes')
				{
					echo 'checked';
					array_push($tests_default_tracked, $test['test_name']);
				}
				?>>
				<label><?= $test['test_name'];?></label>
			</div>
<?php
		}								
?>						
		</div>
	</div>
	</div>
	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 audit-headers">
			<hr class="section-mark"> 
<?php 
	if (isset($num_samples[0]['num_samples']) && isset($selected_month_name) && isset($_GET['year']) && isset($num_tests[0]['num_tests']))
	{ 
?>	

			<b>Total Number of samples (<?= $selected_month_name;?> <?= $_GET['year'];?>):</b> <?= $num_samples[0]['num_samples'];?>
			<br>
			<b>Total Number of Tests (<?= $selected_month_name;?> <?= $_GET['year'];?>):</b> <?= $num_tests[0]['num_tests'];?>
<?php
	}
?>						
		</div>
	</div>
	<div class="row page-break-after" style="padding:0px;margin:0px;">
		<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6" style="padding:0px;margin:0px;">
			<div id="sampes_per_test"></div>
			<div id="samples_tissues_per_test"></div>
		</div>
		
		<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6" style="padding:0px;margin:0px;">		
			<div id="month_counts"></div>	
			<div id="tissue_counts"></div>
			
		</div>
	</div>
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 d-print-none">
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 alert alert-info">
					For statics on all historic turnaround times click setup -> tests
		        </div>		          
		     </div>
		</div>
	<div class="row" style="padding:0px;margin:0px;">
		<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 audit-headers">
			Number of Samples per Test
		
			<table class="formated_table sort_table" style="overflow-x:auto;margin-bottom: 10px;">
				<thead>
					<th>Test</th>
					<th>Count</th>
				</thead>
				<tbody>

		<?php

		foreach ($samples_per_test as $key => $test)
		{
		?>
				<tr>
					<td><?= $test['test_name'];?></td>
					<td><?= $test['num_tests'];?></td>					
				</tr>
		<?php
		}
		?>
				</tbody>
			</table>
		</div>

		<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 audit-headers">
			Turn around Time Stats per test

			<table class="formated_table sort_table" style="overflow-x:auto;margin-bottom: 10px;">
				<thead>
					<th>Test</th>
					<th>Month</th>
					<th>Expected turnaround time</th>
					<th>Failed Count</th>
					<th>Passed Count</th>
					<th>Total Count</th>
					<th>Average</th>
					<th>STD</th>
					<th>Min</th>
					<th>Max</th>
				</thead>
				<tbody>

		<?php	
		foreach ($turnaround_stats as $key => $test)
		{
		?>
				<tr>
					<td><?= $test['test_name'];?></td>
					<td><?= $test['month'];?> <?= $test['year'];?></td>
					<td><?= $test['expected_turnaround_time'];?></td>
					<td><?= $test['failed_count'];?></td>
					<td><?= $test['passed_tests'];?></td>
					<td><?= $test['total_tests'];?></td>
					<td><?= $test['average_turnaround'];?></td>
					<td><?= $test['std_turnaround'];?></td>
					<td><?= $test['min_turnaround'];?></td>
					<td><?= $test['max_turnaround'];?></td>

				</tr>
		<?php
		}
		?>
				</tbody>
			</table>
		</div>
	</div>
		<!-- <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 audit-headers">
			Turn around Times

			<table class="formated_table sort_table" style="overflow-x:auto;margin-bottom: 10px;">
				<thead>
					<th>Month</th>
					<th>Failed Count</th>
					<th>Passed Count</th>
					<th>Total Count</th>
				</thead>
				<tbody>

		<?php	
		foreach ($turnaround_counts as $key => $month)
		{
		?>
				<tr>
					<td><?= $month['month'];?> <?= $month['year'];?></td>
					<td><?= $month['failed_count'];?></td>
					<td><?= $month['passed_tests'];?></td>
					<td><?= $month['total_tests'];?></td>					
				</tr>
		<?php
		}
		?>
				</tbody>
			</table>
		</div> -->
	</div>
		
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 d-print-none">
			<div class="row">
<?php
	if (isset($_GET['all_turn_around_times']))
	{
		$link = str_replace('&tracked_tests=1', '', $current_address);
	}
	else if (isset($_GET['tracked_tests']))
	{
		$link = str_replace('&tracked_tests=1', '&all_turn_around_times=1', $current_address);
	}
	else
	{
		$link = $current_address.'&all_turn_around_times=1';
	}
?>					
				<div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
					<a href="?page=<?= $link;?>" type="button" class="
					<?php
						if (isset($_GET['all_turn_around_times']))
						{
							echo 'btn btn-success btn-success-hover';
						}
						else
						{
							echo 'btn btn-primary btn-primary-hover';
						}
					?>">
		                    All turn around times
		               </a>
		          </div>
		          <div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
<?php
	if (isset($_GET['all_turn_around_times']))
	{
		$link = str_replace('&all_turn_around_times=1','&tracked_tests=1', $current_address);
	}
	else
	{
		$link = $current_address.'&tracked_tests=1';
	}
?>


					<a id="toggle-tracked-tests-btn" href="?page=<?= $link;?>" type="button" class="
					<?php
						if (isset($_GET['tracked_tests']))
						{
							echo 'btn btn-success btn-success-hover';
						}
						else
						{
							echo 'btn btn-primary btn-primary-hover';
						}
					?>">
		                    Tracked Tests
		               </a>
		          </div>
		          <div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
<?php
	if (isset($_GET['all_turn_around_times']))
	{
		$link = str_replace('&all_turn_around_times=1', '', $current_address);
	}
	else if (isset($_GET['tracked_tests']))
	{
		$link = str_replace('&tracked_tests=1', '', $current_address);
	}
	else
	{
		$link = $current_address;
	}
?>		          	
					<a href="?page=<?= $link;?>" type="button" class="
					<?php
						if (!isset($_GET['all_turn_around_times']) && !isset($_GET['tracked_tests']))
						{
							echo 'btn btn-success btn-success-hover';
						}
						else
						{
							echo 'btn btn-primary btn-primary-hover';
						}
					?>">
		                    Failed turn around times
		               </a>
		          </div>		          
		     </div>
		</div>
	</div>

	<div class="row" style="padding:0px;margin:0px;">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 audit-headers">
<?php
		// Find header for table depending on samples being shown
	if (isset($_GET['all_turn_around_times']))
	{
		$filter_samp_type = 'of All';
	}
	else if (isset($_GET['tracked_tests']))
	{
		// add all tests default tracked but make it so they can be easily deleted and added to

		$tracked_test_str = 'of <span id="all-tracked-samples-table-header-span">';

		foreach ($tests_default_tracked as $key => $default_tracked)
		{
			$tracked_test_str.= ' <span id="'.$default_tracked.'_table_header">'.$default_tracked.'</span>';
		}
		$filter_samp_type = $tracked_test_str.'</span>';
		
	}
	else
	{
		$filter_samp_type = 'of Failed Expected Time';
	}

?>			
			Turnaround Times <?= $filter_samp_type;?> Tests  
		</div>
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			<table class="formated_table sort_table" style="overflow-x:auto;margin-bottom: 10px;">
				<thead>
					<th>Soft Lab#</th>
					<th>Test</th>
					<th># Days</th>
					<th># Work Days</th>
					<th>Expected</th>
					<th>Start Date</th>
					<th>Finish Date</th>					
					<th>User/Task</th>
				</thead>
				<tbody>

		<?php
		if (isset($failed_turnaround) && !empty($failed_turnaround))
		{
			foreach ($failed_turnaround as $key => $failed)
			{
				$show_status = '';
				if ( !in_array($failed['test_name'], $tests_default_tracked) && isset($_GET['tracked_tests']))
				{
					$show_status = ' d-none';
				}
		?>
				<tr class="test_<?= str_replace(' ', '_', $failed['test_name']);?><?= $show_status;?>">
					<td><?= $failed['soft_lab_num'];?></td>
					<td><?= $failed['test_name'];?></td>
					<td 
					<?php
						if ($failed['turnaround_time'] > $failed['expected_turnaround_time'])
						{
							echo ' class="red-background"';
						}
					?>
					><?= $failed['turnaround_time'];?></td>
					<td 
					<?php

						$numWorkingDays = $productionDays->numWorkDays($failed['start_date'], $failed['finish_date']);


						if ($numWorkingDays > $failed['expected_turnaround_time'])
						{
							echo ' class="red-background"';
						}
					?>
					><?= $numWorkingDays;?></td>
					<td><?= $failed['expected_turnaround_time'];?></td>
					<td><?= $utils->addEditableTableCell($failed['start_date'], 'start_date', $failed['ordered_test_id'],'ordered_test_id',$failed['ordered_test_id']);?></td>
					<td><?= $utils->addEditableTableCell($failed['finish_date'], 'finish_date', $failed['ordered_test_id'],'ordered_test_id',$failed['ordered_test_id']);?></td>
					<td><?= $failed['users_by_task'];?></td>
				</tr>
		<?php
			}
		}
		?>
				</tbody>
			</table>
		</div>
	</div>
	<div class="row" style="padding:0px;margin:0px;">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 audit-headers">
			Counts of Tissues Per Test
		</div>
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			<table class="formated_table sort_table" style="overflow-x:auto;margin-bottom: 10px;">
				<thead>
					<th>Test</th>
	<?php
				// Find all tissues for the header of the matrix
				$tissues = array();
				$tests = array();
						
				foreach ($tissues_per_test as $key => $count)
				{					
					if (!in_array($count['test_name'], $tests))
					{
						array_push($tests, $count['test_name']);
					}

					if (!in_array($count['test_tissue'], $tissues))
					{
						array_push($tissues, $count['test_tissue']);
	?>
						<th><?=$count['test_tissue'];?></th>
	<?php
					}
				}
	?>
				</thead>
				<tbody>

		<?php

		foreach ($tests as $key => $test)
		{
			// make an empty array of size of number of tissues
			$curr_counts = array_fill(0,sizeof($tissues), 0);

			foreach ($tissues_per_test as $key2 => $count)
			{
				$curr_test = $count['test_name'];

				if ($curr_test === $test)
				{
					$column_num = array_search($count['test_tissue'], $tissues);

					$curr_counts[$column_num] = $count['num_tissues'];
				}
			}
		?>
				<tr>
					<td><?= $test;?></td>
		<?php
			foreach ($curr_counts as $key3 => $add_counts)
			{
		?>					
					<td><?= $add_counts;?></td>				
		<?php
			}
		?>
				</tr>
		<?php
		}
		?>
				</tbody>
			</table>
		</div>
	</div>
	<div class="row" style="padding:0px;margin:0px;">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 audit-headers">
			Counts of Tissues
		</div>
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			<table class="formated_table sort_table" style="overflow-x:auto;margin-bottom: 10px;">
				<thead>
					<th>Month</th>
	<?php
				// Find all tissues for the header of the matrix
				$tissues = array();
				$months = array();
			
				foreach ($num_tissue_counts as $key => $count)
				{					
					if (!in_array($count['month'].' '.$count['year'], $months))
					{
						array_push($months, $count['month'].' '.$count['year']);
					}

					if (!in_array($count['test_tissue'], $tissues))
					{
						array_push($tissues, $count['test_tissue']);
	?>
						<th><?=$count['test_tissue'];?></th>
	<?php
					}
				}
	?>
				</thead>
				<tbody>

		<?php

		foreach ($months as $key => $month)
		{
			// make an empty array of size of number of tissues
			$curr_counts = array_fill(0,sizeof($tissues), 0);

			foreach ($num_tissue_counts as $key2 => $count)
			{
				$curr_month = $count['month'].' '.$count['year'];

				if ($curr_month === $month)
				{
					$column_num = array_search($count['test_tissue'], $tissues);

					$curr_counts[$column_num] = $count['count_test_tissue'];
				}
			}
		?>
				<tr>
					<td><?= $month;?></td>
		<?php
			foreach ($curr_counts as $key3 => $add_counts)
			{
		?>					
					<td><?= $add_counts;?></td>				
		<?php
			}
		?>
				</tr>
		<?php
		}
		?>
				</tbody>
			</table>
		</div>
	</div>


</div>