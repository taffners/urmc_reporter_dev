<?php
	require_once('templates/lists/training_nav_bar.php');
?>
<?php
	require_once('templates/lists/run_info_list.php');
?>

<div class="container-fluid">
	<div class="row">
		<?php
			require_once('templates/shared_layouts/qc_variant_progress_bar.php');
						
		?>

		<div id="toggle-report-progress-bar" class="col-xs-10 col-sm-10 col-md-10 col-lg-10" style="height:500px;">
			
				<fieldset>
					<legend>
						<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
							Strand Bias
						</div>
						<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
							<button class="info-btn-black btn-circle">
								<label id="strand-bias-descript-hover-card">
									<span class="fas fa-info-circle"></span>
								</label>
							</button>
						</div>						
					</legend>

<?php
	$search_array = array(
		'step_regulator_name' => 'strand_bias_calc',
		'ngs_panel_id'			=> isset($visitArray[0]['ngs_panel_id']) ? $visitArray[0]['ngs_panel_id'] : 0
	);

	$step_regulator = $db->listAll('step-regulator-status', $search_array);

	// check if the panel can automatically calculate strand bias.  If off default to user input.
	if (isset($step_regulator[0]['status']) && $step_regulator[0]['status'] === 'on')
	{
?>
					<div class="row" style="overflow-x:auto;">
						<table class="formated_table ">
							<thead>
								<th>Gene</th>		                    
				                    <th>Coding</th>                        
				               	<th>Protein</th>
				               	<th>Ref Allele</th>
				               	<th>Variant Allele</th>
		               	<?php 
		               		if ($visitArray[0]['ngs_panel'] === 'Oncomine Focus Hotspot Assay')
							{
						?>
								<th>TSV Ref+/Ref-/Var+/Var- Column</th>
						<?php
							}
		               	?>
				               	<th>ref -</th>
				               	<th>ref +</th>
				               	<th>Variant -</th>
				               	<th>Variant +</th>
				               	<!-- <th>IGV strain Bias</th> -->
				               	<th>Strand Bias</th>
				               	<th>Status</th>
				               	
							</thead>
							<tbody>
	<?php
					if (isset($strand_bias_per_variant))
					{
					
						foreach($strand_bias_per_variant as $key => $var)
						{	
							$status = $var['status']; 

	?>
								<tr class="background_row_status_color_<?=$status;?>" row_id="<?= $var['observed_variant_id']; ?>">
									<td><?= $var['genes']; ?></td>
									<td><?= $var['coding']; ?></td>
									<td><?= $var['amino_acid_change']; ?></td>
									<td><?= $var['ref_allele']; ?></td>	
									<td><?= str_replace(',', ', ', $var['variant_allele']); ?></td>
							<?php 
			               		if ($visitArray[0]['ngs_panel'] === 'Oncomine Focus Hotspot Assay')
								{
							?>
									<td><?= $var['ref_var_strand_counts'];?></td>
							<?php
								}
			               	?>									
									<td>
									<?php
										echo $utils->addEditableTableCell($var['ref_neg'], 'ref_neg', $var['strand_bias_id'],'strand_bias_id',$var['strand_bias_id']);
									?>								
									</td>	
									<td>
									<?php
										echo $utils->addEditableTableCell($var['ref_pos'], 'ref_pos', $var['strand_bias_id'],'strand_bias_id',$var['strand_bias_id']);
									?>
									</td>
									<td>
									<?php
									
										echo $utils->addEditableTableCell($var['variant_neg'], 'variant_neg', $var['strand_bias_id'],'strand_bias_id',$var['strand_bias_id']);
									?>
									</td>
									<td>
									<?php
										echo $utils->addEditableTableCell($var['variant_pos'], 'variant_pos', $var['strand_bias_id'],'strand_bias_id',$var['strand_bias_id']);
									?>
									</td>
									<!-- <td>
									<?php
										echo $utils->addEditableTableCell($var['igv_strain_bias'], 'igv_strain_bias', $var['strand_bias_id'],'strand_bias_id',$var['strand_bias_id']);
									?></td> -->
									<td id="sb_td_<?= $var['observed_variant_id']; ?>"><?= $var['strand_bias'];?></td>
									<td><?= $status;?></td>
									
								</tr>
	<?php
						}
					}
	?>
							</tbody>
						</table>
					</div>
<?php
	}
	else 
	{
?>
		<div class="alert alert-info" style="font-size:20px;">
			<strong>Protocol:</strong>

			<ol>
				<li>Use the QC Excel Sheet to open IGV for each variant.</li>				
				<!-- <li>Save an image (.png) of all variants in IGV using the windows snipping tool <img src="images/Snipping_Tool_icon.png" height="50px">.</li>
				<ul>
					<li><strong>DO NOT USE PRINT SCREEN</strong>.  This will create a very large image and waste server space.</li>
					<li><a href="https://support.microsoft.com/en-us/help/13776/windows-use-snipping-tool-to-capture-screenshots" target="_blank">Click here for more information on how to use windows snipping tool</a></li>
				</ul> -->
				<li>
					Assess each variant for strand bias. Click on the bottom blue bar corresponding to the variant of interest in the VCF track. A pop-up box is open which contains genotype information and attributes. Under Genotype Attributes the Strand Bias should be equal to -100. If it is not equal to -100 and it is a SNV examine the data in the BAM Coverage track by clicking on the coverage for that base. A box pops up which provides the number of positive and negative reads for each base. Examine the read numbers for the alternate base call and determine if there is strand bias as defined by a strand having more than 50% more reads than the other strand. If there is strand bias contact the Director, who will determine if further testing needs to be performed for this variant. If it is not equal to -100 and the variant is an indel then contact the Director for guidance. Make note of any issues with strand bias in the message board. There is no problem with strand bias for FLT3 ITDs because the software used to detect this mutation uses the sequence from both reads. If the ITD is found in only one direction then it is not reported by the software.
				</li>
				<li>Fill out the message board below submit message.  IF THE SUBMIT MESSAGE BUTTON IS NOT CLICKED THIS WILL NOT SAVE.</li>
				<!-- <li>After IGV images (.png) are saved for all variants:
					<ul>
						<li><strong>Upload a variant Image for each variant</strong></li>
						<li><span class="mulitple-field">*</span> => Multiple files can be uploaded at the same time.</li>
						<li>
							<a href="https://www.computerhope.com/issues/ch000771.htm" target="_blank">Click here to find out how to select multiple files</a>
						</li>
					</ul> 
				</li> -->
			</ol>
			  
		</div>
		<!-- <fieldset>
			<legend>Upload IGV Variant Images</legend>
			<form id="IGV_files_form" name="IGV_files_form" class="form" method="post" class="form-horizontal" enctype="multipart/form-data" autocomplete="off">
				<div class="form-group row">				
					<div class="col-xs-12 col-sm-7 col-md-5 col-lg-5">
						<label for="IGV_files" class="form-control-label" >IGV Variant Images: <span class="required-field">*</span> <span class="mulitple-field">*</span></label><br><span>(file types accepted .png)</span>
					</div>
					<div class="col-xs-12 col-sm-5 col-md-7 col-lg-7">
						<input id="IGV-files-id" type="file" multiple name="IGV_files[]" accept=".png, image/png"/>
					</div>
				</div>
				<div class="form-group row">
					<div class="col-md-8 col-lg-8">

					</div>
					<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
						<button type="submit" id="upload_igv_images" name="upload_igv_images_sumbit" value="submit"  class="btn btn-primary btn-primary-hover submit-show-loader">
							Upload IGV Variant Images <br><br>
							<div style="background-color:red;font-size:16px;">Warning!!! Resets message board</div>
						</button>
					</div>
				</div>
				<div class="form-group row d-none" id="md5-file-hash-div">
						
				</div>
			</form>
		</fieldset> -->
<?php
		if (isset($variantArray) && $num_snvs !== 0)
	     {  
	 	
	     	$text_message_form = '';
	          for ($i = 0; $i < sizeOf($variantArray); $i++)
	          {
$text_message_form .= '
Variant: '.$variantArray[$i]['genes'].' '. $variantArray[$i]['coding'].' '.$variantArray[$i]['amino_acid_change'].'
	IGV (Strand Bias):
	IGV (Coverage):

';	
	          }       
	     }
	}
?>
<?php
		require_once('templates/shared_layouts/message_board.php')
?>
				<form class="form" method="post" class="form-horizontal">		
					<div class="row">
<?php
	// nav buttons
	// qcVarStepTracker is declared in shared_logic/qc_variant_shared_logic.php
	$previous_step = $qcVarStepTracker->FindPreviousStep($page);
	$step_status = $qcVarStepTracker->StepStatus($completed_steps, $page);	
?>
						<div class="offset-xs-4 offset-sm-4 offset-md-4 offset-lg-4 col-xs-2 col-sm-2 col-md-2 col-lg-2">
<?php
	if (isset($_GET['page']) && $step_status !='completed')
	{
?>		
							<button type="submit" id="<?= $page;?>_submit" name="<?= $page;?>_submit" value="Submit"  class="btn btn-primary btn-primary-hover">
								Done
							</button>		
<?php
	}
?>
						</div>
						<div class="offset-xs-2 offset-sm-2 offset-md-2 offset-lg-2 col-xs-2 col-sm-2 col-md-2 col-lg-2">
<?php
	// if this isn't the first step add previous button
	if ($previous_step != 'first step')
	{
?>
							<a href="?page=<?= $previous_step;?>&run_id=<?= RUN_ID;?>&visit_id=<?= $_GET['visit_id'];?>&patient_id=<?= $_GET['patient_id'];?>" class="btn 
							btn-primary btn-primary-hover" role="button">
								❮ Previous
							</a>
						</div>
<?php
	}

	if ($step_status ==='completed')
	{
		$next_step = $qcVarStepTracker->FindNextStep($page);
?>
					     <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
							<a href="?page=<?= $next_step;?>&run_id=<?= RUN_ID;?>&visit_id=<?= $_GET['visit_id'];?>&patient_id=<?= $_GET['patient_id'];?>" class="btn 
							btn-primary btn-primary-hover" role="button" 
							<?php 
								if ($page == 'review_report')
								{
									echo 'target="_blank"';
								}
							?>>
								Next ❯
							</a>
						</div>
<?php
	}
?>
					</div>
				</form>
				</fieldset>
			
		</div>
	</div>
</div>


