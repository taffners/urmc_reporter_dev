<div class="container-fluid my-4">
     
     <div class="row m-1">
          <h1>Bugs, Maintenance, and Downtime</h1>
     </div>
     <ul class="row" style="list-style-type:none;padding-left:0px;font-size:18px;">
          <li class="pb-5 col-xs-12 col-sm-6 col-md-4 col-lg-4">

               <div class="d-flex ">
                    <ul  style="list-style-type:none;">
                         <li>
                              <a id="report-a-bug-btn" href="?page=report_a_bug" title="Investigate history of bugs and downtime since 2020-11-13" style="padding-top:5px;padding-bottom:5px;" aria-label="report a bug">
                                   <img src="images/bug2.png" style="margin-left:5px;height:45px;width:45px;" meta="an image of a bug" alt="an image of a bug">
                              </a>
                         </li>
                         <li><b>Report a Bug or Maintenance time.</b></li>                                     
                    </ul>                              
               </div>                    
          </li>
          <li class="pb-5 col-xs-12 col-sm-6 col-md-4 col-lg-4">

               <div class="d-flex ">
                    <ul  style="list-style-type:none;">
                         <li>
                              <a id="report-a-bug-btn" href="?page=bug_list&status=pending" title="" style="padding-top:5px;padding-bottom:5px;" aria-label="">
                                   <span class="fas fa-list"></span>
                              </a>
                         </li>
                         <li><b>Access list of outstanding Bugs and maintenance</b></li>                                     
                    </ul>                              
               </div>                    
          </li>
          <li class="pb-5 col-xs-12 col-sm-6 col-md-4 col-lg-4">

               <div class="d-flex ">
                    <ul  style="list-style-type:none;">
                         <li>
                              <a id="report-a-bug-btn" href="?page=bug_list" title="" style="padding-top:5px;padding-bottom:5px;" aria-label="">
                                   <span class="fas fa-list"></span>
                              </a>
                         </li>
                         <li><b>Bug, maintenance, and downtime history</b></li>                                      
                    </ul>                              
               </div>                    
          </li>
     </ul>
     
     
</div>