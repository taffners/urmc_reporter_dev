<div class="container-fluid">
<?php
// insert a list of all current and previous tests for the patient.  Do not show if the sample_type_toggles is not a sample cause sample_type_toggles like CAP sample will have tons of entries which are not relevant. 
if (isset($sampleLogBookArray[0]['sample_type_toggle']) && $sampleLogBookArray[0]['sample_type_toggle'] === 'sample')
{
?>
	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" id="sample-log-book-table">
			<fieldset>
				<legend>
					Previous tests / Current Tests for Patient
				</legend>
				<table  class="notebook_table sort_table_no_inital_sort" style="overflow-x:auto;margin-bottom: 10px;">
					<thead>
						<th class="pink_right_border">Log #</th>
						<th>Search Criteria</th>
						<th>First Name</th>
						<th>Last Name</th>
						<th>Soft Path Num</th>
						<th>Soft Lab Num</th>
						<th>MD#</th>
						<th>MRN</th>
						<th>MPI</th>
						<th>Tissue</th>
						<th>Received</th>
						<th>Comments</th>
						
						<th>Summary of Previous Tests Added</th>
						<th>Current Sample</th>

						<th>Test Results</th>
					</thead>
					<tbody>
	<?php
		if (isset($previous_tests_by_mrn) && !empty($previous_tests_by_mrn))
		{
			$sample_log_book_ids = array();
			foreach ($previous_tests_by_mrn as $key => $sample)
			{		
				if (!in_array($sample['sample_log_book_id'], $sample_log_book_ids))
				{
					$row_color = '';
					$curr_sample = '';
					if ($sample['sample_log_book_id'] === $_GET['sample_log_book_id'])
					{
						$row_color = 'style="font-weight:bold;"';
						$curr_sample = '<span class="fas fa-check" style="color:#03AC13;"></span>';
					}
	?>
					<tr <?= $row_color;?>>
						<td class="pink_right_border"><?=$sample['sample_log_book_id'];?></td>
						<td><?=$sample['search_criteria'];?></td>
						<td><?=$sample['first_name'];?></td>
						<td><?=$sample['last_name'];?></td>
						<td><?=$sample['soft_path_num'];?></td>
						<td><?=$sample['soft_lab_num'];?></td>
						<td><?=$sample['mol_num'];?></td>
						<td><?=$sample['medical_record_num'];?></td>
						<td><?=$sample['mpi'];?></td>
						<td><?=$sample['test_tissue'];?></td>
						<td><?=$sample['date_received'];?></td>
						<td><?=$sample['problems'];?></td>


						<td>
						<?php
								$ordered_tests = explode(',', $sample['ordered_tests']);
								$test_status = explode(',', $sample['test_status_concat']);

								foreach ($ordered_tests as $key => $test)
								{
									// switch ($key) 
									// {
									//  	case 0:
									//  		$test_status[$key] = 'pending';
									//  		break;
									 	
									//  	case 1:
									//  		$test_status[$key] = 'complete';
									//  		break;

									//  	case 2:
									//  		$test_status[$key] = 'stop';
									//  		break;
									//  	case 3:
									//  		$test_status[$key] = 'revising';
									//  		break;
									//  	case 4:
									//  		$test_status[$key] = 'waiting';
									//  		break;
									//  	case 5:
									//  		$test_status[$key] = 'Sent Out';
									//  		break;
									//  	case 6:
									//  		$test_status[$key] = 'In Progress';
									//  		break;
									//  } 

									$test_dot = '';
									// add a yellow dot
									if ($test_status[$key] === 'pending')
									{
										$test_dot = '<span class="dot dot-right dot-yellow" title="Test Pending"></span>';
									}

									else if ($test_status[$key] === 'complete')
									{
										$test_dot = '<span class="dot dot-right dot-green" title="Test Complete"></span>';
									}
									else if ($test_status[$key] === 'stop')
									{
										$test_dot = '<span class="dot dot-right dot-red" title="Test Canceled"></span>';
									}
									else if ($test_status[$key] === 'revising')
									{
										$test_dot = '<span class="dot dot-right dot-blue" title="Revising Report"></span>';
									}
									else if ($test_status[$key] === 'waiting')
									{
										$test_dot = '<span class="dot dot-right dot-purple" title="Test Waiting/On Hold"></span>';
									}
									else if ($test_status[$key] === 'Sent Out')
									{
										$test_dot = '<span class="dot dot-right dot-grey" title="Test Sent Out"></span>';
									}
									else if ($test_status[$key] === 'In Progress')
									{
										$test_dot = '<span class="dot dot-right dot-lime-green" title="Test in progress"></span>';
									}
									echo $test.' '.$test_dot.'<br>';
								}

							?>	
						</td>
						<td><?= $curr_sample;?></td>
						<td><?= $sample['ordered_tests_by_test_result'];?></td>
					</tr>
	<?php

					array_push($sample_log_book_ids, $sample['sample_log_book_id']);
				}
			}
		}
	?>


					</tbody>
				</table>
			</fieldset>
		</div>		
	</div>
<?php
}
?>
	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="height:500px;">

			<form class="form" method="post" class="form-horizontal">
				<fieldset>
					<legend class='show'>
						Add a test to <?= $sampleLogBookArray[0]['mol_num'];?>
					</legend>
					<?php					
						require_once('templates/shared_layouts/sample_overview.php');
					?>
					
					<div class="alert alert-info" style="font-size:18px;">
						<strong>IMPORTANT!!</strong>
						<span class="fas fa-exclamation-triangle"></span>
						Make sure to add DNA extraction as a separate test on all samples. <span class="fas fa-exclamation-triangle"></span>						
					</div>
					<!-- <div id="form-control-fields">
						<div class="form-group row">
							<div class="col-xs-12 col-sm-3 col-md-2 col-lg-2">
								<label for="data_type" class="form-control-label">
									Field type <span class="required-field d-none div-to-append-extra-fields-required-span">*</span>
								</label>
							</div>	
							<div class="col-xs-12 col-sm-8 col-md-3 col-lg-3 ">
								<select class="selectpicker div-to-append-extra-fields-required-field field-type-restriction" data-live-search="true" data-field_apply_restriction_to="extra_field_val" name="data_type[]" data-width="100%">										

									<option value="text">text</option>
									<option value="number">number</option>
									<option value="date">date</option>
									<option value="datetime-local">Date time</option>
									<option value="email">email</option>
									<option value="month">month</option>										
									<option value="tel">phone number</option>
									<option value="time">time</option>
									<option value="url">url</option>
									<option value="week">week</option>
								</select>
							</div>
						</div>
					</div> -->

					<div id="form-control-fields">
						<div class="form-group row">
							<div class="col-xs-12 col-sm-4 col-md-4">
								<label for="orderable_tests_id" class="form-control-label">Test: <span class="required-field">*</span></label>
							</div>
							<div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">
								
								<select class="selectpicker" data-live-search="true" data-width="100%" name="orderable_tests_id" id="orderable_tests_id" required="required" <?php
								if (isset($orderedTestArray[0]['orderable_tests_id']) && !empty($orderedTestArray[0]['orderable_tests_id']))
								{
									echo 'readonly="readonly"';
								}
								?>>

									<option value="" selected="selected"></option>
								<?php

									for($i=0;$i<sizeof($allTestsAvailable);$i++)
			               			{	
			               // check if this test is still being offered.  If sample is
			               // an anonymous_donor only provide tests with donor in the name.
						$anonymous_allowed_field = True;
			               if 	(
			               		isset($sampleLogBookArray[0]['sample_type_toggle'])
			               		&&
			               		$sampleLogBookArray[0]['sample_type_toggle'] === 'anonymous_donor' && 
			               		(
			               			strpos($allTestsAvailable[$i]['full_test_name'], 'donor') === False &&
			               			strpos($allTestsAvailable[$i]['test_name'], 'Extraction') === False
			               		)
			               	)
			               {
			               	$anonymous_allowed_field = False;
			               }
			             
          				if 	(
          						isset($allTestsAvailable[$i]['test_status']) && 
          						$allTestsAvailable[$i]['test_status'] === 'Active' &&
          						$anonymous_allowed_field
          					)
          				{
								?>
											<option <?= $allTestsAvailable[$i]['test_name'] == 'DNA Extraction'? 'data-icon="fas fa-dna"':'';?>
												value="<?= $allTestsAvailable[$i]['orderable_tests_id']; ?>" <?= $utils->GetValueForUpdateSelect($orderedTestArray, 'orderable_tests_id', $allTestsAvailable[$i]['orderable_tests_id'])?> 
												data-orderable_tests_id="<?= $allTestsAvailable[$i]['orderable_tests_id']; ?>"
												data-consent_required="<?= $allTestsAvailable[$i]['consent_required']; ?>"
												data-custom_fields_available="<?= $allTestsAvailable[$i]['custom_fields_available']; ?>"
												data-previous_positive_required="<?= $allTestsAvailable[$i]['previous_positive_required']; ?>"
												data-mol_num_required="<?= $allTestsAvailable[$i]['mol_num_required']; ?>"
														
											>
													
													<?= $allTestsAvailable[$i]['test_name']; ?> 
													(<?= $allTestsAvailable[$i]['full_test_name']; ?>, 
													<?= $allTestsAvailable[$i]['test_type']; ?>, 
													<?= $allTestsAvailable[$i]['assay_name']; ?>)
											</option>
								<?php
										}
									}
								?>
								</select>
								
							</div>
						</div>
					</div>

					
				<?php

				// add an alert for each notes_for_work_sheet and show it with JavaScript if the test is selected
				foreach ($allTestsAvailable as $key => $test)
				{
					if (!empty($test['notes_for_work_sheet']))
					{

				?>
					<div class="row notes-for-work-sheet-row d-none" id="notes-for-work-sheet-<?= $test['orderable_tests_id'];?>">
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
							<div class="alert alert-info">
								<?= nl2br($test['notes_for_work_sheet']);?>
							</div>
						</div>
					</div>
				<?php
					}
				}
				?>								
					<div class="form-group row d-none">
						<div class="col-xs-12 col-sm-4 col-md-4">
							<label for="outside_case_num" class="form-control-label">Outside Case #:</label>
						</div>
						<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
							<input type="text" class="form-control" id="outside_case_num" maxlength="50" name="outside_case_num" value="<?= $utils->GetValueForUpdateInput($orderedTestArray, 'outside_case_num');?>"/>
						</div>
					</div>
					<div id="history_fields" 
						<?php
							if (isset($testInfo[0]['previous_positive_required']) && $testInfo[0]['previous_positive_required'] === 'yes')
							{
								echo 'class="alert"';
							}

							else 
							{
								echo 'class="d-none alert"';
							}
						?>>
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
							<h2 class="raised-card-header">History Section</h2>

							<hr class="section-mark">
							<div class="form-group row">
							
								<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 mb-2 mt-0">
									<button type="button" class="btn btn-success btn-success-hover append-sample-input" data-clone_template="div-to-clone-history-section-field" data-rm_clone_template_btn="clone-history-section-remove-btn" data-append_to="div-to-append-history-section" style="float:right;">
						        		<span class="fas fa-plus"></span> Insert another History 
						            </button>
								</div>
							</div>	
							<div id="div-to-append-history-section">
<?php
		// The general template for updating for the history section is different than if this is a new
		// test.
		if (
				isset($orderedTestArray) && 
				isset($orderedTestArray[0]['previous_positive_required']) && 
				$orderedTestArray[0]['previous_positive_required'] === 'yes' &&
				isset($histories) &&
				!empty($histories)
			)
		{

			foreach ($histories as $key_1 => $history)
			{
				$history_arr = array(0 => $history);
?>
								<div class="form-group row">
									<div class="col-xs-12 col-sm-4 col-md-4">
										<label for="history_overview" class="form-control-label">History: <span class="required-field">*</span></label>
									</div>
									
									<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
										<select class="required-history-field" name="history_overview[]">

											<option value="" selected="selected"></option>

											<option value="No History" <?= $utils->GetValueForUpdateSelect($history_arr, 'history_overview', 'No History')?>>

												No History
													
											</option>
									<?php
									foreach($historyResults as $key => $historyresult)
									{
									?>
											<option value="<?= $historyresult['test_result'];?>" <?= $utils->GetValueForUpdateSelect($history_arr, 'history_overview', $historyresult['test_result'])?>>

												<?= $historyresult['test_result'];?>
													
											</option>
									<?php	
									}
									?>
										</select>
									</div>


								</div>
								<div class="form-group row d-none">
									<div class="col-xs-12 col-sm-4 col-md-4">
										<label for="previous_positive_id" class="form-control-label">previous_positive_id:</label>
									</div>
									<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
										<input type="number" class="form-control" id="previous_positive_id" name="previous_positive_id[]" 
										<?php
										if (isset($history_arr[0]['previous_positive_id']))
										{
											echo 'value="'.$utils->GetValueForUpdateInput($history_arr, 'previous_positive_id').'"';
										}
										else
										{
											echo 'value="0"';
										}
										?>/>
									</div>
								</div>
								<div class="form-group row">
									<div class="col-xs-12 col-sm-4 col-md-4">
										<label for="history_mol_num" class="form-control-label">Historical Positive MD#:</label>
									</div>
									<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
										<input type="text" class="form-control" id="history_mol_num" name="history_mol_num[]" value="<?= $utils->GetValueForUpdateInput($history_arr, 'history_mol_num');?>"/>
									</div>
								</div>
								<div class="form-group row">
									<div class="col-xs-12 col-sm-4 col-md-4">
										<label for="history_soft_path_num" class="form-control-label">Historical Soft Path#:</label>
									</div>
									<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
										<input type="text" class="form-control" id="history_soft_path_num" name="history_soft_path_num[]" value="<?= $utils->GetValueForUpdateInput($history_arr, 'history_soft_path_num');?>"/>
									</div>
								</div>
								<div class="form-group row">
									<div class="col-xs-12 col-sm-4 col-md-4">
										<label for="history_test_tissue" class="form-control-label">Historical Test Tissue:</label>
									</div>								
									<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
										<input type="text" class="form-control" name="history_test_tissue[]" maxlength="30" list="history_test_tissue" value="<?= $utils->GetValueForUpdateInput($history_arr, 'history_test_tissue');?>"/>
										<datalist name="history_test_tissue[]">
										<?php
											for($i=0;$i<sizeof($all_tissues);$i++)
					               			{
										?>
												<option value="<?= $all_tissues[$i]['tissue']; ?>">
													<?= $all_tissues[$i]['tissue']; ?>
												</option>
										<?php
											}
										?>
										</datalist>
									</div>
								</div>
								<div class="form-group row">
									<div class="col-xs-12 col-sm-4 col-md-4">
										<label for="history_date" class="form-control-label">Historical Date:</label>
									</div>
									<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
										<input type="date" class="form-control" id="history_date" name="history_date[]" value="<?= $utils->GetValueForUpdateInput($history_arr, 'history_date');?>"/>
									</div>
								</div>
								<hr class="section-mark">
<?php
			}
		}

		// New test
		else
		{
?>
								<div class="form-group row">
									<div class="col-xs-12 col-sm-4 col-md-4">
										<label for="history_overview" class="form-control-label">History: <span class="required-field">*</span></label>
									</div>
									
									<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
										<select class="required-history-field" name="history_overview[]">

											<option value="" selected="selected"></option>

											<option value="No History" <?= $utils->GetValueForUpdateSelect($orderedTestArray, 'history_overview', 'No History')?>>

												No History
													
											</option>
									<?php
									foreach($historyResults as $key => $historyresult)
									{
									?>
											<option value="<?= $historyresult['test_result'];?>" <?= $utils->GetValueForUpdateSelect($orderedTestArray, 'history_overview', $historyresult['test_result'])?>>

												<?= $historyresult['test_result'];?>
													
											</option>
									<?php	
									}
									?>
										</select>
									</div>


								</div>
								<div class="form-group row d-none">
									<div class="col-xs-12 col-sm-4 col-md-4">
										<label for="previous_positive_id" class="form-control-label">previous_positive_id:</label>
									</div>
									<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
										<input type="number" class="form-control" id="previous_positive_id" name="previous_positive_id[]" 
										<?php
										if (isset($orderedTestArray[0]['previous_positive_id']))
										{
											echo 'value="'.$utils->GetValueForUpdateInput($orderedTestArray, 'previous_positive_id').'"';
										}
										else
										{
											echo 'value="0"';
										}
										?>/>
									</div>
								</div>
								<div class="form-group row">
									<div class="col-xs-12 col-sm-4 col-md-4">
										<label for="history_mol_num" class="form-control-label">Historical Positive MD#:</label>
									</div>
									<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
										<input type="text" class="form-control" id="history_mol_num" name="history_mol_num[]" value="<?= $utils->GetValueForUpdateInput($orderedTestArray, 'history_mol_num');?>"/>
									</div>
								</div>
								<div class="form-group row">
									<div class="col-xs-12 col-sm-4 col-md-4">
										<label for="history_soft_path_num" class="form-control-label">Historical Soft Path#:</label>
									</div>
									<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
										<input type="text" class="form-control" id="history_soft_path_num" name="history_soft_path_num[]" value="<?= $utils->GetValueForUpdateInput($orderedTestArray, 'history_soft_path_num');?>"/>
									</div>
								</div>
								<div class="form-group row">
									<div class="col-xs-12 col-sm-4 col-md-4">
										<label for="history_test_tissue" class="form-control-label">Historical Test Tissue:</label>
									</div>								
									<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
										<input type="text" class="form-control" name="history_test_tissue[]" maxlength="30" list="history_test_tissue" value="<?= $utils->GetValueForUpdateInput($orderedTestArray, 'history_test_tissue');?>"/>
										<datalist name="history_test_tissue[]" id="history_test_tissue">
										<?php
											for($i=0;$i<sizeof($all_tissues);$i++)
					               			{
										?>
												<option value="<?= $all_tissues[$i]['tissue']; ?>">
													<?= $all_tissues[$i]['tissue']; ?>
												</option>
										<?php
											}
										?>
										</datalist>
									</div>
								</div>
								<div class="form-group row">
									<div class="col-xs-12 col-sm-4 col-md-4">
										<label for="history_date" class="form-control-label">Historical Date:</label>
									</div>
									<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
										<input type="date" class="form-control" id="history_date" name="history_date[]" value="<?= $utils->GetValueForUpdateInput($orderedTestArray, 'history_date');?>"/>
									</div>
								</div>
								<hr class="section-mark">
<?php
		}	// end new test
?>					
							</div>
						</div>
					</div>
					<div id="ngs_required_fields" 
						<?php
							if (!isset($testInfo[0]['mol_num_required']) || $testInfo[0]['mol_num_required'] !== 'yes')
							{
								echo 'class="d-none"';
							}
						?>>
						<div class="form-group row">
							<div class="col-xs-12 col-sm-4 col-md-4">
								<label for="sample_type" class="form-control-label">Sample Type: <span class="required-field">*</span></label>
							</div>
							<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
								<label class="radio-inline"><input type="radio" name="sample_type" value="Patient Sample" checked required>Patient Sample</label>
								<label class="radio-inline"><input type="radio" name="sample_type" value="Patient Sample">CAP Sample</label>
								<label class="radio-inline"><input type="radio" name="sample_type" value="Validation">Validation</label>
								<label class="radio-inline"><input type="radio" name="sample_type" value="Sent Out">Sent Out</label>
							</div>
						</div>
						<div class="form-group row">
							<div class="col-xs-12 col-sm-4 col-md-4">
								<label for="first_name" class="form-control-label">First Name:</label>
							</div>
							<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
								<input type="text" class="form-control" id="first_name" name="first_name" maxlength="50" value="<?= $sampleLogBookArray[0]['first_name'];?>" readonly="readonly" title="Refer to edit sample log book to update the first name" />
							</div>
						</div>
						<!-- Not used as an update-able field because NGS tests are updated in visit and patient table -->
						<div class="form-group row">
							<div class="col-xs-12 col-sm-4 col-md-4">
								<label for="middle_name" class="form-control-label">Middle Name: </label>
							</div>
							<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
								<input type="text" class="form-control" id="middle_name" name="middle_name" maxlength="50"/>
							</div>
						</div>
						<div class="form-group row">
							<div class="col-xs-12 col-sm-4 col-md-4">
								<label for="last_name" class="form-control-label">Last Name:</label>
							</div>
							<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
								<input type="text" class="form-control" id="last_name" name="last_name" maxlength="50" value="<?= $sampleLogBookArray[0]['last_name'];?>" readonly="readonly" title="Refer to edit sample log book to update the last name"/>
							</div>
						</div>
						<div class="form-group row">
							<div class="col-xs-12 col-sm-4 col-md-4">
								<label for="medical_record_num" class="form-control-label">Medical Record Number:</label>
							</div>
							<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
								<input type="text" class="form-control" id="medical_record_num" name="medical_record_num" value="<?= $sampleLogBookArray[0]['medical_record_num'];?>" readonly="readonly" title="Refer to edit sample log book to update the medical record number"/>
							</div>
						</div>
						<!-- Not used as an update-able field because NGS tests are updated in visit and patient table -->

						<div class="form-group row">
							<div class="col-xs-12 col-sm-4 col-md-4">
								<label for="dob" class="form-control-label">Date of Birth: <span class="required-field">*</span></label>
							</div>
							<div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
								<input class="form-control <?php 
								if(!isset($sampleLogBookArray[0]['dob']))
								{
									echo 'date-picker" type="date"';
								}

								?>" id="dob" name="dob" <?php
								if(isset($sampleLogBookArray[0]['dob']))
								{
									echo 'value="'.$dfs->ChangeDateFormatUS($sampleLogBookArray[0]['dob'], FALSE).'"'; 
									echo ' readonly="readonly" title="Refer to edit sample log book to update the dob" type="text"';
								}
								?> placeholder="MM/DD/YYYY" pattern="\d{1,2}/\d{1,2}/\d{4}" />
							</div>
						</div>


						<!-- Not used as an update-able field because NGS tests are updated in visit and patient table -->
						<div class="form-group row">
							<div class="col-xs-12 col-sm-4 col-md-4">
								<label for="sex" class="form-control-label">Sex: <span class="required-field">*</span></label>
							</div>
							<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
								<?php
								if (isset($sampleLogBookArray[0]['sex']) && !empty($sampleLogBookArray[0]['sex']))
								{
									echo $sampleLogBookArray[0]['sex'];
								}
								?>
								<div class="radio<?php
									if (isset($sampleLogBookArray[0]['sex']) && !empty($sampleLogBookArray[0]['sex']))
									{
										echo ' d-none';
									}
								?>">
									<label class="radio-inline"><input id="sex-M" type="radio" name="sex" value="M" <?php
									if(isset($sampleLogBookArray[0]['sex']) && $sampleLogBookArray[0]['sex'] == 'M')
									{
										echo 'checked'; 								
									}

									if(isset($sampleLogBookArray[0]['sex']))
									{
										echo ' onclick="return false;"';
									}
									?>>Male</label>
								
									<label class="radio-inline"><input type="radio" name="sex" value="F" <?php
									if(isset($sampleLogBookArray[0]['sex']) && $sampleLogBookArray[0]['sex'] == 'F')
									{
										echo 'checked'; 
									}

									if(isset($sampleLogBookArray[0]['sex']))
									{
										echo ' onclick="return false;"';
									}
									?>>Female</label>

									<label class="radio-inline"><input type="radio" name="sex" value="U" <?php
									if(isset($sampleLogBookArray[0]['sex']) && $sampleLogBookArray[0]['sex'] == 'U')
									{
										echo 'checked'; 
									}

									if(isset($sampleLogBookArray[0]['sex']))
									{
										echo ' onclick="return false;"';
									}
									?>>Not Known</label>
								</div>
								
							</div>

						</div>
						
						<div class="form-group row">							
											
							<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
								<label for="order_num" class="form-control-label">mol#: </label> 
							</div>
							
							<div class="col-xs-10 col-sm-6 col-md-6 col-lg-6 pull-left">
								<input type="text" class="form-control" id="order_num" name="order_num" readonly maxlength="50" value="<?= $utils->GetValueForUpdateInput($sampleLogBookArray, 'order_num');?>"/>
							</div>
							<?php
							// The order num sometimes assigns the two samples the same order num if two users are adding tests at the same time. Unfortunately since we do not want any numbers skipped it makes it a little harder to automatically solve this problem. I had to fix this right before I left the job so I'm adding a workaround for the manager to edit the number after it happens because of time.

						    // Notes about order_num  
						    	// Each sample only gets one order_num
								// No numbers can be skipped
								// Format is year-MOL# (21-MOL8)
								// Every test that obtains are report via Infotrack gets an order_num
								// The lab will call this a mol# not an order_num
							    	// mol_num and order_num was mixed up because I didn't realize there 
							    	// was a md# and mol_num and mol_num already was being used for md#
								// New order nums are assigned in utils.js _getNextMolNum()
							if (
									isset($sampleLogBookArray[0]['order_num']) && 
									!empty($sampleLogBookArray[0]['order_num']) &&
									isset($_GET['visit_id']) &&
									isset($user_permssions) && 
									(
										strpos($user_permssions, 'manager') !== false ||
										strpos($user_permssions, 'admin') !== false
									) &&
									sizeof($num_tests_with_order_num) === 1
								)
							{

							?>
							<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 ml-0 pl-0">
								<button id="assign-new-mol-num" type="button" class="btn btn-danger btn-danger-hover remove-cloned-input" title="This button will allow you to assign the next new mol#.  Only one mol# can be assigned per sample however.">
				    				Assign next mol#
				        		</button>
							</div>
							<?php
							}
							else if (
										isset($sampleLogBookArray[0]['order_num']) && 
										!empty($sampleLogBookArray[0]['order_num']) &&
										isset($_GET['visit_id']) &&
										sizeof($num_tests_with_order_num) > 1
									)
							{
							?>
							<div class="alert alert-info col-xs-2 col-sm-2 col-md-2 col-lg-2 ml-0 pl-0">
								This mol# can not be updated because multiple tests use the same mol#. 
				        		</button>
							</div>
							<?php
							}
							else if (
										isset($sampleLogBookArray[0]['order_num']) && 
										!empty($sampleLogBookArray[0]['order_num']) &&
										isset($_GET['visit_id']) &&
										isset($user_permssions) && 
										(
											strpos($user_permssions, 'manager') === false &&
											strpos($user_permssions, 'admin') === false
										)
									)
							{
							
							?>
							<div class="alert alert-info col-xs-2 col-sm-2 col-md-2 col-lg-2 ml-0 pl-0">
								Users with admin or manger permission settings can update mol# for samples that have only one reportable test. 
				        		</button>
							</div>
							<?php
							}
							?>
						</div>
						<div class="form-group row">
							<div class="col-xs-12 col-sm-4 col-md-4">
								<label for="mol_num" class="form-control-label">MD#:</label>
							</div>
							<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
								<input type="text" class="form-control" id="mol_num" name="mol_num" value="<?= $sampleLogBookArray[0]['mol_num'];?>" readonly="readonly" />
							</div>
						</div>

						<div class="form-group row">
							<div class="col-xs-12 col-sm-4 col-md-4">
								<label for="soft_lab_num" class="form-control-label">Soft Lab #: </label> <span class="required-field">*</span></label>
							</div>
							<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
								<input type="text" class="form-control no-spaces" id="soft_lab_num" value="<?= $sampleLogBookArray[0]['soft_lab_num'];?>" name="soft_lab_num" maxlength="50"/>
							</div>
						</div>

						<div class="form-group row">
							<div class="col-xs-12 col-sm-4 col-md-4">
								<label for="soft_path_num" class="form-control-label">Soft Path #:</label>
							</div>
							
							<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
								<input type="text" class="form-control" id="soft_path_num" maxlength="50" name="soft_path_num" value="<?= $utils->GetValueForUpdateInput($sampleLogBookArray, 'soft_path_num');?>"/>
							</div>
						</div>

						<!-- Not used as an update-able field because NGS tests are updated in visit and patient table -->
						<div class="form-group row">
							<div class="col-xs-12 col-sm-4 col-md-4">
								<label for="account_num" class="form-control-label">Account #: </label>
							</div>
							<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
								<input type="text" class="form-control" id="account_num" name="account_num" maxlength="50" value="<?= $utils->GetValueForUpdateInput($visitArray, 'account_num');?>"/>
							</div>
						</div>
						<!-- Not used as an update-able field because NGS tests are updated in visit and patient table -->
						<div class="form-group row">
							<div class="col-xs-12 col-sm-4 col-md-4">
								<label for="chart_num" class="form-control-label">Chart #: </label>
							</div>
							<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
								<input type="text" class="form-control" id="chart_num" name="chart_num" maxlength="50" value="<?= $utils->GetValueForUpdateInput($visitArray, 'chart_num');?>"/>
							</div>
						</div>
						<!-- Not used as an update-able field because NGS tests are updated in visit and patient table -->
						<div class="form-group row">
							<div class="col-xs-12 col-sm-4 col-md-4">
								<label for="collected" class="form-control-label">Collected: </label>
							</div>
							<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
								<input class="form-control" type="datetime-local" id="collected" name="collected" placeholder="MM/DD/YYYY HH:MM:SS" <?php
								if(isset($visitArray[0]['collected']) && $visitArray[0]['collected'] !== DEFAULT_DATE_TIME)
								{
									echo 'value="'.$dfs->convertToUTC($visitArray[0]['collected']).'"'; 
								}
								?>/>
							</div>
						</div>
						<!-- Not used as an update-able field because NGS tests are updated in visit and patient table -->

						<div class="form-group row">
							<div class="col-xs-12 col-sm-4 col-md-4">
								<label for="received" class="form-control-label">Received: </label>
							</div>
							<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
								<input class="form-control" type="datetime-local" id="received" name="received" placeholder="MM/DD/YYYY HH:MM:SS" <?php
								if(isset($visitArray[0]['received']) && $visitArray[0]['received'] !== DEFAULT_DATE_TIME)
								{
									echo 'value="'.$dfs->convertToUTC($visitArray[0]['received']).'"'; 
								}
								else
								{
									echo date('Y-m-d\TH:i:s');
								}
								?>/>
							</div>


						</div>

						<!-- Not used as an update-able field because NGS tests are updated in visit and patient table -->
						<div class="form-group row">
							<div class="col-xs-12 col-sm-4 col-md-4">
								<label for="primary_tumor_site" class="form-control-label">Primary Site: </label>
							</div>
							<div class="col-xs-12 col-sm-8 col-md-8 pull-left">

								<select class="form-control" name="primary_tumor_site" id="primary_tumor_site">
								<?php

									
									for($i=0;$i<sizeof($all_tissues);$i++)
			               			{
								?>
										<option value="<?= $all_tissues[$i]['tissue']; ?>">
											<?= $all_tissues[$i]['tissue']; ?>
										</option>
								<?php
									}
									
									echo '<option value="" selected="selected">Unknown</option>';
									
								?>
								</select>

							</div>
						</div>

						<div class="form-group row">
							<div class="col-xs-12 col-sm-4 col-md-4">
								<label for="diagnosis_id" class="form-control-label">Tumor Type: </label>
							</div>
							<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
								<select class="form-control" name="diagnosis_id" id="diagnosis_id">

								<?php
									for($i=0;$i<sizeof($possible_diagnosis);$i++)
			               			{	
								?>
										<option value="<?= $possible_diagnosis[$i]['tumor_id']; ?>">
											<?= $possible_diagnosis[$i]['tumor']; ?>
										</option>

								<?php
									}							
								?>	
										<option value="6" selected="selected">Unknown</option>								
								</select>
							</div>
						</div>	
										
						<div class="form-group row">
							<div class="col-xs-12 col-sm-4 col-md-4">
								<label for="test_tissue" class="form-control-label">Tissue Tested:</label>
							</div>
							<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
								<input type="text" class="form-control" name="test_tissue" maxlength="30" list="test_tissue" value="<?= $utils->GetValueForUpdateInput($sampleLogBookArray, 'test_tissue');?>"/>
								<datalist name="test_tissue" id="test_tissue">
								<?php


									for($i=0;$i<sizeof($all_tissues);$i++)
			               			{
								?>
										<option value="<?= $all_tissues[$i]['tissue']; ?>">
											<?= $all_tissues[$i]['tissue']; ?>
										</option>
								<?php
									}
								?>
								</datalist>
							</div>
						</div>
						
						<div class="form-group row">
							<div class="col-xs-12 col-sm-4 col-md-4">
								<label for="block" class="form-control-label">Block: </label>
							</div>
							<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
								<input type="text" class="form-control" id="block" name="block" maxlength="10" value="<?= $utils->GetValueForUpdateInput($sampleLogBookArray, 'block');?>"/>
							</div>
						</div>
						<div class="form-group row">
							<div class="col-xs-12 col-sm-4 col-md-4">
								<label for="tumor_cellularity" class="form-control-label">Neoplastic Cellularity: </label>
							</div>
							<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
								<input type="number" class="form-control" id="tumor_cellularity" name="tumor_cellularity" min="0" max="100" value="<?= $utils->GetValueForUpdateInput($sampleLogBookArray, 'tumor_cellularity');?>" step=".01"/>
							</div>
						</div>					
						<div class="form-group row">
							<div class="col-xs-12 col-sm-4 col-md-4">
								<label for="req_physician" class="form-control-label">Requesting physician: <span class="required-field">*</span></label>
							</div>
							<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
								<input type="text" class="form-control" id="req_physician" name="req_physician" maxlength="78" value="<?= $utils->GetValueForUpdateInput($visitArray, 'req_physician');?>"/>
							</div>
						</div>
						<div class="form-group row">
							<div class="col-xs-12 col-sm-4 col-md-4">
								<label for="req_loc" class="form-control-label">Requesting Location: </label>
							</div>
							<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
								<input type="text" class="form-control" id="req_loc" name="req_loc" maxlength="78" value="<?= $utils->GetValueForUpdateInput($visitArray, 'req_loc');?>"/>
							</div>
						</div>				
					</div>		

					<div id="consent_required"
						<?php
							if (!isset($testInfo[0]['consent_required']) || $testInfo[0]['consent_required'] !== 'yes')
							{
								echo 'class="d-none"';
							}
						?>>
						<div class="row">
							<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
								<div class="alert alert-info">
									<span class="fas fa-exclamation-triangle"></span>Note: If consent comes in under an alias, make sure to note this in the comments section below
								</div>
							</div>
						</div>
						<div class="form-group row">
							<div class="col-xs-12 col-sm-4 col-md-4">
								<label for="consent_info" class="form-control-label">Consent type: <span class="required-field">*</span></label>
							</div>
							<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
								<div class="radio">
									<label class="radio-inline">
										<input type="radio" name="consent_info" id="consent_info_first_input" value="No Consent" <?= $utils->GetValueForUpdateRadioCBX($orderedTestArray, 'consent_info', 'No Consent');?>>No Consent
									</label>
									<label class="radio-inline">
										<input type="radio" name="consent_info" value="Received with sample" <?= $utils->GetValueForUpdateRadioCBX($orderedTestArray, 'consent_info', 'Received with sample');?>>Received with sample</label>
									<label class="radio-inline">
										<input type="radio" name="consent_info"  value="File with provider" <?= $utils->GetValueForUpdateRadioCBX($orderedTestArray, 'consent_info', 'File with provider');?>>File with provider</label>

									<!-- On July 21st, 2020 I was notified that Onbase was on longer being used.  However, the exact date of abandonment of Onbase was unknown.  Therefore there will be some tests labeled as Onbase when it was soft media. -->
									<!-- <label class="radio-inline">
										<input type="radio" name="consent_info" value="Onbase check found" <?= $utils->GetValueForUpdateRadioCBX($orderedTestArray, 'consent_info', 'Onbase check found');?>>Onbase check found</label> -->
									<label title="On July 21st, 2020 <?= ADMINS;?> was notified that Onbase was on longer being used.  However, the exact date of abandonment of Onbase was unknown.  Therefore there will be some tests labeled as Onbase when it was soft media."class="radio-inline">
										<input type="radio" name="consent_info" value="Scanned Document Found" <?= $utils->GetValueForUpdateRadioCBX($orderedTestArray, 'consent_info', 'Soft Media Check Found');?>>Soft Media Check Found</label>

									<label class="radio-inline">
										<input type="radio" name="consent_info" value="Consent Faxed" <?= $utils->GetValueForUpdateRadioCBX($orderedTestArray, 'consent_info', 'Consent Faxed');?>>Consent Faxed</label>

									<label class="radio-inline">
										<input type="radio" name="consent_info" value="Hard Copy Received Later" <?= $utils->GetValueForUpdateRadioCBX($orderedTestArray, 'consent_info', 'Hard Copy Received Later');?>>Hard Copy Received Later</label>
								</div>
							</div>
						</div>

						<?php
						/////////////////////////////////
						// On March 15th 2021 consent_for_research field was discontinued from collection via 
						// infotrack because it was no longer being collected on the consent form.  Dan Bach 
						// requested for its removal.  Since this form is updatable this field is only deactivated 
						// going forward so old data can still be viewed.
						////////////////////////////////
						if (
								isset($sampleLogBookArray[0]['date_received']) &&
								strtotime($sampleLogBookArray[0]['date_received']) < strtotime('2021-03-15')
							)
						{

						?>
						<div class="form-group row">
							<div class="col-xs-12 col-sm-4 col-md-4">
								<label for="consent_for_research" class="form-control-label">Consented to Save for Research: <span class="required-field">*</span></label>
							</div>
							<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
								<div class="radio">
									<label class="radio-inline">
										<input type="radio" name="consent_for_research" id="consent_for_research_first_input" value="No" <?= $utils->GetValueForUpdateRadioCBX($orderedTestArray, 'consent_for_research', 'No');?>>No
									</label>
									<label class="radio-inline">
										<input type="radio" name="consent_for_research" value="Yes" <?= $utils->GetValueForUpdateRadioCBX($orderedTestArray, 'consent_for_research', 'Yes');?>>Yes
									</label>
								</div>
							</div>
						</div>
						<?php
						}
						?>
						<div class="alert alert-info">
						
							If no consent has been received Consent Date should be left blank.  When we receive the consent you should go back and fill this field in with the date we received the consent.
							<br><br>
							If consent comes with the sample fill it in as the day you logged in the sample.								
							
						</div>
						<div class="form-group row">
							<div class="col-xs-12 col-sm-4 col-md-4">
								<label for="consent_date" class="form-control-label">Date Consent Received:</label>
							</div>
							<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
								<input type="date" class="form-control" id="consent_date" name="consent_date" value="<?= $utils->GetValueForUpdateInput($orderedTestArray, 'consent_date');?>"/>
							</div>
						</div>
					</div>

					<?php
						require_once('templates/shared_layouts/previous_comments.php');
					?>
					<div class="form-group row">
						<div class="col-xs-12 col-sm-4 col-md-4">
							<label for="problems" class="form-control-label">Comments/Sample Identifiers/Notes:</label>
						</div>

						<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
							<textarea class="form-control" name="comments" rows="4" maxlength="65535" style="margin-bottom: 10px;"></textarea>
						</div>
					</div>
					<div class="row d-none">
						<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
							<input type="number" class="form-control" id="ordered_test_id" name="ordered_test_id" value="<?= $utils->GetValueForUpdateInput($orderedTestArray, 'ordered_test_id');?>"/>
						</div>
					</div>
					<div class="form-group row">
						<div class="offset-xs-4 offset-sm-4 offset-md-4 offset-lg-4 col-xs-2 col-sm-2 col-md-2 col-lg-2">
							<button type="submit" id="<?= $page;?>_submit" name="<?= $page;?>_submit" value="Submit"  class="btn btn-primary btn-primary-hover">
								Submit
							</button>
						</div>
					</div>
				</fieldset>
			</form>
		</div>
	</div>
</div>
<div class="clones d-none">
	<div id="div-to-clone-history-section-field">
		<div class="form-group row d-none">
			<div class="col-xs-12 col-sm-4 col-md-4">
				<label for="previous_positive_id" class="form-control-label">previous_positive_id:</label>
			</div>
			<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
				<input type="number" class="form-control" name="previous_positive_id[]" value="0"/>
			</div>
		</div>
		<div class="form-group row">
			

			<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
				<label for="history_overview" class="form-control-label">History: <span class="required-field">*</span></label>
			</div>

			<div class="col-xs-12 col-sm-8 col-md-7 col-lg-7">
				<select name="history_overview[]" required="required">

					<option value="" selected="selected"></option>

					<option value="No History">

						No History
							
					</option>
			<?php
			foreach($historyResults as $key => $historyresult)
			{
			?>
					<option value="<?= $historyresult['test_result'];?>">

						<?= $historyresult['test_result'];?>
							
					</option>
			<?php	
			}
			?>
				</select>
			</div>


			<div class="col-xs-1 col-sm-1 col-md-1 col-lg-1 ml-0 pl-0">
				<button id="clone-history-section-remove-btn" type="button" class="btn btn-danger btn-danger-hover remove-cloned-input" title="delete this history section.">
    				<span class="fas fa-minus-circle"></span>
        		</button>
			</div>
		</div>
		<div class="form-group row">
			<div class="col-xs-12 col-sm-4 col-md-4">
				<label for="history_mol_num" class="form-control-label">Historical Positive MD#:</label>
			</div>
			<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
				<input type="text" class="form-control" name="history_mol_num[]"/>
			</div>
		</div>
		<div class="form-group row">
			<div class="col-xs-12 col-sm-4 col-md-4">
				<label for="history_soft_path_num" class="form-control-label">Historical Soft Path#:</label>
			</div>
			<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
				<input type="text" class="form-control" name="history_soft_path_num[]"/>
			</div>
		</div>
		<div class="form-group row">
			<div class="col-xs-12 col-sm-4 col-md-4">
				<label for="history_test_tissue" class="form-control-label">Historical Test Tissue:</label>
			</div>								
			<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
				<input type="text" class="form-control" name="history_test_tissue[]" maxlength="30" list="history_test_tissue"/>
				<datalist name="history_test_tissue[]">
				<?php
					for($i=0;$i<sizeof($all_tissues);$i++)
           			{
				?>
						<option value="<?= $all_tissues[$i]['tissue']; ?>">
							<?= $all_tissues[$i]['tissue']; ?>
						</option>
				<?php
					}
				?>
				</datalist>
			</div>
		</div>
		<div class="form-group row">
			<div class="col-xs-12 col-sm-4 col-md-4">
				<label for="history_date" class="form-control-label">Historical Date:</label>
			</div>
			<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
				<input type="date" class="form-control" name="history_date[]"/>
			</div>
		</div>
		<hr class="section-mark">
	</div>
</div>	