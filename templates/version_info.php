<div class="container-fluid">

<?php
     if 	(
     		isset($user_permssions) && 
     		(
     			strpos($user_permssions, 'manager') !== false ||
     			strpos($user_permssions, 'admin') !== false ||
     			strpos($user_permssions, 'director') !== false
     		)
     	)
     {                        
?>
	<div class="row">
		<div class="col-md-9 col-lg-9">

		</div>
		<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
			<a href="?page=dev_progress_report" type="button" class="btn btn-info btn-info-hover">
                    Development Progress Report
               </a>
		</div>
	</div>
<?php
	}
     if (isset($user_permssions) && strpos($user_permssions, 'admin') !== false)
     {                        
?>	
	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			<form id="add_version" class="form" method="post" class="form-horizontal">
				<fieldset>
					<legend>
						Add Version
					</legend>

					<div class="form-group row">
						<div class="col-xs-12 col-sm-4 col-md-4">
							<label for="version_comment" class="form-control-label">Version Comment: </label>
						</div>
						<div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">
							<textarea id="version_comment" name="version_comment" maxlength="255" rows="4" class="form-control" required="required"></textarea>
						</div>
					</div>
					<div class="form-group row">
						<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
							<label for="version_num" class="form-control-label">Version Number:</label>
						</div>
						<div class="col-xs-1 col-sm-1 col-md-1 col-lg-1">
							<strong>V</strong>
						</div>
						<div class="col-xs-1 col-sm-1 col-md-1 col-lg-1">
							<input type="number" class="form-control" id="version_num_a" name="version_num_a" value="<?= $versionArray[0]['version_num_a'];
							?>" required="required"/>
						</div>
						<div class="col-xs-1 col-sm-1 col-md-1 col-lg-1">
							<strong>.</strong>
						</div>
						<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
							<input type="number" class="form-control" id="version_num_b" name="version_num_b" value="<?= $versionArray[0]['version_num_b'];
							?>" required="required"/>
						</div>
						<div class="col-xs-1 col-sm-1 col-md-1 col-lg-1">
							<strong>.</strong>
						</div>
						<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
							<input type="number" class="form-control" id="version_num_c" name="version_num_c" value="<?= $versionArray[0]['version_num_c'] + 1;
							?>" required="required"/>
						</div>
					</div>
					<div id="form-control-fields">
						<div class="form-group row">
							<div class="col-xs-12 col-sm-4 col-md-4">
								<label for="version_diagram" class="form-control-label">Version Diagram:</label>
							</div>
							<div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">
								
								<select class="selectpicker" data-live-search="true" data-width="100%" name="version_diagram" id="version_diagram">

									<option value="" selected="selected"></option>
								<?php

									foreach ($all_possible_diagrams as $key => $diagram)
			               			{	
			               				if ($diagram !== 'index.html')
			               				{
								?>
											<option value="<?= $diagram; ?>">
													<?= $diagram; ?>
											</option>
								<?php
										}
									}
								?>
								</select>								
							</div>
						</div>
					</div>
					<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
						<button type="submit" id="<?= $page;?>_submit" name="<?= $page;?>_submit" value="Submit"  class="btn btn-primary btn-primary-hover">
							submit
						</button>
					</div>
				</fieldset>
			</form>
		</div>
	</div>
<?php
	}
?>	




	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			<fieldset>
				<legend>
					Version History
				</legend>
				<ul>
	<?php

		foreach ($versionArray as $key => $version)
		{
	?>
					<li style="margin-bottom: 4px;">
						<b><?= $version['version'];?></b> <?= $version['version_comment'];?> Updated by:<b><?= $version['user_name'];?></b> <i><?= $version['time_stamp'];?></i>
	<?php
		if (isset($version['version_diagram']) && !empty($version['version_diagram']) && isset($user_permssions) && strpos($user_permssions, 'admin') !== false)
		{
	?>	
						<div class="row">
							<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
								<img src="about/diagrams/<?=$version['version_diagram'];?>" alt="version diagram">
							</div>
						</div>
	<?php		
		}
		
	?>
					</li>	
	<?php
		}
	?>			
				</ul>
			</fieldset>
		</div>
	</div>

</div>