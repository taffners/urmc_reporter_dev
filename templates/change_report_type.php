<?php
	require_once('templates/lists/run_info_list.php');
?>

<div class="container-fluid">
	<div class="row">
		<?php
			require_once('templates/shared_layouts/report_progress_bar.php');
		?>

			<div id="toggle-report-progress-bar" class="col-xs-10 col-sm-10 col-md-10 col-lg-10">
				<form class="form" method="post" class="form-horizontal">
					<fieldset>
					
						<legend id='new_legend' class='show'>
							Change Report Type
						</legend>

						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 alert alert-info">
							<h1>There are 3 different report types currently</h1>
							<ul style="font-size:18px;">
								<li <?= isset($runInfoArray[0]['report_type']) && $runInfoArray[0]['report_type'] == 'reportable'? 'style="border-style: dotted;"': ''?>>Reportable
									<ul>
										<li>Default for samples</li>
										<li>Mark Reports that will be added to a patient chart as Reportable.</li>
										<li>An automatic email notification will be sent to <?= $softpath_users;?> once completed to add the report to the patient record.</li>
										<li>The report will be added to a patient chart pending list so <?= $softpath_users;?> can add it.</li>
									</ul>
								</li>
								<li <?= isset($runInfoArray[0]['report_type']) && $runInfoArray[0]['report_type'] == 'validation'? 'style="border-style: dotted;"': ''?>>Validation
									<ul>
										<li>Used for samples that will not be reported out.</li>
										<li>Sample will not trigger an automatic notification once complete and it will not be added to the patient record pending list</li>
									</ul>
								</li>
								<li <?= isset($runInfoArray[0]['report_type']) && $runInfoArray[0]['report_type'] == 'na'? 'style="border-style: dotted;"': ''?>>na
									<ul>
										<li>Used for Control Samples</li>
										<li>Used for reports started before July 1st, 2020.</li>
										<li>This functionality was added on July 1st, 2020 and it is unknown which reports were used as validation and which were reported into a patient chart from these reports.</li>
									</ul>
								</li>
							</ul>
						</div>
						
						<div class="form-group row" style="font-size:18px;">
							<div class="col-xs-12 col-sm-3 col-md-2 col-lg-2">
								<label for="report_type" class="form-control-label">Report Type: <span class="required-field">*</span></label>
							</div>
							<div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">
								<div class="radio">
									<label class="radio-inline"><input type="radio" name="report_type" required="required" value="reportable" <?php
									if(isset($runInfoArray[0]['report_type']) && $runInfoArray[0]['report_type'] == 'reportable')
									{
										echo 'checked'; 								
									}
									?>>Reportable</label>
								
									<label class="radio-inline"><input type="radio" name="report_type" value="validation" <?php
									if(isset($runInfoArray[0]['report_type']) && $runInfoArray[0]['report_type'] == 'validation')
									{
										echo 'checked'; 								
									}
									?>>Validation</label>
									<label class="radio-inline"><input type="radio" name="report_type" value="na" <?php
									if(isset($runInfoArray[0]['report_type']) && $runInfoArray[0]['report_type'] == 'na')
									{
										echo 'checked'; 								
									}
									?>>na</label>
								</div>
							</div>
						</div>
						<div class="form-group row">
							<div class="offset-sm-3 offset-md-2 offset-lg-2 col-xs-12 col-sm-9 col-md-9 col-lg-9">
								<button type="submit" id="<?= $page;?>_submit" name="<?= $page;?>_submit" value="Submit"  class="btn btn-primary btn-primary-hover">
									Submit
								</button>
							</div>
						</div>

					</fieldset>
				</form>
			</div>
		</div>
	</div>
</div>
