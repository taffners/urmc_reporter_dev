<div class="container-fluid">
	<div class="row hidden-print" id="control-view-buttons" >
		<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
			<button type="button" id="all-btn-id" class="btn btn-primary toggle-view toggle-groups" data-toggle-view-ids="sams,phils" data-hide="" data-show="completed,pending,not-discussed">All</button>
		</div>
		<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
			<button type="button" id="sams-btn-id" class="btn btn-primary toggle-view" data-toggle-view-ids="sams,phils">Samantha</button>
		</div>
		<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
			<button type="button" id="phils-btn-id" class="btn btn-primary toggle-view" data-toggle-view-ids="sams,phils">Phil</button>
		</div>
	</div>
	<div class="row  hidden-print" id="control-view-buttons">
		<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
			<button type="button" id="pending-toggle-groups" class="btn btn-primary toggle-groups" data-hide="completed,not-discussed" data-show="pending">pending</button>
		</div>
		<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
			<button type="button" id="completed-toggle-groups" class="btn btn-primary toggle-groups" data-hide="not-discussed,pending" data-show="completed">Completed</button>
		</div>
		<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
			<button type="button" id="not-discussed-toggle-groups" class="btn btn-primary toggle-groups" data-hide="completed,pending" data-show="not-discussed">Need to Discuss at Lab Meeting</button>
		</div>
	</div>
	<div class="row" id="sams-view">
<?php
	require_once('templates/shared_layouts/sams_progress.php');
?>
	</div>

	<div class="row" id="phils-view">
<?php
	require_once('templates/shared_layouts/phils_progress.php');
?>
	</div>
</div>
