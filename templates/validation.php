<div class="container-fluid">
	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
			<a href="?page=upload_validation_document&instrument_id=<?=$_GET['instrument_id'];?>" class="btn btn-primary btn-primary-hover">Upload Validation Document</a>
		</div>

		<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
			<a href="?page=setup_instruments" class="btn btn-primary btn-primary-hover" role="button">				
				All Instruments
			</a>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			<table class="formated_table sort_table" style="overflow-x:auto;margin-bottom: 10px;">
				<thead>
					<th>Validation Date</th>
					<th>Validation Document</th>
					<th>Time Stamp</th>
					<th>Last Updated By</th>
					<th>Update</th>
				</thead>
				<tbody>
<?php 

			foreach ($validationArray as $key => $validation)
			{
?>
					<tr>
						<td><?= $validation['validation_date'];?></td>
						<td>
							<?= $validation['validation_docs'];?> 
							<a href="?page=download_validation_docs&instrument_validation_id=<?=$validation['instrument_validation_id'];?>" class="btn btn-primary btn-primary-hover">
								Download Docs
							</a>
						</td>
						<td><?= $validation['time_stamp'];?></td>
						<td><?= $validation['user_name'];?></td>
						<td>
							<a href="?page=upload_validation_document&instrument_validation_id=<?=$validation['instrument_validation_id'];?>&instrument_id=<?=$validation['instrument_id'];?>" class="btn btn-primary btn-primary-hover">
								Update Validation
							</a>
						</td>
					</tr>
<?php 
			}
?>					

				</tbody>
			</table>
		</div>
	</div>
</div>