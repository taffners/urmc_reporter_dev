<div class="container-fluid">
	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="height:500px;">
			<form class="form" method="post" class="form-horizontal">
				<fieldset>
					<legend>
						<?= $page_title;?>
					</legend>
					<div class="form-group row">
						<div class="col-xs-12 col-sm-4 col-md-4">
							<label for="step_name" class="form-control-label">Step Name <span class="required-field">*</span></label>
						</div>

						<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
							<input type="text" class="form-control no-spaces" id="step_name" name="step_name" maxlength="40" required="required" value="<?= $utils->GetValueForUpdateInput($curr_reporting_flow_steps, 'step_name');?>"/>
						</div>
					</div>

					<div class="form-group row">
						<div class="col-xs-12 col-sm-4 col-md-4">
							<label for="description" class="form-control-label">Description:</label>
						</div>

						<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
							<textarea class="form-control" name="description" rows="4" maxlength="65535" style="margin-bottom: 10px;"><?= $utils->GetValueForUpdateInput($curr_reporting_flow_steps, 'description');?></textarea>
						</div>
					</div>
					<div class="form-group row">
						<div class="col-xs-12 col-sm-4 col-md-4">
							<label for="flow_type" class="form-control-label">Flow Type: <span class="required-field">*</span></label>
						</div>
						<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
							<div class="radio">
								<label class="radio-inline"><input type="radio" name="flow_type" value="pre" required="required" <?= $utils->DefaultRadioBtn($curr_reporting_flow_steps, 'flow_type', 'pre');?>>Pre</label>
							
								<label class="radio-inline"><input type="radio" name="flow_type" value="reporting"  <?= $utils->GetValueForUpdateRadioCBX($curr_reporting_flow_steps, 'flow_type', 'reporting');?>>Reporting</label>

							</div>
						</div>
					</div>
					<div class="form-group row">
						<div class="col-xs-12 col-sm-4 col-md-4">
							<label for="no_variant_status" class="form-control-label">View status if no variant is present in report: <span class="required-field">*</span></label>
						</div>
						<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
							<div class="radio">
								<label class="radio-inline"><input type="radio" name="no_variant_status" value="show" required="required" <?= $utils->DefaultRadioBtn($curr_reporting_flow_steps, 'no_variant_status', 'show');?>>Show</label>
							
								<label class="radio-inline"><input type="radio" name="no_variant_status" value="hide"  <?= $utils->GetValueForUpdateRadioCBX($curr_reporting_flow_steps, 'no_variant_status', 'hide');?>>Hide</label>
								<label class="radio-inline"><input type="radio" name="flow_type" value="n/a"  <?= $utils->GetValueForUpdateRadioCBX($curr_reporting_flow_steps, 'n/a', 'reporting');?>>N/A</label>
							</div>
						</div>
					</div>
					<div class="form-group row">
						<div class="col-xs-12 col-sm-4 col-md-4">
							<label for="step_status" class="form-control-label">Step Status: <span class="required-field">*</span></label>
						</div>
						<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
							<div class="radio">
								<label class="radio-inline"><input type="radio" name="step_status" value="active" required="required" <?= $utils->DefaultRadioBtn($curr_reporting_flow_steps, 'step_status', 'active');?>>Active</label>
							
								<label class="radio-inline"><input type="radio" name="step_status" value="not active"  <?= $utils->GetValueForUpdateRadioCBX($curr_reporting_flow_steps, 'step_status', 'not active');?>>Not Active</label>
							</div>
						</div>
					</div>
					<div class="form-group row">
						<div class="offset-xs-4 offset-sm-4 offset-md-4 offset-lg-4 col-xs-2 col-sm-2 col-md-2 col-lg-2">
							<button type="submit" id="<?= $page;?>_submit" name="<?= $page;?>_submit" value="Submit"  class="btn btn-primary btn-primary-hover">
								Submit
							</button>
						</div>
					</div>	
				</fieldset>
			</form>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="height:500px;">
			<form class="form" method="post" class="form-horizontal">
				<fieldset>
					<legend>
						Previously added reporting steps
					</legend>
				<?php
				if (isset($all_reporting_steps) && !empty($all_reporting_steps))
				{
				?>
					<table class="formated_table sort_table_no_inital_sort_no_paging">
						<thead>
							<th>ID</th>
							<th>Step Name</th>
							<th>Description</th>
							<th>no_variant_status</th>
							<th>flow_type</th>
							<th>step_status</th>
							<th>Update</th>
						</thead>
						<tbody>
						<?php
						foreach($all_reporting_steps as $key => $step)
						{
						?>
							<tr>
								<td><?= $step['reporting_flow_steps_id'];?></td>
								<td><?= $step['step_name'];?></td>
								<td><?= $step['description'];?></td>
								<td><?= $step['no_variant_status'];?></td>
								<td><?= $step['flow_type'];?></td>
								<td><?= $step['step_status'];?></td>
								<td>
									<a href="?page=add_reporting_flow_step&reporting_flow_steps_id=<?= $step['reporting_flow_steps_id'];?>" class="btn btn-primary btn-primary-hover">Update</a>

								</td>
							</tr>
						<?php
						}
						?>

						</tbody>
                    </table>
                <?php
            	}
                ?>
				</fieldset>
			</form>
		</div>
	</div>


	
</div>