<!-- add knowledge info -->
<?php
	require_once('templates/knowledge_base.php');
?>

<section id="tier-interpretation-section">
	<div class="container-fluid">
		<div class="row">
     <!-- When variants have comments add the notes here.-->
     		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		     <?php

		     	// Get all comments related to 
			     $comments = $db->listAll('knowledge-comments', $knowledge_base[0]['knowledge_id']);
			     
			     // add a table of comments if $comments is not empty
			     if (!empty($comments))
			     {
					require('templates/lists/knowledge_comment_list.php');
			     }

		    ?>
			</div>
		</div>
<?php
     // turn off any adding for view only permissions
     if (isset($user_permssions) &&  strpos($user_permssions, 'View_Only') === false)
     {
     	echo 'here';
?>    		
    		<?php
    			require('templates/shared_layouts/links_to_report_alert.php');
    		?>
  	
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
    			<form id="add_patient_info" class="form" method="post" class="form-horizontal">
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
						<div id="div_textarea_<?= $knowledge_base[0]['knowledge_id']; ?>" class="form-group">
							<!-- since there are multiple different textareas on this page I have opted not to add a form to have more control over the submit therefore any html checking on the textarea will not work.  I will have to check with javascript   -->
							<textarea class="form-control" name="comment" rows="13" id="textarea_<?= $knowledge_base[0]['knowledge_id']; ?>" maxlength="65535"></textarea>
							<input type="number" name="knowledge_id" value="<?= $knowledge_base[0]['knowledge_id']; ?>" class="d-none">
							<!-- add a button to submit new interpretation -->
							<button type="submit" name="submit" value="submit" class="btn btn-primary btn-primary-hover" style="margin-top:10px;">
								Submit
							</button>

						</div>
					</div>
				</form>
			</div>
		</div>
<?php
	}
?>		
	</div>
    
    


</section>