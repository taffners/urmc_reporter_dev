     

     <div id="page-bottom" class="container-fluid" > 
          <div class="row">
               <div class="text-center col-xs-12 col-sm-12 col-md-12 col-lg-12" style="color:white;"></div>
          </div>
     </div>
     </body>
     
     <footer class="fixed-bottom d-print-none">
          <div class="row">
               <div class="text-center col-xs-1 col-sm-1 col-md-1 col-lg-1" >
                    <a href="<?= $_SERVER['REQUEST_URI'];?>#page-head" style="color:white;"><i class="fas fa-arrow-up"></i></a>
               </div>
               <div class="text-center col-xs-10 col-sm-10 col-md-10 col-lg-10" >
                    <?= htmlspecialchars(SITE_TITLE); ?> <span id="site-version-num"><?= htmlspecialchars(VERSION); ?></span>
               </div>
               <div class="text-center col-xs-1 col-sm-1 col-md-1 col-lg-1" >
                    
                    <a href="<?= $_SERVER['REQUEST_URI'];?>#page-bottom" style="color:white;"><i class="fas fa-arrow-down"></i></a>
                    
               </div>
          </div>
<?php
     if (isset($page) && $page !== 'login')
     {

          // I am trying to upgrade to bootstrap 4 and newer JQuery If this is on it is for testing the new versions
          if (JS_UPGRADE_TESTING)
          {
     ?>
               <script src="js/libraries/libraries_v4.js?ver=<?= htmlspecialchars(VERSION); ?>"></script>
     <?php          
          }
          else
          {
     ?>
               <script src="js/libraries/libraries.js?ver=<?= htmlspecialchars(VERSION); ?>"></script>
     <?php
          }
     ?>


     <?php
          // include unit testing to admin page
          if ($page === 'admin')
          {
     ?>
               <script src="js/libraries/qunit.js?ver=<?= htmlspecialchars(VERSION); ?>"></script>
     <?php
          }
     ?>

          
     <?php
          $pages_using_google_charts = array('admin', 'performance_audit', 'show_edit_report', 'verify_variants', 'cummulative_month_audit', 'new_home');
          if (isset($_GET['page']) && in_array($_GET['page'], $pages_using_google_charts))
          {
     ?>          
               <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
     <?php
          }
     ?>
          <script data-main="js/main" src="js/libraries/require.js?ver=<?= htmlspecialchars(VERSION); ?>"></script>
<?php
     }
?>

     </footer>
</html>
