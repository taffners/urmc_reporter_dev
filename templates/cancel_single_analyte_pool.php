<div class="container-fluid">

	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="height:500px;">
			
			<fieldset class="alert alert-danger">
				<legend class='show'>
					Cancel Test <?= $page_title;?>
				</legend>

				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
						<h1>Warning !!! You are about to cancel following Single Analyte Pool. <br>
						<br>
						This is permanent and all tests in this pool will be changed back to pending from in progress.</h1>
					</div>
					<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
						<img src="images/warning.png">
					</div>
				</div>
				<hr class="thick-red-line">
				<?php
					require_once('templates/tables/single_analyte_pool_table.php');
					require_once('templates/shared_layouts/comments.php');
				?>

				
				<hr class="thick-red-line">
				<form class="form" method="post" class="form-horizontal">

					<div class="form-group row">
						<div class="col-xs-12 col-sm-3 col-md-3">
							<label for="problems" class="form-control-label" style="font-size:20px;">Reason for Canceling:</label>
						</div>

						<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
							<textarea class="form-control" name="comments" rows="4" maxlength="65535" style="margin-bottom: 10px;"></textarea>
						</div>
					</div>
					<div class="form-group row">
						<div class="offset-md-3 offset-lg-3 col-xs-12 col-sm-12 col-md-8 col-lg-8">
							<button type="submit" id="<?= $page;?>_submit" name="<?= $page;?>_submit" value="Submit"  class="btn-lg btn-danger btn-danger-hover">
								Press here if you are sure you would like to cancel the single analyte pool <?= $page_title;?>.  This is permanent and all tests in this pool will be changed back to pending from in progress.
							</button>
						</div>
					</div>
				</form>
			</fieldset>
		</div>
	</div>
</div>