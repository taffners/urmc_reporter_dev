<?php
if (isset($_GET['pool_chip_linker_id']) && isset($poolStepTracker) && isset($_GET['ngs_panel_id']))
{

?>
<div class="row">
	<!-- report progress bar toggles -->	
	<?php
	require_once('templates/shared_layouts/report_toggle_show_status.php');
	?>

	<!-- Progress bar -->
	<?php
	require_once('templates/shared_layouts/pool_step_nav_bar.php')
	?>
	<!-- update area -->
	<div id="toggle-report-progress-bar" class="col-xs-10 col-sm-10 col-md-10 col-lg-10">
		<form id="make_run_template_form" class="form" method="post" class="form-horizontal">
			<fieldset>
				<legend>
					<?= $page_title;?>
				</legend>
		
				<div class="row">
					<div class="alert alert-info" role="alert">
						<h1><strong>IMPORTANT!!</strong> Use a barcode scanner for the following to fields.  This will ensure no typos are introduced.</h1>

						<ol>
							<li>Fill out the two following fields using a barcode scanner.</li>
							<li>Push the Make run template button.  A sample sheet csv for the MiSeq will download.</li>
							<li><strong>Copy downloaded sample sheet csv</strong> file to the M drive SampleSheets folder. 
								<ul>
									<li>Open <strong>Windows Explorer</strong></li>
									<li>
										Click on <strong>Computer</strong> and <strong>Map Network drive</strong>.
										<br>
										<img src="images/map_network_drive.png" alt="image of how to mape a network drive in windows explorer" width="800px">
									</li>
									<li>
										<br>
										Fill in all of the information highlighted in yellow below.
										<br>
										<br><strong>\\M-M70203R\MiSeq​ Control Software</strong>
										<br>
										<img src="images/map_network_drive2.png" alt="image of how to mape a network drive in windows explorer" width="400px">
									</li>
									<li>
										<br><b>Username:</b> sbsuser 
										<br><b>Password:</b> Danryan#1
										<br><strong>Click on remember my credentials</strong>
										<br>
										<img src="images/map_network_drive3.png" alt="image of how to mape a network drive in windows explorer" width="400px">
									</li>
								</ul>
														
							<li>Once you will no longer need the Make run template function click Done.</li>
						</ol>
					</div>
				</div>
		
				<div class="form-group row">
					<div class="col-xs-12 col-sm-4 col-md-4">
						<label for="chip_flow_cell" class="form-control-label turn-off-enter-key" >Flow Cell ID #: <span class="required-field">*</span> <br>(example: C97FP)</label>
					</div>
					<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
						<input type="text" class="form-control" id="chip_flow_cell" name="chip_flow_cell" required="required" pattern=".{5}" value="<?php
						if (isset($library_pool[0]['chip_flow_cell']) && !empty($library_pool[0]['chip_flow_cell']))
						{
							echo $library_pool[0]['chip_flow_cell'];
						}
						?>"/>
					</div>
				</div>
				<div class="form-group row">
					<div class="col-xs-12 col-sm-4 col-md-4">
						<label for="reagent_cartidge" class="form-control-label" >Reagent Cartridge Lot #: <span class="required-field">*</span> <br>(example: MS7735847-600V3)</label>
					</div>
					<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
						<input type="text" class="form-control turn-off-enter-key" id="reagent_cartidge" name="reagent_cartidge" required="required" value="<?php
						if (isset($library_pool[0]['reagent_cartidge']) && !empty($library_pool[0]['reagent_cartidge']))
						{
							echo $library_pool[0]['reagent_cartidge'];
						}
						?>"/>
					</div>
				</div>
				<div class="form-group row">
					<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
						<button type="button" id="make_run_template" class="btn btn-success btn-success-hover" data-pool_chip_linker_id="<?=$_GET['pool_chip_linker_id'];?>" data-chip="A">
							Make run template
						</button>	
					</div>
					<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
						<button type="button" id="make_run_template_updated" class="btn btn-success btn-success-hover" data-pool_chip_linker_id="<?=$_GET['pool_chip_linker_id'];?>" data-chip="A">
							Make run template for updated local run manager
						</button>	
					</div>
				</div>
				
			<?php
				// add form submit, previous, and next buttons.
				require_once('templates/shared_layouts/form_pool_submit_nav_buttons.php');
			?>
			</fieldset>
		</form>
	</div>


<?php

}
?>
	

	</div>
</div>