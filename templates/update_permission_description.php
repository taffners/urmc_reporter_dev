<div class="container-fluid">
	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

			<form class="form" method="post" class="form-horizontal">
				<fieldset>
					<legend >
						<?= $page_title;?>
					</legend>			

					<div class="form-group row">
						<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
							<label for="permission" class="form-control-label">Permission: <span class="required-field">*</span></label>
						</div>
						<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
							<input type="text" class="form-control" id="permission" name="permission" maxlength="15" required="required" value="<?= $utils->GetValueForUpdateInput($permission_info, 'permission');?>"/>
						</div>
					</div>		
					<div class="form-group row">
						<div class="col-xs-12 col-sm-4 col-md-4">
							<label for="description" class="form-control-label">Description:</label>
						</div>

						<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
							<textarea class="form-control" name="description" rows="4" maxlength="65535" style="margin-bottom: 10px;"><?= $permission_info[0]['description'];?></textarea>
						</div>
					</div>	

					<div class="form-group row">
						<div class="offset-xs-4 offset-sm-4 offset-md-4 offset-lg-4 col-xs-2 col-sm-2 col-md-2 col-lg-2">
							<button type="submit" id="<?= $page;?>_submit" name="<?= $page;?>_submit" value="Submit"  class="btn btn-primary btn-primary-hover">
								Submit
							</button>
						</div>
					</div>
				</fieldset>
			</form>
		</div>
	</div>
</div>