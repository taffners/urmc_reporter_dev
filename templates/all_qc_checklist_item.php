<div class="container-fluid">
<?php
     if (isset($user_permssions) && strpos($user_permssions, 'admin') !== false)
     {                        
?>     
     <div class="row">
          <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
               <a href="?page=add_qc_checklist_item" class="btn btn-primary btn-primary-hover" role="button">Add QC Checklist Item</a>
               
          </div>
     </div>
<?php
     }
?>
     <fieldset>
         
          <legend>All QC Checklist Items</legend>
         

          <div class="row" style="overflow-x:auto;">
               <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <table class="formated_table sort_table_no_inital_sort_no_paging">
                         <thead>
                              <th>Field Type</th>
                              <th>Test Name</th>
                              
                              <th>Description</th>
                              <th>Category</th>
                              
                         <?php
                              if (isset($user_permssions) && strpos($user_permssions, 'admin') !== false)
                              {                        
                         ?>
                              <th>Update Checklist Item</th>
                         <?php
                              }
                         ?>

                         </thead>
                         <tbody>

                         <!-- iterate over all pending runs and add each entry as a new row in the table-->

               <?php

                    for($i=0;$i<sizeof($all_qc_checklist_items);$i++)
                    {    
               ?>
                              <tr>
                                  <td><?= $all_qc_checklist_items[$i]['field_type'];?></td>
                                  <td><?= $all_qc_checklist_items[$i]['step_name'];?></td>
                                  <td><?= $all_qc_checklist_items[$i]['description'];?></td>
                                  <td><?= $all_qc_checklist_items[$i]['category'];?></td>

                         <?php
                              if (isset($user_permssions) && strpos($user_permssions, 'admin') !== false)
                              {                        
                         ?>
                                  <td><a href="?page=add_qc_checklist_item&check_list_validation_step_id=<?= $all_qc_checklist_items[$i]['check_list_validation_step_id'];?>" class="btn btn-primary btn-primary-hover" role="button">Update Checklist item</a></td>
                                  
                         <?php
                              }
                         ?>
                                   

                              </tr>
               <?php 
                    }
               ?>
                         </tbody>
                    </table>
               </div>
          </div>
     </fieldset>
</div>
