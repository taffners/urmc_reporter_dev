
<div class="container-fluid">
	<form id="add_library_pool" class="form" method="post" class="form-horizontal">
		<fieldset>
			<legend>
				Make a Library Pool
			</legend>	

			<div id="form-control-fields">
				<div class="form-group row">
					<div class="col-xs-12 col-sm-4 col-md-4">
						<label for="ngs_panel_id" class="form-control-label">NGS Panel: <span class="required-field">*</span></label>
					</div>
					<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
						<select class="form-control" name="ngs_panel_id" id="ngs_panel_id" required="required">
							<option value="" selected="selected"></option>
						<?php

							$panel_not_found = true;
							for($i=0;$i<sizeof($ngs_panels);$i++)
		          			{	
						?>
								<option value="<?= $ngs_panels[$i]['ngs_panel_id']; ?>" 
								<?php 
									if(isset($_GET['ngs_panel_id']) && $_GET['ngs_panel_id'] == $ngs_panels[$i]['ngs_panel_id']) 
									{
										$panel_not_found = false;
										echo 'selected'; 
									}

								?>>
									<?= $ngs_panels[$i]['type']; ?>
								</option>
						<?php
							}
						?>
						</select>
					</div>
				</div>
<?php
	
	// Currently only the OFA panel (ngs_panel_id === 1) uses two chips.  Make sure
	// the user has to chips to add

	if (isset($_GET['ngs_panel_id']) && $_GET['ngs_panel_id'] === '1')
	{
?>
				<div class="form-group row">
					<div class="col-xs-12 col-sm-4 col-md-4">
						<label for="sample_type" class="form-control-label">Number of Chips in Pool: <span class="required-field">*</span></label>
					</div>
					<div class="col-xs-12 col-sm-8 col-md-8 ">
						<label class="radio-inline"><input type="radio" name="num_chips" value="1" <?php 
							if (isset($_GET['num_chips']) && $_GET['num_chips'] === '1') 
							{
								echo 'checked'; 
							}
						?>>1</label>
						<label class="radio-inline"><input type="radio" name="num_chips" value="2" <?php 
							if (isset($_GET['num_chips']) && $_GET['num_chips'] === '2') 
							{
								echo 'checked'; 
							}
							else if (!isset($_GET['num_chips']))
							{
								echo 'checked';
							}
						?>>2</label>
					</div>
				</div>
<?php
	}
	// if there are more than 1 chip make sure there are at least enough samples to fill
	// one chip
if (isset($_GET['num_chips']) && $_GET['num_chips'] > 1 && isset($current_ngs_panel[0]['max_sample_per_chip']) && intval($current_ngs_panel[0]['max_sample_per_chip']) >= sizeof($available_samples))
{
?>
				<div class="alert alert-danger" role="alert">
				<h1><strong>Each <?=$current_ngs_panel[0]['type'];?> chip can hold <?= $current_ngs_panel[0]['max_sample_per_chip'];?> samples.  There are only <?= sizeof($available_samples);?> samples available.  Therefore <?= $_GET['num_chips'];?> chips are not necessary.  Please add more samples or adjust the number chips needed.</strong></h1>
  				
				</div>
<?php
}

// if num_chips, ngs_panel_id and the arrays $available_samples and $current_ngs_panel are set the form will not be redirected again unless the user changes something.  This will reduce the number of times they need to fill in the information bellow.  That is why it does not appear until here.  
else if (isset($_GET['num_chips']) && isset($available_samples) && isset($current_ngs_panel))
{
	if (isset($_GET['ngs_panel_id']) && $_GET['ngs_panel_id'] === '1')
	{

		// find barcodes to automatically chosen based on last used barcode set.
		if (empty($last_barcode))
		{
			$chosen_i_barcode = 0;		
		}
		else
		{
		
			$chosen_i_barcode = intval($last_barcode[0]['reagent_list_id']) - 1;
		}

		for ($i = 1; $i <= $_GET['num_chips']; $i++)
		{
?>		
				<div class="form-group row">
					<div class="col-xs-12 col-sm-4 col-md-4">
						<label for="barcodes_<?= $i;?>[reagent_list_id]" class="form-control-label">Barcode Set <?= $i;?>: <span class="required-field">*</span></label>
					</div>
					<div class="col-xs-12 col-sm-8 col-md-8">
						<select class="form-control" name="barcodes_<?= $i;?>[reagent_list_id]" required="required">
						<?php
							foreach ($possible_barcodes as $key => $barcode) 
		          			{	
						?>
								<option value="<?= $barcode['reagent_list_id']; ?>" <?php
									// choose barcode based on the last chosen.  It is possible that not all barcodes were used previously so advance to last used
									if ($chosen_i_barcode === $key)
									{
										echo 'selected';
									}
								?>>
									<?= $barcode['reagent_name'];?>
								</option>
						<?php
							}
							$chosen_i_barcode++;

							// There are only 6 barcodes so restart at zero once
							// it gets greater than 5
							if ($chosen_i_barcode > 5)
							{
								$chosen_i_barcode = 0;
							}
						?>
						</select>
					</div>
				</div>
<?php
		}
?>
				<div class="form-group row">
					<div class="col-xs-12 col-sm-4 col-md-4">
						<label for="library_pool_name" class="form-control-label" >Run Plan Name: <span class="required-field">*</span></label>
					</div>
					<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
						<input type="text" class="form-control" id="library_pool_name" name="library_pool_name" required="required" maxlength="50" value="<?php 
							if (isset($library_pool_array[0]['library_pool_name'])) 
							{
								echo $library_pool_array[0]['library_pool_name']; 
							}
							else
							{
								echo OFA_WORKFLOW.'_'.date("Y_m_d");
							}
						?>" />
					</div>
				</div>
<?php
	}
?>
				<div class="form-group row">
					<div class="col-xs-12 col-sm-4 col-md-4">
						<label for="library_prep_date" class="form-control-label">Library Start Date: <span class="required-field">*</span></label></label>
					</div>
					<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
						<input type="date" class="form-control" id="library_prep_date" name="library_prep_date" value="<?= date('Y-m-d');?>" required="required"/>
					</div>
				</div>
				<div class="row alert alert-info" role="alert">
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" >
					<h1><strong>Include all samples in library pool including controls in an increasing order.</strong></h1>
		  				<ul>
		  					<li>Use Check box under</li>
		  					<li>Edit the order numbers if necessary.  The order only needs to be increasing for the samples it does not need to start at 1.</li>
<?php
	if (isset($_GET['num_chips']) && $_GET['num_chips'] > 1)
	{
?>
							<li>Make sure all chip A samples have a lower order number than chip b samples by using the chip A B radio buttons.</li>
<?php
	}
?>		  					
		  				</ul>
					</div>
				</div>
				<div class="row">
					<button type="button" id="unselect-chkbx" class="btn-primary btn-primary-hover">Unselect all</button>
				</div>
				<table class="formated_table dataTable table-striped">
					<thead>

						<th>Include</th>
						<th>Reporter #</th>
						<th class="d-none">Run_id</th>
						<th>Mol#</th>
						<th>Sample Name</th>
						<th>Patient Name</th>
						<th>Order number</th>
						
<?php

	if (isset($current_ngs_panel[0]['max_num_chips']) && intval($current_ngs_panel[0]['max_num_chips']) > 1 && isset($available_samples)  && !empty($available_samples))
	{
?>
						<th>Chip</th>
<?php
	}
?>					
					</thead>
					<tbody>
<?php
				foreach ($available_samples as $key => $sample)
				{	
								
?>
					<tr>
		                    <td>
	                         	<input class="chkbx" id="toggle_include_visit_<?= $sample['visit_id'];?>" type="checkbox" name="visit_<?=$key;?>[include_in_pool]" value="1" checked/>
	                    	</td>
	                    	<td><?= $sample['visit_id'];?></td>
	                    	<td class="d-none">
	                    		<input type="number" name="visit_<?=$key;?>[visit_id]" value="<?= $sample['visit_id'];?>"/>
	                    	</td>
	                    	
	                    	<td><?= $sample['order_num'];?></td>
	                    	<td>
<?php
	// Add control name which is pt.first_name,"_",vt.version
	// vt.first_name is control name and vt.version is version-use
	if (strpos($sample['sample_type'], 'Control') !== false )
	{
?>
							<?= $sample['control_name'];?>
							<input class="d-none" type="text" name="visit_<?=$key;?>[sample_name]" value="<?= $sample['control_name'];?>"/>
<?php
	}

	// Add sample name for samples which is MD#_soft_lab_#. 
	else if ($sample['sample_type'] == null || $sample['sample_type'] == '' || $sample['sample_type'] == 'Patient Sample' || $sample['sample_type'] == 'Validation' || $sample['sample_type'] == 'Sent Out')
	{		
?>
							<?= $sample['mol_num_soft_lab'];?>
							<input class="d-none" type="text" name="visit_<?=$key;?>[sample_name]" value="<?= $sample['mol_num_soft_lab'];?>"/>
<?php
	}

?>	                    		
	                    	</td>
	                    	<td><?= $sample['patient_name'];?><input class="d-none" type="text" name="visit_<?=$key;?>[patient_name]" value="<?= $sample['patient_name'];?>"/></td>
	                    	<td>
	                    		<input type="number" name="visit_<?=$key;?>[order_num]" value="<?= $key + 1;?>" style="width:45px;"/>
	                    	</td>
<?php
// For panels with more than one chip include chip for each sample.  
	if (isset($current_ngs_panel[0]['max_num_chips']) && intval($current_ngs_panel[0]['max_num_chips']) > 1 && isset($current_ngs_panel[0]['max_sample_per_chip']) && $key + 1 <= intval($current_ngs_panel[0]['max_sample_per_chip']) )
	{	
								
?>
						<td>
							<label class="radio-inline"><input type="radio" name="visit_<?=$key;?>[chip_flow_cell]" value="A" checked>A</label>

							<label class="radio-inline"><input type="radio" name="visit_<?=$key;?>[chip_flow_cell]" value="B" >B</label>
						</td>


<?php
	}
	else if (isset($current_ngs_panel[0]['max_num_chips']) && intval($current_ngs_panel[0]['max_num_chips']) > 1 && isset($current_ngs_panel[0]['max_sample_per_chip']) && $key + 1 > intval($current_ngs_panel[0]['max_sample_per_chip']) )
	{
?>
						<td>
							<label class="radio-inline"><input type="radio" name="visit_<?=$key;?>[chip_flow_cell]" value="A">A</label>

							<label class="radio-inline"><input type="radio" name="visit_<?=$key;?>[chip_flow_cell]" value="B" checked>B</label>
						</td>
<?php
	}
?>
	                    </tr>

<?php
				} // foreach
?>
<?php						
}

?>			
					</tbody>

				</table>
				<div class="row">
					<div class="col-xs-12 col-sm-4 col-md-4">

					</div>
					<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
	
						<button type="submit" id="submit-add-library-pool" name="submit-add-library-pool" value="Submit" class="btn btn-primary btn-primary-hover">
							Submit
						</button>	
					</div>
				</div>
			</div>
		</fieldset>
	</form>
</div>

