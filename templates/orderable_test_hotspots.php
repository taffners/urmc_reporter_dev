<div class="container-fluid">
	<div class="row">
		
	
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			<fieldset>
				<legend>Hotspots</legend>	

			<?php
			if (isset($all_hotspots) && !empty($all_hotspots))
			{
			?>
				<table class="formated_table sort_table" >
					<thead>
						<th>Gene</th>
						<th>Coding</th>
						<th>Amino Acid Change</th>
						<th>Exon</th>
						<th>Codon</th>
						<th>Test Genetic Call</th>
						<th>Mutation Abbrevation</th>
						<th>Reported</th>
						<th>Knowledge Base</th>
					</thead>
					<tbody>
					<?php
					foreach ($all_hotspots as $key => $variant)
					{
					?>
						<tr>
							<td><?= $variant['genes'];?></td>
							<td><?= $variant['coding'];?></td>
							<td><?= $variant['amino_acid_change'];?></td>
							<td><?= $variant['exon'];?></td>
							<td><?= $variant['codon'];?></td>
							<td><?= $variant['ordered_test_genetic_call'];?></td>
							<td><?= $variant['mutation_abbr'];?></td>
							<td><?= $variant['reported'];?></td>
							<td>
								<?php 
									if (!empty($variant['knowledge_id']))
									{
								?>
										<a href="?page=add_interpretation&knowledge_id=<?= $variant['knowledge_id'];?>" class="btn btn-primary btn-primary-hover">Knowledge Base</a>
								<?php
									}
									// some only if the test is reportable do these variants need to be in the the knowledge base
									else if ($variant['infotrack_report'] === 'yes')
									{
								?>
										<a href="?page=add_SNV_to_knowledge_base&genes=<?= $variant['genes'];?>&coding=<?= $variant['coding'];?>&amino_acid_change=<?= $variant['amino_acid_change'];?>&orderable_tests_id=<?= $_GET['orderable_tests_id'];?>&previous_page=orderable_test_hotspots" class="btn btn-danger btn-danger-hover"><span class="fas fa-plus" aria-hidden="true"> SNV</span></a>
								<?php
									}
								?>
								

							</td>


						</tr>
					<?php
					}
					?>
					</tbody>
				</table>
			<?php
			}
			else
			{
			?>
				<h1>Currently this version of Infotrack does not allow you to add hotspots.  Provide an admin (<?= $all_admins_str;?>) with all the info to add the hotspots including:  </h1><br>
				<ul>
					<li>Gene</li>
					<li>Coding</li>
					<li>Amino Acid Change</li>
					<li>Exon</li>
					<li>Codon</li>
					<li>Test Genetic Call (required)</li>
					<li>Mutation Abbreviation (required)</li>
				</ul>
			<?php
			}
			?>
			
			</fieldset>
		</div>

		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			<fieldset>
				<legend>Regulation Info</legend>	

			<?php
			if (isset($test_reporting_regulation_info) && !empty($test_reporting_regulation_info))
			{
			?>
				<table class="formated_table sort_table" >
					<thead>
						<th>Method</th>
						<th>Limitations</th>
						<th>Disclaimer</th>
						<th>Go Live Date</th>
					</thead>
					<tbody>
					<?php
					foreach ($test_reporting_regulation_info as $key => $regs)
					{
					?>
						<tr>
							<td><?= $regs['method'];?></td>
							<td><?= $regs['limitations'];?></td>
							<td><?= $regs['disclaimer'];?></td>
							<td><?= $regs['go_live_date'];?></td>
						</tr>
					<?php
					}
					?>
					</tbody>
				</table>
			<?php
			}
			else
			{
			?>
				<h1>Currently this version of Infotrack does not allow you to add or modify regulation info.  Provide an admin (<?= $all_admins_str;?>) with all the info to add the regulation info including:  </h1><br>
				<ul>
					<li>method</li>
					<li>limitations</li>
					<li>disclaimers</li>					
				</ul>
			<?php
			}
			?>
			
			</fieldset>
		</div>
	</div>
</div>