
<div class="container-fluid">

<?php
	require_once('templates/tables/historic_control_table.php');
?>

</div>
<div class="container-fluid">
	
	<h3 class="raised-card-subheader">Update Range for Variant Above</h3>
	<form id="update_control_range_form" class="form" method="post" class="form-horizontal">
		<div class="form-group row">
			<div class="col-xs-12 col-sm-4 col-md-4">
				<label for="min_accepted_frequency" class="form-control-label">Min Range: <span class="required-field">*</span></label>
			</div>
			<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
				<input type="number" class="form-control" id="min_accepted_frequency" name="min_accepted_frequency" value="<?= $summary_control_vaf[0]['min_accepted_frequency'];?>" required>
			</div>
		</div>
		<div class="form-group row">
			<div class="col-xs-12 col-sm-4 col-md-4">
				<label for="max_accepted_frequency" class="form-control-label">Max Range: <span class="required-field">*</span></label>
			</div>
			<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
				<input type="number" class="form-control" id="max_accepted_frequency" name="max_accepted_frequency" value="<?= $summary_control_vaf[0]['max_accepted_frequency'];?>" required>
			</div>
		</div>
		<div class="row d-none">
			<div class="col-xs-12 col-sm-4 col-md-4">
				<label for="expected_control_variants_id" class="form-control-label">expected_control_variants_id: <span class="required-field">*</span></label>
			</div>
			<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
				<input type="number" class="form-control" id="expected_control_variants_id" name="expected_control_variants_id" value="<?= $_GET['expected_control_variants_id'];?>" required>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12 col-sm-4 col-md-4">
			</div>
			<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">

				<button type="submit" id="submit_update_control_range" name="submit_update_control_range" value="Submit" class="btn btn-primary btn-primary-hover">
					Update Expected Variant
				</button>	
			</div>
		</div>
	</form>

</div>
