<?php
	require_once('templates/lists/run_info_list.php');
?>

<div class="container-fluid">
	<div class="row">
		<?php
			require_once('templates/shared_layouts/report_progress_bar.php');
		?>

		<!-- update area -->
		<div id="toggle-report-progress-bar" class="col-xs-10 col-sm-10 col-md-10 col-lg-10" style="height:500px;">
			<form id="add_variants_form" class="form" method="post" class="form-horizontal">
				<fieldset>

					<legend id='new_legend' class='show'>
						<?= $legend_title;?>
					</legend>
<?php
					require_once('templates/lists/control_overview.php');
?>	


<?php
	// for controls type ids greater than 0 show a btn to get a summary of historic data. 0 is a default value.
	if (isset($visitArray[0]['control_type_id']) && !empty($visitArray[0]['control_type_id']) && intval($visitArray[0]['control_type_id']) > 0)
	{
?>
					<div class="row" id="expected_links">
						
						<a href="?page=historic_controls&control_type_id=<?=$visitArray[0]['control_type_id'];?>&ngs_panel_id=<?= $visitArray[0]['ngs_panel_id'];?> role="button" class="btn btn-info" style="color:black;font-weight:bold;">Control Historic<br>Data Summary</a>
					</div>
<?php					
	}
?>

<?php
					require_once('templates/tables/expected_control_variants.php');

?>						

					<h3 class="raised-card-subheader">Variants We Do Not Track</h3>

					<div class="row" style="overflow-x:auto;">
					     <table class="formated_table sort_table">
          					<thead>
          						<th>Observed Gene</th>
          						<th>Observed Coding</th>
          						<th>Observed Protien</th>
          						<th>Observed VAF</th>
<?php          						
     						if (isset($user_permssions) && strpos($user_permssions, 'director') !== false)
                    			{
?>
								<th>Add Variant to Expected List</th>
<?php
                    			}
?>          						
          						
          					</thead>
          					<tbody>
<?php
	if (isset($unexpected_control_variants))
	{
		$unexpected_count = 0;
		foreach ($unexpected_control_variants as $key => $var)
		{
			// The $unexpected_control_variants list is sorted to have
			// expected_gene nulls on top of the list.  If a non null is found
			// it isn't necessary to continue looking at the list
			if (!empty($var['expected_gene']))
			{
				break;
			}
			
			// only include variants that were not expected.
			else if (empty($var['expected_gene']))
			{
				$unexpected_count++;
?>
								<tr>
									<td><?= $var['genes'];?></td>
									<?=$utils->toggleMoreLess($var['coding'], 'coding', $key);?>
									<?=$utils->toggleMoreLess($var['amino_acid_change'], 'amino_acid_change', $key);?>
									<td><?= round($var['frequency'],2);?></td>
<?php          						
     						if (isset($user_permssions) && strpos($user_permssions, 'director') !== false)
                    			{
?>
								<td>Go to step up -> NGS controls</td>
<?php
                    			}
?>          						          										
								</tr>
<?php			
			}
		}
	}
?>
          						
          					</tbody>
          				</table>
          			</div>


					<div class="row" id="review-control-summary">
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
<?php
							// Depending on if the sample is a positive 
							// or a negative control alert will be different
							// Positive controls look at variants falling 
							// expected variant table while Negative controls 
							// look at the unexpected variant table.
							// find kind of alert only %2 can fail

					$num_allowed_to_fail = intval($total_expected_count) * 0.02;
					$total_failed = $num_missing_variants + $total_failed_vaf;
					$total_observed = $total_expected_count - $num_missing_variants;
					if ($control_info[0]['type'] == 'positive')
					{
							if ($total_failed > $num_allowed_to_fail)
							{
								$control_status = '<h1><strong>Control did not pass.</strong></h1>';
								$alert_class = 'alert alert-danger';
							}
							else
							{
								$control_status = '<h1><strong>Control Passed!</strong></h1>';
								$alert_class = 'alert alert-success';
							}
?>
							
						</div>
					</div>
<?php
					}
					else if ($control_info[0]['type'] == 'negative')
					{
						if ($unexpected_count > 0)
						{
							$control_status = '<h1><strong>Control did not pass.</strong></h1>';
							$alert_class = 'alert alert-danger';
						}
						else
						{
							$control_status = '<h1><strong>Control Passed!</strong></h1>';
							$alert_class = 'alert alert-success';
						}
					}
?>

							<div class="<?=$alert_class;?>">
								<?=$control_status;?><br><br>
								As mentioned in the <a href="downloads/NY_NGS_guidelines.pdf"> New York State NGS guidelines for somatic variant detection (January 2018)</a>, the error rate for observed variants should be &lt;2% to pass the run which is <strong><?= $num_allowed_to_fail;?></strong> number of errors for this control.<br><br>

								<strong>Errors found</strong>
								<ul>
									<li>Variants not found: <strong><?= $num_missing_variants;?></strong></li>
									<li>Number of variants with VAF's outside of expected range: <strong><?= $total_failed_vaf;?></strong></li>
									<li>Expected number of variants <strong><?=$total_expected_count;?></strong></li>
									<li>Observed number of variants <strong><?=$total_observed;?></strong></li>
									<li>Total number of Unexpected Variants: <strong><?= $unexpected_count;?></strong></li>
								</ul>
							</div>




					<div class="row">
						<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
		
							<button type="submit" id="pass_control_submit" name="pass_control_submit" value="Submit" class="btn btn-success btn-success-hover">
								Pass Control
							</button>	
						</div>
						<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
							<button type="submit" id="fail_control_submit" name="fail_control_submit" value="Submit" class="btn btn-danger btn-danger-hover">
								Fail Control
							</button>		
						</div>

					</div>
			



					<!-- add the common hidden fields for every form -->
					<?php
						require_once('templates/shared_layouts/form_hidden_fields.php');
					?>
      				
				</fieldset>
			</form>
			
		</div>
	</div>
</div>
