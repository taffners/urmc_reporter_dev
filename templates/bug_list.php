<div class="container-fluid">
     <fieldset>
         
          <legend><?= $page_title;?></legend>
         

          <div class="row" style="overflow-x:auto;">
               <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <table class="formated_table sort_table_no_inital_sort_no_paging">
                         <thead>
                              <th>Description</th>
                              <th>Type</th>
                              <th>Maintenance Window</th>
                              <th>Reporter</th>
                              
                              <th>Status</th>
                              <th>Report Time</th>
                    <?php
                         if (isset($user_permssions) && strpos($user_permssions, 'admin') !== false)
                         {                        
                    ?>                              
                              <th>Report Fix</th>
                    <?php
                         }

                         if (!isset($_GET['status']))
                         {
                    ?>
                              <th>Fix Git Commit ID</th>
                              <th>Fix User Name</th>
                              <th>Final Summary</th>
                              <th>Resulted in Downtime</th>
                              <th>Downtime Type</th>
                              <th># hours down</th>
                    <?php
                         }
                    ?>
                         </thead>
                         <tbody>

                         

               <?php

                    for($i=0;$i<sizeof($all_bugs);$i++)
                    {    
               ?>
                              <tr>
                                   <td><?= $all_bugs[$i]['bug_description'];?></td>
                                   <td>
                         <?php
                         if ($all_bugs[$i]['report_type'] === 'bug')
                         {
                         ?>
                                        <img src="images/bug2.png" style="margin-left:5px;height:45px;width:45px;" meta="an image of a bug" alt="an image of a bug" title="Type is Bug">
                                   
                         <?php
                         }
                         else
                         {
                         ?>
                                        <img src="images/Admin.png" style="margin-left:5px;height:45px;width:45px;" meta="maintenance icon" alt="maintenance icon" type="maintenance">
                         <?php
                         }
                         ?>                                        
                                   </td>
                                   <td><?= $all_bugs[$i]['maintenance_start'];?> - <?= $all_bugs[$i]['maintenance_end'];?></td>
                                   <td><?= $all_bugs[$i]['user_name'];?></td>
                                   <td <?= $all_bugs[$i]['status'] === 'pending' ? 'class="red-background"': 'class="green-background"';?>>
                                        <?= $all_bugs[$i]['status'];?>
                                             
                                   </td>
                                   <td><?= $all_bugs[$i]['time_stamp'];?></td>
                         <?php
                         if (isset($user_permssions) && strpos($user_permssions, 'admin') !== false)
                         {                        
                         ?>  
                                   <td>
                                   <?php
                                   if ($all_bugs[$i]['status'] === 'pending')
                                   {
                                   ?>
                                        <a href="?page=report_bug_fix&bugs_reported_id=<?= $all_bugs[$i]['bugs_reported_id'];?>" class="btn btn-primary btn-primary-hover" role="button">Report Bug Fix</a>
                                   <?php
                                   }
                                   ?>
                                   </td>
                         <?php
                         }
                         ?>

                         <?php
                         
                              if (!isset($_GET['status']))
                              {
                         ?>
                                   <td><?= $all_bugs[$i]['commit_id'];?></td>
                                   <td><?= $all_bugs[$i]['fix_user_name'];?></td>
                                   <td><?= $all_bugs[$i]['summary_of_bug'];?></td>
                                   <td><?= $all_bugs[$i]['resulted_in_downtime'];?></td>
                                   <td><?= $all_bugs[$i]['downtime_type'];?></td>
                                   <td><?= $all_bugs[$i]['downtime_in_hour'];?></td>
                         <?php
                              }
                         ?>
                              </tr>
               <?php 
                    }
               ?>
                         </tbody>
                    </table>
               </div>
          </div>
     </fieldset>
</div>
