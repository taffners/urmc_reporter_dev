<div class="container-fluid">

	<div class="row">
		
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			<form class="form" method="post" class="form-horizontal">
				<fieldset>
				
					

				<?php
				if (isset($all_reporting_flow_steps) && !empty($all_reporting_flow_steps) && isset($orderableTestArray[0]['test_name']))
				{
				?>	
						<legend>
							Select which steps you wold like to add to report building for <?= $orderableTestArray[0]['test_name'];?>
						</legend>

						<ul id="sortable-order-samples" class="connectedSortable sortable-list">
					
								
					<?php
					foreach ($all_reporting_flow_steps as $key => $step)
					{
						if ($step['flow_type'] === 'reporting' && $step['step_status'] === 'active')
						{
					?>
							<li class="ui-state-default ui-bkgrd-color-grey">
								<span class="ui-icon ui-icon-arrowthick-2-n-s" style="margin-top:5px;"></span>
								<div class="form-group row">
										
										<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
											<label for="test_result" class="form-control-label"><?= $step['step_name'];?>? <span class="required-field">*</span></label>
										</div>
										<div class="col-xs-12 col-sm-3 col-md-3 col-lg-2">
											<div class="radio">
												<label class="radio-inline"><input type="radio" name="reporting_steps_by_test[<?= $step['reporting_flow_steps_id'];?>]" value="yes" required="required">Yes</label>
											
												<label class="radio-inline"><input type="radio" name="reporting_steps_by_test[<?= $step['reporting_flow_steps_id'];?>]" value="no">No</label>

											</div>
										</div>
										<div class="col-xs-12 col-sm-5 col-md-5 col-lg-6 alert alert-info">
											<?= $step['description'];?>
										</div>
									</div>
								</li>
					<?php
						}
					}
					?>	
						</ul>
						<div class="form-group row">
							<div class="offset-xs-4 offset-sm-4 offset-md-4 offset-lg-4 col-xs-2 col-sm-2 col-md-2 col-lg-2">
								<button type="submit" id="<?= $page;?>_submit" name="<?= $page;?>_submit" value="Submit"  class="btn btn-primary btn-primary-hover">
									Submit
								</button>
							</div>
						</div>

				<?php
				}
				else
				{
					?>
						<legend>
							Something is wrong
						</legend>
				<?php
				}
				?>

				</fieldset>
			</form>
		</div>
	</div>
<?php
if (isset($curr_test_reporting_flow_steps) && !empty($curr_test_reporting_flow_steps))
{
?>
	<div class="row">
		
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			
			<fieldset>

				<legend>Current reporting flow steps</legend>

				<table class="formated_table sort_table_no_inital_sort_no_paging">
					<thead>
						<th>ID</th>
						<th>Step Name</th>
						<th>Description</th>
						<th>no_variant_status</th>
						<th>flow_type</th>
						<th>step_status</th>
						
					</thead>
					<tbody>
					<?php
					foreach($curr_test_reporting_flow_steps as $key => $step)
					{
					?>
						<tr>
							<td><?= $step['reporting_flow_steps_id'];?></td>
							<td><?= $step['step_name'];?></td>
							<td><?= $step['description'];?></td>
							<td><?= $step['no_variant_status'];?></td>
							<td><?= $step['flow_type'];?></td>
							<td><?= $step['step_status'];?></td>
							
						</tr>
					<?php
					}
					?>

					</tbody>
                </table>
			</fieldset>
		</div>
	</div>
<?php
}
?>
</div>