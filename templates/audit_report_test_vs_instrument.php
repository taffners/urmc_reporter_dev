<div class="container-fluid">
     <fieldset>
         
          <legend>All Instrument Use Reports</legend>
         

          <div class="row" style="overflow-x:auto;">
               <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <table class="formated_table sort_table_no_inital_sort_no_paging">
                         <thead>
                              <th>Yellow Tag #</th>
                              <th>Task</th>
                              <th>Manufacturer</th>
                              <th>Serial #</th>
                              <th>Clinical Engineering #</th>
                              <th>Model</th>
                              <th>Description</th>
                              <th># Tests Performed</th>                         
                              <th class="d-print-none">Get Report</th>
                         </thead>
                         <tbody>
               <?php

                    for($i=0;$i<sizeof($all_instruments_used);$i++)
                    {    
               ?>
                              <tr>
                                  <td><?= $all_instruments_used[$i]['yellow_tag_num'];?></td>
                                  <td><?= $all_instruments_used[$i]['task'];?></td>
                                  <td><?= $all_instruments_used[$i]['manufacturer'];?></td>
                                  <td><?= $all_instruments_used[$i]['serial_num'];?></td>
                                  <td><?= $all_instruments_used[$i]['clinical_engineering_num'];?></td>
                                  <td><?= $all_instruments_used[$i]['model'];?></td>
                                  <td><?= $all_instruments_used[$i]['description'];?></td>
                                  <td><?= $all_instruments_used[$i]['num_tests'];?></td>
                                  

                        
                                  <td class="d-print-none"><a href="?page=instrument_use_report&instrument_id=<?= $all_instruments_used[$i]['instrument_id'];?>" class="btn btn-primary btn-primary-hover" role="button">Instrument Use Report</a></td>
                                  
                                        
                                   </td>

                              </tr>
               <?php 
                    }
               ?>
                         </tbody>
                    </table>
               </div>
          </div>
     </fieldset>
</div>
