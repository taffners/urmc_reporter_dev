<?php
if (isset($_GET['pool_chip_linker_id']) && isset($poolStepTracker) && isset($_GET['ngs_panel_id']))
{

?>
<div class="container-fluid">
	<div class="row">
		<!-- report progress bar toggles -->	
		<?php
		require_once('templates/shared_layouts/report_toggle_show_status.php');
		?>

		<!-- Progress bar -->
		<?php
		require_once('templates/shared_layouts/pool_step_nav_bar.php')
		?>
		<div id="toggle-report-progress-bar" class="col-xs-10 col-sm-10 col-md-10 col-lg-10">
			<form id="make_run_template_form" class="form" method="post" class="form-horizontal">
				<fieldset>
					<legend>
						Select the results associated with this pool
					</legend>
				

		<?php
		// notify user if $most_recent_runs is not showing
		if (!$most_recent_runs)
		{
		?>
					<div class="row">
						<div class="alert alert-info" role="alert">
							<strong>There's a problem accessing the torrent server.</strong>
						</div>
					</div>
		<?php
		}
		?>
		<!-- Add a drop down menu to find data -->
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 d-print-none">
				<div class="form-group row d-none">
					<div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
						<label for="month" class="form-control-label">
							Month: <span class="required-field">*</span>
						</label>
					</div>
					<div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
						<select class="form-control" name="month" id="month" required="required">					
		<?php
			$months = array(1 => 'Jan', 2 => 'Feb', 3 => 'Mar', 4 => 'Apr', 5 => 'May', 6 => 'Jun', 7 => 'Jul', 8 => 'Aug', 9 => 'Sep', 10 => 'Oct', 11 => 'Nov', 12 => 'Dec');	

			// Add all months selecting current month
			foreach ($months as $month_num => $month_name)
			{
		?>
							<option value="<?= $month_num;?>" <?php
								// select the month which was selected on reload
								if (isset($_GET['month']) && $_GET['month'] == $month_num)
								{
									echo 'selected';
								}

								// default selection to current month.  Useful for selecting all.
								else if (!isset($_GET['month']) && CURR_MONTH === $month_num)
								{
									echo 'selected';
								}
							?>>
								<?= $month_name;?>
							</option>
		<?php
			}
		?>
						</select>			
					</div>
					<div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
						<label for="year" class="form-control-label">
							Year: <span class="required-field">*</span>
						</label>
					</div>
					<div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
						<select class="form-control" name="year" id="year" required="required">					
		<?php
			// Add possible years selecting current year
			for ($y=2019; $y <= CURR_YEAR; $y++)
			{
		?>
							<option value="<?= $y;?>" <?php
								// select the month which was selected on reload
								if (isset($_GET['year']) && $_GET['year'] == $y)
								{
									echo 'selected';
								}

								// default selection to current month.  Useful for selecting all.
								else if (!isset($_GET['year']) && CURR_YEAR === $y)
								{
									echo 'selected';
								}
							?>>
								<?= $y;?>
							</option>
		<?php
			}
		?>
						</select>					
					</div>
				</div>
		

		<?php
		// only display if chips a being is being used
		if 	(
					!empty($most_recent_runs) &&
					isset($pending_pools[0]['count_chip_a']) &&
					is_numeric($pending_pools[0]['count_chip_a']) &&
					intval($pending_pools[0]['count_chip_a']) > 0
				)
		{
			// check to see if there's a chip b so info message can reflect having to chips.
			$chip_b_present = False;
			if 	(
					!empty($most_recent_runs) &&
					isset($pending_pools[0]['count_chip_b']) &&
					is_numeric($pending_pools[0]['count_chip_b']) &&
					intval($pending_pools[0]['count_chip_b']) > 0
				)
			{
				$chip_b_present = True;
			}
		?>	
					<div class="form-group row">
						<div class="alert alert-info">
							<h1>Before proceeding confirm Ion Reporter analysis is complete for <?= $chip_b_present ? '<b><u>BOTH A and B chips</b></u>':'chip A.';?>.</h1>
							<h3>Ion Reporter link: <u><?= $API_Utils->getServerLink('Ion_Reporter');?></u></h3>
						</div>
					</div>				
					<div class="form-group row">
						<div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
							<label for="chipA_results_name" class="form-control-label">Chip A: <span class="required-field">*</span></label>
						</div>
						<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
							<select class="form-control" name="chipA_results_name" id="chipA_results_name" required="required">
								<option value="" selected="selected"></option>
		<?php
			// Add all of the most recent runs but select the one with a timestamp closest to the run date
			foreach ($most_recent_runs as $key => $run)
			{
				
				if (isset($run['chefSamplePos']) && $run['chefSamplePos'] == 1)
				{
		?>
								<option value="<?= $run['results_name'];?>" <?= $utils->GetValueForUpdateSelect($torrent_results_array, 'chipA_results_name', $run['results_name'])?>>
									<?= $run['results_name'];?>
								</option>
		<?php
				}
			}
		?>

		
							</select>
						</div>
					</div>
					
		<?php
		}
		
		// only display if chips a being is being used
		if 	(isset($chip_b_present) && $chip_b_present)
		{
		?>							
					<div class="form-group row">
						<div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
							<label for="chipB_results_name" class="form-control-label">Chip B: <span class="required-field">*</span></label>
						</div>
						<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
							<select class="form-control" name="chipB_results_name" id="chipB_results_name" required="required">
								<option value="" selected="selected"></option>
		<?php
			// Add all of the most recent runs but select the one with a timestamp closest to the run date
			foreach ($most_recent_runs as $key => $run)
			{
				
				if (isset($run['chefSamplePos']) && $run['chefSamplePos'] == 2)
				{
		?>
								<option value="<?= $run['results_name'];?>" <?= $utils->GetValueForUpdateSelect($torrent_results_array, 'chipB_results_name', $run['results_name'])?>>
									<?= $run['results_name'];?>
								</option>
		<?php
				}
			}
		?>
							</select>
						</div>
					</div>
					
		<?php
		}
		?>
					<div class="form-group row d-none">
						<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
							<label for="chip_count" class="form-control-label">Chip Count</label>
						</div>
						<div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">
							<input id="chip_count" type="number" class="form-control turn-off-enter-key" name="chip_count" value="<?= $chip_count;?>"/>
						</div>
					</div>
			<?php
				// add form submit, previous, and next buttons.
				require_once('templates/shared_layouts/form_pool_submit_nav_buttons.php');
			?>				
				</fieldset>
			</form>
		</div>
	</div>
</div>
<?php
}
?>