<?php
	if (isset($_GET) && isset($_GET['run_id']))
	{
		require_once('templates/lists/run_info_list.php');
	}
	
	if (isset($visitArray) && !empty($visitArray) && $visitArray[0]['sample_type'] !== 'Positive Control' && $visitArray[0]['sample_type'] !== 'Negative Control')
	{
		$form_status = 'updating';
	}
	else
	{
		$form_status = 'new';
	}
	
?>

<div class="container-fluid">

	<div class="row">
		<?php
			if (isset($_GET) && isset($_GET['run_id']))
			{
				require_once('templates/shared_layouts/report_progress_bar.php');
		?>
		<div id="toggle-report-progress-bar" class="col-xs-12 col-sm-10 col-md-10 col-lg-10" style="height:500px;">
		<?php
			}
			else
			{
		?>
		<div id="toggle-report-progress-bar" class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="height:500px;">
		<?php
			}
		?>
			<form id="add_visit_info" class="form" method="post" class="form-horizontal">
				<fieldset>
				<?php 
					if ($form_status === 'new')
					{
				?>
					<legend id="new_legend">
						Add Control
					</legend>
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 alert alert-info">
						Add Patient samples, CAP samples, and Send out samples via the sample log book.
					</div>
				<?php 
					}
					else if ($form_status === 'updating')
					{
				?>
					<legend id="update_legend">
						Update Visit Info
					</legend>
				<?php
					}
				?>					
					<div id="form-control-fields">
						<div class="form-group row">
							<div class="col-xs-12 col-sm-4 col-md-4">
								<label for="ngs_panel_id" class="form-control-label">NGS Panel: <span class="required-field">*</span></label>
							</div>
							<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
								<select class="form-control" name="ngs_panel_id" id="ngs_panel_id" required="required">
									<option value="" selected="selected"></option>
								<?php

									$panel_not_found = true;
									for($i=0;$i<sizeof($ngs_panels);$i++)
			               			{	
								?>
										<option value="<?= $ngs_panels[$i]['ngs_panel_id']; ?>" 
										<?php 
											if(isset($visitArray[0]['ngs_panel_id']) && $visitArray[0]['ngs_panel_id'] == $ngs_panels[$i]['ngs_panel_id']) 
											{
												$panel_not_found = false;
												echo 'selected'; 
											}

										?>>
											<?= $ngs_panels[$i]['type']; ?>
										</option>
								<?php
									}
								?>
								</select>
							</div>
						</div>						
						<div class="form-group row">
							<div class="col-xs-12 col-sm-4 col-md-4">
								<label for="sample_type" class="form-control-label">Sample Type: <span class="required-field">*</span></label>
							</div>

							<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
					<?php

						// Only add option if this form is for updating 
						if ($form_status === 'updating')
						{
					?>
								<label class="radio-inline"><input type="radio" name="sample_type" value="Patient Sample" <?php 
									if (isset($visitArray[0]['sample_type']) && $visitArray[0]['sample_type'] === 'Patient Sample') 
									{
										echo 'checked'; 
									}
									else if (!isset($visitArray[0]['sample_type']))
									{
										echo 'checked';
									}
									else if (empty($visitArray[0]['sample_type']))
									{
										echo 'checked';
									}
								?>>Patient Sample or CAP Sample</label>
					<?php
						}
					?>								
								<label class="radio-inline"><input type="radio" name="sample_type" value="Positive Control" <?php 
									if (isset($visitArray[0]['sample_type']) && $visitArray[0]['sample_type'] === 'Positive Control') 
									{
										echo 'checked'; 
									}

								?>>Positive Control</label>
								<label class="radio-inline"><input type="radio" name="sample_type" value="Negative Control" <?php 
									if (isset($visitArray[0]['sample_type']) && $visitArray[0]['sample_type'] === 'Negative Control') 
									{
										echo 'checked'; 
									}
								?>>Negative Control</label>
					<?php
						// Only add option if this form is for updating 
						if ($form_status === 'updating')
						{
					?>
								<label class="radio-inline"><input type="radio" name="sample_type" value="Validation" <?php 
									if (isset($visitArray[0]['sample_type']) && $visitArray[0]['sample_type'] === 'Validation') 
									{
										echo 'checked'; 
									}
								?>>Validation</label>
								<label class="radio-inline"><input type="radio" name="sample_type" value="Sent Out" <?php 
									if (isset($visitArray[0]['sample_type']) && $visitArray[0]['sample_type'] === 'Sent Out') 
									{
										echo 'checked'; 
									}
								?>>Sent Out</label>
					<?php
						}
					?>

							</div>
						</div>
					</div>

					<div id="all-fields-control" <?php 
						// Add class of hide or show for visits updating.
						if 	(
								!empty($visitArray[0]['sample_type']) && 
								(
									isset($visitArray[0]['sample_type']) && 
									(
										$visitArray[0]['sample_type'] !=='Patient Sample' &&
										$visitArray[0]['sample_type'] !== 'Sent Out' &&
										$visitArray[0]['sample_type'] !== 'Validation'
									)
								) 
							)
						{
							echo 'class="show"'; 
						}
						else
						{
							echo 'class="d-none"';
						}
						?> >
						<div id="positive-control-drop-down" class="form-group row  <?php 
									if (isset($visitArray[0]['sample_type']) && $visitArray[0]['sample_type'] !== 'Positive Control') 
									{
										echo 'd-none'; 
									}
								?>">
							<div class="col-xs-12 col-sm-4 col-md-4">
								<label for="control_selection" class="form-control-label">Choose positive control: <span class="required-field">*</span></label>
							</div>
							<div class="col-xs-12 col-sm-8 col-md-8 pull-left">

								<select class="form-control" name="control_type_id[positive]" id="control-type-id-positive">
								<?php

									
									for($i=0;$i<sizeof($positive_controls);$i++)
			               			{
								?>
										<option value="<?= $positive_controls[$i]['control_type_id']; ?>" <?php
										if (isset($visitArray[0]['control_type_id']) && $visitArray[0]['control_type_id'] === $positive_controls[$i]['control_type_id'])
										{
											echo 'selected';
										}
										?>>
											<?= $positive_controls[$i]['control_name']; ?>
										</option>
								<?php
									}
								?>
								</select>

							</div>
						</div>
						<div id="negative-control-drop-down" class="form-group row <?php 
									if (isset($visitArray[0]['sample_type']) && $visitArray[0]['sample_type'] !== 'Negative Control') 
									{
										echo 'd-none'; 
									}
								?>">
							<div class="col-xs-12 col-sm-4 col-md-4">
								<label for="control_selection" class="form-control-label">Choose negative control: <span class="required-field">*</span></label>
							</div>
							<div class="col-xs-12 col-sm-8 col-md-8 pull-left">

								<select class="form-control" name="control_type_id[negative]" id="control-type-id-negative">
								<?php

									
									for($i=0;$i<sizeof($negative_controls);$i++)
			               			{
								?>
										<option value="<?= $negative_controls[$i]['control_type_id']; ?>" <?php
										if (isset($visitArray[0]['control_type_id']) && $visitArray[0]['control_type_id'] === $positive_controls[$i]['control_type_id'])
										{
											echo 'selected';
										}
										?>>
											<?= $negative_controls[$i]['control_name']; ?>
										</option>
								<?php
									}
								?>
								</select>

							</div>
						</div>
						<div class="form-group row">
							<div id="last-version-used" class="col-xs-12 col-sm-12 col-md-12">
								
							</div>
						</div>				

						<div class="form-group row">

							<div class="col-xs-12 col-sm-4 col-md-4">
						<?php
							foreach ($control_version_fields as $key => $label)
							{
						?>
								<label for="version" class="form-control-label version-lot-label" id="label_<?=$label['control_type_id'];?>" style="display:none;"><?=$label['field_name'];?>: <span class="required-field">*</span></label>		
						<?php		
							}
						?>								
							</div>
							<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
								<input type="text" class="form-control" id="version" name="version" maxlength="40" value="<?php 
									if (isset($visitArray[0]['version'])) 
									{
										echo $visitArray[0]['version']; 
									}
								?>" />
							</div>
						</div>
						<!-- 
							Doesn't work

							pattern="*-[0-9]" => lets eee submit
							pattern="[..]*-[0-9]" => can't submit 5h#-5

							might work
							pattern="[.]*-[0-9]*" => can't submit 5h#-5

							\b[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}\b
						-->
					</div>
					<div id="all-fields-sample" <?php 
						// Add class of hide or show for visits updating.
						if 	(
								(
									isset($visitArray[0]['sample_type']) &&
									$visitArray[0]['sample_type'] === 'Patient Sample'
								) 
								|| 
								(
									isset($visitArray[0]['sample_type']) &&
									$visitArray[0]['sample_type'] === 'Validation'
								) 
								||
								(
									isset($visitArray[0]['sample_type']) &&
									$visitArray[0]['sample_type'] === 'Sent Out'
								) 
								||
								(
									isset($visitArray[0]['sample_type']) &&
									empty($visitArray[0]['sample_type']) 
								)
							) 
						{
							echo 'class="show d-none"'; 
						}
						else if (!isset($visitArray[0]['sample_type']))
						{
							echo 'class="d-none"';
						}
						else
						{
							echo 'class="d-none"';
						}
						?>
						>
						<div class="form-group row">
							<div class="col-xs-12 col-sm-4 col-md-4">
								<label for="soft_lab_num" class="form-control-label">Soft Lab #: </label> <span class="required-field">*</span></label>
							</div>
							<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
								<input type="text" class="form-control no-spaces" id="soft_lab_num" name="soft_lab_num" maxlength="50" value="<?php 
									if (isset($visitArray[0]['soft_lab_num'])) 
									{
										echo $visitArray[0]['soft_lab_num']; 
									}
								?>" <?php 
									if (isset($visitArray[0]['sample_type']) && $visitArray[0]['sample_type'] !== 'Negative Control' && $visitArray[0]['sample_type'] !== 'Positive Control') 
									{
										echo 'required'; 
									}
									else if (!isset($visitArray[0]['sample_type']))
									{
										echo 'required';
									}
								?>
								/>
							</div>
						</div>
						<div class="form-group row">
							<div class="col-xs-12 col-sm-4 col-md-4">
								<label for="soft_path_num" class="form-control-label" >Soft Path #:</label>
							</div>
							<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
								<input type="text" class="form-control no-spaces" id="soft_path_num" name="soft_path_num" maxlength="50" value="<?php 
									if (isset($visitArray[0]['soft_path_num'])) 
									{
										echo $visitArray[0]['soft_path_num']; 
									}
								?>" />
							</div>
						</div>

						<div class="form-group row">
							<div class="col-xs-12 col-sm-4 col-md-4">
								<label for="mol_num" class="form-control-label">MD#: <?php 
									if (isset($visitArray[0]['sample_type']) && $visitArray[0]['sample_type'] !== 'Negative Control' && $visitArray[0]['sample_type'] !== 'Positive Control') 
									{
										echo '<span class="required-field">*</span>'; 
									}
									else if (!isset($visitArray[0]['sample_type']))
									{
										echo '<span class="required-field">*</span>';
									}
								?></label>
							</div>
							<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
								<input type="text" class="form-control no-spaces no-period" id="mol_num" name="mol_num" maxlength="50" value="<?php 
									if (isset($visitArray[0]['mol_num'])) 
									{
										echo $visitArray[0]['mol_num']; 
									}
								?>" <?php 
									if (isset($visitArray[0]['sample_type']) && $visitArray[0]['sample_type'] !== 'Negative Control' && $visitArray[0]['sample_type'] !== 'Positive Control') 
									{
										echo 'required'; 
									}
									else if (!isset($visitArray[0]['sample_type']))
									{
										echo 'required';
									}
								?>/>
							</div>
						</div>
						<div class="form-group row">
							<div class="col-xs-12 col-sm-4 col-md-4">
								<label for="account_num" class="form-control-label">Account #: </label>
							</div>
							<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
								<input type="text" class="form-control" id="account_num" name="account_num" maxlength="50" value="<?php 
								
									if (isset($visitArray[0]['account_num'])) 
									{
										echo $visitArray[0]['account_num']; 
									}
								?>" />
							</div>
						</div>
						<div class="form-group row">
							<?php
								// Find year for start MOL num
								// allow some leeway for the year for January

								$y = date('y');
								if (date('m') === '01' || date('m') === '02')
								{
									$pattern_y = strval($y).'-MOL\d+|'.strval($y-1).'-MOL\d+|00-MOL\d+';
								}
								else
								{
									$pattern_y = $y.'-MOL\d+|00-MOL\d+';

								}
								
							?>
<?php
	// NOTE !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
     // mol_num and order_num was mixed up because I didn't realize there 
     // was a md# and mol_num and mol_num already was being used for md#
?>							
							<div class="col-xs-12 col-sm-4 col-md-4">
								<label for="order_num" class="form-control-label">mol#: </label> <span class="required-field">*</span> 
							</div>
							
							<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
								<input type="text" class="form-control" id="order_num" name="order_num" readonly maxlength="50" value="<?php 
								
									if (isset($visitArray[0]['order_num']) && !empty($visitArray[0]['order_num']) && isset($last_mol_num[0]['last_mol_num'])) 
									{
										echo $visitArray[0]['order_num']; 
									}

									// start at 1 for the first sample of the year
									else if (!isset($last_mol_num[0]['last_mol_num']) || empty($last_mol_num[0]['last_mol_num']))
									{
										echo $y.'-MOL1';
									}

									// get next number mol num of the year
									else
									{
										$next_mol_num = intval($last_mol_num[0]['last_mol_num']) + 1;

										echo $y.'-MOL'.$next_mol_num;
									}
								?>" <?php 

									if (isset($visitArray[0]['sample_type']) && $visitArray[0]['sample_type'] !== 'Negative Control' && $visitArray[0]['sample_type'] !== 'Positive Control') 
									{
										echo 'required="required"'.'pattern="'.$pattern_y.'"'; 
									}
									else if (!isset($visitArray[0]['sample_type']))
									{
										echo 'required="required"'.'pattern="'.$pattern_y.'"';
									}?>/>
							</div>
						</div>	
						<div class="form-group row">
							<div class="col-xs-12 col-sm-4 col-md-4">
								<label for="chart_num" class="form-control-label">Chart #: </label>
							</div>
							<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
								<input type="text" class="form-control" id="chart_num" name="chart_num" maxlength="50" value="<?php 
								
									if (isset($visitArray[0]['chart_num'])) 
									{
										echo $visitArray[0]['chart_num']; 
									}
								?>" />
							</div>
						</div>	
				
						<div class="form-group row">
							<div class="col-xs-12 col-sm-4 col-md-4">
								<label for="collected" class="form-control-label">Collected: </label>
							</div>
							<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
								<input class="form-control" type="datetime-local" id="collected" name="collected" 
								<?php
								if(isset($visitArray[0]['collected']) && $visitArray[0]['collected'] !== DEFAULT_DATE_TIME)
								{
									echo 'value="'.$dfs->convertToUTC($visitArray[0]['collected']).'"'; 
								}
								?>/>
							</div>
						</div>
						<div class="form-group row">
							<div class="col-xs-12 col-sm-4 col-md-4">
								<label for="received" class="form-control-label">Received: </label>
							</div>
							<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
								<input class="form-control" type="datetime-local" id="received" name="received" <?php
								if(isset($visitArray[0]['received']) && $visitArray[0]['received'] !== DEFAULT_DATE_TIME)
								{
									echo 'value="'.$dfs->convertToUTC($visitArray[0]['received']).'"'; 
								}
								?>/>
							</div>
						</div>
			
						<div class="form-group row">
							<div class="col-xs-12 col-sm-4 col-md-4">
								<label for="primary_tumor_site" class="form-control-label">Primary Site: </label>
							</div>
							<div class="col-xs-12 col-sm-8 col-md-8 pull-left">

								<select class="form-control" name="primary_tumor_site" id="primary_tumor_site">
								<?php

									$panel_not_found = true;
									for($i=0;$i<sizeof($all_tissues);$i++)
			               			{
								?>
										<option value="<?= $all_tissues[$i]['tissue']; ?>" 
										<?php 
											if(isset($visitArray[0]['primary_tumor_site']) && $visitArray[0]['primary_tumor_site'] == $all_tissues[$i]['tissue']) 
											{
												$panel_not_found = false;
												echo 'selected'; 
												
											}

										?>>
											<?= $all_tissues[$i]['tissue']; ?>
										</option>
								<?php
									}

									if ($panel_not_found)
									{
										echo '<option value="" selected="selected">Unknown</option>';
									}
								?>
								</select>

							</div>
						</div>
						<div class="form-group row">
							<div class="col-xs-12 col-sm-4 col-md-4">
								<label for="diagnosis_id" class="form-control-label">Tumor Type: </label>
							</div>
							<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
								<select class="form-control" name="diagnosis_id" id="diagnosis_id">

								<?php
									$diagnosis_not_found = true;
									for($i=0;$i<sizeof($possible_diagnosis);$i++)
			               			{	
								?>
										<option value="<?= $possible_diagnosis[$i]['tumor_id']; ?>" 
										<?php 
											if(isset($visitArray[0]['tumor_id']) && $visitArray[0]['tumor_id'] == $possible_diagnosis[$i]['tumor_id']) 
											{
												echo 'selected'; 
												$diagnosis_not_found = False;
											}

										?>>
											<?= $possible_diagnosis[$i]['tumor']; ?>
										</option>

								<?php
									}
									if ($diagnosis_not_found)
									{								
								?>	
										<option value="6" selected="selected">Unknown</option>
								<?php
										
									}
								?>
								</select>
							</div>
						</div>	
										
						<div class="form-group row">
							<div class="col-xs-12 col-sm-4 col-md-4">
								<label for="test_tissue" class="form-control-label">Tissue Tested:</label>
							</div>
							<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
								<input type="text" class="form-control" name="test_tissue" maxlength="30" list="test_tissue"
								<?php 
									if(isset($visitArray[0]['test_tissue']) && $visitArray[0]['test_tissue'] !== '') 
									{									
										echo 'value="'.$visitArray[0]['test_tissue'].'"'; 
									}
								?>/>
								<datalist name="test_tissue" id="test_tissue">
								<?php


									for($i=0;$i<sizeof($all_tissues);$i++)
			               			{
								?>
										<option value="<?= $all_tissues[$i]['tissue']; ?>">
											<?= $all_tissues[$i]['tissue']; ?>
										</option>
								<?php
									}
								?>
								</datalist>
							</div>
						</div>
						
						<div class="form-group row">
							<div class="col-xs-12 col-sm-4 col-md-4">
								<label for="block" class="form-control-label">Block: </label>
							</div>
							<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
								<input type="text" class="form-control" id="block" name="block" maxlength="10" value="<?php 
									if (isset($visitArray[0]['block'])) 
									{
										echo $visitArray[0]['block']; 
									}
								?>"/>
							</div>
						</div>
						<div class="form-group row">
							<div class="col-xs-12 col-sm-4 col-md-4">
								<label for="tumor_cellularity" class="form-control-label">Neoplastic Cellularity: </label>
							</div>
							<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
								<input type="number" class="form-control" id="tumor_cellularity" name="tumor_cellularity" min="0" max="100" value="<?php 
									if (isset($visitArray[0]['tumor_cellularity'])) 
									{
										echo $visitArray[0]['tumor_cellularity']; 
									}
								?>" />
							</div>
						</div>					
						<div class="form-group row">
							<div class="col-xs-12 col-sm-4 col-md-4">
								<label for="req_physician" class="form-control-label">Requesting physician: </label>
							</div>
							<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
								<input type="text" class="form-control" id="req_physician" name="req_physician" maxlength="78" value="<?php 
									if (isset($visitArray[0]['req_physician'])) 
									{
										echo $visitArray[0]['req_physician']; 
									}
								?>"/>
							</div>
						</div>
						<div class="form-group row">
							<div class="col-xs-12 col-sm-4 col-md-4">
								<label for="req_loc" class="form-control-label">Requesting Location: </label>
							</div>
							<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
								<input type="text" class="form-control" id="req_loc" name="req_loc" maxlength="78" value="<?php 
									if (isset($visitArray[0]['req_loc'])) 
									{
										echo $visitArray[0]['req_loc']; 
									}
								?>"/>
							</div>
						</div>

						<?php
							require_once('templates/shared_layouts/previous_comments.php');
						?>
						<div class="form-group row">
							<div class="col-xs-12 col-sm-4 col-md-4">
								<label for="comments" class="form-control-label">Comments:</label>
							</div>

							<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
								<textarea class="form-control" name="comments" rows="4" maxlength="65535" style="margin-bottom: 10px;"> </textarea>
							</div>
						</div>			
					</div>

					<?php
						// add the common hidden fields for every form
						require_once('templates/shared_layouts/form_hidden_fields.php');
					?>


					<?php
						// Add submit and home button if this is a pre-step.  Pre steps do not have a run_id.

						if (isset($_GET) && !isset($_GET['run_id']))
						{
							require_once('templates/shared_layouts/form_pre_submit_nav_buttons.php');
						}

						// Add submit and navigate buttons for reporting.
						else if (isset($_GET) && isset($_GET['run_id']) && isset($_GET['visit_id']))
						{	
							require_once('templates/shared_layouts/form_submit_nav_buttons.php');
						}
					?>
				</fieldset>
			</form>
		</div>
	</div>
</div>