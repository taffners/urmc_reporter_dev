<div class="container-fluid">
<?php
     if (isset($user_permssions) && strpos($user_permssions, 'admin') !== false)
     {                        
?>     
     <div class="row">
          <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
               <a href="?page=add_all_missing_qc_page" class="btn btn-primary btn-primary-hover" role="button">Add all missing QC pages</a>
               
          </div>
     </div>
<?php
     }
?>
     <fieldset>
         
          <legend>All QC Pages</legend>
         

          <div class="row" style="overflow-x:auto;">
               <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <table class="formated_table sort_table_no_inital_sort_no_paging">
                         <thead>
                              <th>page id</th>
                              <th>User Name</th>
                              <th>Page Name</th>
                              <th>Page Description</th>
                              <th>How to Access Page</th>
                              <th>Allowed Users</th>
                              <th>Page Assumptions</th>
                              <th>Module Name</th>
                         <?php
                              if (isset($user_permssions) && strpos($user_permssions, 'admin') !== false)
                              {                        
                         ?>
                              <th class="d-print-none">Update Page</th>
                              <th class="d-print-none">Add New Validation</th>
                         <?php
                              }
                         ?>
                              <th class="d-print-none">All Page Validation Reports</th>
                         </thead>
                         <tbody>

                         <!-- iterate over all pending runs and add each entry as a new row in the table-->

               <?php

                    for($i=0;$i<sizeof($all_qc_pages);$i++)
                    {    
               ?>
                              <tr>
                                  <td><?= $all_qc_pages[$i]['page_id'];?></td>
                                  <td><?= $all_qc_pages[$i]['user_name'];?></td>
                                  <td><?= $all_qc_pages[$i]['page_name'];?></td>
                                  <td><?= $all_qc_pages[$i]['page_description'];?></td>
                                  <td><?= $all_qc_pages[$i]['how_to_access_page'];?></td>
                                  <td><?= $all_qc_pages[$i]['permissions'];?></td>
                                  <td><?= $all_qc_pages[$i]['page_assumption'];?></td>
                                  <td><?= $all_qc_pages[$i]['module_name'];?></td>
                                  

                         <?php
                              if (isset($user_permssions) && strpos($user_permssions, 'admin') !== false)
                              {                        
                         ?>
                                  <td class="d-print-none"><a href="?page=add_a_qc_page&page_id=<?= $all_qc_pages[$i]['page_id'];?>" class="btn btn-primary btn-primary-hover" role="button">Update Page</a></td>
                                  <td class="d-print-none"><a href="?page=add_a_validation&page_id=<?= $all_qc_pages[$i]['page_id'];?>" class="btn btn-primary btn-primary-hover" role="button">Add Validation</a></td>
                         <?php
                              }
                         ?>
                                   <td class="d-print-none">

                         <?php
                              if (!empty($all_qc_pages[$i]['num_validations']))
                              {
                         ?>
                                        <a href="?page=page_validation_reports&page_id=<?= $all_qc_pages[$i]['page_id'];?>" class="btn btn-success btn-success-hover" role="button" aria-label="open qc report">
                                             <img src="images/qc_report.png" style="margin-left:5px;height:45px;width:45px;" alt="report icon">
                                        </a>
                         <?php
                              }
                         ?>
                                        
                                   </td>

                              </tr>
               <?php 
                    }
               ?>
                         </tbody>
                    </table>
               </div>
          </div>
     </fieldset>
</div>
