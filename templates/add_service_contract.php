<div class="container-fluid">

	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="height:500px;">

			<form class="form" method="post" class="form-horizontal">
				<fieldset>

<?php 
				if (isset($_GET['instrument_service_contract_id']))
				{
?>
					<legend class='show'>
						Update a Service Contract for <span style="color:black;background-color:white;"><?= $utils->instrumentInputName($instrumentArray[0]);?></span>
					</legend>
<?php
				}
				else 
				{
?>
					<legend class='show'>
						Add a Service Contract to <span style="color:black;background-color:white;"><?= $utils->instrumentInputName($instrumentArray[0]);?></span> 
					</legend>
<?php 
				}
?>
					<div class="form-group row">
						<div class="col-xs-12 col-sm-4 col-md-4">
							<label for="contract_num" class="form-control-label">Contract NO.: <span class="required-field">*</span></label>
						</div>
						<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
							<input type="text" class="form-control"  name="contract_num" required="required" maxlength="20" value="<?= $utils->GetValueForUpdateInput($serviceArray, 'contract_num');?>"/>
						</div>
					</div>
					<div class="form-group row">
						<div class="col-xs-12 col-sm-4 col-md-4">
							<label for="quote_num" class="form-control-label">Quote NO.: <span class="required-field">*</span></label>
						</div>
						<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
							<input type="text" class="form-control"  name="quote_num" required="required" maxlength="20" value="<?= $utils->GetValueForUpdateInput($serviceArray, 'quote_num');?>"/>
						</div>
					</div>
					<div class="form-group row">
						<div class="col-xs-12 col-sm-4 col-md-4">
							<label for="start_date" class="form-control-label">Start Date: <span class="required-field">*</span></label>
						</div>
						<div class="col-xs-12 col-sm-8 col-md-3 col-lg-3 pull-left">
							<input type="date" class="form-control"  name="start_date" required="required" value="<?= $utils->GetValueForUpdateInput($serviceArray, 'start_date');?>"/>
						</div>
					</div>
					<div class="form-group row">
						<div class="col-xs-12 col-sm-4 col-md-4">
							<label for="expiration_date" class="form-control-label">Expiration Date: <span class="required-field">*</span></label>
						</div>
						<div class="col-xs-12 col-sm-8 col-md-3 col-lg-3 pull-left">
							<input type="date" class="form-control"  name="expiration_date" required="required" value="<?= $utils->GetValueForUpdateInput($serviceArray, 'expiration_date');?>"/>
						</div>
					</div>
					<div class="form-group row">
						<div class="col-xs-12 col-sm-4 col-md-4">
							<label for="po" class="form-control-label">PO#: <span class="required-field">*</span></label>
						</div>
						<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
							<input type="text" class="form-control"  name="po" required="required" maxlength="20" value="<?= $utils->GetValueForUpdateInput($serviceArray, 'po');?>"/>
						</div>
					</div>

					<div class="form-group row">
						<div class="col-xs-12 col-sm-4 col-md-4">
							<label for="price" class="form-control-label">Price: <span class="required-field">*</span></label>
						</div>
						<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
							<input type="number" step=".01" class="form-control"  name="price" required="required" value="<?= $utils->GetValueForUpdateInput($serviceArray, 'price');?>"/>
						</div>
					</div>

					<div class="form-group row">
						<div class="col-xs-12 col-sm-4 col-md-4">
							<label for="description" class="form-control-label">Description:</label>
						</div>
						<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
							<textarea id="description" class="form-control" name="description" rows="3" maxlength="255"><?= $utils->GetValueForUpdateInput($serviceArray, 'description');?></textarea>
						</div>
					</div>

					<div class="form-group row">
						<div class="offset-xs-4 offset-sm-4 offset-md-4 offset-lg-4 col-xs-2 col-sm-2 col-md-2 col-lg-2">
							<button type="submit" id="<?= $page;?>_submit" name="<?= $page;?>_submit" value="Submit"  class="btn btn-primary btn-primary-hover">
								Submit
							</button>
						</div>
						<div class="offset-xs-4 offset-sm-4 offset-md-4 offset-lg-4 col-xs-2 col-sm-2 col-md-2 col-lg-2">
							<a href="?page=setup_instruments" class="btn btn-primary btn-primary-hover" role="button">
								
								All Instruments
							</a>
						</div>
					</div>
				</fieldset>
			</form>
		</div>
	</div>
</div>
