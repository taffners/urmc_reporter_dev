<div class="container-fluid">
	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="height:500px;">

			<form class="form" method="post" class="form-horizontal">
				<fieldset>
					<legend >
						<?= $page_title;?>
					</legend>
<?php
	if (!isset($_GET['pool_type']))
	{
?>					
					<div class="form-group row mb-0" >
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 alert alert-info">
							Features of Single Analyte Pools differ depending on the test.  The overall goal of Single Analyte Pools it to group samples processed together, change status of sample from pending to in process, and add assist the sign out process of the tests.  These feature might not be useful for all tests.  A Single Analyte Pool I required to make any single analyte reports.  Example: Idylla reports
						</div>
					</div>
					<div class="form-group row">
						<div class="col-xs-12 col-sm-4 col-md-4">
							<label for="pool_type" class="form-control-label">Select Pool Type: <span class="required-field">*</span></label>
						</div>
				
						<div class="col-xs-12 col-sm-8 col-md-8">
							<select class="selectpicker" data-live-search="true" data-width="100%" name="pool_type" id="pool_type">
									<option value="" selected="selected"></option>
							<?php 

								foreach ($all_single_analyte_tests as $key => $test)
								{

									if 	(
											$test['assay_name'] !== 'ngs' &&
											$test['assay_name'] !== 'extraction' &&
											$test['assay_name'] !== 'Quantification'
										)
									{
							?>
									<option value="<?= $test['orderable_tests_id']; ?>">
										<?= $test['test_name']; ?> (<?= $test['count']; ?> tests pending)
									</option>
							<?php 
									}
								}
							?>									
							</select>
						</div>
					</div>
<?php
	}
	else if (isset($_GET['pool_type']) && isset($pending_tests) && isset($pending_tests[0]['test_name']))
	{		
?>					
					<div class="form-group row">
						
					
						<div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
							<label for="run_number" class="form-control-label">Run Number (DO NOT INCLUDE YEAR): <span class="required-field">*</span></label>
						</div>
						
					<?php
					if (!empty($last_run_number))
					{
					?>
						<div class="col-xs-12 col-sm-8 col-md-2 col-lg-2">							
							<div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
								<input type="number" name="run_number" min="0" required="required" step="1" value="<?= intval($last_run_number[0]['run_number']) +1 ;?>"/>
							</div>
						</div>
						<div class="col-xs-12 col-sm-12 col-md-8 col-lg-8 alert alert-info">
							<h3><strong>Last test pool for <?= $last_run_number[0]['test_name'];?>:</strong> <?= ' Run # '.$last_run_number[0]['run_number'].'-'.date('y', strtotime($last_run_number[0]['start_date'])).' (Tech: '.$last_run_number[0]['tech_name'].' Start Date: '.$last_run_number[0]['start_date'].')';?>
						</div>
					<?php
					}
					else
					{
					?>	
						<div class="col-xs-12 col-sm-8 col-md-2 col-lg-2">							
							<div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
								<input type="number" name="run_number" min="0" required="required" step="1" />
							</div>
						</div>
					<?php
					}
					?>
					</div>
					<div class="form-group row">
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
							<h3>Select the samples to add to this Single Analyte Pool:</h3> 
						</div>
					</div>

					<div class="form-group row mb-0" >
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 alert alert-info">
							Below are all of the pending <?= $pending_tests[0]['test_name'];?> Tests. <br><br>
							<ul>
								<li>Use the select button to add sample tests to the single analyte pool</li>
								<li>The sample tests are currently in the order added to <?= SITE_TITLE_ABBR;?>.  If you would like them in a different order in the pool drag the grey boxes to the order you would like them.  
								</li>
							</ul>
							<div class="row">
								<div class="col-xs-12 col-sm-9 col-md-9 col-lg-9">
									<b style="margin-left:25px;">
										<span style="color:#337ab7;">MD#</span> | 
										Last Name |
										First Name |
										<span style="color:#ed5575;">Soft Lab #</span> |  
										
										MRN |   
										Soft Path Num |
										Test Tissue |
										Block |
										<span style="color:#24ba46;background-color:black;margin:5px;padding:5px;">Extraction/Quantification Count</span> |

										<span style="color:#24ba46;background-color:black;margin:5px;padding:5px;">Stock conc.</span> |

										<span style="color:#24ba46;background-color:black;margin:5px;padding:5px;">Dilution conc.</span>
										

									</b>
								</div>
								<div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
									<button type="button" id="unselect-chkbx" class="btn-primary btn-primary-hover float-right">
								Unselect all
							</button>
								</div>
							</div>
						</div>
					</div>

					<div class="form-group row">

						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
							<ul id="sortable-order-samples" class="connectedSortable sortable-list">
					<?php
						$num_checked_samples = 0;
						foreach($pending_tests as $key => $samps)
						{		
					?>
								<li class="ui-state-default ui-bkgrd-color-grey" id="<?= $samps['sample_log_book_id'];?>">
									<span class="ui-icon ui-icon-arrowthick-2-n-s" style="margin-top:5px;"></span>
									<div class="form-group row" style="margin:0px;">
										<div class="col-xs-12 col-sm-9 col-md-9 col-lg-9">
											<label for="text_example" class="form-control-label" style="font-size:16px;margin-bottom:0px;">
												<b>
													<span style="color:#337ab7;"><?=$samps['mol_num'];?></span> | 
													
													<?= $samps['last_name'];?> | 
													<?= $samps['first_name'];?>  | 
													<span style="color:#ed5575;"><?=$samps['soft_lab_num'];?></span> | 
													<?= $samps['medical_record_num'];?> | 
													<?= $samps['soft_path_num'];?> |
													<?= $samps['test_tissue'];?> | 
													<?= $samps['block'];?> |
													<?php
													if (!empty($samps['extraction_count']))
													{
													?>
													<span style="color:#24ba46;background-color:black;margin:5px;padding:5px;"><?= $samps['extraction_count'];?></span>
													<?php
													}
													?>
													| 
													<?php

													if (!empty($samps['stock_concentrations']))
													{
													?>
													<span style="color:#24ba46;background-color:black;margin:5px;padding:5px;"><?= $samps['stock_concentrations'];?></span>
													<?php
													}
													?>
													| 
													<?php

													if (!empty($samps['dilution_concentrations']))
													{
													?>
													<span style="color:#24ba46;background-color:black;margin:5px;padding:5px;"><?= $samps['dilution_concentrations'];?></span>
													<?php
													}
													?>
												</b>
											</label>
										</div>
										<div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">	
											<span class="float-right">
												<input type="checkbox" class="chkbx" name="selected-samples[]" value="<?= $samps['ordered_test_id'];?>" <?php
												if 	(
														isset($_GET['max_num_samples_per_run']) && 
														$num_checked_samples <= intval($_GET['max_num_samples_per_run']) &&
														!empty($samps['extraction_count']) 
													)
												{
													$num_checked_samples++;
													echo 'checked';
												}
												else if (!isset($_GET['max_num_samples_per_run']) &&
														!empty($samps['extraction_count']))
												{
													echo 'checked';
												}
												?>> 
												Select				
											</span>
										</div>
									</div>					
								</li>
					<?php
						}
					?>			
							</ul>
						</div>
					</div>	
						
					<div class="form-group row">
						<div class="col-xs-12 col-sm-4 col-md-4">
							<label for="comments" class="form-control-label">Problems/Comments/Notes:</label>
						</div>

						<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
							<textarea class="form-control" name="comments" rows="4" maxlength="65535" style="margin-bottom: 10px;"></textarea>
						</div>
					</div>				
<?php
	}
?>
					<div class="form-group row">
						<div class="offset-xs-4 offset-sm-4 offset-md-4 offset-lg-4 col-xs-2 col-sm-2 col-md-2 col-lg-2">
							<button type="submit" id="<?= $page;?>_submit" name="<?= $page;?>_submit" value="Submit"  class="btn btn-primary btn-primary-hover">
								Submit
							</button>
						</div>
<?php
	if (isset($_GET['pool_type']))
	{
?>						
						<div class="offset-xs-4 offset-sm-4 offset-md-4 offset-lg-4 col-xs-2 col-sm-2 col-md-2 col-lg-2">
							<a href="?page=add_single_analyte_pool" class="btn btn-primary btn-primary-hover" role="button">
								Change test selected
							</a>
						</div>
<?php
	}
?>
					</div>
					
				</fieldset>
			</form>
		</div>
	</div>
</div>