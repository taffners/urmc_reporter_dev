<div class="container-fluid">
	
	<div class="row">
		
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			<form class="form" method="post" class="form-horizontal">
				<fieldset>
				<?php 
					if (isset($_GET['module_id']))					
					{
				?>
					<legend>
						Update Module 
					</legend>
					
				<?php 
					}
					else
					{
				?>
					<legend>
						Add Module
					</legend>
				<?php
					}
				?>					
					
					<div class="form-group row">
						<div class="col-xs-12 col-sm-4 col-md-4">
							<label for="module_name" class="form-control-label" >Module Name: <span class="required-field">*</span></label>
						</div>
						<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
							<input type="text" class="form-control" id="module_name" name="module_name" maxlength="100" value="<?= $utils->GetValueForUpdateInput($module_array, 'module_name');?>" required="required"/>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 alert alert-info">
							Make sure to upload the diagram to the about folder before filling this out.
						</div>
					</div>

					<div id="form-control-fields">
						<div class="form-group row">
							<div class="col-xs-12 col-sm-4 col-md-4">
								<label for="module_diagram" class="form-control-label">Module Diagram:</label>
							</div>
							<div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">
								
								<select class="selectpicker" data-live-search="true" data-width="100%" name="module_diagram" id="module_diagram">

									<option value="" selected="selected"></option>
								<?php

									foreach ($all_possible_diagrams as $key => $diagram)
			               			{	
			               				if ($diagram !== 'index.html')
			               				{
								?>
											<option value="<?= $diagram; ?>" <?= $utils->GetValueForUpdateSelect($module_array, 'module_diagram', $diagram)?>>
													<?= $diagram; ?>
											</option>
								<?php
										}
									}
								?>
								</select>								
							</div>
						</div>
					</div>
					<div class="form-group row">
						<div class="col-xs-12 col-sm-4 col-md-4">
							<label for="module_description" class="form-control-label">Module Description:</label>
						</div>

						<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
							<textarea class="form-control" name="module_description" rows="4" maxlength="65535" style="margin-bottom: 10px;" id="module_description"><?= $utils->GetValueForUpdateInput($module_array, 'module_description');?></textarea>
						</div>
					</div>
					

			




					<div class="form-group row">
						<div class="offset-xs-4 offset-sm-4 offset-md-4 offset-lg-4 col-xs-2 col-sm-2 col-md-2 col-lg-2">
							<button type="submit" id="<?= $page;?>_submit" name="<?= $page;?>_submit" value="Submit"  class="btn btn-primary btn-primary-hover">
								Submit
							</button>
						</div>
					</div>
				</fieldset>
			</form>
		</div>
	</div>
</div>