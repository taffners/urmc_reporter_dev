<div id="add-sample-name-modal" title="Add sample name"></div>
<div class="container-fluid">
     <?php
          require_once('templates/lists/pre_ngs_samples_list.php');
     ?>

	<div class="row">

		<!-- update area -->
		<div id="toggle-report-progress-bar" class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			<form id="upload_NGS_data" name="upload_NGS_data" class="form" method="post" class="form-horizontal" enctype="multipart/form-data" autocomplete="off" >
				<fieldset>
					<legend id='new_legend' class='show'>
						Upload NGS data
					</legend>
			
					<div class="form-group row required-input">
						<div class="col-xs-12 col-sm-4 col-md-4">
							<label for="run_date" class="form-control-label" >Run Date: <span class="required-field">*</span></label>
						</div>
						<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
							<input type="date" class="form-control" id="run_date" name="run_date" required <?php
							if (isset($library_pool) && isset($library_pool[0]['ts_date']))
							{
								echo 'value="'.$library_pool[0]['ts_date'].'"';
							}

							?>/>
						</div>
					</div>
<?php
	///////////////////////////////////////////////////////////////////////////
	// Fields wet bench techs, NGS TSV, and  Coverage file used for all panels
	///////////////////////////////////////////////////////////////////////////
?>
					<div id="wet_bench_techs" class="form-group row">
						<div class="col-xs-12 col-sm-4 col-md-4">
							<label for="wet_bench_techs" class="form-control-label" >Select All Wet Bench Techs for this Test: <span class="required-field">*</span></label>
						</div>
						<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
					<?php
						// add all the users to a check box
						if (isset($all_wet_bench_users))
						{
							foreach($all_wet_bench_users as $key => $user)
							{
					?>
							<div class="checkbox">
						  		<label><input type="checkbox" name="wet_bench_techs[<?= $user['user_id'];?>]" value="<?= $user['user_id'];?>"><?= $user['first_name'];?> <?= $user['last_name'];?> <?= $user['credentials'];?></label>
							</div>
					<?php									
							}

						}
					?>

						</div>
					</div>
				<?php
					if (isset($ngs_panel) && $ngs_panel === 'Myeloid/CLL Mutation Panel')
					{
				?>
					<div class="row">
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 alert alert-info">
							<h3>Variant Studio Filter Instructions</h3>
							<ul>
								<li>Apply desired filter to all samples EXCEPT the positive control.  Some variants we look for in the positive control are filtered out with the filter.</li>
							</ul>
						</div>
					</div>
				<?php
					}

				?>
					
<?php
	///////////////////////////////////////////////////////////////////////
	// Show an example file for tsv.
	///////////////////////////////////////////////////////////////////////

	// The myeloid panel gets sample name from the coverage file so this value needs 
	// to make sure it is correct and no typos.  It also needs to be chosen by user 
	// from all samples on the chip.  However, the OFA the sample name is obtained 
	// from the ngs tsv file.  
	$sample_name_select_list = '<div id="sample_name_row_id" class="form-group row d-none">
						<div class="col-xs-12 col-sm-4 col-md-4">
							<label for="sample_name" class="form-control-label" >Choose Sample Name: <span class="required-field">*</span></label>
						</div>
						<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
							<select class="form-control" name="sample_name" id="sample_name">
								<option value=""></option>
							</select>
						</div>						
					</div>';

	if (isset($pending_pre_runs[0]['full_sample_name']))
	{

	$sample_name_input = '<div id="sample_name_row_id" class="form-group row">
						<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
							<label for="sample_name" class="form-control-label" >Sample Name: <span class="required-field">*</span></label>
						</div>
						<div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
							<input type="text" class="form-control" id="sample_name" name="sample_name" readonly required maxlength="100" value="'.$pending_pre_runs[0]['full_sample_name'].'"/>
						</div>';

		if ($ngs_panel === 'Oncomine Focus Hotspot Assay' && isset($user_permssions) &&  strpos($user_permssions, 'ngs_samp_nam_ov') !== false)
     	{ 
     		$sample_name_input.='<div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
							<button type="button" id="manual_sample_name_override" class="btn btn-primary btn-primary-hover">
								Manual Override Sample Name
							</button>
						</div>';
     	}

	$sample_name_input.='</div>';
	}
	$ofa_layout = array(
		'tsv_descript'		=>  	'NGS TSV File (variant_folder/*filtered_SB_added.tsv)',
		'tsv_file' 			=> 		'example_upload_files/3701-18_180927B_v180426_filtered_SB_added.tsv',//'example_upload_files/ofa_ngs_tsv.tsv',
		'tsv_info'		=> 	'',
		'tsv_hover_card_id'	=> 	'',//'ofa-ngs-info-hover-card',

		'cov_file' 		=> 	'example_upload_files/all_coverage_amplicons.tsv',//'example_upload_files/ofa_cov_file.cov.xls',
		'cov_descript'		=> 	'Amplicon Coverage File (coverage/all_coverage_amplicons.tsv)',
		'cov_file_info'	=>	'',
		'cov_hover_card_id'	=> 	'',//'ofa-coverage-info-hover-card',
		'sample_name_field'	=> 	isset($sample_name_input)?$sample_name_input:''
	);

	$myeloid_layout = array(
		'tsv_descript'		=>  	'NGS TSV File',
		'tsv_file' 		=> 	'example_upload_files/myeloid_tsv.tsv',
		'tsv_info'		=> 	'',
		'tsv_hover_card_id'	=> 	'myeloid-ngs-info-hover-card',

		'cov_file' 		=> 	'example_upload_files/cov_file_depth_coverage.txt',
		'cov_descript'		=> 	'Coverage File (depth_coverage.txt)',
		'cov_file_info'	=>	'',
		'cov_hover_card_id'	=> 	'myeloid-coverage-info-hover-card',
		'sample_name_field'	=> 	$sample_name_select_list
	);

	$layouts = array(
		'Oncomine Focus Hotspot Assay' => $ofa_layout,
		'Myeloid/CLL Mutation Panel' => $myeloid_layout
	);	
?>								
<?php

	// if a pool is linked to this upload and a backup location is known provide that to the user
	if 	( 
			isset($library_pool) && 
			isset($library_pool[0]['backup_location']) && 
			!empty($library_pool[0]['backup_location'])
		)
	{
?>
					<div class="row">
						<div class="alert alert-info">
							Files for upload are located at: <?= $API_Utils->ConvertBackupLocToTechFormat($library_pool[0]['backup_location']);?>
						</div>
					</div>
<?php
	}

	if (array_key_exists($ngs_panel, $layouts))
	{
		$curr_layout = $layouts[$ngs_panel];
?>

					<div class="form-group row required-input">		
						<div class="col-xs-12 col-sm-4 col-md-4">
							<label for="NGS_file" class="form-control-label"><?= $curr_layout['tsv_descript'];?> <span class="required-field">*</span></label>
						</div>
						<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
							
							<div class="row">
								<!-- Add data-sample_type and data-ngs_panel_id to NGS file input.  This is used for myeloid positive control.  Since the user needs to make sure they download a tsv from variant studio which has no filters a check is performed via javascript to make sure there are at least 600 rows in the file.  This will ensure no filter was applied. -->
								<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
									<input type="file" id="NGS_file_upload" name="NGS_file" data-sample_type="<?=$visitArray[0]['sample_type'];?>" data-ngs_panel_id="<?=$visitArray[0]['ngs_panel_id'];?>"
									data-soft_lab_num="<?= $visitArray[0]['soft_lab_num'];?>"
									data-mol_num="<?= $visitArray[0]['mol_num'];?>"
									data-visit_id="<?= $visitArray[0]['visit_id'];?>"/>
								</div>
								<div class="col-xs-12 col-sm-4 col-md-1 col-lg-1">
									<a  href="<?= $curr_layout['tsv_file'];?>" role="button" class="btn-sm btn-primary btn-primary-hover" target="_blank">
										Example
									</a>
								</div>
<?php
	}
	///////////////////////////////////////////////////////////////////////
	// Add an info hover-card for info on getting NGS file
	// set id for hover-card toggle for each ngs panel type.  Each id will show 
	// different info in the hover-card
	// http://designwithpc.com/Plugins/Hovercard#demo
	// JS controller adds card body
	///////////////////////////////////////////////////////////////////////

	// only add the possibility of info on file if $hover_card_id is set.
	if ($utils->varSetNotEmpty($curr_layout['tsv_hover_card_id']))
	{
?>		 
								<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
									<button id="ngs_tsv_file_info_btn" class="info-btn">
										<label id="<?= $curr_layout['tsv_hover_card_id'];?>">
											<span class="fas fa-info-circle"></span>
										</label>
									</button>
								</div>
<?php
	}
?>					
							</div>
						</div>
					</div>
					<div class="form-group row required-input">
						<div class="col-xs-12 col-sm-4 col-md-4">
							<label for="depth_thres" class="form-control-label">
								<?= $curr_layout['cov_descript'];?>
								<span class="required-field">*</span>
							</label>
						</div>
						<div class="col-xs-12 col-sm-8 col-md-8 pull-left">

							<div class="row">

								<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
									<input type="file" id="depth_thres_upload" name="depth_thres"/>
								</div>

								<div class="col-xs-12 col-sm-4 col-md-1 col-lg-1">
									<a  href="<?= $curr_layout['cov_file'];?>" role="button" class="btn-sm btn-primary btn-primary-hover" target="_blank">
										Example
									</a>
								</div>			
<?php
	///////////////////////////////////////////////////////////////////////
	// Add an info hover-card for info on getting NGS file
	// set id for hover-card toggle for each ngs panel type.  Each id will show 
	// different info in the hover-card
	// http://designwithpc.com/Plugins/Hovercard#demo
	// JS controller adds card body
	///////////////////////////////////////////////////////////////////////
	
	// only add the possibility of info on file if $hover_card_id is set.
	if ($utils->varSetNotEmpty($curr_layout['cov_hover_card_id']))
	{
?>		 
								<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
									<button id="cov_file_info_btn" class="info-btn">
										<label id="<?= $curr_layout['cov_hover_card_id'];?>">
											<span class="fas fa-info-circle"></span>
										</label>
									</button>
								</div>
<?php
	}
?>
							</div>
						</div>
					</div>
					<div id="not-found-row" class="form-group row d-none">
						<!-- This is a hidden field to store not found as value if user confirms that no amplicons are present as low coverage. -->
						<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
							<input type="text" class="form-control" id="not_found" name="not_found"/>
						</div>						
					</div>	
					<div id="ngs-panel-row" class="form-group row d-none">
						<!-- This is a hidden field to store not found as value if user confirms that no amplicons are present as low coverage. -->
						<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
							<input type="text" class="form-control" id="ngs-panel" name="ngs-panel" value="<?= $ngs_panel;?>"/>
						</div>						
					</div>
					
					<!-- add sample name field -->
					<?= $curr_layout['sample_name_field'];?>

					<!-- Add via javascript Coverage file type here after coverage file was selected -->
					<div id="cov_file_type_row_id" class="form-group row d-none">
						<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
							<input type="text" class="form-control" id="cov_file_type_id" name="cov_file_type_id" value=""/>
						</div>
					</div>

					<div class="form-group row d-none">
						<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
							<input type="text" class="form-control" id="ngs-tsv-md5" name="ngs-tsv-md5" value=""/>
						</div>
					</div>
					<div class="form-group row d-none">
						<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
							<input type="text" class="form-control" id="amp-cov-md5" name="amp-cov-md5" value=""/>
						</div>
					</div>

					<div class="form-group row d-none">
						<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
							<input type="text" class="form-control" id="manual_override_performed" name="manual_override_performed" value="no"/>
						</div>
					</div>
					<div class="form-group row d-none">
						<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
							<input type="text" class="form-control" id="manual_override_original_name" name="manual_override_original_name" value="<?= isset($pending_pre_runs[0]['full_sample_name'])? $pending_pre_runs[0]['full_sample_name']: '';?>"/>
						</div>
					</div>
					<div class="row">
						<div class="offset-xs-4 offset-sm-4 offset-md-4 offset-lg-4 col-xs-2 col-sm-2 col-md-2 col-lg-2">
							<button type="submit" id="<?= $page;?>_submit" name="<?= $page;?>_submit" value="Submit"  class="btn btn-primary btn-primary-hover">
								Submit							
							</button>
						</div>
						<div class="offset-xs-4 offset-sm-4 offset-md-4 offset-lg-4 col-xs-2 col-sm-2 col-md-2 col-lg-2">
							<a href="?page=home" class="btn btn-primary btn-primary-hover" role="button">
								<img src="images/house.png" style="height:23px;width:23px;margin-right:5px;" alt="house icon">
								Home
							</a>
						</div>
					</div>
					<!-- add the common hidden fields for every form -->
					<?php
						require_once('templates/shared_layouts/form_hidden_fields.php')
					?>
				</fieldset>
			</form>
		</div>
	</div>
</div>