
<?php
	if ($brute_force_occuring)
	{
?>
	<div class="container-fluid">

		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				<img src="images/homer_doh.jpg" alt="homer doh cartoon">
				<br>
				<h1>Since to many failed logins have occurred recently <?= htmlspecialchars(SITE_TITLE);?> is temporarily not available.</h1>
				<br><br>
				This application complies with PCI Data Security Standards.   
				<ul>
					<li>8.5.13 (Limit repeated access attempts by locking out after not more than five attempts)</li>
					<li>8.5.14 (Set the lockout duration to thirty minutes or until administrator enables the account).</li>
				</ul>
			</div>
		</div>
	</div>
<?php
	}
	else
	{
?>

	<div class='container-fluid' id="loginContainer" style="padding: 0px;">
		
	<?php
		if (isset($_GET['error']) && !empty($_GET['error']) && $_GET['error'] === 'redirect_wrong_app')
		{
	?>
		<div class='row' >
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				<div class="alert alert-danger">
					<span class="fas fa-exclamation-triangle"></span> It looks like you tried to login to the wrong App. 
				</div>
			</div>
		</div>
	<?php
		}
	?>	


	<?php
		if (isset($_GET['inactive_time']))
		{
	?>
		<div class='row' >
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				<div class="alert alert-danger">
					<span class="fas fa-exclamation-triangle"></span> No activity within <?= htmlspecialchars($_GET['inactive_time']);?> seconds.  Please log in again.
				</div>
			</div>
		</div>
	<?php
		}
	?>		
		<div id="login-div" class="row" style="text-align:justify;border-radius:10px;font-size:18px;padding-left:20px;">
			<div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
				<div style="margin-top:50px;">
					<?= htmlspecialchars(SITE_TITLE); ?> is a tool to speed up Clinical Molecular Analysis at University of Rochester Medical Center.  It is transforming into a one stop, start to finish software tool where samples are tracked from login to report generation.  Single analyte and NGS tests are all logged into <?= htmlspecialchars(SITE_TITLE); ?> providing a test tracking system which allows managers the ability to easily produce monthly reports and track audits to evaluate laboratory productivity.  <?= htmlspecialchars(SITE_TITLE);?> transforms into an analysis and report building system for NGS tests.  Allowing users to access the growing knowledge base along with integrated publicly available databases.  
				</div>  

				<!-- Hide all login information if they are using a browser which is not supported -->
				<div id="all_login_fields" class="show" style="margin-top:80px;">
					<div class="FormBoxCustom">
						<form class="form" method="post" class="form-horizontal">
							
							<div class="form-group row">
								<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
									<label for="inputEmailAddress" class="form-control-label">Email Address: </label>
								</div>
								<div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
									<input type='email' id="inputEmailAddress" name='email_address' class="form-control" placeholder='Full Email Address' required>
								</div>
							</div>
							<div class="form-group row">
								<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
									<label for="password" class="form-control-label">Password:</label>
								</div>

								<div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
									<input type="password" class="form-control" name="password" placeholder="Password" required>
								</div>
							</div>
							<div class="form-group row">
								<div class="col-xs-8 col-sm-8 col-md-8">
									<button type="submit" name="login_submit" value="submit" class='btn btn-primary btn-lg'>Sign In</button>
								</div>
							</div>	
						</form>
					</div>
				</div>
			</div>

			<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
				<img src="images/background_DNA.png" height="700" align="right">
			</div>
		</div>

		
	</div>
<?php
	}
?>

