<div class="container-fluid">
	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="height:500px;">
			<form class="form" method="post" class="form-horizontal">
				<fieldset>
					<legend class='show'>
						<?= $page_title;?>
					</legend>


					<div class="form-group row">
						<div class="col-xs-12 col-sm-4 col-md-4">
							<label for="gene" class="form-control-label">Gene: <span class="required-field">*</span></label>
						</div>
						<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
							<input type="text" class="form-control" id="gene" name="gene" maxlength="7" value="<?= $utils->GetValueForUpdateInput($genes_covered_by_panel_array, 'gene');?>" required="required"/>
						</div>
					</div>

					<div class="form-group row">
						<div class="col-xs-12 col-sm-4 col-md-4">
							<label for="exon" class="form-control-label">Exon: </label>
						</div>
						<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
							<input type="text" class="form-control" id="exon" name="exon" maxlength="30" value="<?= $utils->GetValueForUpdateInput($genes_covered_by_panel_array, 'exon');?>"/>
						</div>
					</div>

					<div class="form-group row">
						<div class="col-xs-12 col-sm-4 col-md-4">
							<label for="accession_num" class="form-control-label">Accession Num: </label>
						</div>
						<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
							<input type="text" class="form-control" id="accession_num" name="accession_num" maxlength="16" value="<?= $utils->GetValueForUpdateInput($genes_covered_by_panel_array, 'accession_num');?>"/>
						</div>
					</div>

					<div class="form-group row">
						<div class="col-xs-12 col-sm-4 col-md-4">
							<label for="date_gene_added" class="form-control-label">Date Gene Added to panel (Use a date in future): <span class="required-field">*</span></label>
						</div>
						<div class="col-xs-12 col-sm-8 col-md-4 col-lg-3">
							<input type="date" class="form-control" id="date_gene_added" name="date_gene_added" value="<?= $utils->GetValueForUpdateInput($genes_covered_by_panel_array, 'date_gene_added');?>"required="required"/>
						</div>
					</div>

					<div class="form-group row">
						<div class="offset-xs-4 offset-sm-4 offset-md-4 offset-lg-4 col-xs-2 col-sm-2 col-md-2 col-lg-2">
							<button type="submit" id="<?= $page;?>_submit" name="<?= $page;?>_submit" value="Submit"  class="btn btn-primary btn-primary-hover">
								Submit
							</button>
						</div>
					</div>	
				</fieldset>
			</form>
		</div>
	</div>
</div>