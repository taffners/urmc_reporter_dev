<!-- this is the summary for the report -->

<?php
	if (isset($_GET['run_id']))
	{
		require_once('templates/lists/run_info_list.php');	
	}	
?>

<div class="container-fluid">
	<div class="row">
		<?php
			if (isset($_GET['run_id']))
			{
				require_once('templates/shared_layouts/report_progress_bar.php');
			}
		?>
		<!-- update area -->
		<div id="toggle-report-progress-bar" class="col-xs-10 col-sm-10 col-md-10 col-lg-10">
			
			<fieldset>
				<legend>
					Send Confirmations
				</legend>
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 alert alert-info">
						<strong>How confirmations work:</strong>

						<ul>
							<li>Edit the message below to include a reason or type of confirmation request.</li>
							<li>Once you send the confirmation request, all users with confirmation permission settings will receive an with your message.  Current users include <?= $email_utils->getEmailAddresses($all_confirmation);?></li>
							
							<li>Also the message will be added to the message board for record keeping and the pending confirmation will be added to the pending confirmation list.</li>
							<li>Once the confirmation has been completed the tech should go into infotrack and mark the confirmation as complete and add a status of confirmed or not confirmed.  During this time the tech will have an option to send a notification to other users of the finished confirmation.</li>
							
						</ul>

					</div>
				</div>
				<div class="alert alert-warning">
					<h5 class="FormHeaderCustom">
						Send Confirmation Request on <?=$_GET['genes'];?> <?=$_GET['coding'];?> <?=$_GET['amino_acid_change'];?> # <?=$_GET['visit_id'];?>		
					</h5>

				
					<form class="form" method="post" class="form-horizontal" style="margin-top:10px;">
								
						<!-- add text box for new message -->
						<div class="form-group">
							<textarea class="form-control" name="message" rows="15"  maxlength="65535">Please confirm the following variant.
Gene: <?=$_GET['genes'];?> 
Coding: <?=$_GET['coding'];?> 
Amino Acid Change: <?=$_GET['amino_acid_change'];?> 

mol#: <?= $visitArray[0]['order_num'];?> 
MD#: <?= $visitArray[0]['mol_num'];?> 
Soft Lab #: <?= $visitArray[0]['soft_lab_num'];?> 

report #: <?=$_GET['visit_id'];?>

<?php
if (isset($visitArray[0]['ngs_panel']) && !empty($visitArray[0]['ngs_panel']))
{
	echo 'ngs panel: '.$visitArray[0]['ngs_panel'];
}
?>
	

Thank you								
<?= $_SESSION['user']['first_name'];?> <?= $_SESSION['user']['last_name'];?>
							</textarea>
						</div>

						<div class="row">
							<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
								<button type="submit" name="<?= $page;?>_submit" value="Submit"  class="btn btn-primary btn-primary-hover">
									<span class="fas fa-envelope"></span>
									Send Confirmation Request
								</button>
							</div>
						</div>
					</form>	
				
				</div>
			</fieldset>

		</div>
	</div>
</div>


