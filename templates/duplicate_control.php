<div class="container-fluid">
	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" >
			<form class="form" method="post" class="form-horizontal">
				<fieldset>
					<legend>
						<?= $page_title;?>
					</legend>
					<div class="row alert alert-info">
						This page will make a duplicate of the <?= $curr_control[0]['control_name'];?> control but will allow you to change the fields below.  All of the expected variants for <?= $curr_control[0]['control_name'];?> will also be linked to this new control.  It is required that the control name is unique.
					</div>
					<div class="form-group row">
						<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
							<label for="control_name" class="form-control-label">Control Name: <span class="required-field">*</span></label>
						</div>
						<div class="col-xs-12 col-sm-7 col-md-8 col-lg-8">
							<input type="text" class="form-control" id="control_name" name="control_name" maxlength="40" required="required"/>
						</div>
					</div>
					<div class="form-group row">	
						<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
							<label for="type" class="form-control-label">Control Type: <span class="required-field">*</span></label>
						</div>
						<div class="col-xs-12 col-sm-7 col-md-4 col-lg-3">
							<div class="radio">
								<label class="radio-inline"><input type="radio" name="type" required="required" value="positive" <?php
								if(isset($curr_control[0]['type']) && $curr_control[0]['type'] == 'positive')
								{
									echo 'checked'; 								
								}
								?>>Positive</label>
							
								<label class="radio-inline"><input type="radio" name="type" value="negative" <?php
								if(isset($curr_control[0]['negative']) && $curr_control[0]['type'] == 'negative')
								{
									echo 'checked'; 
								}
								?>>Negative</label>
							</div>
						</div>
					</div>
					<div class="form-group row">
						<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
							<label for="catalog_number" class="form-control-label">Catalog Number (use control name if a unique catalog number is not available): <span class="required-field">*</span></label>
						</div>
						<div class="col-xs-12 col-sm-7 col-md-8 col-lg-8">
							<input type="text" class="form-control" id="catalog_number" name="catalog_number" maxlength="40" required="required"/>
						</div>
					</div>

					<div class="form-group row">	
						<div class="col-xs-12 col-sm-4 col-md-4">
							<label for="dummy_sex" class="form-control-label">Dummy Sex:</label>
						</div>
						<div class="col-xs-12 col-sm-7 col-md-4 col-lg-3">
							<div class="radio">
								<label class="radio-inline"><input type="radio" name="dummy_sex" value="M" <?php
								if(isset($curr_control[0]['dummy_sex']) && $curr_control[0]['dummy_sex'] == 'M')
								{
									echo 'checked'; 								
								}
								?>>Male</label>
							
								<label class="radio-inline"><input type="radio" name="dummy_sex" value="F" <?php
								if(isset($curr_control[0]['dummy_sex']) && $curr_control[0]['dummy_sex'] == 'F')
								{
									echo 'checked'; 
								}
								?>>Female</label>
							</div>
						</div>
						<div class="col-x-12 col-sm-1 col-md-2 col-lg-2">
							<button type="button" class="uncheck-radio-btn btn btn-primary btn-primary-hover" data-name_radio_input="dummy_sex">
								Uncheck
							</button>
						</div>
					</div>
					<div class="form-group row">
						<div class="col-xs-12 col-sm-4 col-md-4">
							<label for="description" class="form-control-label">Description:</label>
						</div>

						<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
							<textarea class="form-control" name="description" rows="4" maxlength="65535" style="margin-bottom: 10px;"><?= $curr_control[0]['description'];?></textarea>
						</div>
					</div>
					<div class="form-group row">
						<div class="offset-xs-4 offset-sm-4 offset-md-4 offset-lg-4 col-xs-2 col-sm-2 col-md-2 col-lg-2">
							<button type="submit" id="<?= $page;?>_submit" name="<?= $page;?>_submit" value="Submit"  class="btn btn-primary btn-primary-hover">
								Submit
							</button>
						</div>
					</div>
				</fieldset>
			</form>
		</div>
	</div>
</div>
