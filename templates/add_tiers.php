
<div class="container-fluid">
	<?php
		require_once('templates/lists/run_info_list.php');
	?>

	<div class="row">
		<?php
			require_once('templates/shared_layouts/report_progress_bar.php');
		?>
		<div id="toggle-report-progress-bar" class="col-xs-10 col-sm-10 col-md-10 col-lg-10" style="height:500px;">
			<form id="add_interpts_variants_form" class="form" method="post" class="form-horizontal">
				<fieldset>
					<legend id='new_legend' class='show'>
						Add Tier for Each Included Variant
					</legend>
					<?php
					if (isset($patientArray)  && isset($visitArray))
					{
						require_once('templates/tables/patient_visit_report_table.php');
						require_once('templates/shared_layouts/panel_info_report.php');	

						// SAMPLE INFORMATION 
						require_once('templates/shared_layouts/sample_information.php');
					}					

					// MUTANTS
					require_once('templates/tables/mutant_tables.php');
					
					// Low Coverage 					
					require_once('templates/tables/low_cov_tables.php');
					?>	
					<span class="report-section-bold">
						INTERPRETATION: 
					</span>
			<?php 

			if (isset($reportGeneOrder) && !empty($reportGeneOrder))
			{
				// add gene and text area for each gene which has a mutation
				foreach ($reportGeneOrder as $gene)
				{
					// Add gene label
			?>
					<div class="form-group">		
			<?php


					// add Gene interpt if one exists
					if (isset($run_xref_interpts[$gene][0]['interpt']) &&  $run_xref_interpts[$gene][0]['interpt'] !== null)
					{
			?>
						<div style="margin-bottom:10px;"><?=  $run_xref_interpts[$gene][0]['interpt'];?></div>

			<?php
					}

					// Add Each Mutant found and a textarea box.
					if (isset($reportVariantOrder) && !empty($reportVariantOrder))
					{
						foreach ($reportVariantOrder[$gene] as $key => $variant)	
						{
							$variant_name = $variant['Gene'].' '.$variant['DNA'].' ('.$variant['Protein'].')';

					if (isset($variant['variant_interpt']) && $variant['variant_interpt'] != '')
					{
			?>	
						<div style="margin-bottom:10px;">
							<?= $utils->AddPMIDLinkUrls($variant['variant_interpt']);?>
						</div>
			<?php
					}
			?>
						<div class="row" style="margin-bottom:15px;">
							<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" <?= $utils->oddEvenBackground($key);?>>
								<?= $utils->oddEvenSectionMark($key);?>
								<div class="row">
									<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
										<h3 class="raised-card-subheader">
											Add a Tier for: <?= $variant['Gene'];?> <?= $variant['DNA'];?> (<?= $variant['Protein'];?>) <a href="?page=knowledge_base&genes=<?= $variant['Gene'];?>&coding=<?= $variant['DNA'];?>&amino_acid_change=<?= $variant['Protein'];?>" type="button" target="_blank" title="Show all <?= SITE_TITLE; ?> info on this variant">
												<span class="fas fa-info-circle"></span>
											</a>
										</h3>
									</div>
							        
									<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">

										<label for="observed_variant_id_<?= $variant['observed_variant_id'];?>_ckbx" class="form-control-label">
										  Select a Tier
										</label>
					                    <div class="radio">
						                 <?php
						                 	if (isset($tiers))
						                 	{
						                 		
						                 		foreach ($tiers as $key => $val)
						                 		{            			
						                 ?>
											<label class="radio">
						                      		<input type="radio" 
						                      		class="add_tier_to_variant" data-tier="<?= $val['tier'];?>" 
						                      		data-observed-variant-id="<?= $variant['observed_variant_id'];?>" 
						                      		data-user-id="<?= USER_ID;?>"
						                      		name="observed_variant_id_<?= $variant['observed_variant_id'];?>_ckbx" 
						                      		data-tier-id="<?= $val['tier_id'];?>"
						                      		data-tissue-id="<?= $visitArray[0]['tissue_id'];?>"
						                      		data-knowledge-id="<?= $variant['knowledge_id'];?>"
						                      		data-run-id="<?= $_GET['run_id'];?>"
						                      		data-patient-id="<?= $_GET['patient_id'];?>"
						                      		data-visit-id="<?= $_GET['visit_id'];?>"
						                      		value="<?= $val['tier'];?>" <?php 
						                      		if($variant['tier'] === $val['tier']) 
						                      			echo 'checked'; ?>> <?= $val['tier']; 
						                      		?> (<?= $val['tier_summary']; ?>) 
						                      		</input>
						                 		</label>
						                 <?php
						                 		}
						                 	}                        	
						                 ?>

											<label class="radio">
												<input type="radio" class="add_tier_to_variant" data-tier="null" data-observed-variant-id="<?= $variant['observed_variant_id'];?>" name="observed_variant_id_<?= $variant['observed_variant_id'];?>_ckbx" value="null" <?php if($variant['tier'] === null || $variant['tier'] === '') echo 'checked'; ?>> Unknown </input>
											</label>	    
						                     
						                </div>
						            </div>
						            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
						           <?php 
									$past_tiers = $db->listAll('tier-knowledge-id-overview', $variant['knowledge_id']); 
									if (isset($past_tiers) && !empty($past_tiers))
									{
								?>
										<table class="formated_table">
											<thead>
												<th colspan="2">Tally of Past Reporting</th>
											</thead>
											<tbody>
								<?php
										foreach ($past_tiers as $key => $tier)
										{
								?>
											<tr>
												<td><?= $tier['tier'];?></td>
												<td><?= $tier['count'];?></td>
											</tr>						
								<?php
										} 

										// Remove $past_tiers
										unset($past_tiers);
									}


						           ?>
						           			</tbody>
						           		</table>
						           	</div>
								</div>
							</div>	
						</div>		
			<?php
							
						}
					}
				}
			}
			?>					
					</div>
					<div class="row">
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
							<!-- add the common hidden fields for every form -->
							<?php
								require_once('templates/shared_layouts/form_hidden_fields.php')
							?>
						</div>
					</div>
					<!-- add submit and nav buttons -->
					<?php
						require_once('templates/shared_layouts/form_submit_nav_buttons.php')
					?>
					<!-- add the knowledge base for current view of variants for this view.  If  variant_filter=passed only show variants containing these -->
					<div class="row" style="margin-bottom:100px;">
					<?php
						require_once('templates/knowledge_base.php');
					?>
				</fieldset>
			</form>
		</div>
	</div>
</div>

