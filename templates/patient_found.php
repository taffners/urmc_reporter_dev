<?php
	require_once('templates/lists/run_info_list.php');
?>



<div class="container-fluid">
	<div class="row">
		<?php
			require_once('templates/shared_layouts/report_progress_bar.php');
		?>

		<!-- update area -->
		<div id="toggle-report-progress-bar" class="col-xs-10 col-sm-10 col-md-10 col-lg-10">
			<form id="add_QC" class="form" method="post" class="form-horizontal">
				<fieldset>
					<legend id='new_legend' class='show'>
						Patient Already Added
					</legend>

					<!-- add all patient info -->
					<?php
						require_once('templates/lists/patient_info.php');
					?>

					<div class="row">
						<h2 class="raised-card-header">Past Run Info</h2>
					<!-- add past run info -->
					<?php 
						foreach($patientVisits as $pastVisit)
						{
					?>
						<ul class="raised-card">
							<?php
							foreach($pastVisit as $key => $value)
							{
								if(!$utils->InputAID($key))
								{
							?>

								<li><b><?= $utils->UnderscoreCaseToHumanReadable($key);?></b>: <?= $value;?></li>
							<?php
								}
							}
							?>
						</ul>					
					<?php
						}
					?>

						<input type="hidden" id="form_checked" name="form_checked" value="0"/>

						<input type="hidden" id="visit_id" name="visit_id" value="<?php 
								if(isset($visitArray[0]['visit_id']))
								{
									echo $visitArray[0]['visit_id'];
								}
							?>" required/>

						<input type="hidden" id="user_id" name="user_id" value="<?= USER_ID; ?>"/>

						<input type="hidden" id="run_id" name="run_id" value="<?= RUN_ID; ?>"/>

	         				<div class="row">
	         					<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
		               			<button type="button" id="back_button" class="btn btn-primary ">
	               					❮ Back
	               				</button>
	         					</div>

							<button type="submit" id="<?= $page;?>_submit" name="<?= $page;?>_submit" value="Submit"  class="btn btn-primary btn-primary-hover">
								This is the Patient
							</button>
    					<?php
    						if (isset($completed_steps))
    						{
    							$step_status = $stepTracker->StepStatus($completed_steps, 'patient_info');

    							if ($step_status ==='completed')
         						{
         							// find next step
         							$next_step = $stepTracker->FindNextStep('patient_info');

    						
    					?>
					          <div class="offset-xs-4 offset-sm-4 offset-md-4 offset-lg-4 col-xs-2 col-sm-2 col-md-2 col-lg-2">
		               			<a href="?page=<?= $next_step;?>&run_id=<?= RUN_ID;?>" class="btn 
								btn-primary btn-primary-hover" role="button">
									Skip ❯<!-- Next ❯  ❮ Previous-->
								</a>
		         				</div>

		         				<?php
		         						}
		         					}
		         				?>
		         			</div>
	         			</div>
				</fieldset>
			</form>
		</div>
	</div>
</div