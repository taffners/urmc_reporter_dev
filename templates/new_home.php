<div class="container-fluid mt-0" id="home-page-id">

<?php
	if (isset($maintenance_pending) && !empty($maintenance_pending))
	{
?>
	<div class="row d-print-none alert alert-warning mb-0" role="alert">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			<strong>Planned Maintenance to <?= SITE_TITLE_ABBR; ?></strong> 
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				<ul>
	<?php
		foreach ($maintenance_pending as $key => $maintenance)
		{
	?>
					<li>
						<b><?= $maintenance['maintenance_start'];?> - <?= $maintenance['maintenance_end'];?></b> <?= $maintenance['bug_description'];?> <b><?= $maintenance['user_name'];?></b> 
					</li>	
	<?php
		}
	?>			
				</ul>
			</div>	
		</div>
	</div>
<?php
	}

?>
<?php

	if (isset($recent_version_history_update) && !empty($recent_version_history_update))
	{
?>
	<div class="row d-print-none alert alert-info mb-0" id="version-update-alert" role="alert">

		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			<strong>Version updates in the last week to <?= SITE_TITLE_ABBR; ?></strong> (for more info refer to <img src="images/qc_qa.png" style="margin-left:5px;height:35px;width:35px;" alt="A green check mark indicating qc/qa">)
		</div>
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			<ul>
<?php
	foreach ($recent_version_history_update as $key => $version)
	{
?>
				<li>
					<b><?= $version['version'];?></b> <?= $version['version_comment'];?> <b><?= $version['user_name'];?></b> <i><?= $version['time_stamp'];?></i>
				</li>	
<?php
	}
?>			
				<!-- <li><b><?=$last_2_months_version_update_count;?></b> version updates in the last 2 months, <b><?=$last_month_version_update_count;?></b> version updates in the last month, and <b><?=$last_7_days_version_update_count;?></b> version updates in the last 7 days</li> -->
			</ul>
		</div>
	</div>
<?php
	}
?>
<div class="container-fluid">
	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
			
			<?php
				if (isset($user_permssions) && strpos($user_permssions, 'log_book') !== false)
				{
			?>
			<div class="row d-print-none mb-0">
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
					<hr class="plain-section-mark">
				</div>
			</div>
			<div class="row d-print-none">
				<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
					<h3>Sample Log Book</h3>
				</div>
				<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
					<a href="?page=sample_log_book&filter=in_progress"  class="btn btn-yellow-green-gradient btn-yellow-green-gradient-hover float-right ml-2">In Progress Tests</a>
					<a href="?page=sample_log_book&filter=pending" id="pending-tests-btn" class="btn btn-yellow btn-warning-hover float-right ml-2">Pending Tests</a>
					
					<a href="?page=in_progress_work_book&nav_status=off" id="samples-wo-tests-btn" class="btn btn-primary btn-primary-hover" target="_blank">In Progress workbook</a>
				
				</div>
			</div>
			<div class="row d-print-none mb-0">
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
					<form id="search-all-samples" class="form" method="post" class="form-horizontal">
						<div class="form-group row mb-0">
							
							<div id="search-all-samples-div" class="col-xs-10 col-sm-10 col-md-11 col-lg-11" >
								<div class="form-group row">
									<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6" style="padding-right:2px;">
										<input id="search-all-samples-input" type="text" class="form-control" placeholder="Search All Samples ..." name="search"/>
										<label class="d-none" for="search-all-samples-input">Search all samples</label>
									</div>
									<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3" style="padding-right:2px;padding-left:0px;">
										<input id="search-all-samples-input-start-date" type="date" class="form-control" placeholder="Start Date" name="search-by-start-date"/>
										<label class="d-none" for="search-all-samples-input-start-date">Search all samples start date</label>
									</div>
									<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3" style="padding-right:0px;padding-left:0px;">
										<input id="search-all-samples-input-end-date" type="date" class="form-control" placeholder="End Date" name="search-by-end-date"/>
										<label class="d-none" for="search-all-samples-input-end-date">Search all samples end date</label>
									</div>
								</div>
							</div>
							<div class="col-xs-2 col-sm-2 col-md-1 col-lg-1"style="padding-right:0px;padding-left:0px;">
								<button type="submit" name="search-all-samples-submit" class="btn btn-primary btn-primary-hover" value="" aria-label="search all samples">
									<span class="fas fa-search" style="margin-top:2px;margin-bottom:2px;"></span>
								</button>
							</div>
						</div>
					</form>
				</div>
			</div>
			<?php
				}
			?>	

			<div class="row d-print-none mb-0">
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
					<hr class="plain-section-mark">
				</div>
			</div>
			<div class="row d-print-none">
				<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
					<h3>Reports</h3>
				</div>
				<div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
					<?php
                        if (isset($user_permssions) && strpos($user_permssions, 'revise') !== false)
                        {
                   ?>
                   <a href="?page=completed_reports" class="btn btn-primary btn-primary-hover float-right ml-2">View or Revise All Completed Reports</a>
                   <?php
                        }
                        else
                        {
                   ?>
                   <a href="?page=completed_reports" class="btn btn-primary btn-primary-hover float-right ml-2">View All Completed Reports</a>
                   <?php
                        }
                   ?>
					<a href="?page=transfered_to_patient_chart&scan_status=pending"  class="btn btn-primary btn-primary-hover float-right ml-2">Scanned Transfer Status</a>
					
					<a href="?page=confirmation_list&status=pending" class="btn btn-primary btn-primary-hover float-right ml-2">Confirmations</a>
				
				</div>
			</div>
			
			<?php
				if (isset($user_permssions) && strpos($user_permssions, 'NGS') !== false && isset($ngs_panels))
				{
			?>

			<div class="row d-print-none">
				<?php require('templates/shared_layouts/ngs_page_links.php');?>
			</div>
			<?php
				}
				else
				{
			?>
			<div class="row d-print-none">
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
					<hr class="plain-section-mark">
				</div>
			</div>
			<?php
				}
			?>
			
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-6" id="all-pending-counts-pie-chart">

				</div>
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-6" id="all-in-progress-counts-pie-chart">

				</div>
			</div>
		</div>
		
		<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 ">					
			<?php
				if (isset($user_permssions) && strpos($user_permssions, 'log_book') !== false &&  strpos($user_permssions, 'View_Only') === false)
				{
			?>		
			<div class="row d-print-none">
				<div class="col-xs-12 col-sm-12 col-md-6 col-lg-12">
					<hr class="plain-section-mark">
					<label class="form-control-label">Add: </label>
					<a href="?page=add_sample_log_book" id="add-sample-to-log-book-btn" class="btn btn-primary btn-primary-hover" title="Add a Sample to the log book"> <span class="fas fa-plus"></span> <span class="fas fa-vial"></span></a>
					<a href="?page=add_single_analyte_pool" class="btn btn-primary btn-primary-hover">Add Single Analyte Pool</a>

				<?php
					require('templates/shared_layouts/ngs_add_links.php');
				?>

					<hr class="plain-section-mark">
				</div>
			</div>
			<?php
				}
			?>
			
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
					<h3>
						Pending Single Analyte Pools (SAP)						
					</h3>
				</div>

			<?php
			if (isset($_GET['single_analyte_pool_view']))
			{
				if ($_GET['single_analyte_pool_view'] === 'my_pools')
				{
					$my_pools_class = 'btn btn-success btn-success-hover';
					$all_pools_class = 'btn btn-primary btn-primary-hover';
					$reportable_pools_class = 'btn btn-primary btn-primary-hover';
				}
				else if ($_GET['single_analyte_pool_view'] === 'all_pools')
				{
					$my_pools_class = 'btn btn-primary btn-primary-hover';
					$all_pools_class = 'btn btn-success btn-success-hover';
					$reportable_pools_class = 'btn btn-primary btn-primary-hover';
				}
				else if ($_GET['single_analyte_pool_view'] === 'reportable_pools')
				{
					$my_pools_class = 'btn btn-primary btn-primary-hover';
					$all_pools_class = 'btn btn-primary btn-primary-hover';
					$reportable_pools_class = 'btn btn-success btn-success-hover';
				}
			}

			?>

				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-6 float-right">		
					
					<a href="?page=new_home&single_analyte_pool_view=my_pools" class="<?= $my_pools_class;?>">My Pools</a>

					<a href="?page=new_home&single_analyte_pool_view=all_pools" class="<?= $all_pools_class;?>">All Pools</a>

					<a href="?page=new_home&single_analyte_pool_view=reportable_pools" class="<?= $reportable_pools_class;?>">Reportable Pools</a>
				<?php
				if (isset($user_permssions) && strpos($user_permssions, 'log_book') !== false &&  strpos($user_permssions, 'View_Only') === false)
				{
				?>
					<a href="?page=search_single_analyte_pools" class="btn btn-primary btn-primary-hover float-right" title="Search all historic pools"> Search SAPs</a>
				<?php
				}
				?>
				</div>
			</div>

			<hr class="plain-section-mark">
			<?php 
				require_once('templates/shared_layouts/single_analyte_pool_table.php');
			?>
			</div>
		</div>
	</div>
</div>