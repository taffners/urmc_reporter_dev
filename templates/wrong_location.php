<div class="container-fluid">
	<?php
		if (!empty($ip_loc))
		{
	?>
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
					<img src="images/location_cartoon.png" alt="location cartoon">
					<br>
					<strong>Maybe a good place for your bar would be <?= $ip_loc['city'];?> <?= $ip_loc['state'];?></strong>
				</div>
			</div>

	<?php
		}
		else
		{
	?>
			<div class="row">
				Something went wrong with your location info
			</div>
	<?php
		}
	?>
</div>