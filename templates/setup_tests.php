<div class="container-fluid">
<?php 

	if (isset($all_tests_available))
	{		
?>

	<?php
        if (isset($user_permssions) && (strpos($user_permssions, 'admin') !== false || strpos($user_permssions, 'manager') !== false))
        {                        
   	?>
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
				<a href="?page=add_new_orderable_test" class="btn btn-primary btn-primary-hover" title="Use this button to add a new Orderable Single Analyte Test">Add a New Orderable Test</a>
			</div>
			<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
				<a href="?page=add_a_reflex_test_tracker" class="btn btn-primary btn-primary-hover" title="Use this button to add a new Orderable Reflex Test Tracker.  Reflex Test Trackers are not tests but are a way to track when reflexs need to be added to a test.">Add a Reflex Test Tracker</a>
			</div>
			<?php
	        if (isset($user_permssions) && strpos($user_permssions, 'admin') !== false)
	        {                        
		   	?>
		   		<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
				<a href="?page=add_reporting_flow_step" class="btn btn-primary btn-primary-hover" title="Only admins have access to this page.  Use this button to add possible <?= SITE_TITLE_ABBR;?> flow reporting steps">Add an <?= SITE_TITLE_ABBR;?> reporting flow step</a>
			</div>
		   	<?php
		   	}
		   	?>
		</div>
	<?php
		}
	?>

		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				<table class="formated_table sort_table" >
					<thead>
						<th>Test#</th>
					<?php
			        if (isset($user_permssions) && (strpos($user_permssions, 'admin') !== false || strpos($user_permssions, 'manager') !== false))
			        {                        
				   	?>
						<th>Update Test</th>
					<?php
					}
					?>						
						<th>Test Name</th>
						<th>Full Name</th>
						<th>Assay Type</th>
						<th>Send out tests</th>
						<th>Consent Required</th>						
						<th>Previous History Required</th>
						<th>Mol Number Required</th>
						<th>Turnaround Time</th>
						<th>Actual Average Turnaround Time</th>
						<th>Actual Max Turnaround Time</th>
						<th>Total Number of Tests Completed</th>
						<th>Include in turn around Calculation</th>
						<th>Clock starts after another test finishes</th>
						<th>Test Status</th>
						<th>Max # samples per pool</th>
						<th>User Name</th>
						<th>Form Control extraction info</th>
						<th>Form Control quantification info</th>
						<th>Form Control thermal cyclers instruments</th>
						<th>Form Control sanger sequencing instruments</th>
						<th>Form Control wet bench techs</th>
						<th>Form Control analysis techs</th>
						<th>Form Control test result</th>
						<th>Infotrack Report</th>

						<th>hotspot_variants</th>
						<th>Reportable genes for Non Hotspot Tests</th>
						<th>hotspot type</th>
						<th>Test instructions provided</th>
						<th>Test Name</th>
                        
					
					</thead>
					<tbody>
	<?php

		foreach ($all_tests_available as $key => $test)
		{	
			
	?>
						<tr>
							<td><?= $test['orderable_tests_id'];?></td>
						<?php
				        if (isset($user_permssions) && (strpos($user_permssions, 'admin') !== false || strpos($user_permssions, 'manager') !== false))
				        {                        
					   	?>
							<td>								
								<a href="?page=add_new_orderable_test&orderable_tests_id=<?= $test['orderable_tests_id'];?>" class="btn btn-primary btn-primary-hover">Update Orderable Test</a>			
							</td>
						<?php
						}
						?>
							<td><?= $test['test_name'];?></td>
							<td><?= $test['full_test_name'];?></td>
							<td><?= $test['assay_name'];?></td>
							<td><?= $test['send_out_test'];?></td>
							<?= $utils->colorTdBKGRDGreen($test['consent_required']);?>
							<?= $utils->colorTdBKGRDGreen($test['previous_positive_required']);?>
							
							<?= $utils->colorTdBKGRDGreen($test['mol_num_required']);?>
							
							<td>
							<?php 
								$expected_turnaround_time = $db->listAll('turn-around-time',$test['orderable_tests_id']);
								foreach ($expected_turnaround_time as $key => $time_per_result)
								{
									echo 'Result: '.$time_per_result['dependent_result'].' Allowed days: '.$time_per_result['turnaround_time'].'<br>';
								}
							?>								
							</td>

							<td><?= $test['avg_turnaround_time'];?></td>
							<td><?= $test['max_turnaround_time'];?></td>
							<td><?= $test['num_tests'];?></td>
							
							<?= $utils->colorTdBKGRDGreen($test['include_in_turn_around_calculation']);?>
							<td <?= $test['turnaround_time_depends_on_ordered_test'] == 'yes'? 'class="alert-passed-qc"':''?>>
								<?= $test['dependent_test'] != null? $test['dependent_test']:$test['turnaround_time_depends_on_ordered_test']; ?>
							</td>
							<td <?= $test['test_status'] == 'Active'? 'class="alert-passed-qc"':''?>><?= $test['test_status'];?></td>

							<td><?= $test['max_num_samples_per_run'];?></td>
							<td><?= $test['user_name'];?></td>
							<?= $utils->colorTdBKGRDGreen($test['extraction_info']);?>
							<?= $utils->colorTdBKGRDGreen($test['quantification_info']);?>
							<?= $utils->colorTdBKGRDGreen($test['thermal_cyclers_instruments']);?>
							<?= $utils->colorTdBKGRDGreen($test['sanger_sequencing_instruments']);?>
							<?= $utils->colorTdBKGRDGreen($test['wet_bench_techs']);?>
							<?= $utils->colorTdBKGRDGreen($test['analysis_techs']);?>
							<?= $utils->colorTdBKGRDGreen($test['test_result']);?>
							

							<?php
					        if (isset($user_permssions) && (strpos($user_permssions, 'admin') !== false || strpos($user_permssions, 'manager') !== false))
					        {                        
						   	?>
						   	<td <?= $test['infotrack_report'] == 'yes'? 'class="alert-passed-qc"':''?>>
								<?= $test['infotrack_report'];?>
								<?php
									if ($test['infotrack_report'] == 'yes')
									{
								?>
										<a href="?page=infotrack_test_report_steps&orderable_tests_id=<?= $test['orderable_tests_id'];?>" class="btn btn-primary btn-primary-hover">Infotrack report building steps</a>
								<?php
									}

								?>	
							</td>
						   	<?php
						   	}
						   	else
						   	{	
						   	?>
						   	<?= $utils->colorTdBKGRDGreen($test['infotrack_report']);?>
						   	<?php
						   	}
						   	?>
							
							<td <?= $test['hotspot_variants'] == 'yes'? 'class="alert-passed-qc"':''?>>
								<?= $test['hotspot_variants'];?>
								<?php
									if ($test['hotspot_variants'] == 'yes')
									{
								?>
										<a href="?page=orderable_test_hotspots&orderable_tests_id=<?= $test['orderable_tests_id'];?>" class="btn btn-primary btn-primary-hover">Hotspots</a>
								<?php
									}

								?>	
							</td>
							<td <?= $test['hotspot_variants'] !== 'yes' && $test['infotrack_report'] == 'yes'? 'class="alert-passed-qc"':'';?>>
								
								<?php
							        if (isset($user_permssions) && strpos($user_permssions, 'edit_covered_genes') !== false && $test['hotspot_variants'] !== 'yes' && $test['infotrack_report'] == 'yes')
							        {                        
							   	?>

							   			<a href="?page=report_covered_genes&orderable_tests_id=<?= $test['orderable_tests_id'];?>&ngs_panel_id=<?= $test['ngs_panel_id'];?>" class="btn btn-primary btn-primary-hover">Covered Genes</a>
								
								<?php
									}
									else if ($test['hotspot_variants'] !== 'yes' && $test['infotrack_report'] == 'yes')
									{
								?>
										To edit covered genes you will need edit covered genes permission.  Ask an Admin or manager for your permissions to be updated.
								<?php
									}
									else
									{
										echo 'no';
									}

								?>	
							</td>
							<td><?= $test['hotspot_type'];?></td>

							<td>
							<?php
							if (isset($test['notes_for_work_sheet']) && !empty($test['notes_for_work_sheet']))
							{
								echo 'yes';
							}
							else
							{
								echo 'no';
							}
							?>

							</td>
							<td><?= $test['test_name'];?></td>
						</tr>
	<?php
		}
	?>
					</tbody>
				</table>
			</div>
		</div>
<?php
	}
?>
</div>