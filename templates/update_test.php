<div class="container-fluid">

	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

			<form class="form" method="post" class="form-horizontal">
				<fieldset>
					<legend class='show'>
						Change Status of <?= isset($testInfo[0]['test_name']) ? $testInfo[0]['test_name'] : 'Test';?> To Complete
					</legend>
<?php
					////////////////////////////////////////////////////////////////
					// Header
					////////////////////////////////////////////////////////////////
					require_once('templates/shared_layouts/sample_overview.php');
?>
					
<?php
					////////////////////////////////////////////////////////////////
					// Fields for DNA Extraction and Nanodrop
					////////////////////////////////////////////////////////////////
					require_once('templates/shared_layouts/extraction_update_test.php');
					?>
<?php
// Add thermal cyclers is not made in the logic for DNA extraction or Nanodrop tests
if (isset($thermal_cyclers) && !empty($thermal_cyclers))
{
?>
					<hr class="section-mark">
					<div class="form-group row">
						<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
							<label for="thermal_cyclers" class="form-control-label">Thermal Cyclers Used: <span class="required-field">*</span><span class="mulitple-field">*</span></label>
						</div>
						<div class="col-xs-12 col-sm-8 col-md-8 pull-left ">
				<?php
						foreach ($thermal_cyclers as $key => $instrument)
						{
				?>
							<div class="form-check">
								<input class="form-check-input" type="checkbox" value="<?= $instrument['instrument_id'];?>" name="instruments[thermal_cyclers][]"
							<?php
								if (isset($instrument['sap_status']) && $instrument['sap_status'] === 'added')
								{
									echo 'checked="checked"';
								}
							?>
								>
								<label><?= $utils->instrumentInputName($instrument);?></label>
							</div>
				<?php
						}								
				?>			
							<div class="form-check">
								
								<input class="form-check-input" type="checkbox" value="NA" name="instruments[thermal_cyclers][]">
								<label>NA</label>
							</div>
						</div>
					</div>
<?php
}

// Add Sanger Sequencer for assay_name Sanger Sequencing
if (isset($sanger_sequencers))
{
?>	
					<hr class="section-mark">
					<div class="form-group row">
						<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
							<label for="sanger_sequencers" class="form-control-label">Sanger Sequencer Used: <span class="required-field">*</span></label>
						</div>
						<div class="col-xs-12 col-sm-8 col-md-8 pull-left">							
				<?php
						foreach ($sanger_sequencers as $key => $instrument)
						{
				?>
							<label class="radio">
								<input type="radio" name="instruments[sanger_sequencers][]" value="<?= $instrument['instrument_id'];?>" required="required"
								<?php
								if (isset($instrument['sap_status']) && $instrument['sap_status'] === 'added')
								{
									echo 'checked';
								}
								?>
								>
								
								<?= $utils->instrumentInputName($instrument);?>
							</label>
				<?php
						}
				?>
							<!-- sometimes the test is only sequenced if negative-->
							<label class="radio">
								<input type="radio" name="instruments[sanger_sequencers][]" value="NA">
								NA
							</label>
						</div>
					</div>
<?php	
}

// Add idylla instruments
if (isset($testInfo[0]['idylla_instruments']) && $testInfo[0]['idylla_instruments'] === 'yes')
{
	$idylla_instruments = $db->listAll('instrument-groups-description', 'Idylla instrument');
?>
					<hr class="section-mark">
					<div class="form-group row">
						<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
							<label for="extra_tracked_fields" class="form-control-label">Sample Loaded: <span class="required-field">*</span></label>
						</div>
						<div class="col-xs-12 col-sm-8 col-md-2 col-lg-2">							
				
							<label class="radio">
								<input type="radio" name="extra_tracked_fields[Sample_Loaded]" value="DNA" required="required">								
								DNA
							</label>
							<label class="radio">
								<input type="radio" name="extra_tracked_fields[Sample_Loaded]" value="FFPE" >	
								FFPE
							</label>
						</div>
					
						<div class="col-xs-12 col-sm-12 col-md-1 col-lg-1">
							<label for="extra_tracked_fields" class="form-control-label">If DNA, Volume (&micro;l):</label>
						</div>
						<div class="col-xs-12 col-sm-8 col-md-2 col-lg-2">							
				
							<div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
								<input type="number" name="extra_tracked_fields[DNA_vol]" step="0.01"/>
							</div>

						</div>
						<div class="col-xs-12 col-sm-12 col-md-1 col-lg-1">
							<label for="extra_tracked_fields" class="form-control-label">Conc. (ng/&micro;l):</label>
						</div>
						<div class="col-xs-12 col-sm-8 col-md-2 col-lg-2">							
				
							<div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
								<input type="number" name="extra_tracked_fields[DNA_conc]" step="0.01"/>
							</div>

						</div>
					</div>
					<div class="form-group row">
						
					
						<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
							<label for="extra_tracked_fields" class="form-control-label">Cartridge ID: </label>
						</div>
						<div class="col-xs-12 col-sm-8 col-md-2 col-lg-2">							
				
							<div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
								<input type="text" name="extra_tracked_fields[Catridge_ID]" maxlength="30"/>
							</div>

						</div>
						<div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
							<label for="extra_tracked_fields" class="form-control-label">Lot ID): </label>
						</div>
						<div class="col-xs-12 col-sm-8 col-md-2 col-lg-2">							
				
							<div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
								<input type="text" name="extra_tracked_fields[Lot_ID]" maxlength="30"/>
							</div>

						</div>
					</div>
					<div class="form-group row">
						<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
							<label for="idylla_instruments" class="form-control-label">Idylla Instrument Used: <span class="required-field">*</span></label>
						</div>
						<div class="col-xs-12 col-sm-8 col-md-8 pull-left">							
				<?php
						foreach ($idylla_instruments as $key => $instrument)
						{
				?>
							<label class="radio">
								<input type="radio" name="instruments[idylla_instruments][]" value="<?= $instrument['instrument_id'];?>" required="required"
								<?php
								if (isset($instrument['sap_status']) && $instrument['sap_status'] === 'added')
								{
									echo 'checked';
								}
								?>
								>
								
								<?= $utils->instrumentInputName($instrument);?>
							</label>
				<?php
						}
				?>
							<!-- sometimes the test is only sequenced if negative-->
							<label class="radio">
								<input type="radio" name="instruments[idylla_instruments][]" value="NA">
								
								NA
							</label>

						</div>
					</div>
<?php					
}

// Add Tasks
if 	(	
		isset($testInfo[0]['wet_bench_techs']) && $testInfo[0]['wet_bench_techs'] == 'yes'
	)
{
?>	

					<hr class="section-mark">
					<div class="form-group row">
						<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
							<label for="wet_bench" class="form-control-label">Wet Bench Techs: <span class="required-field">*</span><span class="mulitple-field">*</span></label>
						</div>
						<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
								
					<?php
						if (isset($users_log_book))
						{
							echo $utils->techCheckbox('task_initials[wet_bench][]', $users_log_book);
						}
					?>
								
						</div>
					</div>
<?php
}
// Add Tasks
if 	(	
		isset($testInfo[0]['analysis_techs']) && $testInfo[0]['analysis_techs'] == 'yes'
	)
{
?>	
					<hr class="section-mark">
					<div class="form-group row">
						<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
							<label for="analysis" class="form-control-label">Analysis Techs: <span class="required-field">*</span><span class="mulitple-field">*</span></label>
						</div>
						<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
					<?php
						if (isset($users_log_book))
						{
							echo $utils->techCheckbox('task_initials[analysis][]', $users_log_book);
						}
					?>							
						</div>
					</div>
<?php 
}


////////////////////////////////////////////////////////////////
// Add hotspots for non-reportable tests
////////////////////////////////////////////////////////////////
if (isset($testInfo[0]['hotspot_variants']) && $testInfo[0]['hotspot_variants'] === 'yes' && $testInfo[0]['infotrack_report'] === 'no')
{
	$idylla_variants = $db->listAll('all-hotspot-variants-no-duplicates', $orderedTestArray[0]['orderable_tests_id']);
?>

				<hr class="section-mark">
					<div class="form-group row">
						<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
							<label for="hotspots" class="form-control-label"><?= $testInfo[0]['hotspot_type'];?>: <span class="required-field">*</span></label>
						</div>
						<div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">							
				<?php
						foreach ($idylla_variants as $key => $curr_hotspots)
						{
				?>
							<div class="form-check">
								<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
									<input type="checkbox" name="hotspots[<?= intval($key);?>][test_hotspots_id]" value="<?= $curr_hotspots['test_hotspots_id'];?>" >
									<label class="checkbox">
										<?= $curr_hotspots['mutation_abbr'] != $curr_hotspots['ordered_test_genetic_call']? $curr_hotspots['mutation_abbr']:'';?> <?= $curr_hotspots['ordered_test_genetic_call'];?>
									</label>
								
								<?php
								if ($testInfo[0]['hotspot_val_type'] === 'observed & normal size')
								{
								?>
								
								
									<label for="hotspots[<?= intval($key);?>][observed]" class="form-control-label" style="width:auto;display:inline;margin-bottom:20px;">Size: </label>
								
									<input type="number" class="form-control capitalize" name="hotspots[<?= intval($key);?>][observed]" step="0.01" mininium="0" style="width:auto;display:inline;margin-bottom:20px;"/>
								
									<label for="hotspots[<?= intval($key);?>][normal]" class="form-control-label" style="width:auto;display:inline;margin-bottom:20px;">Normal: </label>
								
									<input type="number" class="form-control capitalize" name="hotspots[<?= intval($key);?>][normal]" step="0.01" mininium="0" style="width:auto;display:inline;margin-bottom:20px;"/>
								
								<?php
								}
								?>
								</div>
							</div>
				<?php
						}
				?>
							<!-- sometimes the test is only sequenced if negative-->
							<div class="form-check">
								<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
									<input type="checkbox" name="hotspots[<?= intval($key)+1;?>]" value="None">
									<label class="checkbox">									
										None
									</label>
								</div>
							</div>

						</div>
					</div>
<?php

}
////////////////////////////////////////////////////////////////
// Add hotspots for reportable tests
////////////////////////////////////////////////////////////////
else if (isset($testInfo[0]['hotspot_variants']) && $testInfo[0]['hotspot_variants'] === 'yes' && $testInfo[0]['infotrack_report'] === 'yes')
{
	$idylla_variants = $db->listAll('all-hotspot-variants', $orderedTestArray[0]['orderable_tests_id']);
?>

				<hr class="section-mark">
					<div class="form-group row">
						<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
							<label for="hotspots" class="form-control-label"><?= $testInfo[0]['hotspot_type'];?>: <span class="required-field">*</span></label>
						</div>
						<div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">							
				<?php
						foreach ($idylla_variants as $key => $curr_hotspots)
						{
				?>
							<div class="form-check">
								<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
									<input type="checkbox" name="hotspots[<?= intval($key);?>][test_hotspots_id]" value="<?= $curr_hotspots['test_hotspots_ids'];?>" >
									
									<b><?= $curr_hotspots['ordered_test_genetic_call'];?></b> | 
									<b style="color:blue;"><?= $curr_hotspots['genes'];?></b> | 
									<b style="color:green;"><?= $curr_hotspots['codings'];?></b> | 
									<b style="color:red;"><?= $curr_hotspots['amino_acid_changes'];?></b>
									
									
								<?php
								if ($testInfo[0]['hotspot_val_type'] === 'observed & normal size')
								{
								?>
								
								
									<label for="hotspots[<?= intval($key);?>][observed]" class="form-control-label" style="width:auto;display:inline;margin-bottom:20px;">Size: </label>
								
									<input type="number" class="form-control capitalize" name="hotspots[<?= intval($key);?>][observed]" step="0.01" mininium="0" style="width:auto;display:inline;margin-bottom:20px;"/>
								
									<label for="hotspots[<?= intval($key);?>][normal]" class="form-control-label" style="width:auto;display:inline;margin-bottom:20px;">Normal: </label>
								
									<input type="number" class="form-control capitalize" name="hotspots[<?= intval($key);?>][normal]" step="0.01" mininium="0" style="width:auto;display:inline;margin-bottom:20px;"/>
								
								<?php
								}
								?>
								<div class="row d-none">
									<input type="text" class="form-control capitalize" name="hotspots[<?= intval($key);?>][genetic_call]" style="width:auto;display:inline;margin-bottom:20px;" value="<?= $curr_hotspots['ordered_test_genetic_call'];?>"/>
									<input type="text" class="form-control capitalize" name="hotspots[<?= intval($key);?>][genes]" style="width:auto;display:inline;margin-bottom:20px;" value="<?= $curr_hotspots['genes'];?>"/>
									<input type="text" class="form-control capitalize" name="hotspots[<?= intval($key);?>][codings]" style="width:auto;display:inline;margin-bottom:20px;" value="<?= $curr_hotspots['codings'];?>"/>
									<input type="text" class="form-control capitalize" name="hotspots[<?= intval($key);?>][amino_acid_changes]" style="width:auto;display:inline;margin-bottom:20px;" value="<?= $curr_hotspots['amino_acid_changes'];?>"/>
								</div>

								</div>
							</div>
				<?php
						}
				?>
							<!-- sometimes the test is only sequenced if negative-->
							<div class="form-check">
								<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
									<input type="checkbox" name="hotspots[<?= intval($key)+1;?>]" value="None">
									<label class="checkbox">									
										None
									</label>
								</div>
							</div>

						</div>
					</div>
<?php

}

////////////////////////////////////////////////////////////////
// Add QC section
////////////////////////////////////////////////////////////////
if (isset($testInfo[0]['hotspot_variants']) && $testInfo[0]['hotspot_variants'] === 'yes')
{
?>
					<hr class="section-mark">
					<div class="form-group row">
						<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
							<label for="qc_staus" class="form-control-label">QC Status: <span class="required-field">*</span></label>
						</div>
						<div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">							
				
							<label class="radio">
								<input type="radio" name="qc_staus" value="pass" required="required">								
								Pass
							</label>
							<label class="radio">
								<input type="radio" name="qc_staus" value="fail" >	
								Fail - 
								<?php
								if ($testInfo[0]['infotrack_report'] === 'yes')
								{
									echo 'Submit this form. Cancel this test, add new test to infotrack, and repeat run';
								}
								else
								{
									echo 'Sign out this test, add new test to infotrack, and repeat run';
								}
								?>
							</label>
						</div>
					</div>
					<div class="form-group row">
						<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
							<label for="qc_overview" class="form-control-label">Any comments about QC
								
							</label>
							
						</div>

						<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
							<textarea class="form-control" name="qc_overview" rows="4" maxlength="65535" style="margin-bottom: 10px;"></textarea>
						</div>
					</div>	
<?php							
}

// To build information for history of tests have user add results for tests that previous_positive_required (example tests IGH, IGK, FLT3)
// Also some test have a differing result depending on result
if 	(	
		isset($testInfo[0]['test_result']) && $testInfo[0]['test_result'] === 'yes'
	)
{
?>	
					<hr class="section-mark">

	<?php
	// Add a reminder about reflex tests linked to this test.
	if  (
			isset($reflexTestArray) && !empty($reflexTestArray) && 
			isset($nextReflexes) && !empty($nextReflexes)
		)
	{
	?>
					<div id="reflex-update-section" class="form-group row">
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
							<div class="alert alert-info">
								This test is linked to the reflex tracker <b><?= $reflexTestArray[0]['reflex_name'];?></b> if the result of this test is <b><?= $reflexTestArray[0]['reflex_value'];?></b> make sure to add a <b><?= $nextReflexes[0]['test_name'];?></b> test to this patient's sample.
							</div>
							<div class="d-none">
								<input type="text" name="trigger_result" value="<?= $reflexTestArray[0]['reflex_value'];?>">
								<input type="text" name="status_linked_reflexes_xref_id" value="<?= $reflexTestArray[0]['status_linked_reflexes_xref_id'];?>">
								<input type="text" name="next_test_name" value="<?= $nextReflexes[0]['test_name'];?>">
								<input type="text" name="ordered_reflex_test_id" value="<?= $reflexTestArray[0]['ordered_reflex_test_id'];?>">	
								<input type="text" name="reflex_status" value="<?= $reflexTestArray[0]['reflex_status'];?>">								
							</div>
						</div>
					</div>

	<?php
	}

	if (isset($testInfo[0]['result_note']) && !empty($testInfo[0]['result_note']))
	{
	?>
					<div id="reflex-update-section" class="form-group row">
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
							<div class="alert alert-info">
								<?= nl2br($testInfo[0]['result_note']);?>
							</div>
						</div>
					</div>
	<?php
	}

	if (isset($testInfo[0]['result_type']) && $testInfo[0]['result_type'] === 'pos neg')
	{

	?>
					<div class="form-group row">
						<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
							<label for="test_result" class="form-control-label">Test Result: <span class="required-field">*</span></label>
						</div>
						<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
				
							<label class="radio-inline">
								<input type="radio" name="test_result" value="Positive" required>
								Positive
							</label>
							<label class="radio-inline">
								<input type="radio" name="test_result" value="Weak Positive" required>
								Weak Positive
							</label>
							<label class="radio-inline">
								<input type="radio" name="test_result" value="Negative" required>
								Negative
							</label>
							<label class="radio-inline">
								<input type="radio" name="test_result" value="Weak Negative" required>
								Weak Negative
							</label>
							<label class="radio-inline">
								<input type="radio" name="test_result" value="No Signal" required>
								No Signal
							</label>
							<label class="radio-inline">
								<input type="radio" name="test_result" value="See Interpt." required>
								See Interpt.
							</label>
						</div>
					</div>
<?php
	}
	else if (isset($testInfo[0]['result_type']) && $testInfo[0]['result_type'] === 'normal,low,high,QC')
	{
?>
					<div class="form-group row">
						<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
							<label for="test_result" class="form-control-label">Test Result: <span class="required-field">*</span></label>
						</div>
						<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
							<label class="radio-inline">
								<input type="radio" name="test_result" value="Normal" required>
								Normal
							</label>

							<label class="radio-inline">
								<input type="radio" name="test_result" value="MSS/MSI-L report is 0/5 or 1/5 markers shifted" required>
								MSS/MSI-L report is 0/5 or 1/5 markers shifted
							</label>
							
							<label class="radio-inline">
								<input type="radio" name="test_result" value="MSI-H report is 2/5 - 5/5 markers shifted" required>
								MSI-H report is 2/5 - 5/5 markers shifted
							</label>
							<label class="radio-inline">
								<input type="radio" name="test_result" value="Quantity/Quality not sufficient for a result" required>
								Quantity/Quality not sufficient for a result
							</label>
							
						</div>
					</div>
<?php
	}
}
?>					
					<!-- allow comments to be added for any test sign out -->
					<hr class="section-mark">
					<div class="form-group row">
						<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
							<label for="problems" class="form-control-label">Document everything relevant to this test here.  Examples include:
								
							</label>
							<ul>
								<li>Was this a training run?</li>
								<li>Add interpt if test result was See Interpt.</li>
								<li>Reason why the test failed.</li>
							</ul>
						</div>

						<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
							<textarea class="form-control" name="comments" rows="4" maxlength="65535" style="margin-bottom: 10px;"></textarea>
						</div>
					</div>
<?php
////////////////////////////////////////////////////////////////
// Add hotspots for reportable tests
////////////////////////////////////////////////////////////////
if (isset($testInfo[0]['assign_directors']) && $testInfo[0]['assign_directors'] === 'yes' && $testInfo[0]['infotrack_report'] === 'yes')
{
	$all_directors = $db->listAll('users-director-permissions');
?>
					<hr class="section-mark">
					<div class="form-group row">
						<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
							<label for="assigned_director_user_id" class="form-control-label">Assign a Director: <span class="required-field">*</span></label>
						</div>
						<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
							<?php
							foreach($all_directors as $key => $director)
							{
							?>
							<label class="radio">
								<input type="radio" name="assigned_director_user_id" value="<?= $director['user_id'];?>" required>
								<?= $director['first_name'];?> <?= $director['last_name'];?> (<?= $director['email_address'];?>)
							</label>
							<?php
							}
							?>
							
							<label class="radio">
								<input type="radio" name="assigned_director_user_id" value="None" required checked="checked">
								None
							</label>
						</div>
					</div>
<?php
}
if ($page === 'update_test')
{
?>
					<div class="form-group row">
						<div class="offset-xs-4 offset-sm-4 offset-md-4 offset-lg-4 col-xs-2 col-sm-2 col-md-2 col-lg-2">
							<button type="submit" id="<?= $page;?>_submit" name="<?= $page;?>_submit" value="Submit"  class="btn btn-primary btn-primary-hover">
								Submit
							</button>
						</div>
					</div>
<?php
}
else
{
	require_once('templates/shared_layouts/form_submit_nav_buttons.php');
}	
?>
				</fieldset>
			</form>
		</div>
	</div>
</div>