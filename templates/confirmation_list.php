
<div class="container-fluid">
			
	<fieldset>
		
		<legend>
			<?= $page_title;?>
		</legend>


     	<div class="row">
     		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				<a href="?page=confirmation_list&status=pending" class="btn btn-primary btn-primary-hover float-right ml-2">Pending Confirmations</a>
				<a href="?page=confirmation_list" class="btn btn-primary btn-primary-hover float-right ml-2">All Confirmations</a>
			</div>
		</div>
     	<div class="row">
     		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		     	<table class="formated_table sort_table_no_inital_sort_no_paging" style="overflow-x:auto;margin-bottom: 10px;">
		        	<thead>
		        		<th>Report #</th>
		        		<th>Sample</th>
		        		<th>mol#</th>
		        		<th>MD#</th>
		        		<th>Soft Lab #</th>
		        		<th>Test Type</th>
		          		<th>Genes</th>
		          		<th>Coding</th>
		          		<th>Amino Acid Change</th>	              
		          		<th>Confirmation Request Status</th>
		          		<th>Confirmation Result</th>
		          		<th>Report Status</th>
		          		<th>Update Confirmation Status</th>
		          	                       
		          	</thead>
		          	<tbody>
				<?php
					foreach($confirmations as $key => $c)
					{
						
				?>
						<tr>
							<td><?= $c['visit_id'];?></td>
							<td><?= $c['sample_name'];?></td>
							<td><?= $c['order_num'];?></td>
							<td><?= $c['mol_num'];?></td>
							<td><?= $c['soft_lab_num'];?></td>

							<td><?= $c['test_name'];?></td>
							<td><?= $c['genes'];?></td>
							<td><?= $c['coding'];?></td>
							<td><?= $c['amino_acid_change'];?></td>
							<td>
								<?= $c['status'];?>
							</td>
							<td><?= $c['outcome'];?></td>
							<td>
								<?= $c['report_status'];?>
							</td>
							<td>
								<a href="?page=update_confirmation&confirmation_id=<?= $c['confirmation_id'];?>" type="button" class="btn btn-primary btn-primary-hover d-print-none">
									Update Confirmation Status
								</a>
							</td>
						</tr>
				<?php
					}
				?>				     
		     
		          	</tbody>
				</table>
			</div>
		</div>	
	</fieldset>
	
</div>
