

<div class="container-fluid">
	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			<div class="alert alert-info">	
				<p>
					<strong> <a href="https://cancer.sanger.ac.uk/cosmic/download" target="_blank">Download Cosmic TSV</a> (cosmic mutation data) CosmicMutantExport.tsv.gz</strong>
				</p>
				<p>
					email: samantha_taffner@urmc.rochester.edu
				</p>
				<p>
					password: SNV_Cosmic_2018
				</p>
			</div>
		</div>
	</div>
<?php	
if (isset($gene_list))
{
?>
	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			<table class="formated_table">
				<thead>
					<th colspan="10">Genes which can be delete</th>
				</thead>
				<tbody>
<?php			

			foreach ($gene_list as $key => $gene)
			{

				// make sure current gene is now in upload gene list
				if (!in_array($gene['genes'], $upload_genes))
				{

					// find if the gene $key is divisible by 10 if it is add row
					if ($key%10 == 0 && $key !== 0)
					{
?>
					</tr>

<?php
					}
					if ($key%10 == 0)
					{
?>
					<tr>
<?php
					}
				
?>
						<td id="gene_delete_cell_<?= $gene['genes'];?>" class="hover delete-gene-in-knowledge-base" data-gene="<?= $gene['genes'];?>"><?= $gene['genes'];?></td>
					
<?php
				}
			}
?>
				</tbody>
			</table>

		</div>
	</div>

	<div class="row">		
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			<div class="alert alert-info">	
				Place CosmicMutantExport.tsv file in cosmic_tsvs folder
			</div>	
		</div>			
	</div>
	

<?php
}
else
{
?>
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		<div class="alert alert-info">	
			Something is wrong with the cosmic/genes_in_assays.csv $upload_genes does not exist.
		</div>
	</div>
<?php
}
?>
</div>