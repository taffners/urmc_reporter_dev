<div class="container-fluid">


<?php
	if (isset($num_pending_tests) && empty($num_pending_tests) && !isset($_GET['view_only']) && isset($samples_in_single_analyte_pool[0]['status']) && $samples_in_single_analyte_pool[0]['status'] !== 'complete')
	{
?>
	<div class="row d-print-none">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 alert alert-info">
			<h1>All samples in this pool have been signed out, remember to push the finish pool button at the bottom right hand corner of this page.</h1>
		</div>
	</div>
<?php
	}

	require_once('templates/tables/single_analyte_pool_table.php');
?>	

<?php
if (isset($num_pending_tests) && empty($num_pending_tests) && !isset($_GET['view_only']) && isset($samples_in_single_analyte_pool[0]['status']) && $samples_in_single_analyte_pool[0]['status'] !== 'complete')
{
?>				
			<form class="form" method="post" class="form-horizontal">
				<div class="form-group row d-print-none">
					<div class="offset-xs-4 offset-sm-4 offset-md-11 offset-lg-11 col-xs-2 col-sm-2 col-md-1 col-lg-1">
						<button type="submit" id="<?= $page;?>_submit" name="<?= $page;?>_submit" value="Submit"  class="btn btn-primary btn-primary-hover">
							Finish Pool
						</button>
					</div>
				</div>	
			</form>	
			
<?php
	}
	require_once('templates/shared_layouts/comments.php');
?>
			<form class="form" method="post" class="form-horizontal">
				<div class="form-group row d-print-none">
					<div class="col-xs-12 col-sm-4 col-md-4">
						<label for="comments" class="form-control-label">Add a new comment:</label>
					</div>

					<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
						<textarea class="form-control" name="comments" rows="4" maxlength="65535" style="margin-bottom: 10px;"></textarea>
					</div>
				</div>				

				<div class="form-group row">
					<div class="offset-xs-4 offset-sm-4 offset-md-4 offset-lg-4 col-xs-2 col-sm-2 col-md-2 col-lg-2">
						<button type="submit" id="comment_submit" name="comment_submit" value="Submit"  class="btn btn-primary btn-primary-hover">
							Submit
						</button>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>