
<div class="container-fluid" style="margin-top:10px;">
	<div class="row">
	     <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12"> 
			<table class="formated_table sort_table">
				<thead>
					<th>Tier</th>
					<th>Tier Summary</th>
					<th>Tissue</th>
					<th>Add By</th>					
				</thead>

				<tbody>
					<?php
						foreach($tiers as $i => $ea_array)
						{
					?>
							<tr>
								<td><?= $ea_array['tier'];?></td>
								<td><?= $ea_array['tier_summary'];?></td>
								<td><?= $ea_array['tissue'];?></td>
								<td><?= $ea_array['user_name'];?></td>					
                                   </tr>
					<?php
						}
					?>
				</tbody>
			</table>
		</div>
     </div>
     <div class="row">
          <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" >
               <div class="alert alert-danger">
                    <strong>Are you sure you like to delete the tier above?</strong>
               </div>
          </div>
     </div>
     <div class="row">
          <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" >
               <form id="add_visit_info" class="form" method="post" class="form-horizontal">
                    <input type="hidden" name="vt_xref_id" value="<?php
                         if(isset($_GET['vt_xref_id']))
                         {
                              echo $_GET['vt_xref_id'];
                         }
                         else
                         {
                              header('Location:'.REDIRECT_URL.'?page=home');
                         }
                    ?>"/>

                    <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
                         <button type="submit" name="submit" value="Submit"  class="btn btn-danger btn-danger-hover">
                              Delete Tier Info
                         </button>

                    </div>
                    <div class="offset-xs-8 offset-sm-8 offset-md-8 offset-lg-9 col-xs-2 col-sm-2 col-md-2 col-lg-1">
                         <a type="button" class="btn btn-success btn-success-hover" href="?page=update_tiers_kb&knowledge_id=<?= $_GET['knowledge_id'];?>">
                              Cancel
                         </a>
                    </div>

               </form>
          </div>
	</div>
</div>