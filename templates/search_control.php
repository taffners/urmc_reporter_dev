<div class="container-fluid">
	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			<h3 class="raised-card-subheader">Positive Controls</h3>
			<ul class="hover-card-list" style="font-size:20px;">
<?php
		if (isset($positive_controls))
		{
			foreach ($positive_controls as $key => $control)
			{
?>	
				<li>
					<a href="?page=historic_controls&control_type_id=<?= $control['control_type_id'];?>&ngs_panel_id=<?= $control['ngs_panel_id'];?>">
						<?= $control['control_name'];?> (<?= $control['ngs_panel'];?>)
					</a>
				</li>
<?php
			}
		}	
?>			
			</ul>
			<h3 class="raised-card-subheader">Negative Controls</h3>
			<ul class="hover-card-list" style="font-size:20px;">
<?php
		if (isset($negative_controls))
		{
			foreach ($negative_controls as $key => $control)
			{
?>	
				<li>
					<a href="?page=historic_controls&control_type_id=<?= $control['control_type_id'];?>&ngs_panel_id=<?= $control['ngs_panel_id'];?>">
						<?= $control['control_name'];?> (<?= $control['ngs_panel'];?>)
					</a>
				</li>
<?php
			}
		}	
?>			</ul>
		</div>
	</div>
</div>