<div class="container-fluid">
	<div class="row alert alert-info">
		These are all of the <?= $page_title;?>.  They can be updated from this page but please <b>make sure you want to update them because it will affect historically also</b>.  New expected variants can also be added from this page.
	</div>
	<div class="row" style="overflow-x:auto;">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			<a href="?page=add_expected_variant&control_type_id=<?= $_GET['control_type_id'];?>" class="btn btn-primary btn-primary-hover">
				Add expected variant
			</a>
		</div>
	</div>
	<div class="row" style="overflow-x:auto;">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			<table class="formated_table sort_table">
				<thead>
					<th>Genes</th>
					<th>Coding</th>
					<th>Amino Acid Change</th>
					<th>Min Accepted Frequency</th>
					<th>Max Accepted Frequency</th>
					<th>Update</th>
					<th>Delete</th>
				</thead>
				<tbody>
<?php
	foreach($expected_vars as $key => $v)
	{
?>
					<tr>
						<td><?= $v['genes'];?></td>
						<td><?= $v['coding'];?></td>
						<td><?= $v['amino_acid_change'];?></td>
						<td><?= $v['min_accepted_frequency'];?></td>
						<td><?= $v['max_accepted_frequency'];?></td>
						<td>
							<a href="?page=add_expected_variant&expected_control_variants_id=<?= $v['expected_control_variants_id'];?>&control_type_id=<?= $_GET['control_type_id'];?>" class="btn btn-primary btn-primary-hover">
								Update
							</a>
						</td>
						<td>Work in progress</td>
					</tr>
<?php
	}
?>					
				</tbody>
			</table>
		</div>
	</div>
</div>

