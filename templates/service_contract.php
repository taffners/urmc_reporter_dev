<div class="container-fluid">
	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
			<a href="?page=add_service_contract&instrument_id=<?=$_GET['instrument_id'];?>" class="btn btn-primary btn-primary-hover">Add Service Contract</a>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			<table class="formated_table sort_table" style="overflow-x:auto;margin-bottom: 10px;">
				<thead>
					<th>Expiration Date</th>
					<th>Days Until Expired</th>
					<th>PO #</th>
					<th>Description</th>
					<th>Update</th>
				</thead>
				<tbody>
<?php 
			foreach ($serviceArray as $key => $contract)
			{
?>
					<tr>
						<td><?= $contract['expiration_date'];?></td>
						<td <?php
						if ($contract['days_left'] < 0)
						{
							echo 'class="dark-grey-background"';
						}
						else if ($contract['days_left'] > 90)
						{
							echo 'class="green-background"';
						}

						else if ($contract['days_left'] > 60)
						{
							echo 'class="btn-warning"';
						}

						else 
						{
							echo 'class="red-background"';
						}

						?>><?= $contract['days_left'] > 0 ? $contract['days_left']: 'expired';?></td>
						<td><?= $contract['po'];?></td>
						<td><?= $contract['description'];?></td>
						<td>
							<a href="?page=add_service_contract&instrument_id=<?=$_GET['instrument_id'];?>&instrument_service_contract_id=<?= $contract['instrument_service_contract_id'];?>" class="btn btn-primary btn-primary-hover">Update<br>Service<br>Contract</a>
						</td>
					</tr>
<?php 
			}
?>					

				</tbody>
			</table>
		</div>
	</div>
</div>