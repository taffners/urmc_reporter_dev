<div class="container-fluid">

	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="height:500px;">
			
			<fieldset class="alert alert-danger">
				<legend class='show'>
					Cancel Test <?= $test_name;?>
				</legend>

				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
						<h1>Warning !!! You are about to cancel following test. <br>
						<br>
						<?= isset($pools_in) && !empty($pools_in) ? 'This will permanently remove the tests from any pools also.':'' ;?></h1>
					</div>
					<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
						<img src="images/warning.png">
					</div>
				</div>
				<hr class="thick-red-line">
				<div class="row" style="font-size: 20px;">
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
						<h1>Test Information:</h1>
					</div>
					<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
						Test:
					</div>
					<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
						<b><?= $test_name;?> <?= $full_test_name;?></b>
					</div>
				</div>
<?php
		if (isset($sampleLogBookArray))
		{
				require_once('templates/shared_layouts/sample_overview.php');
		}
		else
		{
?>
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
					<h1>Sample Information:</h1>
				</div>
			</div>
			<div class="row" style="font-size: 20px;">
				<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">Patient Name: </div>
				<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
					<b>
					<?= $patient_name;?> 
					</b>
				</div>
				<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">MD#: </div>
				<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
					<b>
					<?= $visitArray[0]['mol_num'];?> 
					</b>
				</div>
			</div>
			<div class="row" style="font-size: 20px;">
				<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">Soft Lab #: </div>
				<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
					<b>
					<?= $visitArray[0]['soft_lab_num'];?> 
					</b>
				</div>
				<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">Soft Path #: </div>
				<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
					<b>
					<?= $visitArray[0]['soft_path_num'];?> 
					</b>
				</div>
			</div>
<?php
	if (isset($visitArray[0]['type']) && !empty($visitArray[0]['type']) && isset($visitArray[0]['control_name']) && !empty($visitArray[0]['control_name']))
?>			
			<div class="row" style="font-size: 20px;">
				<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">Control Type: </div>
				<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
					<b>
					<?= $visitArray[0]['type'];?> 
					</b>
				</div>
				<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">Control Name: </div>
				<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
					<b>
					<?= $visitArray[0]['control_name'];?> 
					</b>
				</div>
			</div>

<?php		
		}
?>
				
				<hr class="thick-red-line">
				<form class="form" method="post" class="form-horizontal">

					<div class="form-group row">
						<div class="col-xs-12 col-sm-3 col-md-3">
							<label for="problems" class="form-control-label" style="font-size:20px;">Reason for Canceling:</label>
						</div>

						<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
							<textarea class="form-control" name="comments" rows="4" maxlength="65535" style="margin-bottom: 10px;"></textarea>
						</div>
					</div>
					<div class="form-group row">
						<div class="offset-md-3 offset-lg-3 col-xs-12 col-sm-12 col-md-8 col-lg-8">
							<button type="submit" id="<?= $page;?>_submit" name="<?= $page;?>_submit" value="Submit"  class="btn-lg btn-danger btn-danger-hover">
								Press here if you are sure you would like to cancel <?= $test_name;?> for <?= $patient_name;?>
								<?= isset($pools_in) && !empty($pools_in) ? '<br><br><h1>and you are sure you want to permanently remove the test from all pools.</h1>':'' ;?>
							</button>
						</div>
					</div>
				</form>
			</fieldset>
		</div>
	</div>
</div>