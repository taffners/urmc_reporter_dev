<div class="container-fluid">

	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			<span class="audit-headers">About this document</span>
			<hr class="plain-section-mark">
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			The purpose of this document is to provide the most information about different modules added to <?= SITE_TITLE;?>. Every module created or updated after October 1st, 2020 will be added to this report. This document will evolve with each newly released version of <?= SITE_TITLE;?>.  Key modules created before October 1st, 2020 will be included.   
			<br><br>
			A complementary report to this Module report can be found in the page validation report.
		</div>
	</div>

	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			<hr class="plain-section-mark">
			<span class="audit-headers">Current Info</span>
			<hr class="plain-section-mark">
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3" style="font-weight: bold;">
			Website: 
		</div>
		<div class="col-xs-12 col-sm-12 col-md-9 col-lg-9" >
			<?= SITE_TITLE?>
		</div>		
	</div>
	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3" style="font-weight: bold;">
			Website Version: 
		</div>
		<div class="col-xs-12 col-sm-12 col-md-9 col-lg-9" >
			<?= VERSION?>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3" style="font-weight: bold;">
			Report Date: 
		</div>
		<div class="col-xs-12 col-sm-12 col-md-9 col-lg-9" >
			<?= date('Y-m-d')?>
		</div>
	</div>



<?php
	if (isset($all_modules))
	{
		foreach ($all_modules as $key => $module)
		{
?>


	<hr class="plain-section-mark">
	<span class="audit-headers">Module Name: <?= htmlspecialchars($module['module_name']);?></span>
	<hr class="plain-section-mark">

	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3" style="font-weight: bold;">
			Module Description
		</div>
		<div class="col-xs-12 col-sm-12 col-md-9 col-lg-9" >
			<?= htmlspecialchars($module['module_description']);?>
		</div>
	</div>

<?php
			if ( 
					isset($module['module_diagram']) &&
					!empty($module['module_diagram']) &&
					file_exists('about/diagrams/'.$module['module_diagram'])
				)
			{

				$diagram = 'about/diagrams/'.htmlspecialchars($module['module_diagram']);
?>
	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			<img src="<?= $diagram;?>" alt="<?= htmlspecialchars($module['module_diagram']);?>" style="max-width:1200px;">
		</div>
	</div>
<?php
			}

		}
	}
?>
</div>