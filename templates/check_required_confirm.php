<?php
	require_once('templates/lists/run_info_list.php');
?>

<div class="container-fluid">
	<div class="row">
		<?php
			require_once('templates/shared_layouts/report_progress_bar.php');
		?>

		<!-- update area -->
		<div id="toggle-report-progress-bar" class="col-xs-10 col-sm-10 col-md-10 col-lg-10" style="height:500px;">
			<form class="form" method="post" class="form-horizontal">
				<fieldset>
					<legend>
						Variants Requiring Confirmation
					</legend>					

			<?php
				if (isset($variants_needing_confirm) && empty($variants_needing_confirm))
				{
			?>
					<h3>Confirmations are not required for this report.</h3>

			<?php
				}
				else if(isset($variants_needing_confirm) && !empty($variants_needing_confirm))
				{
			?>
					<div class="alert alert-info">
						<h3>NYS Confirmation Regulations</h3>
						This assay has passed initial NYS validation however, we are currently still working to get this assay fully validated by <a href="downloads/NY_NGS_guidelines.pdf" target="_blank">New York State Regulations</a>.  For an assay to be fully validated 10 positive samples for each type of intended variant in each target area must be sequenced and confirmed. Confirmations for intended variant types of target areas can be ceased once a minimum of 20 target areas have been fully validated/confirmed.  
						<h3><?= SITE_TITLE;?> Implementation of NYS Guidelines</h3> 
						<ul>
							<li>Since the guidelines listed above require some interpretation here is a summary of how <?= SITE_TITLE;?> interprets the following:
								<ul>
									<li>Target area = gene </li>
									<li>Intended variants = SNVs and Indels.</li>
								</ul>
							</li>
						</ul>
						<h3>How to Track confirmations in <?= SITE_TITLE;?></h3>
						<ul>
							<li>Variants in the table below are in genes which have not been fully validated therefore they need to be confirmed by another method.  The purpose of this page is to notify you that the following variants need to be validated.</li>
							<li>Once you click done these variants will be added to the pending confirmations list.</li>
							<li>After you validate these variants please update the pending confirmations list (NGS -> pending confirmations).</li>
						</ul>
					</div>
					<h3>Please Confirm the Following Variants by Another Method Per NYS Regulations: </h3>
					<table class="formated_table" style="margin-bottom:20px;">
						<thead>
							<th>Gene</th>						
							<th>Coding</th>
							<th>Amino Acid Change</th>
							<th>Mutation Type</th>
							<th>Current Confirmation Count</th>
						</thead>
						<tbody>
					<?php
						// This form can not submit if snp or indel is not 
						// provided.  Therefore change the Mutation Type 
						// cell for that sample to a drop down menu and
						// remove the Navigation buttons until it is 
						// filled out
						$require_snp_or_indel_input = False;
						$drop_down_count = 0;
						foreach ($variants_needing_confirm as $key => $variant)
						{
					?>
							<tr>
								<td><?= $variant['genes'];?></td>
								<td><?= $variant['coding'];?></td>
								<td><?= $variant['amino_acid_change'];?></td>
								<td>
					<?php
							// Since not every tech would be able to classify 
							// a mutation to the level that cosmic breaks down
							// mutation type let the user only choose from SNP
							// or Indel this will ensure more accurate 
							// classification
							if (empty($variant['snp_or_indel']) || $variant['snp_or_indel'] === 'unknown')
							{
								$drop_down_count ++;
								$require_snp_or_indel_input = True;
					?>	
								<select class="choose_snp_or_indel" data-observed_variant_id="<?=$variant['observed_variant_id']?>">
		                                   <option value="" disabled="" selected="">-------</option>
		                                   <option>SNP</option>
		                                   <option>Indel</option>
		                                   <option>Skip Classify/Validation</option>
		                              </select>
					<?php
							}
							else
							{
					?>
									<?= $variant['mutation_type'];?> (<?= $variant['snp_or_indel'];?>)
					<?php
							}
					?>
								</td>
								<td><?= $variant['current_confirmation_count'];?></td>
							</tr>
					<?php
						}

					?>
						</tbody>
					</table>

			<?php
				}				
			?>
					<input type="hidden" name="drop-down-count" id="drop_down_count" value="<?=$drop_down_count;?>">
					<div id="div-snp-or-indel" class="d-none">

					</div>
			<?php
				// add submit and nav buttons if a mutation type was chosen for each 
				if ($require_snp_or_indel_input)
				{
			?>
					<div class="row show" id="replace-nav-buttons">
						<div class="col-sm-12 col-xs-12 col-md-12 col-lg-12">
							<div class="alert alert-info">
								<h3>Select mutation type for all unknown above</h3>
							</div>
						</div>
					</div>
			<?php 
				}
			?>
					<div class="d-none" id="nav-buttons">
			<?php
						require_once('templates/shared_layouts/form_submit_nav_buttons.php');			
			?>
					</div>
				</fieldset>
			</form>

		</div>
	</div>
</div>