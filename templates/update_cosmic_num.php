<?php
	if (isset($edit_allowed) && $edit_allowed)
	{
?>		
		<div class="container-fluid">
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="height:500px;">
					<form class="form" method="post" class="form-horizontal">
						<fieldset>
							<legend class='show'>
								Update Cosmic Number for <?= $variant;?>
							</legend>
							<div class="form-group row">
								<div class="alert alert-info">
									Only add integers of the cosmic number.
								</div>
							</div>
							<div class="form-group row">
								<div class="col-xs-12 col-sm-4 col-md-4">
									<label for="cosmic" class="form-control-label" >Cosmic #: <span class="required-field">*</span></label>
								</div>

								<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
									<input type="int" class="form-control" id="cosmic" name="cosmic" value="<?= $utils->GetValueForUpdateInput($knowledge_array, 'cosmic');?>" required="required"/>
								</div>
							</div>
							<div class="offset-xs-4 offset-sm-4 offset-md-4 offset-lg-4 col-xs-2 col-sm-2 col-md-2 col-lg-2">
								<button type="submit" id="<?= $page;?>_submit" name="<?= $page;?>_submit" value="Submit"  class="btn btn-primary btn-primary-hover">
									Update
								</button>
							</div>
							<div class="offset-xs-4 offset-sm-4 offset-md-4 offset-lg-4 col-xs-2 col-sm-2 col-md-2 col-lg-2">
								<a href="?page=<?= $gets_exploded[1];?>" class="btn 
									btn-primary btn-primary-hover" role="button">
									❮ Previous
								</a>
							</div>
						</fieldset>
					</form>
				</div>
			</div>
		</div>
<?php					
	}
	else
	{
?>
	<div class="container">
		<div class="row">
			<div class="offset-xs-4 offset-sm-4 offset-md-4 offset-lg-4 col-xs-2 col-sm-2 col-md-2 col-lg-2">
				<a href="?page=<?= $gets_exploded[1];?>" class="btn 
					btn-primary btn-primary-hover" role="button">
					❮ Previous
				</a>
			</div>
		</div>
	</div>
<?php
	}
?>