<div class="container-fluid">
	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			<form class="form" method="post" class="form-horizontal" enctype="multipart/form-data" autocomplete="off" >
				<fieldset>

					<legend>
						<?= $page_title;?>
					</legend>
					<div class="row">
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
							<b>bug description</b>: <?= $bug_info[0]['bug_description'];?>
						</div>
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
							<b>Reported by</b>: <?= $bug_info[0]['user_name'];?>
						</div>
					</div>
					<div class="form-group row">
						<div class="col-xs-12 col-sm-4 col-md-4">
							<label for="commit_id" class="form-control-label">Git Commit: <span class="required-field">*</span></label>
						</div>

						<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
												
							<input type="text" class="form-control" id="commit_id" name="commit_id" maxlength="10" required="required"/>
						</div>
					</div>

					<div class="form-group row">
						<div class="col-xs-12 col-sm-4 col-md-4">
							<label for="summary_of_bug" class="form-control-label">Summary of Bug: <span class="required-field">*</span></label>
						</div>

						<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
							<textarea class="form-control" name="summary_of_bug" rows="4" maxlength="65535" style="margin-bottom: 10px;" id="summary_of_bug" required="required"></textarea>
						</div>
					</div>	


					<div class="form-group row">
						<div class="col-xs-12 col-sm-4 col-md-4">
							<label for="resulted_in_downtime" class="form-control-label">Did the bug result in downtime? <span class="required-field">*</span></label>
						</div>
						<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
							<div class="radio">
								<label class="radio-inline"><input type="radio" name="resulted_in_downtime" required="required" value="no">No</label>
							
								<label class="radio-inline"><input type="radio" name="resulted_in_downtime" value="yes">Yes</label>
							</div>
						</div>
					</div>
					<div class="form-group row">
						<div class="col-xs-12 col-sm-4 col-md-4">
							<label for="downtime_type" class="form-control-label">Downtime Type? <span class="required-field">*</span></label>
						</div>
						<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
							<div class="radio">
								<label class="radio-inline"><input type="radio" name="downtime_type" required="required" value="full">full</label>
							
								<label class="radio-inline"><input type="radio" name="downtime_type" value="partial">partial</label>

								<label class="radio-inline"><input type="radio" name="downtime_type" value="none">none</label>
							</div>
						</div>
					</div>
					<div class="form-group row">
						<div class="col-xs-12 col-sm-4 col-md-4">
							<label for="downtime_start_time" class="form-control-label">Downtime start time: </label>
						</div>
						<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
							<input class="form-control" type="datetime-local" id="downtime_start_time" name="downtime_start_time"/>
						</div>
					</div>
					<div class="form-group row">
						<div class="col-xs-12 col-sm-4 col-md-4">
							<label for="downtime_end_time" class="form-control-label">Downtime end time: </label>
						</div>
						<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
							<input class="form-control" type="datetime-local" id="downtime_end_time" name="downtime_end_time"/>
						</div>
					</div>
<!-- //////////////////////////////////////////////////////////////////////////////////////////////////////////////////// -->					
					<hr class="section-mark">
					<h2 class="text-center">Add Affected Pages</h2>

					<div class="form-group row">
											
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 mb-2 mt-0">
							<button type="button" class="btn btn-success btn-success-hover append-sample-input" data-clone_template="div-to-clone-affected-pages-field" data-rm_clone_template_btn="clone-affected-pages-remove-btn" data-append_to="div-to-append-affected-pages" style="float: right;">
				        		<span class="fas fa-plus"></span> Insert another affected page
				            </button>
						</div>
					</div>
					<div id="div-to-append-affected-pages">
						<div class="form-group row">
							<div class="col-xs-12 col-sm-3 col-md-2 col-lg-2">
								<label for="page_id" class="form-control-label">
									Affected Page
									<span class="required-field div-to-append-affected-pages-required-span">*</span>
								</label>
							</div>
							<div class="col-xs-12 col-sm-9 col-md-10 col-lg-10">

								<select class="editable-select div-to-append-affected-pages-required-field" name="page_id[]" style="width:100%;">
									<option value="" selected="selected"></option>
			<?php
			
				if (isset($all_qc_pages) && !empty($all_qc_pages))
				{
					foreach ($all_qc_pages as $key => $curr_page)
					{
			?>
									<option value="<?= $curr_page['page_id'];?>"><?= $curr_page['page_name']; ?></option>
			<?php			
					}
												
				}
			?>														
								</select>
								
							</div>
						</div>	
					</div>
					<div class="form-group row">
						<div class="offset-xs-2 offset-sm-2 offset-md-2 offset-lg-2 col-xs-2 col-sm-2 col-md-2 col-lg-2">
							<button type="submit" id="<?= $page;?>_submit" name="<?= $page;?>_submit" value="Submit"  class="btn btn-primary btn-primary-hover">
								Submit
							</button>
						</div>
					</div>
				</fieldset>
			</form>
			<!-- ///////////////////////////////////////////////////////// 
				// Clones
				/////////////////////////////////////////////////////////
				 -->
			<div class="clones">
				<div id="div-to-clone-affected-pages-field" class="form-group row d-none">
					<div class="col-xs-12 col-sm-3 col-md-2 col-lg-2">
						<label for="page_id" class="form-control-label" >
							Affected Page
							<span class="required-field div-to-append-affected-pages-required-span">*</span>
						</label>
					</div>
					<div class="col-xs-11 col-sm-8 col-md-9 col-lg-9">

						<select class="editable-select div-to-append-affected-pages-required-field" name="page_id[]" style="width:100%;">
							<option value="" selected="selected"></option>

	<?php
	
		if (isset($all_qc_pages) && !empty($all_qc_pages))
		{
			foreach ($all_qc_pages as $key => $curr_page)
			{
	?>
							<option value="<?= $curr_page['page_id'];?>"><?= $curr_page['page_name']; ?></option>
	<?php			
			}
										
		}
	?>														
						</select>
						
					</div>
					<div class="col-xs-1 col-sm-1 col-md-1 col-lg-1 ml-0 pl-0">
						<button id="clone-affected-pages-remove-btn" type="button" class="btn btn-danger btn-danger-hover remove-cloned-input" title="delete this linked test.">
	        				<span class="fas fa-minus-circle"></span>
	            		</button>
					</div>
						
				
				</div>
			</div>
		</div>
	</div>
</div>
