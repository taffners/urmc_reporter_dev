<?php
	// find which toggle button is pushed so colors of buttons can be switched
	if (isset($_GET['scan_status']) && $_GET['scan_status'] === 'pending')
	{
		$pending_class = 'btn btn-success btn-success-hover';
		$yes_class = 'btn btn-warning btn-warning-hover';
		$legend_name = 'Unscanned Reports';
		
	}
	else if (isset($_GET['scan_status']) && $_GET['scan_status'] === 'yes')
	{
		$yes_class = 'btn btn-success btn-success-hover';
		$pending_class = 'btn btn-warning btn-warning-hover';
		$legend_name = 'Scanned Reports';
	}
?>

<div class="container-fluid">
	<div class="alert alert-info">
	  	
  		After reports are made in <?= SITE_TITLE;?> <?= $all_softpath;?> will download the report without a header, print the report, and scan it into the patient chart.  This page was created to track this process.  
  		<br><br>
  		<strong>Scan Status:</strong> 
  		<ul>
  			<li  style="margin-bottom: 5px;" >Pending reports have not been included in the patient chart yet. 
  				<a id="not-complete-scan-status-btn" href="?page=transfered_to_patient_chart&scan_status=pending" type="button" class="<?= $pending_class;?>">
                	Click here to view reports with scan status pending
           		</a>
       		</li>
  			<li >While complete reports have been scanned and added to the patient chart.
  				<a id="complete-scan-status-btn" href="?page=transfered_to_patient_chart&scan_status=yes" type="button" class="<?= $yes_class;?>">
					Click here to view reports with scan status complete
				</a>
  			</li>
  		</ul>
  		<br>
  		<strong style="font-size:20px;">
  			Once the report is added to the patient chart it is <span id="softpath-users"><?= $all_softpath;?></span> responsibility to mark the scan status to complete.
  		</strong>
	</div>
	
	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		     <?php 

if ( isset($completed_reports) )
{

?>
		     <fieldset>
		        <legend><?= htmlspecialchars($legend_name);?></legend>

		          

		        <div class="row" style="overflow-x:auto;">
		          	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		               	<table class="formated_table sort_table_no_inital_sort">
		                    <thead>                         
								<th id="pdf-header">PDF</th>
								

								<th
								<?php
								if (isset($user_permssions) && strpos($user_permssions, 'softpath') !== false)
								{
								?> id="scan-status-update-header" 
								<?php
								}
								?>
								title="After reports are made in <?= SITE_TITLE;?> <?= $all_softpath;?> will download the report without a header, print the report, and scan it into the patient chart.">Scan Status</th>
								<th title="When ever a soft path users changes the status of the scan status it is recordered here">Status Change History</th>
								<th>Reporter#</th>
								<th>MD#</th>
								<th>mol#</th>
								<th>Soft Path#</th>
								<th>Soft Lab#</th>
								<th>MRN</th>
								<th>Patient Name</th>
								<th>Panel</th>

								<th>TimeStamp</th> 
		              
		                    </thead>
		                    <tbody>

		                         <!-- iterate over all pending runs and add each entry as a new row in the table-->

		                         <?php
										for($i=0;$i<sizeof($completed_reports);$i++)
										{         											           
											$href = '?page=download_report&visit_id='.$completed_reports[$i]['visit_id'].'&patient_id='.$completed_reports[$i]['patient_id'].'&run_id='.$completed_reports[$i]['run_id'];
											
											if  (
													isset($completed_reports[$i]['ngs_panel_id']) && 
													!empty($completed_reports[$i]['ngs_panel_id'])
												)
											{
												$href.='&ngs_panel_id='.$completed_reports[$i]['ngs_panel_id'];
											} 

											if 	(
													isset($completed_reports[$i]['orderable_tests_id']) && 
													!empty($completed_reports[$i]['orderable_tests_id'])
												)
											{
												$href.='&orderable_tests_id='.$completed_reports[$i]['orderable_tests_id'];
											} 

											if  (
													isset($completed_reports[$i]['sample_log_book_id']) &&
													!empty($completed_reports[$i]['sample_log_book_id'])
												)
											{
												$href.='&sample_log_book_id='.$completed_reports[$i]['sample_log_book_id'];
											} 

											if  (
													isset($completed_reports[$i]['ordered_test_id']) && 
													!empty($completed_reports[$i]['ordered_test_id'])
												)
											{
												$href.='&ordered_test_id='.$completed_reports[$i]['ordered_test_id'];
											} 

											
		                         ?>
										<tr>



											<td><a href="<?= $href;?>" type="button" class="btn btn-success btn-success-hover"><img src="images/adobe-pdf-icon.png" alt="pdf icon" style="height:30px;width:30px"></a></td>
											<td>
											<?php
											if (isset($user_permssions) && strpos($user_permssions, 'softpath') !== false)
											{
											?>
												<div class="radio">
													<label class="radio-inline"><input type="radio" name="scan_status_<?= $i;?>" value="pending" class="toggle-transferred-to-patient-chart-status" data-run_id="<?= $completed_reports[$i]['run_id']; ?>" data-val="pending" <?php
													if(isset($completed_reports[$i]['transferred_to_patient_chart']) && $completed_reports[$i]['transferred_to_patient_chart'] === 'pending')
													{
														echo 'checked="checked"'; 
													}
													?>>Pending</label>
												
													<label class="radio-inline"><input type="radio" name="scan_status_<?= $i;?>" value="yes" class="toggle-transferred-to-patient-chart-status" data-run_id="<?= $completed_reports[$i]['run_id']; ?>" data-val="yes" <?php
													if(isset($completed_reports[$i]['transferred_to_patient_chart']) && $completed_reports[$i]['transferred_to_patient_chart'] === 'yes')
													{
														echo 'checked="checked"'; 
													}
													?>>Complete</label>
												</div>
											<?php
											}
											else
											{
											?>
												<?= $completed_reports[$i]['transferred_to_patient_chart'] === 'yes'? 'Complete':htmlspecialchars($completed_reports[$i]['transferred_to_patient_chart']); ?>
											<?php
											}
											?>

											</td>
											<td><?=  str_replace('yes', 'Complete', htmlspecialchars($completed_reports[$i]['change_log'])) ; ?></td>
											<td><?= htmlspecialchars($completed_reports[$i]['visit_id']); ?></td>
											<td><?= htmlspecialchars($completed_reports[$i]['mol_num']); ?></td>
											<td><?= htmlspecialchars($completed_reports[$i]['order_num']); ?></td>
											<td><?= htmlspecialchars($completed_reports[$i]['soft_path_num']); ?></td>
											<td><?= htmlspecialchars($completed_reports[$i]['soft_lab_num']); ?></td>
											<td><?= htmlspecialchars($completed_reports[$i]['medical_record_num']); ?></td>
											<td><?= htmlspecialchars($completed_reports[$i]['patient_name']); ?></td>
											<td><?= isset($completed_reports[$i]['panel']) ?htmlspecialchars($completed_reports[$i]['panel']) : ''; ?><?= isset($completed_reports[$i]['test_name']) ?htmlspecialchars($completed_reports[$i]['test_name']) : ''; ?></td>
											<td><?= htmlspecialchars($completed_reports[$i]['time_stamp']); ?></td>
		                                </tr>                                    
		                        <?php
		                        		}
		                        ?>
		                    </tbody>
		               	</table>
		            </div>
		        </div>
		     </fieldset>
<?php
}
?>

		</div>
	</div>

</div>
