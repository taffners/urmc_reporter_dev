<div class="container-fluid">	
	<div class="row">
<?php
     // turn off any adding for view only permissions
     if (isset($user_permssions) &&  strpos($user_permssions, 'View_Only') === false)
     {
?>  		
		<div class="col-xs-12 col-sm-12 col-md-1 col-lg-1">
			<a href="?page=add_sample_log_book" id="add-sample-to-log-book-btn" class="btn btn-primary btn-primary-hover" > Add Sample to Log Book</a>
		</div>
<?php
	}
	// find the class for each filter button
	if (isset($_GET['filter']) && $_GET['filter'] === 'pending')
	{
		$pending_filter_btn_class = 'btn btn-success btn-success-hover';
		$in_progress_filter_btn_class = 'btn btn-primary btn-primary-hover';
		$waiting_filter_btn_class = 'btn btn-primary btn-primary-hover';
		$all_filter_btn_class = 'btn btn-primary btn-primary-hover';
	}
	else if (isset($_GET['filter']) && $_GET['filter'] === 'waiting')
	{
		$pending_filter_btn_class = 'btn btn-primary btn-primary-hover';
		$in_progress_filter_btn_class = 'btn btn-primary btn-primary-hover';
		$waiting_filter_btn_class = 'btn btn-success btn-success-hover';
		$all_filter_btn_class = 'btn btn-primary btn-primary-hover';
	}
	else if (isset($_GET['filter']) && $_GET['filter'] === 'all')
	{
		$pending_filter_btn_class = 'btn btn-primary btn-primary-hover';
		$in_progress_filter_btn_class = 'btn btn-primary btn-primary-hover';
		$waiting_filter_btn_class = 'btn btn-primary btn-primary-hover';
		$all_filter_btn_class = 'btn btn-success btn-success-hover';
	}
	else if (isset($_GET['filter']) && $_GET['filter'] === 'in_progress')
	{
		$pending_filter_btn_class = 'btn btn-primary btn-primary-hover';
		$in_progress_filter_btn_class = 'btn btn-success btn-success-hover';
	
		$waiting_filter_btn_class = 'btn btn-primary btn-primary-hover';
		$all_filter_btn_class = 'btn btn-primary btn-primary-hover';
	}
	else
	{		
		$pending_filter_btn_class = 'btn btn-primary btn-primary-hover';
		$in_progress_filter_btn_class = 'btn btn-primary btn-primary-hover';
		$waiting_filter_btn_class = 'btn btn-primary btn-primary-hover';
		$all_filter_btn_class = 'btn btn-primary btn-primary-hover';	
	}
?>
		<div class="col-xs-12 col-sm-12 col-md-1 col-lg-1">
			<a href="?page=sample_log_book&filter=pending" id="pending-tests-btn" class="<?= $pending_filter_btn_class;?>">Pending Tests</a>
		</div>
		<div class="col-xs-12 col-sm-12 col-md-1 col-lg-1">
			<a href="?page=sample_log_book&filter=in_progress" id="tests-in-progress" class="<?= $in_progress_filter_btn_class;?>">In Progress Tests</a>
		</div>
		<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
			<a href="?page=in_progress_work_book&nav_status=off" target="_blank" id="sample-in-progress-workbook" class="btn btn-primary btn-primary-hover">In Progress workbook</a>
		</div>

		<!-- 
		<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
			<a href="?page=sample_log_book&filter=waiting" class="<?= $waiting_filter_btn_class;?>">Waiting tests</a>
		</div>
		 -->
		<!-- <div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
			<a href="?page=sample_log_book&filter=all" class="<?= $all_filter_btn_class;?>">All Samples</a>
		</div>
		<div class="col-md-1 col-lg-1">

		</div> -->

		

		
		<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 d-print-none ">
			<form id="search-all-samples" class="form" method="post" class="form-horizontal">
				<div class="form-group row mb-0">
					
					<div id="search-all-samples-div" class="col-xs-10 col-sm-10 col-md-11 col-lg-11" >
						<div class="form-group row">
							<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6" style="padding-right:2px;">
								<input id="search-all-samples-input" type="text" class="form-control" placeholder="Search All Samples ..." name="search"/>
								<label class="d-none" for="search-all-samples-input">Search all samples</label>
							</div>
							<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3" style="padding-right:2px;padding-left:0px;">
								<input id="search-all-samples-input-start-date" type="date" class="form-control" placeholder="Start Date" name="search-by-start-date" value=""/>
								<label class="d-none" for="search-all-samples-input-start-date">Search all samples start date</label>
							</div>
							<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3" style="padding-right:0px;padding-left:0px;">
								<input id="search-all-samples-input-end-date" type="date" class="form-control" placeholder="End Date" name="search-by-end-date"/>
								<label class="d-none" for="search-all-samples-input-end-date">Search all samples end date</label>
							</div>
						</div>
					</div>
					<div class="col-xs-2 col-sm-2 col-md-1 col-lg-1"style="padding-right:0px;padding-left:0px;">
						<button type="submit" name="search-all-samples-submit" class="btn btn-primary btn-primary-hover" value="" aria-label="search all samples">
							<span class="fas fa-search" style="margin-top:2px;margin-bottom:2px;"></span>
						</button>
					</div>
				</div>
			</form>
		</div>
	</div>
	<hr class="section-mark">
	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" id="sample-log-book-table">
			<table  class="notebook_table sort_table_no_inital_sort" style="overflow-x:auto;margin-bottom: 10px;">
				<thead>
					<th class="pink_right_border">Log #</th>
					<th>Test Status</th>
					
					<th>Last Name</th>
					<th>First Name</th>
					<th>Soft Lab #</th>
					<th>MRN</th>
					<th>MPI</th>
					<th>MD#</th>
					<th>Received</th>
					<th>Sample Type</th>
					<th id="comments-th" title="All comments added under the sample but not the test are shown here.  This makes all sample level comments searchable.">Comments</th>
					<th id="summary-tests-added-th">Summary Tests Added</th>
<?php
     // turn off any adding for view only permissions
     if (isset($user_permssions) &&  strpos($user_permssions, 'View_Only') === false)
     {
?>  					
					<th id="edit-update-ordered-tests-th" class="d-print-none">Edit/Update Ordered Tests</th>
					<th id="add-test-th" class="d-print-none">Add Test</th>
					<th id="add-reflex-th" class="d-print-none">Add Reflex Tracker</th>
					<th id="edit-sample-th" class="d-print-none">Edit Sample</th>
<?php
	}
?>
				</thead>
				<tbody>
<?php
	if (isset($sampleLogBookArray) && !empty($sampleLogBookArray))
	{
		foreach ($sampleLogBookArray as $key => $sample)
		{
?>
					<tr>
						<td class="pink_right_border"><?= $sample['sample_log_book_id'];?></td>
						<td><?php
						// find if any test has a pending status
						if (strpos($sample['test_status_concat'], 'pending') !== false)
						{
							echo 'test pending';
						}
						else
						{
							echo 'X';
						}
						?> 	
						</td>
						
						
						<td><?= $sample['last_name'];?></td>
						<td><?= $sample['first_name'];?></td>
						<td><?= $sample['soft_lab_num'];?></td>
						<td><?= $sample['medical_record_num'];?></td>
						<td><?= $sample['mpi'];?></td>
						<td><?= $sample['mol_num'];?></td>
						
						<td><?= $sample['date_received'];?></td>
						<td><?= $sample['test_tissue'];?></td>
						<td><?= str_replace(',', '<br>', $sample['problems']);?></td>
						
						<td>
						<?php
							$ordered_tests = explode(',', $sample['ordered_tests']);
							$test_status = explode(',', $sample['test_status_concat']);

							foreach ($ordered_tests as $key_t => $test)
							{
								$test_dot = $utils->getStatusDotColor($test_status[$key_t]);
								echo $test.' '.$test_dot.'<br>';
							}

							if (isset($sample['ordered_reflexes']) && isset($sample['reflex_status_concat']))
							{
								// Add any reflexes
								$ordered_reflexes = explode(',', $sample['ordered_reflexes']);
								$reflex_status = explode(',', $sample['reflex_status_concat']);
								
								foreach ($ordered_reflexes as $key_r => $reflex)
								{
									$test_dot = $utils->getStatusDotColor($reflex_status[$key_r]);
									echo '<span title="'.$reflex.'">more info hover</span>'.$test_dot.'<br>';
								}	
							}						

						?>	
						</td>
<?php
     // turn off any adding for view only permissions
     if (isset($user_permssions) &&  strpos($user_permssions, 'View_Only') === false)
     {
?>  						
						<td><?php
						if (!empty($sample['ordered_tests']))
						{
						?>
							<a href="?page=all_sample_tests&sample_log_book_id=<?=$sample['sample_log_book_id'];?>" class="btn btn-primary btn-primary-hover"> Edit/Update<br>Print/ Cancel<br>Test</a>
						<?php
						}
						?></td>
						<td><a href="?page=add_test&sample_log_book_id=<?=$sample['sample_log_book_id'];?>" class="btn btn-primary btn-primary-hover"> Add Test</a></td>
						<td><a href="?page=add_reflex_tracker&sample_log_book_id=<?=$sample['sample_log_book_id'];?>" title="A reflex tracker will remind the user of reflexes depending on result triggers." class="btn btn-primary btn-primary-hover">Add Reflex<br>Tracker</a></td>
						<td>
							<a href="?page=add_sample_log_book&sample_log_book_id=<?=$sample['sample_log_book_id'];?>" class="btn btn-primary btn-primary-hover" aria-label="edit sample log book" title="edit sample log book"> 
								<img src="images/sample_log_book_edit.png" style="margin-left:5px;height:45px;width:45px;" alt="edit sample log book">
							</a>
						</td>
<?php
	} // view only close
?>
					</tr>
<?php
		}
	}
?>
				</tbody>
			</table>
		</div>
	</div>
</div>
