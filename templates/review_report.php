<?php
	require_once('templates/show_edit_report.php');
?>
<?php
if	(
		!isset($orderableTestArray[0]['hotspot_variants']) || 
		empty($orderableTestArray[0]['hotspot_variants']) ||
		$orderableTestArray[0]['hotspot_variants'] !== 'yes'
	)
{

?>
<div class="container-fluid">
	<div class="row">


		<!-- update area -->
		<div id="toggle-report-progress-bar" class="offset-xs-2 offset-sm-2 offset-md-2 offset-lg-2 col-xs-10 col-sm-10 col-md-10 col-lg-10">			
			<fieldset>
				<legend>
					Tech Reviewed History
				</legend>
				<div class="alert alert-info">
					<h2>Tech Approval History</h2> <br>
		<?php
				$tech_approved_found = False;
				// add all tech approved
				if (isset($completed_steps))
				{					
					foreach ($completed_steps as $key=>$step)
					{
						if ($step['step'] === 'tech_approved')
						{
							$tech_approved_found = True;
		?>		
							Tech approved by: <?= $step['user_name'];?> on <?= $step['time_stamp'];?><br>	
		<?php
						}						
					}
				}

				if ($tech_approved_found === False)
				{
					echo 'Currently there is no tech approval History';
				}
		?>
				</div>
<?php
	///////////////////////////////////////////////////////////
	// Add information about reviewers that were already notified. Including who notified, who was notified, and when.
	///////////////////////////////////////////////////////////

	if (isset($reviewers_emailed) && !empty($reviewers_emailed))
	{
?>
		<div class="alert alert-info" id="notified-reviewers-box">
			<h2>Notified reviewers</h2> <br>
			<ul>
<?php
		foreach ($reviewers_emailed as $key => $sent_email)
		{
?>
			<li><?=$sent_email['sender_name'];?> notified <?=$sent_email['recipient_name'];?> at <?=$sent_email['time_stamp'];?></li>
			

<?php
		}
?>
			</ul>
		</div>
<?php
	}

?>


<?php
		// if ( ($num_reviews < 2) && (isset($all_reviewers) && (strpos($user_permssions, 'admin') !== false || strpos($user_permssions, 'director') !== false)) )
		if ( ($num_reviews < 1) && (isset($all_reviewers) && (strpos($user_permssions, 'director') !== false)) )
		{
?>
				<div class="alert alert-success" >
					<h2>Notify Reviewers:</h2>
					<form class="form" method="post" class="form-horizontal" style="margin-top:10px;">
						<div class="form-group row">
							<div class="col-xs-12 col-sm-4 col-md-4">
								<label for="selected_reviewers" class="form-control-label" >Select all reviewers to email: <span class="required-field">*</span></label>
							</div>
							<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
	<?php
						foreach($all_reviewers as $key => $reviewer)
						{						
	?>
								<div class="checkbox">
									<label>
										<input type="checkbox" 
										name="selected_reviewers[<?= $key;?>]" 
										value="<?= $reviewer['user_id'];?>" >
											
											<?= $reviewer['first_name'];?> <?= $reviewer['last_name'];?> (<?= $reviewer['email_address'];?>)
									</label>
								</div>
	<?php
						}
	?>
							</div>
						</div>

						<div class="row">
							<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
								<button type="submit" id="notify_reviewers_submit" name="notify_reviewers_submit" value="Submit"  class="btn btn-primary btn-primary-hover">
									<span class="fas fa-envelope"></span>
									Notify Reviewers					
								</button>
							</div>
						</div>
					</form>			
				</div>
<?php
		}

?>	
<?php
		require_once('templates/shared_layouts/message_board.php')
?>
			</fieldset>
		</div>
	</div>

</div>
<?php
}
?>
<form class="form" method="post" class="form-horizontal">

			<!-- add the common hidden fields for every form -->
<?php
				require_once('templates/shared_layouts/form_hidden_fields.php');
		
	if (isset($runInfoArray) && $runInfoArray[0]['status'] === 'revising')
	{
?>
	<div class="row">

		<div class="offset-xs-2 offset-sm-2 offset-md-2 offset-lg-2 col-xs-10 col-sm-10 col-md-10 col-lg-10">
			<div class="form-group">
				<textarea class="form-control" name="change_summary" rows="5" maxlength="65535" placeholder="Add a summary of the changes for this version." required="required"></textarea>
			</div>
		</div>
	</div>
<?php
	}
?>

		<!-- add submit and nav buttons -->
		<?php
			require_once('templates/shared_layouts/form_submit_nav_buttons.php')
		?>

	

</form>			


