<div class="container-fluid">
	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			<fieldset>
				<legend>Search Results</legend>
				<div class="row alert alert-info">
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
						This stats table shows all Infotrack Reportable tests.  Therefore Idylla and NGS tests will be included.  Also if one sample has two mutations in the same gene the count will increase by two.  For example if patient A has a test tissue of Blood and has two BRAF mutations and sample B also has a test tissue of Blood has 1 mutation the total count of BRAF mutations for blood will be 3.  Refer to the search patients and their reports for a more detailed list of mutations for gene and tissue type. 
					</div>
				</div>
				<div class="row">
					<?php

						if (isset($tissue_gene_stats) && !empty($tissue_gene_stats))
						{
					?>
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">						
						<table class="formated_table sort_table">
		 					<thead>
		 						<th>Test Tissue</th>
								<th>Genes</th>
								<th>Count</th>
								<th>Patient Reports</th>
								<th>Mutations</th>
		 					</thead>
		 					<tbody>
		 						<?php
		 						foreach ($tissue_gene_stats as $key => $value)
		 						{
		 						?>
		 						<tr>
		 							<td><?= $value['test_tissue'];?></td>
		 							<td><?= $value['genes'];?></td>
		 							<td><?= $value['count'];?></td>
		 							<td>
		 								<a href="?page=patient_search_results&test_tissue=<?= $value['test_tissue'];?>&genes=<?= $value['genes'];?>" type="button" class="btn btn-primary btn-primary-hover">
		 									All Patient Reports
		 								</a>
		 							</td>
		 							<td>
		 								<a href="?page=tissue_specfic_gene_info&test_tissue=<?= $value['test_tissue'];?>&genes=<?= $value['genes'];?>" type="button" class="btn btn-primary btn-primary-hover">
		 									Mutations
		 								</a>
		 							</td>
		 						</tr>
		 						<?php
		 						}
		 						?>

		 					</tbody>
		 				</table>
         		 			
    		 		</div>
					<?php
						}
					?>
				</div>

			</fieldset>
		</div>

	</div>
</div>