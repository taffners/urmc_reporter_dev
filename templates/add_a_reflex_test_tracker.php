<div class="container-fluid">
	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			<form class="form" method="post" class="form-horizontal" enctype="multipart/form-data" autocomplete="off" >
				<fieldset>

					<legend>
						<?= $page_title;?>
					</legend>


					<div class="form-group row">
						<div class="col-xs-12 col-sm-4 col-md-4">
							<label for="reflex_name" class="form-control-label">Reflex Name (This will auto fill.  Your are not expected to fill this in): <span class="required-field">*</span></label>
						</div>

						<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
												
							<input type="text" class="form-control" id="reflex_name" name="reflex_name" maxlength="255" value="<?= $utils->GetValueForUpdateInput($reflexArray, 'reflex_name');?>" required="required" readonly="readonly"/>
						</div>
					</div>

<!-- //////////////////////////////////////////////////////////////////////////////////////////////////////////////////// -->					
					<hr class="section-mark">
					<h2 class="text-center">Add Reflex Tracker</h2>

					<div class="form-group row">
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
							<div class="alert alert-info">
								<u>Purpose</u>:  A Reflex Tracker will remind the user to add the next reflex test if the proceeding test meets the reflex trigger result. 
								<br><br>
								Tests that have a form control test result equal to yes or send out tests equal to yes will appear in the linked tests drop down menu.
								<br><br>
								Add tests in the order of reflex.
								<br><br>
								For example if we have the following linked reflex:
								<br><b>JAK2 reflex to CALR reflex to Myeloid NGS</b>
								<br><br>
								It would operate like this:
								<br><br>
								CALR reflex is triggered upon a negative JAK2 result.  Reflex to Myeloid NGS would be triggered by a negative JAK2 and CALR
									
							</div>
						</div>
					
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 mb-2 mt-0">
							<button type="button" class="btn btn-success btn-success-hover append-sample-input" data-clone_template="div-to-clone-linked-tests-field" data-rm_clone_template_btn="clone-linked-tests-remove-btn" data-append_to="div-to-append-linked-tests" style="float: right;">
				        		<span class="fas fa-plus"></span> Insert another Linked Reflex Test
				            </button>
						</div>
					</div>
					<div id="div-to-append-linked-tests">
		<?php

		//////////////////////////////////////
		// Add previously added fields
		//////////////////////////////////////
		if (isset($all_reflex_links) && !empty($all_reflex_links))
		{
			
			foreach ($all_reflex_links as $link_key => $link)
			{
		?>
							<div id="div-to-clone-linked-tests-field_<?= $link_key;?>" class="form-group row">
								<div class="col-xs-12 col-sm-3 col-md-2 col-lg-2">
									<label for="orderable_tests_id" class="form-control-label" title="The only tests that appear here are tests that have form control test result equal to yes or send out tests equal to yes.">
										Linked Test
										<span class="required-field div-to-append-linked-tests-required-span">*</span>
									</label>
								</div>
								<div class="col-xs-11 col-sm-8 col-md-9 col-lg-9">

									<select class="editable-select div-to-append-linked-tests-required-field" name="test_name[]" style="width:100%;">
										<option value="" selected="selected"></option>

				<?php
				
					if (isset($allTestsAvailable) && !empty($allTestsAvailable))
					{
						foreach ($allTestsAvailable as $key => $test)
						{
				?>
										<option value="<?= $test['test_name'];?>" <?=$utils->GetValueForUpdateSelect($link, 'test_name', $test['test_name']);?>>
											<?= $test['test_name']; ?> 
											(<?= $test['full_test_name']; ?>, 
											<?= $test['test_type']; ?>, 
											<?= $test['assay_name']; ?>)
										</option>
				<?php			
						}
													
					}
				?>														
									</select>
									
								</div>
								<div class="col-xs-1 col-sm-1 col-md-1 col-lg-1">
									<button id="clone-linked-tests-remove-btn_<?= $link_key;?>" type="button" class="btn btn-danger btn-danger-hover remove-cloned-input div-to-append-linked-tests-added-clone" title="delete this linked test." data-rm_temp_id="div-to-clone-linked-tests-field_<?= $link_key;?>">
				        				<span class="fas fa-minus-circle"></span>
				            		</button>
				            	</div>

								<div class="col-xs-12 col-sm-3 col-md-2 col-lg-2">
									<label for="data_type" class="form-control-label">
										Reflex Trigger Result <span class="required-field div-to-append-linked-tests-required-span">*</span>
									</label>
								</div>	


								<div class="col-xs-12 col-sm-8 col-md-10 col-lg-10 ">
								<?php
								foreach($historyResults as $key => $hr)
								{
								?>
									<label class="radio-inline">
										<input type="radio" class="added-radio-update-name" name="test_result_<?= $link_key;?>" value="<?= $hr['test_result'];?>" required <?= $utils->GetValueForUpdateRadioCBX($link, 'reflex_value', $hr['test_result']);?>>
										<?= $hr['test_result'];?>
									</label>
								<?php
								}
								?>		
									
									<label class="radio-inline">
										<input type="radio" class="added-radio-update-name" name="test_result_<?= $link_key;?>" value="Last Reflex Test" required <?= $utils->GetValueForUpdateRadioCBX($link, 'reflex_value', 'Last Reflex Test');?>>
										Last Reflex Test no Trigger
									</label>
								</div>
							</div>
							
			<?php
			}
			?>
							<div class="form-group row d-none">
								<div class="col-xs-12 col-sm-4 col-md-4">
									<label for="clone-number" class="form-control-label">clone-number <span class="required-field">*</span></label>
								</div>

								<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
														
									<input type="number" class="form-control" id="clone-number" name="clone-number" value="<?= $link_key;?>" />
								</div>
							</div>
		<?php

		}

		/////////////////////////////////////
		// Add a default empty if previously values not already present.
		/////////////////////////////////////
		else
		{
		?>
					
						
							<div class="form-group row">
								<div class="col-xs-12 col-sm-3 col-md-2 col-lg-2">
									<label for="orderable_tests_id" class="form-control-label" title="The only tests that appear here are tests that have form control test result equal to yes or send out tests equal to yes.">
										Linked Test
										<span class="required-field div-to-append-linked-tests-required-span">*</span>
									</label>
								</div>
								<div class="col-xs-12 col-sm-9 col-md-10 col-lg-10">

									<select class="editable-select div-to-append-linked-tests-required-field" name="test_name[]" style="width:100%;">
										<option value="" selected="selected"></option>

				<?php
				
					if (isset($allTestsAvailable) && !empty($allTestsAvailable))
					{
						foreach ($allTestsAvailable as $key => $test)
						{
				?>
							<option value="<?= $test['test_name'];?>" ><?= $test['test_name']; ?> 
													(<?= $test['full_test_name']; ?>, 
													<?= $test['test_type']; ?>, 
													<?= $test['assay_name']; ?>)</option>
				<?php			
						}
													
					}
				?>														
									</select>
									
								</div>
							</div>
							<div class="form-group row">
								<div class="col-xs-12 col-sm-3 col-md-2 col-lg-2">
									<label for="data_type" class="form-control-label">
										Reflex Trigger Result <span class="required-field div-to-append-linked-tests-required-span">*</span>
									</label>
								</div>	
								<div class="col-xs-12 col-sm-8 col-md-10 col-lg-10 ">
								<?php
								foreach($historyResults as $key => $hr)
								{
								?>
									<label class="radio-inline">
										<input type="radio" class="added-radio-update-name" name="test_result_0" value="<?= $hr['test_result'];?>" required>
										<?= $hr['test_result'];?>
									</label>
								<?php
								}
								?>									
									
								</div>
							</div>
					

		<?php
		}
		?>		
					</div>
					<div class="form-group row">
						<div class="offset-xs-2 offset-sm-2 offset-md-2 offset-lg-2 col-xs-2 col-sm-2 col-md-2 col-lg-2">
							<button type="submit" id="<?= $page;?>_submit" name="<?= $page;?>_submit" value="Submit"  class="btn btn-primary btn-primary-hover">
								Submit
							</button>
						</div>
					</div>
				</fieldset>
			</form>
			<!-- ///////////////////////////////////////////////////////// 
				// Clones
				/////////////////////////////////////////////////////////
				 -->
			<div class="clones">
				<div id="div-to-clone-linked-tests-field" class="form-group row d-none">
					<div class="col-xs-12 col-sm-3 col-md-2 col-lg-2">
						<label for="orderable_tests_id" class="form-control-label" title="The only tests that appear here are tests that have form control test result equal to yes or send out tests equal to yes.">
							Linked Test
							<span class="required-field div-to-append-linked-tests-required-span">*</span>
						</label>
					</div>
					<div class="col-xs-11 col-sm-8 col-md-9 col-lg-9">

						<select class="editable-select div-to-append-linked-tests-required-field" name="test_name[]" style="width:100%;">
							<option value="" selected="selected"></option>

	<?php
	
		if (isset($allTestsAvailable) && !empty($allTestsAvailable))
		{
			foreach ($allTestsAvailable as $key => $test)
			{
	?>
							<option value="<?= $test['test_name'];?>" ><?= $test['test_name']; ?> 
										(<?= $test['full_test_name']; ?>, 
										<?= $test['test_type']; ?>, 
										<?= $test['assay_name']; ?>)</option>
	<?php			
			}
										
		}
	?>														
						</select>
						
					</div>
					<div class="col-xs-1 col-sm-1 col-md-1 col-lg-1 ml-0 pl-0">
						<button id="clone-linked-tests-remove-btn" type="button" class="btn btn-danger btn-danger-hover remove-cloned-input" title="delete this linked test.">
	        				<span class="fas fa-minus-circle"></span>
	            		</button>
					</div>
					<div class="col-xs-12 col-sm-3 col-md-2 col-lg-2">
						<label for="data_type" class="form-control-label">
							Reflex Trigger Result <span class="required-field  div-to-append-linked-tests-required-span">*</span>
						</label>
					</div>	
					<div class="col-xs-12 col-sm-8 col-md-10 col-lg-10 ">	
					<?php
					foreach($historyResults as $key => $hr)
					{
					?>
						<label class="radio-inline">
							<input type="radio" class="added-radio-update-name" name="test_result" value="<?= $hr['test_result'];?>" required>
							<?= $hr['test_result'];?>
						</label>
					<?php
					}
					?>	
						<label class="radio-inline">
							<input type="radio" class="added-radio-update-name" name="test_result" value="Last Reflex Test" required>
							Last Reflex Test no Trigger
						</label>
					</div>
								
				
				</div>
			</div>
		</div>
	</div>
</div>
