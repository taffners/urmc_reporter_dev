<?php
if (isset($_GET['pool_chip_linker_id']) && isset($poolStepTracker) && isset($_GET['ngs_panel_id']))
{

?>
<div class="row">
		<!-- report progress bar toggles -->	
		<?php
		require_once('templates/shared_layouts/report_toggle_show_status.php');
		?>

		<!-- Progress bar -->
		<?php
		require_once('templates/shared_layouts/pool_step_nav_bar.php')
		?>
		<!-- update area -->
		<div id="toggle-report-progress-bar" class="col-xs-10 col-sm-10 col-md-10 col-lg-10">
			<form id="add_pool_conc" class="form" method="post" class="form-horizontal">
				<fieldset>
					<legend>
						<?= $utils->UnderscoreCaseToHumanReadable($page);?>
					</legend>
					<div class="row">
						<ul class="hover-card-list" style="font-size:20px;">
	<?php
						// add general information for library pool
						if (isset($pending_pools[0]['library_prep_date']))
						{
	?>
							<li>Library Prep Date: <?= $pending_pools[0]['library_prep_date'];?></li>
	<?php
						}

						if (isset($pending_pools[0]['ngs_run_date']))
						{
	?>
							<li>Run Date: <input type="date" name="ngs_run_date" value="<?= $pending_pools[0]['ngs_run_date'];?>"/></li>
	<?php
						}

						else
						{
							$tomorrow = new DateTime('+1 day');
	?>
							<li>Run Date: <input type="date" name="ngs_run_date" value="<?= $tomorrow->format('Y-m-d');?>"/></li>
	<?php
						}
	?>					
					<?php
						if (!isset($last_used_indexes) || empty($last_used_indexes))
						{
							$previous_i7 = 1;
							$previous_i5 = 1;
							echo 'No current history';
						}
						else
						{

							// more explained about this below after the table is added.
							$previous_i7 = intval($last_used_indexes[0]['i7_index_miseq']) + 1;

							if ($previous_i7 > 12)
							{
								$previous_i7 = 1;
							}

							$previous_i5 = intval($last_used_indexes[0]['i5_index_miseq']) + 1;

							if ($previous_i5 > 8)
							{
								$previous_i5 = 1;
							}
											
						}	
					?>
							<li>Last indexes for run previously performed:
								<br>i7: <?= $last_used_indexes[0]['i7_index_miseq'];?>
								<br>i5: <?= $last_used_indexes[0]['i5_index_miseq'];?>
							</li>
					<?php
						

						// since the new NTC will be one greater than the previous I value 
						// This start value needs to be kept.
						$start_i5 = $previous_i5;
					?>						

							
						</ul>
					</div>										
					<div class="row">
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
							<h3 class="raised-card-subheader">Visits in Library Pool</h3>

							<table class="formated_table">
								<thead>
									<th>Reporter#</th>			
									<th>Sample Name<br>(MD# / soft lab#)</th>
									<th>Patient Name</th>
									<th>Qubit<br>(stock DNA conc.)</th>
									<th>Dilution Total Volume</th>
									<th>DNA Volume</th>
									<th>AE Volume</th>
									<th>Order #<br>(Library Tube)</th>
									<th>Index 1 i7</th>
									<th>Index 2 i5</th>
								</thead>
								<tbody>
			<?php
				if (isset($visits_in_pool))
				{				
					
					foreach ($visits_in_pool as $key => $visit)
					{			

						$skip_dilution_calc = False;
						$order_num =  intval($visit['order_num']); // used to keep track of last num so ntc can be the next one.

						// check if dna_conc is null.  If so the sample is most likely a control.
						// Either way it dilution total volume, dna volume and ae volume can not be
						// calculated.
						if (empty($visit['dna_conc']))
						{
							$total_vol = '';
							$skip_dilution_calc = True;
						}
						
						else
						{
							// If total volume was previously added use this value.
							if (isset($visit['dilution_total']) && !empty($visit['dilution_total']))
							{
								$total_vol = $visit['dilution_total'];
							}

							// For visits without a previous entry for dilution total find a starting point for total volume depending on stock concentration DNA_conc
							else if (intval($visit['dna_conc']) > 200)
							{
								$total_vol = 150;
							}
							else if (intval($visit['dna_conc']) < 50)
							{
								$total_vol = 40;
							} 
							else
							{
								$total_vol = 100;
							}

							$DNA_vol = round((5 * $total_vol ) / $visit['dna_conc'], 1);
							$AE_vol = round($total_vol - $DNA_vol, 1);
						}
			?>
									<tr>
										<td>
											<?= $visit['visit_id'];?>
											<input class="d-none" type="number" name="visit_<?=$key;?>[visit_id]" value="<?= $visit['visit_id'];?>" style="width:45px;" />
											<input class="d-none" type="number" name="visit_<?=$key;?>[visits_in_pool_id]" value="<?= $visit['visits_in_pool_id'];?>" style="width:45px;"/>
										</td>
										<td><?= $visit['sample_name'];?></td>
										
										<td><?= $visit['patient_name'];?></td>
	<?php
								if ($skip_dilution_calc)
								{
	?>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
	<?php								
								}
								else
								{
	?>
										<td><?= $visit['dna_conc'];?></td>
										<td style="white-space: nowrap;">
											<span class="required-field">*</span><input type="number" name="visit_<?=$key;?>[dilution_total]" value="<?= $total_vol;?>" style="width:65px;" required="required" class="dilution-total-js-calc" id="dilution_total_js_calc_<?= $visit['visit_id'];?>" data-visit_id="<?= $visit['visit_id'];?>" data-dna_conc="<?= $visit['dna_conc'];?>"/>
										</td>
										<td>
											<span id="span_dna_vol_js_calc_<?= $visit['visit_id'];?>"><?= $DNA_vol;?></span>
										</td>
										<td>
											<span id="span_ae_vol_js_calc_<?= $visit['visit_id'];?>"><?= $AE_vol;?></span>
										</td>
	<?php								
								}
	?>								
							
										<!-- Add index information based on previously used NTC -->
										<td><?= $visit['order_num'];?></td>

										<td style="white-space: nowrap;">
									<?php
										// Get value for i7 index
										if (isset($visit['i7_index_miseq']))
										{
											$i7 = $visit['i7_index_miseq'];
										}
										else
										{
											$i7 =  $previous_i7;
										}

										// if number is 1-9 prefix should be A70.. Otherwise drop the 
										if (intval($i7) > 9)
										{
											$i7_prefix = 'A7';
										}
										else
										{
											$i7_prefix = 'A70';
										}
									?>


											<span class="required-field">*</span><?= $i7_prefix;?><input type="text" name="visit_<?=$key;?>[i7_index_miseq]" value="<?= $i7;?>" style="width:40px;" required="required" pattern="^[1-9][0-2]*$"/>
										</td>
										<td style="white-space: nowrap;">
									<?php
										// Get value for i7 index
										if (isset($visit['i5_index_miseq']))
										{
											$i5 = $visit['i5_index_miseq'];
										}
										else if (sizeof($visits_in_pool) === $key + 1)
										{
											$i5 =  $start_i5;
										}
										else
										{
											$i5 =  $key + 1;
										}

										// if number is 1-9 prefix should be A70.. Otherwise drop the 
										if (intval($i5) > 9)
										{
											$i5_prefix = 'A5';
										}
										else
										{
											$i5_prefix = 'A50';
										}
									?>
											<span class="required-field">*</span><?= $i5_prefix;?><input type="text" name="visit_<?=$key;?>[i5_index_miseq]" value="<?= $i5?>" style="width:40px;" required="required" pattern="[1-8]"/>
										</td>
									
									</tr>
			<?php
						////////////////////////////////////////////////
						// Find set indexes based on previous indexes used.
						// i7 
							// 12 possible barcodes 
							// next index to use will be the previous NTC index plus 1 if > 12 set to 1
						// i5 
							// 8 possible barcodes
							// index will always start with 1 for samples.
							// NTC rotates which one to use.  For example NTC last time was 6 next 
							// time NTC index will be 7.
						////////////////////////////////////////////////
						// this is for the first sample in 
						$previous_i7++;

						if ($previous_i7 > 12)
						{
							$previous_i7 = 1;
						}

						$previous_i5++;

						if ($previous_i5 > 8)
						{
							$previous_i5 = 1;
						}



					}
				}
			?>			
								</tbody>
							</table>
						</div>
					</div>
					
				<?php
					// add form submit, previous, and next buttons.
					require_once('templates/shared_layouts/form_pool_submit_nav_buttons.php');
				?>
				</fieldset>
			</form>
		</div>


	<?php

	}
	?>
		

		</div>
	</div>
</div>