</div>
<?php
	require_once('templates/lists/run_info_list.php');
?>

<div class="container-fluid">
	<div class="row">
		<?php
			require_once('templates/shared_layouts/report_progress_bar.php');
		?>

		<!-- update area -->
		<div id="toggle-report-progress-bar" class="col-xs-10 col-sm-10 col-md-10 col-lg-10">
			<form id="add_patient_info" class="form" method="post" class="form-horizontal">
				<fieldset>
					<legend id="new_legend">
						Add Patient Info
					</legend>
					<legend id='update_legend' class="d-none">
						Update Patient Info
					</legend>

					<div class="form-group row">
						<div class="col-xs-12 col-sm-4 col-md-4">
							<label for="first_name" class="form-control-label">First Name: <span class="required-field">*</span></label>
						</div>
						<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
							<input type="text" class="form-control" id="first_name" name="first_name" maxlength="50" value="<?php 
							if(isset($patientArray[0]['first_name']))
							{
								echo $patientArray[0]['first_name'];
							}
						?>" required/>
						</div>
					</div>
					<div class="form-group row">
						<div class="col-xs-12 col-sm-4 col-md-4">
							<label for="middle_name" class="form-control-label">Middle Name: </label>
						</div>
						<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
							<input type="text" class="form-control" id="middle_name" name="middle_name" maxlength="50" value="<?php 
							if(isset($patientArray[0]['middle_name']))
							{
								echo $patientArray[0]['middle_name'];
							}
						?>"/>
						</div>
					</div>
					<div class="form-group row">
						<div class="col-xs-12 col-sm-4 col-md-4">
							<label for="last_name" class="form-control-label">Last Name: <span class="required-field">*</span></label>
						</div>
						<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
							<input type="text" class="form-control" id="last_name" name="last_name" maxlength="50" value="<?php 
							if(isset($patientArray[0]['last_name']))
							{
								echo $patientArray[0]['last_name'];
							}
						?>" required/>
						</div>
					</div>
					<div class="form-group row">
						<div class="col-xs-12 col-sm-4 col-md-4">
							<label for="dob" class="form-control-label">Date of Birth: <span class="required-field">*</span></label>
						</div>
						<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
							<input class="form-control date-picker" type="text" id="dob" name="dob" value="<?php
							if(isset($patientArray[0]['dob']))
							{
								echo $dfs->ChangeDateFormatUS($patientArray[0]['dob'], FALSE); 
							}
							?>" placeholder="MM/DD/YYYY"/>
						</div>
					</div>
					<div class="form-group row">
						<div class="col-xs-12 col-sm-4 col-md-4">
							<label for="sex" class="form-control-label">Sex: <span class="required-field">*</span></label>
						</div>
						<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
							<div class="radio">
								<label class="radio-inline"><input type="radio" name="sex" value="M" <?php
								if(isset($patientArray[0]['sex']) && $patientArray[0]['sex'] == 'M')
								{
									echo 'checked'; 
								}
								?>>Male</label>
							
								<label class="radio-inline"><input type="radio" name="sex" value="F" <?php
								if(isset($patientArray[0]['sex']) && $patientArray[0]['sex'] == 'F')
								{
									echo 'checked'; 
								}
								?>>Female</label>
							</div>
						</div>
					</div>
					<div class="form-group row">
						<div class="col-xs-12 col-sm-4 col-md-4">
							<label for="medical_record_num" class="form-control-label">Medical Record #: <span class="required-field">*</span></label>
						</div>
						<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
							<input type="text" class="form-control" id="medical_record_num" name="medical_record_num" maxlength="50" value="<?php 
							if(isset($patientArray[0]['medical_record_num']))
							{
								echo $patientArray[0]['medical_record_num'];
							}
						?>" required/>
						</div>
					</div>
					<!-- add the common hidden fields for every form -->
					<?php
						require_once('templates/shared_layouts/form_hidden_fields.php')
					?>

					
					<!-- add submit and nav buttons -->
					<?php
						require_once('templates/shared_layouts/form_submit_nav_buttons.php')
					?>
         				
				</fieldset>
			</form>
		</div>
	</div>
</div>