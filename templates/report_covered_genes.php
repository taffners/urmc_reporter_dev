
<div class="container-fluid">
     <div class="row">
          <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
               <a href="?page=add_gene_to_panel&ngs_panel_id=<?= $_GET['ngs_panel_id'];?>" class="btn btn-primary btn-primary-hover add-gene-to-panel">
                    Add New Gene To Panel
               </a> 
          </div>
     </div>
     <div class="row">
          <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 alert alert-info">
               To ensure no reports have genes added to the panel without the knowledge of the director currently writing the report there are two fail safes
               <ul>
                    <li>Any data <b>Uploaded</b> on or after the date gene added will include that gene in the panel of genes.</li>
                    <li>Genes can not be added the same day because it might affect reports currently being written without the director knowing and the panel might not be accurate.</li>
               </ul>
               
          </div>
     </div>
     <div class="row">
          <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
               <table class="formated_table sort_table" >
                    <thead>
                         <th>Gene</th>
                         <th>Exon</th>
                         <th>Accession #</th>
                         <th>Date Gene Added</th>
                         <th>Add to panel</th>
                    </thead>
                    <tbody>
               <?php
               if (isset($covered_genes))
               {
                    foreach($covered_genes as $key => $gene)
                    {
               ?>
                         <tr>
                              <td><?= $gene['gene'];?></td>
                              <td><?= $gene['exon'];?></td>
                              <td><?= $gene['accession_num'];?></td>
                              <td>
                              <?php
                              if ($gene['gene_present_in_panel'] == '1')
                              {
                                   echo $gene['date_gene_added'];
                              }
                              else
                              {
                                   echo 'Currently not in panel '.$gene['date_gene_added'];
                              }
                              ?>
                                   
                              </td>
                              <td>
                              <?php
                              if ($gene['gene_present_in_panel'] == '0')
                              {
                              ?>
                                   <a href="?page=add_gene_to_panel&genes_covered_by_panel_id=<?= $gene['genes_covered_by_panel_id'];?>&ngs_panel_id=<?= $_GET['ngs_panel_id'];?>" class="btn btn-primary btn-primary-hover add-gene-to-panel">
                                        Add to Panel
                                   </a>               
                              <?php
                              }
                              else
                              {
                                   echo 'Currently can not remove genes';
                              }
                              ?>
                              </td>
                         </tr>        
               <?php
                    }
               }
               ?>
                    </tbody>
               </table>
          </div>
     </div>
</div>