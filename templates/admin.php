<?php
     if (isset($user_permssions) && strpos($user_permssions, 'admin') !== false)
     {                        
?>



<div class="container-fluid">
	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			<fieldset>
				<legend class='show'>
					JS Unit Tests (make sure url access_filter=all)
				</legend>
				<div id="qunit"></div>
				<div id="qunit-fixture"></div>
			</fieldset>
		</div>
	</div>
</div>
<?php
}

if (
	isset($user_permssions) && 
		(
			strpos($user_permssions, 'admin') !== false ||
			strpos($user_permssions, 'manager') !== false
		)
	)
{                        

?>
<div class="container-fluid">
	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			<form id="add_new_user" class="form" method="post" class="form-horizontal">
				<fieldset>
					<legend class='show'>
						Add User
					</legend>
					<div class="row alert alert-info">
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
							<ul>
								<li>
									Make sure to enter email address all lowercase.
								</li>
							</ul>
						</div>
					</div>
					<div class="form-group row">
						<div class="col-xs-12 col-sm-4 col-md-4">
							<label for="first_name" class="form-control-label">First Name:</label>
						</div>
						<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
							<input type="text" class="form-control" id="first_name" name="first_name" maxlength="50" required/>
						</div>
					</div>
					<div class="form-group row">
						<div class="col-xs-12 col-sm-4 col-md-4">
							<label for="last_name" class="form-control-label">last Name:</label>
						</div>
						<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
							<input type="text" class="form-control" id="last_name" name="last_name" maxlength="50" required/>
						</div>
					</div>
					<div class="form-group row">
						<div class="col-xs-12 col-sm-4 col-md-4">
							<label for="email_address" class="form-control-label">email address:</label>
						</div>
						<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
							<input type="text" class="form-control" id="email_address" name="email_address" maxlength="40" required/>
						</div>
					</div>
					<div class="form-group row">
						<div class="col-xs-12 col-sm-4 col-md-4">
							<label for="credentials" class="form-control-label">credentials:</label>
						</div>
						<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
							<input type="text" class="form-control" id="credentials" name="credentials" maxlength="255"/>
						</div>
					</div>
					<div class="form-group row">
						<div class="col-xs-12 col-sm-4 col-md-4">
							<label for="title" class="form-control-label">title:</label>
						</div>
						<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
							<input type="text" class="form-control" id="title" name="title" maxlength="50"/>
						</div>
					</div>					
					<div class="row">	
						<div class="col-xs-12 col-sm-4 col-md-4">
							<label class="form-control-label">CAPTCHA:</label>
						</div>

						<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
							<img src="<?= $src_captcha; ?>"/>	
						</div>	
					</div>	
					<div class="form-group row">
						<div class="col-xs-12 col-sm-4 col-md-4">
							<label for="captcha" class="form-control-label">Type CAPTCHA text here:</label>
						</div>
						<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
							<input type="text" class="form-control" id="captcha" name="captcha" maxlength="8" required/>
						</div>
					</div>	
				     <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
						<button type="submit" name="add_user_submit" value="Submit"  class="btn btn-primary btn-primary-hover">
							Add User
						</button>
					</div>	
				</fieldset>
			</form>
		</div>
	</div>
</div>

<?php
}

if (
	isset($user_permssions) && 
		(
			strpos($user_permssions, 'admin') !== false 
		)
	)
{                        

?>

<div class="container-fluid">
	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			<fieldset>
				<legend class='show'>
					Send all active users an email
				</legend>
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
						<?= $active_users_email_addresses;?>
					</div>
				</div>
				<form class="form" method="post" class="form-horizontal">
					<div class="form-group row">
						<div class="col-xs-12 col-sm-4 col-md-4">
							<label for="email_subject_line" class="form-control-label">Email Subject Line: <span class="required-field">*</span></label>
						</div>
						<div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
							<input type="text" class="form-control" id="email_subject_line" name="email_subject_line" maxlength="50" required="required" />
						</div>
					</div>
					<div class="form-group row">
						<div class="col-xs-12 col-sm-4 col-md-4">
							<label for="email_contents" class="form-control-label">Email Contents: <span class="required-field">*</span></label>
						</div>

						<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
							<textarea class="form-control" name="email_contents" rows="4" maxlength="65535" style="margin-bottom: 10px;" required></textarea>
						</div>
					</div>

					<div class="form-group row">
						<div class="offset-xs-4 offset-sm-4 offset-md-4 offset-lg-4 col-xs-2 col-sm-2 col-md-2 col-lg-2">
							<button type="submit" id="active_user_email_submit" name="active_user_email_submit" value="Submit"  class="btn btn-primary btn-primary-hover">
								Submit
							</button>
						</div>
					</div>	
				</form>
			</fieldset>
		</div>
	</div>
</div>

<?php
}

if (
	isset($user_permssions) && 
		(
			strpos($user_permssions, 'admin') !== false ||
			strpos($user_permssions, 'manager') !== false
		)
	)
{                        

?>
<div class="container-fluid">
	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			<fieldset>
				<legend class='show'>
					Unlock user Account
				</legend>

<?php
	if (isset($user_list) && isset($reporter_permission_list))
	{
?>
				<div class="row alert alert-info">
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
						<ul>
							<li>
								<b>Unlock a users account</b>: Use the <button class="btn btn-primary">Unlock</button> button to unlock the users account for <?= SITE_TITLE;?>.  The user will be sent a new temporary password.  Check back within 24 hours to make sure they reset their password.  If they did not lock their account.  It is a security risk to keep their account in this status.
							</li>
							<li>
								<b>Lock a users account</b>: Use the <button type="button" class="btn btn-primary"><span class="fas fa-lock"></span>
								</button> button is used to lock users out of <?= SITE_TITLE;?>.  This is useful when the user leaves the university or no longer needs access to <?= SITE_TITLE;?>.  It is also useful for resetting the users password.  Refer to the next list item for more info on this. 
							</li>
							<li>
								<b>Reset a users password</b>: Press <button type="button" class="btn btn-primary"><span class="fas fa-lock"></span>
								</button> and then press <button class="btn btn-primary">Unlock</button>.  The user will be sent a new temporary password.  Check back within 24 hours to make sure they reset their password.  If they did not lock their account.  It is a security risk to keep their account in this status.
							</li>
							<li>
								<b>User locked their account temporarily because they had too many failed logins</b>: It is best to just wait 30 mins.  The account will automatically unlock. If they are in a rush a database professional can login to the database and delete login in attempts from the database.  This is risky and should only be done by someone that is very confident in SQL after backing up the database.
							</li>
		<?php
			// Add a list including descriptions  of each permission
			foreach($reporter_permission_list as $key => $permission)
			{ 
		?>
							<li>
								<b><?= $permission['permission'];?></b>: <?= $permission['description'];?>
							<?php
							if (isset($user_permssions) && strpos($user_permssions, 'admin') !== false)
							{   
							?>                     
								<a href="?page=update_permission_description&permission_id=<?= $permission['permission_id'];?>" class="btn btn-primary">
									Update <?= $permission['permission'];?> description
	                            </a>
	                        <?php
	                    	}
	                        ?>
							</li>
		<?php
			}
		?>
						</ul>
					</div>
				</div>		
				<table class="formated_table sort_table" style="margin-bottom:20px;">
					<thead>
						<th>User Name</th>
						<th>Email Address</th>
						<th>Unlock Account</th>
						<th>Account Status</th>
						<th>Lock Account</th>
			<!-- Add a new header column for each reporter_permission_list -->
		<?php
			foreach($reporter_permission_list as $key => $permission)
			{ 
		?>

						<th>
							<?= $permission['permission'];?> 
						</th>
		<?php
			}
		?>
					</thead>
					<tbody>
<?php
			foreach ($user_list as $key => $ea_user)
			{				
				// find user permission
				$this_user_permission = $db->listAll('user-permissions', $ea_user['user_id']);
				if (empty($this_user_permission))
				{
					$this_user_permission = '';
				}
				else
				{
					$this_user_permission = $this_user_permission[0]['permissions'];
				}
?>
						<tr>
							<td><?= $ea_user['user_name'];?></td>
							<?=$utils->toggleMoreLess($ea_user['email_address'], 'email_address', $key);?>
							<td><button class="unlock-account btn btn-primary" data-email-address="<?= $ea_user['email_address'];?>">Unlock</button></td>
							<td><?php
							if ($ea_user['password_need_reset'] == 0)
							{
								echo 'unlocked';
							}
							else if ($ea_user['password_need_reset'] == 1)
							{
								echo 'pass reset';
							}
							else if ($ea_user['password_need_reset'] == 2)
							{
								echo '<span class="fas fa-lock"></span>';
							}
							?></td>
							<td><button class="lock-account btn btn-primary" data-email-address="<?= $ea_user['email_address'];?>"><span class="fas fa-lock"></span></button></td>							
							<!-- Add Permission -->
			<?php
				foreach($reporter_permission_list as $key => $permission)
				{ 
			?>
							<td><button  class="add-permission btn <?php
								if (isset($this_user_permission) && strpos($this_user_permission, $permission['permission']) !== false)
								{
									echo 'btn-success';
								}
								else 
								{
									echo 'btn-danger';
								}
								?>" data-user-id="<?= $ea_user['user_id'];?>" data-permission="<?=$permission['permission'];?>" data-curr-permissions="<?= $this_user_permission;?>"><span class="fas fa-plus"></span> <?=$permission['permission'];?></button>
							</td>
			<?php
				}
			?>							
						</tr>
<?php
			}
?>
					</tbody>
				</table>
<?php
	}
?>
			</fieldset>	
		</div>


	</div>

<div class="container-fluid">
	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			<fieldset>
				<legend class='show'>
					Access Summary
				</legend>	

				<div class="row">
					<div class="col-xs-6 col-sm-4 col-md-2 col-lg-12">
						<a href="?page=admin&access_filter=all" class="btn btn-primary">
							All Time                                       
                              </a>
					</div>
					<div class="col-xs-6 col-sm-4 col-md-2 col-lg-12">
						<a href="?page=admin&access_filter=month" class="btn btn-primary">
							Last Month                                     
                              </a>
					</div>
					<div class="col-xs-6 col-sm-4 col-md-2 col-lg-12">
						<a href="?page=admin&access_filter=seven_days" class="btn btn-primary">
							Last 7 Days                                       
                              </a>
					</div>
					<div class="col-xs-6 col-sm-4 col-md-2 col-lg-12">
						<a href="?page=admin&access_filter=today" class="btn btn-primary">
							Today                                    
                              </a>
					</div>
					<div class="col-xs-6 col-sm-4 col-md-2 col-lg-12">
						<a href="?page=admin&access_filter=half_hour" class="btn btn-primary">
							Last Half an Hour                                    
                              </a>
					</div>
				</div>
			
    				<div id="login_details" style="width: 900px; height: 900px;"></div>
<!--     				<div id="time_access_bar_chart" style="width: 900px; height: 500px;"></div> -->

    			</fieldset>
    		</div>
    	</div>
</div>
<div class="container-fluid">
	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			<fieldset>
				<legend class='show'>
					Check APIs that <?= SITE_TITLE;?> uses
				</legend>
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
						<a href="?page=check_thermo_apis" class="btn btn-primary btn-primary-hover" role="button" alt="Do a through check of thermofisher APIs" title="Do a through check of thermofisher APIs.  This is useful if the thermofisher APIs were updated or for routine checking.">Check Thermofisher APIs that <?= SITE_TITLE;?> uses</a>
						
					</div>
				</div>
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
						<a href="?page=check_apis" class="btn btn-primary btn-primary-hover" role="button" alt="Do a through check of thermofisher APIs" title="Do a through check of all API's except Thermofisher API's.">Check All other APIs that <?= SITE_TITLE;?> uses</a>
					</div>
				</div>
			</fieldset>
		</div>
	</div>
</div>
<?php
}

if (
	isset($user_permssions) && 
		(
			strpos($user_permssions, 'admin') !== false 
		)
	)
{                        

?>

	<div class="row">
		<strong>Make a place to add new users including a random password and email the user</strong>
	</div>

	<div class="row">
		<strong>Make user logins have status.  This will be useful for Yi user and any other account which is deactivated.  Maybe use password reset.</strong>
		
	</div>		
	<div class="row">
			<strong>Delete notes to application developer</strong><br>
			DELETE FROM notes_table WHERE note_id = 11;
		
	</div>	
			
	<div class="row">
		<strong>Check server status</strong><br>
		<a href="https://www.tecmint.com/check-apache-httpd-status-and-uptime-in-linux/">check-apache</a>
		systemctl status httpd
	</div>

	<a href="https://blogs.oracle.com/pranav/how-to-find-out-cpu-utilization-in-linux">Check CPU utilization</a>

	<ul>
		reboot
		<li>sudo systemctl restart httpd</li>
		<li>sudo reboot</li>
		<li>mount -a (if storage fs-ngs is not mounted)</li>
	</ul>


	mysql -u anonymous -h ensembldb.ensembl.org<br>

	mysql> use homo_sapiens_core_75_37;<br>
	https://www.biostars.org/p/106470/<br>

	mysql  --user=genome --host=genome-mysql.cse.ucsc.edu -A -D hg19
mysql> select distinct G.gene,N.value from ensGtp as G, ensemblToGeneName as N where 
  G.transcript=N.name and
  G.gene in ("ENSG00000197021", "ENSG00000204379") ;
</div>




<div class="container-fluid">
	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			<fieldset>
				<legend class='show'>
					Update Knowledge base
				</legend>
				<div class="offset-xs-4 offset-sm-4 offset-md-4 offset-lg-4 col-xs-2 col-sm-2 col-md-2 col-lg-2">
					<a href="?page=update_knowledge_base" class="btn btn-primary btn-primary-hover" role="button" style="margin-left:20px;">
						Update Knowledge base with cosmic data
					</a>
				</div>
			</fieldset>
		</div>
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			<fieldset>
				<legend class='show'>
					Delete Knowledge base
				</legend>
				<div class="offset-xs-4 offset-sm-4 offset-md-4 offset-lg-4 col-xs-2 col-sm-2 col-md-2 col-lg-2">
					<a href="?page=delete_gene_knowledge_base" class="btn btn-warning btn-warning-hover" role="button" style="margin-left:20px;">
						Delete All SNVs for Genes From Database
					</a>
				</div>
			</fieldset>
		</div>
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			<fieldset>
				<legend class='show'>
					Find Panel Info
				</legend>
				<div class="offset-xs-4 offset-sm-4 offset-md-4 offset-lg-4 col-xs-2 col-sm-2 col-md-2 col-lg-2">
					<a href="?page=panel_info&panel=ofa" class="btn btn-primary btn-primary-hover" role="button" style="margin-left:20px;">
						OFA
					</a>
				</div>
			</fieldset>
		</div>
	</div>
</div>

<?php
}
?>

