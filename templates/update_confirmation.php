
<div class="container-fluid">			
	<fieldset>
		<legend>
			<?= $page_title;?>
		</legend>
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 alert alert-info">
				<strong>Current Variant Confirmation:</strong>

				<ul>
					<li>Gene: <?= $confirmation_array[0]['genes'];?></li>
					<li>Coding: <?= $confirmation_array[0]['coding'];?></li>
					<li>amino_acid_change: <?= $confirmation_array[0]['amino_acid_change'];?></li>
					<li>Sample: <?= $confirmation_array[0]['sample_name'];?></li>
					<li>Test: <?= $confirmation_array[0]['test_name'];?></li>
					<li>Message: <?= nl2br($confirmation_array[0]['message']);?></li>
					
				</ul>

			</div>
		</div>
		
		<form class="form" method="post" class="form-horizontal" style="margin-top:10px;">

			<div class="form-group row">
				<div class="col-xs-12 col-sm-4 col-md-4">
					<label for="sample-type-toggle" class="form-control-label">Confirmation Status: <span class="required-field">*</span></label>
				</div>
				<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
					<div class="radio">
						<label class="radio-inline"><input type="radio" name="status" value="completed" required="required" checked>Completed</label>
					
						<label class="radio-inline"><input type="radio" name="status" value="pending">pending</label>
					</div>
				</div>
			</div>


			<div class="form-group row">
				<div class="col-xs-12 col-sm-4 col-md-4">
					<label for="sample-type-toggle" class="form-control-label">Outcome: <span class="required-field">*</span></label>
				</div>
				<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
					<div class="radio">
						<label class="radio-inline"><input type="radio" name="outcome" value="confirmed" required="required" <?= $utils->DefaultRadioBtn($confirmation_array, 'outcome', 'confirmed');?>>Confirmed</label>
					
						<label class="radio-inline"><input type="radio" name="outcome" value="not confirmed"  <?= $utils->GetValueForUpdateRadioCBX($confirmation_array, 'outcome', 'not confirmed');?>>Not Confirmed</label>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
					The following message will be sent to <?= $confirmation_array[0]['confirmation_requester_name'];?>.  <h3>Please make sure the confirmation result is correct before sending the message and add any additional info here.</h3>
				</div>
			</div>
			<div class="form-group">
				<textarea class="form-control" name="message" rows="20"  maxlength="65535">Hello <?= $confirmation_array[0]['confirmation_requester_name'];?>,

I finished confirming the following variant.  

CONFIRMATION RESULT: <?php
if (!isset($confirmation_array[0]['outcome']) || empty($confirmation_array[0]['outcome']) || $confirmation_array[0]['outcome'] === 'confirmed')
{
	echo 'confirmed';
}
else
{
	echo 'not confirmed';
}
?>

Gene: <?=$confirmation_array[0]['genes'];?> 
Coding: <?=$confirmation_array[0]['coding'];?> 
Amino Acid Change: <?=$confirmation_array[0]['amino_acid_change'];?> 

mol#: <?= $confirmation_array[0]['order_num'];?> 
MD#: <?= $confirmation_array[0]['mol_num'];?> 
Soft Lab #: <?= $confirmation_array[0]['soft_lab_num'];?> 

report #: <?=$confirmation_array[0]['visit_id'];?>

Test: <?= $confirmation_array[0]['test_name'];?>


Thank you								
<?= $_SESSION['user']['first_name'];?> <?= $_SESSION['user']['last_name'];?>

				</textarea>
			</div>

			<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
				<button type="submit" name="<?= $page;?>_submit" value="Submit"  class="btn btn-primary btn-primary-hover">
					Submit
				</button>
			</div>
		</form>	
	
		
	</fieldset>
</div>


