<div class="container-fluid">

	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="height:500px;">

			<form class="form" method="post" class="form-horizontal">
				<fieldset>
					<legend class='show'>
						<?= $page_title;?>
					</legend>
					<?php					
						require_once('templates/shared_layouts/sample_overview.php');
					?>
					<div id="form-control-fields">
						<div class="form-group row">
							<div class="col-xs-12 col-sm-4 col-md-4">
								<label for="reflex_id" class="form-control-label">Reflex Tracker: <span class="required-field">*</span></label>
							</div>
							<div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">
								
								<select class="selectpicker" data-live-search="true" data-width="100%" name="reflex_id" id="reflex_id" required="required">

									<option value="" selected="selected"></option>
							<?php
								for($i=0;$i<sizeof($allReflexsAvailable);$i++)
		               			{			              
							?>
									<option value="<?= $allReflexsAvailable[$i]['reflex_id']; ?>">
										<?= $allReflexsAvailable[$i]['reflex_name']; ?> 
									</option>
							<?php
									
								}
							?>
								</select>								
							</div>
						</div>
					</div>

					
	
					<div class="form-group row">
						<div class="offset-xs-4 offset-sm-4 offset-md-4 offset-lg-4 col-xs-2 col-sm-2 col-md-2 col-lg-2">
							<button type="submit" id="<?= $page;?>_submit" name="<?= $page;?>_submit" value="Submit"  class="btn btn-primary btn-primary-hover">
								Submit
							</button>
						</div>
					</div>
				</fieldset>
			</form>
		</div>
	</div>
</div>
