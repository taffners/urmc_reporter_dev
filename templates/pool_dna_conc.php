<div class="container-fluid">
	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			<?php
			     require_once('templates/tables/pending_ngs_library_pool_table.php');
			?>
		</div>
	</div>
	<div class="row">
<?php
if (isset($_GET['pool_chip_linker_id']) && isset($poolStepTracker) && isset($_GET['ngs_panel_id']))
{

?>
	<!-- report progress bar toggles -->	
	<?php
	require_once('templates/shared_layouts/report_toggle_show_status.php');
	?>

	<!-- Progress bar -->
	<?php
	require_once('templates/shared_layouts/pool_step_nav_bar.php')
	?>
	<!-- update area -->
	<div id="toggle-report-progress-bar" class="col-xs-10 col-sm-10 col-md-10 col-lg-10">
		<form id="add_pool_conc" class="form" method="post" class="form-horizontal">
			<fieldset>

				<legend>
					<?= $utils->UnderscoreCaseToHumanReadable($page);?>
				</legend>

				<div class="alert alert-info">
					<a role="button" class="btn btn-primary btn-primary-hover" href="?page=training&amp;training_step=FAQ#NGS_delete_pending_tests">
						How do I cancel pending pre NGS samples and remove the test from this pool?
					</a>
				</div>

<?php
			// find if DNA_CONC_Instrument step is turned on for this panel.  $onPanelStepRegulators is made in config
			if 	(
					isset($onPanelStepRegulators[0]['on_panel_steps'])&&
					strpos($onPanelStepRegulators[0]['on_panel_steps'],'DNA_CONC_Instrument') !== False && 
					isset($dna_conc_field_controller[0]['field_controller']) &&
					(
						$page === 'pool_dna_conc'
					)

				)
			{
?>
				<div class="form-group row">
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

						<div id="dna_conc_control_quant_label" class="col-xs-12 col-sm-4 col-md-4">
							<label for="dna_conc_control_quant" class="form-control-label"><?= $dna_conc_field_controller[0]['field_controller'];?> Control: <span class="required-field">*</span></label>
						</div>
											
						<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
							<input type="number" class="form-control capitalize" id="dna_conc_control_quant" name="dna_conc_control_quant" step=".01" required="required" value="<?= $utils->GetValueForUpdateInput($pool_dna_conc_control_quant, 'dna_conc_control_quant');?>" />
						</div>
					</div>
				</div>

<?php
			}

?>
				<div class="row">
					<h3 class="raised-card-subheader">Visits in Library Pool</h3>

					<table class="formated_table">
						<thead>
							<th>Reporter#</th>
							<th>Order #</th>
	<?php
		if ($_GET['ngs_panel_id'] === '1')
		{	
	?>
							<th>Chip / Flow Cell</th>
	<?php
		}
	?>						
							
							<th>Sample Name</th>
							<th>Patient Name</th>
							<th>MRN</th>
							<th>DNA Concentration ng/&mu;l</th>
							<th>DNA Conc. Status</th>
						</thead>
						<tbody>
		<?php
			if (isset($visits_in_pool))
			{
				foreach ($visits_in_pool as $key => $visit)
				{		
					// if chip B does not have any data to Union on there will
					// be a null entry.
					if (empty($visit['visit_id']))
					{
						break;
					}

					// Find if sample is a control or not
					if ($visit['sample_type'] === 'Positive Control' || $visit['sample_type'] === 'Negative Control')
					{
						$required = '';
						$requried_span = '';
					}
					else
					{
						$required = 'required="required"';
						$requried_span = '<span style="font-size:20px;" class="required-field">*</span>';
					}

					// find page data since this page works for pool_amp_conc and pool_dna_conc
					if ($_GET['page'] === 'pool_dna_conc')
					{
						$conc = $visit['dna_conc'];
						$dna_status = $visit['dna_status'];
					}
					else if ($_GET['page'] === 'pool_amp_conc')
					{
						$conc = $visit['amp_conc'];
						$dna_status = $visit['amp_status'];
					}
					
		?>
							<tr>
								<td>
									<?= $visit['visit_id'];?>
									<input class="d-none" type="number" name="visit_<?=$key;?>[visit_id]" value="<?= $visit['visit_id'];?>" style="width:45px;"/>
									<input class="d-none" type="number" name="visit_<?=$key;?>[pre_step_visit_id]" value="<?= $visit[$_GET['page'].'_id'];?>" style="width:45px;"/>
								</td>
								<td><?= $visit['order_num'];?></td>
	<?php
		if ($_GET['ngs_panel_id'] === '1')
		{	
	?>
								<td><?= $visit['chip_flow_cell'];?></td>
	<?php
		}
	?>						
								<td><?= $visit['sample_name'];?></td>
								<td><?= $visit['patient_name'];?></td>
								<td><?= $visit['medical_record_num'];?></td>
								<td>
									<?=$requried_span;?> <input type="number" step="0.01" name="visit_<?=$key;?>[conc]" value="<?= $conc;?>" style="width:65px;" <?=$required;?>/>
								</td>
								<td>
									<?=$requried_span;?> <label class="radio-inline"><input type="radio" name="visit_<?=$key;?>[dna_status]" <?=$required;?> value="passed"<?php
									if ($dna_status === 'passed')
									{
										echo 'checked';
									}
									// make default passed checked
									else if ($dna_status != 'Flagged')
									{
										echo 'checked';
									}
									?> >Passed</label>
									<label class="radio-inline"><input type="radio" name="visit_<?=$key;?>[dna_status]" value="Flagged" <?php
									if ($dna_status === 'Flagged')
									{
										echo 'checked';
									}

									?>>Flagged</label>

								</td>
							</tr>
		<?php
				}
			}
		?>			
						</tbody>
					</table>
				</div>
			<?php
				// add form submit, previous, and next buttons.
				require_once('templates/shared_layouts/form_pool_submit_nav_buttons.php');
			?>

			</fieldset>
		</form>
	</div>


<?php
}
?>
	

	</div>
</div>