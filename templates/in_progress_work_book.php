<div class="container-fluid">
	<div class="row">
		
<?php
	if (isset($inProgressTests) && !empty($inProgressTests))
	{
?>		
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			<form id="filter-tests" class="form" method="post" class="form-horizontal">
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
						<label for="orderable_tests_id" class="form-control-label">Test: <span class="required-field">*</span></label>
					</div>
					<div class="col-xs-12 col-sm-8 col-md-6 col-lg-6">
						
						<select class="selectpicker" data-live-search="true" data-width="100%" name="orderable_tests_id" id="orderable_tests_id" required="required">

							<option value="" selected="selected"></option>
							<option value="all">All Tests</option>
						<?php
							foreach($inProgressTests as $key => $samp)
							{
						?>
									<option value="<?= $samp['orderable_tests_id']; ?>">
											<?= $samp['test_name']; ?> 
									</option>
						<?php
							}
						?>
						</select>
						
					</div>
					<div class="col-xs-2 col-sm-2 col-md-1 col-lg-1"style="padding-right:0px;padding-left:0px;">
						<button type="submit" name="filter-pending-tests-submit" class="btn btn-primary btn-primary-hover" value="" aria-label="filter pending tests">
							<span class="fas fa-search" style="margin-top:2px;margin-bottom:2px;"></span>
						</button>
					</div>
				</div>
			</form>
		</div>

<?php
	}
?>
	</div>
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		<hr class="plain-section-mark">
	</div>
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
<?php
	if (isset($_GET['orderable_tests_id']) && isset($inProgressArray[0]['test_name']))
	{
?>
		<h3><?= $inProgressArray[0]['test_name'];?> In Progress Tests</h3>
<?php
	}
	else
	{
?>
		<h3>All In Progress Tests</h3>
<?php
	}
?>
	</div>
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 mb-5">
		<hr class="plain-section-mark">
	</div>

<?php
if (isset($inProgressArray) && !empty($inProgressArray))
{
?>

	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
			<table class="formated_table">
				<thead>
					<th>#</th>					
					<th>MD#</th>
					<th>mol#</th>
					<th>soft lab#</th>
					<th>MRN#</th>                                         
					<th>Patient Name</th>
                </thead>
                <tbody>
    <?php
    	foreach ($inProgressArray as $key => $curr_test)
    	{
   	?>
   					<tr>
   						<td><?= $curr_test['visit_id'];?></td>   						
   						<td><?= $curr_test['mol_num'];?></td>
   						<td><?= $curr_test['order_num'];?></td>
   						<td><?= $curr_test['soft_lab_num'];?></td>
   						<td><?= $curr_test['medical_record_num'];?></td>
   						<td><?= $curr_test['first_name'];?><br><?= $curr_test['last_name'];?></td>
   					</tr>
   	<?php
    	}
    ?>
                </tbody>
            </table>
        </div>
    </div>

<?php
	}
?>





	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
<?php
	if (isset($inProgressArray) && !empty($inProgressArray))
	{
		foreach($inProgressArray as $key => $samp)
		{
?>
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
					Sample ID: <b><u><?= $samp['sample_log_book_id'];?></u></b> NGS Visit ID: <b><u><?= $samp['visit_id'];?></u></b> <?= $samp['patient_initials'];?> (<?= $samp['mol_num'];?>, <?= $samp['order_num'];?>, <?= $samp['medical_record_num'];?>)
				</div>
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
					-----
				</div>

				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
					<b>Detected Alterations of Known or Potential Pathogenicity</b>
					<br>
					<br>
					<br>
					<br>
					<br>
				</div>
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
					<b>Interpretation of Results:</b>
					<br>
					<br>
					<br>
					<br>
					<br>
				</div>

				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 mb-5">

					<hr class="plain-section-mark">
				</div>
			</div>
<?php
		}
	}
?>

		</div>
	</div>
</div>


