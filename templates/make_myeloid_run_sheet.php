
<div class="container-fluid">
<!-- 	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			<fieldset>
				<legend class='show'>
					General Info
				</legend>		
				<div class="form-group row">
					<div class="col-xs-12 col-sm-4 col-md-4">
						<label for="library_prep_date" class="form-control-label">Library Prep Date</label>
					</div>
					<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
						<input type="date" class="form-control date-picker" id="library_prep_date" name="library_prep_date" placeholder="MM/DD/YYYY" pattern="\d{1,2}/\d{1,2}/\d{4}"  
					/>
					</div>
				</div>
				<div class="form-group row">
					<div class="col-xs-12 col-sm-4 col-md-4">
						<label for="ngs_run_date" class="form-control-label">NGS Run Date</label>
					</div>
					<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
						<input type="date" class="form-control date-picker" id="ngs_run_date" name="ngs_run_date" placeholder="MM/DD/YYYY" pattern="\d{1,2}/\d{1,2}/\d{4}"  
					/>
					</div>
				</div>
				<div class="form-group row">
					<div class="col-xs-12 col-sm-4 col-md-4">
						<label for="version" class="form-control-label">Version</label>
					</div>
					<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
						<input type="text" class="form-control date-picker" id="version" name="version""  
					/>
					</div>
				</div>
			</fieldset>

		</div>		
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			<fieldset>
				<legend class='show'>
					Choose Samples
				</legend>
			     <div class="row">
			          <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			               <button id="add_row" type="button"class="btn btn-success btn-success-hover pull-right">
			                    Add Row
			               </a>
			          </div>
			     </div>
<?php
	if (isset($pending_pre_runs))
	{
?>				
				<table class="formated_table sort_table dataTable">
					<thead>
						<th>Include</th>		                    
		                    <th>MD# / soft lab#</th>                        
		               	<th>Patient Name</th>
		               	<th>Qubit</th>
		               	<th>Dil total vol</th>
		               	<th>DNA vol</th>
		               	<th>AE vol</th>
		               	<th>Library Tube</th>
		               	<th>Index 1 i7</th>
		               	<th>Index 2 i5</th>
					</thead>
					<tbody id="make_run_sheet_tbody" >
<?php

		foreach($pending_pre_runs as $key => $sample)
		{				
?>
						<tr row_id="<?= $sample['visit_id']; ?>">
							<td>
								<input id="cbx_<?= $sample['visit_id']; ?>" type="checkbox" class="add_sample_to_run_sheet" value="1"/>
							</td>
							<td><?= $sample['mol_num_soft_lab']; ?></td>

							<td><?= $sample['patient_name']; ?></td>
							<td id="qubit_<?= $sample['visit_id']; ?>" col_name="qubit"><?= $sample['qubit']; ?></td>
							<td>
<?php
					if ($sample['qubit'] > 200)
					{
						$total_vol = 150;
					}
					else if ($sample['qubit'] < 50)
					{
						$total_vol = 40;
					} 
					else
					{
						$total_vol = 100;
					}

					$DNA_vol = round((5 * $total_vol ) / $sample['qubit'], 1);
					$AE_vol = round($total_vol - $DNA_vol, 1);
?>								
								<div class="row_data" edit_type="click" col_name="total_vol" id="total_vol_<?= $sample['visit_id']; ?>">
									
									<?= $total_vol;?>
								</div>
							</td>
							<td id="DNA_vol_<?= $sample['visit_id']; ?>"><?= $DNA_vol;?></td>
							<td id="AE_vol_<?= $sample['visit_id']; ?>"><?= $AE_vol;?></td>
							<td id="lib_tube_<?= $sample['visit_id']; ?>"></td>
							<td>									<div class="row_data" edit_type="click" col_name="index_1_i7">
									A7
								</div>
							</td>
							<td>									<div class="row_data" edit_type="click" col_name="index_2_i5">
									A5
								</div>
							</td>						
						</tr>
<?php				
			
		}
?>
					</tbody>

				</table>
<?php
	}
?>				
			</fieldset>


		     <div class="row">
		          <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		               <button id="submit_and_download" type="button"class="btn-lg btn-success btn-success-hover pull-right">
		                    Submit and Download
		               </a>
		          </div>
		     </div> -->

		     <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		     	<div class="alert alert-info">
		     		<h1>This method is being phased out and being replaced by a Library Pool.</h1> 
		     		<br><br>
		     		<strong>STEPS:</strong>
		     		<ol>
		     			<li>Add all samples including any positive and negative controls as a separate visit. <strong>DO NOT ADD NTC.  THIS WILL BE ADDED AUTOMATICALLY.</strong></li>
		     			<li>Make a Library Pool.<br><img src="images/make_library_pool.png"><br></li>
		     			<li>After the Library Pool is made a new entry will be added to Pre NGS Library Pools table on the home page.  Click on the new library pool.</li>
		     			<li>The Library Pool will make a lot of steps easier and less error prone.  Also the worksheet is can now be updated.</li>
		     			<li>Use the Library Pool to add both the DNA and Amp concentrations.</li>
		     			<li>The Library Pool will also make the run template for the MiSeq.  Eventually it will be automatically added to the M drive once it is tested.</li>
		     		</ol>
		     	</div>
		     </div>

		     <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				<fieldset>
					<legend class='show'>
						Most Recent Run Sheets
					</legend>

<?php
	if (isset($previous_run_sheets))
	{
?>
					<ul>
<?php
		foreach ($previous_run_sheets as $key => $sheet)
		{
?>
						<li>
							<a href="?page=view_run_sheet&run_sheet_general_id=<?= $sheet['run_sheet_general_id'];?>"><?= $sheet['ngs_run_date'];?>_<?= $sheet['sheet_panel_type'];?>_<?= $sheet['ngs_run_date'];?><?php
					
							if (!empty($sheet['version']))
							{

							 	echo '_'.$sheet['version'];
							}
							?></a>
							
						</li>	
<?php
		}
?>
					</ul>
<?php
	}
?>

				</fieldset>
			</div>


		     
		</div>	
	</div>
</div>