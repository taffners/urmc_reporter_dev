<div class="container-fluid">
     <fieldset>
         
          <legend>All QC Checklist Items</legend>
         

          <div class="row" style="overflow-x:auto;">
               <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <table class="formated_table sort_table_no_inital_sort_no_paging">
                         <thead>
                              <th>Module Name</th>
                              <th>Module Diagram</th>
                              
                              <th>Module Description</th>
                              <th>Update Module</th>
                              

                         </thead>
                         <tbody>

                         <!-- iterate over all pending runs and add each entry as a new row in the table-->

               <?php

                    for($i=0;$i<sizeof($all_modules);$i++)
                    {    
               ?>
                              <tr>
                                  <td><?= $all_modules[$i]['module_name'];?></td>
                                  <td><?= $all_modules[$i]['module_diagram'];?></td>
                                  <td><?= $all_modules[$i]['module_description'];?></td>
                                  
                                  <td><a href="?page=add_module&module_id=<?= $all_modules[$i]['module_id'];?>" class="btn btn-primary btn-primary-hover" role="button">Update Module</a></td>
                              </tr>
               <?php 
                    }
               ?>
                         </tbody>
                    </table>
               </div>
          </div>
     </fieldset>
</div>
