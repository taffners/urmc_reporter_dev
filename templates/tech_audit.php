<div class="container-fluid">
	<div class="row d-print-none">
		<div class="col-xs-2 col-sm-2 col-md-1 col-lg-1">
			<a href="?page=tech_audit&time_frame=all" type="button" class="<?php
				if (
					(isset($_GET['time_frame']) && $_GET['time_frame'] == 'all') ||
					(!isset($_GET['time_frame']))
				   )				
				{
					echo 'btn btn-success btn-success-hover';
				}
				else
				{
					echo 'btn btn-primary btn-primary-hover';
				}
			?>">
                    All
               </a>
          </div>
          <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
			<a href="?page=tech_audit&time_frame=6_months" type="button" class="<?php
				if (isset($_GET['time_frame']) && $_GET['time_frame'] == '6_months')
				{
					echo 'btn btn-success btn-success-hover';
				}
				else
				{
					echo 'btn btn-primary btn-primary-hover';
				}
			?>">
                    Last 6 Months
               </a>
          </div>
          <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
			<a href="?page=tech_audit&time_frame=3_months" type="button" class="<?php
				if (isset($_GET['time_frame']) && $_GET['time_frame'] == '3_months')
				{
					echo 'btn btn-success btn-success-hover';
				}
				else
				{
					echo 'btn btn-primary btn-primary-hover';
				}
			?>">
                    Last 3 Months
               </a>
          </div>
          <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
			<a href="?page=tech_audit&time_frame=1_month" type="button" class="<?php
				if (isset($_GET['time_frame']) && $_GET['time_frame'] == '1_month')
				{
					echo 'btn btn-success btn-success-hover';
				}
				else
				{
					echo 'btn btn-primary btn-primary-hover';
				}
			?>">
                    Last Month
               </a>
          </div>
          <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
			<a href="?page=tech_audit&time_frame=7days" type="button" class="<?php
				if (isset($_GET['time_frame']) && $_GET['time_frame'] == '7days')
				{
					echo 'btn btn-success btn-success-hover';
				}
				else
				{
					echo 'btn btn-primary btn-primary-hover';
				}
			?>">
                    7 Days
               </a>
          </div>
	</div>
	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			<h1>Extraction Summary Per Technician</h1>
		</div>
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			<table class="formated_table sort_table">
				<thead>
					<th>Extractor Name</th>
					<th>Avg Purity</th>
					<th>Purity Count</th>
					<th>Min Purity</th>
					<th>Stand Deviation purity</th>
					<th>Avg Stock Conc</th>
					<th>Stock Conc Count</th>
					<th>Total Extractions</th>
				</thead>
				<tbody>

<?php
	foreach ($dna_extraction as $key => $extract)
	{
?>
					<tr>
						<td><?= $extract['extractors_name'];?></td>
						<td><?= $extract['avg_purity'];?></td>
						<td><?= $extract['purity_count'];?></td>
						<td><?= $extract['purity_min'];?></td>
						<td><?= $extract['purity_stddev'];?></td>
						<td><?= $extract['avg_stock_conc'];?></td>
						<td><?= $extract['count_stock_conc'];?></td>
						<td><?= $extract['total_count'];?></td>
					</tr>
<?php
	}

?>


			</table>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			<h1>Tests Performed per Technician History</h1>
		</div>
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			<table class="formated_table sort_table">
				<thead>
					<th>Test Name</th>
					<th>Task</th>
					<th>Technician</th>
					<th>Count</th>
					<th>Average Turnaround Time</th>
				</thead>
				<tbody>
<?php
	foreach($tech_tests as $key => $test)
	{
?>
					<tr>
						<td><?= $test['test_name'];?></td>
						<td><?= $test['task'];?></td>
						<td><?= $test['users_name'];?></td>
						<td><?= $test['count_test_tasks'];?></td>
						<td><?= $test['avg_actual_turnaround_time'];?></td>
					</tr>
<?php
	}
?>
				</tbody>
			</table>
		</div>
	</div>
</div>