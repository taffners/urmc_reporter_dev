<div class="container-fluid">
	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			<fieldset>
				<legend>Controlled in class.api_utils.php [CIViC genes](https://civicdb.org/api/genes/) </legend>

				<h1>If there's a response about BRAF gene below it is working.</h1>
<?php
				echo $API_Utils->searchCivicGene('Braf');
				
?>				


			</fieldset>

		</div>
	</div>
	<!-- <div class="row">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			<fieldset>
				<legend>Controlled in class.api_utils.php [CIViC variants](https://civicdb.org/api/variants)</legend>
				<h1>CURRENLTY NOT BEING USED!!!!!!!!</h1>
			</fieldset>

		</div>
	</div> -->
	<!-- <div class="row">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			<fieldset>
				<legend>Controlled in class.api_utils.php [ensembl](https://rest.ensembl.org/vep/human/region/)</legend>
				<h1>CURRENLTY NOT BEING USED!!!!!!!!</h1>
			</fieldset>

		</div>
	</div> -->


	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			<fieldset>
				<legend>API's running in utils.py [mutalyzer](https://mutalyzer.nl/json/runMutalyzer?variant=)</legend>
				<h1>The presence of a NM_000222.2(KIT_i001):p.(Leu576Pro) below says this API is working</h1>
<?php
				var_dump($API_Utils->findProteinNameMutalyzer('NM_000222.2', 'c.1698_1727delCAATTATGTTTACATAGACCCAACACAACTinsTAATTATGTTTACATAGACCCAACACAACC'));

?>
			</fieldset>

		</div>
	</div>

	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			<fieldset>
				<legend>API's/services also running on the pathlamp server [google charts](https://www.gstatic.com/charts/loader.js)</legend>

				<h1>If a bar graph appears here then it is working.</h1>
				<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
				<div id="dual_x_div" style="width: 900px; height: 500px;"></div>
				<script type="text/javascript">
					google.charts.load('current', {'packages':['bar']});
					google.charts.setOnLoadCallback(drawStuff);

					function drawStuff() 
					{
						var data = new google.visualization.arrayToDataTable(
							[
								['Galaxy', 'Distance', 'Brightness'],
								['Canis Major Dwarf', 8000, 23.3],
								['Sagittarius Dwarf', 24000, 4.5],
								['Ursa Major II Dwarf', 30000, 14.3],
								['Lg. Magellanic Cloud', 50000, 0.9],
								['Bootes I', 60000, 13.1]
							]);

						var options = 
						{
							width: 800,
							chart: 
							{
								title: 'Nearby galaxies',
								subtitle: 'distance on the left, brightness on the right'
							},
							bars: 'horizontal', // Required for Material Bar Charts.
							series: 
							{
								0: { axis: 'distance' }, // Bind series 0 to an axis named 'distance'.
								1: { axis: 'brightness' } // Bind series 1 to an axis named 'brightness'.
							},
							axes: 
							{
								x: 
								{
									distance: {label: 'parsecs'}, // Bottom x-axis.
									brightness: {side: 'top', label: 'apparent magnitude'} // Top x-axis.
								}
							}
						};

						var chart = new google.charts.Bar(document.getElementById('dual_x_div'));
						chart.draw(data, options);
					};

				</script>


			</fieldset>

		</div>
	</div>

	<!-- <div class="row">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			<fieldset>
				<legend>API's/services also running on the pathlamp server [geoplugin](http://www.geoplugin.net/json.gp?ip=)</legend>
<?php
	var_dump($db->IPInfo());
?>
			</fieldset>

		</div>
	</div> -->

	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			<fieldset>
				<legend>Emails are being sent out from the pathlamp server via PHP mail function [PHP Mail](https://www.php.net/manual/en/function.mail.php)</legend>

				<h1>Check Email an email should of been sent to all Admins</h1>

				<?php
				$email_utils->emailAdminsProblemURL('Test Email sending');
				?>

			</fieldset>

		</div>
	</div>
</div>