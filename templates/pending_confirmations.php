
<!-- pending confirmations -->
<div class="container-fluid">
	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		     <fieldset id="outstanding-reports-fieldset">
		     	<legend>Pending Confirmations</legend>
				<?php
					require_once('templates/tables/pending_confirmation_table.php');
				?>
			</fieldset>
			<fieldset id="outstanding-reports-fieldset">
		     	<legend>Failed Confirmations</legend>
		     </fieldset>
			<fieldset id="outstanding-reports-fieldset">
		     	<legend>Confirmation Tally</legend>
		     	<table class="formated_table sort_table">
					<thead>
					<?php
						if (isset($confirmed_count_vw) && !empty($confirmed_count_vw))
						{
							foreach($confirmed_count_vw[0] as $key =>$val)
							{
								if (!$utils->InputAID($key))
								{
					?>
								<th><?= $utils->UnderscoreCaseToHumanReadable($key);?></th>
					<?php
								}
							}
						}
						else
						{
					?>
							There are no past confirmations performed
					<?php		
						}
					
					?>
					</thead>
					<tbody>
					<?php
						if (isset($confirmed_count_vw))
						{
							for($i = 0; $i <sizeOf($confirmed_count_vw); $i++)
							{
					?>
								<tr>
					<?php
								foreach($confirmed_count_vw[$i] as $key => $value)
								{
					?>
									<td><?= $value;?></td>
					<?php
								}
					?>
								</tr>
					<?php
							}
					?>

					<?php
						}
					?>

					</tbody>
				</table>
		     </fieldset>		
		</div>
	</div>
</div>