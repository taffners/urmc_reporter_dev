<div class="container-fluid">
<?php 

	if (isset($all_reflexes_available))
	{		
?>

	<?php
        if (isset($user_permssions) && (strpos($user_permssions, 'admin') !== false || strpos($user_permssions, 'manager') !== false))
        {                        
   	?>
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
				<a href="?page=add_new_orderable_test" class="btn btn-primary btn-primary-hover" title="Use this button to add a new Orderable Single Analyte Test">Add a New Orderable Test</a>
			</div>
			<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
				<a href="?page=add_a_reflex_test_tracker" class="btn btn-primary btn-primary-hover" title="Use this button to add a new Orderable Reflex Test Tracker.  Reflex Test Trackers are not tests but are a way to track when reflexs need to be added to a test.">Add a Reflex Tracker</a>
			</div>
		</div>
	<?php
		}
	?>

		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				<table class="formated_table sort_table" >
					<thead>
						<th>Tracker#</th>
					<?php
			        if (isset($user_permssions) && (strpos($user_permssions, 'admin') !== false || strpos($user_permssions, 'manager') !== false))
			        {                        
				   	?>
						<th>Update Reflex Tracker</th>
					<?php
					}
					?>						
						<th>Reflex Tracker Name</th>
					</thead>
					<tbody>
	<?php

		foreach ($all_reflexes_available as $key => $reflex)
		{	
	?>
						<tr>
							<td><?= $reflex['reflex_id'];?></td>
						<?php
				        if (isset($user_permssions) && (strpos($user_permssions, 'admin') !== false || strpos($user_permssions, 'manager') !== false))
				        {                        
					   	?>
							<td>								
								<a href="?page=add_a_reflex_test_tracker&reflex_id=<?= $reflex['reflex_id'];?>" class="btn btn-primary btn-primary-hover">Update Reflex Tracker</a>			
							</td>
						<?php
						}
						?>
							<td><?= $reflex['reflex_name'];?></td>
							
						
						</tr>
	<?php
		}
	?>
					</tbody>
				</table>
			</div>
		</div>
<?php
	}
?>
</div>