<div class="container-fluid">
	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			<fieldset>
				<legend>Search Results</legend>
				<div class="row">
					<?php

						if (isset($search_result) && !empty($search_result))
						{
					?>
					<div style="overflow-x:auto;">
						
						<table class="formated_table sort_table">
    		 					<thead>
    		 						<th>PDF</th>
								<?php

									$cols = array_keys($search_result[0]);									

				               		foreach ($cols as $key => $col_name)
				               		{

				               			$col_name = str_replace('_status', '', $col_name);
				               			// if the column isn't an id add column as visible otherwise hide column
				               			if (!$utils->InputAID($col_name))
				               			{
				               				// shorten name if include in Report
				               				if ($col_name === 'include_in_report')
				               				{
				               	?>
				               				<th>Included</th>
				               	<?php
				               				}
				               				else
				               				{
				               	?>			
				               				<th><?= $utils->UnderscoreCaseToHumanReadable($col_name);?></th>
				               	<?php
				               				}
				               			}	
				                     	}
				               	?>
    		 					</thead>
    		 					<tbody>

    		 						<?php

    		 							for ($i=0; $i < sizeOf($search_result); $i++)
    		 							{	
    		 						?>
    		 								<tr>
    		 								<?php
    		 									// if this variant was included in a report turn on hovering by adding class hover_success_on (to change color green and in javascript use this class to activate a listener to link to report pdf onclick open a new tab for ?page=generate_report&run_id=1)
    		 									if ($search_result[$i]['include_in_report'] ==='1')
    		 									{
    		 								?>
											<td>
    		 										<a href="?page=download_report&amp;visit_id=<?= $search_result[$i]['visit_id'];?>&amp;patient_id=<?= $search_result[$i]['patient_id'];?>&amp;run_id=<?= $search_result[$i]['run_id'];?>&amp;ngs_panel_id=<?= $search_result[$i]['ngs_panel_id'];?>" type="button" class="btn btn-success btn-success-hover" target="_blank"><img src="images/adobe-pdf-icon.png" alt="pdf icon" style="height:30px;width:30px"></a>


    		 									</td>
    		 								<?php
    		 									}
    		 									else
    		 									{
    		 								?>
    		 										<td>Not Included in Report</td>
    		 								<?php
    		 									}
    		 								?>

    		 						<?php
    		 								foreach ($search_result[$i] as $key => $val)
    		 								{
    		 									if (!$utils->InputAID($key) && $key !== 'include_in_report')
    		 									{
								?>
												<td><?= $val;?></td>
								<?php         		 										
    		 									}
    		 									elseif ($key === 'include_in_report'  && $val === '1')
    		 									{
    		 						?>
    		 										<td>yes</td>
    		 						<?php
    		 									}
    		 									elseif ($key === 'include_in_report'  && $val === '0')
    		 									{
    		 						?>
    		 										<td>no</td>
    		 						<?php
    		 									}   
    		 								}      		 									
    		 						?>		

    		 								</tr>
    		 						<?php
    		 							}
    		 						?>

    		 					</tbody>
    		 				</table>
         		 			
    		 			</div>
					<?php
						}
					?>
				</div>

			</fieldset>
		</div>

		<ul>
			<li>Click on row shows report if included in report</li>
		</ul>
	</div>
</div>