<div class="container">
	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			
			<form name='create_freezer' id="create_freezer" class='form-horizontal' method='post'>
				<fieldset id="search">
					<legend><?= $page_title; ?></legend>
					<div class="row">
						<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
							<label for="test_name" class="form-control-label">
								Test Name:
							</label>
							<input type="text" class="form-control" id="test_name" name="test_name" maxlength="20">
						</div>	
						<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
							<label for="tech" class="form-control-label">
								Technician:
							</label>
							<input type="text" class="form-control" id="tech" name="tech" maxlength="255">
						</div>	
						<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
							<label for="run_number" class="form-control-label">
								Run Number (number only do not include date):
							</label>
							<input type="number" class="form-control" id="run_number" name="run_number" maxlength="255">
						</div>	
					</div>

					<hr class="section-mark">
					<div class="row">
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
							<h4><u>Search a range of dates</u></h4>
						</div>
					</div>

					<div class="row">
						<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
							<label for="start_date">Start Date: (MM/DD/YYYY)</label>
							<input type="date" class="form-control" name="start_date">
						</div>
						<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
							<label for="end_date">End Date: (MM/DD/YYYY)</label>
							<input type="date" class="form-control " name="end_date">
						</div>
					</div>

					<div class="row">
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style>
							<button type="submit" value="Submit" name="submit" class="btn btn-primary btn-primary-hover">Search
							</button>
						</div>
					</div>
					
				</fieldset>
			</form>
			