<?php
	require_once('templates/lists/run_info_list.php');

?>

<div class="container-fluid">
	<div class="row">
		<?php
			require_once('templates/shared_layouts/report_progress_bar.php');
		?>
		<!-- update area -->
		<div class="col-xs-10 col-sm-10 col-md-10 col-lg-10" style="height:500px;">
			<form class="form" method="post" >
				<fieldset>
					<legend id='new_legend' class='show'>
						Add Gene Interpretations
					</legend>
					
					<?php
						if (isset($patientArray)  && isset($visitArray))
						{
							require_once('templates/tables/patient_visit_report_table.php');
							require_once('templates/shared_layouts/panel_info_report.php');	

							// SAMPLE INFORMATION 
							require_once('templates/shared_layouts/sample_information.php');
						}					

						// MUTANTS
						require_once('templates/tables/mutant_tables.php');
						
						// Low Coverage 
						require_once('templates/tables/low_cov_tables.php');					
						// Interpretation
						require_once('templates/shared_layouts/report_building_interpreatation_section.php');
						
					?>	
			<?php 

			///////////////////////////////////////////////
			// Add summary if made already
			//////////////////////////////////////////////
			if (isset($interpt_summary))
			{
				echo $interpt_summary;
			}

			if (isset($all_gene_interpts) && !empty($all_gene_interpts))
			{
				$count = 0;
				// add gene and text area for each gene which has a mutation
				foreach ($all_gene_interpts as $gene => $gene_interpts)
				{
					$count++;		
					
			?>		<div class="row" >
						<div class="col-xs-12 col-sm-12 col-md-12" <?= $utils->oddEvenBackground($count);?>>
							<div class="form-group">
								<?= $utils->oddEvenSectionMark($count);?>
								
								<h3>Add a Gene interpretation for <?= $gene;?></h3>							
								
								<input type="text" name="gene_<?= $gene;?>[gene_name]" value="<?= $gene;?>" class="d-none" >
								
								<input type="text" name="gene_<?= $gene;?>[empty_data]" value="<?= $gene.': ';?>" class="d-none" >
								
								<textarea id="<?= $gene;?>" class="form-control" name="gene_<?= $gene;?>[interpt]" rows="4" maxlength="65535" style="margin-bottom: 10px;"><?php if(isset($run_xref_interpts) && isset($run_xref_interpts[$gene])){echo $run_xref_interpts[$gene][0]['interpt'];}else{echo $gene.': ';}?></textarea>

			<?php

				// find if gene has an interpt in civic
				$civic_interpt = $API_Utils->searchCivicGene($gene);
				
				$civic_found_in_gene_interpts = False;
				if (!empty($gene_interpts))
				{
			?>
								<table class="formated_table sort_table">
									<thead>
										<th><?= $gene;?> Gene Interpetations</th>
										<th>User Name</th>
										<th>Time Stamp</th>
									</thead>
									<tbody>

			<?php
							// add a table containing all gene_interpts already added for this gene.  Make it clickable so it can be added
							
							foreach ($gene_interpts as $key => $interpt_info)
							{
								if ($interpt_info['interpt'] === $civic_interpt)
								{
									$civic_found_in_gene_interpts = True;
								}
			?>
										<tr class="add_interpt_on_click hover_success_on" data-gene="<?= $gene;?>" data-interpt="<?= $interpt_info['interpt'];?>">
											<td><?= $interpt_info['interpt'];?></td>
											<td><?= $interpt_info['user_name'];?></td>
											<td><?= $interpt_info['time_stamp'];?></td>
										</tr>
			<?php	
							}
			?>					
									</tbody>
								</table>
			<?php
				}
				if (!$civic_found_in_gene_interpts && isset($civic_interpt) && $civic_interpt != '')
				{
			?>
					<div class="alert alert-civic">
						<div class="row">
							<div class="col-xs-12 col-sm-12 col-md-10 col-lg-10">
								<?=$civic_interpt;?>
							</div>
							<div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
								<img src="images/CIViC_logo.png" style="width:100px;height:64px;">
								<button type="button" class="btn btn-primary add-civic" data-interpt="<?=$civic_interpt;?>" data-gene="<?=$gene;?>">Use</button>
							</div>
						</div>						
					</div>
			<?php
				}

			///////////////////////////////////////////////
			// add variants if already added
			// get variant interpts ($reportVariantOrder)
			///////////////////////////////////////////////
			if (isset($reportVariantOrder[$gene]) && is_array($reportVariantOrder[$gene]))
			{
				foreach ($reportVariantOrder[$gene] as $key2 => $curr_variant)
				{
					if (isset($curr_variant['variant_interpt']) && !empty($curr_variant['variant_interpt']))
					{

						echo '<br><br>'.$utils->AddPMIDLinkUrls($curr_variant['variant_interpt']);
					}
					
				}
			}
			?>



							</div>
							<?= $utils->oddEvenSectionMark($count);?>
						</div>

					</div>
					
			<?php
				}
			}
			?>
					<div class="row">
					<!-- add the common hidden fields for every form -->
						<?php
							require_once('templates/shared_layouts/form_hidden_fields.php')
						?>
					</div>

	         				<!-- add submit and nav buttons -->
						<?php
							require_once('templates/shared_layouts/form_submit_nav_buttons.php')
						?>
					
							
				</fieldset>
			</form>
	</div>
</div>