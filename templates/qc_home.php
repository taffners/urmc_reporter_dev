<div class="container-fluid my-4">
     
     <div class="row m-1">
          <h1>Quality Assurance and Version links</h1>
     </div>
     <ul class="row" style="list-style-type:none;padding-left:0px;font-size:18px;">
          <li class="pb-5 col-xs-12 col-sm-6 col-md-4 col-lg-4">

               <div class="d-flex">
                    <ul  style="list-style-type:none;">
                         <li>
                              <a id="version-btn" href="?page=version_info" title="Shows a detailed version history for <?= SITE_TITLE;?>" style="padding-top:5px;padding-bottom:5px;" aria-label="Access <?= SITE_TITLE;?> version history.">
                                   <img src="images/version_control.png" style="margin-left:5px;height:45px;width:45px;" meta="a version control icon" alt="a version control icon">
                              </a>
                         </li>
                         <li><b>Version History</b></li>                                   
                         <li>
                              <?= SITE_TITLE;?> is under <a href="https://en.wikipedia.org/wiki/Git" target="_blank" rel="noopener" rel="noreferrer">Git version control</a>
                         </li>
                    </ul>                              
               </div>                    
          </li>
          <li class="pb-5 col-xs-12 col-sm-6 col-md-4 col-lg-4">

               <div class="d-flex ">
                    <ul  style="list-style-type:none;">
                         <li>
                              <a id="report-a-bug-btn" href="?page=bug_and_downtime_tracking_home" title="Investigate history of bugs and downtime since 2020-11-13" style="padding-top:5px;padding-bottom:5px;" aria-label="report a bug">
                                   <img src="images/bug2.png" style="margin-left:5px;height:45px;width:45px;" meta="an image of a bug" alt="an image of a bug">
                                   <img src="images/Admin.png" style="margin-left:5px;height:45px;width:45px;" meta="maintenance icon" alt="maintenance icon">
                              </a>
                         </li>
                         <li><b>Bugs, maintenance, and down time tracking</b></li>                                     
                    </ul>                              
               </div>                    
          </li>


<?php
     if (isset($user_permssions) && strpos($user_permssions, 'infotrack_QC') !== false)
     {
?>
     
          <li class="pb-5 col-xs-12 col-sm-6 col-md-4 col-lg-4">
               <div class="d-flex ">
                    <ul style="list-style-type:none;">
                         <li>
                              <a href="?page=all_qc_pages" role="button" title="Show a list of all qc pages." aria-label="Go to all website qc/validation pages">
                                   <img src="images/browser.png" style="margin-left:5px;height:45px;width:45px;" meta="an icon of a fake webpage" alt="an icon of a fake webpage">
                              </a> 
                         </li>
                         <li><b>QA By Pages</b></li>
                         <li>A detailed list describing all pages with QC performed in <?= SITE_TITLE;?></li> 
                         <li>Links to the page validations performed</li>
                    </ul>
               </div>
          </li>
          <li class="pb-5 col-xs-12 col-sm-6 col-md-4 col-lg-4">
               <div class="d-flex ">
                    <ul style="list-style-type:none;">
                         <li>
                             <a href="?page=last_validation_reports" role="button" aria-label="Make one validation report for all pages.  Includes most recent validation for each report.">
                                  <img src="images/qc_report.png" style="margin-left:5px;height:45px;width:45px;" meta="an icon of a report" alt="an icon of a report">
                             </a>
                         </li>
                         <li><b>Page Validation Report</b></li>
                         <li>Make one validation report for all pages.</li>
                         <li>Includes most recent validation for each report.</li>
                    </ul>
               </div>
          </li>
          
          <li class="pb-5 col-xs-12 col-sm-6 col-md-4 col-lg-4">

               <div class="d-flex ">
                    <ul  style="list-style-type:none;">
                         <li>
                              <a href="?page=module_report">
                                   <img src="images/diagram-scheme.png" style="margin-left:5px;height:45px;width:45px;"  meta="a fake image of schema" alt="a fake image of schema">
                              </a>
                         </li>
                         <li><b>Module Report</b></li>
                         <li>Refer to gitlab for diagrams</li>                                     
                    </ul>                              
               </div>                    
          </li>

          <li class="pb-5 col-xs-12 col-sm-6 col-md-4 col-lg-4">

               <div class="d-flex ">
                    <ul  style="list-style-type:none;">
                         <li>
                              <a href="https://gitlab.circ.rochester.edu" target="_blank" rel="noopener" rel="noreferrer" style="padding-top:5px;padding-bottom:5px;" aria-label="Access gitlab">
                                   <span style="font-size: 45px;color:#eb8934;" class="fab fa-gitlab" ></span>
                              </a>
                         </li>
                         <li><b>gitlab</b></li>
                         <li><?= SITE_TITLE;?> git repository is located on <a href="https://www.circ.rochester.edu/" target="_blank" rel="noopener" rel="noreferrer">CIRC</a> gitlab</li>
                         <li>To access repository set up a <a href="https://registration.circ.rochester.edu/account" target="_blank" rel="noopener" rel="noreferrer">CIRC account</a></li>
                         <li>Then set up <a href="https://tech.rochester.edu/services/two-factor-authentication/" target="_blank" rel="noopener" rel="noreferrer">duo mobile for University IT NetID</a></li> 
                         <li>Then contact <?= ADMINS;?> to request to be added to the repository</li>                                    
                    </ul>                              
               </div>                    
          </li>
<?php
     }
?>          
     </ul>

     <hr class="section-mark">

<?php
     if (isset($user_permssions) && strpos($user_permssions, 'admin') !== false)
     {                        
?>
     <div class="row m-1">
          <h1>Admin QC</h1>
     </div>
     <ul class="row" style="list-style-type:none;padding-left:0px;font-size:18px;">
          <li class="pb-5 col-xs-12 col-sm-6 col-md-4 col-lg-4">
               <div class="d-flex ">
                    <ul style="list-style-type:none;">
                         <li>
                              <a href="?page=add_qc_checklist_item" class="btn btn-primary btn-primary-hover" role="button">Add QC Checklist Item</a>
                         </li>                         
                    </ul>
               </div>
          </li>
          <li class="pb-5 col-xs-12 col-sm-6 col-md-4 col-lg-4">
               <div class="d-flex ">
                    <ul style="list-style-type:none;">
                         <li>
                              <a href="?page=all_qc_checklist_item" class="btn btn-primary btn-primary-hover" role="button">All QC Checklist Items</a>
                         </li>                         
                    </ul>
               </div>
          </li>
          <li class="pb-5 col-xs-12 col-sm-6 col-md-4 col-lg-4">
               <div class="d-flex ">
                    <ul style="list-style-type:none;">
                         <li>
                             <a href="?page=add_all_missing_qc_page" class="btn btn-primary btn-primary-hover" role="button">Add all missing QC pages</a>
                         </li>                         
                    </ul>
               </div>
          </li>
          <li class="pb-5 col-xs-12 col-sm-6 col-md-4 col-lg-4">
               <div class="d-flex ">
                    <ul style="list-style-type:none;">
                         <li>
                             <a href="?page=add_module" class="btn btn-primary btn-primary-hover" role="button">Add a Module</a>
                         </li>                         
                    </ul>
               </div>
          </li>
          <li class="pb-5 col-xs-12 col-sm-6 col-md-4 col-lg-4">
               <div class="d-flex ">
                    <ul style="list-style-type:none;">
                         <li>
                             <a href="?page=all_modules" class="btn btn-primary btn-primary-hover" role="button">All Modules</a>
                         </li>                         
                    </ul>
               </div>
          </li>

     </ul>
<?php
     }
?>
</div>