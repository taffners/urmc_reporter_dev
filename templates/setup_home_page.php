<div class="container-fluid">
	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="height:500px;">

			<form class="form" method="post" class="form-horizontal">
				<fieldset>
					<legend >
						<?= $page_title;?>
					</legend>

<?php
	if (isset($user_permssions) && strpos($user_permssions, 'admin') !== false)
	{                    
?>
					<div id="form-control-fields">
						<div class="form-group row">
							<div class="col-xs-12 col-sm-4 col-md-4">
								<label for="user_id" class="form-control-label">Select a user to update.  Option only available for Admins:</label>
							</div>
							<div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">
								
								<select class="selectpicker" data-live-search="true" data-width="100%" name="user_id" id="user_id">

									<option value="" selected="selected"></option>
								<?php

									foreach ($all_users as $key => $users)
			               			{				               				
								?>
											<option value="<?= $users['user_id']; ?>">
													<?= $users['user_name']; ?>
											</option>
								<?php								
									}
								?>
								</select>								
							</div>
						</div>
					</div>
						
					
					
<?php
	}
?>

					<div class="form-group row">
						<div class="col-xs-12 col-sm-4 col-md-4">
							<label for="single_analyte_pool_view" class="form-control-label">Set Single Analyte Pool Default Pending List: <span class="required-field">*</span></label>
						</div>
						<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
							<div class="radio">
								<label class="radio-inline"><input type="radio" name="single_analyte_pool_view" value="all_pools" required="required" <?= $utils->DefaultRadioBtn($single_analyte_pool_view, 'feature_choice', 'all_pools');?>>Show all pending pools (Default)</label>

								<label class="radio-inline"><input type="radio" name="single_analyte_pool_view" value="my_pools" <?= $utils->GetValueForUpdateRadioCBX($single_analyte_pool_view, 'feature_choice', 'my_pools');?>>Show only your pending pools</label>

								<label class="radio-inline"><input type="radio" name="single_analyte_pool_view" value="reportable_pools" <?= $utils->GetValueForUpdateRadioCBX($single_analyte_pool_view, 'feature_choice', 'reportable_pools');?>>Show only pending reportable reportable pools (ideal for directors)</label>
														
							</div>
						</div>
					</div>
					<div class="form-group row">
						<div class="offset-xs-4 offset-sm-4 offset-md-4 offset-lg-4 col-xs-2 col-sm-2 col-md-2 col-lg-2">
							<button type="submit" id="<?= $page;?>_submit" name="<?= $page;?>_submit" value="Submit"  class="btn btn-primary btn-primary-hover">
								Submit
							</button>
						</div>
					</div>
				</fieldset>
			</form>
		</div>
		<?php
		if (isset($user_permssions) && strpos($user_permssions, 'admin') !== false)
		{                    
		?>

		
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="height:500px;">
			<?php var_dump($all_single_analyte_pool_view);?>
		</div>

		<?php
		}
		?>
	</div>
</div>