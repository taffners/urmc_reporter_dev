<div class="container-fluid">
	
	<div class="row d-print-none" >
<?php	
	if (!isset($_GET['filter']) || $_GET['filter'] === 'current_month')
	{
		$all_class_turnaround_filter = 'btn btn-primary btn-primary-hover';
		$curr_month_class_turnaround_filter = 'btn btn-success btn-success-hover';
	}
	else if (!isset($_GET['filter']) || $_GET['filter'] === 'all')
	{
		$all_class_turnaround_filter = 'btn btn-success btn-success-hover';
		$curr_month_class_turnaround_filter = 'btn btn-primary btn-primary-hover';
	}
	else
	{
		$all_class_turnaround_filter = 'btn btn-primary btn-primary-hover';
		$curr_month_class_turnaround_filter = 'btn btn-primary btn-primary-hover';
	}
?>
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 d-print-none">
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 alert alert-info">
					To print or save as a PDF right click on page and select print.  
		          </div>		          
		     </div>
		</div>
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 d-print-none">
			<div class="form-group row">
				<div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
					<label for="month" class="form-control-label">
						Month: <span class="required-field">*</span>
					</label>
				</div>
				<div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
					<select class="form-control" name="month" id="month" required="required">					
	<?php
		$months = array(1 => 'Jan', 2 => 'Feb', 3 => 'Mar', 4 => 'Apr', 5 => 'May', 6 => 'Jun', 7 => 'Jul', 8 => 'Aug', 9 => 'Sep', 10 => 'Oct', 11 => 'Nov', 12 => 'Dec');	

		// Add all months selecting current month
		foreach ($months as $month_num => $month_name)
		{
	?>
						<option value="<?= $month_num;?>" <?php
							// select the month which was selected on reload
							if (isset($_GET['month']) && $_GET['month'] == $month_num)
							{
								echo 'selected';
							}

							// default selection to current month.  Useful for selecting all.
							else if (!isset($_GET['month']) && CURR_MONTH === $month_num)
							{
								echo 'selected';
							}
						?>>
							<?= $month_name;?>
						</option>
	<?php
		}
	?>
					</select>			
				</div>
				<div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
					<label for="year" class="form-control-label">
						Year: <span class="required-field">*</span>
					</label>
				</div>
				<div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
					<select class="form-control" name="year" id="year" required="required">					
	<?php
		// Add possible years selecting current year
		for ($y=2019; $y <= CURR_YEAR; $y++)
		{
	?>
						<option value="<?= $y;?>" <?php
							// select the month which was selected on reload
							if (isset($_GET['year']) && $_GET['year'] == $y)
							{
								echo 'selected';
							}

							// default selection to current month.  Useful for selecting all.
							else if (!isset($_GET['year']) && CURR_YEAR === $y)
							{
								echo 'selected';
							}
						?>>
							<?= $y;?>
						</option>
	<?php
		}
	?>
					</select>					
				</div>
				<div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
					<a class="btn btn-primary btn-primary-hover" href="?page=performance_audit&filter=months&month=<?=CURR_MONTH;?>&year=<?=CURR_YEAR;?>&tracked_tests=1"  style="padding-top:5px;padding-bottom:5px;">
		               <img src="images/performance.png" style="margin-left:5px;height:45px;width:45px;">
		          	</a>
				</div>
				<div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
					<a class="btn btn-primary btn-primary-hover" href="?page=alter_default_tracked_tests" title="Use this button to edit which tests are checked automatically to be included in the tracked tests view." style="padding-top:5px;padding-bottom:5px;">
	                   Alter Default Tracked Tests
	              	</a>
				</div>
			</div>

		</div>

		<div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
			<label for="tracked_tests" class="form-control-label">Turn around time checked tests: <span class="mulitple-field">*</span></label>
		</div>
		<div id="track-tests-ckbx-div" class="col-xs-12 col-sm-8 col-md-8
<?php
		// only show if tracked tests btn is clicked
		if (!isset($_GET['tracked_tests']))
		{
			echo ' d-none';
		}
?>
		">
<?php
		$tests_default_tracked = array();
		foreach ($tests as $key => $test)
		{

?>
			<div class="form-check">
				<input class="form-check-input tracked_tests-ckbx" type="checkbox" value="<?= $test['orderable_tests_id'];?>" name="tracked_tests[]" required="required" data-orderable_tests_id="<?= $test['orderable_tests_id'];?>" data-test_name="test_<?= str_replace(' ', '_', $test['test_name']);?>"
				<?php
				if ($test['turnaround_time_tracked'] === 'yes')
				{
					echo 'checked';
					array_push($tests_default_tracked, $test['test_name']);
				}
				?>>
				<label><?= $test['test_name'];?></label>
			</div>
<?php
		}								
?>						
		</div>
	</div>
	
	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 audit-headers">
			<hr class="section-mark"> 
<?php 
	if (isset($num_samples[0]['num_samples']) && isset($selected_month_name) && isset($_GET['year']) && isset($num_tests[0]['num_tests']))
	{ 
?>	

			<b>Total Number of samples (<?= $selected_month_name;?> <?= $_GET['year'];?>):</b> <?= $num_samples[0]['num_samples'];?>
			<br>
			<b>Total Number of Tests (<?= $selected_month_name;?> <?= $_GET['year'];?>):</b> <?= $num_tests[0]['num_tests'];?>
<?php
	}
?>						
		</div>
	</div>
	<div class="row" style="padding:0px;margin:0px;">
		<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6" style="padding:0px;margin:0px;">
			<div id="sampes_per_test"></div>
			<div id="samples_tissues_per_test"></div>
		</div>
		
		<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6" style="padding:0px;margin:0px;">		
			<div id="month_counts"></div>	
			<div id="tissue_counts"></div>
			
		</div>
	</div>
	
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 d-print-none">
			<div class="row">
<?php
	if (isset($_GET['all_turn_around_times']))
	{
		$link = str_replace('&tracked_tests=1', '', $current_address);
	}
	else if (isset($_GET['tracked_tests']))
	{
		$link = str_replace('&tracked_tests=1', '&all_turn_around_times=1', $current_address);
	}
	else
	{
		$link = $current_address.'&all_turn_around_times=1';
	}
?>					
				<div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
					<a href="?page=<?= $link;?>" type="button" class="
					<?php
						if (isset($_GET['all_turn_around_times']))
						{
							echo 'btn btn-success btn-success-hover';
						}
						else
						{
							echo 'btn btn-primary btn-primary-hover';
						}
					?>">
		                    All turn around times
		               </a>
		          </div>
		          <div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
<?php
	if (isset($_GET['all_turn_around_times']))
	{
		$link = str_replace('&all_turn_around_times=1','&tracked_tests=1', $current_address);
	}
	else
	{
		$link = $current_address.'&tracked_tests=1';
	}
?>


					<a id="toggle-tracked-tests-btn" href="?page=<?= $link;?>" type="button" class="
					<?php
						if (isset($_GET['tracked_tests']))
						{
							echo 'btn btn-success btn-success-hover';
						}
						else
						{
							echo 'btn btn-primary btn-primary-hover';
						}
					?>">
		                    Tracked Tests
		               </a>
		          </div>
		          <div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
<?php
	if (isset($_GET['all_turn_around_times']))
	{
		$link = str_replace('&all_turn_around_times=1', '', $current_address);
	}
	else if (isset($_GET['tracked_tests']))
	{
		$link = str_replace('&tracked_tests=1', '', $current_address);
	}
	else
	{
		$link = $current_address;
	}
?>		          	
					<a href="?page=<?= $link;?>" type="button" class="
					<?php
						if (!isset($_GET['all_turn_around_times']) && !isset($_GET['tracked_tests']))
						{
							echo 'btn btn-success btn-success-hover';
						}
						else
						{
							echo 'btn btn-primary btn-primary-hover';
						}
					?>">
		                    Failed turn around times
		               </a>
		          </div>		          
		     </div>
		</div>
	</div>

	<div class="row" style="padding:0px;margin:0px;">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 audit-headers">

<?php
		// Find header for table depending on samples being shown
	if (isset($_GET['all_turn_around_times']))
	{
		$filter_samp_type = 'of All';
	}
	else if (isset($_GET['tracked_tests']))
	{
		// add all tests default tracked but make it so they can be easily deleted and added to

		$tracked_test_str = 'of <span id="all-tracked-samples-table-header-span">';

		foreach ($tests_default_tracked as $key => $default_tracked)
		{
			$tracked_test_str.= ' <span id="'.$default_tracked.'_table_header">'.$default_tracked.'</span>';
		}
		$filter_samp_type = $tracked_test_str.'</span>';
		
	}
	else
	{
		$filter_samp_type = 'of Failed Expected Time';
	}

?>			
			Turnaround Times <?= $filter_samp_type;?> Tests 
		</div>
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			<table class="formated_table sort_table" style="overflow-x:auto;margin-bottom: 10px;">
				<thead>
					<th>Soft Lab#</th>
					<th>MRN</th>
					<th>Patient Name</th>
					<th>Test</th>
					<th># Days</th>
					<th># Work Days</th>
					<th>Expected</th>
					<th>Start Date</th>
					<th>Finish Date</th>					
					<th>User/Task</th>
				</thead>
				<tbody>

		<?php
		if (isset($failed_turnaround) && !empty($failed_turnaround))
		{
			foreach ($failed_turnaround as $key => $failed)
			{
				
				$show_status = '';
				if ( !in_array($failed['test_name'], $tests_default_tracked) && isset($_GET['tracked_tests']))
				{
					$show_status = ' d-none';
				}
		?>
				<tr class="test_<?= str_replace(' ', '_', $failed['test_name']);?><?= $show_status;?>">
					<td>
						
						<svg id="td-barcode-softlab-<?= $key;?>" class="barcode-insert-here" data-barcode="<?= $failed['soft_lab_num'];?>"></svg>
					</td>
					<td>
						
						<svg id="td-barcode-mrn-<?= $key;?>" class="barcode-insert-here" data-barcode="<?= $failed['medical_record_num'];?>"></svg>


					</td>
					<td>
						
						<svg id="td-barcode-patient_name-<?= $key;?>" class="barcode-insert-here" data-barcode="<?= $failed['patient_name'];?>"></svg>


					</td>
					<td><?= $failed['test_name'];?></td>
					<td 
					<?php
						if ($failed['turnaround_time'] > $failed['expected_turnaround_time'])
						{
							echo ' class="red-background"';
						}
					?>
					><?= $failed['turnaround_time'];?></td>
					<td 
					<?php

						$numWorkingDays = $productionDays->numWorkDays($failed['start_date'], $failed['finish_date']);


						if ($numWorkingDays > $failed['expected_turnaround_time'])
						{
							echo ' class="red-background"';
						}
					?>
					><?= $numWorkingDays;?></td>
					<td><?= $failed['expected_turnaround_time'];?></td>
					<td><?= $utils->addEditableTableCell($failed['start_date'], 'start_date', $failed['ordered_test_id'],'ordered_test_id',$failed['ordered_test_id']);?></td>
					<td><?= $utils->addEditableTableCell($failed['finish_date'], 'finish_date', $failed['ordered_test_id'],'ordered_test_id',$failed['ordered_test_id']);?></td>
					<td><?= $failed['users_by_task'];?></td>
				</tr>
		<?php
			}
		}
		?>
				</tbody>
			</table>
		</div>
	</div>
</div>