<div class="container-fluid">
	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="height:500px;">
			<form id="add_patient_info" class="form" method="post" class="form-horizontal">
				<fieldset>
					<legend>
						Add FLT3 ITD
					</legend>
					<div class="form-group row d-none">
						<div class="col-xs-12 col-sm-4 col-md-4">
							<label for="run_id" class="form-control-label">run id: <span class="required-field">*</span></label>
						</div>
						<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
							<input type="number" class="form-control" id="run_id" name="run_id" value="<?= $_GET['run_id'];?>" readonly="readonly" required/>
						</div>
					</div>
					<div class="form-group row">
						<div class="col-xs-12 col-sm-4 col-md-4">
							<label for="genes" class="form-control-label">Gene: <span class="required-field">*</span></label>
						</div>
						<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
							<input type="text" class="form-control" id="genes" name="genes" maxlength="50" value="FLT3" readonly="readonly" required/>
						</div>
					</div>
					<div class="form-group row">
						<div class="col-xs-12 col-sm-4 col-md-4">
							<label for="coding" class="form-control-label">Coding: <span class="required-field">*</span></label>
						</div>
						<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
							<input type="text" class="form-control" id="coding" name="coding" maxlength="255" value="ITD" readonly="readonly" required/>
						</div>
					</div>
					<div class="form-group row">
						<div class="col-xs-12 col-sm-4 col-md-4">
							<label for="amino_acid_change" class="form-control-label">Protein: <span class="required-field">*</span></label>
						</div>
						<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
							<input type="text" class="form-control" id="amino_acid_change" name="amino_acid_change" maxlength="255" value="ITD" readonly="readonly" required/>
						</div>
					</div>	
					<div class="form-group row">
						<div class="col-xs-12 col-sm-4 col-md-4">
							<label for="frequency" class="form-control-label">VAF (only include number): <span class="required-field">*</span></label>
						</div>
						<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
							<input type="text" class="form-control" id="frequency" name="frequency" maxlength="255" required/>
						</div>
					</div>
					<div class="form-group row">
						<div class="col-xs-12 col-sm-4 col-md-4">
							<label for="allele_coverage" class="form-control-label">Length of duplication: <span class="required-field">*</span></label>
						</div>
						<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
							<input type="text" class="form-control" id="allele_coverage" name="allele_coverage" maxlength="255" required/>
						</div>
					</div>
					<div class="row">
						<div class="offset-xs-4 offset-sm-4 offset-md-4 offset-lg-4 col-xs-2 col-sm-2 col-md-2 col-lg-2">
							<button type="submit" id="<?= $page;?>_submit" name="<?= $page;?>_submit" value="Submit"  class="btn btn-primary btn-primary-hover">
								Submit
							</button>
						</div>

						<div class="offset-xs-2 offset-sm-2 offset-md-2 offset-lg-2 col-xs-2 col-sm-2 col-md-2 col-lg-2">
					<?php
						if (isset($_GET['previous_page']) && $_GET['previous_page'] === 'check_tsv_upload')
						{
					?>
							<a href="?page=check_tsv_upload&<?= EXTRA_GETS;?>" class="btn 
							btn-primary btn-primary-hover" role="button">
								❮ Previous
							</a>
					<?php
						}
						else if (isset($_GET['previous_page']) && $_GET['previous_page'] === 'verify_variants')
						{
					?>
							<a href="?page=verify_variants&<?= EXTRA_GETS;?>&variant_filter=included" role="button" class="btn 
							btn-primary btn-primary-hover">
								❮ Previous
							</a>
					<?php
						}
					?>
						</div>
					</div>

				</fieldset>
			</form>
		</div>
	</div>
</div>