<?php
	require_once('templates/lists/run_info_list.php');
?>

<div class="container-fluid">
	<div class="row">
		<?php
			require_once('templates/shared_layouts/report_progress_bar.php');
		?>

		<!-- update area -->
		<div id="toggle-report-progress-bar" class="col-xs-10 col-sm-10 col-md-10 col-lg-10" style="height:500px;">
			<form id="add_interpts_variants_form" class="form" method="post" class="form-horizontal">
				<fieldset>
					<legend id='new_legend' class='show'>
						Add Interpretation for Each Included Variant
					</legend>
					
				<?php
					if (isset($patientArray)  && isset($visitArray))
					{
						require_once('templates/tables/patient_visit_report_table.php');
						require_once('templates/shared_layouts/panel_info_report.php');	

						// SAMPLE INFORMATION 
						require_once('templates/shared_layouts/sample_information.php');
					}					

					// MUTANTS
					require_once('templates/tables/mutant_tables.php');

					// Interpretation
					require_once('templates/shared_layouts/report_building_interpreatation_section.php');
				?>	
				<!-- add any gene Interpetations -->
			<?php 

			///////////////////////////////////////////////
			// Add summary if made already
			//////////////////////////////////////////////
			if (isset($interpt_summary))
			{
				echo $utils->AddPMIDLinkUrls($interpt_summary).'<br><br>';
			}

			if (isset($reportGeneOrder) && !empty($reportGeneOrder))
			{
				$count = 0;
				// add gene and text area for each gene which has a mutation
				foreach ($reportGeneOrder as $gene)
				{
					$count++;
					// Add gene label
			?>
					
					<div class="row" >
						<div class="col-xs-12 col-sm-12 col-md-12" <?= $utils->oddEvenBackground($count);?>>
						<?= $utils->oddEvenSectionMark($count);?>
						<h3><?= $gene;?></h3>	
					
			<?php

					// add Gene interpt if one exists
					if (isset($run_xref_interpts[$gene][0]['interpt']) &&  $run_xref_interpts[$gene][0]['interpt'] !== null)
					{
			?>
						<div><?=  $utils->AddPMIDLinkUrls($run_xref_interpts[$gene][0]['interpt']);?></div>
			<?php
					}

					// Add Each Mutant found and a textarea box.
					if (isset($reportVariantOrder) && !empty($reportVariantOrder))
					{
						
						foreach ($reportVariantOrder[$gene] as $key => $variant)	
						{							
							$variant_name = $variant['Gene'].' '.$variant['DNA'].' ('.$variant['Protein'].')';
			?>
						
						<h3>Add a variant interpretation for <?= $variant_name;?></h3>	 

						<input type="text" name="<?= $variant['knowledge_id']?>[knowledge_id]" value="<?= $variant['knowledge_id'];?>" class="d-none">
						<input type="text" name="<?= $variant['knowledge_id']?>[observed_variant_id]" value="<?= $variant['observed_variant_id'];?>" class="d-none">
						<input type="text" name="<?= $variant['knowledge_id']?>[empty_data]" class="d-none" value="<?= $variant_name.': ';?>">
						<textarea id="div_textarea_<?= $variant['knowledge_id'];?>" class="form-control" name="<?= $variant['knowledge_id']?>[variant_interpt]" rows="5" maxlength="65535" style="margin-bottom: 10px;margin-top: 10px;"><?php if (isset($variant['genetic_call']) && $variant['genetic_call'] != ''){echo $variant['genetic_call'].': ';}else if (isset($variant['variant_interpt']) && $variant['variant_interpt'] != ''){echo $variant['variant_interpt'];}else {echo $variant_name.': ';}?></textarea>
			<?php

					// find if variant has an interpt in civic
					// TURNED OFF BECAUSE CIVIC DOES NOT USE A CONSISENT NAME
					// $civic_interpt = $API_utils->searchCivicVariant($variant);

					////////////////////////////////////////////////////////
					// Add all VARIANT INTERTPS into a table 
					////////////////////////////////////////////////////////
							$comments = $db->listAll('knowledge-comments-active', $variant['knowledge_id']);	

						     if (!empty($comments))
			     			{	
			     				require('templates/lists/knowledge_comment_list.php');
			     			}								
						}
					}

			?>
						<?= $utils->oddEvenSectionMark($count);?>
						</div>
					</div>
					
			<?php
				}
			}	
			?>


				<!-- add the common hidden fields for every form -->
				<?php
					require_once('templates/shared_layouts/form_hidden_fields.php')
				?>

    				<!-- add submit and nav buttons -->
				<?php
					require_once('templates/shared_layouts/form_submit_nav_buttons.php')
				?>
         				
				</fieldset>
			</form>
			<!-- add the knowledge base for current view of variants for this view.  If  variant_filter=passed only show variants containing these -->
			<?php
				require_once('templates/knowledge_base.php');
			?>
		</div>
	</div>
</div>

