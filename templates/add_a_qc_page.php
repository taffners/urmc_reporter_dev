<div class="container-fluid">
	<div class="row">
		
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			<a href="?page=<?= $pageArray[0]['page_name'];?>" role="button" target="_blank" class="btn btn-primary btn-primary-hover">
				Open the validation page in new tab
			</a>
		</div>
	</div>
	<div class="row">
		
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			<form class="form" method="post" class="form-horizontal">
				<fieldset>
				<?php 
					if (isset($_GET['page_id']))					
					{
				?>
					<legend>
						Update Page 
					</legend>
					
				<?php 
					}
					else
					{
				?>
					<legend>
						Add Page
					</legend>
				<?php
					}
				?>					
					
					<div class="form-group row">
						<div class="col-xs-12 col-sm-4 col-md-4">
							<label for="page_name" class="form-control-label" >Page Name: <span class="required-field">*</span></label>
						</div>
						<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
							<input type="text" class="form-control" id="page_name" name="page_name" maxlength="100" value="<?= $utils->GetValueForUpdateInput($pageArray, 'page_name');?>"/>
						</div>
					</div>
					<div class="form-group row">
						<div class="col-xs-12 col-sm-4 col-md-4">
							<label for="page_type" class="form-control-label">Page Type: <span class="required-field">*</span></label>
						</div>
						<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
							<div class="radio">
								<label class="radio-inline"><input type="radio" name="page_type" value="Form" required="required" <?= $utils->DefaultRadioBtn($pageArray, 'page_type', 'Form');?>>Form</label>
							
								<label class="radio-inline"><input type="radio" name="page_type" value="View"  <?= $utils->GetValueForUpdateRadioCBX($pageArray, 'page_type', 'View');?>>View</label>

								<label class="radio-inline"><input type="radio" name="page_type" value="Report"  <?= $utils->GetValueForUpdateRadioCBX($pageArray, 'page_type', 'Report');?>>Report</label>

								<label class="radio-inline"><input type="radio" name="page_type" value="Function"  <?= $utils->GetValueForUpdateRadioCBX($pageArray, 'page_type', 'Function');?>>Function</label>
							</div>
						</div>
					</div>

					<div id="form-control-fields">
						<div class="form-group row">
							<div class="col-xs-12 col-sm-4 col-md-4">
								<label for="module_id" class="form-control-label">Module Name:</label>
							</div>
							<div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">
								
								<select class="selectpicker" data-live-search="true" data-width="100%" name="module_id" id="module_id">

									<option value="" selected="selected"></option>
								<?php

									foreach ($all_modules as $key => $module)
			               			{	
								?>
											<option value="<?= $module['module_id']; ?>" <?= $utils->GetValueForUpdateSelect($pageArray, 'module_id', $module['module_id'])?>>
													<?= $module['module_name']; ?>
											</option>
								<?php
									}
								?>
								</select>								
							</div>
						</div>
					</div>

					<div class="form-group row">
						<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
							<label for="restricted_permissions" class="form-control-label">Add all users that are allowed to access page: <span class="required-field">*</span><span class="mulitple-field">*</span></label>
						</div>
						<div class="col-xs-12 col-sm-8 col-md-8 pull-left ">
				<?php
						foreach ($reporter_permission_list as $key => $permission)
						{
				?>
							<div class="form-check">
								<input class="form-check-input" type="checkbox" value="<?= $permission['permission'];?>" name="restricted_permissions[]" required="required"

								<?php 
									if (isset($pageArray[0]['permissions']) && strpos($permission['permission'], $pageArray[0]['permissions']) !== False)
									{
										echo 'checked';
									}
								?>>
								<label><?= $permission['permission'];?></label>
							</div>
				<?php
						}								
				?>			
							<div class="form-check">
								
								<input class="form-check-input" type="checkbox" value="All Users" name="restricted_permissions[]" required="required"
								<?php 
									if (isset($pageArray[0]['permissions']) && strpos('All Users', $pageArray[0]['permissions']) !== False)
									{
										echo 'checked';
									}
								?>>
								<label>All Users</label>
							</div>
						</div>
					</div>
					<div class="form-group row">
						<div class="col-xs-12 col-sm-4 col-md-4">
							<label for="page_description" class="form-control-label">Page Description:</label>
						</div>

						<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
							<textarea class="form-control" name="page_description" rows="4" maxlength="65535" style="margin-bottom: 10px;"><?= $utils->GetValueForUpdateInput($pageArray, 'page_description');?></textarea>
						</div>
					</div>
					<div class="form-group row">
						<div class="col-xs-12 col-sm-4 col-md-4">
							<label for="how_to_access_page" class="form-control-label">How to access page:</label>
						</div>

						<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
							<textarea class="form-control" name="how_to_access_page" rows="4" maxlength="65535" style="margin-bottom: 10px;"><?= $utils->GetValueForUpdateInput($pageArray, 'how_to_access_page');?></textarea>
						</div>
					</div>
					<div class="form-group row">
						<div class="col-xs-12 col-sm-4 col-md-4">
							<label for="page_assumption" class="form-control-label">Page assumptions:</label>
						</div>

						<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
							<textarea class="form-control" name="page_assumption" rows="4" maxlength="65535" style="margin-bottom: 10px;"><?= $utils->GetValueForUpdateInput($pageArray, 'page_assumption');?></textarea>
						</div>
					</div>
					<div class="form-group row">
						<div class="col-xs-12 col-sm-4 col-md-4">
							<label for="printing_necessary" class="form-control-label">Printing Necessary: <span class="required-field">*</span></label>
						</div>
						<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
							<div class="radio">
								<label class="radio-inline"><input type="radio" name="printing_necessary" value="Yes" required="required" <?= $utils->GetValueForUpdateRadioCBX($pageArray, 'printing_necessary', 'Yes');?>>Yes</label>
							
								<label class="radio-inline"><input type="radio" name="printing_necessary" value="No"  <?= $utils->DefaultRadioBtn($pageArray, 'printing_necessary', 'No');?>>No</label>

							</div>
						</div>
					</div>

			




					<div class="form-group row">
						<div class="offset-xs-4 offset-sm-4 offset-md-4 offset-lg-4 col-xs-2 col-sm-2 col-md-2 col-lg-2">
							<button type="submit" id="<?= $page;?>_submit" name="<?= $page;?>_submit" value="Submit"  class="btn btn-primary btn-primary-hover">
								Submit
							</button>
						</div>
					</div>
				</fieldset>
			</form>
		</div>
	</div>
</div>