<div class="container">
	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			
			<form name='create_freezer' id="create_freezer" class='form-horizontal' method='post'>
				<fieldset id="search">
					<legend><?= $page_title; ?></legend>
					<div class="row">
						<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
							<label for="genes" class="form-control-label">
								Gene:
							</label>
							<input type="text" class="form-control" id="genes" name="genes" maxlength="30">
						</div>	
						<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
							<label for="codings" class="form-control-label">
								Coding:
							</label>
							<input type="text" class="form-control" id="coding" name="coding" maxlength="255">
						</div>	
						<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
							<label for="amino_acid_changes" class="form-control-label">
								Amino Acid Change:
							</label>
							<input type="text" class="form-control" id="amino_acid_change" name="amino_acid_change" maxlength="255">
						</div>	
					</div>

					<hr class="section-mark">
					<div class="row">
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
							<h4><u>Search a range of run dates</u></h4>
						</div>
					</div>

					<div class="row">
						<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
							<label for="start_date">Start Date: (MM/DD/YYYY)</label>
							<input type="text" class="form-control input-sm searchPressEnter date-picker" name="start_date">
						</div>
						<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
							<label for="end_date">End Date: (MM/DD/YYYY)</label>
							<input type="text" class="form-control input-sm searchPressEnter date-picker" name="end_date">
						</div>
					</div>

					<hr class="section-mark">
					<div class="row">
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
							<h4><u>Search Run Info</u></h4>
						</div>
					</div>

					<div class="row">
						<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
							<label for="work_flow_name" class="form-control-label">
								Work Flow Name:
							</label>
							<input type="text" class="form-control" id="work_flow_name" name="work_flow_name" maxlength="30">
						</div>	
						<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
							<label for="sample_name" class="form-control-label">
								Sample Name:
							</label>
							<input type="text" class="form-control" id="sample_name" name="sample_name" maxlength="20">
						</div>	
						<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
							<label for="pipeline" class="form-control-label">
								Pipeline:
							</label>
							<input type="text" class="form-control" id="pipeline" name="pipeline" maxlength="30">
						</div>	
						<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
							<label for="filter_chain" class="form-control-label">
								Filter Chain:
							</label>
							<input type="text" class="form-control" id="filter_chain" name="filter_chain" maxlength="20">
						</div>
					</div>

					<hr class="section-mark">
					<div class="row">
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
							<h4><u>Filter Variants on Sequencing Filter Status or if it was Reported</u></h4>
						</div>
					</div>

					<div class="row">
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
							
							<div class="radio" style="padding:0px;">
  								<label class="radio-inline">
  									<input type="radio" name="radio_filter" value="all" checked="checked">All
  								</label>
  								<label class="radio-inline">
  									<input type="radio" name="radio_filter" value="passed">Passed
  								</label>
  								<label class="radio-inline">
  									<input type="radio" name="radio_filter" value="failed">Failed
  								</label>
  								<label class="radio-inline">
  									<input type="radio" name="radio_filter" value="included">Included in Report
  								</label>
							</div>
						</div>
						
					</div>
					<div class="row">
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style>
							<button type="submit" value="Submit" name="submit" class="btn btn-primary btn-primary-hover">Search
							</button>
						</div>
					</div>
					
				</fieldset>
			</form>
			