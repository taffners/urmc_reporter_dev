

<div class="container-fluid">
     <fieldset>
         
          <legend><?= $page_title;?></legend>
         

          <div class="row" style="overflow-x:auto;">
               <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <table class="formated_table sort_table_no_inital_sort_no_paging">
                         <thead>
                              <th>First Name</th>
                              <th>Last Name</th>
                              <th>Soft Lab #</th>
                              <th>MD#</th>
                              <th>MRN</th>
                              <th>Start Date</th>
                              <th>Finish Date</th>
                              <th>Test Name</th>
                              <th>test_result</th>                              
                         </thead>
                         <tbody>
               <?php

                    for($i=0;$i<sizeof($instrument_use_report_array);$i++)
                    {    
               ?>
                              <tr>
                                  <td><?= $instrument_use_report_array[$i]['first_name'];?></td>
                                  <td><?= $instrument_use_report_array[$i]['last_name'];?></td>
                                  <td><?= $instrument_use_report_array[$i]['soft_lab_num'];?></td>
                                  <td><?= $instrument_use_report_array[$i]['mol_num'];?></td>
                                  <td><?= $instrument_use_report_array[$i]['medical_record_num'];?></td>
                                  <td><?= $instrument_use_report_array[$i]['start_date'];?></td>
                                  <td><?= $instrument_use_report_array[$i]['finish_date'];?></td>
                                  <td><?= $instrument_use_report_array[$i]['test_name'];?></td>
                                  <td><?= $instrument_use_report_array[$i]['test_result'];?></td>
                              </tr>
               <?php 
                    }
               ?>
                         </tbody>
                    </table>
               </div>
          </div>
     </fieldset>
</div>
