<div class="container-fluid">

	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			
			<fieldset>
				<legend>
					Sample Information for <?= $sampleLogBookArray[0]['mol_num'];?>
				</legend>
<?php
				require_once('templates/shared_layouts/sample_overview.php');
				
?>
			</fieldset>
		</div>
	</div>
	
	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			
			<fieldset>
				<legend>
					Update <?= $utils->UnderscoreCaseToHumanReadable($_GET['update_type']);?> for <?= $test['test_name'];?> Test
				</legend>

				<div class="form-group row">
					<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
						<label class="form-control-label">Current Test Results: </label>
					</div>
					<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
						<?php require('templates/shared_layouts/test_summary_info.php'); ?>
					</div>
				</div>
<?php

				
			
// To build information for history of tests have user add results for tests that previous_positive_required (example tests IGH, IGK, FLT3)
// Also some test have a differing result depending on result
if 	(	
		isset($testInfo[0]['test_result']) && 
		$testInfo[0]['test_result'] === 'yes' &&
		$_GET['update_type'] === 'test_result'
	)
{
?>	
				<form class="form" method="post" class="form-horizontal">
					<hr class="section-mark">

	<?php
	// Add a reminder about reflex tests linked to this test.
	if  (
			isset($reflexTestArray) && !empty($reflexTestArray) && 
			isset($nextReflexes) && !empty($nextReflexes)
		)
	{
	?>
					<div id="reflex-update-section" class="form-group row">
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
							<div class="alert alert-info">
								This test is linked to the reflex tracker <b><?= $reflexTestArray[0]['reflex_name'];?></b> if the result of this test is <b><?= $reflexTestArray[0]['reflex_value'];?></b> make sure to add a <b><?= $nextReflexes[0]['test_name'];?></b> test to this patient's sample.
							</div>
							<div class="d-none">
								<input type="text" name="trigger_result" value="<?= $reflexTestArray[0]['reflex_value'];?>">
								<input type="text" name="status_linked_reflexes_xref_id" value="<?= $reflexTestArray[0]['status_linked_reflexes_xref_id'];?>">
								<input type="text" name="next_test_name" value="<?= $nextReflexes[0]['test_name'];?>">
								<input type="text" name="ordered_reflex_test_id" value="<?= $reflexTestArray[0]['ordered_reflex_test_id'];?>">	
								<input type="text" name="reflex_status" value="<?= $reflexTestArray[0]['reflex_status'];?>">								
							</div>
						</div>
					</div>

	<?php
	}

	if (isset($testInfo[0]['result_note']) && !empty($testInfo[0]['result_note']))
	{
	?>
					<div id="reflex-update-section" class="form-group row">
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
							<div class="alert alert-info">
								<?= nl2br($testInfo[0]['result_note']);?>
							</div>
						</div>
					</div>
	<?php
	}

	if (isset($testInfo[0]['result_type']) && $testInfo[0]['result_type'] === 'pos neg')
	{

	?>
					<div class="form-group row">
						<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
							<label for="test_result" class="form-control-label">Test Result: <span class="required-field">*</span></label>
						</div>
						<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
				
							<label class="radio-inline">
								<input type="radio" name="test_result" value="Positive" required>
								Positive
							</label>
							<label class="radio-inline">
								<input type="radio" name="test_result" value="Weak Positive" required>
								Weak Positive
							</label>
							<label class="radio-inline">
								<input type="radio" name="test_result" value="Negative" required>
								Negative
							</label>
							<label class="radio-inline">
								<input type="radio" name="test_result" value="Weak Negative" required>
								Weak Negative
							</label>
							<label class="radio-inline">
								<input type="radio" name="test_result" value="No Signal" required>
								No Signal
							</label>
							<label class="radio-inline">
								<input type="radio" name="test_result" value="See Interpt." required>
								See Interpt.
							</label>
						</div>
					</div>
<?php
	}
	else if (isset($testInfo[0]['result_type']) && $testInfo[0]['result_type'] === 'normal,low,high,QC')
	{
?>
					<div class="form-group row">
						<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
							<label for="test_result" class="form-control-label">Test Result: <span class="required-field">*</span></label>
						</div>
						<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
							<label class="radio-inline">
								<input type="radio" name="test_result" value="Normal" required>
								Normal
							</label>

							<label class="radio-inline">
								<input type="radio" name="test_result" value="MSS/MSI-L report is 0/5 or 1/5 markers shifted" required>
								MSS/MSI-L report is 0/5 or 1/5 markers shifted
							</label>
							
							<label class="radio-inline">
								<input type="radio" name="test_result" value="MSI-H report is 2/5 - 5/5 markers shifted" required>
								MSI-H report is 2/5 - 5/5 markers shifted
							</label>
							<label class="radio-inline">
								<input type="radio" name="test_result" value="Quantity/Quality not sufficient for a result" required>
								Quantity/Quality not sufficient for a result
							</label>
							
						</div>
					</div>
<?php
	}
?>
					<hr class="section-mark">
					<div class="form-group row">
						<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
							<label for="problems" class="form-control-label">Please include an explanation of why the data is being updated.
						</div>

						<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
							<textarea class="form-control" name="comments" rows="4" maxlength="65535" style="margin-bottom: 10px;"></textarea>
						</div>
					</div>


					<div class="form-group row">
						<div class="offset-xs-4 offset-sm-4 offset-md-4 offset-lg-4 col-xs-2 col-sm-2 col-md-2 col-lg-2">
							<button type="submit" id="<?= $page;?>_submit" name="<?= $page;?>_submit" value="Submit"  class="btn btn-primary btn-primary-hover">
								Submit (All changes are permanent and will remove original data)
							</button>
						</div>
					</div>
				</form>
<?php
}
?>	


			</fieldset>

		</div>
	</div>
	
</div>