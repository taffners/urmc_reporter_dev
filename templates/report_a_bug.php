<div class="container-fluid">
	
	<div class="row">
		
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			<form class="form" method="post" class="form-horizontal">
				<fieldset>
				
					<legend>
						<?= $page_title;?>
					</legend>
					<div class="form-group row">
						<div class="col-xs-12 col-sm-4 col-md-4">
							<label for="report_type" class="form-control-label">Report Type: <span class="required-field">*</span></label>
						</div>
						<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
							<div class="radio">
								<label class="radio-inline"><input type="radio" name="report_type" required="required" value="bug" >Bug</label>
							
								<label class="radio-inline"><input type="radio" name="report_type" value="suggestion" >Suggestion</label>
							<?php
								if (isset($user_permssions) && strpos($user_permssions, 'admin') !== false)
								{                        
							?>
								<label class="radio-inline"><input type="radio" name="report_type" value="maintenance" >Maintenance</label>
							<?php
								}
							?>								
							</div>
						</div>
					</div>
<?php
	if (isset($user_permssions) && strpos($user_permssions, 'admin') !== false)
	{                        
?>
					<div id="form-control-fields">
						<div class="form-group row">
							<div class="col-xs-12 col-sm-4 col-md-4">
								<label for="user_name" class="form-control-label">Bug Reporters Name:</label>
							</div>
							<div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">
								
								<select class="selectpicker" data-live-search="true" data-width="100%" name="user_name" id="user_name">

									<option value="" selected="selected"></option>
								<?php

									foreach ($all_users as $key => $users)
			               			{				               				
								?>
											<option value="<?= $users['user_name']; ?>">
													<?= $users['user_name']; ?>
											</option>
								<?php								
									}
								?>
								</select>								
							</div>
						</div>
					</div>
						
					<div class="form-group row">
						<div class="col-xs-12 col-sm-4 col-md-4">
							<label for="maintenance_start" class="form-control-label">maintenance start: </label>
						</div>
						<div class="col-xs-12 col-sm-8 col-md-4 col-lg-3">
							<input class="form-control" type="datetime-local" id="maintenance_start" name="maintenance_start" />
						</div>
					</div>
					<div class="form-group row">
						<div class="col-xs-12 col-sm-4 col-md-4">
							<label for="maintenance_end" class="form-control-label">maintenance end: </label>
						</div>
						<div class="col-xs-12 col-sm-8 col-md-4 col-lg-3">
							<input class="form-control" type="datetime-local" id="maintenance_end" name="maintenance_end" />
						</div>
					</div>
					<div class="form-group row">
						<div class="col-xs-12 col-sm-4 col-md-4">
							<label for="downtime_occurs" class="form-control-label">If this is planned maintenance will down time occur</label>
						</div>
						<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
							<div class="radio">
								<label class="radio-inline"><input type="radio" name="downtime_occurs" value="yes" >Yes</label>
							
								<label class="radio-inline"><input type="radio" name="downtime_occurs" value="no" >No</label>
														
							</div>
						</div>
					</div>
<?php
	}
?>					
					<div class="form-group row">
						<div class="col-xs-12 col-sm-4 col-md-4">
							<label for="bug_description" class="form-control-label">Description: <span class="required-field">*</span></label>
						</div>

						<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
							<textarea class="form-control" name="bug_description" rows="4" maxlength="65535" style="margin-bottom: 10px;" id="bug_description" required="required"></textarea>
						</div>
					</div>
					<div class="form-group row">
						<div class="offset-xs-4 offset-sm-4 offset-md-4 offset-lg-4 col-xs-2 col-sm-2 col-md-2 col-lg-2">
							<button type="submit" id="<?= $page;?>_submit" name="<?= $page;?>_submit" value="Submit"  class="btn btn-primary btn-primary-hover">
								Submit
							</button>
						</div>
					</div>
				</fieldset>
			</form>
		</div>
	</div>
</div>