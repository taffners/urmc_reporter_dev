<div class="container-fluid">
	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
<?php
	if (isset($all_possible_sample_types))
	{
?>
			<fieldset>
				<legend>Current Sample Types</legend>
					<div class="row alert alert-info">
						<h1>How to adjust which sample types show up in sample type drop down menu</h1>
						<ul style="font-size:18px;">Show
							<li><span style="color:red;"><span class="fas fa-plus"></span>Red off</span> means this sample type will not show up in the sample type drop down menu in the sample log book.</li>
							<li>Click on <span style="color:red;"><span class="fas fa-plus"></span>Red off</span> button under status to show this sample type in the drop down</li>
						</ul>
						<ul style="font-size:18px;">Hide
							<li><span style="color:green;"><span class="fas fa-plus"></span>Green on</span> means this sample type will show up in the sample type drop down menu in the sample log book.</li>
							<li>Click on <span style="color:green;"><span class="fas fa-plus"></span>Green on</span> button under status to hide this sample type in the drop down</li>
						</ul>
						
					</div>
					<table class="formated_table sort_table" style="margin-bottom:20px;">
						<thead>
							<th>Sample Type</th>
							<th>Status</th>
						</thead>	
						<tbody>

<?php
				foreach ($all_possible_sample_types as $key => $samp_type)
				{
?>
							<tr>
								<td><?= $samp_type['sample_type'];?></td>
								<td>
									<button  class="toggle-on-off-status btn <?php
								if (isset($samp_type['status']) && $samp_type['status'] === 'on')
								{
									echo 'btn-success';
								}
								else 
								{
									echo 'btn-danger';
								}
								?>" data-table="possible_sample_types_table" data-id="<?=$samp_type['possible_sample_types_id'];?>" data-value="<?=$samp_type['sample_type'];?>" data-curr-status="<?= $samp_type['status'];?>"><span class="fas fa-plus"></span> <?=$samp_type['status'];?></button>		
								</td>
							</tr>					
<?php
				}
?>
					</tbody>
				</table>
			</fieldset>
<?php
	}
?>			
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

			<form class="form" method="post" class="form-horizontal">
				<fieldset>
					<legend>Add new Sample Type</legend>

					<div class="form-group row">

						<div class="col-xs-12 col-sm-4 col-md-4">
							<label for="version" class="form-control-label version-lot-label" id="sample_type">Sample Type: <span class="required-field">*</span></label>		
												
						</div>
						<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
							<input type="text" class="form-control" id="sample_type" name="sample_type" maxlength="100" required="required" />
						</div>
					</div>
					<div class="form-group row d-print-none">
						<div class="offset-xs-4 offset-sm-4 offset-md-4 offset-lg-4 col-xs-2 col-sm-2 col-md-2 col-lg-2">
							<button type="submit" id="<?= $page;?>_submit" name="<?= $page;?>_submit" value="Submit"  class="btn btn-primary btn-primary-hover">
								Submit
							</button>
						</div>
					</div>
					
				</fieldset>
			</form>
		</div>
	</div>
</div>