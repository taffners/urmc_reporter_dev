<?php
	require_once('templates/lists/run_info_list.php');
?>

<div class="container-fluid">
	<div class="row">
		<?php
			require_once('templates/shared_layouts/report_progress_bar.php');		
		?>

		<!-- update area -->
		<div id="toggle-report-progress-bar" class="col-xs-10 col-sm-10 col-md-10 col-lg-10" style="height:500px;">

				
			
			<fieldset>
				
		<?php
			if (isset($variantArray))
       		{    
		?>
				<legend>
					Variant Confirmations
				</legend>

		     	<div class="row">
		     		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				     	<table class="formated_table sort_table_no_inital_sort_no_paging" style="overflow-x:auto;margin-bottom: 10px;">
				        	<thead>
				          		<th>Genes</th>
				          		<th>Coding</th>
				          		<th>Amino Acid Change</th>	              
				          		<th>Confirmation Request Status</th>
				          		<th>Request</th>
				          	                       
				          	</thead>
				          	<tbody>
						<?php
							foreach($variantArray as $key => $v)
							{
								// Find if a confirmation was sent previously for this variant.  If so do not let another one be sent
								$curr_confirmations = $db->listAll('curr-confirmations', $v['observed_variant_id']);
						?>
								<tr>
									<td><?= $v['genes'];?></td>
									<td><?= $v['coding'];?></td>
									<td><?= $v['amino_acid_change'];?></td>
									<td>
										<?php
										if (isset($curr_confirmations) && !empty($curr_confirmations))
										{
											echo $curr_confirmations[0]['status'];
										}
										else
										{
											echo 'none';
										}
										?>
									</td>
									<td>
										<?php
										if (isset($curr_confirmations) && !empty($curr_confirmations))
										{
											echo 'Email already sent';
										}
										else
										{
										?>

										<a href="?page=send_confirmation_email&genes=<?= $v['genes'];?>&coding=<?= $v['coding'];?>&amino_acid_change=<?= $v['amino_acid_change'];?>&observed_variant_id=<?= $v['observed_variant_id'];?>&<?= EXTRA_GETS;?>" type="button" class="btn btn-primary btn-primary-hover d-print-none">
											Send Confirmation Email
										</a>
										<?php
										}
										?>
									</td>
								</tr>
						<?php
							}
						?>				     
				     
				          	</tbody>
						</table>
					</div>
				</div>	
		<?php
			}
		?>
			</fieldset>
		</div>
	</div>
</div>
