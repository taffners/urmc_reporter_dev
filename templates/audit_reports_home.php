<div class="container-fluid my-4">
     
	<div class="row m-1">
	<h1>Audit Reports</h1>
	</div>
	<ul class="row" style="list-style-type:none;padding-left:0px;font-size:18px;">
		<li class="pb-5 col-xs-12 col-sm-6 col-md-4 col-lg-4">
			<div class="d-flex ">
			    <ul style="list-style-type:none;">
					<li>
						<a id="performance-audit-btn" href="?page=performance_audit&filter=months&month=<?=CURR_MONTH;?>&year=<?=CURR_YEAR;?>" title="Performance Audit Reports" style="padding-top:5px;padding-bottom:5px;">
							<img src="images/performance.png" style="margin-left:5px;height:45px;width:45px;" alt="monthly audits">
						</a>
					</li>
					<li><b>Monthly Audits</b></li>
					<li>A detailed list summary of tests performed each month</li> 
			         
			    </ul>
			</div>
		</li>

		<li class="pb-5 col-xs-12 col-sm-6 col-md-4 col-lg-4">
			<div class="d-flex ">
			    <ul style="list-style-type:none;">
			         <li>
			              <a id="performance-audit-btn" href="?page=cummulative_month_audit" title="report provides a look at how test counts vary from month to month" style="padding-top:5px;padding-bottom:5px;">
			                   <img src="images/count.png" style="margin-left:5px;height:45px;width:45px;" alt="Count audits icon">
			              </a>
			         </li>
			         <li><b>Cumulative Monthly Count Audit</b></li>
			         <li>This report provides a look at how test counts vary from month to month</li> 
			         
			    </ul>
			</div>
		</li>
		<li class="pb-5 col-xs-12 col-sm-6 col-md-4 col-lg-4">
			<div class="d-flex ">
			    <ul style="list-style-type:none;">
			         <li>
			              <a id="performance-audit-btn" href="?page=audit_report_test_vs_instrument" title="This report will provide test results for each instrument" style="padding-top:5px;padding-bottom:5px;">
			                   <img src="images/seq_instrument.png" style="margin-left:5px;height:45px;width:45px;" alt="Sequencing Instrument Icon">
			              </a>
			         </li>
			         <li><b>Test Result vs Instrument Report</b></li>
			         <li>This report will provide test results for each instrument.</li> 
			         
			    </ul>
			</div>
		</li>

		<li class="pb-5 col-xs-12 col-sm-6 col-md-4 col-lg-4">
			<div class="d-flex ">
			    <ul style="list-style-type:none;">
			         <li>
			              <a class="btn btn-primary btn-primary-hover" href="?page=performance_audit_worksheet&filter=months&month=<?=CURR_MONTH;?>&year=<?=CURR_YEAR;?>&tracked_tests=1" title="Use this button to edit turn around times" style="padding-top:5px;padding-bottom:5px;">
		                       Turn around time worksheet
		                  	</a>
			         </li>
			         <li><b>Turn around time worksheet</b></li>
			         <li></li> 
			         
			    </ul>
			</div>
		</li>
		<li class="pb-5 col-xs-12 col-sm-6 col-md-4 col-lg-4">
			<div class="d-flex ">
			    <ul style="list-style-type:none;">
			         <li>
			              <a class="btn btn-primary btn-primary-hover" href="?page=alter_default_tracked_tests" title="Use this button to edit which tests are checked automatically to be included in the tracked tests view." style="padding-top:5px;padding-bottom:5px;">
	                   Alter Default Tracked Tests
	              	</a>
			         </li>
			         <li><b>Alter Default Tracked Tests</b></li>
			         <li></li> 
			         
			    </ul>
			</div>
		</li>
     </ul>

     <hr class="section-mark">

<?php
     if (isset($user_permssions) && strpos($user_permssions, 'admin') !== false)
     {                        
?>
     <div class="row m-1">
          <h1>Admin Section</h1>
     </div>
     
<?php
     }
?>
</div>


 