<div class="container-fluid">
	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			<fieldset>
				<legend>Search Results</legend>
				<div class="row">
					<?php

						if (isset($search_result) && !empty($search_result))
						{
					?>
					<div style="overflow-x:auto;">						
						<table class="formated_table sort_table">
    		 					<thead>
    		 						<th>PDF</th>
								<?php

									$cols = array_keys($search_result[0]);									

				               		foreach ($cols as $key => $col_name)
				               		{


				               			$col_name = str_replace('_status', '', $col_name);
				               			// if the column isn't an id add column as visible otherwise hide column
				               			if (!$utils->InputAID($col_name) && $col_name !== 'first_name' && $col_name !== 'middle_name' && $col_name !== 'last_name')
				               			{
				               	?>

				               				<th><?= $utils->UnderscoreCaseToHumanReadable($col_name);?></th>
				               	<?php
				               			}	
				                     	}
				               	?>
    		 					</thead>
    		 					<tbody>

    		 						<?php

    		 							for ($i=0; $i < sizeOf($search_result); $i++)
    		 							{
    		 						?>
    		 								<tr>
    		 									<!-- add PDF link -->
    		 									<td>
    		 										<a href="?page=download_report&amp;visit_id=<?= $search_result[$i]['visit_id'];?>&amp;patient_id=<?= $search_result[$i]['patient_id'];?>&amp;run_id=<?= $search_result[$i]['run_id'];?>&amp;ngs_panel_id=<?= $search_result[$i]['ngs_panel_id'];?>" type="button" class="btn btn-success btn-success-hover" target="_blank"><img src="images/adobe-pdf-icon.png" alt="pdf icon" style="height:30px;width:30px"></a>


    		 									</td>

    		 						<?php
    		 								foreach ($search_result[$i] as $key => $val)
    		 								{
    		 									if (!$utils->InputAID($key) && $key !== 'first_name' && $key !== 'middle_name' && $key !== 'last_name')
    		 									{
								?>
												<td><?= $val;?></td>
								<?php         		 										
    		 									}  
    		 								}      		 									
    		 						?>		

    		 								</tr>
    		 						<?php
    		 							}
    		 						?>

    		 					</tbody>
    		 				</table>
         		 			
    		 			</div>
					<?php
						}
					?>
				</div>

			</fieldset>
		</div>

		<ul>
			<li>Not sure what other information the user might want from this list</li>
		</ul>
	</div>
</div>