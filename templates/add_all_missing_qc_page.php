<div class="container-fluid">
	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
<?php

	////////////////////////////////////////
	// Add All missing template pages
	///////////////////////////////////////
	$all_pages = $utils->getAllFilesInDir('templates');
	$added_page_count = 0;
	foreach($all_pages as $key => $page_name)
	{
		// avoid the index file
		if (strpos($page_name, '.php') !== False)
		{
			$page_name = str_replace('.php', '', $page_name);
		
			// check to see if the page is already in page_table
			$page_found = $qc_db->listAll('qc_db-qc-page-info-by-name', $page_name);
			
			if (empty($page_found))
			{
				$add_page_array = array();
				$add_page_array['page_name'] = $page_name;
				$add_page_array['website_name'] = SITE_TITLE;	

				$add_page_result = $qc_db->addOrModifyRecord('page_table',$add_page_array);

				$added_page_count++;

				if ($add_page_result[0])
				{
					echo 'The page '.$page_name.' was added. Make sure to update all page info on this page.<br><br>';
				}
			}
		}
	}

	if ($added_page_count === 0)
	{
		echo 'No pages are missing from validation document';
	}

?>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
			<a href="?page=all_qc_pages" class="btn btn-primary btn-primary-hover" role="button">
				<img src="images/browser.png" style="margin-left:5px;height:45px;width:45px;" meta="an icon of a fake webpage" alt="an icon of a fake webpage" >
				Go to all QC Pages
			</a>
		</div>
	</div>
</div>