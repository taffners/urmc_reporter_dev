<div class="container-fluid">
	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="height:500px;">
			<form id="add_patient_info" class="form" method="post" class="form-horizontal">
				<fieldset>
					<legend>
						Add A variant
					</legend>
					<div class="form-group row d-none">
						<div class="col-xs-12 col-sm-4 col-md-4">
							<label for="run_id" class="form-control-label">run id: <span class="required-field">*</span></label>
						</div>
						<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
							<input type="number" class="form-control" id="run_id" name="run_id" value="<?= $_GET['run_id'];?>" readonly="readonly" required/>
						</div>
					</div>
					<div class="form-group row">
						<div class="col-xs-12 col-sm-4 col-md-4">
							<label for="genes" class="form-control-label">Gene: <span class="required-field">*</span></label>
						</div>
						<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
							<select class="form-control" name="genes" id="genes">
								<option disabled selected value> -- Select a Gene -- </option>
						<?php								
								if (isset($gene_list))
								{
									foreach($gene_list as $key => $gene)
									{
						?>
								<option value="<?= $gene['genes'];?>"><?= $gene['genes'];?></option>
						<?php										
									}
								}
						?>								
							</select>
						</div>
					</div>
					<div class="form-group row">
						<div class="col-xs-12 col-sm-4 col-md-4">
							<label for="coding" class="form-control-label">Coding: <span class="required-field">*</span></label>
						</div>
						<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
							<input type="text" class="form-control" id="coding" name="coding" maxlength="255" required/>
						</div>
					</div>					
					<div class="form-group row">
						<div class="col-xs-12 col-sm-4 col-md-4">
							<label for="amino_acid_change" class="form-control-label">Protein: <span class="required-field">*</span></label>
						</div>
						<div class="col-xs-10 col-sm-6 col-md-6 pull-left">
							<input type="text" class="form-control" id="amino_acid_change" name="amino_acid_change" maxlength="255" required/>
						</div>
						<div class="col-xs-2 col-sm-2 col-md-2 pull-left">
							<input type="checkbox" class="form-check-input" name="unknown_protien" id="unknown_protien">
							<label class="form-control-label" for="unknown_protien">Protein Unknown</label>							
    						</div>
					</div>	
					<div class="form-group row">
						<div class="col-xs-12 col-sm-4 col-md-4">
							<label for="frequency" class="form-control-label">VAF (only include number): <span class="required-field">*</span></label>
						</div>
						<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
							<input type="text" class="form-control" id="frequency" name="frequency" maxlength="255" required/>
						</div>
					</div>
					<div class="form-group row">
						<div class="col-xs-12 col-sm-4 col-md-4">
							<label for="coverage" class="form-control-label">Coverage: <span class="required-field">*</span></label>
						</div>
						<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
							<input type="text" class="form-control" id="coverage" name="coverage" maxlength="255" required/>
						</div>
					</div>
					<div class="form-group row justify-content-between">
						<div class="offset-sm-4 offset-md-4 offset-lg-4 col-xs-2 col-sm-2 col-md-2 col-lg-2">
							<button type="submit" id="<?= $page;?>_submit" name="<?= $page;?>_submit" value="Submit"  class="btn btn-primary btn-primary-hover">
								Submit
							</button>
						</div>

						<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
					<?php
						if (isset($_GET['previous_page']) && $_GET['previous_page'] === 'check_tsv_upload')
						{
					?>
							<a href="?page=check_tsv_upload&amp;<?= EXTRA_GETS;?>&amp;num_variants=<?= $_GET['num_variants'];?>&amp;ngs_file_name=<?= $_GET['ngs_file_name'];?>" class="btn 
							btn-primary btn-primary-hover" role="button">
								❮ Previous
							</a>
					<?php
						}
						else if (isset($_GET['previous_page']) && $_GET['previous_page'] === 'verify_variants')
						{
					?>
							<a href="?page=verify_variants&amp;<?= EXTRA_GETS;?>&amp;variant_filter=included" role="button" class="btn 
							btn-primary btn-primary-hover">
								❮ Previous
							</a>
					<?php
						}
					?>
						</div>
					</div>

				</fieldset>
			</form>
		</div>
	</div>
</div>