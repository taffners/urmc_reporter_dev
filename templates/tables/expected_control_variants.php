					<h3 class="raised-card-subheader">Expected Variants</h3>

					<div class="row" style="overflow-x:auto;">
					     <table class="formated_table sort_table">
          					<thead>
          						<th>Expected Gene</th>
          						<th>Expected Coding</th>
          						<th>Expected Protein</th>
          						<th>Expected VAF Range</th>
          						<th>Observed Found Status</th>
          						<th>Observed VAF</th>
          						<th>Strand Bias</th>
<?php          						
     						if (isset($user_permssions) && strpos($user_permssions, 'director') !== false)
                    			{
?>
								<th>Update Expected Range</th>
<?php
                    			}
?>
          					</thead>
          					<tbody>
<?php
						$num_missing_variants = 0;
						$total_expected_count = 0;
						$total_found = 0;
						$total_failed_vaf = 0;
						if (isset($expected_control_variants) && !empty($expected_control_variants))
						{
							foreach ($expected_control_variants as $key => $var)
							{
								$total_expected_count++;
								// Found status
								if (empty($var['Observed_Gene']) && empty($var['Observed_Coding']) && empty($var['Observed_Protein']))
								{
									$found_status_class = 'alert-flagged-qc';
									$found_info = 'Not Found';
									$num_missing_variants++;
									$vaf_status_class = 'alert-flagged-qc';
									$sb = array(
					                    	'sb_class'	=>	'alert-na-qc',
					                    	'sb'			=>	'QC not done'
					                    );
								}
								else
								{
									$total_found++;
									$found_status_class = 'alert-passed-qc';
									$found_info = 'Found';
									$allele_with_SB = $utils->strandOddsRatio($var);
                    					$sb = $utils->strandBiasCellClass($allele_with_SB['strand_bias']);

									// find if vaf is within range
									if (floatval($var['Observed_VAF']) >= floatval($var['Expected_Min_VAF']) && floatval($var['Observed_VAF']) <= floatval($var['Expected_Max_VAF']))
									{
										$vaf_status_class = 'alert-passed-qc';
									}
									else
									{
										$total_failed_vaf++;
										$vaf_status_class = 'alert-flagged-qc';
									}

								}

?>
								<tr>
									<td><?=$var['Expected_Gene'];?></td>
									<?=$utils->toggleMoreLess($var['Expected_Coding'], 'coding', $key);?>
									<?=$utils->toggleMoreLess($var['Expected_Protein'], 'proten', $key);?>
									<td><?=$var['Expected_Min_VAF'];?>-<?=$var['Expected_Max_VAF'];?>%</td>
									<td class="<?= $found_status_class;?>"><?= $found_info;?></td>
									<td class="<?= $vaf_status_class;?>"><?=$var['Observed_VAF'];?></td>
									<td class="<?=$sb['sb_class'];?>"><?=$sb['sb'];?></td>
<?php          						
	     						if (isset($user_permssions) && strpos($user_permssions, 'director') !== false)
	                    			{
?>
									<td><a href="?page=update_control_range&expected_control_variants_id=<?=$var['expected_control_variants_id']?>&control_type_id=<?= $var['control_type_id'];?>" role="button" class="btn btn-primary btn-primary-hover">Update Expected<br>Range</a></td>
<?php
                    				}
?>									
								</tr>
<?php								
							}							
						}
?>          						
          					</tbody>
          				</table>
					</div>