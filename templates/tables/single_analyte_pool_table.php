<?php
	// Add Barcodes
	require 'vendor/autoload.php';

	// This will output the barcode as HTML output to display in the browser
	$generator = new Picqer\Barcode\BarcodeGeneratorPNG();
if (isset($samples_in_single_analyte_pool) && !empty($samples_in_single_analyte_pool))
{
	?>	<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">			
						
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
						<table id="sample-tests-overview-table" class="formated_table sort_table_no_inital_sort_no_paging" style="overflow-x:auto;margin-bottom: 10px;">
							<thead>
								<th>Sample Name</th>
								<th>Patient Name</th>
								<th>MRN</th>
								<th>Test Status</th>
								<th>Start Date</th>
								<th>Finish Date</th>
								
								<th class="d-print-none">Sign out<br>/<br>Build Report</th>
								<th class="d-print-none">Edit/Update Ordered Tests</th>

								<th>Summary</th>
																
							</thead>
							<tbody>
	<?php						
							foreach ($samples_in_single_analyte_pool as $key => $test)
							{			
	?>
								<tr>
									<td><img src="data:image/png;base64,<?=base64_encode($generator->getBarcode($test['sample_name'], $generator::TYPE_CODE_128));?>"><br><?= $test['sample_name'];?> </td>
									<td><?= $test['patient_name'];?></td>
									<td><?= $test['medical_record_num'];?></td>
									<td><?= $test['test_status'];?></td>
									<td><?= $test['start_date'];?></td>
									<td><?= $test['finish_date'];?></td>
									
									<td class="d-print-none">
	<?php
									if (isset($test['test_status']) && $test['test_status'] == 'complete')
									{
	?>								
										<span class="order-3 dot dot-green" title="Test Complete"></span>
	<?php										
									}
									else if (isset($test['test_status']) && $test['test_status'] == 'stop')
									{
	?>
										<span class="order-3 dot dot-red" title="Test Canceled"></span>
	<?php										
									}
									else if (isset($test['test_status']) && $test['test_status'] == 'Sent Out')
									{
	?>
										<span class="order-3 dot dot-grey" title="Test Sent Out"></span>
	<?php										
									}
									else if(isset($test['visit_id']) && !empty($test['visit_id']) && isset($test['assay_name']) && $test['assay_name'] === 'ngs')
									{
										echo 'See NGS report Section';
									}

									else if (isset($test['visit_id']) && !empty($test['visit_id']) && isset($test['assay_name']) && $test['assay_name'] === 'Idylla')
									{
	?>
										<a href="?page=add_idylla_data&run_id=<?= $test['run_id'];?>&visit_id=<?= $test['visit_id'];?>&patient_id=<?= $test['patient_id'];?>&single_analyte_pool_id=<?= $test['single_analyte_pool_id'];?>&orderable_tests_id=<?= $test['orderable_tests_id'];?>&ordered_test_id=<?= $test['ordered_test_id'];?>&sample_log_book_id=<?= $test['sample_log_book_id'];?>" class="btn btn-primary btn-primary-hover" title="Write report"> 
											Build Report
										</a>										
	<?php										
									}
									else
									{
	?>										
										<a href="?page=update_test&sample_log_book_id=<?= $test['sample_log_book_id'];?>&ordered_test_id=<?= $test['ordered_test_id'];?>&single_analyte_pool_id=<?= $test['single_analyte_pool_id'];?>" class="btn btn-primary btn-primary-hover" title="Change Test Status from pending to complete"> 
											<span class="order-1 dot dot-yellow" ></span>&nbsp;
											<span class="order-2 fas fa-arrow-right"></span>&nbsp;
											<span class="order-3 dot dot-green" ></span>

										</a>
	<?php										
									}
	?>										

									</td>
									<td class="d-print-none">
										<a href="?page=all_sample_tests&sample_log_book_id=<?= $test['sample_log_book_id'];?>" class="btn btn-primary btn-primary-hover"> Edit/Update<br>Print/ Cancel<br>Test</a>
									</td>
									<td>
	<?php

	/////////////////////////////////////////////////////////////////////////////// 
	// Add test summary info
	///////////////////////////////////////////////////////////////////////////////

									if (isset($test['test_status']) && $test['test_status'] === 'complete' )
									{
										// Add summary of completed test info
										$summary = '';								
									
										if (isset($test['test_result']) && !empty($test['test_result']))
										{
											$summary.= 'Test Result: '.$test['test_result'].'<br>';
										}

										// Call the db to find any users_performed_task_table.
										// Since there will not be many tests per sample this should be ok to call the db on each completed test.  It would be better to add to initial $testArray however, this will also make it very complicated because there are multiple types of ref tables represented in users_performed_task_table.  There's ordered_test_table ref and extraction_log_table ref

										// Add NGS test info
										if ($test['mol_num_required'] === 'yes')
										{
											// since wet bench is a required input I will obtain visit_id for 
											// a summary of the rest of the steps from here
											$wetbench = $db->listAll('users-wet-bench-ngs-tasks', $test['ordered_test_id']);

											if (!empty($wetbench))
											{
												// add link to report which hides when printed
												$href = '?page=download_report&amp;visit_id='.$wetbench[0]['visit_id'].'&patient_id='.$wetbench[0]['patient_id'].'&run_id='.$wetbench[0]['run_id'];

												if (isset($test['sample_log_book_id']))
												{
													$href.= '&sample_log_book_id='.$test['sample_log_book_id'];
												}

												if (isset($test['single_analyte_pool_id']))
												{
													$href.= '&single_analyte_pool_id='.$test['single_analyte_pool_id'];
												}

												if (isset($test['orderable_tests_id']))
												{
													$href.= '&orderable_tests_id='.$test['orderable_tests_id'];
												}

												if (isset($test['ordered_test_id']))
												{
													$href.= '&ordered_test_id='.$test['ordered_test_id'];
												}

												$summary.='<a href="'.$href.'" type="button" class="btn btn-success btn-success-hover d-print-none"><img src="images/adobe-pdf-icon.png" alt="pdf icon" style="height:30px;width:30px"></a><br>';

												$summary.='<b>Wet Bench:</b> <br>';
												foreach ($wetbench as $key => $tech)
												{
													$summary.=$tech['user_name'].'<br>';
												}

												$completed_steps = $db->listAll('all-analysis-techs', $wetbench[0]['visit_id']);
												
												$summary.='<b>Analysis:</b> <br>';

												$included_techs = '';

												foreach ($completed_steps as $key => $step)
												{

													$summary.= $step['user_name'].'<br>';
												}	
											}				
										}
										elseif ($test['extraction_info'] === 'no' && $test['quantification_info'] === 'no')
										{
											$non_extraction_users = $db->listAll('users-performed-tasks', $test['ordered_test_id']);
											foreach ($non_extraction_users as $key => $task_user)
											{
												$summary.= $task_user['task'].':'.$task_user['users'].'<br>';
											}

											if (isset($non_extraction_users[0]['instruments_used']) && !empty($non_extraction_users[0]['instruments_used']))
											{
												$summary.= 'Instrument used: '.$non_extraction_users[0]['instruments_used'].'<br>';
											}
										}

										// Add extraction tests
										else
										{
											$extraction_users = $db->listAll('extraction-users-tasks', $test['ordered_test_id']);

											if (isset($extraction_users[0]['extraction_method']) && !empty($extraction_users[0]['extraction_method']))
											{
												$summary.= 'Extraction Method: '.$extraction_users[0]['extraction_method'].'<br>';
											}

											if (isset($extraction_users[0]['quantification_method']) && !empty($extraction_users[0]['quantification_method']))
											{
												$summary.= 'Quant Method: '.$extraction_users[0]['quantification_method'].'<br>';
											}

											if (isset($extraction_users[0]['dna_conc_control_quant']) && !empty($extraction_users[0]['dna_conc_control_quant']))
											{
												$summary.= 'Quant Control: '.$extraction_users[0]['dna_conc_control_quant'].'ng/&mu;l<br>';
											}

											if (isset($extraction_users[0]['stock_conc']) && !empty($extraction_users[0]['stock_conc']))
											{
												$summary.= 'Stock Conc.: '.$extraction_users[0]['stock_conc'].'<br>';
											}

											if (isset($extraction_users[0]['dilution_exist']) && !empty($extraction_users[0]['dilution_exist']))
											{
												$summary.= 'Dilution Exists? '.$extraction_users[0]['dilution_exist'].'<br>';
											}

											if (isset($extraction_users[0]['dilutions_conc']) && !empty($extraction_users[0]['dilutions_conc']))
											{
												$summary.= 'Actual Dilution Conc.: '.$extraction_users[0]['dilutions_conc'].'<br>';
											}

											if (isset($extraction_users[0]['times_dilution']) && !empty($extraction_users[0]['times_dilution']))
											{
												$summary.= 'X Dilution: '.$extraction_users[0]['times_dilution'].'<br>';
											}

											if (isset($extraction_users[0]['purity']) && !empty($extraction_users[0]['purity']))
											{
												$summary.= '260/280: '.$extraction_users[0]['purity'].'<br>';
											}


											if (	
													!empty($extraction_users[0]['vol_dna']) && //v1
													!empty($extraction_users[0]['stock_conc']) && // c1
													!empty($extraction_users[0]['vol_eb']) && // 
													!empty($extraction_users[0]['dilution_final_vol']) && // v2
													!empty($extraction_users[0]['target_conc']) // c2
												)
											{
												$summary.='Dilution Calc C1 V1 = C2 V2: <br>';

												$summary.= '('.$extraction_users[0]['stock_conc'].')('.$extraction_users[0]['vol_dna'].') = ('.$extraction_users[0]['target_conc'].')('.$extraction_users[0]['dilution_final_vol'].')<br>';

												
												$summary.='EB Volume V2 - V1:<br>';

												$summary.= $extraction_users[0]['dilution_final_vol'].' - '.$extraction_users[0]['vol_dna'].' = '.$extraction_users[0]['vol_eb'].'<br>';
											}

											if (isset($extraction_users[0]['instruments_used']) && !empty($extraction_users[0]['instruments_used']))
											{
												$summary.= 'Instrument used: '.$extraction_users[0]['instruments_used'].'<br>';
											}

											foreach ($extraction_users as $key => $task_user)
											{
												$summary.= $task_user['task'].':'.$task_user['users'].'<br>';
											}
										}
										// Add any comments added under ordered_test_table
										if ($test['mol_num_required'] === 'yes')
										{
											$summary.= '<b>Comments:</b> Click PDF icon to obtain comments';
										}
										else
										{
											$comments = $db->listAll('ordered-test-concat-comments', $test['ordered_test_id']);
											if (isset($comments) && !empty($comments))
											{

												$summary.= '<b>Comments:</b><br>';
												foreach ($comments as $key => $comment)
												{	
													$summary.=$comment['problems'].'<br>';
												}
											}	
										}
																				
	?>
										<?=$summary;?>
	<?php

									}
	?>								
									</td>
								</tr>
	<?php
							}	
								
	?>
							</tbody>
						</table>
					</div>
				</div>
<?php
}
?>