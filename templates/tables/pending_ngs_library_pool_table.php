
				<div class="row" style="overflow-x:auto;">
               		<table class="formated_table" id="pending-ngs-library-pool-table">
                    		<thead  id="pending-ngs-library-pool-table-head">
                    			<th>Pool#</th>
                    			<th>Library Pool#s</th>
                    			<th>Excel Workbook#</th>
                    			<th>NGS Panel</th>
                    			<th>Count Visits</th>
                    			<th>Time Stamp</th>
                    			<th>User Name</th>
                    			<th>Status</th>
                    		</thead>
                    		<tbody>
<?php
	if (isset($pending_pools) && !empty($pending_pools))
	{
		foreach ($pending_pools as $key => $pool)
		{
?>
							<tr id="pool_<?= $pool['pool_number'];?>" <?php
							// for the home page the table row acts as a link 
							// therefore only add the class hover_success_on and 
							// open_pool if the page == home
							if ($_GET['page'] === 'home')
							{
								echo 'class="hover_success_on open_pool"';
							}
							?> data-ngs_panel_id="<?= $pool['ngs_panel_id'];?>">
								<td><?= $pool['pool_number'];?></td>
								<td><?= str_replace(', 0', '', $pool['library_pools']);?></td>
								<td><?= $pool['excel_workbook_num_id'];?></td>
								<td><?= $pool['ngs_panel_type'];?></td>
								<td><?= (intval($pool['count_chip_a']) + intval($pool['count_chip_b']));?></td>
								<td><?= $pool['time_stamp'];?></td>
								<td><?= $pool['user_name'];?></td>
								<td><?= $pool['status'];?></td>
							</tr>
<?php
		}
	}
?>                    			
                    		</tbody>
                    	</table>
                    </div>
