<?php
	

	// add the variants which are pending here otherwise no confirmations necessary
	if ($curr_step_complete)
	{
?>
	<table class="formated_table" style="margin-bottom:20px;">
		<thead>
	<?php

		if (sizeOf($completed_confirmation_status) > 0 && isset($completed_confirmation_status))
		{
			foreach($completed_confirmation_status[0] as $key =>$val)
			{
	?>
				<th><?= $utils->UnderscoreCaseToHumanReadable($key);?></th>
	<?php
			}
		}
	?>							
		</thead>
		<tbody>
	<?php
		if (sizeOf($completed_confirmation_status) > 0 && isset($completed_confirmation_status))
		{
			for($i = 0; $i < sizeOf($completed_confirmation_status); $i++)
			{
	?>
				<tr>
	<?php
					foreach($completed_confirmation_status[$i] as $key => $value)
					{
	?>
						<td><?= $value;?></td>
	<?php
					}
	?>
				</tr>
	<?php
			}
		}
	?>
		</tbody>
	</table>
<?php
	}
?>