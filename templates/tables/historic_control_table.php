			<div class="row" style="overflow-x:auto;">
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				    <table class="formated_table sort_table_no_inital_sort dataTable">
     					<thead>
     						<th>Gene</th>
     						<th>Coding</th>
     						<th>Protein</th>
     						<th>Mean VAF</th>
     						<th>Min VAF</th>
     						<th>Max VAF</th>
     						<th>Standard Deviation (std)</th>
     						<th>Standard Error</th>
     						<th>+/- 2 std from Mean</th>
     						<th>Observed Count</th>
     						<th>Current Range</th>
     						<th>Tracked</th>
     					</thead>
     					<tbody>
<?php
	if (isset($summary_control_vaf))
	{
		foreach ($summary_control_vaf as $key => $var)
		{
			if (empty($var['expected_gene']))
			{
				$tracked = '<td class="alert-na-qc">X</td>';
			}
			else
			{
				$tracked = '<td class="alert-passed-qc">&#x2714;</td>';
			}

			$neg_std_range = round($var['avg_freq'] - ($var['standard_deviation'] * 2),2);
			$pos_std_range = round($var['avg_freq'] + ($var['standard_deviation'] * 2),2);

			if (empty($var['min_accepted_frequency']) || empty($var['max_accepted_frequency']))
			{
				$current_range = '<td class="alert-na-qc">X</td>';
			}
			else
			{
				$current_range = '<td>'.$var['min_accepted_frequency'].' - '.$var['max_accepted_frequency'].'%</td>';
			}
			

?>				
							<tr>
								<td><?=$var['genes'];?></td>
								<?=$utils->toggleMoreLess($var['coding'], 'coding', $key);?>
								<?=$utils->toggleMoreLess($var['amino_acid_change'], 'amino_acid_change', $key);?>
								<td><?=round(floatval($var['avg_freq']), 1);?></td>
								<td><?=round(floatval($var['min']), 1);?></td>
								<td><?=round(floatval($var['max']), 1);?></td>
								<td><?= round($var['standard_deviation'],3);?></td>
								<td><?= round(($var['standard_deviation'] / sqrt($var['observed_count'])), 2);?></td>
								<td><?= $neg_std_range;?> - <?= $pos_std_range;?>%</td>
								<td><?=$var['observed_count'];?></td>
								<?= $current_range;?>
								<?= $tracked;?>
							</tr>
<?php
		}
	}
?>     				
     					</tbody>
     				</table>
     			</div>
     		</div>