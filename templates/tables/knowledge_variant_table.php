<!-- 
	requirements:
		- knowledge_base array including all variant info from the knowledge base
		- utils class 
		- db class

 -->
<!-- Find each variant in the knowledge_base array make a new table -->
<?php

     for($i=0;$i<sizeof($knowledge_base);$i++)
     {
?>
<div class="row raised-card" style="overflow-x:auto;margin-bottom:15px;">

     <h3 class="raised-card-subheader">
          <?= $knowledge_base[$i]['genes']; ?>
          <?= $knowledge_base[$i]['coding']; ?>
          (<?= $knowledge_base[$i]['amino_acid_change']; ?>)
     </h3>
     <table class="formated_table sort_table">

          <thead id="knowledge_id_<?= $knowledge_base[$i]['knowledge_id']; ?>">
          	<th>Field</th>
          	<th>Data</th>

          </thead>

          <tbody>
          	<!-- Add all tier information in one row -->

          	<?php
     			$tier_info = $db->listAll('knowledge-tier', $knowledge_base[$i]['knowledge_id']);		

     			// sort tiers by tier level then tissue type
     			$sorted_tier_info = $utils->DoubleAscSort('tier', 'tissue', $tier_info);

     			$tier_str = '';               
     			// build compressed all tier tissue string	
     			foreach($sorted_tier_info as $tier_key => $tier_value)
     			{
     				$tier_str .= $tier_value['tier'].':'.$tier_value['tissue'].' ';

     			}
          	?>
          	<tr>
          		<td>Tier</td>
          		<td><?= $tier_str ?></td>

          	</tr>

          	<?php
          		foreach ($knowledge_base[$i] as $key => $value)
          		{
          			// Skip displaying any of these items in the table
          			if($key != 'knowledge_id' && $key != 'time_stamp' && $key != 'amino_acid_change' && $key != 'coding')
          			{
     		?>

          		<tr>
          			<td><?= $key;?></td>
          			<td><?= $utils->ConvertDatabaseVar($key, $value); ?></td>
          			
          		</tr>

          	<?php
          			}
          		}
          	?>

          </tbody>

     </table>
</div> 
<?php

    }

?>