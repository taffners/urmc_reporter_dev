<?php
		/////////////////////////////////////////////////////////////////////////////////
		// Iterate over all of the information in the $qc_data array
		// array (size=2)
			//   'A' => 
			//     array (size=4)
			//       'cutoffs' => 
			//         array (size=12)
			//       'header' => 
			//         array (size=5)
			//       'lots' => 
			//         array (size=25)
			//       'versions' => 
			//         array (size=18)
			// 		'cutoff_pass' => boolean true
			// 		'errors'
			//   'B' => 
			//     array (size=4)
			//       'cutoffs' => 
			//         array (size=12)        
			//       'header' => 
			//         array (size=5)
			//       'lots' => 
			//         array (size=25)
			//       'versions' => 
			//         array (size=18)
			// 		'cutoff_pass' => boolean true
			// 		'errors'
		/////////////////////////////////////////////////////////////////////////////////
		$errors_found = False;

	if (isset($qc_data))
	{
		// find all chips in the qc_data array
		foreach ($qc_data as $chip => $chip_info)
		{

?>
			<fieldset>
				<legend>
					<h4>Report Name: <?= $chip_info['header']['results_name'];?></h4>
				</legend>
<?php
			// make sure no errors were found while obtaining info from API and files
			// before proceeding
			if (isset($qc_data[$chip]['errors']) && !empty($qc_data[$chip]['errors']))
			{
				$errors_found = True;
				$torrent_server_href = 'http://'.$API_Utils->GetTorrentServerWebSite().'/data/#table';
?>
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 alert alert-danger">
						<h1>ERRORS FOUND WITH CHIP <?= $chip;?></h1>

						<h2><a href="<?= $torrent_server_href;?>" target="_blank">Click HERE: to assess issues further.</a> </h2>
						<ul>

<?php
				foreach($qc_data[$chip]['errors'] as $err_type => $err_message)
				{
?>
							<li><?= $err_message;?></li>
<?php
				}
?>
						</ul>
					</div>
				</div>
<?php
			}

			else
			{

			// Update header['backup_location'] to the tech version
			if (isset($chip_info['header']['backup_location']))
			{
				$chip_info['header']['backup_location'] = $API_Utils->ConvertBackupLocToTechFormat($chip_info['header']['backup_location']);	
			}

			$sections = array('header', 'cutoffs', 'sample_qc', 'lots', 'versions');
			// find all array sections
			foreach ($sections as $section_key => $section)
			{

				$num = 0;
?>
				<!-- Section header -->
				<div class="row" style="margin-bottom:5px;" id="header_<?=$chip;?>_<?=$section;?>_<?=$num;?>_<?=$num % 2;?>">	
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" >			
						<h4><?= $utils->UnderscoreCaseToHumanReadable($section);?></h4>
					</div>
				</div>
<?php				

				foreach($chip_info[$section] as $val_name => $val)
				{		
								
					// add header of sample_qc  table
					if ($section === 'sample_qc' && $num == 0)
					{						
?>
				<div class="row" style="margin-bottom:5px;" id="table_<?=$chip;?>_<?=$section;?>_<?=$num;?>_<?=$num % 2;?>">	
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" >	
						<table class="formated_table">
							<thead>
								<th>Sample Name</th>
								<th>Mapped Reads</th>
								<th>On Target</th>
								<th>Mean Depth</th>
								<th>Uniformity</th>
							</thead>					
<?php				
					}

					if ($section === 'sample_qc')
					{
						
?>
						<tbody>
							<tr>
								<td><?=$val['Sample Name'];?></td>
								<td><?=number_format($val['Mapped Reads']);?></td>
								<td><?=$val['On Target'];?></td>
								<td><?=number_format($val['Mean Depth']);?></td>
								<td><?=$val['Uniformity'];?></td>
							</tr>
						</tbody>
<?php
					}


					///////////////////////////////////////////////
					// Add all sections besides cutoff and sample_qc in a two column list
					///////////////////////////////////////////////
					else if ($section !== 'cutoffs' && $num % 2 == 0)
					{
?>
				<div class="row" style="margin-bottom:2px;" id="row_<?=$chip;?>_<?=$section;?>_<?=$num;?>_<?=$num % 2;?>">

					<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6" id="row1_<?=$chip;?>_<?=$section;?>_<?=$num;?>_<?=$num % 2;?>">

						<?= $utils->UnderscoreCaseToHumanReadable($val_name);?>: <b><?= $val;?></b>
					</div>
<?php
					}
					else if ($section !== 'cutoffs' && $num % 2 != 0)
					{
?> 
					<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6" id="row2_<?=$chip;?>_<?=$section;?>_<?=$num;?>_<?=$num % 2;?>">
						<?= $utils->UnderscoreCaseToHumanReadable($val_name);?>: <b><?= $val;?></b>
					</div>
				</div>
	<?php						
					}

					///////////////////////////////////////////////
					// Add cutoff table
					///////////////////////////////////////////////
					else if ($section === 'cutoffs' && $num === 0)
					{					
?>
				<div class="row" style="margin-bottom:5px;" id="table_<?=$chip;?>_<?=$section;?>_<?=$num;?>_<?=$num % 2;?>">	
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" >	
						<table class="formated_table">
							<thead>
								<th>Metric</th>
								<th>Range</th>
								<th>Observed</th>
							</thead>
							<tbody>
								<tr>
									<td><?= $utils->UnderscoreCaseToHumanReadable($val_name);?></td>
									<td><?= $val['range'];?></td>
									<td class="<?php 
										// add class 
										if ($val['status'] == 'pass')
										{
											echo 'green';
										}
										else
										{
											echo 'red';
										}?>-background">
										<?php
										// Add commas to numbers if necessary
										if ($val['val'] >= 1000)
										{
											echo number_format($val['val']).' '.$val['unit'];
										}
										else
										{
											echo $val['val'].' '.$val['unit'];
										}
										?>
												
									</td>

								</tr>
<?php						
					}
					else if ($section === 'cutoffs' && $num !== 0)
					{
?>						
								<tr>
									<td><?= $utils->UnderscoreCaseToHumanReadable($val_name);?></td>
									<td><?= $val['range'];?></td>
									<td class="<?php 
										// add class 
										if ($val['status'] == 'pass')
										{
											echo 'green';
										}
										else
										{
											echo 'red';
										}?>-background">
										<?php
										// Add commas to numbers if necessary
										if ($val['val'] >= 1000)
										{
											echo number_format($val['val']).' '.$val['unit'];
										}
										else
										{
											echo $val['val'].' '.$val['unit'];
										}
										?>
												
									</td>
								</tr>
<?php						
					}
					$num++;						
				}

				// if there were only one val in the current row close it out
				if ($num % 2 != 0 && $section != 'cutoffs' && $section != 'sample_qc')
				{
?>
				</div>
<?php					
				}
				else if ($section === 'cutoffs' || $section === 'sample_qc')
				{
?>
							<!-- close table -->
							</tbody>
						</table>
					</div>
				</div>
<?php					
				}				
			}

			if ($page === 'pool_qc_backup_data')
			{

				// $chip_info['cutoff_pass'] is a bool where True means all run cutoffs are meant. False means at least one cutoff is not meant.  
				// $chip_info['cutoff_pass'] is assessed every time this page loads by accessing the torrent server API
				// $chip_status stores pass or failed.  It is created by accessing infotrack database.  This is used for updating the form.
				if 	(
						$chip_info['cutoff_pass'] ||
						(
							isset($chip_status[$chip]) && $chip_status[$chip] === 'pass'
						)
					)
				{
					$rowClass = 'alert alert-success';	
				}
				else
				{
					$rowClass = 'alert alert-danger';
				}
?>			
				<div class="form-group row <?= $rowClass;?>" style="font-size: 20px;">
					<div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
						<label for="run_qc_status_<?= $chip;?>" class="form-control-label">Run QC chip <?= $chip;?>: <span class="required-field">*</span></label>
					</div>
					<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
						<div class="radio">
							<label class="radio-inline"><input type="radio" name="run_qc_status_<?= $chip;?>" required="required" value="pass" <?php
							// default to what was previously chosen if not empty
							if (isset($chip_status[$chip]) && $chip_status[$chip] === 'pass')
							{
								echo 'checked';
							}
							else if($chip_info['cutoff_pass'] && !(isset($chip_status[$chip]) && $chip_status[$chip] === 'failed'))
							{
								echo 'checked'; 								
							}?>>Pass</label>
						
							<label class="radio-inline"><input type="radio" name="run_qc_status_<?= $chip;?>" required="required" value="failed" <?php
							// default to what was previously chosen if not empty
							if (isset($chip_status[$chip]) && $chip_status[$chip] === 'failed')
							{
								echo 'checked';
							}
							else if(!$chip_info['cutoff_pass'] && !(isset($chip_status[$chip]) && $chip_status[$chip] === 'pass'))
							{
								echo 'checked'; 								
							}?>>Failed</label>
						</div>
					</div>
				</div>
<?php
			}
			}
?>					
			</fieldset>
<?php		
					
		}
	} // close for isset qc_data
?>