
<div class="row">
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

		<div>
	<?php 

		if(isset($num_snvs) && $num_snvs === 0  && !$extraGenesRequired && isset($full_interpt))
		{
	?>
		<span class="report-section-bold">
			INTERPRETATION: 			
		</span>
		<div>
			<?= $full_interpt;?>				
		</div>
<?php
	}
	else if (isset($reportMutantTables))
	{		
		/////////////////////////////////
		// Tests reports that were built via hotspot_variants should only provide Gene, DNA, and Protein in table
		// and not include the warning about the gene not being in the test.  NGS tests store genes in the 
		// genes_covered_by_panel_table while hotspot variants store this info in test_hotspots_table since they 
		// are tracking specific variants instead of possible genes. Currently VAF and Tier are not being provided
		// for any hotspot_variant tests.  If this changes this method will have to be reworked.  Adding 
		// hotspot_variant_test to the exclude_cols column allows an easier way to turn off warning about subset
		//  genes
		/////////////////////////////////
		if (
				isset($orderableTestArray[0]['hotspot_variants']) && 
				!empty($orderableTestArray[0]['hotspot_variants']) &&
				$orderableTestArray[0]['hotspot_variants'] === 'yes'
			)
		{
			$exlcude_cols = array('VAF', 'Tier', 'variant_interpt', 'activate_status', 'hotspot_variant_test');
		}
		else
		{
			$exlcude_cols = array('variant_interpt', 'activate_status', 'genetic_call');
		}

		foreach ($reportMutantTables as $tableHeader => $rows)
		{
		
			if (!empty($rows[0]))
			{
			// get section table header
?>
			<span class="report-section-bold">
				<?= $tableHeader;?>: 
			</span>
	<?php
				// make table head
				if (isset($rows[0][0]))
				{
					$tableHead = array_keys($rows[0][0]);
				}

			// Only include notes about gene in subset if it is not a hotspot test.
			if (!in_array('hotspot_variant_test', $exlcude_cols))
			{
	?>
			<div class="alert alert-info">
				Mutations in genes with <span class="alert-danger fas fa-exclamation-circle" title="Gene Not included in Assay"></span> are not included in gene subset for this assay. All mutations that appear in the table below will appear in the final report.  <?= SITE_TITLE;?> is set up this way to allow for user flexibility.  To remove a variant that is not included in the assay subset of genes go to verify variants and toggle the include in report check box. 
			</div>
	<?php
			}
	?>
			<table class="formated_table white_table" style="margin-bottom: 10px;">
				<thead>
	<?php

				if (isset($tableHead))
				{

					foreach($tableHead as $th)
					{
						if  (
								!$utils->InputAID($th) && !in_array($th, $exlcude_cols))
						{
	?>
					<th><?= $utils->UnderscoreCaseToHumanReadable($th);?></th>
	<?php 

						}
					}
				}
	?>
				</thead>

				<tbody>
	<?php
				if (isset($tableHead) && isset($rows[0]))
				{
					foreach($rows[0] as $row)
					{
	?>
					<tr>
	<?php
						foreach($tableHead as $th)
						{
							// notify user that the gene is not in the assay for non hotspot tests
							if 	(
									!$utils->InputAID($th) && // used to skip over database ids
									$th === 'Gene' && 
									!in_array($th, $exlcude_cols) && 
									!in_array('hotspot_variant_test', $exlcude_cols) &&

									isset($row['activate_status']) && 
									!$row['activate_status']
								)
							{
	?>
						<td><span class="alert-danger fas fa-exclamation-circle" title="Gene Not included in Assay"></span> <?= $row[$th];?> </td>
	<?php
							}							

							else if (!$utils->InputAID($th) && !in_array($th, $exlcude_cols))
							{
	?>				
						<td><?= $row[$th];?></td>
	<?php
							}
						}
	?>
					</tr>
	<?php									
					}
				}


	?>
				</tbody>
			</table>
	<?php

			}
		}
	}			
	?>
		</div>
	</div>
</div>
