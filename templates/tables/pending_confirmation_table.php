<table class="formated_table sort_table" style="overflow-x:auto;">
	<thead>
	<?php
		if (isset($pending_confirmations) && !empty($pending_confirmations))
		{
			
	?>
				<th>Genes</th>
				<th>Coding</th>
				<th>Amino Acid Change</th>
				<th>Mutation Type</th>
				<th>Confirmation Status</th>
				<th>NGS Panel</th>
				<th>User Name</th>
				<th>Update Confirmation</th>
				<th>Report</th>
	<?php
		
		}
		else
		{
	?>
			There are no current pending confirmations
	<?php
		}
	?>	
	</thead>
	<tbody>
	<?php
		if (isset($pending_confirmations) && !empty($pending_confirmations))
		{
			foreach($pending_confirmations as $key => $confirm)
			{
	?>
				<tr>
					<td><?=$confirm['genes'];?></td>
					<?=$utils->toggleMoreLess($confirm['coding'], 'coding', $key);?>
					<?=$utils->toggleMoreLess($confirm['amino_acid_change'], 'amino_acid_change', $key);?>
					<td><?=$confirm['mutation_type'];?></td>
					<td><?=$confirm['confirmation_status'];?></td>
					<td><?=$confirm['type'];?></td>
					<td><?=$confirm['user_name'];?></td>
					<td>
						<a type="button" class="btn btn-primary btn-primary-hover" href="?page=update_confirmation_status&confirm_id=<?=$confirm['confirm_id']?>">Update Confirmation</a>
					</td>
					<td>
						<a href="?page=download_report&visit_id=<?=$confirm['visit_id'];?>&patient_id=<?=$confirm['patient_id'];?>&run_id=<?=$confirm['run_id'];?>" type="button" class="btn btn-success btn-success-hover"><img src="images/adobe-pdf-icon.png" alt="pdf icon" style="height:30px;width:30px"></a>
					</td>
				</tr>
	<?php
			}
		}
	?>
	</tbody>
</table>