<?php
	if (!isset($_GET['page_id']))
	{
		$message = 'no page was provided';
	}
	else
	{
?>

<div class="container-fluid">
	<div class="row">
		
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			<a href="?page=<?= $pageArray[0]['page_name'];?>" role="button" target="_blank" class="btn btn-primary btn-primary-hover">
				Open the validation page in new tab
			</a>
		</div>
	</div>

	<div class="row">
		
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			<form class="form" method="post" class="form-horizontal">
				<fieldset>
					<legend>
						Add a validation
					</legend>

					<div class="form-group row">
						<div class="col-xs-12 col-sm-4 col-md-4">
							<label for="user_name" class="form-control-label">User Name:</label>
						</div>
						<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
							<input type="text" class="form-control" id="user_name" maxlength="100" name="user_name" value="<?= $_SESSION['user']['first_name'].' '.$_SESSION['user']['last_name'];?>" readonly/>
						</div>
					</div>

					<div class="form-group row">
						<div class="col-xs-12 col-sm-4 col-md-4">
							<label for="browser_version" class="form-control-label">Browser Version:</label>
						</div>
						<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
							<input type="text" class="form-control" id="browser_version" maxlength="100" name="browser_version" readonly/>
						</div>
					</div>

					<div class="form-group row">
						<div class="col-xs-12 col-sm-4 col-md-4">
							<label for="website_version" class="form-control-label">Website Version:</label>
						</div>
						<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
							<input type="text" class="form-control" id="website_version" maxlength="50" name="website_version" value="<?= SITE_TITLE.' '.VERSION;?>" readonly/>
						</div>
					</div>

					

				<?php
					/////////////////////////////////
					// Add all fields 
					/////////////////////////////////
					$category = $all_qc_checklist_items[0]['category'];
				?>
					<hr class="plain-section-mark">
					<?= $category;?>
					<hr class="plain-section-mark">
				<?php
					

					foreach ($all_qc_checklist_items as $key => $item)
					{
						if (
								$item['category'] === 'Form Testing' && 
								$pageArray[0]['page_type'] !== 'Form' 
							)
						{
							continue;
						}

						else if (
									$item['category'] === 'Report Testing' && 
									$pageArray[0]['page_type'] !== 'Report' 
								)
						{
							continue;
						}

						if ($item['category'] !== $category)
						{
							$category = $item['category'];
				?>
							<hr class="plain-section-mark">
							<?= $category;?>
							<hr class="plain-section-mark">
				<?php
						}


						if ($item['field_type'] === 'yes_no')
						{
				?>
							<div class="form-group row">
								<div class="col-xs-12 col-sm-4 col-md-4">
									<label for="actual_result[<?= $item['check_list_validation_step_id'];?>]" class="form-control-label">
										<?= $item['step_name'];?> 
										<span class="required-field">*</span>

									</label>
							<?php
								$description = str_replace(' ', '', $item['description']);

								if (!empty($description))
								{
							?>
									(<?= $item['description'];?>)
							<?php									
								}
							?>									
									
								</div>
								<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
									<div class="radio">
										<label class="radio-inline"><input type="radio" name="actual_result[<?= $item['check_list_validation_step_id'];?>]" value="Yes" required="required">Yes</label>
									
										<label class="radio-inline"><input type="radio" name="actual_result[<?= $item['check_list_validation_step_id'];?>]" value="No" required="required">No</label>	

										<label class="radio-inline"><input type="radio" name="actual_result[<?= $item['check_list_validation_step_id'];?>]" value="N/A" required="required">N/A</label>									
									</div>
								</div>
							</div>
				<?php
						}
						else
						{
				?>
							<div class="form-group row">
								<div class="col-xs-12 col-sm-4 col-md-4">
									<label for="actual_result[<?= $item['check_list_validation_step_id'];?>]" class="form-control-label"><?= $item['step_name'];?></label>
								</div>

								<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
									<textarea class="form-control" name="actual_result[<?= $item['check_list_validation_step_id'];?>]" rows="4" maxlength="65535" style="margin-bottom: 10px;"></textarea>
								</div>
							</div>
				<?php
						}

					}

				?>
					<hr class="plain-section-mark">
					Validation Results
					<hr class="plain-section-mark">
					<div class="form-group row">
						<div class="col-xs-12 col-sm-4 col-md-4">
							<label for="out_come" class="form-control-label">Validation Out Come:</label>
						</div>

						<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
							<div class="radio">
								<label class="radio-inline"><input type="radio" name="out_come" value="Passed" required="required">Passed</label>
							
								<label class="radio-inline"><input type="radio" name="out_come" value="Failed" required="required">Failed</label>					
							</div>
						</div>
					</div>

					<div class="form-group row">
						<div class="col-xs-12 col-sm-4 col-md-4">
							<label for="comment" class="form-control-label">Comments About this Validation.  Is this a new Validation or another validation?  Any bugs that caused this validation?  Any problems uncovered during this validation?</label>
						</div>

						<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
							<textarea class="form-control" name="comment" rows="4" maxlength="65535" style="margin-bottom: 10px;"></textarea>
						</div>
					</div>

					<div class="form-group row">
						<div class="offset-xs-4 offset-sm-4 offset-md-4 offset-lg-4 col-xs-2 col-sm-2 col-md-2 col-lg-2">
							<button type="submit" id="<?= $page;?>_submit" name="<?= $page;?>_submit" value="Submit"  class="btn btn-primary btn-primary-hover">
								Submit
							</button>
						</div>
					</div>
				</fieldset>
			</form>
		</div>
	</div>
</div>
<?php
	}
?>