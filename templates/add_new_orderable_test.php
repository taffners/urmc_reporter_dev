
<div class="container-fluid">

	<div class="row">
		
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			<form class="form" method="post" class="form-horizontal">
				<fieldset>
				<?php 
					if (isset($_GET['orderable_tests_id']))					
					{
				?>
					<legend>
						Update Orderable Tests
					</legend>
					
				<?php 
					}
					else
					{
				?>
					<legend>
						Add a New Orderable Test
					</legend>
				<?php
					}
				?>					
					<div class="row">
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 alert alert-info">
							<strong>NGS tests can not be added via this page.  Please contact a <?= SITE_TITLE;?> admin.</strong>
						</div>
					</div>
					<div class="form-group row">
						<div class="col-xs-12 col-sm-4 col-md-4">
							<label for="assay_type_id" class="form-control-label">Assay Type: <span class="required-field">*</span></label>
						</div>

						
						<div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">
						<?php

						if (isset($_GET['orderable_tests_id']))
						{
						?>

						
							<input type="text" class="form-control d-none" name="assay_type_id" id="assay_type_id" maxlength="20" value="<?= $utils->GetValueForUpdateInput($orderableTestArray, 'assay_type_id');?>"/>
							<?= $orderableTestArray[0]['assay_name'];?>
						<?php
						}
						else
						{
						?>


							<select class="selectpicker" data-live-search="true" data-width="100%" name="assay_type_id" id="assay_type_id" required="required">

								<option value="" selected="selected"></option>
							<?php

								for($i=0;$i<sizeof($allAssayTypes);$i++)
		               			{	
			               
			             
			          				if 	(
			          						isset($allAssayTypes[$i]['assay_name']) && 
			          						isset($allAssayTypes[$i]['assay_type_id'])
			          					)
			          				{
								?>
											<option value="<?= $allAssayTypes[$i]['assay_type_id']; ?>">
												<?= $allAssayTypes[$i]['assay_name'];?>
											</option>
								<?php
										}
									}
								?>
							</select>
						<?php
						}
						?>								
						</div>
						
					</div>

					<div class="form-group row">
						<div class="col-xs-12 col-sm-4 col-md-4">
							<label for="test_name" class="form-control-label" >Test Short Name (example: TCRG): <span class="required-field">*</span></label>
						</div>

						<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
						<?php

						if (isset($_GET['orderable_tests_id']))
						{
						?>

						
							<input type="text" class="form-control d-none" id="test_name" name="test_name" maxlength="20" value="<?= $utils->GetValueForUpdateInput($orderableTestArray, 'test_name');?>" />
							<?= $orderableTestArray[0]['test_name'];?>
						<?php
						}
						else
						{
						?>							
							<input type="text" class="form-control" id="test_name" name="test_name" maxlength="20" value="<?= $utils->GetValueForUpdateInput($orderableTestArray, 'test_name');?>" required="required"/>
						<?php
						}
						?>
						</div>
					</div>
					<div class="form-group row">
						<div class="col-xs-12 col-sm-4 col-md-4">
							<label for="full_test_name" class="form-control-label">Full Test Name (TCR gamma (T-Cell Receptor)): <span class="required-field">*</span></label>
						</div>
						<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
						<?php

						if (isset($_GET['orderable_tests_id']))
						{
						?>

						
							<input type="text" class="form-control d-none" id="full_test_name" name="full_test_name" maxlength="20" value="<?= $utils->GetValueForUpdateInput($orderableTestArray, 'full_test_name');?>"/>
							<?= $orderableTestArray[0]['full_test_name'];?>
						<?php
						}
						else
						{
						?>
							<input type="text" class="form-control" id="full_test_name" name="full_test_name" maxlength="100" value="<?= $utils->GetValueForUpdateInput($orderableTestArray, 'full_test_name');?>" required="required"/>
						<?php
						}
						?>
						</div>
					</div>
					<div class="form-group row">
						<div class="col-xs-12 col-sm-4 col-md-4">
							<label for="test_type" class="form-control-label">Test Type: <span class="required-field">*</span></label>
						</div>
						<div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">
						<?php

						if (isset($_GET['orderable_tests_id']))
						{
						?>

						
							<input type="text" class="form-control d-none" id="test_type" name="test_type" maxlength="20" value="<?= $utils->GetValueForUpdateInput($orderableTestArray, 'test_type');?>"/>
							<?= $orderableTestArray[0]['test_type'];?>
						<?php
						}
						else
						{
						?>
							<select class="selectpicker" data-live-search="true" data-width="100%" name="test_type" id="test_type" required="required">

								<option value="" selected="selected"></option>
							<?php

								for($i=0;$i<sizeof($allTestTypes);$i++)
		               			{	
			               
			             
			          				if 	(
			          						isset($allTestTypes[$i]['test_type'])
			          					)
			          				{
								?>
											<option value="<?= $allTestTypes[$i]['test_type']; ?>">
												<?= $allTestTypes[$i]['test_type'];?>
											</option>
								<?php
										}
									}
								?>
							</select>	
						<?php
						}
						?>							
						</div>
					</div>
					<div class="form-group row">
						<div class="col-xs-12 col-sm-4 col-md-4">
							<label for="test_status" class="form-control-label">Test Status: <span class="required-field">*</span></label>
						</div>
						<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
							<div class="radio">
								<label class="radio-inline"><input type="radio" name="test_status" value="Active" required="required" <?= $utils->DefaultRadioBtn($orderableTestArray, 'test_status', 'Active');?>>Active</label>
							
								<label class="radio-inline"><input type="radio" name="test_status" value="Not Active"  <?= $utils->GetValueForUpdateRadioCBX($orderableTestArray, 'test_status', 'Not Active');?>>Not Active</label>

							</div>
						</div>
					</div>
					
					<div class="form-group row">
						<div class="col-xs-12 col-sm-4 col-md-4">
							<label for="notes_for_work_sheet" class="form-control-label">Test Custom Instructions/Notes:</label>
						</div>

						<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
							<textarea class="form-control" name="notes_for_work_sheet" rows="4" maxlength="65535" style="margin-bottom: 10px;"><?= $utils->GetValueForUpdateInput($orderableTestArray, 'notes_for_work_sheet');?></textarea>
						</div>
					</div>
					<div id="all-non-sendout-fields">
						<hr class="plain-section-mark">
						<div class="raised-card-subheader">
							Work Sheet
						</div>
						<hr class="plain-section-mark">
						<div class="form-group row">
							<div class="col-xs-12 col-sm-4 col-md-4">
								<label for="consent_required" class="form-control-label">Consent Required: <span class="required-field">*</span></label>
							</div>
							<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
								<div class="radio">
									<label class="radio-inline"><input type="radio" name="consent_required" value="yes" required="required" <?= $utils->GetValueForUpdateRadioCBX($orderableTestArray, 'consent_required', 'yes');?>>Yes</label>
								
									<label class="radio-inline"><input type="radio" name="consent_required" value="no"  <?= $utils->DefaultRadioBtn($orderableTestArray, 'consent_required', 'no');?>>No</label>
								</div>
							</div>
						</div>

						<div class="form-group row">
							<div class="col-xs-12 col-sm-4 col-md-4">
								<label for="previous_positive_required" class="form-control-label">Previous Positive Required: <span class="required-field">*</span></label>
							</div>
							<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
								<div class="radio">
									<label class="radio-inline"><input type="radio" name="previous_positive_required" value="yes" required="required" <?= $utils->GetValueForUpdateRadioCBX($orderableTestArray, 'previous_positive_required', 'yes');?>>Yes</label>
								
									<label class="radio-inline"><input type="radio" name="previous_positive_required" value="no"  <?= $utils->DefaultRadioBtn($orderableTestArray, 'previous_positive_required', 'no');?>>No</label>

								</div>
							</div>
						</div>
						<div class="form-group row">
							<div class="col-xs-12 col-sm-4 col-md-4">
								<label for="hotspot_variants" class="form-control-label">Does this test have hotspot variants?: <span class="required-field">*</span></label>
							</div>
							<div class="col-xs-12 col-sm-3 col-md-3 col-lg-2">
								<div class="radio">
									<label class="radio-inline"><input type="radio" name="hotspot_variants" value="yes" required="required" <?= $utils->GetValueForUpdateRadioCBX($orderableTestArray, 'hotspot_variants', 'yes');?>>Yes</label>
								
									<label class="radio-inline"><input type="radio" name="hotspot_variants" value="no"  <?= $utils->DefaultRadioBtn($orderableTestArray, 'hotspot_variants', 'no');?>>No</label>

								</div>
							</div>
							<div class="col-xs-12 col-sm-5 col-md-5 col-lg-6 alert alert-info">
								If this test has a list of possible variants choose yes.  A list of variants will be provided in the work sheet, report, or sign out sheet.
							</div>
						</div>
						<div class="form-group row d-none" id="hot-spots-section">
							<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
								<hr class="plain-section-mark">
								<div class="raised-card-subheader">
									Add Hotspots
								</div>
								<hr class="plain-section-mark">
							</div>

							<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
								<div class="row alert alert-info">
									<h1>Currently this version of Infotrack does not allow you to add hotspots.  Provide an admin (<?= $all_admins_str;?>) with all the info to add the hotspots including:  </h1><br>
									<ul>
										<li>Gene</li>
										<li>Coding</li>
										<li>Amino Acid Change</li>
										<li>Exon</li>
										<li>Codon</li>
										<li>Test Genetic Call (required)</li>
										<li>Mutation Abbreviation (required)</li>
									</ul>

								</div>
							</div>
							<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
								<div class="form-group row">
									<div class="col-xs-12 col-sm-4 col-md-4">
										<label for="hotspot_type" class="form-control-label">Are the hotspots called mutants or markers?: <span class="required-field">*</span></label>
									</div>
									<div class="col-xs-12 col-sm-3 col-md-3 col-lg-2">
										<div class="radio">
											<label class="radio-inline"><input type="radio" id="hotspot_type" name="hotspot_type" value="Mutants" <?= $utils->DefaultRadioBtn($orderableTestArray, 'hotspot_type', 'Mutants');?>>Mutants</label>
										
											<label class="radio-inline"><input type="radio" name="hotspot_type" value="Markers"  <?= $utils->GetValueForUpdateRadioCBX($orderableTestArray, 'hotspot_type', 'Markers');?>>Markers</label>

										</div>
									</div>
									<div class="col-xs-12 col-sm-5 col-md-5 col-lg-6 alert alert-info">
										This will change how a hotspots are called in worksheets and signout.  For instance Microsatellite Instability markers should be used but Idylla oncology tests are looking for mutants.
									</div>
								</div>
							</div>						
							<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
								<div class="form-group row">
									<div class="col-xs-12 col-sm-4 col-md-4">
										<label for="hotspot_val_type" class="form-control-label">Will a value be associated with the hotspot?  If so what type of value will be associated?: <span class="required-field">*</span></label>
									</div>
									<div class="col-xs-12 col-sm-3 col-md-3 col-lg-2">
										<div class="radio">
											<label class="radio-inline"><input type="radio" id="hotspot_val_type" name="hotspot_val_type" value="none" <?= $utils->DefaultRadioBtn($orderableTestArray, 'hotspot_val_type', 'none');?>>None</label>
										
											<label class="radio-inline"><input type="radio" name="hotspot_val_type" value="observed & normal size"  <?= $utils->GetValueForUpdateRadioCBX($orderableTestArray, 'hotspot_val_type', 'observed & normal size');?>>observed & normal size</label>

											

										</div>
									</div>
									<div class="col-xs-12 col-sm-5 col-md-5 col-lg-6 alert alert-info">
										If this value is not equal to none then inputs will be provided for hotspots values. For instance Microsatellite Instability the bp size of each positive marker and the size of the normal tissue will have space to provide these values if observed & normal size is selected.
									</div>
								</div>
							</div>
						</div>	
							<!-- <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
								<div class="alert alert-info">
									<u>Purpose</u>:   
										
								</div>
							</div>
							
							<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 mb-2 mt-0">
								<button type="button" class="btn btn-success btn-success-hover append-sample-input" data-clone_template="div-to-clone-linked-tests-field" data-rm_clone_template_btn="clone-linked-tests-remove-btn" data-append_to="div-to-append-linked-tests" style="float: right;">
					        		<span class="fas fa-plus"></span> Insert another hotspot
					            </button>
							</div>
							<?php $hotspot_key = 0;?>
							<div id="div-to-append-linked-tests" class="col-xs-12 col-sm-12 col-md-12 col-lg-12">														
								<div id="div-to-clone-linked-tests-field_<?= $hotspot_key;?>" class="form-group row">
									<div class="col-xs-12 col-sm-3 col-md-1 col-lg-1">
										<label for="genes" class="form-control-label" title="Add the gene if known of the mutant or marker">
											Gene
										</label>
									</div>
									<div class="col-xs-11 col-sm-8 col-md-2 col-lg-2">

										<select class="editable-select div-to-append-linked-tests-required-field" name="genes[]" style="width:100%;">
											<option value="" selected="selected"></option>
											<?php

											//////////////////////////////////////
											// Add all possible genes
											//////////////////////////////////////
											if (isset($all_genes) && !empty($all_genes))
											{
												
												foreach ($all_genes as $gene_key => $gene)
												{
											?>
											<option value="<?= $gene['genes'];?>">
												<?= $gene['genes']; ?> 
											</option>
											<?php
											}
											?>																
										</select>
									
									</div>
									<div class="col-xs-12 col-sm-3 col-md-1 col-lg-1">
										<label for="coding" class="form-control-label">
											Coding: 
										</label>
									</div>	
									<div class="col-xs-12 col-sm-4 col-md-3">
										<input type="text" class="form-control div-to-append-linked-tests-added-clone" name="coding[]" maxlength="255" />
									</div>
									<div class="col-xs-12 col-sm-3 col-md-1 col-lg-1">
										<label for="amino_acid_change" class="form-control-label">
											Amino Acid Change: 
										</label>
									</div>	
									<div class="col-xs-12 col-sm-4 col-md-3">
										<input type="text" class="form-control div-to-append-linked-tests-added-clone" name="amino_acid_change[]" maxlength="255" />
									</div>
									<div class="col-xs-1 col-sm-1 col-md-1 col-lg-1">
										<button id="clone-linked-tests-remove-btn_<?= $hotspot_key;?>" type="button" class="btn btn-danger btn-danger-hover remove-cloned-input div-to-append-linked-tests-added-clone" title="delete this linked test." data-rm_temp_id="div-to-clone-linked-tests-field_<?= $hotspot_key;?>">
					        				<span class="fas fa-minus-circle"></span>
					            		</button>
					            	</div>
							
									<div class="col-xs-12 col-sm-3 col-md-1 col-lg-1">
										<label for="exon" class="form-control-label">
											Exon: 
										</label>
									</div>	
									<div class="col-xs-12 col-sm-1 col-md-1 col-lg-1">
										<input type="text" class="form-control div-to-append-linked-tests-added-clone" name="exon[]" maxlength="30" />
									</div>

									<div class="col-xs-12 col-sm-3 col-md-1 col-lg-1">
										<label for="codon" class="form-control-label">
											Codon: 
										</label>
									</div>	
									<div class="col-xs-12 col-sm-4 col-md-1 col-lg-1">
										<input type="text" class="form-control div-to-append-linked-tests-added-clone" name="codon[]" maxlength="30" />
									</div>
									<div class="col-xs-12 col-sm-3 col-md-2 col-lg-2">
										<label for="ordered_test_genetic_call" class="form-control-label">
											Test Genetic Call: <span class="required-field">*</span>
										</label>
									</div>	
									<div class="col-xs-12 col-sm-4 col-md-1 col-lg-1">
										<input type="text" class="form-control div-to-append-linked-tests-added-clone" name="ordered_test_genetic_call[]" maxlength="20" />
									</div>
									<div class="col-xs-12 col-sm-3 col-md-1 col-lg-1">
										<label for="mutation_abbr" class="form-control-label">
											mutation_abbr: <span class="required-field">*</span>
										</label>
									</div>	
									<div class="col-xs-12 col-sm-4 col-md-1 col-lg-1">
										<input type="text" class="form-control div-to-append-linked-tests-added-clone" name="mutation_abbr[]" maxlength="20" />
									</div>	
								
							</div>
							
			
							<div class="form-group row d-none">
								<div class="col-xs-12 col-sm-4 col-md-4">
									<label for="clone-number" class="form-control-label">clone-number <span class="required-field">*</span></label>
								</div>

								<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
														
									<input type="number" class="form-control" id="clone-number" name="clone-number" value="<?= $hotspot_key;?>" />
								</div>
							</div>
		<?php

		}

		/////////////////////////////////////
		// Add a default empty if previously values not already present.
		/////////////////////////////////////
		else
		{
		?>
					
						
							<div class="form-group row">
								<div class="col-xs-12 col-sm-3 col-md-2 col-lg-2">
									<label for="orderable_tests_id" class="form-control-label" title="The only tests that appear here are tests that have form control test result equal to yes or send out tests equal to yes.">
										Linked Test
										<span class="required-field div-to-append-linked-tests-required-span">*</span>
									</label>
								</div>
								<div class="col-xs-12 col-sm-9 col-md-10 col-lg-10">

									<select class="editable-select div-to-append-linked-tests-required-field" name="test_name[]" style="width:100%;">
										<option value="" selected="selected"></option>

				<?php
				
					if (isset($allTestsAvailable) && !empty($allTestsAvailable))
					{
						foreach ($allTestsAvailable as $key => $test)
						{
				?>
							<option value="<?= $test['test_name'];?>" ><?= $test['test_name']; ?> 
													(<?= $test['full_test_name']; ?>, 
													<?= $test['test_type']; ?>, 
													<?= $test['assay_name']; ?>)</option>
				<?php			
						}
													
					}
				?>														
									</select>
									
								</div>
							</div>
							<div class="form-group row">
								<div class="col-xs-12 col-sm-3 col-md-2 col-lg-2">
									<label for="data_type" class="form-control-label">
										Reflex Trigger Result <span class="required-field div-to-append-linked-tests-required-span">*</span>
									</label>
								</div>	
								<div class="col-xs-12 col-sm-8 col-md-10 col-lg-10 ">
									<label class="radio-inline">
										<input type="radio" class="added-radio-update-name" name="test_result_0" value="Positive" required>
										Positive
									</label>
									
									<label class="radio-inline">
										<input type="radio" class="added-radio-update-name" name="test_result_0" value="Negative" required>
										Negative
									</label>
									
								</div>
							</div>
					

		<?php
		}
		?>		
					</div> -->
				



											
						<hr class="plain-section-mark">
						<div class="raised-card-subheader">
							Turnaround Times
						</div>
						<hr class="plain-section-mark">
						<div class="form-group row">
							
							<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
								<label for="include_in_turn_around_calculation" class="form-control-label">Include in turn around calculation: <span class="required-field">*</span></label>
							</div>
							<div class="col-xs-12 col-sm-3 col-md-3 col-lg-2">
								<div class="radio">
									<label class="radio-inline"><input type="radio" name="include_in_turn_around_calculation" value="yes" required="required" <?= $utils->DefaultRadioBtn($orderableTestArray, 'include_in_turn_around_calculation', 'yes');?>>Yes</label>
								
									<label class="radio-inline"><input type="radio" name="include_in_turn_around_calculation" value="no"  <?= $utils->GetValueForUpdateRadioCBX($orderableTestArray, 'include_in_turn_around_calculation', 'no');?>>No</label>

								</div>
							</div>
							<div class="col-xs-12 col-sm-5 col-md-5 col-lg-6 alert alert-info">
								If include in turn around is not set to yes all samples run for this test will not appear in the monthly audit at all.  This is not for the tracked subset of tests.
							</div>
						</div>

						<!-- 

						turnaround_time_tracked hidden and set to no unless yes is selected for include_in_turn_around_calculation 	controlled in JS		

						-->					
						<div id="turn-around-calculation-fields">
							<div class="form-group row">
								<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
									<label for="turnaround_time_tracked" class="form-control-label">Turnaround Time is Automatically Tracked in Audit: <span class="required-field">*</span></label>
								</div>
								<div class="col-xs-12 col-sm-3 col-md-3 col-lg-2">
									<div class="radio">
										<label class="radio-inline"><input type="radio" name="turnaround_time_tracked" value="yes" required="required" <?= $utils->GetValueForUpdateRadioCBX($orderableTestArray, 'turnaround_time_tracked', 'yes');?>>Yes</label>
									
										<label class="radio-inline"><input type="radio" name="turnaround_time_tracked" value="no"  <?= $utils->DefaultRadioBtn($orderableTestArray, 'turnaround_time_tracked', 'no');?>>No</label>
									</div>
								</div>
								<div class="col-xs-12 col-sm-5 col-md-5 col-lg-6 alert alert-info">
									In the monthly audit a subset of tests can be set to tracked.  If you would like to add this test to be tracked press yes below.
								</div>
							</div>
							<div class="form-group row">

								<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
									<label for="time_depends_on_result" class="form-control-label">Time Depends on Result: <span class="required-field">*</span></label>
								</div>
								<div class="col-xs-12 col-sm-3 col-md-3 col-lg-2">
									<div class="radio">
										<label class="radio-inline"><input type="radio" name="time_depends_on_result" value="yes" required="required" <?= $utils->GetValueForUpdateRadioCBX($orderableTestArray, 'time_depends_on_result', 'yes');?>>Yes</label>
									
										<label class="radio-inline"><input type="radio" name="time_depends_on_result" value="no"  <?= $utils->DefaultRadioBtn($orderableTestArray, 'time_depends_on_result', 'no');?>>No</label>
									</div>
								</div>
								<div class="col-xs-12 col-sm-5 col-md-5 col-lg-6 alert alert-info">
									Sometimes turn around time will depend on if the test is positive or negative.  If this is the case for this test press yes.
								</div>
							</div>
						<?php
							//////////////////////////////////////////////
							// Get the turnaround times 
							//////////////////////////////////////////////
							$turnaround_times = array(
									'any' => '', 
									'Positive' => '', 
									'Negative' => ''
							);

							if (isset($current_TOTs))
							{
								foreach ($current_TOTs as $key => $tot)
								{
									$turnaround_times[$tot['dependent_result']] = $tot['turnaround_time'];
								}
							}

							//////////////////////////////////
							// set up which expected_turnaround_time fields are automatically shown
							// depending on if  time_depends_on_result is equal to yes or no
							/////////////////////////////////
							$tot_show_class_any = '';
							$tot_show_class_result = 'class="d-none"';
							if (isset($orderableTestArray[0]['time_depends_on_result']) && $orderableTestArray[0]['time_depends_on_result'] === 'yes')
							{
								$tot_show_class_any = 'class="d-none"';
								$tot_show_class_result = '';
							}
						?>
							<div id="time-depends-on-result-no" <?= $tot_show_class_any;?>>
								<div class="form-group row">
									<div class="col-xs-12 col-sm-4 col-md-4">
										<label for="expected_turnaround_time" class="form-control-label" >Expected Turnaround Time: <span class="required-field time-depends-on-result-no-required-span">*</span></label>
									</div>
									
									<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
										
										<input id="any-turnaround_time" type="number" class="form-control time-depends-on-result-no-required-input" min="1" step="1" name="expected_turnaround_time[any][turnaround_time]" required="required" value="<?= $turnaround_times['any'];?>"/>
									</div>
								</div>
							</div>
							<div id="time-depends-on-result-yes" <?= $tot_show_class_result;?>>
								<div class="form-group row">
									<div class="col-xs-12 col-sm-4 col-md-4">
										<label for="expected_turnaround_time" class="form-control-label" >Expected Turnaround Time for Positive Result: <span class="required-field time-depends-on-result-yes-required-span">*</span></label>
									</div>
									
									<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
										
										<input id="Positive-turnaround_time" type="number" class="form-control time-depends-on-result-yes-required-input" min="1" step="1" name="expected_turnaround_time[Positive][turnaround_time]" value="<?= $turnaround_times['Positive'];?>" />
									</div>
								</div>

								<div class="form-group row">
									<div class="col-xs-12 col-sm-4 col-md-4">
										<label for="expected_turnaround_time" class="form-control-label" >Expected Turnaround Time for Negative Result: <span class="required-field time-depends-on-result-yes-required-span">*</span></label>
									</div>
									
									<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
										
										<input id="Negative-turnaround_time" type="number" min="1" step="1" class="form-control time-depends-on-result-yes-required-input" name="expected_turnaround_time[Negative][turnaround_time]" value="<?= $turnaround_times['Negative'];?>"/>
									</div>
								</div>
							</div>

							<?php
								////////////////////////////////
								// Set up visibility of dependent test input
								////////////////////////////////
								$visibility_dependent_test = 'class="form-group row d-none"';
								if (isset($orderableTestArray[0]['turnaround_time_depends_on_ordered_test']) && $orderableTestArray[0]['turnaround_time_depends_on_ordered_test'] === 'yes')
								{
									$visibility_dependent_test = 'class="form-group row"';
								}

							?>

							<div class="form-group row">
								<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
									<label for="turnaround_time_depends_on_ordered_test" class="form-control-label">Turn around time depends on an ordered test: <span class="required-field">*</span></label>
								</div>
								<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
									<div class="radio">
										<label class="radio-inline"><input type="radio" name="turnaround_time_depends_on_ordered_test" value="yes" required="required" <?= $utils->GetValueForUpdateRadioCBX($orderableTestArray, 'turnaround_time_depends_on_ordered_test', 'yes');?>>Yes</label>
									
										<label class="radio-inline"><input type="radio" name="turnaround_time_depends_on_ordered_test" value="no"  <?= $utils->DefaultRadioBtn($orderableTestArray, 'turnaround_time_depends_on_ordered_test', 'no');?>>No</label>
									</div>
								</div>
							</div>

							<!-- dependency_orderable_tests_id will unhide if yes is selected for turnaround_time_depends_on_ordered_test under javascript control -->
							<div id="depend-test-div" <?= $visibility_dependent_test;?>>
								<div class="col-xs-12 col-sm-4 col-md-4">
									<label for="dependency_orderable_tests_id" class="form-control-label">Dependent Test: <span class="required-field">*</span></label>
								</div>
								<div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">
									
									<select class="selectpicker" data-live-search="true" data-width="100%" name="dependency_orderable_tests_id" id="dependency_orderable_tests_id" >

										<option value="" selected="selected"></option>
									<?php

										for($i=0;$i<sizeof($allTestsAvailable);$i++)
				               			{	
				              	             
						      				if 	(
						      						isset($allTestsAvailable[$i]['test_status']) && 
						      						$allTestsAvailable[$i]['test_status'] === 'Active'
						      					)
						      				{
									?>
												<option <?= $allTestsAvailable[$i]['test_name'] == 'DNA Extraction'? 'data-icon="fas fa-dna"':'';?>
													value="<?= $allTestsAvailable[$i]['orderable_tests_id']; ?>" <?= $utils->GetValueForUpdateSelect($orderableTestArray, 'dependency_orderable_tests_id', $allTestsAvailable[$i]['orderable_tests_id']); ?>>
														
														<?= $allTestsAvailable[$i]['test_name']; ?> 
														(<?= $allTestsAvailable[$i]['full_test_name']; ?>, 
														<?= $allTestsAvailable[$i]['test_type']; ?>, 
														<?= $allTestsAvailable[$i]['assay_name']; ?>)
												</option>
									<?php
											}
										}
									?>
									</select>
									
								</div>
							</div>
						</div>
						<hr class="plain-section-mark">
						<div class="raised-card-subheader">
							Pool
						</div>
						<hr class="plain-section-mark">

						
						<div class="form-group row">
							<div class="col-xs-12 col-sm-4 col-md-4">
								<label for="max_num_samples_per_run" class="form-control-label" >Max Number of Samples per run not including any controls: <span class="required-field">*</span></label>
							</div>
							
							<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
								
								<input id="any-max_num_samples_per_run" type="number" class="form-control" min="1" step="1" name="max_num_samples_per_run" required="required" value="<?= $utils->GetValueForUpdateInput($orderableTestArray, 'max_num_samples_per_run');?>"/>
							</div>
						</div>
					

						<hr class="plain-section-mark">
						<div class="raised-card-subheader">
							Set Up Fields Present on Test Sign Out Page
						</div>
						<hr class="plain-section-mark">
						<div class="form-group row">
							
							<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
								<label for="mol_num_required" class="form-control-label">Will Infotrack be making the report for this test? <span class="required-field">*</span></label>
							</div>
							<div class="col-xs-12 col-sm-3 col-md-3 col-lg-2">
								<div class="radio">
									<label class="radio-inline"><input type="radio" name="mol_num_required" value="yes" required="required" <?= $utils->GetValueForUpdateRadioCBX($orderableTestArray, 'mol_num_required', 'yes');?>>Yes</label>
								
									<label class="radio-inline"><input type="radio" class="reportable-tests" name="mol_num_required" value="no"  <?= $utils->DefaultRadioBtn($orderableTestArray, 'mol_num_required', 'no');?>>No</label>
								</div>
							</div>
							<div class="col-xs-12 col-sm-5 col-md-5 col-lg-6 alert alert-info">
								To select this selection you will have to notify an admin (<?= $all_admins_str;?>) to develop the reporting system for this test. 
							</div>
						</div>
						<div class="all-fields-not-necessary-for-reportable-tests">
							<div class="form-group row">
								
								<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
									<label for="extraction_info" class="form-control-label">Would you like to add extraction info to the test sign out page? <span class="required-field">*</span></label>
								</div>
								<div class="col-xs-12 col-sm-3 col-md-3 col-lg-2">
									<div class="radio">
										<label class="radio-inline"><input type="radio" class="non-reportable-tests" name="extraction_info" value="yes" required="required" <?= $utils->GetValueForUpdateRadioCBX($orderableTestArray, 'extraction_info', 'yes');?>>Yes</label>
									
										<label class="radio-inline"><input type="radio" class="reportable-tests" name="extraction_info" value="no"  <?= $utils->DefaultRadioBtn($orderableTestArray, 'extraction_info', 'no');?>>No</label>

									</div>
								</div>
								<div class="col-xs-12 col-sm-5 col-md-5 col-lg-6 alert alert-info">
									Only include extraction info if this test is an extraction test.  This should be rarely used.  This option will provide fields like Extraction Method, Extraction date, aliquoted by, etc. 
								</div>
							</div>

							<div class="form-group row">
								
								<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
									<label for="quantification_info" class="form-control-label">Would you like to add quantification info to the test sign out page? <span class="required-field">*</span></label>
								</div>
								<div class="col-xs-12 col-sm-3 col-md-3 col-lg-2">
									<div class="radio">
										<label class="radio-inline"><input type="radio" class="non-reportable-tests" name="quantification_info" value="yes" required="required" <?= $utils->GetValueForUpdateRadioCBX($orderableTestArray, 'quantification_info', 'yes');?>>Yes</label>
									
										<label class="radio-inline"><input type="radio" name="quantification_info" class="reportable-tests" value="no"  <?= $utils->DefaultRadioBtn($orderableTestArray, 'quantification_info', 'no');?>>No</label>

									</div>
								</div>
								<div class="col-xs-12 col-sm-5 col-md-5 col-lg-6 alert alert-info">
									Only include quantification info if this test is an extraction test or a quantification test like nanodrop.  This should be rarely used.  This option will add the Dilution calculator and other fields related to quantification.  
								</div>
							</div>

							<div class="form-group row">
								
								<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
									<label for="thermal_cyclers_instruments" class="form-control-label">Would you like to add a check list of thermal cyclers instruments to the test sign out page? <span class="required-field">*</span></label>
								</div>
								<div class="col-xs-12 col-sm-3 col-md-3 col-lg-2">
									<div class="radio">
										<label class="radio-inline"><input type="radio" class="non-reportable-tests" name="thermal_cyclers_instruments" value="yes" required="required" <?= $utils->DefaultRadioBtn($orderableTestArray, 'thermal_cyclers_instruments', 'yes');?>>Yes</label>
									
										<label class="radio-inline"><input type="radio" name="thermal_cyclers_instruments" class="reportable-tests" value="no"  <?= $utils->GetValueForUpdateRadioCBX($orderableTestArray, 'thermal_cyclers_instruments', 'no');?>>No</label>

									</div>
								</div>
								<div class="col-xs-12 col-sm-5 col-md-5 col-lg-6 alert alert-info">
									Most tests do use a thermal cyclers to complete the test.  If this test does select yes.  This will allow for sample tracing of thermal cycler instrument used.
								</div>
							</div>

							<div class="form-group row">
								
								<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
									<label for="sanger_sequencing_instruments" class="form-control-label">Would you like to add a check list of sanger sequencing instruments  to the test sign out page? <span class="required-field">*</span></label>
								</div>
								<div class="col-xs-12 col-sm-3 col-md-3 col-lg-2">
									<div class="radio">
										<label class="radio-inline"><input type="radio" class="non-reportable-tests" name="sanger_sequencing_instruments" value="yes" required="required" <?= $utils->DefaultRadioBtn($orderableTestArray, 'sanger_sequencing_instruments', 'yes');?>>Yes</label>
									
										<label class="radio-inline"><input type="radio" name="sanger_sequencing_instruments" class="reportable-tests" value="no"  <?= $utils->GetValueForUpdateRadioCBX($orderableTestArray, 'sanger_sequencing_instruments', 'no');?>>No</label>

									</div>
								</div>
								<div class="col-xs-12 col-sm-5 col-md-5 col-lg-6 alert alert-info">
									Some tests use a sanger sequencing instrument.  If this test requires a sanger sequencing instrument to complete it select yes.  This will allow for sample tracing of sanger sequencing instrument used.
								</div>
							</div>
							<div class="form-group row">
								
								<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
									<label for="sanger_sequencing_instruments" class="form-control-label">Would you like to add a check list of idylla instruments  to the test sign out page? <span class="required-field">*</span></label>
								</div>
								<div class="col-xs-12 col-sm-3 col-md-3 col-lg-2">
									<div class="radio">
										<label class="radio-inline"><input type="radio" class="non-reportable-tests" name="idylla_instruments" value="yes" required="required" <?= $utils->GetValueForUpdateRadioCBX($orderableTestArray, 'idylla_instruments', 'yes');?>>Yes</label>
									
										<label class="radio-inline"><input type="radio" name="idylla_instruments" class="reportable-tests" value="no"  <?= $utils->DefaultRadioBtn($orderableTestArray, 'idylla_instruments', 'no');?>>No</label>

									</div>
								</div>
								<div class="col-xs-12 col-sm-5 col-md-5 col-lg-6 alert alert-info">
									Use this option to keep track of idylla instruments used if this test is performed on idylla.
								</div>
							</div>
							<div class="form-group row">
								
								<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
									<label for="wet_bench_techs" class="form-control-label">Would you like to add a check list of wet bench techs to the test sign out page? <span class="required-field">*</span></label>
								</div>
								<div class="col-xs-12 col-sm-3 col-md-3 col-lg-2">
									<div class="radio">
										<label class="radio-inline"><input type="radio" class="non-reportable-tests" name="wet_bench_techs" value="yes" required="required" <?= $utils->DefaultRadioBtn($orderableTestArray, 'wet_bench_techs', 'yes');?>>Yes</label>
									
										<label class="radio-inline"><input type="radio" name="wet_bench_techs" class="reportable-tests" value="no"  <?= $utils->GetValueForUpdateRadioCBX($orderableTestArray, 'wet_bench_techs', 'no');?>>No</label>

									</div>
								</div>
								<div class="col-xs-12 col-sm-5 col-md-5 col-lg-6 alert alert-info">
									To trace the techs that performed the wet bench part of the assay select yes above.  The check list will be comprised of any user with non_ngs_wet_ben permissions.  <strong>DO NOT select yes if this a DNA extraction or Quantification Test.  Select yes for quantification info and/or extraction info.</strong>
								</div>
							</div>
							 
							<div class="form-group row">
								
								<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
									<label for="analysis_techs" class="form-control-label">Would you like to add a check list of analysis techs to the test sign out page? <span class="required-field">*</span></label>
								</div>
								<div class="col-xs-12 col-sm-3 col-md-3 col-lg-2">
									<div class="radio">
										<label class="radio-inline"><input type="radio" class="non-reportable-tests" name="analysis_techs" value="yes" required="required" <?= $utils->DefaultRadioBtn($orderableTestArray, 'analysis_techs', 'yes');?>>Yes</label>
									
										<label class="radio-inline"><input type="radio" name="analysis_techs" class="reportable-tests" value="no"  <?= $utils->GetValueForUpdateRadioCBX($orderableTestArray, 'analysis_techs', 'no');?>>No</label>

									</div>
								</div>
								<div class="col-xs-12 col-sm-5 col-md-5 col-lg-6 alert alert-info">
									To trace the techs that performed the analysis part of the assay select yes above.  The check list will be comprised of any user with non_ngs_wet_ben permissions.  <strong>DO NOT select yes if this a DNA extraction or Quantification Test.  Select yes for quantification info and/or extraction info.</strong>
								</div>
							</div>

							<div class="form-group row">
								
								<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
									<label for="assign_directors" class="form-control-label">Would you like the tech to be able to assign a director to the report? <span class="required-field">*</span></label>
								</div>
								<div class="col-xs-12 col-sm-3 col-md-3 col-lg-2">
									<div class="radio">
										<label class="radio-inline"><input type="radio" class="non-reportable-tests" name="assign_directors" value="yes" required="required" <?= $utils->GetValueForUpdateRadioCBX($orderableTestArray, 'assign_directors', 'yes');?>>Yes</label>
									
										<label class="radio-inline"><input type="radio" name="assign_directors" value="no" class="reportable-tests" <?= $utils->DefaultRadioBtn($orderableTestArray, 'assign_directors', 'no');?>>No</label>

									</div>
								</div>
								<div class="col-xs-12 col-sm-5 col-md-5 col-lg-6 alert alert-info">
									If yes is selected a list of directors will be provided and one can be selected.  The selected director will be emailed once data is added by the tech.  This only activates if this is an infotrack reportable test.
								</div>
							</div>


							<div class="form-group row">
								
								<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
									<label for="test_result" class="form-control-label">Would you like to a section to report the test results? <span class="required-field">*</span></label>
								</div>
								<div class="col-xs-12 col-sm-3 col-md-3 col-lg-2">
									<div class="radio">
										<label class="radio-inline"><input type="radio" class="non-reportable-tests" name="test_result" value="yes" required="required" <?= $utils->DefaultRadioBtn($orderableTestArray, 'test_result', 'yes');?>>Yes</label>
									
										<label class="radio-inline"><input type="radio" name="test_result" value="no" class="reportable-tests" <?= $utils->GetValueForUpdateRadioCBX($orderableTestArray, 'test_result', 'no');?>>No</label>

									</div>
								</div>
								<div class="col-xs-12 col-sm-5 col-md-5 col-lg-6 alert alert-info">
									Selecting yes for this option will add provide the option to track test results.  Radio buttons will appear with test results.  If this section is added the test will appear in the reflex tracker creator.
								</div>
							</div>
						</div>

						<div class="form-group row">
								
								<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
									<label for="result_type" class="form-control-label">Which type of result would you like available?  <span class="required-field">*</span></label>
								</div>
								<div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">
									<div class="radio">
										<label class="radio-inline"><input type="radio" class="non-reportable-tests" name="result_type" value="pos neg" required="required" <?= $utils->GetValueForUpdateRadioCBX($orderableTestArray, 'result_type', 'pos neg');?>>Positive  Weak Positive  Negative  Weak Negative  No Signal  See Interpt. </label>
									
										<label class="radio-inline"><input type="radio" name="result_type" value="normal,low,high,QC" class="reportable-tests" <?= $utils->GetValueForUpdateRadioCBX($orderableTestArray, 'result_type', 'normal,low,high,QC');?>>Normal, MSS/MSI-L report is 0/5 or 1/5 markers shifted, MSI-H report is 2/5 - 5/5 markers shifted, Quantity/Quality not sufficient for a result</label>

										<label class="radio-inline"><input type="radio" class="non-reportable-tests" name="result_type" value="na" required="required" <?= $utils->DefaultRadioBtn($orderableTestArray, 'result_type', 'na');?>>NA</label>
									</div>
								</div>
								
							</div>
						</div>
						

						<div class="form-group row">
							<div class="col-xs-12 col-sm-4 col-md-4">
								<label for="result_note" class="form-control-label">Add any notes here to the sign out tech with guidance about sign out.</label>
							</div>

							<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
								<textarea class="form-control" name="result_note" rows="4" maxlength="65535" style="margin-bottom: 10px;"><?= $utils->GetValueForUpdateInput($orderableTestArray, 'result_note');?></textarea>
							</div>
						</div>
					</div>


					


					<div class="form-group row">
						<div class="offset-xs-4 offset-sm-4 offset-md-4 offset-lg-4 col-xs-2 col-sm-2 col-md-2 col-lg-2">
							<button type="submit" id="<?= $page;?>_submit" name="<?= $page;?>_submit" value="Submit"  class="btn btn-primary btn-primary-hover">
								Submit
							</button>
						</div>
					</div>

				</fieldset>
			</form>
		</div>
	</div>
</div>

			