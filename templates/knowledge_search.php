<div class="container">
	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			
			<form name='create_freezer' id="create_freezer" class='form-horizontal' method='post'>
				<fieldset id="search">
					<legend>Search</legend>
					<div class="row">
						<div class="col-xs-12 col-sm-12 col-md-4 col-lg-6">
							<label for="genes" class="form-control-label">
								Gene:
							</label>
							<select class="form-control" name="genes" id="genes">
								<option disabled selected value> -- select an option -- </option>
	<?php
								
								if (isset($gene_list))
								{
									foreach($gene_list as $key => $gene)
									{
	?>
										<option value="<?= $gene['genes'];?>"><?= $gene['genes'];?></option>
	<?php										
									}
								}

	?>								

							</select>
						</div>
						<div class="col-xs-12 col-sm-12 col-md-4 col-lg-6">
							<label for="coding" class="form-control-label">
								Coding:
							</label>
							<input type="text" class="form-control" id="coding" name="coding" maxlength="255">
						</div>
					</div>
					<div class="row">	
						<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
							<label for="amino_acid_change" class="form-control-label">
								Amino Acid Change: (example: p.Val600Glu)
							</label>
							<input type="text" class="form-control" id="amino_acid_change" name="amino_acid_change" maxlength="255">
						</div>	
						<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
							<label for="amino_acid_change_abbrev" class="form-control-label">
								Amino Acid Change Abbrev: (example: p.v600e)
							</label>
							<input type="text" class="form-control" id="amino_acid_change_abbrev" name="amino_acid_change_abbrev" maxlength="255">
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12 col-sm-1 col-md-1 col-lg-1">
							<button type="submit" value="Submit" name="submit" class="btn btn-primary btn-primary-hover">Search
							</button>
						</div>	
					</div>

				</fieldset>
			</form>
			
		</div>
	</div>
</div>
	