<div class="container">
	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			
			<form name='create_freezer' id="create_freezer" class='form-horizontal' method='post'>
				<fieldset id="search">
					<legend><?= $page_title;?></legend>
					<hr class="section-mark">
					<div class="row">
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
							<h4><u>Search Patient Information</u></h4>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
							<label for="first_name" class="form-control-label">
								First Name: 
							</label>
							<input type="text" class="form-control" id="first_name" name="first_name" maxlength="50">
						</div>
						<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
							<label for="middle_name" class="form-control-label">
								Middle Name: 
							</label>
							<input type="text" class="form-control" id="middle_name" name="middle_name" maxlength="50">
						</div>
						<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
							<label for="last_name" class="form-control-label">
								Last Name: 
							</label>
							<input type="text" class="form-control" id="last_name" name="last_name" maxlength="50">
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
							<label for="medical_record_num" class="form-control-label">
								Medical Record #: 
							</label>
							<input type="text" class="form-control" id="medical_record_num" name="medical_record_num" maxlength="50">
						</div>
						<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
							<label for="dob" class="form-control-label">
								Date of Birth:
							</label>
							<input type="text" class="form-control date-picker" id="dob" name="dob">
						</div>
					</div>
					<hr class="section-mark">
					<div class="row">
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
							<h4><u>Search Visit Information</u></h4>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
							<label for="soft_path_num" class="form-control-label">
								Soft Path #:
							</label>
							<input type="text" class="form-control" id="soft_path_num" name="soft_path_num" maxlength="50">
						</div>						
						<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
							<label for="soft_lab_num" class="form-control-label">
								Soft Lab #:
							</label>
							<input type="text" class="form-control" id="soft_lab_num" name="soft_lab_num" maxlength="50">
						</div>
						<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
							<label for="mol_num" class="form-control-label">
								Molecular #:
							</label>
							<input type="text" class="form-control" id="mol_num" name="mol_num" maxlength="50">
						</div>						
					</div>
					<hr class="section-mark">
					<div class="row">
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
							<h4><u>Search a range of run dates</u></h4>
						</div>
					</div>

					<div class="row">
						<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
							<label for="start_date">Start Date: (MM/DD/YYYY)</label>
							<input type="text" class="form-control input-sm searchPressEnter date-picker" name="start_date">
						</div>
						<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
							<label for="end_date">End Date: (MM/DD/YYYY)</label>
							<input type="text" class="form-control input-sm searchPressEnter date-picker" name="end_date">
						</div>
					</div>
					<hr class="section-mark">
					<div class="row">
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
							<h4><u>Search Reported Variants</u></h4>
							<h5></h5>
							<div class="row">
								<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
									<ul style="list-style-type: none;">
										<li>If searching multiple Genes, Codings, or amino acid changes seperate them with a comma and a space
											<ul style="list-style-type: none;">
												<li>Example: EGFR, IDH1</li>
											</ul>
										</li>
									</ul>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
							<label for="genes" class="form-control-label">
								Check if you would like to search reports where no variants were reported.
							</label>
							<input type="checkbox" class="chkbx" name="no_variants_found">
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
							<label for="genes" class="form-control-label">
								Gene:
							</label>
							<input type="text" class="form-control" id="genes" name="genes" maxlength="30">
						</div>	
						<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
							<label for="codings" class="form-control-label">
								Coding:
							</label>
							<input type="text" class="form-control" id="codings" name="codings" maxlength="255">
						</div>	
						<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
							<label for="amino_acid_changes" class="form-control-label">
								Amino Acid Change:
							</label>
							<input type="text" class="form-control" id="amino_acid_changes" name="amino_acid_changes" maxlength="255">
						</div>	
					</div>

					<hr class="section-mark">
					<div class="row">
						<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
							<label for="primary_tumor_site" class="form-control-label">
								Primary Tumor Site: 
							</label>
							<input type="text" class="form-control" name="primary_tumor_site" list="primary_tumor_site">

							<datalist name="primary_tumor_site" id="primary_tumor_site">
							<?php
								for($i=0;$i<sizeof($all_primary_tumor_site);$i++)
		               			{
							?>
									<option value="<?= $all_primary_tumor_site[$i]['primary_tumor_site']; ?>">
										<?= $all_primary_tumor_site[$i]['primary_tumor_site']; ?>
									</option>
							<?php
								}
							?>
							</datalist>
						</div>
						
						<div class="col-xs-12 col-sm-4 col-md-4">
							<label for="test_tissue" class="form-control-label">Tissue Tested:</label>
						
							<input type="text" class="form-control" name="test_tissue" maxlength="30" list="test_tissue"/>
							<datalist name="test_tissue" id="test_tissue">
							<?php
								for($i=0;$i<sizeof($all_tissues);$i++)
		               			{
							?>
									<option value="<?= $all_tissues[$i]['tissue']; ?>">
										<?= $all_tissues[$i]['tissue']; ?>
									</option>
							<?php
								}
							?>
							</datalist>
						</div>
					</div>
					
					<div class="row">
					
						<div class="col-xs-12 col-sm-1 col-md-1 col-lg-1">
							<button type="submit" value="Submit" name="submit" class="btn btn-primary btn-primary-hover">Search
							</button>
						</div>
					</div>

				</fieldset>
			</form>
		</div>
	</div>
</div>
