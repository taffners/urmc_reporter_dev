<div class="container-fluid">

	<div class="row">
		
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			<form class="form" method="post" class="form-horizontal">
				<fieldset>
				<?php 
					if (isset($_GET['check_list_validation_step_id']))					
					{
				?>
					<legend>
						Update Checklist Item
					</legend>
					
				<?php 
					}
					else
					{
				?>
					<legend>
						Add Checklist Item
					</legend>
				<?php
					}
				?>					
					
					<div class="form-group row">
						<div class="col-xs-12 col-sm-4 col-md-4">
							<label for="step_name" class="form-control-label" >QC Check: <span class="required-field">*</span></label>
						</div>
						<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
							<input type="text" class="form-control" id="step_name" name="step_name" maxlength="255" value="<?= $utils->GetValueForUpdateInput($checkListArray, 'step_name');?>"/>
						</div>
					</div>
					<div class="form-group row">
						<div class="col-xs-12 col-sm-4 col-md-4">
							<label for="field_type" class="form-control-label">Field Type: <span class="required-field">*</span></label>
						</div>
						<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
							<div class="radio">
								<label class="radio-inline"><input type="radio" name="field_type" value="yes_no" required="required" <?= $utils->DefaultRadioBtn($checkListArray, 'yes_no', 'Form');?>>Yes No Radio Buttons</label>
							
								<label class="radio-inline"><input type="radio" name="field_type" value="text"  <?= $utils->GetValueForUpdateRadioCBX($checkListArray, 'field_type', 'text');?>>Text Area</label>

								
							</div>
						</div>
					</div>

					<div class="form-group row">
						<div class="col-xs-12 col-sm-4 col-md-4">
							<label for="description" class="form-control-label">Description:</label>
						</div>

						<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
							<textarea class="form-control" name="description" rows="4" maxlength="65535" style="margin-bottom: 10px;"> <?= $utils->GetValueForUpdateInput($checkListArray, 'description');?></textarea>
						</div>
					</div>
					
					<div class="form-group row">
						<div class="col-xs-12 col-sm-4 col-md-4">
							<label for="category" class="form-control-label">Category: <span class="required-field">*</span></label>
						</div>
						<div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">
							
							<select class="selectpicker" data-live-search="true" data-width="100%" name="category" id="category" required="required">

								<option value="" selected="selected"></option>
								<option value="Form Testing" <?= $utils->GetValueForUpdateSelect($checkListArray, 'category', 'Form Testing');?>>
									Form Testing
								</option>
								<option value="Report Testing" <?= $utils->GetValueForUpdateSelect($checkListArray, 'category', 'Report Testing');?>>
									Report Testing
								</option>
								<option value="View Testing" <?= $utils->GetValueForUpdateSelect($checkListArray, 'category', 'View Testing');?>>
									View Testing
								</option>

								<option value="General Testing" <?= $utils->GetValueForUpdateSelect($checkListArray, 'category', 'General Testing');?>>
									General Testing
								</option>

								<option value="Error Testing" <?= $utils->GetValueForUpdateSelect($checkListArray, 'category', 'Error Testing');?>>
									Error Testing
								</option>

								<option value="Performance Testing" <?= $utils->GetValueForUpdateSelect($checkListArray, 'category', 'Performance Testing');?>>
									Performance Testing
								</option>

								<option value="User Testing" <?= $utils->GetValueForUpdateSelect($checkListArray, 'category', 'User Testing');?>>
									User Testing
								</option>

								<option value="Loading Testing" <?= $utils->GetValueForUpdateSelect($checkListArray, 'category', 'Loading Testing');?>>
									Loading Testing
								</option>
								<option value="Responsive Screen Testing" <?= $utils->GetValueForUpdateSelect($checkListArray, 'category', 'Responsive Screen Testing');?>>
									Responsive Screen Testing
								</option>
							</select>								
						</div>
					</div>

					<div class="form-group row">
						<div class="offset-xs-4 offset-sm-4 offset-md-4 offset-lg-4 col-xs-2 col-sm-2 col-md-2 col-lg-2">
							<button type="submit" id="<?= $page;?>_submit" name="<?= $page;?>_submit" value="Submit"  class="btn btn-primary btn-primary-hover">
								Submit
							</button>
						</div>
					</div>
				</fieldset>
			</form>
		</div>
	</div>
</div>