<div class="container-fluid">

	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			<img src="../images/homer_doh.jpg" alt="homer doh cartoon">
			<br>
			<strong>Since you have entered your login info incorrectly several times your account has been disabled.  Please contact Samantha to re-enable your account.</strong>
			<br><br>
			This application complies with PCI Data Security Standards.   
			<ul>
				<li>8.5.13 (Limit repeated access attempts by locking out after not more than five attempts)</li>
				<li>8.5.14 (Set the lockout duration to thirty minutes or until administrator enables the account).</li>
			</ul>
		</div>
	</div>
</div>