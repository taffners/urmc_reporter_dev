<?php
	require_once('templates/show_edit_report.php');
?>
<div class="container-fluid">
	<div class="row">
		<!-- update area -->
		<div id="toggle-report-progress-bar" class="offset-xs-2 offset-sm-2 offset-md-2 offset-lg-2 col-xs-10 col-sm-10 col-md-10 col-lg-10">			<fieldset>
				<legend>
					Message Board	
				</legend>

<?php
		require_once('templates/shared_layouts/message_board.php')
?>
			</fieldset>
		</div>
	</div>
</div>

