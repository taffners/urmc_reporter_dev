

<div class="container-fluid">
	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			<div class="alert alert-warning" style="font-size:20px;">	
				On 4/29/2019 I Couldn't find HER2 in ComsicMutantExport File
				<br><br>
				On 4/29/2019 I Couldn't find XP01 in ComsicMutantExport File
				
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			<div class="alert alert-info">	
				<p>
					<strong> <a href="https://cancer.sanger.ac.uk/cosmic/download" target="_blank">Download Cosmic TSV</a> (cosmic mutation data) CosmicMutantExport.tsv.gz</strong>
				</p>
				<p>
					email: samantha_taffner@urmc.rochester.edu
				</p>
				<p>
					password: SNV_Cosmic_2018
				</p>
			</div>
		</div>
	</div>
	<div class="row">		
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			<div class="alert alert-info">	
				Place CosmicMutantExport.tsv file in cosmic_tsvs folder and unzip it.
			</div>	
		</div>			
	</div>
	<div class="row">		
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			<form method="post" action="index.php?page=<?= $_GET['page']; ?>">
				<button type="submit" id="split_into_separte_files" name="split_into_separte_files_submit" value="Submit"  class="btn btn-primary btn-primary-hover">
					Split the large CosmicMutantExport file into a file for each gene of interest 
				</button>
			</form>
		</div>			
	</div>
<?php	
if (isset($upload_genes))
{
?>
	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			<table class="formated_table">
				<thead>
					<th colspan="10">Genes Which Will Be Uploaded</th>
				</thead>
				<tbody>
<?php			
			foreach ($upload_genes as $key => $gene)
			{
				// find if the gene $key is divisible by 10 if it is add row
				if ($key%10 == 0 && $key !== 0)
				{
?>
					</tr>

<?php
				}
				if ($key%10 == 0)
				{
?>
					<tr>
<?php
				}
				// find if gene was updated in last 3 months
				$last_updated = $db->listAll('last-update-gene-knowledge-base', $gene);
?>
						<td id="gene_update_cell_<?= $gene;?>" class="hover update-gene-in-knowledge-base" <?php

					// add green color if updated in the last 3 months
					if (isset($last_updated) && !empty($last_updated))
					{
						echo 'style="background:#22c222;"';
					}?> data-gene="<?= $gene;?>"><?= $gene;?></td>
					
<?php
			}
?>
				</tbody>
			</table>

		</div>
	</div>

	
	

<?php
}
else
{
?>
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		<div class="alert alert-info">	
			Something is wrong with the cosmic/genes_in_assays.csv $upload_genes does not exist.
		</div>
	</div>
<?php
}
?>
</div>