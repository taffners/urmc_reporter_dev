<div class="container">
	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			
			<form name='create_freezer' id="create_freezer" class='form-horizontal' method='post'>
				<fieldset id="search">
					<legend><?= $page_title;?></legend>
					<div class="row">
			
					
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
							<input type="text" class="form-control input-sm searchPressEnter" name="search_term" placeholder="function does not work">
						</div>
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
							<label for="radio_filter" class="form-control-label" style="margin-bottom: 0px;">
                              		Only include a particular filter status:
                         		</label>
							
							<div class="radio" style="padding:0px;">
  								<label class="radio-inline">
  									<input type="radio" name="radio_filter" value="no" checked="checked">All
  								</label>
  								<label class="radio-inline">
  									<input type="radio" name="radio_filter" value="yes">Passed
  								</label>
							</div>
						</div>
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-1">
							<button type="submit" value="Submit" name="submit" class="btn btn-primary btn-primary-hover">Search
							</button>
						</div>
					</div>
					<div class="row" style="margin-top: 10px;">
						
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
							<h2 class="raised-card-header">Search by runs on the sequencing instrument</h2>
							<div class="raised-card">
								<u><b>Searchable fields</b></u> (ONLY TRY TO SEARCH ONE OF THESE AT A TIME): 
								<ul>
									
								</ul>
							</div>
						</div>
					</div>
				</fieldset>
			</form>
		</div>
	</div>
</div>
