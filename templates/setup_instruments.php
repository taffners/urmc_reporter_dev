<div class="container-fluid">
	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
			<a href="?page=add_instrument" class="btn btn-primary btn-primary-hover">Add Instrument</a>
		</div>
		<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 d-none">
			<a href="?page=current_service_contracts" class="btn btn-primary btn-primary-hover">Current Service Contracts</a>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			<table class="formated_table sort_table" style="overflow-x:auto;margin-bottom: 10px;">
				<thead>
					<th>Yellow Tag #</th>			
					<th>Task</th>
					<th>Manufacturer</th>
					<th>Model</th>
					<th>Serial#</th>
					<th>Clinical Engin. #</th>
					<th>Description</th>
					<th>Status</th>
					<th>Update Instrument</th>
					<th>Service Contract</th>
					<th>Validation</th>
					<th>Issues</th>
					<th>Repairs</th>
					<th>Maintenance</th>
				</thead>
				<tbody>
<?php
		foreach ($all_instruments as $key => $instrument)
		{
?>
					<tr>
						<td><?= $instrument['yellow_tag_num'];?></td>		
						<td><?= $instrument['task'];?></td>
						<td><?= $instrument['manufacturer'];?></td>
						<td><?= $instrument['model'];?></td>
						<td><?= $instrument['serial_num'];?></td>
						<td><?= $instrument['clinical_engineering_num'];?></td>
						<td><?= $instrument['description'];?></td>
						<td><?= $instrument['instrument_status'];?></td>
						<td><a href="?page=add_instrument&instrument_id=<?=$instrument['instrument_id'];?>" class="btn btn-primary btn-primary-hover">Update<br>Instrument</a></td>
						<td <?php
						if ($instrument['days_till_expiry'] < 0)
						{
							echo 'class="red-background" title="less than zero days till service contract expires"';
						}
						else if ($instrument['days_till_expiry'] > 90)
						{
							echo 'class="green-background" title="greater than 90 days till service contract expires"';
						}

						else if ($instrument['days_till_expiry'] > 60)
						{
							echo 'class="btn-warning" title="less than 60 days till service contract expires"';
						}

						else if ($instrument['days_till_expiry'] === Null)
						{
							echo '';
						}

						else 
						{
							echo 'class="red-background"';
						}

						?>>
							<a href="?page=service_contract&instrument_id=<?=$instrument['instrument_id'];?>"ervice_contract" class="btn btn-primary btn-primary-hover">Service<br>Contract</a>
						</td>
						<td <?php
						if ($instrument['instrument_validation_ids'] != null)
						{
							echo 'class="green-background" title="Validation present"';
						}
						?>>
							<a href="?page=validation&instrument_id=<?=$instrument['instrument_id'];?>"" class="btn btn-primary btn-primary-hover">Validation</a>
						</td>
						<td>
							<a href="?page=work_progress&page_under_construction=Issues&instrument_id=<?=$instrument['instrument_id'];?>"" class="btn btn-primary btn-primary-hover">Issues</a>
						</td>
						<td>
							<a href="?page=work_progress&page_under_construction=Repairs&instrument_id=<?=$instrument['instrument_id'];?>"" class="btn btn-primary btn-primary-hover">Repairs</a>
						</td>
						<td>
							<a href="?page=work_progress&page_under_construction=Maintenance&instrument_id=<?=$instrument['instrument_id'];?>"" class="btn btn-primary btn-primary-hover">Maintenance</a>
						</td>
					</tr>
<?php
		}
?>				
				</tbody>
			</table>
		</div>
	</div>
</div>