<div class="container-fluid">
	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			<?php
			     require_once('templates/tables/pending_ngs_library_pool_table.php');
			?>
		</div>
	</div>
	<div class="row">
<?php
if (isset($_GET['pool_chip_linker_id']) && isset($poolStepTracker) && isset($_GET['ngs_panel_id']))
{

?>
	<!-- report progress bar toggles -->	
	<?php
	require_once('templates/shared_layouts/report_toggle_show_status.php');
	?>

	<!-- Progress bar -->
	<?php
	require_once('templates/shared_layouts/pool_step_nav_bar.php')
	?>
	<!-- update area -->
	<div id="toggle-report-progress-bar" class="col-xs-10 col-sm-10 col-md-10 col-lg-10">
		
		<fieldset>
			<legend>
				<?= $utils->UnderscoreCaseToHumanReadable($page);?>
			</legend>

			<h3 class="raised-card-subheader">Visits in Library Pool</h3>

			<table class="formated_table">
				<thead>
					<th>Reporter#</th>
<?php		
	// This is useful for Thermofisher runs
	if (isset($current_ngs_panel[0]['sequencing_platform_id']) && $current_ngs_panel[0]['sequencing_platform_id'] == 1)
	{
?>			
					<th>Chip</th>

<?php
	}
?>
					<th>Sample Name</th>
					<th>Patient Name</th>
					<th>Version</th>
					<th>MRN</th>
				</thead>
				<tbody>
<?php
	if (isset($visits_in_pool))
	{

		foreach ($visits_in_pool as $key => $visit)
		{
			
?>
					<tr>
						<td><?= $visit['visit_id'];?></td>
<?php		
	// This is useful for Thermofisher runs
	if (isset($current_ngs_panel[0]['sequencing_platform_id']) && $current_ngs_panel[0]['sequencing_platform_id'] == 1)
	{
?>						
						<td><?= $visit['chip_flow_cell'];?></td>
<?php
	}
?>						
						<td><?= $visit['sample_name'];?></td>
						<td><?= $visit['patient_name'];?></td>
						<td><?= $visit['version'];?></td>
						<td><?= $visit['medical_record_num'];?></td>
					</tr>
<?php
		}
	}
?>			
				</tbody>
			</table>
		</fieldset>
	</div>

<?php
}
?>
	</div>
</div>