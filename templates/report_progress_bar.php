<?php
	if (isset($completed_steps) && isset($_GET) && isset($_GET['run_id']) && isset($_GET['patient_id']) && isset($_GET['visit_id']) && isset($runInfoArray[0]['status']) && $runInfoArray[0]['status'] !== 'completed')
	{

?>
		<!-- report progress bar toggles -->	
	<?php
		require_once('templates/shared_layouts/report_toggle_show_status.php');
	?>

		<!-- Progress bar -->
		<div id="report-nav" class="hidden-xs col-sm-2 col-md-2 col-lg-2">
			<div class="vertical-nav">
				<ul class="affix list-unstyled">		


					<li role="presentation" class="vertical-nav-buttons nav-buttons">
						<a href="?page=show_edit_report&run_id=<?= RUN_ID;?>&visit_id=<?= $_GET['visit_id'];?>&patient_id=<?= $_GET['patient_id'];?>" class="btn btn-primary btn-primary-hover <?= $stepTracker->activeButtonStatus('show_edit_report', 'primary');?>" role="button" title="Show a summary of the steps currently completed for this report.">
							Summary
						</a>
					</li>
					<li role="presentation" class="vertical-nav-buttons nav-buttons">
						<a href="?page=message_board&run_id=<?= RUN_ID;?>&visit_id=<?= $_GET['visit_id'];?>&patient_id=<?= $_GET['patient_id'];?>#message_board" class="btn btn-primary btn-primary-hover <?= $stepTracker->activeButtonStatus('message_board', 'primary');?>" role="button" title="Use the message board to send emails to other people in the lab working on NGS reports.">
							Report Message Board
						</a>
					</li>	
<?php
	// add ordered_test_id and sample_log_book_id when there are values to add.  These do not exist for NGS controls
	$stop_reporting_extra_parms = '';
	if (isset($visitArray[0]['sample_log_book_id']) && !empty($visitArray[0]['sample_log_book_id']))
	{
		$stop_reporting_extra_parms.= '&sample_log_book_id='.$visitArray[0]['sample_log_book_id'];
	}

	if (isset($visitArray[0]['ordered_test_id']) && !empty($visitArray[0]['ordered_test_id']))
	{
		$stop_reporting_extra_parms.= '&ordered_test_id='.$visitArray[0]['ordered_test_id'];
	}

	// Switch change_report_type button name depending on current value.  If current value is live set button name to change to validation report

	$change_btn_types = array(
			'live' => '&Delta; to Validation Report',
			'na' => 'Change to Live Report',
			'validation' => 'Change to Live Report',
			'default' => '&Delta; to Report type'
		);

	if (isset($runInfoArray[0]['report_type']) && in_array($runInfoArray[0]['report_type'], array_keys($change_btn_types)) )
	{
		$change_btn_name = $change_btn_types[$runInfoArray[0]['report_type']];
	}
	else
	{
		$change_btn_name = $change_btn_types['default'];
	}

?>									
					<li class="vertical-nav-buttons nav-buttons">
						<a href="?page=stop_test_in_log_book&run_id=<?= RUN_ID;?>&visit_id=<?= $_GET['visit_id'];?>&patient_id=<?= $_GET['patient_id'];?>&ngs_stage=ngs_report_building<?= $stop_reporting_extra_parms;?>" class="btn btn-danger btn-danger-hover <?= $stepTracker->activeButtonStatus('stop_reporting', 'danger');?>" role="button" title="Cancel this report.  It will be hidden from the samples with outstand reports section.">
							Stop Reporting 
							<span class="glyphicon glyphicon-remove-circle"></span>
						</a>
					</li>
<?php
		var_dump($visitArray[0]['control_type_id']);
		if (isset($visitArray[0]['control_type_id']) && empty($visitArray[0]['control_type_id']))
		{	
?>				
					<li class="vertical-nav-buttons nav-buttons">
						<a href="?page=change_report_type&run_id=<?= RUN_ID;?>&visit_id=<?= $_GET['visit_id'];?>&patient_id=<?= $_GET['patient_id'];?>" class="btn btn-danger btn-danger-hover <?= $stepTracker->activeButtonStatus('change_report_type', 'danger');?>" role="button" title="Change the status of the report.  Live reports will be added to the patient electronic medical record and validation reports will not be added.">
							<?= $change_btn_name;?> 
						</a>
					</li>
<?php
		}
	// Show QC run button only for positive control control_type_id = 1 AcroMetrix
	// FOR NOW TURN ON FOR SAMPLES AND CONTROLS

	if 	(
			(
				isset($visitArray[0]['control_type_id']) 	
			&&
				(				
					$visitArray[0]['control_type_id'] === '0'
				)
			) 	||
			$visitArray[0]['control_type_id'] == null			
		)
	{
?>
					<li class="vertical-nav-buttons nav-buttons">
						<a href="?page=qc&run_id=<?= RUN_ID;?>&visit_id=<?= $_GET['visit_id'];?>&patient_id=<?= $_GET['patient_id'];?>" class="btn <?php
							if(isset($qc_run_step_status) && $qc_run_step_status == 'not_complete')
							{
								echo 'btn-warning btn-warning-hover';
								echo $stepTracker->activeButtonStatus('qc', 'warning');
							}
							else
							{
								echo 'btn-success btn-success-hover';
								echo $stepTracker->activeButtonStatus('qc', 'success');
							}
						?> " role="button">
							QC Run
						</a>
					</li>
<?php
	}
?>
<?php
	// Show QC variant button for positive control or sample.
	if 	(
			(
				isset($visitArray[0]['control_type_id']) 	
			&&
				(					
					$visitArray[0]['control_type_id'] === '0'
				)
				
			) ||
			$visitArray[0]['control_type_id'] == null
		)
	{
?>
					<li class="vertical-nav-buttons nav-buttons">
						<a href="?page=qc_strand_bias&run_id=<?= RUN_ID;?>&visit_id=<?= $_GET['visit_id'];?>&patient_id=<?= $_GET['patient_id'];?>" class="btn <?php
							if(isset($qc_variant_step_status) && $qc_variant_step_status == 'not_complete')
							{
								echo 'btn-warning btn-warning-hover';
							}
							else
							{
								echo 'btn-success btn-success-hover';
							}
						?>" role="button">
							QC Variant
						</a>
					</li>
<?php
	}
?>
					<?php
					
						// add a link for all the current steps in StepTracker
						$steps = $stepTracker->GetSteps();
						foreach ($steps as $key =>$value)
						{
							if ($value === 'verify_variants')
							{
					?>	
								<li role="presentation" class="vertical-nav-buttons nav-buttons">
									<a href="?page=<?= $value;?>&run_id=<?= RUN_ID;?>&visit_id=<?= $_GET['visit_id'];?>&patient_id=<?= $_GET['patient_id'];?>&variant_filter=included" class="btn <?php echo $stepTracker->ButtonStatus($completed_steps, $value);?>" role="button">
									<?= $utils->UnderscoreCaseToHumanReadable($value);?>
									</a>
								</li>
					<?php
							}
							elseif ($value === 'generate_report')
							{
					?>
								<li role="presentation" class="vertical-nav-buttons nav-buttons">
									<a href="?page=<?= $value;?>&run_id=<?= RUN_ID;?>&visit_id=<?= $_GET['visit_id'];?>&patient_id=<?= $_GET['patient_id'];?>" target="_blank" class="btn <?php echo $stepTracker->ButtonStatus($completed_steps, $value);?>" role="button">
									<?= $utils->UnderscoreCaseToHumanReadable($value);?>
									</a>
								</li>
					<?php
							}
							else
							{
					?>	
								<li role="presentation" class="vertical-nav-buttons nav-buttons<?php
								if (isset($num_snvs) && $num_snvs === 0 && ($value === 'add_tiers' || $value === 'add_variant_interpetation'))
								{
									echo ' hide';
								}

								?>" >

									<a href="?page=<?= $value;?>&run_id=<?= RUN_ID;?>&visit_id=<?= $_GET['visit_id'];?>&patient_id=<?= $_GET['patient_id'];?>" class="btn <?php 

											// if no variants were found previous step could be different so might need to activate the button ahead of time 
											if (isset($num_snvs))
											{
												echo $stepTracker->ButtonStatus($completed_steps, $value, $num_snvs);											
											}
											else
											{
												echo $stepTracker->ButtonStatus($completed_steps, $value);
											}


									?>" role="button">
									<?= $utils->UnderscoreCaseToHumanReadable($value);?>
									</a>
								</li>
					<?php
							}
					
						
						}

					?>

				</ul>
			</div>
		</div>
<?php
	}
?>