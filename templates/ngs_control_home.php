<div class="container-fluid">

	<div class="row" style="overflow-x:auto;">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			<a href="?page=add_new_ngs_control_type" class="btn btn-primary btn-primary-hover">
				Add New Control (Use duplicate whenever possible to reduce possible errors)
			</a>
		</div>
	</div>

	
	<div class="row" style="overflow-x:auto;">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			<table class="formated_table sort_table">
				<thead>
					<th>ID</th>
					<th>User Name</th>
					<th>Control Type</th>
					<th>Name</th>
					<th>Catalog Number</th>
					<th>Description</th>
					<th>Dummy Last Name</th>
					<th>Dummy First Name</th>
					<th>Dummy Sex</th>
					<th>Dummy DOB</th>
					<th>Dummy MRN</th>
					<th>Time Stamp</th>
					<th>Update Control</th>
					<th>Update/Add/View Expected Variants</th>
					<th>Duplicate</th>
				</thead>
				<tbody>
<?php
	if (isset($all_controls))
	{

		foreach($all_controls as $key => $c)
		{
?>
					<tr>
						<td><?= $c['control_type_id'];?></td>
						<td><?= $c['user_name'];?></td>
						<td><?= $c['type'];?></td>
						<td><?= $c['control_name'];?></td>
						<td><?= $c['catalog_number'];?></td>
						<td><?= $c['description'];?></td>
						<td><?= $c['dummy_last_name'];?></td>
						<td><?= $c['dummy_first_name'];?></td>
						<td><?= $c['dummy_sex'];?></td>
						<td><?= $c['dummy_dob'];?></td>
						<td><?= $c['dummy_medical_record_num'];?></td>
						<td><?= $c['time_stamp'];?></td>
						<td>
							<a href="?page=add_new_ngs_control_type&control_type_id=<?= $c['control_type_id'];?>" class="btn btn-primary btn-primary-hover">
								Update Control
							</a>
						</td>
						<td>
							<a href="?page=expected_variants&control_type_id=<?= $c['control_type_id'];?>" class="btn btn-primary btn-primary-hover">
								Expected variants
							</a>
						</td>
						<td>
							<a href="?page=duplicate_control&add_type=duplicate&control_type_id=<?= $c['control_type_id'];?>" class="btn btn-primary btn-primary-hover">
								Duplicate
							</a>
						</td>
					</tr>
<?php
		}
	}
?>
				</tbody>
			</table>
		</div>
	</div>
</div>
