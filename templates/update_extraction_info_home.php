<div class="container-fluid">
	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				
				<div class="alert alert-info row">
					<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
						<ul>
		<?php
					if (isset($extraction_users[0]['extraction_method']) && !empty($extraction_users[0]['extraction_method']))
					{
		?>
							<li>Extraction Method: <?= $extraction_users[0]['extraction_method'];?></li>
		<?php
					}

					if (isset($extraction_users[0]['quantification_method']) && !empty($extraction_users[0]['quantification_method']))
					{
		?>
							<li>Quant Method: <?= $extraction_users[0]['quantification_method'];?></li>
		<?php
					}

					if (isset($extraction_users[0]['dna_conc_control_quant']) && !empty($extraction_users[0]['dna_conc_control_quant']))
					{
		?>
							<li>Quant Control: <?= $extraction_users[0]['dna_conc_control_quant'];?> ng/&mu;l</li>
		<?php				
					}

					if (isset($extraction_users[0]['stock_conc']) && !empty($extraction_users[0]['stock_conc']))
					{
		?>
							<li>Stock Conc.: <?= $extraction_users[0]['stock_conc'];?></li>
		<?php				
					}

					if (isset($extraction_users[0]['dilution_exist']) && !empty($extraction_users[0]['dilution_exist']))
					{
		?>
							<li>Dilution Exists? <?= $extraction_users[0]['dilution_exist'];?> </li>
		<?php				

					}

					if (isset($extraction_users[0]['dilutions_conc']) && !empty($extraction_users[0]['dilutions_conc']))
					{
		?>
							<li>Actual Dilution Conc.: <?= $extraction_users[0]['dilutions_conc'];?> </li>
		<?php				

					}

					if (isset($extraction_users[0]['times_dilution']) && !empty($extraction_users[0]['times_dilution']))
					{
		?>
							<li>X Dilution: <?= $extraction_users[0]['times_dilution'];?> </li>
		<?php				
					}

					if (isset($extraction_users[0]['purity']) && !empty($extraction_users[0]['purity']))
					{
		?>
							<li>260/280: <?= $extraction_users[0]['purity'];?> </li>
		<?php								
						
					}
					if (	
							!empty($extraction_users[0]['vol_dna']) && //v1
							!empty($extraction_users[0]['stock_conc']) && // c1
							!empty($extraction_users[0]['vol_eb']) && // 
							!empty($extraction_users[0]['dilution_final_vol']) && // v2
							!empty($extraction_users[0]['target_conc']) // c2
						)
					{
						$summary ='Dilution Calc C1 V1 = C2 V2: <br>';

						$summary.= '('.$extraction_users[0]['stock_conc'].')('.$extraction_users[0]['vol_dna'].') = ('.$extraction_users[0]['target_conc'].')('.$extraction_users[0]['dilution_final_vol'].')<br>';

						
						$summary.='EB Volume V2 - V1:<br>';

						$summary.= $extraction_users[0]['dilution_final_vol'].' - '.$extraction_users[0]['vol_dna'].' = '.$extraction_users[0]['vol_eb'].'<br>';

		?>
							<li><?= $summary;?> </li>

	<?php

				}

	?>
						</ul>
					</div>
					<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">

						<strong>Update the info highlighted in blue info permanently.</strong><br>
						<a href="?page=update_extraction_info&ordered_test_id=<?= $_GET['ordered_test_id'];?>&sample_log_book_id=<?= $_GET['sample_log_book_id'];?>&extraction_log_id=<?= $extraction_users[0]['extraction_log_id'];?>" class="mt-2 btn btn-danger btn-danger-hover d-print-none" title="update extraction info">&Delta; Extraction Info</a>
					</div>
				</div>
				<div class="alert alert-success row">
					<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
						<ul>
			<?php

					if (isset($extraction_users[0]['instruments_used']) && !empty($extraction_users[0]['instruments_used']))
					{
						$summary = 'Instrument used: '.$extraction_users[0]['instruments_used'].'<br>';
			?>
						<li><?= $summary;?> </li>
			<?php				
					}

					$summary = '';

					foreach ($extraction_users as $key => $task_user)
					{
						$summary.= $task_user['task'].':'.$task_user['users'].'<br>';
					}
			?>
						<li><?= $summary;?></li>
					</ul>
				</div>
				<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">

						<strong>Update the info highlighted in green info permanently. (WORK IN PROGRESS CHECK BACK SOON)</strong>
					</div>
			</div>
		</div>
	</div>
</div>
			