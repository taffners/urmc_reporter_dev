<!-- 
	requirements:
		- knowledge_base array including all variant info from the knowledge base
		- utils class 
		- db class
	
	viewing options:
		- if interpretation and tier info should be viewed unhide tier-interpretation-section
 -->

<?php
	$gets_redirect_str = str_replace('page=', 'redirect_page=', $_SERVER['QUERY_STRING']);
?>

<div class="container-fluid">
	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
    

			<section>
				<table class="formated_table <?php
					if ($_GET['page'] !== 'update_tiers_kb')
					{
						echo 'sort_table';
					}

				?>">

					<thead>
			<?php	
						if ($page == 'knowledge_base' || $page == 'verify_variants' || $page == 'tissue_specfic_gene_info')
						{
			?>		
						<th>&Delta; Tier</th>
						<th>Interpt</th>
			<?php
						}


						foreach ($ttk_vw_cols as $k => $col)
						{
							$col_name = $col['COLUMN_NAME'];
							if (!$utils->InputAID($col_name) &&  $col_name != 'time_stamp' && $col_name != 'notes' && $col_name != 'exon' && $col_name != 'dbsnp' && $col_name != 'pfam' && $col_name != 'hgnc')
							{

			?>
							<th><?= $utils->UnderscoreCaseToHumanReadable($col_name);?></th>
			<?php				
							}	

							// Insert Varsome column after cosmic
							if ($col_name === 'cosmic')
							{
			?>								
								<th>Varsome</th>
			<?php
							}
						}
						if ($page == 'tissue_specfic_gene_info')
						{
						?>
							<th>Patient Report</th>
						<?php
						}
			?>	

					</thead>
					<tbody>

			<!-- Find each variant in the knowledge_base array make a new table -->
          	<?php         	
          		if (isset($knowledge_base) && isset($ttk_vw_cols))
          		{

                    for($i=0;$i<sizeof($knowledge_base);$i++)
                    {

                    	if($page == 'knowledge_base' || $page == 'add_interpretation' || $page == 'update_tiers_kb' || $page == 'tissue_specfic_gene_info')
                    	{
     		?>    					
     					<tr id="knowledge_row_<?= $utils->ReplaceSpecialChars($knowledge_base[$i]['genes']);?>_<?= $utils->ReplaceSpecialChars($knowledge_base[$i]['coding']);?>_<?= $utils->ReplaceSpecialChars($knowledge_base[$i]['amino_acid_change']);?>">
     		<?php
     					}
     					else if (isset($variantArray))
     					{
     		?>
						<tr id="knowledge_id_<?= $variantArray[$i]['knowledge_id']; ?>">
			<?php	
						}
						if ($page == 'knowledge_base' || $page == 'verify_variants' || $page == 'tissue_specfic_gene_info')
						{
			?>					
							<td>
					         		<a href="?page=update_tiers_kb&knowledge_id=<?= $knowledge_base[$i]['knowledge_id']; ?>" target="_blank" rel="noopener" type="button" class="btn btn-primary btn-primary-hover toggle_textarea" title="Update tiers for this variant">				       
					          		<span class="far fa-hand-point-right"></span>
					          	</a>
					         	</td>
					         	<td>
					         		<a href="?page=add_interpretation&knowledge_id=<?= $knowledge_base[$i]['knowledge_id']; ?>" target="_blank" type="button" rel="noopener" class="btn btn-primary btn-primary-hover toggle_textarea" title="Add an interpretation for this variant">
					          	
					          		<span class="far fa-hand-point-right"></span>
					          	</a>
					         	</td>
			<?php
						}
						
						foreach ($ttk_vw_cols as $k => $col)
						{
							$col_name = $col['COLUMN_NAME'];
							if (!$utils->InputAID($col_name) && $col_name != 'time_stamp' && $col_name != 'notes' && $col_name != 'exon' && $col_name != 'dbsnp' && $col_name != 'pfam' && $col_name != 'hgnc')
							{
								// for cell values larger than 20 add a more span
								$cell_val = $knowledge_base[$i][$col_name];

								// add all the links to outside databases
								if ($col_name == 'pmid' || $col_name == 'transcript')
								{
									$link = $utils->ConvertDatabaseVar($col_name, $knowledge_base[$i][$col_name]);
			?>
									<td><?= $link;?></td>
			<?php
								}
								elseif ($col_name == 'genes')
								{									
									$link = $utils->ConvertDatabaseVar($col_name, $knowledge_base[$i][$col_name], $knowledge_base[$i]['hgnc']);
			?>
									<td><?= $link;?></td>
			<?php
								}
								elseif ($col_name == 'cosmic')
								{									
									$link = $utils->ConvertDatabaseVar($col_name, $knowledge_base[$i][$col_name]);
			?>
									
									<td><?= $link;?> 
									<?php
									if (isset($user_permssions) &&  strpos($user_permssions, 'director') !== false)
									{
									?>
										<a href="?page=update_cosmic_num&knowledge_id=<?= $knowledge_base[$i]['knowledge_id']; ?>&<?= $gets_redirect_str;?>"title="Update Cosmic Number"><span class="fas fa-edit"></span></a></td>
									<?php
									}
									?>
			<?php
									/////////////////////////////////////////////////////////////////////////////////////////////
									// Add Varsome Link.  Sometimes there's more than one accession number so the varsome_accession_link can not be used
									// if NGS panel is available only provide the link for that panel.  However, there is two genes that have two accession numbers.
									// SMO for OFA and BCOR for myeloid
									// | SMO    | NM_005631.4,NM_005631.4     |            1 |						
									// | BCOR   | NM_017745.5,NM_001123385.1  |            2 |
									/////////////////////////////////////////////////////////////////////////////////////////////						
									if 	(
											isset($knowledge_base[$i]['accession_num_count']) && 
											is_numeric($knowledge_base[$i]['accession_num_count']) &&
											intval($knowledge_base[$i]['accession_num_count'] )  == 1
										)
									{
			?>
										<td><a href="<?= $knowledge_base[$i]['varsome_accession_link'];?>" target="_blank" rel="noopener">Varsome</a></td>
			<?php
									}
									elseif 	(
												isset($knowledge_base[$i]['accession_num_count']) && 
												is_numeric($knowledge_base[$i]['accession_num_count'])  &&
												intval($knowledge_base[$i]['accession_num_count'] ) > 1
											)
									{
			?>
										<td><a href="<?= $knowledge_base[$i]['varsome_simple_link'];?>" target="_blank" rel="noopener">Varsome</a></td>
			<?php							
									}
									elseif (isset($knowledge_base[$i]['varsome_simple_link']) && !empty($knowledge_base[$i]['varsome_simple_link']))
									{
			?>
										<td><a href="<?= $knowledge_base[$i]['varsome_simple_link'];?>" target="_blank" rel="noopener">Varsome</a></td>
			<?php							
									}
									else
									{
			?>
										<td></td>
			<?php							
									}
			?>									
									
			<?php
								}
								// add link to ensembl for location of mutation
								//http://www.ensembl.org/Homo_sapiens/Location/View?r=6:151807913-152098966
								elseif ($col_name == 'loc')
								{
									$link = $utils->ConvertDatabaseVar($col_name, $knowledge_base[$i][$col_name], $knowledge_base[$i]['chr']);
			?>	
									<td><?= $link;?></td>
			<?php
								}
								
								elseif (strlen($cell_val) > 26)
								{

									// get the first 20 char of string
									$substr_cell_val = substr($cell_val, 0, 26);									
			?>				
									<td>	
										<span id="teaser_<?= $col_name;?>_<?= $knowledge_base[$i]['knowledge_id']?>" class="teaser">
											<?= $substr_cell_val;?>
										</span>

										<span id="complete_<?= $col_name;?>_<?= $knowledge_base[$i]['knowledge_id']?>" class="complete d-none">
											<?= $cell_val;?>				 
										</span>

										<span id="show_complete_<?= $col_name;?>_<?= $knowledge_base[$i]['knowledge_id']?>" class="show-complete">
											more ...				 
										</span>

										<span id="show_teaser_<?= $col_name;?>_<?= $knowledge_base[$i]['knowledge_id']?>" class="show-teaser d-none">
											less ...				 
										</span>
									</td>
			<?php
								}
								else
								{
			?>
									<td><?= $cell_val;?></td>
			<?php
								}

							}
						}
						if ($page == 'tissue_specfic_gene_info')
						{
						?>
							<td>
								<a href="?page=download_report&amp;visit_id=<?= $knowledge_base[$i]['visit_id'];?>&amp;patient_id=<?= $knowledge_base[$i]['patient_id'];?>&amp;run_id=<?= $knowledge_base[$i]['run_id'];?>&amp;ngs_panel_id=<?= $knowledge_base[$i]['ngs_panel_id'];?>" type="button" class="btn btn-success btn-success-hover" target="_blank" rel="noopener"><img src="images/adobe-pdf-icon.png" alt="pdf icon" style="height:30px;width:30px"></a>
							</td>
						<?php
						}
					}
					
				}
			?>							
						</tr>
					</tbody>
				</table>
			</section>

		</div>
	</div>
</div>