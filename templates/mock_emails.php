<div class="container-fluid">
	<div class="row">
<?php
	$two_identifers = 'Patient Name: (Two Identifiers needed)<br>
<ul>
	<li>MRN:</li>
	<li>DOB</li>
	<li>SoftPath</li>
	<li>SoftLab Order</li>
</ul>
';


?>
		<!-- 
			<hr class="section-mark">
			<h2 style="color:red;"></h2>
			<h3>Subject line: </h3>


			<br><br>
			Thank you,<br>
			Molecular Diagnostics Laboratory 
		-->

		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

			<hr class="section-mark">
			<h2 style="color:red;">For FFPE samples ordered for BRAF (7/27/2020)</h2>
			<h3>Subject line: Please confirm ngs panel order</h3>
			<?= $two_identifers;?>
			We received a specimen ordered for BRAF.  This gene is no longer offered as a single analyte assay due to coverage by our in-house solid tumor ngs panel.  
 			<br><br>
			Please confirm that you would like us to proceed with the ngs panel.  If you prefer to send this out for single gene analysis please contact the Send-out Lab Senior Techs for further instruction.  We will hold the sample for confirmation.
			<br><br>
			Thank you,<br>
			Molecular Diagnostics Laboratory

			<hr class="section-mark">
			<h2 style="color:red;">FLT3 single gene assay order (8/11/2020)</h2>
			<h3>Subject line: Please confirm FLT3 single gene assay order</h3>

			We received a specimen ordered for NPM1 and FLT3 in addition to the heme malignancy ngs panel.  NPM1 is no longer offered as a single gene assay due to coverage by the ngs panel.  FLT3, although also on the panel, is still offered.  It is ordered in conjunction to the panel in cases that require a quicker turnaround time.
			<br><br>
			<?= $two_identifers;?>
			Please confirm that you would like us to proceed with FLT3 in addition to the ngs panel.
			<br><br>
			Thank you,<br>
			Molecular Diagnostics Laboratory

			<hr class="section-mark">
			<h2 style="color:red;">Send to Pathologist and cc Special Stains Tech: (7/31/2020)</h2>
			<h3>Subject: Request for Re-cut</h3>

			We did not obtain enough DNA from <span style="background-color:yellow;text-decoration:underline;">(fill case number & block)</span> to complete the ordered assay <span style="background-color:yellow;text-decoration:underline;">(assay name)</span>.   Please request a re-cut of the specimen to be sent to us or confirm that the test should be canceled due to insufficient quantity.
			<br><br>  
			Thank you,<br>
			Molecular Diagnostics Laboratory


			<hr class="section-mark">
			<h2 style="color:red;">Send to Pathologist: (7/28/2020)</h2>
			<h3>Subject: IDH1 offered as part of our in-house NGS panel</h3>

			We received an IDH1 order for <span style="background-color:yellow;text-decoration:underline;">(specimen type)</span>  specimen <span style="background-color:yellow;text-decoration:underline;">(path order# / lab order# if you don't have a path#)</span>.   We typically perform these requests as part of the solid tumor ngs panel (<u>order code STMP</u>).  Would you like us to cancel IDH1 and add STMP?
 			<br><br>
			If they write back saying that they would like to stick with just IDH1 if that is an option, then proceed with IDH1.  Since there is debate about whether what platform we will run these on, please make sure I am aware if there is an IDH1 single gene request that isn't being changed to STMP. 


			

			


			<hr class="section-mark">
			<h2 style="color:red;">Send to Pathologist: (7/31/2020)</h2>
			<h3>Subject: % Tumor Estimation</h3>

			We received sample <span style="background-color:yellow;text-decoration:underline;">(fill case number & block)</span> ordered for <span style="background-color:yellow;text-decoration:underline;">(assay name)</span>.  Please provide us with a tumor percentage estimate for this specimen.  Testing will be held until this information is provided


			<br><br>  
			Thank you,<br>
			Molecular Diagnostics Laboratory


			<hr class="section-mark">
			<h2 style="color:red;">Send to Doctor: (8/11/2020)</h2>
			<h3>Subject line: Please confirm NPM1 single gene assay order</h3>

			We received a specimen for <span style="background-color:yellow;text-decoration:underline;">( Name, DOB, or MRN, Order #)</span>  ordered for NPM1.  This is no longer offered as a single gene assay due to coverage by the heme malignancy ngs panel. 
			<br><br>  
			If you prefer the single gene assay we can give this sample to the send-out techs to be sent to a reference laboratory.  Otherwise, please let us know to process the sample for our in-house ngs panel.
			<br><br>  
			Thank you,
			Molecular Diagnostics Laboratory



		</div>
	</div>

</div>