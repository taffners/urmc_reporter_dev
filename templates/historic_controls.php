<div class="container-fluid">
	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			<div class="raised-card" style="margin-bottom:15px;">
				<h3 class="raised-card-subheader"><?=$control_info[0]['control_name'];?> <?=$control_info[0]['type'];?> control</h3>
<?php
	require_once('templates/lists/control_overview.php');
?>				
			</div>
		</div>
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			<div class="raised-card" style="margin-bottom:15px;">
				<h3 class="raised-card-subheader"><?=$control_info[0]['control_name'];?> <?=$control_info[0]['type'];?> control Historic Observed Information <br>for <?= $ngs_panel_info[0]['type'];?></h3>
<?php
	require_once('templates/tables/historic_control_table.php');
?>				
			</div>
		</div>
	</div>
	<div class="row" id="outstanding-reports-row">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		     <?php
		          require_once('templates/lists/reports_list.php');
		     ?>
		</div>
	</div>

</div>

