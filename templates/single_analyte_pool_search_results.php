<div class="container-fluid">
	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 float-right">		
							
			<a href="?page=new_home&single_analyte_pool_view=my_pools" class="btn btn-primary btn-primary-hover">My Pools</a>

			<a href="?page=new_home&single_analyte_pool_view=all_pools" class="btn btn-primary btn-primary-hover">All Pools</a>

			<a href="?page=new_home&single_analyte_pool_view=reportable_pools" class="btn btn-primary btn-primary-hover">Reportable Pools</a>

			<a href="?page=search_single_analyte_pools" class="btn btn-primary btn-primary-hover float-right" title="Search all historic pools"> Search SAPs</a>
		</div>
	</div>
<?php 
	require_once('templates/shared_layouts/single_analyte_pool_table.php');
?>

</div>