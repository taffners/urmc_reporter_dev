<?php
if (isset($_GET['pool_chip_linker_id']) && isset($poolStepTracker) && isset($_GET['ngs_panel_id']))
{

?>
	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			<?php
			     require_once('templates/tables/pending_ngs_library_pool_table.php');
			?>
		</div>
	</div>
	<div class="row">
		<!-- report progress bar toggles -->	
		<?php
		require_once('templates/shared_layouts/report_toggle_show_status.php');
		?>

		<!-- Progress bar -->
		<?php
		require_once('templates/shared_layouts/pool_step_nav_bar.php')
		?>
		<!-- update area -->
		<div id="toggle-report-progress-bar" class="col-xs-10 col-sm-10 col-md-10 col-lg-10">
			<form id="add_pool_conc" class="form" method="post" class="form-horizontal">
				<fieldset>

					<legend>
						Download Ion Torrent Run Templates
					</legend>
				
					<div class="row alert alert-info">
											
						<!--///////////////////////////////////////////////
							// Hidden fields 
							///////////////////////////////////////////////
						-->

				<?php
					// Get sequencing date.
					if 	(	
							!isset($pending_pools[0]['ngs_run_date']) || 
							empty($pending_pools[0]['ngs_run_date']) ||
							$pending_pools[0]['ngs_run_date'] === '0000-00-00'
						)
					{
						$tomorrow = new DateTime('tomorrow');
						$seq_date = $tomorrow->format("m/d/Y");
					}						
					else 
					{
						$seq_date = $dfs->ChangeDateFormatUS($pending_pools[0]['ngs_run_date'], FALSE); 
					}

					$seq_date_YYYYMMDD = $utils->US_DateToYYYYMMDD($seq_date);

					$run_plan_name_A = 'Run'.$pending_pools[0]['excel_workbook_num_id'].'_'.$pending_pools[0]['library_pool_id_A'].'_';
					$run_plan_name_A.=$seq_date_YYYYMMDD.'_A_'.OFA_PLANNAME;

					$run_plan_name_B = 'Run'.$pending_pools[0]['excel_workbook_num_id'].'_'.$pending_pools[0]['library_pool_id_B'].'_';
					$run_plan_name_B.=$seq_date_YYYYMMDD.'_B_'.OFA_PLANNAME;

				?>						
						<div class="form-group row d-none">
							<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
								<label for="chip_count" class="form-control-label" >Chip Count</label>
							</div>
							<div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">
								<input id="chip_count" type="number" class="form-control turn-off-enter-key" name="chip_count" value="<?= $chip_count;?>"/>
							</div>
						</div>
						<div class="form-group row d-none">
							<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
								<label for="excel_workbook_num_id" class="form-control-label">excel_workbook_num_id</label>
							</div>
							<div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">
								<input id="excel_workbook_num_id" type="number" class="form-control turn-off-enter-key" name="excel_workbook_num_id" value="<?= $utils->GetValueForUpdateInput($pending_pools, 'excel_workbook_num_id');?>"/>
							</div>
						</div>	

						<div class="form-group row d-none">
							<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
								<label for="library_pool_id_A" class="form-control-label">library_pool_id_A</label>
							</div>
							<div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">
								<input id="library_pool_id_A" type="number" class="form-control turn-off-enter-key" name="library_pool_id_A" value="<?= $utils->GetValueForUpdateInput($pending_pools, 'library_pool_id_A');?>"/>
							</div>
						</div>	

						<div class="form-group row d-none">
							<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
								<label for="library_pool_id_B" class="form-control-label">library_pool_id_B</label>
							</div>
							<div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">
								<input id="library_pool_id_B" type="number" class="form-control turn-off-enter-key" name="library_pool_id_B" value="<?= $utils->GetValueForUpdateInput($pending_pools, 'library_pool_id_B');?>"/>
							</div>
						</div>	
						<div class="form-group row d-none">
							<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
								<label for="OFA_PLANNAME" class="form-control-label">OFA_PLANNAME</label>
							</div>
							<div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">
								<input id="OFA_PLANNAME" type="text" class="form-control turn-off-enter-key" name="OFA_PLANNAME" value="<?= OFA_PLANNAME;?>"/>
							</div>
						</div>
						<div class="form-group row d-none">
							<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
								<label for="run_plan_name_A" class="form-control-label">run_plan_name_A</label>
							</div>
							<div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">
								<input id="run_plan_name_A" type="text" class="form-control turn-off-enter-key" name="run_plan_name_A" value="<?= $run_plan_name_A;?>"/>
							</div>
						</div>
						<div class="form-group row d-none">
							<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
								<label for="run_plan_name_B" class="form-control-label">run_plan_name_B</label>
							</div>
							<div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">
								<input id="run_plan_name_B" type="text" class="form-control turn-off-enter-key" name="run_plan_name_B" value="<?= $run_plan_name_B;?>"/>
							</div>
						</div>

						
<?php
	}
?>						
						<div class="form-group row">
							<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="font-size:18px;">			
			  					<span><strong>Steps to run Ion Torrent:</strong></span>
			  					<ol>
			  						<li>Add Sequencing date
			  							<ul>
			  								<li>
			  									<div class="form-group row required-input">
													<div class="col-xs-12 col-sm-4 col-md-3 col-lg-3">
														<label for="ngs_run_date" class="form-control-label" >Sequencing Date: <span class="required-field">*</span></label>
													</div>
													<div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">
														<input type="text" class="form-control" id="ngs_run_date" name="ngs_run_date" value="<?= $seq_date;?>" pattern="\d{1,2}/\d{1,2}/\d{4}" required/>
													</div>
												</div>
											</li>
			  							</ul>
			  						</li>
			  						<li><strong>Download</strong> run templates
			  							<ul>
			  								<li>
			  									<button type="button" id="download-ofa-template" class="btn btn-success btn-success-hover" data-pool_chip_linker_id="<?=$_GET['pool_chip_linker_id'];?>">
													Download Run Templates
												</button>
											</li>
			  							</ul>

			  						</li>
			  						<li>Go to Torrent Server: <u><?= $API_Utils->getServerLink('Torrent_Server');?></u></li>
			  						<li>Click on the <strong>Plan tab.</strong> Then click on <strong><?= OFA_PLANNAME;?></strong><br><img src="images/Torrent_server_plan_template.png" alt="Image of Torrent Server and how to plan a run template."></li>
			  						
			  						<li style="margin-top:15px;">
			  							Change <strong>Run Plan Name</strong> to the field below which corresponds the the chip
			  							<ul>
			  								<li id="run-name-chip-a-list">Chip A
			  									<div class="row">
			  										<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" id="run-name-chip-a-copy-paste">
			  											<?= $run_plan_name_A;?>
			  										</div>
			  										<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			  											<svg id="run-name-chip-a-barcode">

			  											</svg>
			  										</div>
			  									</div>

			  								</li>
									<?php
										if (isset($pending_pools[0]['library_pool_id_B']) && !empty($pending_pools[0]['library_pool_id_B']))
										{
									?>			  								
			  								<li id="run-name-chip-a-list">Chip B
			  									<div class="row">
			  										<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" id="run-name-chip-b-copy-paste">
			  											<?= $run_plan_name_B;?>
			  										</div>
			  										<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			  											<svg id="run-name-chip-b-barcode">

			  											</svg>
			  										</div>
			  									</div>

			  								</li>
			  						<?php
			  							}
			  						?>
			  							</ul>
			  						</li>

			  						<!-- <li style="margin-top:15px;">
			  							Change <strong>Run Plan Name</strong>.  Before the default name (yellow highlighted below) add the date (YYMMDD) and chip (red underlined below).  A run number that links .
			  							<br><img src="images/Torrent_server_plan_template2.png" alt="Image of Torrent Server and how to plan a run template.">
			  						</li> -->
			  						<li>
			  							Click on <strong>Load Samples Table</strong> button.  <strong>Upload</strong> Template Chip A downloaded in step 1.  Then <strong>repeat steps 3, 4, and 5 for Template Chip B</strong>.
			  						</li>
			  						<li>Click the <strong>submit button</strong> below once everything is done.</li>
			  					</ol>
			  				</div>
			  			</div>
				  		

			  			
			  			<div class="form-group row">
							<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
									
							</div>							
						</div>
						
					</div>


					<?php
					// add form submit, previous, and next buttons.
					require_once('templates/shared_layouts/form_pool_submit_nav_buttons.php');
					?>
								
				</fieldset>
			</form>
		</div>


	

	</div>
</div>