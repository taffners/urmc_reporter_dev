<?php
	require_once('templates/lists/run_info_list.php');
?>

<div class="container-fluid">
	<div class="row">
		<?php
			require_once('templates/shared_layouts/report_progress_bar.php');
		?>

		<!-- update area -->
		<div id="toggle-report-progress-bar" class="col-xs-10 col-sm-10 col-md-10 col-lg-10" style="height:500px;">
			<form id="add_interpts_variants_form" class="form" method="post" >
				<fieldset>
					<legend id='new_legend' class='show'>
						Add Summary of Report
					</legend>
			<?php
				if (isset($patientArray)  && isset($visitArray))
				{
					require_once('templates/tables/patient_visit_report_table.php');
					require_once('templates/shared_layouts/panel_info_report.php');	

					// SAMPLE INFORMATION 
					require_once('templates/shared_layouts/sample_information.php');
				}	
				require_once('templates/tables/mutant_tables.php');
					
				require_once('templates/tables/low_cov_tables.php');
			?>
					<!-- INTERPRETATION -->
					<div class="row">
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
							<span class="report-section-bold">
								INTERPRETATION:
							</span>
							<div class="form-group">
						<?php
							if($num_snvs === 0 && isset($allSummaries) && sizeof($allSummaries) > 0 )
							{
						?>
								<div class="alert alert-success" class="form-control-label">
									Add a summary for this report.  Either type a summary in the text box below or click on a previous a summary in the previous summaries list. Summaries for reports which do not have mutants are saved and can be reused.  This will be the only section of the interpretation for this report since no mutants are being reported.  
								</div>	
						<?php
							}
							else
							{
						?>
								<div class="alert alert-success" class="form-control-label">
									Add a summary of the entire report.
								</div>	
						<?php
							}
						?>
								<textarea id="summary_textarea" class="form-control" name="summary" rows="5" maxlength="65535"><?php

										if (isset($runInfoArray) )
										{
											echo $runInfoArray[0]['summary'];
										}
								;?></textarea>
								
							</div>
							<div>
								<?= $utils->AddPMIDLinkUrls($interpt_without_summary);?>
							</div>
						</div>
					</div>
         				
					<!-- ADD PREVIOUS SUMMARIES FOR NO VARIANT REPORTS -->
			<?php
				if($num_snvs === 0 && isset($allSummaries) && sizeof($allSummaries) > 0 )
				{
			?>
					<div class="row">
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
							<span class="report-section-bold">
								PREVIOUS SUMMARIES: 
							</span>
							<ul>
			<?php
						foreach ($allSummaries as $key => $summary)
						{
			?>
								<li class="text-hover add-summary" data-summary="<?= $summary['summary'];?>">
									<?= $summary['summary'];?>
								</li>
			<?php
						}
			?>
							</ul>
						</div>
					</div>
			<?php
				}
			?>

					<!-- add the common hidden fields for every form -->
					<?php
						require_once('templates/shared_layouts/form_hidden_fields.php')
					?>

         				<!-- add submit and nav buttons -->
					<?php
						require_once('templates/shared_layouts/form_submit_nav_buttons.php')
					?>
				</fieldset>
			</form>

		</div>
	</div>
</div>

