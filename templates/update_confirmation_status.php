
<!-- pending confirmations -->
<div class="container">
	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			<form class="form" method="post" class="form-horizontal">
			     <fieldset id="outstanding-reports-fieldset">
			     	<legend>Update Pending Confirmations</legend>
					<div class="form-group row d-none">
						<div class="col-xs-12 col-sm-4 col-md-4">
							<label for="confirm_id" class="form-control-label">confirm_id: <span class="required-field">*</span></label>
						</div>
						<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
							<input type="number" class="form-control" id="confirm_id" name="confirm_id" value="<?php 
							if(isset($pending_confirmations['confirm_id']))
							{
								echo $pending_confirmations['confirm_id'];
							}
						?>" readonly/>
						</div>
					</div>
					<div class="form-group row d-none">
						<div class="col-xs-12 col-sm-4 col-md-4">
							<label for="observed_variant_id" class="form-control-label">observed_variant_id: <span class="required-field">*</span></label>
						</div>
						<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
							<input type="number" class="form-control" id="observed_variant_id" name="observed_variant_id" value="<?php 
							if(isset($pending_confirmations['observed_variant_id']))
							{
								echo $pending_confirmations['observed_variant_id'];
							}
						?>" readonly/>
						</div>
					</div>
					<div class="form-group row d-none">
						<div class="col-xs-12 col-sm-4 col-md-4">
							<label for="ngs_panel_id" class="form-control-label">ngs_panel_id: <span class="required-field">*</span></label>
						</div>
						<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
							<input type="number" class="form-control" id="ngs_panel_id" name="ngs_panel_id" value="<?php 
							if(isset($pending_confirmations['ngs_panel_id']))
							{
								echo $pending_confirmations['ngs_panel_id'];
							}
						?>" readonly/>
						</div>
					</div>
					<div class="form-group row">
						<div class="col-xs-12 col-sm-4 col-md-4">
							<label for="genes" class="form-control-label">Genes: </label>
						</div>
						<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
							<input type="text" class="form-control" id="genes" name="genes" value="<?php 
							if(isset($pending_confirmations['genes']))
							{
								echo $pending_confirmations['genes'];
							}
						?>" readonly/>
						</div>
					</div>
					<div class="form-group row">
						<div class="col-xs-12 col-sm-4 col-md-4">
							<label for="mutation_type" class="form-control-label">Mutation type: </label>
						</div>
						<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
							<input type="text" class="form-control" id="mutation_type" name="mutation_type" value="<?php 
							if(isset($pending_confirmations['mutation_type']))
							{
								echo $pending_confirmations['mutation_type'];
							}
						?>" readonly/>
						</div>
					</div>
					<div class="form-group row">
						<div class="col-xs-12 col-sm-4 col-md-4">
							<label for="confirmation_type" class="form-control-label">Confirmation Type:<span class="required-field">*</span> </label>
						</div>
						<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
							<select class="form-control" name="confirmation_type" id="confirmation_type" required>
								<option disabled selected value> -- select a type -- </option>
								<option value="Sanger">Sanger</option>
								<option value="Light Cycler">Light Cycler</option>
								<option value=""></option>
								<option value="Other">Other</option>
							</select>
						</div>
					</div>					
					<div class="form-group row">
						<div class="col-xs-12 col-sm-4 col-md-4">
							<label for="confirmation_status" class="form-control-label">Confirmation Status:<span class="required-field">*</span> </label>
						</div>
						<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
							<select class="form-control" name="confirmation_status" id="confirmation_status" required>
								<option disabled selected value> -- select a status -- </option>
								<option value="1">Confirmed</option>
								<option value="2">Failed Confirmation</option>
							</select>
						</div>
					</div>
					<!-- <div class="form-group row required-input">
						<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
							<label for="validation_file" class="form-control-label" >Add all Files pertaining to the confirmation: <span class="mulitple-field">*</span></label><br><span>(file types accepted .jpg, .jpeg, .png, .pdf, .doc, .docx, .xls, .xlsx)</span>
						</div>
						<div class="col-xs-12 col-sm-8 col-md-8 col-lg-8 pull-left">
							<input id="validation-files-id" type="file" multiple name="validation_files[]" accept=".jpg,.jpeg,.png,.pdf,.doc,.docx, .xlsx, .xls, application/msword, application/vnd.openxmlformats-officedocument.wordprocessingml.document,application/pdf, image/png,image/jpeg, application/vnd.ms-excel"/>
						</div>
					</div> -->
					<div class="form-group row">
						<div class="col-xs-12 col-sm-4 col-md-4">
							<label for="comments" class="form-control-label">Comments:</label>
						</div>
						<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
							<textarea class="form-control" name="comments" rows="4" maxlength="65535" style="margin-bottom: 10px;"> </textarea>
						</div>
					</div>
					
					<div class="offset-xs-4 offset-sm-4 offset-md-4 offset-lg-4 col-xs-2 col-sm-2 col-md-2 col-lg-2">
						<button type="submit" name="submit" value="Submit"  class="btn btn-primary btn-primary-hover">
							Submit
						</button>
					</div>
				</fieldset>
			</form>
		</div>
	</div>
</div>