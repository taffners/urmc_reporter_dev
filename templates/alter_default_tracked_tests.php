<div class="container-fluid">
	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 alert alert-info">
			Use this page to update the tests that will automatically tracked for turnaround time.  This page will automatically save when you click the check box.
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
			<a class="btn btn-primary btn-primary-hover" href="?page=performance_audit_worksheet&filter=months&month=<?=CURR_MONTH;?>&year=<?=CURR_YEAR;?>" title="Use this button to edit turn around times" style="padding-top:5px;padding-bottom:5px;">
               Turn around time worksheet
          	</a>
		</div>
		<div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
			<a class="btn btn-primary btn-primary-hover" href="?page=performance_audit&filter=months&month=<?=CURR_MONTH;?>&year=<?=CURR_YEAR;?>&tracked_tests=1"  style="padding-top:5px;padding-bottom:5px;">
               <img src="images/performance.png" style="margin-left:5px;height:45px;width:45px;">
          	</a>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
			<label for="tracked_tests" class="form-control-label">Turn around time checked tests: <span class="mulitple-field">*</span></label>
		</div>
		<div id="track-tests-ckbx-div" class="col-xs-12 col-sm-8 col-md-8">
<?php
		$tests_default_tracked = array();
		foreach ($tests as $key => $test)
		{

?>
			<div class="form-check">
				<input class="form-check-input tracked_tests-ckbx" type="checkbox" value="<?= $test['orderable_tests_id'];?>" name="tracked_tests[]" required="required" data-orderable_tests_id="<?= $test['orderable_tests_id'];?>" data-test_name="test_<?= str_replace(' ', '_', $test['test_name']);?>"
				<?php
				if ($test['turnaround_time_tracked'] === 'yes')
				{
					echo 'checked';
					array_push($tests_default_tracked, $test['test_name']);
				}
				?>>
				<label><?= $test['test_name'];?></label>
			</div>
<?php
		}								
?>						
		</div>
	</div>
</div>