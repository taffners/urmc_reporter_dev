
<div class="container-fluid">

	<div class="alert alert-success" role="alert" style="font-size:18px;">
		Please review the information which was just uploaded.  <strong>This is not the final report.</strong>  At this step just make sure the expected data was uploaded correctly.  
		<br><br>
		After reviewing the data click confirmed at the bottom of the page if everything looks correct or if something is incorrect push something is wrong and notify Samantha
	</div>
	<fieldset id="pre-ngs-samples-fieldset">
	     <legend class="legend-gr">Confirm Information Uploaded</legend>

		     <?php

				if (isset($patientArray)  && isset($visitArray))
				{
					require_once('templates/tables/patient_visit_report_table.php');
					require_once('templates/shared_layouts/panel_info_report.php');	
					require_once('templates/shared_layouts/sample_information.php');
				}

		     	if (isset($_GET['num_variants']) && isset($_GET['ngs_file_name']))
		     	{
		     		echo '<u>Number of Variants Found in '.$_GET['ngs_file_name'].' is </u>: '.$_GET['num_variants'];
		     	}
		     ?>
		     <?php
		     	require_once('templates/shared_layouts/verify_variant_table.php');
		     ?>
		<div class="row" style="margin-top:20px;">
			<div class="offset-md-2 offset-lg-2 col-xs-12 col-sm-12 col-md-4 col-lg-4">
				<a href="?page=add_flt3&amp;run_id=<?= $_GET['run_id'];?>&amp;visit_id=<?= $_GET['visit_id'];?>&amp;patient_id=<?= $_GET['patient_id'];?>&amp;num_variants=<?= isset($_GET['num_variants']) ? $_GET['num_variants'] : '';?>&amp;ngs_file_name=<?= isset($_GET['ngs_file_name']) ? $_GET['ngs_file_name'] : '' ;?>&amp;previous_page=check_tsv_upload" class="btn  btn-success btn-success-hover" role="button">
					Add FLT3 Internal Tandem Duplication (ITD)
				</a>
			</div>

			<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
				<a href="?page=add_variant&amp;run_id=<?= $_GET['run_id'];?>&amp;visit_id=<?= $_GET['visit_id'];?>&amp;patient_id=<?= $_GET['patient_id'];?>&amp;num_variants=<?= isset($_GET['num_variants']) ? $_GET['num_variants'] : '';?>&amp;ngs_file_name=<?= isset($_GET['ngs_file_name']) ? $_GET['ngs_file_name'] : '' ;?>&amp;previous_page=check_tsv_upload" class="btn  btn-success btn-success-hover" role="button">
					Add a Variant not listed
				</a>
			</div>
		</div>

		<?php 
			// Low Coverage 
			require_once('templates/tables/low_cov_tables.php');

			// Wet Bench tech info
			if (isset($wet_bench_tech) && !empty($wet_bench_tech))
			{
				require_once('templates/shared_layouts/wet_bench_techs.php');
			}
		?>

	</fieldset>
	<div class="row">

		<!-- update area -->
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			<form class="form" method="post" class="form-horizontal">
  				<div class="row" style="margin-bottom:30px;">
  					
					<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
						<button type="submit" id="<?= $page;?>_confirm" name="<?= $page;?>_confirm" value="Submit"  class="btn btn-success btn-success-hover">
							Confirm
							
						</button>
					</div>
					<div class="offset-xs-8 offset-sm-8 offset-md-8 offset-lg-8 col-xs-2 col-sm-2 col-md-2 col-lg-2">
						<button type="submit" id="<?= $page;?>_incorrect" name="<?= $page;?>_incorrect" value="Submit"  class="btn btn-danger btn-danger-hover">
							Something is Wrong
							
						</button>
					</div>						
					<!-- add the common hidden fields for every form -->
					<?php
						require_once('templates/shared_layouts/form_hidden_fields.php')
					?>
				</div>

			</form>
		</div>
	</div>
</div>