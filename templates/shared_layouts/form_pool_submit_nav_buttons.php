
<div class="row d-print-none">
	<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
<?php

///////////////////////////////////////////////////////////////////////////////////////// 
// Set download button for downloading excel workbook
///////////////////////////////////////////////////////////////////////////////////////// 
if (isset($_GET['page']) && $_GET['page'] === 'pool_download_excel' && isset($name_file) && $step_status === 'not_complete')
	{
?>
		<div class="row" id="new_excel_num">
			<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
				<button type="submit" id="<?= $page;?>_new_submit" name="<?= $page;?>_submit" class="btn btn-primary btn-primary-hover">
					Download <?= $name_file;?>					
				</button>
			</div>

			<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
				<button type="button" id="<?= $page;?>_refresh" name="<?= $page;?>_refresh" class="btn btn-primary btn-primary-hover d-none">
					Submit
				</button>
			</div>
		</div>
		
		<div class="row" id="old_excel_num">
			<button type="submit" id="<?= $page;?>_submit" name="<?= $page;?>_submit" class="btn btn-primary btn-primary-hover d-none">
				Submit Excel Number					
			</button>
		</div>
		
<?php
	}
//&& $terminal_step_reached === 'not_complete'
		
else if (
			(
				isset($terminalStepBtnStatus) &&
				$terminalStepBtnStatus === 'show'
			)

			&&
			(
				isset($lockPageStatus) &&
				$lockPageStatus === 'show'
			)
		)
{
?>		
		<button type="submit" id="<?= $page;?>_submit" name="<?= $page;?>_submit" value="Submit"  class="btn btn-primary btn-primary-hover">
			Submit
		</button>
<?php
}

else if (
			isset($lockPageStatus) &&
			$lockPageStatus != 'show'
		)
{
?>
		<div class="alert alert-info">
			<?= $lockPageStatus;?>   
		</div>
<?php
}
else
{
?>
		<div class="alert alert-info">
			You have reached point of no return.   
		</div>
<?php
}
?>		
	</div>

	<!-- add previous step button if this isn't the first step-->
	<div class="offset-md-2 offset-lg-2 col-xs-2 col-sm-2 col-md-2 col-lg-2">
<?php	

	// if this isn't the first step add previous button
	if ($previous_step != 'first step')
	{
	?>
		<a href="?page=<?= $previous_step;?>&pool_chip_linker_id=<?= $_GET['pool_chip_linker_id'];?>&ngs_panel_id=<?= $_GET['ngs_panel_id'];?>" class="btn 
		btn-primary btn-primary-hover" role="button">
			❮ Previous
		</a>
<?php
	}
?>
	</div>
	<!-- Add next button -->
<?php

	if ($step_status ==='completed')
	{

?>
     <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
		<a href="?page=<?= $next_step;?>&pool_chip_linker_id=<?= $_GET['pool_chip_linker_id'];?>&ngs_panel_id=<?= $_GET['ngs_panel_id'];?>" class="btn 
		btn-primary btn-primary-hover" role="button" 
		<?php 
			if ($page == 'review_report')
			{
				echo 'target="_blank"';
			}
		?>>
			Next ❯
		</a>
	</div>
<?php
	}
?>
</div>