<table class="formated_table dataTable">
	<thead>
		<tr>
			<th>Metric</th>
			<th>Range</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td>Reads total</td>
			<td>10-22 million</td>
		</tr>
		<tr>
			<td>ISP loading</td>
			<td>> 60 %</td>
		</tr>
		<tr>
			<td>Median Read Length</td>
			<td>100 - 130 bp</td>
		</tr>
		<tr>
			<td>Reads usable</td>
			<td>> 30%</td>
		</tr>
		<tr>
			<td>Enrichment</td>
			<td>> 98%</td>
		</tr>									
		<tr>
			<td>Clonal</td>
			<td>> 50%</td>
		</tr>
		<tr>
			<td>Polyclonal</td>
			<td>30-45% or less if Quality < 10%</td>
		</tr>
		<tr>
			<td>Library Final</td>
			<td>> 40%</td>
		</tr>
		<tr>
			<td>Test Fragment</td>
			<td>&le; 3%</td>
		</tr>									
		<tr>
			<td>Adapter Dimer</td>
			<td>< 10%</td>
		</tr>							
		<tr>
			<td>Low Quality</td>
			<td>< 30%</td>
		</tr>									
		<tr>
			<td>Accuracy</td>
			<td>&ge; 99%</td>
		</tr>					
	</tbody>
</table>	