<div class="row d-print-none">

	<?php
		//submit button is different for verify_variants page.  This button should be called Done.  It should not be disabled but also should only be present if $step status === 'completed' 

		$previous_step = $stepTracker->FindPreviousStep($page);
		$step_status = $stepTracker->StepStatus($completed_steps, $page);
		$done_page = False;

		// there are some pages which just submit that the step is done.  "done page"  This page only has a submit button when the page hasn't been completed before.  Otherwise there are just prev and next buttons
		if 	(
				isset($_GET['page']) && 
				( 
					$_GET['page'] === 'verify_variants' || 
					$_GET['page'] === 'add_tiers'
				)
			) 
		{
			$done_page = True;
		}

		if (isset($_GET['page']) && $step_status !='completed' && $done_page)
		{
	?>
			<div class="offset-xs-4 offset-sm-4 offset-md-4 offset-lg-4 col-xs-2 col-sm-2 col-md-2 col-lg-2">
				<button type="submit" id="<?= $page;?>_submit" name="<?= $page;?>_submit" value="Submit"  class="btn btn-primary btn-primary-hover">
					Done
				</button>
			</div>
	<?php
		}
		else if ($_GET['page'] === 'check_required_confirm' && !$curr_step_complete)
		{
	?>
			<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
				<button type="submit" id="<?= $page;?>_submit" name="<?= $page;?>_submit" value="Submit"  class="btn btn-primary btn-primary-hover">
					I attest that all pending variants will be confirmed
				</button>
			</div>
	<?php
		}
		else if (isset($_GET['page']) && $_GET['page'] == 'review_report')
		{
	?>
			<div class="offset-xs-2 offset-sm-2 offset-md-2 offset-lg-2 col-xs-2 col-sm-2 col-md-2 col-lg-2" >
				<a href="?page=generate_report&<?= EXTRA_GETS;?>" target="_blank" class="btn btn-primary btn-primary-hover" role="button" style="margin-left:20px;">
					Show PDF
				</a>
			</div>
<?php

			// if the current user is a tech and they already approved report
			// change button to a message that it is done
			$tech_approved_found = False;
			$approval_time_stamp = '';
			// add all tech approved
			if (isset($completed_steps))
			{					
				foreach ($completed_steps as $key=>$step)
				{					
					if 	(
							$step['step'] === 'tech_approved' &&
							$step['user_id'] === USER_ID
						)
					{					
						$tech_approved_found = True;
						$approval_time_stamp = $step['time_stamp'];
					}						
				}
			}


			if 	(
					$tech_approved_found && 
					strpos($user_permssions, 'director') === false && 
					(
						strpos($user_permssions, 'tech') !== false ||
						strpos($user_permssions, 'reviewer') !== false
					)
				)
			{
?>
			<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 alert alert-info">
				You approved this on <?= $approval_time_stamp; ?>
			</div>	
<?php				
			}	

			// A director and a tech can be a reviewer.  That is why tech review is 
			// locked to tech permissions.  The review permissions puts user name on 
			// message board list
			else if 	( 
							(	strpos($user_permssions, 'director') !== false && 
								$num_reviews >= 1 && 
								isset($qc_run_step_status) && 
								$qc_run_step_status == 'completed' && 
								$runInfoArray[0]['status'] !== 'revising' && 
								isset($qc_variant_step_status)  && 
								$qc_variant_step_status == 'completed'
							) 
							|| 
							(
								strpos($user_permssions, 'tech') !== false && 
								$runInfoArray[0]['status'] !== 'revising'
							) 
							||
							
							(
								strpos($user_permssions, 'revise') !== false && 
								$runInfoArray[0]['status'] === 'revising'
							)
							||
							(
								isset($orderableTestArray[0]['hotspot_variants']) && 
								!empty($orderableTestArray[0]['hotspot_variants']) &&
								$orderableTestArray[0]['hotspot_variants'] === 'yes'
							)
						)
			{

?>
			<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
				<button type="submit" id="<?= $page;?>_submit" name="<?= $page;?>_submit" value="Submit"  class="btn btn-primary btn-primary-hover">
					Looks Good Submit					
				</button>
			</div>
<?php
			}
			else if ($runInfoArray[0]['status'] !== 'revising')
			{
?>
			<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 alert alert-danger">
				<strong>Waiting for:</strong><br>
				Reviews: <?= $num_reviews;?>/1 
<?php
				if ($qc_run_step_status == 'not_complete')
				{
?>
					<br>QC run: pending
<?php
				}
				else
				{
?>
					<br>QC run: passed
<?php
				}

				if ($qc_variant_step_status == 'not_complete')
				{
?>
					<br>QC Variant: pending
<?php
				}
				else
				{
?>
					<br>QC Variant: completed
<?php
				}
?>
			</div>
<?php
			}
		}

		

		elseif (isset($_GET['page']) && $_GET['page'] !== 'verify_variants' && $_GET['page'] !== 'add_patient' && !$done_page && $_GET['page'] !== 'review_report')
		{
	?>
			<div class="offset-xs-2 offset-sm-2 offset-md-2 offset-lg-2 col-xs-2 col-sm-2 col-md-2 col-lg-2">
				<button type="submit" id="<?= $page;?>_submit" name="<?= $page;?>_submit" value="Submit"  class="btn btn-primary btn-primary-hover <?php
				if ($_GET['page'] !== 'review_report')
				{
					echo 'disabled-link';
				}
				?>">
					Submit					
				</button>
			</div>
	<?php
		}
		else
		{
	?>
			<div class="offset-xs-4 offset-sm-4 offset-md-4 offset-lg-4 col-xs-2 col-sm-2 col-md-2 col-lg-2"></div>
	<?php
		}
	?>
	
	<!-- add previous step button if this isn't the first step-->
	<div class="offset-xs-2 offset-sm-2 offset-md-2 offset-lg-2 col-xs-2 col-sm-2 col-md-2 col-lg-2">
	
	<?php		
		// if this isn't the first step add previous button
		if ($previous_step != 'first step')
		{
	?>
			<a href="?page=<?= $previous_step;?>&<?= EXTRA_GETS;?>" class="btn 
			btn-primary btn-primary-hover" role="button">
				❮ Previous
			</a>
	<?php
		}
	?>
	</div>
	<!-- Add next button -->
	<?php
		$step_status = $stepTracker->StepStatus($completed_steps, $page);
		$next_step = $stepTracker->FindNextStep($page);

		if ($step_status ==='completed')
		{

	?>
     <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
		<a href="?page=<?= $next_step;?>&<?= EXTRA_GETS;?>" class="btn 
		btn-primary btn-primary-hover" role="button" 
		<?php 
			if ($page == 'review_report')
			{
				echo 'target="_blank"';
			}
		?>>
			Next ❯
		</a>
	</div>
		<?php
			}
			if ($step_status !=='completed' && ($page === 'add_summary' || $page === 'add_gene_interpetation' || $page === 'add_variant_interpetation'))
			{
		?>
     <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
		<button type="submit" name="<?= $page;?>_skip_submit" value="Submit"  class="btn btn-primary btn-primary-hover">
			Skip ❯
		</button>
	</div>
		<?php 
			}
		?>
</div>