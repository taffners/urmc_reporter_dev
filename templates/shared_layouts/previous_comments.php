<?php
	if (isset($previousComments))
	{
?>
			<div class="form-group row">
				<div class="col-xs-12 col-sm-4 col-md-4">
					<label for="previous_comments" class="form-control-label">Previous Comments:</label>
				</div>

				<div class="col-xs-12 col-sm-8 col-md-8 pull-left">

					<ul>
	<?php
				foreach($previousComments as $key => $previous_comments)
				{
	?>
						<li><?= $previous_comments['comment'];?> (<?= $previous_comments['user_name'];?> <?= $previous_comments['time_stamp'];?>)</li>
	<?php
				}
	?>
					</ul>
				</div>
			</div>
<?php
	}
?>