<!-- report progress bar toggles -->	
<div class="container-fluid d-print-none" style="height:38px;">
	<div class="row">
		<ul class="list-unstyled">	
					
			<li role="presentation" class="vertical-nav-buttons nav-buttons ">
				<button id="toggle-report-progress-bar-btn-minus" type="button" class="btn btn-primary btn-primary-hover toggle-report-progress-bar-btn" aria-label="minimize the report builder progress buttons">
					<span class="fa fa-minus"></span>
				</button>
			</li>
			<li role="presentation" class="vertical-nav-buttons nav-buttons">
				<button id="toggle-report-progress-bar-btn-plus"  type="button" class="btn btn-primary btn-primary-hover toggle-report-progress-bar-btn d-none" aria-label="show the report builder progress buttons">
					<span class="fa fa-plus"></span>
				</button>
			</li>
		</ul>
	</div>
</div>