<?php
     ////////////////////////////////////////////////////
     // Add a column to either add variants which are not in 
     // knowledge_base_table or scroll to variant
     ////////////////////////////////////////////////////
     if ($page !== 'check_tsv_upload' && $variantArray[$i]['knowledge_id'] == null && $page !== 'show_edit_report')
     {
?>
          <td>
               <a href="?page=add_SNV_to_knowledge_base&genes=<?= $variantArray[$i]['genes'];?>&coding=<?= str_replace('+', '%2B',$variantArray[$i]['coding']);?>&amino_acid_change=<?= $variantArray[$i]['amino_acid_change'];?>&<?= EXTRA_GETS;?>&variant_filter=<?= $_GET['variant_filter']; ?>" class="btn btn-danger btn-danger-hover" role="button">
                    <span class="fas fa-plus" aria-hidden="true"> SNV</span>  
               </a>
          </td>
<?php
     }
     else if ($page !== 'check_tsv_upload' && $variantArray[$i]['knowledge_id'] != null && $page !== 'show_edit_report')
     {
?>
          <td>
               <button type="button" class="btn btn-primary btn-primary-hover scroll_to_variant" data-scroll-knowledge-id="knowledge_id_<?= $variantArray[$i]['knowledge_id']; ?>">
                    <span class="far fa-hand-point-down" aria-hidden="true"> SNV</span>  
               </button>
          </td>
<?php
     }