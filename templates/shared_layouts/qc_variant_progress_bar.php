
<!-- report progress bar toggles -->

<?php
	require_once('templates/shared_layouts/report_toggle_show_status.php');
?>

<!-- Progress bar -->
<div id="report-nav" class="hidden-xs col-sm-2 col-md-2 col-lg-2">
	<div class="vertical-nav">
		<ul class="affix list-unstyled">
			<li role="presentation" class="vertical-nav-buttons nav-buttons">
				<a href="?page=show_edit_report&<?= EXTRA_GETS;?>" class="btn btn-primary btn-primary-hover" role="button">
					Update Report
				</a>
			</li>

<?php
		foreach ($qcVarSteps as $key =>$value)
		{
			$qcVarStepTracker->ButtonStatus($completed_steps, $value);
?>

			<li role="presentation" class="vertical-nav-buttons nav-buttons">

				<a href="?page=<?= $value;?>&<?= EXTRA_GETS;?>" class="btn <?php 

						echo $qcVarStepTracker->ButtonStatus($completed_steps, $value);	
				?>" role="button">
				<?= $utils->UnderscoreCaseToHumanReadable($value);?>
				</a>
			</li>
<?php
		}
?>
		</ul>
	</div>
</div>