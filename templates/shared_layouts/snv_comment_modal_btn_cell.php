<?php
     if ($page !== 'check_tsv_upload' && $page !== 'show_edit_report')
     {
?>
     <!-- add comments modal -->
     <td>
     	<button type="button" <?php 
     		// add an id if comments there are comments 
     		if (isset($variantArray[$i]['comment_ids'])) 
     		{
     			echo 'id="comment_'.$variantArray[$i]['comment_ids'].'"'; 
     		}
     	?><?php
     		// change the color of the button depending on if 
     		if (isset($variantArray[$i]['comment_ids'])) 
     		{
     	?>
     			class="btn btn-success variant_comment_modal_btn btn-success-hover"
     			data-comment-id="<?= $variantArray[$i]['comment_ids']; ?>"
     	
     	<?php
     		}
     		else
     		{
     	?>
     			class="btn btn-primary variant_comment_modal_btn btn-primary-hover"
     			data-comment-id="none"
     	<?php
     		}

     		// add data elements gene, genotype, amino acid change as variant name
     		if (isset($variantArray[$i]['genes']) && isset($variantArray[$i]['coding']) && isset($variantArray[$i]['amino_acid_change']) && isset($variantArray[$i]['observed_variant_id']))
     		{
     	?>

     			data-variant-name="<?= $variantArray[$i]['genes'];?>_<?= $variantArray[$i]['coding'];?>_<?= $variantArray[$i]['amino_acid_change'];?>"
     			data-observed-variant-id="<?= $variantArray[$i]['observed_variant_id'];?>"
     	<?php
     		}
     	?> >
     		<span class="fas fa-comment"></span>
     	</button>
     </td>
<?php
     }
?>