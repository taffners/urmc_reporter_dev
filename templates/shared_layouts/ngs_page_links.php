<?php
	if (isset($user_permssions) && strpos($user_permssions, 'NGS') !== false && isset($ngs_panels))
	{
?>

	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		
		<hr class="plain-section-mark">
	
		<label for="extra-info-radio-btn" class="form-control-label">NGS Panel: </label>
	
		
			
		<label class="radio-inline" title="These radio buttons are used as a filter in for the links to the NGS home page to the right.">
			<input type="radio" name="extra-info-radio-btn" value="None" checked >
			All
		</label>
	<?php
		foreach($ngs_panels as $key => $panel)
		{
	?>								
		<label class="radio-inline" title="These radio buttons are used as a filter in for the links to the NGS home page to the right.">
			<input type="radio" name="extra-info-radio-btn" value="<?= $panel['ngs_panel_id'];?>" >
			<?= $panel['test_name'];?>
		</label>
	<?php
		}
	?>	
			
		<button class="btn btn-primary btn-primary-hover page-link-btn" data-scroll-id="pre-ngs-samples-list-div" data-extra-info-radio-btn-name="extra-info-radio-btn" data-link-page="home" data-extra-field-name="ngs_panel_id" title="Once this button is clicked you will be redirected to the NGS home page pre table and the results will be filterd by the NGS panel selected in the radio buttons to the left.">
        Pre</button>
        <button class="btn btn-primary btn-primary-hover page-link-btn" data-scroll-id="pre-ngs-library-pool-row" data-extra-info-radio-btn-name="extra-info-radio-btn" data-link-page="home" data-extra-field-name="ngs_panel_id" title="Once this button is clicked you will be redirected to the NGS home page pools table and the results will be filterd by the NGS panel selected in the radio buttons to the left.">
        Pools</button>
        <button class="btn btn-primary btn-primary-hover page-link-btn" data-scroll-id="outstanding-reports-fieldset" data-extra-info-radio-btn-name="extra-info-radio-btn" data-link-page="home" data-extra-field-name="ngs_panel_id" title="Once this button is clicked you will be redirected to the NGS home page samples with outstanding reports table and the results will be filterd by the NGS panel selected in the radio buttons to the left.">
        Outstanding</button>
        <?php
        if ($page !== 'new_home')
        {
			if (isset($user_permssions) && strpos($user_permssions, 'revise') !== false)
			{
		?>
		<a href="?page=completed_reports" class="btn btn-primary btn-primary-hover">Completed or Revise</a>
		<?php
			}
			else
			{
		?>
		<a href="?page=completed_reports" class="btn btn-primary btn-primary-hover">Completed</a>
		<?php
			}
		}
		?>
        <hr class="plain-section-mark">
	</div>
<?php
	}
?>