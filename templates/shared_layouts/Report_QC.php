<div class="row">

	<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
		Time Stamp: <b><?= $chip_qc['header']['time_stamp'];?></b>
	</div>
	<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
		Experiment ID: <b><?= $chip_qc['header']['experiment_id'];?></b>
	</div>
</div>

<div class="row">

	<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
		Run id: <b><?= $chip_qc['header']['run_id'];?></b>
	</div>
	<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
		Chip Barcode: <b><?= $chip_qc['header']['chip_barcode'];?></b>
	</div>
</div>

<table class="formated_table">
	<thead>
		<th>Metric</th>
		<th>Range</th>
		<th>Observed</th>
	</thead>
	<tbody>

<?php
	foreach ($chip_qc['cutoffs'] as $metric => $info)
	{
?>
		<tr>
			<td 
				<?php
				// add title if present in html_infos
			 	if (isset($html_infos[$metric]['title']))
			 	{
				?>
					title="<?= $html_infos[$metric]['title'];?>" 
				<?php
			 	}
				?>>
				<?= $utils->UnderscoreCaseToHumanReadable($metric);?>
				<?php
				// add a link to more information about the metric
				if (isset($html_infos[$metric]['link']))
				{
				?>
					<a href="<?= $html_infos[$metric]['link'];?>" target="_blank"><span class="fas fa-info-circle"></span></a>
				<?php
				}
				?>
			</td>
			<td>
				<?= $info['range'];?>
			</td>
			<td class="<?php 
				// add class 
				if ($info['status'] == 'pass')
				{
					echo 'green-background';
				}
				else
				{
					$passed = false;
					echo 'red-background';
				}?>">
				<?php
					// Add commas to numbers if necessary
					if ($info['val'] >= 1000)
					{
						echo number_format($info['val']).' '.$info['unit'];
					}
					else
					{
						echo $info['val'].' '.$info['unit'];
					}
				?>
						
			</td>
		</tr>
<?php
	}
?>		
	</tbody>
</table>

<div class="form-group row">
	<div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
		<label for="run_qc_status_<?= $chip;?>" class="form-control-label">Run QC Status chip <?= $chip;?>: <span class="required-field">*</span></label>
	</div>
	<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
		<div class="radio">
			<label class="radio-inline"><input type="radio" name="run_qc_status_<?= $chip;?>" required="required" value="pass" <?php
			if($passed)
			{
				echo 'checked'; 								
			}?>>Pass</label>
		
			<label class="radio-inline"><input type="radio" name="run_qc_status_<?= $chip;?>" required="required" value="failed" <?php
			if(!$passed)
			{
				echo 'checked'; 								
			}?>>Failed</label>
		</div>
	</div>
</div>


