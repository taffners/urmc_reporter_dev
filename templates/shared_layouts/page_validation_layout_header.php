	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			<span class="audit-headers">About this document</span>
			<hr class="plain-section-mark">
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			The purpose of this document is to provide the most up to date validations for each page in <?= SITE_TITLE;?>.  Every page created or updated after October 1st, 2020 will go through the same documented validation process which will be recorded in this document. This document will evolve with each newly released version of <?= SITE_TITLE;?>.  
			<br><br>
			A complementary report to this page validation report is the module report. 
		</div>
	</div>

	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			<hr class="plain-section-mark">
			<span class="audit-headers">Current Info</span>
			<hr class="plain-section-mark">
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3" style="font-weight: bold;">
			Website: 
		</div>
		<div class="col-xs-12 col-sm-12 col-md-9 col-lg-9" >
			<?= SITE_TITLE?>
		</div>		
	</div>
	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3" style="font-weight: bold;">
			Website Version: 
		</div>
		<div class="col-xs-12 col-sm-12 col-md-9 col-lg-9" >
			<?= VERSION?>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3" style="font-weight: bold;">
			Report Date: 
		</div>
		<div class="col-xs-12 col-sm-12 col-md-9 col-lg-9" >
			<?= date('Y-m-d')?>
		</div>
	</div>
	