<?php
	if ($stepTracker->StepStatus($completed_steps, 'verify_variants') === 'completed' || $stepTracker->StepStatus($completed_steps, 'add_summary') === 'completed')
	{	
?>
	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
<?php 
	// only include the INTERPRETATION section if $full_interpt does not start with 
	// VARIANTS OF UNKNOWN SIGNIFICANCE INTERPRETATION.  This means there are not any 
	// Tier 1 and Tier 2 variants
		$unknown_section_title = 'VARIANTS OF UNKNOWN SIGNIFICANCE INTERPRETATION';
		$pos_unknown_sig = strpos($full_interpt, $unknown_section_title);

		if ($pos_unknown_sig !== 0)
		{
?>
			<span class="report-section-bold">
				INTERPRETATION: 			
			</span>
<?php			
		}

		// update full interpt to so a new section is added 
		$span_unknown_section_title = '<span class="report-section-bold">'.$unknown_section_title.':</span>';
		$full_interpt = str_replace($unknown_section_title, $span_unknown_section_title, $full_interpt);
?>
			<div>
				<?= $utils->AddPMIDLinkUrls($full_interpt);?>				
			</div>
		</div>
	</div>
<?php
	}
?>