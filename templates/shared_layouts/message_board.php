<div class="alert alert-warning" id="message_board">
	<h5 class="FormHeaderCustom">
		Message Board for Report # <?=$_GET['visit_id'];?>		
	</h5>

<?php
	
		if ( isset($all_directors) && isset($all_reviewers) )
		{
?>
	<form class="form" method="post" class="form-horizontal" style="margin-top:10px;">
		<!-- add previous messages 
			the users messages should be right justified and everyone else's should be left justified
		-->
<?php

if(isset($all_messages) && !empty($all_messages))
{
	foreach($all_messages as $key => $message)
	{	
		
		// find if message is from the current user.  If it is right justify
		if ($message['user_id'] === USER_ID && !empty($message['comment']))
		{
?>
		<!-- <div class="row"> -->

		
		<div class="msg-list sender">
			<div class="messenger-container">
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
						<p><strong><?= $message['user_name'];?></strong></p>
					</div>
					<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 text-right">
						<?= $message['comment_time_stamp'];?>
					</div>
				</div>
				<p style="font-size:15px;"><?= nl2br($message['comment']);?></p>
			</div>
		</div>
		<div class="clear"></div>
<?php
		}
		else if (!empty($message['comment']))
		{
?>
		<div class="msg-list">
			<div class="messenger-container">
				
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
						<p><strong><?= $message['user_name'];?></strong></p>
					</div>
					<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 text-right">
						<?= $message['comment_time_stamp'];?>
					</div>
				</div>
		   		<p style="font-size:15px;"><?= nl2br($message['comment']);?></p>
			</div>
		</div>
		<div class="clear"></div>
<?php
		}

	}
}
?>
		<div class="form-group row">
			<div class="col-xs-12 col-sm-4 col-md-4">
				<label for="message_board_memebers" class="form-control-label" >Select who to notify of message:<span class="required-field">*</span> <br>(Click skip email if you would just like to post the message)</label>
			</div>
			<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
<?php
			// find all possible reviewers and directors to include
			$merge_members = array_merge($all_reviewers, $all_directors);

			// remove duplicates from this list
			$possible_message_board_members = array_map("unserialize", array_unique(array_map("serialize", $merge_members)));

			foreach($possible_message_board_members as $key => $reviewer)
			{						
?>
				<div class="checkbox">
					<label>
						<input type="checkbox" 
						name="message_board_memebers[<?= $key;?>]" 
						value="<?= $reviewer['user_id'];?>" >
							
							<?= $reviewer['first_name'];?> <?= $reviewer['last_name'];?> (<?= $reviewer['email_address'];?>)
					</label>
				</div>
<?php
			}
		
?>
				<div class="checkbox">
					<label>
						<input type="checkbox" 
						name="message_board_memebers[skip]" 
						value="skip" >
							
							Skip Sending Email
					</label>
				</div>

			</div>
		</div>		
		<!-- add text box for new message -->
		<div class="form-group">
			<textarea class="form-control" name="message" rows="<?php
			if (isset($text_message_form) && $text_message_form !== '')
			{
				echo 40;
			}
			else
			{
				echo 5;
			}
			?>"  maxlength="65535"><?php
			if (isset($text_message_form) && $text_message_form !== '')
			{
				echo $text_message_form;
			}
			?></textarea>
		</div>

		<div class="row">
			<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
				<button type="submit" id="notify_reviewers_submit" name="message_board_submit" value="Submit"  class="btn btn-primary btn-primary-hover">
					<span class="fas fa-envelope"></span>
					Submit Message					
				</button>
			</div>
		</div>
	</form>	
<?php					
		}
	
?>
</div>