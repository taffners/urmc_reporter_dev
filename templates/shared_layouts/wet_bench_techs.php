<div class="row">
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		<span class="report-section-bold">
			WET BENCH TECH<?php 
				// add s if more than one tech
				if (sizeof($wet_bench_tech) > 1)
				{
					echo 'S';
				}

			?>:
		</span>

		
<?php
	$techs = '';
	foreach ($wet_bench_tech as $key => $wet_tech)
	{
		if ($techs != '')
		{
			$techs.= ',';
		}

		$techs .= $wet_tech['first_name'].' '.$wet_tech['last_name'].' '.$wet_tech['credentials'];

	}
?>
		<br>
		<?= $techs;?>
	</div>
</div>