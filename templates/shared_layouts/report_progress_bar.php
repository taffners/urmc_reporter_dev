<?php
	if (isset($completed_steps) && isset($_GET) && isset($_GET['run_id']) && isset($_GET['patient_id']) && isset($_GET['visit_id']) && isset($runInfoArray[0]['status']) && $runInfoArray[0]['status'] !== 'completed')
	{

?>
		<!-- report progress bar toggles -->	
	<?php
		require_once('templates/shared_layouts/report_toggle_show_status.php');
	?>

		<!-- Progress bar -->
		<div id="report-nav" class="hidden-xs col-sm-2 col-md-2 col-lg-2" >
			<div class="vertical-nav" >
				<ul id="toggle-last-step-not-visible" class="affix list-unstyled" style="overflow-y:scroll;max-height:75vh;overflow-x:hidden;">		
				
					<li role="presentation" class="vertical-nav-buttons nav-buttons">
						<a href="?page=show_edit_report&<?= EXTRA_GETS;?>" class="btn btn-primary btn-primary-hover <?= $stepTracker->activeButtonStatus('show_edit_report', 'primary');?>" role="button" title="Show a summary of the steps currently completed for this report.">
							Summary
						</a>
					</li>
					<li role="presentation" class="vertical-nav-buttons nav-buttons">
						<a href="?page=confirmations&<?= EXTRA_GETS;?>" class="btn btn-primary btn-primary-hover <?= $stepTracker->activeButtonStatus('confirmations', 'primary');?>" role="button" title="Request a confirmation and check which confirmations have already been requested.">
							Confirmations
						</a>
					</li>
	<?php
	if (isset($_GET['single_analyte_pool_id']) && !empty($_GET['single_analyte_pool_id']))
	{
	?>
					<li role="presentation" class="vertical-nav-buttons nav-buttons">
						<a href="?page=single_analyte_pool_home&single_analyte_pool_id=<?= $_GET['single_analyte_pool_id'];?>" class="btn btn-primary btn-primary-hover <?= $stepTracker->activeButtonStatus('message_board', 'primary');?>" role="button" title="Use the message board to send emails to other people in the lab working on NGS reports.">
							Single Analyte Pool
						</a>
					</li>
	<?php
	}
	?>					
					<li role="presentation" class="vertical-nav-buttons nav-buttons">
						<a href="?page=message_board&<?= EXTRA_GETS;?>#message_board" class="btn btn-primary btn-primary-hover <?= $stepTracker->activeButtonStatus('message_board', 'primary');?>" role="button" title="Use the message board to send emails to other people in the lab working on NGS reports.">
							Report Message Board
						</a>
					</li>	
<?php
	// add ordered_test_id and sample_log_book_id when there are values to add.  These do not exist for NGS controls
	$stop_reporting_extra_parms = '';
	if (isset($visitArray[0]['sample_log_book_id']) && !empty($visitArray[0]['sample_log_book_id']))
	{
		$stop_reporting_extra_parms.= '&sample_log_book_id='.$visitArray[0]['sample_log_book_id'];
	}

	if (isset($visitArray[0]['ordered_test_id']) && !empty($visitArray[0]['ordered_test_id']))
	{
		$stop_reporting_extra_parms.= '&ordered_test_id='.$visitArray[0]['ordered_test_id'];
	}

	

?>									
					<li class="vertical-nav-buttons nav-buttons">
						<a href="?page=stop_test_in_log_book&<?= EXTRA_GETS;?>&ngs_stage=ngs_report_building<?= $stop_reporting_extra_parms;?>" class="btn btn-danger btn-danger-hover <?= $stepTracker->activeButtonStatus('stop_reporting', 'danger');?>" role="button" title="Cancel this report.  It will be hidden from the samples with outstand reports section.">
							Stop Reporting 
							<span class="fas fa-times"></span>
						</a>
					</li>
<?php


	// Show QC run button only for positive control control_type_id = 1 AcroMetrix
	// FOR NOW TURN ON FOR SAMPLES AND CONTROLS

	if 	(
			(
				isset($visitArray[0]['control_type_id']) 	
			&&
				isset($_GET['ngs_panel_id'])
			&&
				(				
					$visitArray[0]['control_type_id'] === '0'
				)
			) 	||
			$visitArray[0]['control_type_id'] == null			
		)
	{
?>
					<li class="vertical-nav-buttons nav-buttons">
						<a href="?page=qc&<?= EXTRA_GETS;?>" class="btn <?php
							if(isset($qc_run_step_status) && $qc_run_step_status == 'not_complete')
							{
								echo 'btn-warning btn-warning-hover';
								echo $stepTracker->activeButtonStatus('qc', 'warning');
							}
							else
							{
								echo 'btn-success btn-success-hover';
								echo $stepTracker->activeButtonStatus('qc', 'success');
							}
						?> " role="button">
							QC Run
						</a>
					</li>
<?php
	}
?>
<?php
	// Show QC variant button for positive control or sample.
	if 	(
			(
				isset($visitArray[0]['control_type_id']) 	
			&&
				isset($_GET['ngs_panel_id'])
			&&
				(					
					$visitArray[0]['control_type_id'] === '0'
				)
				
			) ||
			$visitArray[0]['control_type_id'] == null
		)
	{
?>
					<li class="vertical-nav-buttons nav-buttons">
						<a href="?page=qc_strand_bias&<?= EXTRA_GETS;?>" class="btn <?php
							if(isset($qc_variant_step_status) && $qc_variant_step_status == 'not_complete')
							{
								echo 'btn-warning btn-warning-hover';
							}
							else
							{
								echo 'btn-success btn-success-hover';
							}
						?>" role="button">
							QC Variant
						</a>
					</li>
<?php
	}
?>
					<?php
					
						// add a link for all the current steps in StepTracker
						$steps = $stepTracker->GetSteps();
						$num_steps = sizeOf($steps);
						foreach ($steps as $key =>$value)
						{

							if ($value === 'verify_variants')
							{
					?>	
								<li id="<?= $num_steps === $key +1? 'last-progress-step':'progress_btn_'.$key;?>" role="presentation" class="vertical-nav-buttons nav-buttons">
									<a href="?page=<?= $value;?>&<?= EXTRA_GETS;?>&variant_filter=included" class="btn <?php echo $stepTracker->ButtonStatus($completed_steps, $value);?>" role="button">
									<?= $utils->UnderscoreCaseToHumanReadable($value);?>
									</a>
								</li>
					<?php
							}
							elseif ($value === 'generate_report')
							{
					?>
								<li id="<?= $num_steps === $key +1? 'last-progress-step':'progress_btn_'.$key;?>" role="presentation" class="vertical-nav-buttons nav-buttons">
									<a href="?page=<?= $value;?>&<?= EXTRA_GETS;?>" target="_blank" class="btn <?php echo $stepTracker->ButtonStatus($completed_steps, $value);?>" role="button">
									<?= $utils->UnderscoreCaseToHumanReadable($value);?>
									</a>
								</li>
					<?php
							}
							else
							{
					?>	
								<li id="<?= $num_steps === $key +1? 'last-progress-step':'progress_btn_'.$key;?>" role="presentation" class="vertical-nav-buttons nav-buttons<?php
								if (isset($num_snvs) && $num_snvs === 0 && ($value === 'add_tiers' || $value === 'add_variant_interpetation'))
								{
									echo ' d-none';
								}

								?>" >

									<a href="?page=<?= $value;?>&<?= EXTRA_GETS;?>" class="btn <?php 

											// if no variants were found previous step could be different so might need to activate the button ahead of time 
											if (isset($num_snvs))
											{
												echo $stepTracker->ButtonStatus($completed_steps, $value, $num_snvs);											
											}
											else
											{
												echo $stepTracker->ButtonStatus($completed_steps, $value);
											}


									?>" role="button">
									<?= $utils->UnderscoreCaseToHumanReadable($value);?>
									</a>
								</li>
					<?php
							}
					
						
						}

					?>

				</ul>
			</div>
		</div>
<?php
	}
?>