<?php
	if (isset($pageArray[0]) && isset($page_validation_info[0]['out_come']))
	{
		if ($page_validation_info[0]['out_come'] === 'Passed')
		{
			$out_come = '<span class="fas fa-check" style="color:#03AC13;"></span>';
		}
		else
		{
			$out_come = '<span class="fas fa-times" style="color:#ff0000;"></span>';
		}
?>
	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			<hr class="plain-section-mark">
			<span class="audit-headers">Page: <?= $pageArray[0]['page_name'].'  '.$out_come;?></span>
			<hr class="plain-section-mark">
		</div>
	</div>

	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3" style="font-weight: bold;">
			Page description: 
		</div>
		<div class="col-xs-12 col-sm-12 col-md-9 col-lg-9" >
			<?= $pageArray[0]['page_description'];?>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3" style="font-weight: bold;">
			How to access page: 
		</div>
		<div class="col-xs-12 col-sm-12 col-md-9 col-lg-9" >
			<?= $pageArray[0]['how_to_access_page'];?>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3" style="font-weight: bold;">
			Allowed Users: 
		</div>
		<div class="col-xs-12 col-sm-12 col-md-9 col-lg-9" >
			<?= $pageArray[0]['permissions'];?>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3" style="font-weight: bold;">
			Page Assumptions: 
		</div>
		<div class="col-xs-12 col-sm-12 col-md-9 col-lg-9" >
			<?= $pageArray[0]['page_assumption'];?>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3" style="font-weight: bold;">
			Page type: 
		</div>
		<div class="col-xs-12 col-sm-12 col-md-9 col-lg-9" >
			<?= $pageArray[0]['page_type'];?>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3" style="font-weight: bold;">
			Module Name: 
		</div>
		<div class="col-xs-12 col-sm-12 col-md-9 col-lg-9" >
			<?= $pageArray[0]['module_name'];?>
		</div>
	</div>
	
	
<?php
	}

	if (isset($page_validation_info[0]) && !empty($page_validation_info[0]))
	{
?>
	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			<hr class="plain-section-mark">
			<span class="audit-headers">Validation Info</span>
			<hr class="plain-section-mark">
		</div>
	</div>

	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3" style="font-weight: bold;">
			Validation Performed By: 
		</div>
		<div class="col-xs-12 col-sm-12 col-md-9 col-lg-9" >
			<?= $page_validation_info[0]['user_name'];?>
		</div>
	</div>

	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3" style="font-weight: bold;">
			Validation Performed Using: 
		</div>
		<div class="col-xs-12 col-sm-12 col-md-9 col-lg-9" >
			<?= $page_validation_info[0]['browser_version'];?>
		</div>
	</div>

	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3" style="font-weight: bold;">
			Validation Performed on Website Version: 
		</div>
		<div class="col-xs-12 col-sm-12 col-md-9 col-lg-9" >
			<?= $page_validation_info[0]['website_version'];?>
		</div>
	</div>

	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3" style="font-weight: bold;">
			Validation Performed: 
		</div>
		<div class="col-xs-12 col-sm-12 col-md-9 col-lg-9" >
			<?= $page_validation_info[0]['time_stamp'];?>
		</div>
	</div>

	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3" style="font-weight: bold;">
			Comments About this Validation: 
		</div>
		<div class="col-xs-12 col-sm-12 col-md-9 col-lg-9" >
			<?= $page_validation_info[0]['comment'];?>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3" style="font-weight: bold;">
			Validation Out Come: 
		</div>
		<div class="col-xs-12 col-sm-12 col-md-9 col-lg-9" >
			<?php
			if (isset($out_come))
			{

				echo $page_validation_info[0]['out_come'].' '.$out_come;
			
			}
			?>
		</div>
	</div>

<?php
	}
	if (isset($qc_checklist_items) && !empty($qc_checklist_items))
	{

?>
	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			<hr class="plain-section-mark">
			<span class="audit-headers">Validation Checklist and Response</span>
			<hr class="plain-section-mark">
		</div>
	</div>
<?php
		foreach ($qc_checklist_items as $key => $item)
		{
?>
	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3" style="font-weight: bold;">
			<?= $item['step_name']?>: 
		</div>
		<div class="col-xs-12 col-sm-12 col-md-9 col-lg-9" >
			<?= $item['actual_result']?>
		</div>
	</div>

<?php
		}
	}


?>
