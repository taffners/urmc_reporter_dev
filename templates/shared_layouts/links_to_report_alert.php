<div class="alert alert-success">
	<strong>How to add links to report</strong> 
	<ul>
		<li>Web addresses
			<ul>
				<li>Make sure they include http://, https://, or www.</li>
				<li>Example: http://goo.gl/Kwt9e3</li>
			</ul>
		</li>
		<li>PMID
			<ul>
				<li>Make sure it includes PMID: followed by the number</li>
				<li>Example PMID:20008640</li>
			</ul>
		</li>
	</ul>
</div>