<div class="loader">
	<div class="container h-100">
		<div class="row" style="margin-top: 20px;">
			<div class="col-xs-12 col-sm-12 col-lg-12 col-md-12">
				<div class="loader-logo">
					<img src="images/urmc_logo.png">
				</div>
			</div>
		</div>
		<div class="row h-100 justify-content-center align-items-center">
			<div class="col-12">
				
				<div class="align-items-center">
				
					<div class="loader-inner">
						<div class="loader-line-wrap">
							<div class="loader-line"></div>
						</div>
						<div class="loader-line-wrap">
							<div class="loader-line"></div>
						</div>
						<div class="loader-line-wrap">
							<div class="loader-line"></div>
						</div>
						<div class="loader-line-wrap">
							<div class="loader-line"></div>
						</div>
						<div class="loader-line-wrap">
							<div class="loader-line"></div>
						</div>
						<div class="loader-line-wrap">
							<div class="loader-line"></div>
						</div>
						<div class="loader-line-wrap">
							<div class="loader-line"></div>
						</div>            
					</div>
					<div class='loadingText'>
<?php
	if (isset($page) && $page === 'pool_qc_backup_data')
	{
?>
						<p class="loading">Please Wait! <b>Do NOT close window!</b> Files are transferring.  This might take up to 10 minutes.</p>
<?php
	}
	else
	{
?>
						<p class="loading">Please Wait&nbsp;<span>.&nbsp;</span><span>.&nbsp;</span><span>.</span></p>
<?php
	}
?>
						
					</div>
				</div>	
			</div>
		</div>
		
				

	</div>
</div>