<?php                    
                    ////////////////////////////////////////////////////
                    // Add previous classification or add a drop down menu
                    // to include a classification
                    ////////////////////////////////////////////////////
                    $classification_array = array(
                         'genes'   => $variantArray[$i]['genes'],
                         'coding'   => $variantArray[$i]['coding'],
                         'amino_acid_change'   => $variantArray[$i]['amino_acid_change'],
                         'panel'   => $visitArray[0]['ngs_panel']
                    );
                             
                    $classification_result = $db->listAll('classification-variant', $classification_array);
               
                    if (!empty($classification_result))
                    {    

                         $classifcation_empty = 'no';

?>                              
                         <td><?= $classification_result[0]['classification'];?></td>
<?php
                    }

                    // add a select menu under an ajax call 
                    else
                    {

                         $classifcation_empty = 'yes';          
?>                          

                         <td>
                              <select class="change-classification select-right-open" data-genes="<?= $variantArray[$i]['genes'];?>" data-amino_acid_change="<?= $variantArray[$i]['amino_acid_change'];?>" data-coding="<?= $variantArray[$i]['coding'];?>" data-panel="<?= $visitArray[0]['ngs_panel'];?>">
                                   <option value="" disabled selected>-------</option>
                                   <option>Pathogenic</option>
                                   <option>Benign</option>
                                   <option>Presumed Benign</option>
                                   <option>Unknown Significance</option>
                                   <option>Likely Seq error - low Read Depth</option>
                                   <option>Seq error</option>
                              </select>
                         </td>
<?php
                    }

                    ////////////////////////////////////////////////////
                    // Add previous tiers 
                    ////////////////////////////////////////////////////
                    if ($variantArray[$i]['knowledge_id'] !== null)
                    {
                         $past_tiers = $db->listAll('tier-knowledge-id-overview', $variantArray[$i]['knowledge_id']);  
                    }                 
                    
                    if ($variantArray[$i]['knowledge_id'] === null || empty($past_tiers))
                    {
                         $past_tiers_present = 'no';
?>
                         <td>None</td>                         
<?php
                    }  
                    else
                    {
                         $past_tiers_present = 'yes';
                         $tier_str = '';
                         foreach ($past_tiers as $key => $tier_count)
                         {
                              if ($tier_str !== '')
                              {
                                   $tier_str.=', ';
                              }

                              $tier_str.= '<b>'.$tier_count['tier'].'</b>:'.$tier_count['count'];
                         }
?>
                         <td><?= $tier_str;?></td>
<?php
                    } 

                    // 
                    if ($_GET['page'] === 'QC_variant_status' && $past_tiers_present === 'no' && $classifcation_empty === 'yes')
                    {
$text_message_form .= '
Variant: '.$variantArray[$i]['genes'].' '. $variantArray[$i]['coding'].' '.$variantArray[$i]['amino_acid_change'].'
     Varsome Header (Number of persons classifying as Pathogenic)
     Varsome Variant basic info (VarSome Views)
     Varsome ClinVar (Varsome Variant basic info):
     Varsome Frequencies (gnomeAD Exomes):
     Varsome Cancer Databases (COSMIC Samples):
     
     ';
?>
                         <td class="alert-flagged-qc">Check Population Data and Predictions <a href="<?= $varsome_link; ?>" target="blank">varsome</a></td>                         
<?php    
                    }
                    else if ($_GET['page'] === 'QC_variant_status')
                    {
?>
                         <td class="alert-passed-qc">NA</td>
<?php                         
                    }
?>                 