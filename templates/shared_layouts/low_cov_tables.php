<?php
	if ( ( isset($lowCoverageArray) && !empty($lowCoverageArray) ) ||  
		( isset($low_cov_status) && !empty($low_cov_status) ) )
	{
?>

	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			<span class="report-section-bold">
				LOW-COVERAGE ( &lt;500-fold) REGIONS:
			</span>

		<!-- need to check if low_coverage_status_table no_low_amp-->
	<?php
		if ( isset($low_cov_status[0]) && !empty($low_cov_status[0]) && $low_cov_status[0]['status'] == 'no_low_amp')
		{
	?>
			<br>
			All of the targeted regions had coverage of at least 500-fold.  There were no regions of low-coverage. (Confirmed by: <?= $low_cov_status[0]['user_name'];?>)
	<?php
		}
		if ( isset($lowCoverageArray) && !empty($lowCoverageArray) )
		{
	?>
			<div class="alert alert-info">
				Low Coverage for genes with <span class="alert-danger glyphicon glyphicon-exclamation-sign" title="Gene Not included in Assay"></span> are only provided for user information here while building reports.  It will not be added to the final report.  
			</div>
			<table class="formated_table white_table" style="margin-bottom: 10px;">

				<thead>
					<th>Gene</th>
					<th>Amplicon</th>

<?php
					// The low_coverage_status_table was added after samples where already added to the database.  All of these samples had a type of exon_codons low_coverage_table.  Therefore default is set to exon_codons for this reason.
					if(isset($low_cov_status[0]['type']) && $low_cov_status[0]['type'] == 'chr_start_stop')
					{
?>
					<th>Chr</th>
					<th>Amp Start</th>
					<th>Amp End</th>
					
<?php
					}

					else
					{
?>
					<th>Exon</th>
					<th>Codons</th>
<?php
					}
?>
					<th>Depth</th>
				</thead>
				<tbody>
			<?php
				foreach ($lowCoverageArray as $key => $amp)
				{
			?>
					<tr>
						<td><?= !$amp['activate_status'] ? '<span class="alert-danger glyphicon glyphicon-exclamation-sign" title="Gene Not included in Assay"></span>':'';?> <?= $amp['gene'];?></td>
						<td><?= $amp['Amplicon'];?></td>
<?php
					if (isset($low_cov_status[0]['type']) && $low_cov_status[0]['type'] == 'chr_start_stop')
					{
?>	
						<td><?= $amp['chr'];?></td>
						<td><?= $amp['amp_start'];?></td>
						<td><?= $amp['amp_end'];?></td>

											
<?php
					}
					else
					{
?>	
						<td><?= $amp['exon'];?></td>
						<td><?= $amp['codons'];?></td>				
<?php
					}
?>					
						<td><?= $amp['depth'];?></td>
					</tr>	
			<?php
				}
			?>
				</tbody>
			</table>
	<?php
		}
	?>
		</div>
	</div>
<?php
	}
?>