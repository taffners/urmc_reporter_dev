<?php

// If this test is extraction_info yes then add 


if (isset($testInfo[0]['extraction_info']) && $testInfo[0]['extraction_info'] === 'yes')
{
?>
		<div class="form-group row">
			<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
				<label for="extraction_aliquoted_date" class="form-control-label">Aliquoted for Extraction Date: <span class="required-field d-none" id="extraction_aliquoted_date_span">*</span> </label>
			</div>
			<div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
				<input type="date" class="form-control" id="extraction_aliquoted_date" name="task_date[extraction_aliquoted]" placeholder="MM/DD/YYYY" pattern="\d{1,2}/\d{1,2}/\d{4}" value="<?= $utils->GetValueForUpdateInput($extraction_array, 'extraction_aliquoted_date');?>"/>
			</div>
		</div>
		<div class="form-group row">
			<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
				<label for="extraction_aliquoted" class="form-control-label">Initials: <span class="mulitple-field">*</span></label>
			</div>
			<div class="col-xs-12 col-sm-8 col-md-9 col-lg-9">	

			<?php
					if (isset($users_log_book))
					{

						$settings_array = array(
							'required-linked-field-status' => True,
					  		'required_linked_field'  => 'extraction_aliquoted_date',
							'required' 			=> False,
							'column-count'			=> 2
						);

						echo $utils->techCheckbox('task_initials[extraction_aliquoted][]', $users_log_book, $settings_array);
					}
			?>					
			</div>
		</div>
		<hr class="section-mark">
		<div class="form-group row">
			<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
				<label for="pk_digestion_date" class="form-control-label">If PE, PK 56C digestion Date: <span class="required-field d-none" id="pk_digestion_date_span">*</span></label>
			</div>
			<div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
				<input type="date" class="form-control" id="pk_digestion_date" name="task_date[pk_digestion]" placeholder="MM/DD/YYYY" pattern="\d{1,2}/\d{1,2}/\d{4}" />
			</div>
			<div class="col-xs-12 col-sm-12 col-md-1 col-lg-1">
				<label for="pk_digestion" class="form-control-label">Time: </label>
			</div>
			<div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
				<input type="time" class="form-control" id="pk_digestion_time" name="task_time[pk_digestion]"/>
			</div>
		</div>
		<div class="form-group row">
			<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
				<label for="pk_digestion" class="form-control-label">Initials: <span class="mulitple-field">*</span></label>
			</div>
			<div class="col-xs-12 col-sm-8 col-md-9 col-lg-9">	

			<?php
					if (isset($users_log_book))
					{

						$settings_array = array(
							'required-linked-field-status' => True,
					  		'required_linked_field'  => 'pk_digestion_date',
							'required' 			=> False,
							'column-count'			=> 2
						);

						echo $utils->techCheckbox('task_initials[pk_digestion][]', $users_log_book, $settings_array);
					}
			?>					
			</div>
		</div>

		<hr class="section-mark">
		<div class="form-group row">
			<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
				<label for="extraction_method" class="form-control-label">Extraction Method: <span class="required-field">*</span></label>
			</div>
			<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
				
				<label class="radio-inline">
					<input type="radio" name="extraction_method" value="Maxwell">
					Maxwell
				</label>
				<label class="radio-inline">
					<input type="radio" name="extraction_method" value="Qiagen">
					Qiagen
				</label>
			</div>
		</div>
	<?php
	// If user clicks on sprite for extraction_method use JavaScript to show and 
	// require sprite instrument used.
	if (isset($extractors) && !empty($extractors))
	{
	?>					
		<div id="extractors_row" class="form-group row d-none">
			<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
				<label for="extractors" class="form-control-label">Instrument Used: <span class="required-field">*</span></label>
			</div>
			<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
				
	<?php
			foreach ($extractors as $key => $instrument)
			{
	?>
				<div class="form-check">
					<input type="radio" <?= $key == 0? 'id="extractors_input_1"': '';?>name="extractors" value="<?= $instrument['instrument_id'];?>">
					<label class="radio"><?= $utils->instrumentInputName($instrument);?></label>
				</div>
	<?php
			}
	}
	?>
			</div>
		</div>
		<div class="form-group row">
			<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
				<label for="extraction_performed_by" class="form-control-label">Extraction Performed by Date:<span class="required-field">*</span></label>
			</div>
			<div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
				<input type="date" class="form-control" id="extraction_performed_by_date" name="task_date[extraction_performed_by]" placeholder="MM/DD/YYYY" pattern="\d{1,2}/\d{1,2}/\d{4}" required value="<?= date('Y-m-d');?>"/>
			</div>

		</div>
		<div class="form-group row">
			<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
				<label for="extraction_performed_by" class="form-control-label">Initials: <span class="mulitple-field">*</span></label>
			</div>
			<div class="col-xs-12 col-sm-8 col-md-9 col-lg-9">	

			<?php
					if (isset($users_log_book))
					{

						$settings_array = array(
							'required-linked-field-status' => False,
					  		'required_linked_field'  => False,
							'required' 			=> True,
							'column-count'			=> 2
						);

						echo $utils->techCheckbox('task_initials[extraction_performed_by][]', $users_log_book, $settings_array);
					}
			?>					
			</div>
		</div>
		<hr class="section-mark">
<?php
}
if 	(
		isset($testInfo[0]['quantification_info']) && 
		$testInfo[0]['quantification_info'] === 'yes' &&
		isset($testInfo[0]['extraction_info']) && 
		$testInfo[0]['extraction_info'] === 'no' 
	)
{
		$elution_vol_label = 'Volume Received';
?>
		<div class="form-group row">
			<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
				<label for="Nanodrop_date" class="form-control-label">Quantification Date: <span class="required-field">*</span> </label>
			</div>
			<div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
				<input type="date" class="form-control" id="extraction_aliquoted_date" name="task_date[Nanodrop]" placeholder="MM/DD/YYYY" pattern="\d{1,2}/\d{1,2}/\d{4}" value="<?= $utils->GetValueForUpdateInput($extraction_array, 'Nanodrop_date');?>"/>
			</div>
		</div>
	

		<div class="form-group row">
			<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
				<label for="Nanodrop" class="form-control-label">Quantification Performed by: <span class="mulitple-field">*</span><span class="required-field">*</span></label>
			</div>
			<div class="col-xs-12 col-sm-8 col-md-9 col-lg-9">	

			<?php
					if (isset($users_log_book))
					{

						$settings_array = array(
							'required-linked-field-status' => False,
					  		'required_linked_field'  => False,
							'required' 			=> True,
							'column-count'			=> 2
						);

						echo $utils->techCheckbox('task_initials[Nanodrop][]', $users_log_book, $settings_array);
					}
			?>					
			</div>
		</div>
		<hr class="section-mark">
<?php
}
if 	(
		isset($testInfo[0]['quantification_info']) && $testInfo[0]['quantification_info'] === 'yes' 
	)
{
?>		
		<div class="form-group row">
			<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
				<label for="quantification_method" class="form-control-label">Quantification Method: <span class="required-field">*</span></label>
			</div>
			<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
				<label class="radio">
					<input type="radio" name="quantification_method" value="nanodrop" checked>
					Nanodrop
				</label>
				<label class="radio">
					<input type="radio" name="quantification_method" value="quantus" >
					Quantus
				</label>
				<label class="radio">
					<input type="radio" name="quantification_method" value="qubit" >
					Qubit
				</label>
				<label class="radio">
					<input type="radio" name="quantification_method" value="NA" >
					NA
				</label>						
			</div>





			<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
				<label for="dna_conc_control_quant" class="form-control-label">DNA Concentration Quantification Control (ng/&mu;l):</label>
			</div>
			<div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
				<input type="number" class="form-control" id="dna_conc_control_quant" name="dna_conc_control_quant" step="0.01" min="0"/>
			</div>
		</div>
<?php
// Add thermal cyclers is not made in the logic for DNA extraction or Nanodrop tests
if (isset($quantifiers) && !empty($quantifiers))
{
?>
					
		<div class="form-group row">
			<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
				<label for="quantifiers" class="form-control-label">Quantifier Used: <span class="required-field">*</span><span class="mulitple-field">*</span></label>
			</div>
			<div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
	<?php
			foreach ($quantifiers as $key => $instrument)
			{
	?>
				<div class="form-check">
					<input class="form-check-input" type="checkbox" value="<?= $instrument['instrument_id'];?>" name="instruments[quantifiers][]">
					<label><?= $utils->instrumentInputName($instrument);?></label>
				</div>
	<?php
			}								
	?>			
				<div class="form-check">
					
					<input class="form-check-input" type="checkbox" value="NA" name="instruments[quantifiers][]" >
					<label>NA</label>
				</div>
			</div>
		</div>
<?php
}
?>


		<div class="form-group row">
			<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
				<label for="stock_conc" class="form-control-label">Stock Concentration (ng/&mu;l) (C<sub>1</sub>):</label>
			</div>
			<div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
				<input type="number" class="form-control" id="stock_conc" name="stock_conc" step="0.01" min="0"/>
			</div>
			<div class="col-xs-12 col-sm-12 col-md-1 col-lg-1">
				<label for="purity" class="form-control-label">260 / 280:</label>
			</div>
			<div class="col-xs-12 col-sm-12 col-md-1 col-lg-1">
				<input type="number" class="form-control" id="purity" name="purity" step="0.01" min="0"/>
			</div>
			<div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
				<label for="elution_volume" class="form-control-label"><?= $testInfo[0]['test_name'] === 'DNA Extraction'? 'Elution Volume': 'Volume Received';?>: <?= $testInfo[0]['test_name'] === 'DNA Extraction'? '<span class="required-field">*</span>': '';?></label>

			</div>
			<div class="col-xs-12 col-sm-12 col-md-1 col-lg-1">
				<input type="number" class="form-control" id="elution_volume" name="elution_volume" step="0.1" min="0" <?= $testInfo[0]['test_name'] === 'DNA Extraction'? 'required="required"': '';?>/>
			</div>

		</div>
		<div class="form-group row">
			<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
				<label for="dilution_exist" class="form-control-label">Will you be making a dilution?: <span class="required-field">*</span></label>
			</div>
			<div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
				<label class="radio">
					<input type="radio" name="dilution_exist" value="yes" required>
					Yes
				</label>
				<label class="radio">
					<input type="radio" name="dilution_exist" value="no">
					No
				</label>						
			</div>

			<span id="dilution-type-span" class="d-none col-xs-12 col-sm-12 col-md-7 col-lg-7">
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-5 col-lg-5">
						<label for="dilution_type" class="">What kind of dilution will you be making?: <span class="required-field">*</span></label>
					</div>
					<div class="col-xs-12 col-sm-12 col-md-7 col-lg-7">
						
						<div class="form-check">
						
							<input id="dilution-type-precise" type="radio" name="dilution_type" value="precise">
							<label class="form-check-label">
								Precise dilution using <?= SITE_TITLE;?> dilution calculator
							</label>
						</div>
						<label class="radio">
							
							<input type="radio" name="dilution_type" value="x_dilution">
							<label class="form-check-label">
								X Dilution
							</label>			
						</label>			
					</div>
				</div>
			</span>

		</div>
		

		<div class="form-group d-none" id="dilution-calculator">
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
					<hr class="section-mark">
				
					<h1>Dilution calculator</h1>
					<div class="alert alert-info">
						Equation used:<br> C<sub>1</sub>V<sub>1</sub> = C<sub>2</sub>V<sub>2</sub>
					</div>
					<div id="alert-stock-conc-empty" class="alert alert-danger d-none">
						<h1>Add Stock Concentration </h1>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
					<label for="vol_dna" class="form-control-label">Volume DNA (&mu;l) (V<sub>1</sub>):</label>
				</div>
				
				<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
					<label for="vol_eb" class="form-control-label">Volume EB (&mu;l):</label>
				</div>
				
				<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
					<label for="dilution_final_vol" class="form-control-label">Dilution final volume (&mu;l) (V<sub>2</sub>): <span class="required-field">*</span></label>
				</div>
				
				<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
					<label for="target_conc" class="form-control-label">Target Concentration (ng/&mu;l) (C<sub>2</sub>): <span class="required-field">*</span></label>
				</div>
			</div>
			
			<div class="row">

				<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
					
					<input type="number" class="form-control" id="vol_dna" name="vol_dna" step="0.1" min="0" readonly/>
										
				</div>

				<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
					
					<input type="number" class="form-control" id="vol_eb" name="vol_eb" step="0.1" min="0" readonly/>
										
				</div>

				<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
					
					<input type="number" class="form-control" id="dilution_final_vol" name="dilution_final_vol" step="0.1" min="0" value="50"/>
										
				</div>

				<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
					
					<input type="number" class="form-control" id="target_conc" name="target_conc" step="0.1" min="0" value="50"/>
										
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
					<label for="dilution_conc" class="form-control-label">Actual Dilution Final Concentration (ng/&mu;l) (C<sub>2</sub>): <span class="required-field">*</span></label>
				</div>
				<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
					
					<input type="number" class="form-control" id="dilution_conc" name="dilution_conc" step="0.1" min="0"/>
										
				</div>
			</div>
		</div>
		<div class="form-group d-none" id="x-dilution">
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
					<hr class="section-mark">
					<h3>X Dilution</h3>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-5 col-lg-5">
					
					<div class="row">
						<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
							<label for="times_dilution" class="form-control-label">Number X Dilution <br>(ex. enter 25 for 25X dilution): <span class="required-field">*</span></label>
						</div>
						<div class="col-xs-12 col-sm-4 col-md-6 col-lg-6">
							<input type="number" class="form-control" id="times_dilution" name="times_dilution" min="0"/>
						</div>
					</div>
				</div>			
			</div>
		</div>
		<div class="form-group row">
			
		</div>
<?php
}
?>