<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
	<h1>Samantha's Progress</h1>
<?php $day_count=0;?>
	<table class="formated_table-left">
		<thead>
			<tr>
				<th>End Date</th>
				<th>Task</th>
				<th class="d-print-none">Progress</th>
				<th class="d-print-none">Notes</th>
				<th class="d-print-none">Status</th>
				<th>Estimated # days</th>
			</tr>
		</thead>
		<tbody>
			<tr class="pending">
				<td></td>
				<td>Feature Add:  Huijie is almost ready with her Idylla EGFR test validation and she would need your help with integrating the reporting in Infotrack.</td>
				<td class="d-print-none">
					
				</td>
				<td class="d-print-none">					
					

				</td>
				<td class="d-print-none"><span class="dot dot-yellow"></span></td>
				<td>Unknown</td>
			</tr>
			
			<tr class="pending">
				<td></td>
				<td>Feature Add: Co sign feature
				</td>
				<td class="d-print-none">
					This is the summary of what I'm thinking for the cosign button.  Let me know if it is approved and you agree with this.



				Add a cosign option to MD level directors.  



				1. When a report is signed out by a non MD level director the MD level directors will be automatically notified by email to review the report and cosign the report.  



				2. I will add a cosign button which is only available to MD level directors to the right of the timestamp in the completed reports.



				3. Then the another signature will be added below.



				4. If the cosigner needs to change something should the report be placed in revision?  Meaning the old report will be saved and the revision history will appear at the top?
				</td>
				<td class="d-print-none">					
					
				</td>
				<td class="d-print-none"><span class="dot dot-yellow"></span></td>
				<td><?php $day_count= $day_count + 3;?>3</td>
			</tr>
			<tr class="pending">
				<td></td>
				<td>Feature Add: Since a reanalysis can be performed of data in thermofisher I need to make a way to transfer that data and seperate it from already backed up data.</td>
				<td class="d-print-none">
					
				</td>
				<td class="d-print-none">
					
				</td>
				<td class="d-print-none"><span class="dot dot-yellow"></span></td>
				<td><?php $day_count= $day_count + 3;?>3</td>
			</tr>

			<tr class="pending">
				<td></td>
				<td>Feature Add:  If a sample fails library prep make it removable</td>
				<td>
					<ol>
						<li>Currently the concentration pages have pass flagged radio button which is tied to the sample.  This might be best to change to be changed to </li>
						<li>For OFA, the final pooling Conc. should be more than 14 pM.</li>
						<li></li>
						<li></li>
						<li></li>
						<li></li>
					</ol>
				</td>
				<td>					
					
				</td>
				<td><span class="dot dot-yellow"></span></td>
				<td><?php $day_count= $day_count + 5;?>5 </td>
			</tr>
			<tr class="pending">
				<td></td>
				<td>Feature Add: Add a feature to duplicate a pool.  This will be useful for reanalysis.</td>
				<td class="d-print-none">
					
				</td>
				<td class="d-print-none">					
					
				</td>
				<td class="d-print-none"><span class="dot dot-yellow"></span></td>
				<td><?php $day_count= $day_count + 5;?>5</td>
			</tr>
			<tr class="pending">
				<td></td>
				<td>Feature Add: Add the ability for both a director and a tech to review an NGS report</td>
				<td class="d-print-none">
					
				</td>
				<td class="d-print-none">					
					
				</td>
				<td class="d-print-none"><span class="dot dot-yellow"></span></td>
				<td><?php $day_count= $day_count + 3;?>3</td>
			</tr>
			<tr class="pending">
				<td></td>
				<td>Feature Add: Since Bill wants to update the OFA software very often and thermofisher has awful documentation for there software I need to make a way more easily streamline process without knowing what Thermofisher is going to change.</td>
				<td class="d-print-none">
					
				</td>
				<td class="d-print-none">					
					
				</td>
				<td class="d-print-none"><span class="dot dot-yellow"></span></td>
				<td><?php $day_count= $day_count + 3;?>3</td>
			</tr>
			<tr class="pending">
				<td></td>
				<td>Feature Add: Lock down which samples data can be selected added to an OFA pool.  Currently any recent analysis can be linked.</td>
				<td class="d-print-none">
					
				</td>
				<td class="d-print-none">					
					
				</td>
				<td class="d-print-none"><span class="dot dot-yellow"></span></td>
				<td><?php $day_count= $day_count + 1;?>1</td>
			</tr>
			<tr class="pending">
				<td></td>
				<td>Feature Add: remove btn from old selected Download run OFA library prep excel sheet pool_download_excel page.  Set next excel number to 50.</td>
				<td class="d-print-none">
					
				</td>
				<td class="d-print-none">					
					
				</td>
				<td class="d-print-none"><span class="dot dot-yellow"></span></td>
				<td><?php $day_count= $day_count + 1;?>1</td>
			</tr>

			<tr class="pending">

				<td></td>
				<td>Add test status of in progress.  Display date status was added.  Include option to pick a status of update progress.  Add a way to view all of a certain test.  Include a filter dependent of status. Add test pool.  This is important for chimerism since turnaround time does not start right away.  I know you have a long list ahead of this, but I believe you already intend to work in our run calculation sheets at some point.  When that happens I would really like it to make a run sheet that we can see.  I had to track down John a couple times today to see if certain patients were on his run.  I'd love to be able to just go into infotrack and see a simple tasklist with the test, date we began setting up the assay, tech, and the patients on the run.   As I said, I realize it's not at the top of the list, just wanted to get it on the list if it wasn't already.

				Since you have been working at home you miss out on some of the notifications that are by word of mouth.  I'm sorry about that and will try to make sure to pass those things along to you.  It was recently announced that Bonnie will be retiring soon.  Her last day is June 5th.  I'm happy for her, but sad for me!  I'm trying to have Suni and myself train on all the myriad of things she handles for us.  Today we were working on correcting the turn around times and I was wondering if there was a way to make infotrack do this more accurately.
				 
				Here are my suggestions, and let me know if any of these seem reasonable to change..
				 
				1. If we use the time the test is added rather than the time the specimen is received for the start time we won't need to go back and correct the add-on tests TAT's
				 
				2. If we use the date the consent is rec'd for FVL, PTG, and HFE rather than the specimen rec'd date that would be best.. or we could fold that into #1 by having genetics tests logged in under a separate code like "SRHGC" Specimen Received Hold for Genetics Consent.   And then we would add the test when the consent is received and the clock would start. 
				 
				Also, can we make the list where we change the turn around times sorted by test, or sortable by selection of the column headers? It's easier to run through the genetics tests when they are grouped. 
				 
				I know you are very busy and this would probably take a while to make happen.  I'm hoping to have this implemented before Bonnie leaves if possible.  With all the new responsibilities for Suni to take on at once I just want to make things as easy for her as possible.  Thank you!!
				</td>
				<td>
					<ol>
						<li>Requirements:<span class="dot dot-purple"></span></li>
						<li>Design:<span class="dot dot-purple"></span></li>
						<li>List Risks:<span class="dot dot-purple"></span></li>
						<li>Implementation:<span class="dot dot-purple"></span></li>
						<li>Testing:<span class="dot dot-purple"></span></li>

					</ol>
				</td>
				<td></td>
				<td><span class="dot dot-purple"></span> ASAP</td>
				<td><?php $day_count= $day_count + 10;?>10</td>
			</tr>

			<tr class="pending">
				<td></td>
				<td>Feature Add: The excel sheet used to make the library for the OFA is very confusing and we would like to add it into infotrack.  This is a huge task.</td>
				<td>
					
				</td>
				<td>					
					
				</td>
				<td><span class="dot dot-yellow"></span></td>
				<td><?php $day_count= $day_count + 3;?>3</td>
			</tr>

			<tr class="pending">
				<td></td>
				<td>Feature Add: I have been wanting to make a way to automatically backup Infotrack on a daily basis that does not rely on ISD.  There turn around time for most things is too slow and I feel it would be best to have this backup locally also</td>
				<td>
					
				</td>
				<td>					
					
				</td>
				<td><span class="dot dot-yellow"></span></td>
				<td><?php $day_count= $day_count + 3;?>3</td>
		
			<tr class="pending">
				<td></td>
				<td>		///////////////////////////////////////////////////////////////
						// load phpMussel-1
							// Designed to detect trojans, viruses, malware and other threats 
							// phpMussel activates when $_FILES not empty.
							// Turned off for upload_NGS_data since the tsv from the ion torrent triggers an Office chameleon attack.  Most likely this happens because the file is named with multiple extensions (.amplicon.cov.xls)  Since the files uploaded on upload_NGS_data page are not being stored on the server and the files are checked for a specific format this should be ok.  Another possible solution to this is to change phpMussel config.ini file by turning of allow_leading_trailing_dots or chamelon_to_doc.  I will try this first because turning off these checks seems more hazardous because it will be turned off for files which are actually stored on the server.
						///////////////////////////////////////////////////////////////		  
						if ($page !== 'upload_NGS_data')
						{			
							require_once('/var/www/phpMussel-1/loader.php');
						}
				</td>
				<td>
				</td>
				<td>					
					
				</td>
				<td><span class="dot dot-yellow"></span></td>
				<td><?php $day_count= $day_count + 2;?>2</td>
			</tr>
			
			
			<tr class="pending">
				<td></td>
				<td>Feature Add:  remove drop down menu for start barcode OFA.  Instead autofill with next barcodes.</td>
				<td>
				</td>
				<td>					
					
				</td>
				<td><span class="dot dot-yellow"></span></td>
				<td><?php $day_count= $day_count + 2;?>2</td>
			</tr>
			<tr class="pending">
				<td></td>
				<td>Feature Add:  Add a dilution worksheet to 10 ng/ul (myeloid is set at 5 ng/ul)</td>
				<td>
				</td>
				<td>					
					
				</td>
				<td><span class="dot dot-yellow"></span></td>
				<td><?php $day_count= $day_count + 2;?>2</td>
			</tr>
			<tr class="pending">
				<td></td>
				<td>Feature Add: Automatically email user with pool_email permissions when a pool is created.</td>
				<td>
					
				</td>
				<td>					
					
				</td>
				<td><span class="dot dot-yellow"></span></td>
				<td><?php $day_count= $day_count + 2;?>2</td>
			</tr>

			<tr class="pending">
				<td></td>
				<td>Add a sub panel for ngs.  Add this as part of login.  Then genes that are not in the panel will not be included for QC and in verify variants however, they will be visible in all variants.  The low coverage table and the table of genes in the report will also reflect the sub panel.  For OFA default will be the live sub panel subset.</td>
				<td>
					
				</td>
				<td>					
					
				</td>
				<td><span class="dot dot-yellow"></span></td>
				<td><?php $day_count= $day_count + 5;?>5</td>
			</tr>
			

			
			
			
			
			
			<tr class="pending">
				<td></td>
				<td>Bug Fix: "I am adding NGS tests to some Stanford proficiency samples and I have been making a comment at the end of the NGS page saying "Proficiency sample". However, when I print the NGS test sheet my comment isn't on there. Only my comment that I placed on the login sheet is printed."
				</td>
				<td>
					<ol>
						<li>Design:<span class="dot dot-purple"></span></li>
						<li>List Risks:<span class="dot dot-purple"></span></li>
						<li>Implementation:<span class="dot dot-purple"></span></li>
						<li>Testing:<span class="dot dot-purple"></span></li>
					</ol>
				</td>
				<td>					
					<ul>
						
						<li>
							
						</li>
						<li>
							
						</li>
						<li></li>
						<li></li>
					</ul>
				</td>
				<td><span class="dot dot-yellow"></span></td>
				<td><?php $day_count= $day_count + 1;?>1</td>
			</tr>
			
			<tr class="pending">
				<td></td>
				<td>Feature Add: If something happens while copying files and md5 is incorrect make an override to delete and restart.  Currently it will not try again to make sure we do not write over any files that have been transfered to infotrack.  This can be a task accomplished after OFA test goes live.</td>
				<td>
					<ol>
						<li>Design:<span class="dot dot-yellow"></span></li>
						<li>List Risks:<span class="dot dot-yellow"></span></li>
						<li>Implementation:<span class="dot dot-yellow"></span></li>
						<li>Testing:<span class="dot dot-yellow"></span></li>
					</ol>
				</td>
				<td>					
					
				</td>
				<td><span class="dot dot-yellow"></span></td>
				<td><?php $day_count= $day_count + 2;?>2 </td>
			</tr>
			<tr class="pending">
				<td></td>
				<td>Feature Add: thermo_backup_ion_reporter_run.py add check user input function</td>
				<td>
					<ol>
						<li>Design:<span class="dot dot-yellow"></span></li>
						<li>List Risks:<span class="dot dot-yellow"></span></li>
						<li>Implementation:<span class="dot dot-yellow"></span></li>
						<li>Testing:<span class="dot dot-yellow"></span></li>
					</ol>
				</td>
				<td>					
					Since this script is not planned to be run by a user I put this off until after OFA goes live.
				</td>
				<td><span class="dot dot-yellow"></span></td>
				<td><?php $day_count= $day_count + 1;?>1 </td>
			</tr>
			<tr class="pending">
				<td></td>
				<td>Check: Check that run_qc files are not added again if the file is already there.</td>
				<td>
					<ol>
						<li>Design:<span class="dot dot-yellow"></span></li>
						<li>List Risks:<span class="dot dot-yellow"></span></li>
						<li>Implementation:<span class="dot dot-yellow"></span></li>
						<li>Testing:<span class="dot dot-yellow"></span></li>
					</ol>
				</td>
				<td>					
					
				</td>
				<td><span class="dot dot-yellow"></span></td>
				<td><?php $day_count= $day_count + 1;?>1 </td>
			</tr>			
			<tr class="pending">
				<td></td>
				<td>Bug Fix:  User is able to push back button after signing out a test.  It will add another record.  It doesn't really cause any issues just found it.</td>
				<td>
					
				</td>
				<td>					
					<b>Why are there multiple additions of extraction to extraction_log_table for the same extraction log id?</b><br><br>
					MariaDB [urmc_reporter_db]> SELECT * FROM ordered_test_table WHERE sample_log_book_id = 3451;
					+-----------------+---------+--------------------+----------------------+----------+---------------+-----------------                                                                                                                        -+--------------+--------------+----------------------+------------+-------------+--------------------------+--------                                                                                                                        -----+---------------------+--------------------+-------------+
					| ordered_test_id | user_id | orderable_tests_id | previous_positive_id | visit_id | soft_path_num | outside_case_num                                                                                                                         | consent_info | consent_date | consent_for_research | start_date | finish_date | expected_turnaround_time | test_st                                                                                                                        atus | time_stamp          | sample_log_book_id | test_result |
					+-----------------+---------+--------------------+----------------------+----------+---------------+-----------------                                                                                                                        -+--------------+--------------+----------------------+------------+-------------+--------------------------+--------                                                                                                                        -----+---------------------+--------------------+-------------+
					|            6979 |      21 |                 11 |                 NULL |     NULL |               |                                                                                                                                          |              | 0000-00-00   |                      | 2020-01-27 | 2020-01-27  |                     NULL | complet                                                                                                                        e    | 2020-01-27 12:21:02 |               3451 |             |
					+-----------------+---------+--------------------+----------------------+----------+---------------+-----------------                                                                                                                        -+--------------+--------------+----------------------+------------+-------------+--------------------------+--------                                                                                                                        -----+---------------------+--------------------+-------------+
					1 row in set (0.01 sec)

					MariaDB [urmc_reporter_db]> SELECT * FROM extraction_log_table WHERE ordered_test_id = 6979;
					+-------------------+---------+-----------------+-------------------+------------+----------------+----------------+-                                                                                                                        --------------------+----------------+--------+
					| extraction_log_id | user_id | ordered_test_id | extraction_method | stock_conc | dilution_exist | dilutions_conc |                                                                                                                         time_stamp          | times_dilution | purity |
					+-------------------+---------+-----------------+-------------------+------------+----------------+----------------+-                                                                                                                        --------------------+----------------+--------+
					|              3215 |       9 |            6979 | NULL              |      834.8 | yes            |           35.7 |                                                                                                                         2020-01-27 15:59:43 |           NULL |      2 |
					|              3216 |       9 |            6979 | NULL              |      834.8 | yes            |           25.7 |                                                                                                                         2020-01-27 16:00:19 |           NULL |      2 |
					|              3217 |       9 |            6979 | NULL              |      834.8 | yes            |           25.7 |                                                                                                                         2020-01-27 16:01:01 |           NULL |      2 |
					+-------------------+---------+-----------------+-------------------+------------+----------------+----------------+-                                                                                                                        --------------------+----------------+--------+
					3 rows in set (0.00 sec)

					MariaDB [urmc_reporter_db]> SELECT * FROM users_performed_task_table WHERE ref_table = 'extraction_log_table' AND ref_id = 3215;
					+-------------------------+---------+---------+--------+----------------------+----------------+----------------+---------------------+
					| users_performed_task_id | user_id | task_id | ref_id | ref_table            | date_performed | time_performed | time_stamp          |
					+-------------------------+---------+---------+--------+----------------------+----------------+----------------+---------------------+
					|                   10430 |       9 |       3 |   3215 | extraction_log_table | 2020-01-27     | 00:00:00       | 2020-01-27 15:59:43 |
					+-------------------------+---------+---------+--------+----------------------+----------------+----------------+---------------------+
					1 row in set (0.00 sec)

					MariaDB [urmc_reporter_db]> SELECT * FROM users_performed_task_table WHERE ref_table = 'extraction_log_table' AND ref_id = 3216;
					+-------------------------+---------+---------+--------+----------------------+----------------+----------------+---------------------+
					| users_performed_task_id | user_id | task_id | ref_id | ref_table            | date_performed | time_performed | time_stamp          |
					+-------------------------+---------+---------+--------+----------------------+----------------+----------------+---------------------+
					|                   10431 |       9 |       3 |   3216 | extraction_log_table | 2020-01-27     | 00:00:00       | 2020-01-27 16:00:19 |
					+-------------------------+---------+---------+--------+----------------------+----------------+----------------+---------------------+
					1 row in set (0.01 sec)

					MariaDB [urmc_reporter_db]> SELECT * FROM users_performed_task_table WHERE ref_table = 'extraction_log_table' AND ref_id = 3217;
					+-------------------------+---------+---------+--------+----------------------+----------------+----------------+---------------------+
					| users_performed_task_id | user_id | task_id | ref_id | ref_table            | date_performed | time_performed | time_stamp          |
					+-------------------------+---------+---------+--------+----------------------+----------------+----------------+---------------------+
					|                   10432 |      21 |       3 |   3217 | extraction_log_table | 2020-01-27     | 00:00:00       | 2020-01-27 16:01:01 |
					|                   10433 |       9 |       3 |   3217 | extraction_log_table | 2020-01-27     | 00:00:00       | 2020-01-27 16:01:01 |
					+-------------------------+---------+---------+--------+----------------------+----------------+----------------+---------------------+

					MariaDB [urmc_reporter_db]> SELECT * FROM task_table;
					+---------+---------+-------------------------+---------------------+
					| task_id | user_id | task                    | time_stamp          |
					+---------+---------+-------------------------+---------------------+
					|       1 |       1 | extraction_aliquoted    | 2019-05-20 14:51:04 |
					|       2 |       1 | pk_digestion            | 2019-05-20 14:51:16 |
					|       3 |       1 | extraction_performed_by | 2019-05-20 14:51:09 |
					|       4 |       1 | wet_bench               | 2019-05-21 14:45:40 |
					|       5 |       1 | analysis                | 2019-05-21 14:45:40 |
					|       6 |       1 | Nanodrop                | 2019-11-05 16:01:10 |
					+---------+---------+-------------------------+---------------------+

					MariaDB [urmc_reporter_db]> SELECT * FROM users_performed_task_table WHERE ref_table = 'ordered_test_table' AND ref_id = 6979;
					Empty set (0.00 sec)


				</td>
				<td><span class="dot dot-yellow"></span></td>
				<td><?php $day_count= $day_count + 1;?>1</td>
			</tr>


			<tr class="pending">
				<td></td>
				<td>Bug Fix: When creating a user my name is referenced in the location email not the user.  It says Hello Samantha,....</td>
				<td>
					<ol>
						<li>Design:<span class="dot dot-yellow"></span></li>
						<li>List Risks:<span class="dot dot-yellow"></span></li>
						<li>Implementation:<span class="dot dot-yellow"></span></li>
						<li>Testing:<span class="dot dot-yellow"></span></li>
					</ol>
				</td>
				<td>					
					
				</td>
				<td><span class="dot dot-yellow"></span></td>
				<td><?php $day_count= $day_count + 1;?>1 </td>
			</tr>
			
			<tr class="pending">
				<td></td>
				<td>Bug Fix: Make sure version-use is unique and follows a specfic format with no spaces.</td>
				<td>
					<ol>
						<li>Design:<span class="dot dot-yellow"></span></li>
						<li>List Risks:<span class="dot dot-yellow"></span></li>
						<li>Implementation:<span class="dot dot-yellow"></span></li>
						<li>Testing:<span class="dot dot-purple"></span></li>
					</ol>
				</td>
				<td>					
					
				</td>
				<td><span class="dot dot-yellow"></span></td>
				<td><?php $day_count= $day_count + 1;?>1</td>
			</tr>


			<tr class="pending">
				<td></td>
				<td>Transfer the OFA sample sheet to the N: ofa_queue folder</td>
				<td>
					<ol>
						<li>Design:<span class="dot dot-green"></span></li>
						<li>List Risks:<span class="dot dot-green"></span></li>
						<li>Implementation:<span class="dot dot-yellow"></span></li>
						<li>Testing:<span class="dot dot-purple"></span></li>
					</ol>
				</td>
				<td>					
					

				</td>
				<td><span class="dot dot-yellow"></span></td>
				<td><?php $day_count= $day_count + 1;?>1</td>
			</tr>
			
			<tr class="pending">
				<td></td>
				<td>Make Sure only low coverage in genes looking at displays in final PDF.  However, show all info while building form.  Maybe color it different if the gene is not being reported.</td>
				<td>
					<ol>
						<li>Design:<span class="dot dot-green"></span></li>
						<li>List Risks:<span class="dot dot-green"></span></li>
						<li>Implementation:<span class="dot dot-yellow"></span></li>
						<li>Testing:<span class="dot dot-purple"></span></li>

					</ol>
				</td>
				<td>					
					<ol>
						<li>
							<ul>
								<li></li>
							</ul>
						</li>
						<li></li>
						<li></li>
						<li></li>
					</ol>

				</td>
				<td><span class="dot dot-yellow"></span></td>
				<td><?php $day_count= $day_count + 1;?>1</td>
			</tr>	
			<tr class="pending">
				<td></td>
				<td>
					Add a button to hide or keep canned summary for NGS.  Requested December 10, 2019 by Paul and Zoltan.
				</td>
				<td>
					<ol>
						<li>Design:<span class="dot dot-green"></span></li>
						<li>List Risks:<span class="dot dot-green"></span></li>
						<li>Implementation:<span class="dot dot-yellow"></span></li>
						<li>Testing:<span class="dot dot-purple"></span></li>

					</ol>
				</td>
				<td>					
					

				</td>
				<td><span class="dot dot-yellow"></span></td>
				<td><?php $day_count= $day_count + 1;?>1</td>
			</tr>

			<tr class="pending">
				<td></td>
				<td>
					Add a search box to make signing out samples faster.  Requested by Nufatt on December 10th, 2019 lab meeting.
				</td>
				<td>
					<ol>
						<li>Design:<span class="dot dot-green"></span></li>
						<li>List Risks:<span class="dot dot-green"></span></li>
						<li>Implementation:<span class="dot dot-yellow"></span></li>
						<li>Testing:<span class="dot dot-purple"></span></li>

					</ol>
				</td>
				<td>					
					

				</td>
				<td><span class="dot dot-yellow"></span></td>
				<td><?php $day_count= $day_count + 1;?>1</td>
			</tr>
			<tr class="pending">
				<td></td>
				<td>mysql dump db when the first user of the day logs in.  This will require a check for the first user.</td>
				<td>
					<ol>
						<li>Design:<span class="dot dot-green"></span></li>
						<li>List Risks:<span class="dot dot-green"></span></li>
						<li>Implementation:<span class="dot dot-yellow"></span></li>
						<li>Testing:<span class="dot dot-purple"></span></li>

					</ol>
				</td>
				<td>					
					

				</td>
				<td><span class="dot dot-yellow"></span></td>
				<td><?php $day_count= $day_count + 1;?>1</td>
			</tr>			

			<tr class="pending">

				<td></td>
				<td>Add sample log book comments to NGS tests and message board.</td>
				<td>
					<ol>
						<li>Requirements:<span class="dot dot-purple"></span></li>
						<li>Design:<span class="dot dot-purple"></span></li>
						<li>List Risks:<span class="dot dot-purple"></span></li>
						<li>Implementation:<span class="dot dot-purple"></span></li>
						<li>Testing:<span class="dot dot-purple"></span></li>
					</ol>
				</td>
				<td>
					<ol>
						
					</ol>

				</td>
				<td><span class="dot dot-purple"></span> ASAP</td>
				<td><?php $day_count= $day_count + 1;?>1</td>
			</tr>

			<tr class="pending">

				<td></td>
				<td>Make sure OFA is checking file name during upload.</td>
				<td>
					<ol>
						<li>Requirements:<span class="dot dot-purple"></span></li>
						<li>Design:<span class="dot dot-purple"></span></li>
						<li>List Risks:<span class="dot dot-purple"></span></li>
						<li>Implementation:<span class="dot dot-purple"></span></li>
						<li>Testing:<span class="dot dot-purple"></span></li>

					</ol>
				</td>
				<td></td>
				<td><span class="dot dot-purple"></span> ASAP</td>
				<td><?php $day_count= $day_count + 1;?>1</td>
			</tr>
			<tr class="pending">

				<td></td>
				<td>Change myeloid panel name to HEMATOLYMPHOID PANEL BY NGS W/ INTERP.  Divide ngs_panel_step_regulator into two tables ngs_flow_steps_table and ngs_panel_step_regulator This way all ngs panel will have to either but on or off for each step. </td>
				<td>
					<ol>
						<li>Requirements:<span class="dot dot-purple"></span></li>
						<li>Design:<span class="dot dot-purple"></span></li>
						<li>List Risks:<span class="dot dot-purple"></span></li>
						<li>Implementation:<span class="dot dot-purple"></span></li>
						<li>Testing:<span class="dot dot-purple"></span></li>

					</ol>
				</td>
				<td></td>
				<td><span class="dot dot-purple"></span> ASAP</td>
				<td><?php $day_count= $day_count + 5;?>5</td>
			</tr>
			<tr class="pending">

				<td></td>
				<td>Add Varsome API to verify variants, QC, and when adding data to message board</td>
				<td>
					<ol>
						<li>Requirements:<span class="dot dot-purple"></span></li>
						<li>Design:<span class="dot dot-purple"></span></li>
						<li>List Risks:<span class="dot dot-purple"></span></li>
						<li>Implementation:<span class="dot dot-purple"></span></li>
						<li>Testing:<span class="dot dot-purple"></span></li>

					</ol>
				</td>
				<td></td>
				<td><span class="dot dot-purple"></span> ASAP</td>
				<td><?php $day_count= $day_count + 10;?>10</td>
			</tr>			
			<tr class="pending">

				<td></td>
				<td>Add a way to track confirmations, primers, MM </td>
				<td>
					<ol>
						<li>Requirements:<span class="dot dot-purple"></span></li>
						<li>Design:<span class="dot dot-purple"></span></li>
						<li>List Risks:<span class="dot dot-purple"></span></li>
						<li>Implementation:<span class="dot dot-purple"></span></li>
						<li>Testing:<span class="dot dot-purple"></span></li>

					</ol>
				</td>
				<td></td>
				<td><span class="dot dot-purple"></span> ASAP</td>
				<td><?php $day_count= $day_count + 10;?>10</td>
			</tr>
			
			<tr class="pending">

				<td></td>
				<td>Link Sample log book with NGS data.  Currently if a sample visit form is updated it is not linked to NGS visit form.  Also if a test is canceled in log book it is not removed in from NGS.

				Currently only Directors can cancel NGS tests.  Make this its own permission so I can grant permissions to Suni, Paige, and Bonnie.  Might want the cancel of tests in sample log book to also cancel pending tests in NGS pending list.  Might not be good because everyone has cancel permissions for this.</td>
				<td>
					<ol>
						<li>Requirements:<span class="dot dot-purple"></span></li>
						<li>Design:<span class="dot dot-purple"></span></li>
						<li>List Risks:<span class="dot dot-purple"></span></li>
						<li>Implementation:<span class="dot dot-purple"></span></li>
						<li>Testing:<span class="dot dot-purple"></span></li>

					</ol>
				</td>
				<td>
					<ul>
						<li>If sample is canceled but it shouldn't be.  Do I want every to have access to this?</li>
						<li>What if test is in a pool?</li>
						<li>What if test is in outstanding reports section?</li>
					</ul>
				</td>
				<td><span class="dot dot-purple"></span> ASAP</td>
				<td><?php $day_count= $day_count + 1;?>1</td>
			</tr>
			
			<tr class="pending">

				<td></td>
				<td>Make signed out sample data editable.  For right now I need to edit these when a mistake happens.</td>
				<td>
					<ol>
						<li>Requirements:<span class="dot dot-purple"></span></li>
						<li>Design:<span class="dot dot-purple"></span></li>
						<li>List Risks:<span class="dot dot-purple"></span></li>
						<li>Implementation:<span class="dot dot-purple"></span></li>
						<li>Testing:<span class="dot dot-purple"></span></li>

					</ol>
				</td>
				<td></td>
				<td><span class="dot dot-purple"></span> ASAP</td>
				<td><?php $day_count= $day_count + 5;?>5</td>
			</tr>
			<tr class="pending">

				<td></td>
				<td>Add oncoKB https://oncokb.org/cancerGenes variant interpt API.  Requested by Huijie</td>
				<td>
					<ol>
						<li>Requirements:<span class="dot dot-purple"></span></li>
						<li>Design:<span class="dot dot-purple"></span></li>
						<li>List Risks:<span class="dot dot-purple"></span></li>
						<li>Implementation:<span class="dot dot-purple"></span></li>
						<li>Testing:<span class="dot dot-purple"></span></li>

					</ol>
				</td>
				<td></td>
				<td><span class="dot dot-purple"></span> Medium</td>
				<td><?php $day_count= $day_count + 5;?>5</td>
			</tr>
			<tr class="pending">

				<td></td>
				<td>Add DOB and Gender to sample log book and make sure it automatically transfers to NGS test form.  Make sure it shows up on pdf work sheet print out.  Nuffatt wants this for chimerism.</td>
				<td>
					<ol>
						<li>Requirements:<span class="dot dot-purple"></span></li>
						<li>Design:<span class="dot dot-purple"></span></li>
						<li>List Risks:<span class="dot dot-purple"></span></li>
						<li>Implementation:<span class="dot dot-purple"></span></li>
						<li>Testing:<span class="dot dot-purple"></span></li>

					</ol>
				</td>
				<td></td>
				<td><span class="dot dot-purple"></span> ASAP</td>
				<td><?php $day_count= $day_count + 1;?>1</td>
			</tr>
			<tr class="pending">

				<td></td>
				<td>For genetic tests the turn around time should start when the consent is received.</td>
				<td>
					<ol>
						<li>Requirements:<span class="dot dot-purple"></span></li>
						<li>Design:<span class="dot dot-purple"></span></li>
						<li>List Risks:<span class="dot dot-purple"></span></li>
						<li>Implementation:<span class="dot dot-purple"></span></li>
						<li>Testing:<span class="dot dot-purple"></span></li>

					</ol>
				</td>
				<td></td>
				<td><span class="dot dot-purple"></span> ASAP</td>
				<td><?php $day_count= $day_count + 2;?>2</td>
			</tr>
			<tr class="pending">

				<td></td>
				<td>When this came up previously, I thought Nufatt was saying it was a duplicate sample with the same order number etc.  For example, sometimes when 3 tests are ordered they will give us 3 lavs instead of just 1.  But what he is saying is there is a second sample sent with a different order number.  He wants another option up in the sample type toggle for duplicate sample and no tests would be added to it.  I guess he doesn't want it just mentioned in the comments because then you can't search for or add tests to it if you need it? </td>
				<td>
					<ol>
						<li>Requirements:<span class="dot dot-purple"></span></li>
						<li>Design:<span class="dot dot-purple"></span></li>
						<li>List Risks:<span class="dot dot-purple"></span></li>
						<li>Implementation:<span class="dot dot-purple"></span></li>
						<li>Testing:<span class="dot dot-purple"></span></li>
					</ol>
				</td>
				<td></td>
				<td><span class="dot dot-purple"></span> ASAP</td>
				<td><?php $day_count= $day_count + 5;?>5</td>
			</tr>

			<tr class="pending">

				<td></td>
				<td>OFA tsv is being detected as a virus file.  Currently phpMussel is turned off for upload_NGS_data.  See if phpMussel allows a way to train it with files. (allow_leading_trailing_dots) (chamelon_to_doc)  More info in config</td>
				<td>
					<ol>
						<li>Requirements:<span class="dot dot-purple"></span></li>
						<li>Design:<span class="dot dot-purple"></span></li>
						<li>List Risks:<span class="dot dot-purple"></span></li>
						<li>Implementation:<span class="dot dot-purple"></span></li>
						<li>Testing:<span class="dot dot-purple"></span></li>

					</ol>
				</td>
				<td></td>
				<td><span class="dot dot-purple"></span> mid priority</td>
				<td>unknown</td>
			</tr>
			
			<tr class="pending">

				<td></td>
				<td>OFA:  Currently run QC needs to be done on all samples and I want to link it to Positive control.  This way Positive control needs to be completed before the NGS sample reports can be signed out.</td>
				<td>
					<ol>
						<li>Requirements:<span class="dot dot-purple"></span></li>
						<li>Design:<span class="dot dot-purple"></span></li>
						<li>List Risks:<span class="dot dot-purple"></span></li>
						<li>Implementation:<span class="dot dot-purple"></span></li>
						<li>Testing:<span class="dot dot-purple"></span></li>

					</ol>
				</td>
				<td>Need to implement auto pull for myeloid also otherwise I will need to maintain to systems for QC run.</td>
				<td><span class="dot dot-purple"></span> low</td>
				<td><?php $day_count= $day_count + 3;?>3</td>
			</tr>

			<tr class="pending">

				<td></td>
				<td>Add worksheets for each bench test for single analyte test</td>
				<td>
					<ol>
						<li>Requirements:<span class="dot dot-purple"></span></li>
						<li>Design:<span class="dot dot-purple"></span></li>
						<li>List Risks:<span class="dot dot-purple"></span></li>
						<li>Implementation:<span class="dot dot-purple"></span></li>
						<li>Testing:<span class="dot dot-purple"></span></li>

					</ol>
				</td>
				<td></td>
				<td><span class="dot dot-purple"></span> low</td>
				<td><?php $day_count= $day_count + 10;?>10</td>
			</tr>

			<tr class="pending">

				<td></td>
				<td>Add worksheet for OFA to replace current excel sheet</td>
				<td>
					<ol>
						<li>Requirements:<span class="dot dot-purple"></span></li>
						<li>Design:<span class="dot dot-purple"></span></li>
						<li>List Risks:<span class="dot dot-purple"></span></li>
						<li>Implementation:<span class="dot dot-purple"></span></li>
						<li>Testing:<span class="dot dot-purple"></span></li>

					</ol>
				</td>
				<td>This is low priority because it is uncertain if the Chef will take this over.</td>
				<td><span class="dot dot-purple"></span> low</td>
				<td><?php $day_count= $day_count + 10;?>10</td>
			</tr>
			<tr class="pending">

				<td></td>
				<td>Make CosmicID editable.  Currently it is only editable when it wasn't used in in report yet.  Most of the time it isn't an issue since the reporter is updated from cosmic but for so reason sometimes a Cosmic number is entered on new variants which is incorrrect.  This can only be edited if I am here.</td>
				<td>
					<ol>
						<li>Requirements:<span class="dot dot-purple"></span></li>
						<li>Design:<span class="dot dot-purple"></span></li>
						<li>List Risks:<span class="dot dot-purple"></span></li>
						<li>Implementation:<span class="dot dot-purple"></span></li>
						<li>Testing:<span class="dot dot-purple"></span></li>

					</ol>
				</td>
				<td>Problem occurs if a report was already signed out under the incorrect cosmicID.  Then the previous report would have to be sent to revise.  This way the current report will save.</td>
				<td><span class="dot dot-purple"></span> low</td>
				<td><?php $day_count= $day_count + 10;?>10</td>
			</tr>
			<tr class="pending">
				
				<td></td>
				<td>Add control data for each test.  This is to track controls thru time like Bonnie was previously doing.</td>
				<td>
					<ol>
						<li>Requirements:<span class="dot dot-purple"></span></li>
						<li>Design:<span class="dot dot-purple"></span></li>
						<li>List Risks:<span class="dot dot-purple"></span></li>
						<li>Implementation:<span class="dot dot-purple"></span></li>
						<li>Testing:<span class="dot dot-purple"></span></li>

					</ol>
				</td>
				<td></td>
				<td><span class="dot dot-purple"></span> low</td>
				<td><?php $day_count= $day_count + 10;?>10</td>
			</tr>
			<tr class="pending">

				<td></td>
				<td>Allow for multiple images to be uploaded for both the sample and the test.  Examples of images would be Reqs, emails, reports, previous test results, etc.  Requested by Zoltan November 19th, 2019</td>
				<td>
					<ol>
						<li>Requirements:<span class="dot dot-purple"></span></li>
						<li>Design:<span class="dot dot-purple"></span></li>
						<li>List Risks:<span class="dot dot-purple"></span></li>
						<li>Implementation:<span class="dot dot-purple"></span></li>
						<li>Testing:<span class="dot dot-purple"></span></li>

					</ol>
				</td>
				<td>Need More storage space</td>
				<td><span class="dot dot-purple"></span> low</td>
				<td><?php $day_count= $day_count + 10;?>10</td>
			</tr>
			<tr class="pending">

				<td></td>
				<td>Add Cellularity to OFA run template</td>
				<td>
					<ol>
						<li>Requirements:<span class="dot dot-purple"></span></li>
						<li>Design:<span class="dot dot-purple"></span></li>
						<li>List Risks:<span class="dot dot-purple"></span></li>
						<li>Implementation:<span class="dot dot-purple"></span></li>
						<li>Testing:<span class="dot dot-purple"></span></li>

					</ol>
				</td>
				<td>Low Priority, This is for copy number but I would like to add it</td>
				<td><span class="dot dot-purple"></span> low</td>
				<td><?php $day_count= $day_count + 2;?>2</td>
			</tr>
			<tr class="pending">

				<td></td>
				<td>Search for variants.  search by reports which do not have a variant in a gene</td>
				<td>
					<ol>
						<li>Requirements:<span class="dot dot-purple"></span></li>
						<li>Design:<span class="dot dot-purple"></span></li>
						<li>List Risks:<span class="dot dot-purple"></span></li>
						<li>Implementation:<span class="dot dot-purple"></span></li>
						<li>Testing:<span class="dot dot-purple"></span></li>

					</ol>
				</td>
				<td></td>
				<td><span class="dot dot-purple"></span> low</td>
				<td><?php $day_count= $day_count + 3;?>3</td>
			</tr>
			<tr class="pending">

				<td></td>
				<td>For genetic tests do not allow tests to be signed out without consent.</td>
				<td>
					<ol>
						<li>Requirements:<span class="dot dot-purple"></span></li>
						<li>Design:<span class="dot dot-purple"></span></li>
						<li>List Risks:<span class="dot dot-purple"></span></li>
						<li>Implementation:<span class="dot dot-purple"></span></li>
						<li>Testing:<span class="dot dot-purple"></span></li>

					</ol>
				</td>
				<td></td>
				<td><span class="dot dot-purple"></span> low</td>
				<td><?php $day_count= $day_count + 2;?>2</td>
			</tr>
			<tr class="pending">

				<td></td>
				<td>strand bias hover card not working Strand bias occurs when the genotype inferred from information presented by the forward strand and the reverse strand disagrees.  For example, at a given position in the genome, the reads mapped to the forward strand support a heterozygous genotype, while the reads mapped to the reverse strand support a homozygous genotype.</td>
				<td>
					<ol>
						<li>Requirements:<span class="dot dot-purple"></span></li>
						<li>Design:<span class="dot dot-purple"></span></li>
						<li>List Risks:<span class="dot dot-purple"></span></li>
						<li>Implementation:<span class="dot dot-purple"></span></li>
						<li>Testing:<span class="dot dot-purple"></span></li>

					</ol>
				</td>
				<td>This will be added to Phil's new tutorial.</td>
				<td><span class="dot dot-purple"></span> low</td>
				<td><?php $day_count= $day_count + 1;?>1</td>
			</tr>
			<tr class="pending">
				<td></td>
				<td>Deanna and I were adding new samples to infotrack and we both ended up getting assigned MD4429 for our separate orders. I was able to submit mine first so infotrack stopped her from using the same MD#. It cleared everything that she had entered and bumped her up to MD4430. I am just wondering if there is a way to prevent infotrack from assigning the same MD# once someone is already in the "add sample page". This way more than one person can be working on inputting samples to infotrack without having to wait for the other to claim the next MD#. Daniel Bach at 2019-10-17 09:14:25</td>
				<td>
					<ol>
						<li>Requirements:<span class="dot dot-purple"></span></li>
						<li>Design:<span class="dot dot-purple"></span></li>
						<li>List Risks:<span class="dot dot-purple"></span></li>
						<li>Implementation:<span class="dot dot-purple"></span></li>
						<li>Testing:<span class="dot dot-purple"></span></li>

					</ol>
				</td>
				<td></td>
				<td><span class="dot dot-purple"></span> low</td>
				<td><?php $day_count= $day_count + 5;?>5</td>
			</tr>
			<tr class="pending">

				<td></td>
				<td>Compare NGS data including controls.  Maybe a venn diagram would be best.</td>
				<td>
					<ol>
						<li>Requirements:<span class="dot dot-purple"></span></li>
						<li>Design:<span class="dot dot-purple"></span></li>
						<li>List Risks:<span class="dot dot-purple"></span></li>
						<li>Implementation:<span class="dot dot-purple"></span></li>
						<li>Testing:<span class="dot dot-purple"></span></li>

					</ol>
				</td>
				<td></td>
				<td><span class="dot dot-purple"></span> low</td>
				<td><?php $day_count= $day_count + 5;?>5</td>
			</tr>
			<tr class="pending">

				<td></td>
				<td>Add IGV image file for variant QC</td>
				<td>
					<ol>
						<li>Requirements:<span class="dot dot-purple"></span></li>
						<li>Design:<span class="dot dot-purple"></span></li>
						<li>List Risks:<span class="dot dot-purple"></span></li>
						<li>Implementation:<span class="dot dot-purple"></span></li>
						<li>Testing:<span class="dot dot-purple"></span></li>

					</ol>
				</td>
				<td>Request more space before these can be uploaded or get space on a drive just for uploads.  This is best.</td>
				<td><span class="dot dot-purple"></span> low priority</td>
				<td><?php $day_count= $day_count + 1;?>1</td>
			</tr>
			<tr class="pending">
				<td></td>
				<td>Feature add: Make extraction sheet editable</td>
				<td>
					
				</td>
				<td>					
					
				</td>
				<td><span class="dot dot-purple"></span></td>
				<td><?php $day_count= $day_count + 2;?>2</td>
			</tr>
<?php
	require_once('templates/dev_designs/sam/completed.php');
?>			
		</tbody>
	</table>
	<h2>Total Number of Days: <?= $day_count;?></h2>
	<h2>Estimated number of months left on task list <?= round($day_count/10.5,2);?></h2>
</div>