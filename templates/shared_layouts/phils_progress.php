<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
	<h1>Phil's Progress</h1>
<?php $day_count=0;?>
	<table class="formated_table">
		<thead>
			<tr>
				<th>Start Date</th>
				<th>End Date</th>
				<th>Task</th>
				<th>Progress</th>
				<th>Status</th>
				<th>Estimated # days</th>
			</tr>
		</thead>
		<tbody>
			<tr class="completed">
				<td>Nov. 8th 2019</td>
				<td>Nov. 8th 2019</td>
				<td>Demo of <?= SITE_TITLE;?></td>
				<td>Complete</td>
				<td><span class="dot dot-green"></span></td>
				<td></td>
			</tr>
			<tr class="completed">
				<td>Nov. 8th 2019</td>
				<td>Nov. 8th 2019</td>
				<td>Develop a Design</td>
				<td>Phil came up with a very cool design.  It will allow for both an in application training module, reference pop-ups throughout the software to help users, and a way to compile all of the data into a PDF/word for an official document.</td>
				<td><span class="dot dot-green"></span></td>
				<td></td>
			</tr>
			<tr class="completed">
				<td>Nov. 8th 2019</td>
				<td></td>
				<td>
					Merge different versions of the existing SOPs into 1 updated master version written in Markdown
				</td>
				<td>
					Samantha was having issues with consistency of word document formating and was in the middle of merging an old document into a correctly formated SOP when Phil took over the project. As of November 8th 2019 about 3/4 of the first document is now in Markdown. 
				</td>
				<td><span class="dot dot-yellow"></span></td>
				<td></td>
			</tr>
			<tr class="completed">
				<td>12/5/2019</td>
				<td>12/5/2019</td>
				<td>Finished MyPath training</td>
				<td></td>
				<td><span class="dot dot-green"></span></td>
				<td></td>
			</tr>
			<tr class="completed">
				<td>12/5/2019</td>
				<td>12/5/2019</td>
				<td>Attended OFA planning meeting, learning about NGS and software process, wrote documentation for the newly proposed automated data transfer schema.</td>
				<td></td>
				<td><span class="dot dot-green"></span></td>
				<td></td>
			</tr>
			<tr class="completed">
				<td>12/5/2019</td>
				<td>12/8/2019</td>
				<td>Meeting notes 12/5/2019</td>
				<td>
					<a href="?page=dev_designs&dev=phil&design_num=1&task=meeting_notes_12_5_19" type="button" class="btn btn-info btn-info-hover">
		                   	Meeting notes 12/5/2019
		             </a>
		        	</td>
				<td><span class="dot dot-green"></span></td>
				<td></td>
			</tr>
			<tr class="completed">
				<td></td>
				<td></td>
				<td>Get ID and Flu Shot</td>
				<td></td>
				<td><span class="dot dot-green"></span></td>
				<td></td>
			</tr>
			<tr class="completed">
				<td></td>
				<td></td>
				<td>Create XML schema to organize SOP markdown files</td>
				<td></td>
				<td><span class="dot dot-green"></span></td>
				<td></td>
			</tr>
			<tr class="completed">
				<td></td>
				<td></td>
				<td>Create JS framework to apply schema to Markdown files in repository</td>
				<td></td>
				<td><span class="dot dot-green"></span></td>
				<td></td>
			</tr>
			<tr class="completed">
				<td></td>
				<td></td>
				<td>Create XSLT to parse SOP into printer-friendly format</td>
				<td></td>
				<td><span class="dot dot-green"></span></td>
				<td></td>
			</tr>
			<tr class="completed">
				<td></td>
				<td>1/19/2020</td>
				<td>Create JS and CSS to integrate markdown into "training" functionality in the application</td>
				<td></td>
				<td><span class="dot dot-green"></span></td>
				<td></td>
			</tr>
			<tr class="completed">
				<td></td>
				<td></td>
				<td>Create JS and CSS to make PDF of document</td>
				<td></td>
				<td><span class="dot dot-green"></span></td>
				<td></td>
			</tr>
			<tr class="pending">
				<td></td>
				<td></td>
				<td>Update all images and wording to reflect current version of software.</td>
				<td></td>
				<td><span class="dot dot-yellow"></span></td>
				<td>approx 2 more days</td>
			</tr>
			<tr class="pending">
				<td>1/23/2020</td>
				<td></td>
				<td>Phil will use the current SOP and go thru all of the steps.  Updating images and text as he goes</td>
				<td></td>
				<td><span class="dot dot-purple"></span></td>
				<td>4</td>
			</tr>
			<tr class="pending">
				<td></td>
				<td></td>
				<td>Finish OFA section of SOP</td>
				<td></td>
				<td><span class="dot dot-purple"></span></td>
				<td></td>
			</tr>
			<tr class="pending">
				<td></td>
				<td></td>
				<td>Add Myeloid Section to SOP.  Meet with Bill to obtain all current tasks.</td>
				<td></td>
				<td><span class="dot dot-purple"></span></td>
				<td></td>
			</tr>
			<tr class="pending">
				<td></td>
				<td></td>
				<td>Help optimize loading speed of MD Infotrack.  Especially sample log book.</td>
				<td></td>
				<td><span class="dot dot-purple"></span></td>
				<td></td>
			</tr>
			<tr class="pending">
				<td></td>
				<td></td>
				<td>Add new versions since commit cf036d6</td>
				<td></td>
				<td><span class="dot dot-purple"></span></td>
				<td></td>
			</tr>
			<tr class="pending">
				<td></td>
				<td></td>
				<td>Perform a code review of MD Infotrack</td>
				<td></td>
				<td><span class="dot dot-purple"></span></td>
				<td></td>
			</tr>
			<tr class="pending">
				<td></td>
				<td></td>
				<td>Perform a code review of MD Infotrack</td>
				<td></td>
				<td><span class="dot dot-purple"></span></td>
				<td></td>
			</tr>
			<tr class="pending">
				<td></td>
				<td></td>
				<td>Perform a security audit</td>
				<td></td>
				<td><span class="dot dot-purple"></span></td>
				<td></td>
			</tr>
			<tr class="pending">
				<td></td>
				<td></td>
				<td>Setup cronjob to automatically dump database to box.</td>
				<td></td>
				<td><span class="dot dot-purple"></span></td>
				<td></td>
			</tr>
		</tbody>
	</table>
	<h2>Total Number of Days: <?= $day_count;?></h2>
	<h2>Estimated number of months left on task list <?= round($day_count/4.5,2);?></h2>
</div>