<div class="row" style="overflow-x:auto;">

     <table class="formated_table sort_table">
          <thead>
          		<!-- // include as the first column include in report -->
          		<?php                     
					// if the page is show_edit_report do not include
          			if ($page !== 'show_edit_report'  && $page !== 'check_tsv_upload')
          			{
	              	?>
          			<th>Include in Report</th>
          		<?php
          			}

                         if ($page !== 'check_tsv_upload'  && $page !== 'show_edit_report')
                         {
                    ?>
                              <th>SNV in Knowledge Base</th>
                              <th><span class="fas fa-comment"></span></th>
                         <?php
                              // Strand bias is not on for every panel see if
                              // it is turned on.  If so add to table
                              if   (
                                        isset($onPanelStepRegulators[0]['on_panel_steps']) &&
                                        strpos($onPanelStepRegulators[0]['on_panel_steps'],'strand_bias_calc') !== False
                                   )
                              {
                         ?>
                              <th>Strand Bias</th>
                    <?php
                              } // close strand bias
                    ?>

                              <th title="User classification">Classification</th>
                              <th title="Count of each time the variant was reported out for each tier.  For more information about reports with tiers go to table at the bottom of page.">Past Tiers</th>
                    <?php
                         } 
          		?>
                              
          	<?php
               
                    if (isset($cols) && !empty($cols))
                    {

               		foreach ($cols as $key => $col_name)
               		{

               			$col_name = str_replace('_status', '', $col_name);
               			// if the column isn't an id add column as visible otherwise hide column
                             
                              if (!$utils->InputAID($col_name) && $col_name != 'time_stamp' && $col_name != 'include_in_report' && $col_name != 'confirm' && $col_name != 'tier' && $col_name == 'frequency' )
                              {
                                      
               ?>

                                   <th>VAF</th>
               <?php
                              }

               			else if (!$utils->InputAID($col_name) && $col_name != 'time_stamp' && $col_name != 'include_in_report' && $col_name != 'confirm' && $col_name != 'tier' )
               			{

               ?>
               				<th><?= $utils->UnderscoreCaseToHumanReadable($col_name);?></th>
          	<?php
               			}
               			else
               			{
          	?>
               				<th class="d-none" id="<?= $col_name;?>_th"><?= $col_name;?></th>
          	<?php
               			}
                         }
          		}
          	?>
                                   
          </thead>
          <tbody>
<?php

     $knowledge_ids_for_all = True;
     if (isset($variantArray) && $num_snvs !== 0)
     {  
         
          for ($i = 0; $i < sizeOf($variantArray); $i++)
          {
               /////////////////////////////////////////////////////////
               // If knowledge ID was not found for any sample remove nav buttons
               /////////////////////////////////////////////////////////   
               if ($variantArray[$i]['knowledge_id'] == '')
               {
                    $knowledge_ids_for_all = False;
               }
?>
               <tr id ="tr_var_id_<?= $variantArray[$i]['observed_variant_id'];?>" data-scroll-knowledge-id="knowledge_id_<?= $variantArray[$i]['knowledge_id']; ?>">
<?php 
               ////////////////////////////////////////////////////////////////
               // Add include in report button
               ////////////////////////////////////////////////////////////////
               if ($page !== 'show_edit_report'&& $page !== 'check_tsv_upload')
               {
?>
                    <td>
                         <input id="toggle_include_variant_<?= $variantArray[$i]['observed_variant_id'];?>" type="checkbox" name="include_in_report" value="1" <?php if($variantArray[$i]['include_in_report'] == 1) echo 'checked';?>/>
                    </td>
<?php
               
                    ////////////////////////////////////////////////////
                    // Add a column to either add variants which are not in 
                    // knowledge_base_table or scroll to variant depending on 
                    // if knowledge_id is present
                    ////////////////////////////////////////////////////
                    require('templates/shared_layouts/snv_in_knowledge_base_cell.php');

                    ////////////////////////////////////////////////////
                    // add comment modal button
                    ////////////////////////////////////////////////////

                    require('templates/shared_layouts/snv_comment_modal_btn_cell.php');

                    //////////////////////////////////////////////////
                    // Add strand bias column if it is not turned off for panel.  (ngs_panel_step_regulator_table)
                    //////////////////////////////////////////////////
                    if   (
                              isset($onPanelStepRegulators[0]['on_panel_steps']) &&
                              strpos($onPanelStepRegulators[0]['on_panel_steps'],'strand_bias_calc') !== False
                         )
                    {
                         $allele_with_SB = $utils->strandOddsRatio($variantArray[$i]);
                         $sb = round($allele_with_SB['strand_bias'],4);
                         
                         // strand bias color
                         if ($sb == 0)
                         {
                              $sb_class = 'alert-na-qc';
                              $sb = 'QC not done';
                              $sb_color = 'black';
                              $sb_bg_color = 'grey';
                         }

                         else if ((int)$allele_with_SB['strand_bias'] > 4.0)
                         {
                              $sb_class = 'alert-flagged-qc';
                              $sb_bg_color = '#ac2925';
                              $sb_color = 'white';
                         }                        
                         else
                         {
                              $sb_class = 'alert-passed-qc';
                              $sb_bg_color = '#0a9601';
                              $sb_color = 'white';
                         }
?>
                    <td class="<?= $sb_class;?>"><?= $sb;?></td>
<?php
                    } // turn off strand bias column depending on ngs_panel_step_regulator_table

                    ////////////////////////////////////////////////////
                    // Add previous classification or add a drop down menu
                    // to include a classification
                    ////////////////////////////////////////////////////
                    $classification_array = array(
                         'genes'   => $variantArray[$i]['genes'],
                         'coding'   => $variantArray[$i]['coding'],
                         'amino_acid_change'   => $variantArray[$i]['amino_acid_change'],
                         'panel'   => $visitArray[0]['ngs_panel']
                    );
                    
                    // Once a user adds a classification to add another classification there will be a toggle where the dropdown is hidden most of the time
                    
                    $dropDownHTML = '<select class="change-classification select-right-open" data-genes="'.$variantArray[$i]['genes'].'" data-amino_acid_change="'.$variantArray[$i]['amino_acid_change'].'" data-coding="'.$variantArray[$i]['coding'].'" data-panel="'.$visitArray[0]['ngs_panel'].'">';
                    $dropDownHTML.= '<option value="" disabled selected>-------</option>';
                    $dropDownHTML.= '<option>Pathogenic</option>';
                    $dropDownHTML.= '<option>Benign</option>';
                    $dropDownHTML.= '<option>Presumed Benign</option>';
                    $dropDownHTML.= '<option>Unknown Significance</option>';
                    $dropDownHTML.= '<option>Likely Seq error - low Read Depth</option>';
                    $dropDownHTML.= '<option>Seq error</option>';
                    $dropDownHTML.= '</select>';

                    if (!empty($variantArray[$i]['classification']))
                    {    

                         echo $utils->toggleDropDownMenu($variantArray[$i]['classification'], 'classification', $i, $dropDownHTML);                            
                    }

                    // add a select menu under an ajax call 
                    else
                    {
?>
                         <td>
                              <?= $dropDownHTML;?>
                         </td>
<?php
                    }

                    ////////////////////////////////////////////////////
                    // Add previous tiers 
                    ////////////////////////////////////////////////////
                    if ($variantArray[$i]['knowledge_id'] !== null)
                    {
                         $past_tiers = $db->listAll('tier-knowledge-id-overview', $variantArray[$i]['knowledge_id']);  
                    }                 
                    
                    if ($variantArray[$i]['knowledge_id'] === null || empty($past_tiers))
                    {
?>
                         <td>None</td>
<?php
                    }  
                    else
                    {
                         $tier_str = '';
                         foreach ($past_tiers as $key => $tier_count)
                         {
                              if ($tier_str !== '')
                              {
                                   $tier_str.=', ';
                              }

                              $tier_str.= '<b>'.$tier_count['tier'].'</b>:'.$tier_count['count'];
                         }
?>
                         <td><?= $tier_str;?></td>
<?php
                    } 
               }

               ////////////////////////////////////////////////////
               // Columns which are part of the observed variant table can
               // now be added
               ////////////////////////////////////////////////////

               if (isset($cols) && !empty($cols))
               {
                    foreach ($cols as $key => $col_name)
                    {

                         ////////////////////////////////////////////////////
                         // Do not add columns which are ids, time_stamp, etc.
                         ////////////////////////////////////////////////////
                         if ($col_name == 'genes')
                         {
?>
                              <td><?= !$variantArray[$i]['activate_status'] ? '<span class="alert-danger fas fa-exclamation-circle" title="Gene Not included in Assay.  If you would like to remove it from the final report uncheck include in report"></span>':'';?> <?= $variantArray[$i]['genes'];?></td>
<?php                              
                         }
                         else if ($col_name == 'frequency' && floatval($variantArray[$i][$col_name]) < 5)
                         {
?>
                              <td class="alert-flagged-qc"> <?= $variantArray[$i][$col_name]; ?>
                              </td>
<?php                                             
                         }
                         else if ($col_name == 'frequency' && floatval($variantArray[$i][$col_name]) >= 5)
                         {
?>
                              <td class="alert-passed-qc"> <?= $variantArray[$i][$col_name]; ?>
                              </td>
<?php                                             
                         }
                         else if (!$utils->InputAID($col_name) && $col_name != 'time_stamp' && $col_name != 'include_in_report' && $col_name != 'confirm_status' && $col_name != 'tier' )
                         {
                              ////////////////////////////////////////////////////
                              // Larger strings need to be toggled so more information
                              // can fit in smaller places
                              ////////////////////////////////////////////////////
                              $cell_val = $variantArray[$i][$col_name];
                              if (strlen($cell_val) > 20)
                              {
                                   
                                   // get the first 20 char of string
                                   $substr_cell_val = substr($cell_val, 0, 20);
?>
                                   <td> 
                                        <span id="teaser_<?= $col_name;?>_<?= $variantArray[$i]['observed_variant_id']?>" class="teaser">
                                             <?= $substr_cell_val;?>
                                        </span>

                                        <span id="complete_<?= $col_name;?>_<?= $variantArray[$i]['observed_variant_id']?>" class="complete  d-none">
                                             <?= $cell_val;?>                    
                                        </span>

                                        <span id="show_complete_<?= $col_name;?>_<?= $variantArray[$i]['observed_variant_id']?>" class="show-complete">
                                             more ...                  
                                        </span>

                                        <span id="show_teaser_<?= $col_name;?>_<?= $variantArray[$i]['observed_variant_id']?>" class="show-teaser d-none">
                                             less ...                  
                                        </span>
                                   </td>
<?php

                              }

                              ////////////////////////////////////////////////////
                              // Smaller strings can just be added directly to the cell
                              ////////////////////////////////////////////////////
                              else
                              {
?>
                                   <td><?= $cell_val;?></td>
<?php
                              }
                         }
                         else
                         {
?>
                                   <td class="d-none"><?= $cell_val;?></td>
<?php 
                         }
                    }
?>

<?php 
               }
?>                  
               </tr>
<?php
          }
     }
?>
          </tbody>
	</table>
</div>	