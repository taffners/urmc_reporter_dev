<?php
	// The OFA and myeloid panel have different pool steps find which panel
	// is being used and adjust poolsteps accordingly 
	if (isset($_GET['ngs_panel_id']) && $_GET['ngs_panel_id'] === '1')
	{
		// ofa steps
		$poolSteps = array(
			'pool_dna_conc',
			'pool_amp_conc',
			'pool_make_run_template',
			'pool_upload_samples'
		);

		// The above steps are minimum but adding pcr templates and lots needs too 
		// happen in the future.

		$poolStepTracker = new StepTracker($db, $poolSteps, 'pool_steps_table');
	}
	else if (isset($_GET['ngs_panel_id']) && $_GET['ngs_panel_id'] === '2')
	{
		// myeloid steps
		$poolSteps = array(
			'pool_dna_conc',
			'pool_dilution_worksheet',
			'pool_amp_conc',			
			'pool_make_run_template', // sbsuser pass danryan#1
			'pool_upload_samples'
		);
		$poolStepTracker = new StepTracker($db, $poolSteps, 'pool_steps_table');
	}
	



	if (isset($_GET['pool_chip_linker_id']))
	{

		$completed_steps = $db->listAll('all-pool-complete-steps', $_GET['pool_chip_linker_id']);

		if (isset($poolStepTracker))
		{
			$previous_step = $poolStepTracker->FindPreviousStep($page);
			$step_status = $poolStepTracker->StepStatus($completed_steps, $page);
			$step_status = $stepTracker->StepStatus($completed_steps, $page);
			$next_step = $stepTracker->FindNextStep($page);
		}

		$pending_pools = $db->listAll('curr-pending-pools', $_GET['pool_chip_linker_id']);
		$visits_in_pool = $db->listAll('visits-in-pool', $_GET['pool_chip_linker_id']);
	
	}

?>