<?php
if (strpos($user_permssions, 'NGS') !== false && strpos($user_permssions, 'View_Only') === false)
{
?>
	<div class="float-right">
		<label class="form-control-label"  >Add NGS: </label>
		<a href="?page=add_visit&sample_type=control" class="btn btn-primary btn-primary-hover" title="Add an NGS Control"> Add NGS Control</a>
		<a href="?page=add_library_pool" class="btn btn-primary btn-primary-hover">Add NGS Library Pool</a>
	</div>
<?php
}
?>