<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
					<table class="formated_table sort_table">
						<thead id="single_analyte_pools">
							<th>Run #</th>
							<th>Full Run #</th>
							<th>Test Name</th>
							<th>Technician</th>
							<th>Start Date</th>
							<th>Comments</th>
					<?php
						if (isset($user_permssions) && strpos($user_permssions, 'log_book') !== false &&  strpos($user_permssions, 'View_Only') === false)
						{
							if ($page === 'new_home')
							{
					?>
							<th class="d-print-none">Update</th>
						<?php
							}
							else
							{
						?>
							<th class="d-print-none">Go to pool</th>
						<?php
							}
							if ($page === 'new_home')
							{
						?>
							<th>Cancel</th>
					<?php
							}
							else
							{
						?>
							<th>Status</th>
						<?php
							}
						}
					?>
					<?php
						if (isset($user_permssions) && strpos($user_permssions, 'edit_data') !== false)
						{
					?>
							<th>Update Run Number</th>

					<?php
						}
					?>
						</thead>			

						<tbody>
				<?php

					if (isset($single_analyte_pools) && !empty($single_analyte_pools))
					{
						foreach ($single_analyte_pools as $key => $pool)
						{
				?>
							<tr>
								<td>
								<?php
									if (!empty($pool['run_number']))
									{
								?>
										<?= $pool['run_number'];?>
								<?php
									}									
								?>
								</td>
								<td>
								<?php
									if (!empty($pool['run_number']))
									{
								?>
										<?= $pool['run_number'].'-'.date('y', strtotime($pool['start_date']));?>
								<?php
									}
									
								?>
								</td>		
								<td><?= $pool['test_name'];?></td>
								<td><?= $pool['user_name'];?></td>
								<td><?= $pool['start_date'];?></td>
								<td>
									<?= $pool['comments'];?> 
									
									<?php
									if (isset($pool['all_assigned_directors']) && !empty($pool['all_assigned_directors']))
									{
										if (!empty($pool['comments']))
										{
											echo '<br>';
										}
									?>
										Assigned Director(s): <?= $pool['all_assigned_directors'];?>
									<?php
									}

									?>
								</td>
						<?php
							if (isset($user_permssions) && strpos($user_permssions, 'log_book') !== false &&  strpos($user_permssions, 'View_Only') === false)
							{
						?>
								<td class="d-print-none">
									<a href="?page=single_analyte_pool_home&single_analyte_pool_id=<?= $pool['single_analyte_pool_id'];?>" class="btn btn-primary btn-primary-hover" title="Open Pool"> <i class="fas fa-hand-point-right"></i></a>
								</td>
								<?php
								if ($page === 'new_home')
								{
								?>
								<td><a href="?page=cancel_single_analyte_pool&single_analyte_pool_id=<?= $pool['single_analyte_pool_id'];?>" class="btn btn-danger btn-danger-hover" title="Cancel Pool"> <span class="fa fa-times"></span></a></td>
								<?php
								}
								else
								{
								?>
								<td><?= $pool['status'];?></td>
								<?php
								}
								?>
						<?php
							}
						?>	
						<?php
							if (isset($user_permssions) && strpos($user_permssions, 'edit_data') !== false)
							{
						?>
								<td>
									<a href="?page=update_run_number_single_analyte_pool&single_analyte_pool_id=<?= $pool['single_analyte_pool_id'];?>" class="btn btn-danger btn-danger-hover" title="Update Run Number"> Update Run Number</a>
								</td>

						<?php
							}
						?>
							</tr>
				<?php
						}
					}
				?>
						</tbody>
					</table>
				</div>