<div class="row">
	<div class="offset-xs-4 offset-sm-4 offset-md-4 offset-lg-4 col-xs-2 col-sm-2 col-md-2 col-lg-2">
		<button type="submit" id="<?= $page;?>_submit" name="<?= $page;?>_submit" value="Submit"  class="btn btn-primary btn-primary-hover <?php
		if ($_GET['page'] !== 'review_report')
		{
			echo 'disabled-link';
		}
		?>">
			Submit
			
		</button>
	</div>
	<div class="offset-xs-4 offset-sm-4 offset-md-4 offset-lg-4 col-xs-2 col-sm-2 col-md-2 col-lg-2">
		<a href="?page=home" class="btn btn-primary btn-primary-hover" role="button">
			<img src="images/house.png" style="height:23px;width:23px;margin-right:5px;" alt="house icon">
			Home
		</a>
	</div>
</div>