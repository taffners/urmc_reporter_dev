	<div id="report-nav" class="hidden-xs col-sm-2 col-md-2 col-lg-2 d-print-none">
		<div class="vertical-nav">
			<ul class="affix list-unstyled">	
				<li role="presentation" class="vertical-nav-buttons nav-buttons">
					<a href="?page=pool_summary&pool_chip_linker_id=<?= $_GET['pool_chip_linker_id'];?>&ngs_panel_id=<?= $_GET['ngs_panel_id'];?>" class="btn btn-primary btn-primary-hover" role="button">
						Pool Summary
					</a>
				</li>
				<li class="vertical-nav-buttons nav-buttons">
					<a href="?page=stop_library_pool&pool_chip_linker_id=<?= $_GET['pool_chip_linker_id'];?>&ngs_panel_id=<?= $_GET['ngs_panel_id'];?>" class="btn btn-danger btn-primary-danger" role="button">
						Stop Library Pool 
						<span class="fas fa-times"></span>
					</a>
				</li>
		<?php
			// add a link for all the current steps in $poolStepTracker
			$steps = $poolStepTracker->GetSteps();
			foreach ($steps as $key => $value)
			{
		?>
				<li role="presentation" class="vertical-nav-buttons nav-buttons">

					<a href="?page=<?= $value;?>&pool_chip_linker_id=<?= $_GET['pool_chip_linker_id'];?>&ngs_panel_id=<?= $_GET['ngs_panel_id'];?>" class="btn <?php 

							echo $poolStepTracker->ButtonStatus($completed_steps, $value);
					?>" role="button">
					<?= $utils->UnderscoreCaseToHumanReadable($value);?>
					</a>
				</li>
		<?php				
			}
		?>

			</ul>
		</div>
	</div>