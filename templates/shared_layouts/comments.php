<?php
if (isset($all_comments) && !empty($all_comments))
{
?>
			<div class="row">
				<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
					<label for="comments" class="form-control-label">Problems/Comments/Notes:</label>
				</div>
				<div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">
					<ul>
					<?php
					foreach ($all_comments as $key => $comment)
					{
					?>
						<li><?= $comment['comment'];?> <?= $comment['user_name'];?> <?= $comment['comment_time_stamp'];?> </li>
					<?php
					}
					?>
					</ul>

					
				</div>
			</div>	
<?php
}
?>
			