<?php										
										// Add summary of completed test info
										$summary = '';
										if (isset($test['test_result']) && !empty($test['test_result']))
										{
											$summary.= 'Test Result: '.$test['test_result'].'<br>';
										}

										// Add hotspot info
										if (
												isset($test['hotspot_variants']) && 
												$test['hotspot_variants'] === 'yes' && 
												isset($test['hotspot_type'])
											)
										{
											$hotspots_array = $db->listAll('observed-hotspots', $test['ordered_test_id']);
											
											if (!empty($hotspots_array))
											{											
												
												$hotspots = "";
												$len_array = sizeOf($hotspots_array);

												for ($i=0; $i < $len_array; $i++)
												{
													if (!empty($hotspots))
													{
														$hotspots.=',<br>';
													}
													
													$hotspots.= '<b>'.$hotspots_array[$i]['ordered_test_genetic_call'].'</b>';

													if (!empty($hotspots_array[$i]['observed_val']))	
													{
														$hotspots.= ' <i>Size</i>: '.$hotspots_array[$i]['observed_val'];
													}
													
													if (!empty($hotspots_array[$i]['observed_control_val']))	
													{
														$hotspots.= ' <i>Normal</i>: '.$hotspots_array[$i]['observed_control_val'];
													}
												} 

												echo $hotspots.'<br>';
											}
										}


										// Call the db to find any users_performed_task_table.
										// Since there will not be many tests per sample this should be ok to call the db on each completed test.  It would be better to add to initial $testArray however, this will also make it very complicated because there are multiple types of ref tables represented in users_performed_task_table.  There's ordered_test_table ref and extraction_log_table ref

										// Add NGS test info
										if ($test['mol_num_required'] === 'yes' && $test['test_status'] === 'completed')
										{
											// since wet bench is a required input I will obtain visit_id for 
											// a summary of the rest of the steps from here
											$wetbench = $db->listAll('users-wet-bench-ngs-tasks', $test['ordered_test_id']);

											if (!empty($wetbench))
											{

												// add link to report which hides when printed
												$summary.='<a href="?page=download_report&amp;visit_id='.$wetbench[0]['visit_id'].'&patient_id='.$wetbench[0]['patient_id'].'&run_id='.$wetbench[0]['run_id'].'&ngs_panel_id='.$wetbench[0]['ngs_panel_id'].'" type="button" class="btn btn-success btn-success-hover d-print-none"><img src="images/adobe-pdf-icon.png" alt="pdf icon" style="height:30px;width:30px"></a><br>';

												$summary.='<b>Wet Bench:</b> <br>';
												foreach ($wetbench as $key => $tech)
												{
													$summary.=$tech['user_name'].'<br>';
												}

												$completed_steps = $db->listAll('all-analysis-techs', $wetbench[0]['visit_id']);
												
												$summary.='<b>Analysis:</b> <br>';

												$included_techs = '';

												foreach ($completed_steps as $key => $step)
												{

													$summary.= $step['user_name'].'<br>';
												}	
											}				
										}
										elseif ($test['extraction_info'] === 'no' && $test['quantification_info'] === 'no')
										{
											$non_extraction_users = $db->listAll('users-performed-tasks', $test['ordered_test_id']);
											foreach ($non_extraction_users as $key => $task_user)
											{
												$summary.= $task_user['task'].':'.$task_user['users'].'<br>';
											}

											if (isset($non_extraction_users[0]['instruments_used']) && !empty($non_extraction_users[0]['instruments_used']))
											{
												$summary.= 'Instrument used: '.$non_extraction_users[0]['instruments_used'].'<br>';
											}
										}

										// Add extraction tests
										else
										{
											$extraction_users = $db->listAll('extraction-users-tasks', $test['ordered_test_id']);

											if (isset($extraction_users[0]['extraction_method']) && !empty($extraction_users[0]['extraction_method']))
											{
												$summary.= 'Extraction Method: '.$extraction_users[0]['extraction_method'].'<br>';
											}

											if (isset($extraction_users[0]['quantification_method']) && !empty($extraction_users[0]['quantification_method']))
											{
												$summary.= 'Quant Method: '.$extraction_users[0]['quantification_method'].'<br>';
											}

											if (isset($extraction_users[0]['dna_conc_control_quant']) && !empty($extraction_users[0]['dna_conc_control_quant']))
											{
												$summary.= 'Quant Control: '.$extraction_users[0]['dna_conc_control_quant'].'ng/&mu;l<br>';
											}

											if (isset($extraction_users[0]['stock_conc']) && !empty($extraction_users[0]['stock_conc']))
											{
												$summary.= 'Stock Conc.: '.$extraction_users[0]['stock_conc'].'<br>';
											}

											if (isset($extraction_users[0]['dilution_exist']) && !empty($extraction_users[0]['dilution_exist']))
											{
												$summary.= 'Dilution Exists? '.$extraction_users[0]['dilution_exist'].'<br>';
											}

											if (isset($extraction_users[0]['dilutions_conc']) && !empty($extraction_users[0]['dilutions_conc']))
											{
												$summary.= 'Actual Dilution Conc.: '.$extraction_users[0]['dilutions_conc'].'<br>';
											}

											if (isset($extraction_users[0]['times_dilution']) && !empty($extraction_users[0]['times_dilution']))
											{
												$summary.= 'X Dilution: '.$extraction_users[0]['times_dilution'].'<br>';
											}

											if (isset($extraction_users[0]['purity']) && !empty($extraction_users[0]['purity']))
											{
												$summary.= '260/280: '.$extraction_users[0]['purity'].'<br>';
											}


											if (	
													!empty($extraction_users[0]['vol_dna']) && //v1
													!empty($extraction_users[0]['stock_conc']) && // c1
													!empty($extraction_users[0]['vol_eb']) && // 
													!empty($extraction_users[0]['dilution_final_vol']) && // v2
													!empty($extraction_users[0]['target_conc']) // c2
												)
											{
												$summary.='Dilution Calc C1 V1 = C2 V2: <br>';

												$summary.= '('.$extraction_users[0]['stock_conc'].')('.$extraction_users[0]['vol_dna'].') = ('.$extraction_users[0]['target_conc'].')('.$extraction_users[0]['dilution_final_vol'].')<br>';

												
												$summary.='EB Volume V2 - V1:<br>';

												$summary.= $extraction_users[0]['dilution_final_vol'].' - '.$extraction_users[0]['vol_dna'].' = '.$extraction_users[0]['vol_eb'].'<br>';
											}



											if (isset($extraction_users[0]['instruments_used']) && !empty($extraction_users[0]['instruments_used']))
											{
												$summary.= 'Instrument used: '.$extraction_users[0]['instruments_used'].'<br>';
											}

											foreach ($extraction_users as $key => $task_user)
											{
												$summary.= $task_user['task'].':'.$task_user['users'].'<br>';
											}
										}

										// add extra fields
										$extra_fields = $db->listAll('all-added-extra-fields', array('ref_table'=> 'ordered_test_table', 'ref_id' => $test['ordered_test_id']));
										if (!empty($extra_fields))
										{
											
											$extra_fields_str = '';
											foreach($extra_fields as $key => $field_info)
											{
												if (!empty($field_info['field_val']))
												{
													if ($extra_fields_str !== '')
													{
														$extra_fields_str.='<br> ';
													}

													$extra_fields_str.=$field_info['field_name'].': '.$field_info['field_val'];
												}
											}
											echo $extra_fields_str.'<br>';
										}

										// Add QC info
										if (isset($test['qc_status']) && !empty($test['qc_status']))
										{
											echo 'QC Status: '.$test['qc_status'].'<br>';
										}
										if (isset($test['qc_overview']) && !empty($test['qc_overview']))
										{
											echo 'QC overview: '.$test['qc_overview'].'<br>';
										}

										// Add any comments added under ordered_test_table
										if ($test['mol_num_required'] === 'yes' && $test['test_status'] === 'completed')
										{
											$summary.= '<b>Comments:</b> Click PDF icon to obtain comments';
										}
										else
										{
											$comments = $db->listAll('ordered-test-concat-comments', $test['ordered_test_id']);
											if (isset($comments) && !empty($comments))
											{

												$summary.= '<b>Comments:</b><br>';
												foreach ($comments as $key => $comment)
												{	
													$summary.=$comment['problems'].'<br>';
												}
											}	
										}
																				
?>
										<?=$summary;?>
<?php
										// If the sample was part of a single analyte pool add a button to view the pool details
										$single_analyte_pool = $db->listAll('test-in-single-analyte-pool', $test['ordered_test_id']);

										if (isset($single_analyte_pool[0]['single_analyte_pool_id']))
										{							
?>
										<a href="?page=single_analyte_pool_home&single_analyte_pool_id=<?=$single_analyte_pool[0]['single_analyte_pool_id'];?>&view_only=yes" class="btn btn-primary btn-primary-hover d-print-none" title="View all samples in pool"> View Pool</a>
<?php
										}
?>										