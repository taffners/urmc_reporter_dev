<?php 
	if (isset($faqs) && !empty($faqs))
	{	
?>
	<div class="row">

		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			<h3 class="raised-card-subheader">Frequently Asked questions</h3>
	<?php
		foreach ($faqs as $subheader => $questions)
		{
	?>
			<h3><?= $subheader;?></h3>
			<ul class="hover-card-list" style="font-size:20px;">


	<?php
			foreach($questions as $quest_id => $quest)
			{
	?>
				<li>
					<a href="?page=training&training_step=FAQ#<?= $quest_id;?>">
						<?= $quest['Question'];?>
					</a>
				</li>
	<?php
			}
	?>
			</ul>
	<?php
		}
	?>			
			
		</div>
	</div>
<?php
	}
?>	