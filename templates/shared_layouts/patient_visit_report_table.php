<?php
	if (isset($sample_type_toggle[0]['sample_type_toggle']) && !empty($sample_type_toggle[0]['sample_type_toggle']) && $sample_type_toggle[0]['sample_type_toggle'] !== 'cap_sample')
	{
?>

<div class="row">
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		<div id="patient-timeline-history"></div>
	</div>
</div>
<?php
	}
?>
<hr class="section-mark">

<?php
if 	(isset($visitArray[0]) && ($visitArray[0]['control_type_id'] === null || $visitArray[0]['control_type_id'] == 0) && isset($runInfoArray[0]['report_type']))
{

		$change_btn_types = array(
			'reportable' => 'Change to Validation Report',
			'na' => 'Change to Reportable',
			'validation' => 'Change to Reportable',
			'default' => 'Change to Report type'
		);

		if (isset($runInfoArray[0]['report_type']) && in_array($runInfoArray[0]['report_type'], array_keys($change_btn_types)) )
		{
			$change_btn_name = $change_btn_types[$runInfoArray[0]['report_type']];
		}
		else
		{
			$change_btn_name = $change_btn_types['default'];
		}


?>
<div class="row">
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		<h3>Report Type is: <b><?= ucfirst(htmlspecialchars($runInfoArray[0]['report_type']));?></b>

			<a href="?page=change_report_type&run_id=<?= RUN_ID;?>&visit_id=<?= $_GET['visit_id'];?>&patient_id=<?= $_GET['patient_id'];?>" class="btn btn-primary btn-primary-hover" role="button" title="Change the status of the report.  Live reports will be added to the patient electronic medical record and validation reports will not be added.">
				<?= htmlspecialchars($change_btn_name);?> 
			</a>
		</h3>
	</div>
</div>
<?php
}
?>

<div class="row">
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		<table style="width:100%;">
			<tbody>
				<tr>
					<td style="width:150px;">
						Patient Name:
					</td>
					<td style="width:550px;font-weight:bold;">
						<?= isset($patientArray[0]['patient_name']) ? htmlspecialchars($patientArray[0]['patient_name']): '';?>		
					</td>
					<td  style="width:90px;">
						MRN:
					</td>
					<td style="font-weight:bold;">
						<?= isset($patientArray[0]['medical_record_num']) ? htmlspecialchars($patientArray[0]['medical_record_num']) : '';?>
					</td>
				</tr>
				<tr>
					<td style="width:150px;">
						Age/DOB/Sex:
					</td>
					<td style="width:550px;font-weight:bold;">
						<?php
							if (isset($patientArray[0]['dob']))
							{
								$years_old = $utils->calc_age($patientArray[0]['dob'], $visitArray[0]['time_stamp']);
						 		echo $years_old.' Y '.htmlspecialchars($patientArray[0]['dob']).' '.htmlspecialchars($patientArray[0]['sex']);
							}
							else
							{
								echo '';
							}

						?>	
					</td>
					<td  style="width:90px;">
						Soft Path#:
					</td>
					<td style="font-weight:bold;">
					<?php
						// append block number if not empty
						if (strtotime($visitArray[0]['received']) > strtotime('2020-08-25') && isset($visitArray[0]['block']) && !empty($visitArray[0]['block']))
						{

					?>						
						<?= htmlspecialchars($visitArray[0]['soft_path_num']).'-'.htmlspecialchars($visitArray[0]['block']);?>
					<?php
						}
						else
						{
					?>
						<?= htmlspecialchars($visitArray[0]['soft_path_num']);?>
					<?php
						}
					?>
					</td>
				</tr>						
				<tr>
					<td style="width:150px;">
						Collected:
					</td>
					<td style="width:550px;font-weight:bold;">
						<?= $visitArray[0]['collected'] !== DEFAULT_DATE_TIME ? htmlspecialchars($visitArray[0]['collected']) : '';?>
					</td>
					<td  style="width:90px;">
						Soft Lab#:
					</td>
					<td style="font-weight:bold;">
						<?= htmlspecialchars($visitArray[0]['soft_lab_num']) ;?>
					</td>
				</tr>						
				<tr>
					<td style="width:150px;">
						Received:
					</td>
					<td style="width:550px;font-weight:bold;">
						<?= $visitArray[0]['received'] !== DEFAULT_DATE_TIME ? htmlspecialchars($visitArray[0]['received']) : '';?>
					</td>
					<td  style="width:90px;">
						MD#:
					</td>
					<td style="font-weight:bold;">
						<?= htmlspecialchars($visitArray[0]['mol_num']);?>
					</td>
				</tr>						
				<tr>
					<td style="width:150px;">
						Requesting Physician:
					</td>
					<td style="width:550px;font-weight:bold;">
						<?= htmlspecialchars($visitArray[0]['req_physician']);?>
					</td>
					<td  style="width:90px;">
						Account#:
					</td>
					<td style="font-weight:bold;">
						<?= htmlspecialchars($visitArray[0]['account_num']);?>
					</td>
				</tr>						
				<tr>
					<td style="width:150px;">
						Requesting Location:
					</td>
					<td style="width:550px;font-weight:bold;">
						<?= htmlspecialchars($visitArray[0]['req_loc']);?>
					</td>
					<td  style="width:90px;">
						Chart#:
					</td>
					<td style="font-weight:bold;">
						<?= htmlspecialchars($visitArray[0]['chart_num']);?>
					</td>
				</tr>
				<tr>
	<?php 
			if ($visitArray[0]['ngs_panel'] === 'Myeloid/CLL Mutation Panel')
			{
	?>
					<td style="width:150px;">
						Tested Tissue:
					</td>
					<td style="width:550px;font-weight:bold;">
						<?= htmlspecialchars($visitArray[0]['test_tissue']);?>
					</td>
					<td  style="width:90px;">
						mol#:
					</td>
					<td style="font-weight:bold;">
						<?= htmlspecialchars($visitArray[0]['order_num']);?>
					</td>

	<?php
			}
			else if ($visitArray[0]['ngs_panel'] === 'Oncomine Focus Hotspot Assay')
			{
	?>
					<td style="width:150px;">
						Order#:
					</td>
					<td style="width:550px;font-weight:bold;">
						<?= htmlspecialchars($visitArray[0]['order_num']);?>
					</td>
					<td  style="width:90px;">
						
					</td>
					<td style="font-weight:bold;">
						
					</td>
	<?php
			}
	?>	
				</tr>						
			</tbody>
		</table>

	

	</div>
</div>

					<hr class="section-mark">