<?php
	if (isset($sampleLogBookArray))
	{
?>
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-11 col-lg-11">
				<h1>Sample Information:</h1>
			</div>
			<div class="col-xs-12 col-sm-12 col-md-1 col-lg-1" >
				<a href="?page=add_sample_log_book&sample_log_book_id=<?= $sampleLogBookArray[0]['sample_log_book_id'];?>" class="btn btn-primary btn-primary-hover" aria-label="edit sample log book" title="edit sample log book"> 
					<img src="images/sample_log_book_edit.png" style="margin-left:5px;height:45px;width:45px;" alt="edit sample log book">
				</a>
			</div>
		</div>
		<div class="row" style="font-size: 20px;">
			<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">Patient Name: </div>
			<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
				<b>
				<?= $sampleLogBookArray[0]['first_name'];?> 
				<?= $sampleLogBookArray[0]['last_name'];?>
				</b>
			</div>
			<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">Tissue: </div>
			<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
				<b>
				<?= $sampleLogBookArray[0]['test_tissue'];?> 
				</b>
			</div>
		</div>	
		<div class="row" style="font-size: 20px;">
			<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">DOB:</div> 
			<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
				<b>
					<?= $sampleLogBookArray[0]['dob'];?> 
				</b>
			</div>
			<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">Sex:</div> 
			<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
				<b>
					<?= $sampleLogBookArray[0]['sex'];?> 
				</b>
			</div>
		</div>
		<div class="row" style="font-size: 20px;">
			<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">MRN:</div> 
			<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
				<b>
					<?= $sampleLogBookArray[0]['medical_record_num'];?> 
				</b>
			</div>
			<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">Sample Type:</div> 
			<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
				<b>
					<?= $sampleLogBookArray[0]['sample_type'];?> 
				</b>
			</div>
		</div>
		<div class="row" style="font-size: 20px;">					
			<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">Soft Path#:</div> 
			<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
				<b>
					<?= $sampleLogBookArray[0]['soft_path_num'];?> 
				</b>
			</div>
			<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">WBC Count:</div> 
			<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
				<b>
					<?= $sampleLogBookArray[0]['WBC_count'];?> 
				</b>
			</div>
		</div>
		<div class="row" style="font-size: 20px;">
			<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">Soft Lab#:</div> 
			<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
				<b>
					<?= $sampleLogBookArray[0]['soft_lab_num'];?> 
				</b>
			</div>
			<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">H&amp;E circled:</div> 
			<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
				<b>
					<?= $sampleLogBookArray[0]['h_and_e_circled'];?> 
				</b>
			</div>
		</div>
		<div class="row" style="font-size: 20px;">
			<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">MD#:</div> 
			<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
				<b>
					<?= $sampleLogBookArray[0]['mol_num'];?> 
				</b>
			</div>
			<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">Block:</div> 
			<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
				<b>
					<?= $sampleLogBookArray[0]['block'];?> 
				</b>
			</div>
		</div>
		<div class="row" style="font-size: 20px;">
			<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">Outside Case#:</div> 
			<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
				<b>
					<?= $sampleLogBookArray[0]['outside_case_num'];?> 
				</b>
			</div>
			<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">Date Received:</div> 
			<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
				<b>
					<?= $sampleLogBookArray[0]['date_received'];?> 
				</b>
			</div>
		</div>
	<?php
		if (isset($sampleLogBookArray[0]['user_name']))
		{
	?>

		<div class="row" style="font-size: 20px;">
			<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">Logged in By:</div> 
			<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
				<b>
					<?= $sampleLogBookArray[0]['initial_login_user_name'];?> 
				</b>
			</div>
			<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">Last Updated By:</div> 
			<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
				<b>
					<?= $sampleLogBookArray[0]['user_name'];?> 
				</b>
			</div>
		</div>
	<?php
		}
		if (isset($sampleLogBookArray[0]['problems']))
		{
	?>
		<div class="row" style="font-size: 20px;">
			<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">Comments/Problems:</div> 
			<div class="col-xs-10 col-sm-10 col-md-10 col-lg-10">
				<b>
					<?= str_replace('&#*', ', ', $sampleLogBookArray[0]['problems']);?> 
				</b>
			</div>
		</div>
<?php
		}
	}
?>