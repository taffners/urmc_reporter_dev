<div class="row">
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		<span class="report-section-bold">
			INTERPRETATION: 
		</span>
		<?php
			require('templates/shared_layouts/links_to_report_alert.php');
		?>
		<div class="alert alert-success" role="alert">
	<?php


$no_snvs_sections = array(
'summary'  			=> 'Use to summarize the entire report.  '
);

$interp_sections = array(
'summary'  			=> 'Use to summarize the entire report.  This will be the first section of the interpretation in the report.', 
'gene_interpetation' 	=> 'Use to make general statements about mutations in a gene.  Each gene summary will be before all variants for this gene.', 
'variant_interpetation'	=> 'Add interpretations for each variant found.  Each variant interpretation will be included after a gene interpetation for that gene.'
);

	?>
				<b><u>Interpretation sections for report (None of these sections are required)</u></b> 
				<ul>
		<?php
				foreach ($num_snvs !== 0 ? $interp_sections : $no_snvs_sections as $section => $descript)
				{
					if ($page === 'add_'.$section)
					{
		?>	
					<li><strong><?= $utils->UnderscoreCaseToHumanReadable($section);?> (<?= $utils->AddPMIDLinkUrls($descript);?>)</strong></li>
		<?php
					}
					else
					{
		?>
						<li><?= $utils->UnderscoreCaseToHumanReadable($section);?> (<?= $utils->AddPMIDLinkUrls($descript);?>)</li>
		<?php
					}
				}
		?>
				</ul>

		</div>
	</div>
</div>