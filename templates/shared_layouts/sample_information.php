<?php 
	if (
			(
				isset($visitArray[0]['primary_tumor_site'])  && !empty($visitArray[0]['primary_tumor_site'])
			) ||
			(
				isset($visitArray[0]['test_tissue'])  && !empty($visitArray[0]['test_tissue'])
			) ||
			(
				isset($visitArray[0]['tumor_type'])  && !empty($visitArray[0]['tumor_type'])
			) ||
			(
				isset($visitArray[0]['tumor_cellularity'])  && !empty($visitArray[0]['tumor_cellularity'])
			) 
		)
	{
?>
	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			<span class="report-section-bold">
				SAMPLE INFORMATION: 
			</span>

			<table style="width:100%;">
				<tbody>
					<tr>
						<td style="width:150px;">Primary Site:</td>
						<td style="width:550px;font-weight:bold;"><?= $visitArray[0]['primary_tumor_site'];?></td>
						<td style="width:250px;">Tissue Tested:</td>
						<td style="width:500px;font-weight:bold;"><?= $visitArray[0]['test_tissue'];?></td>
					</tr>
					<tr>
						<td style="width:150px;">Tumor Type:</td>
						<td style="width:550px;font-weight:bold;"><?= $visitArray[0]['tumor_type'];?></td>
						<td style="width:250px;">Neoplastic Cellularity:</td>
						<td style="width:500px;font-weight:bold;"><?= $visitArray[0]['tumor_cellularity'];?>%</td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
<?php
	}
?>