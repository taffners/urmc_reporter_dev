<table class="formated_table dataTable">
	<thead>
		<tr>
			<th>Metric</th>
			<th>Range</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td>Density (K/mm2)</td>
			<td>800 - 1,700</td>
		</tr>
		<tr>
			<td>Clusters passing filter (%)</td>
			<td>80 - 100</td>
		</tr>
		<tr>
			<td>Reads passing filter (M)</td>
			<td>&ge; 18</td>
		</tr>
		<tr>
			<td>% ≥ Q30 Total</td>
			<td>70 - 100</td>
		</tr>
		<tr>
			<td>% Aligned – Total (φX174)</td>
			<td>2.5 - 20</td>
		</tr>									
		<tr>
			<td>Error Rate Total % (φX174)</td>
			<td>&le; 4</td>
		</tr>
				
	</tbody>
</table>	