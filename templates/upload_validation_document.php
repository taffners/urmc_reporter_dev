
<div class="container-fluid">    
	<div class="row">

		<!-- update area -->
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			<fieldset>
					<legend id='new_legend' class='show'>
						Upload Validation
					</legend>
				<form id="upload_validation_data" name="upload_validation_data" class="form" method="post" class="form-horizontal" enctype="multipart/form-data" autocomplete="off" >		
					<div class="form-group row required-input">
						<div class="col-xs-12 col-sm-4 col-md-4">
							<label for="validation_date" class="form-control-label" >Validation Date: <span class="required-field">*</span></label>
						</div>
						<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
							<input type="date" class="form-control" id="validation_date" name="validation_date" required value="<?= $utils->GetValueForUpdateInput($validationArray, 'validation_date');?>"/>
						</div>
					</div>
					<div class="form-group alert alert-info">
						<strong>Multiple files can be uploaded below.</strong>
						<br>
						<br> 
						<a href="https://www.computerhope.com/issues/ch000771.htm" target="_blank">Click here to find out how to select multiple files</a>   
					</div>
<?php
				if (isset($validationArray[0]['validation_docs']))
				{
?>					
					<div class="form-group alert alert-warning">
						<strong>Files already uploaded:</strong>
						<br>
						<br> 
						<?= $validationArray[0]['validation_docs'];?>  
					</div>
<?php
				}
?>					
					<div class="form-group row required-input">
						<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
							<label for="validation_file" class="form-control-label" >Add all Files for the Validation Dated above: <span class="required-field">*</span> <span class="mulitple-field">*</span></label><br><span>(file types accepted .jpg, .jpeg, .png, .pdf, .doc, .docx)</span>
						</div>
						<div class="col-xs-12 col-sm-8 col-md-8 col-lg-8 pull-left">
							<input id="validation-files-id" type="file" multiple name="validation_files[]" accept=".jpg,.jpeg,.png,.pdf,.doc,.docx, application/msword, application/vnd.openxmlformats-officedocument.wordprocessingml.document,application/pdf, image/png,image/jpeg"/>
							
						</div>
					</div>
					<div class="form-group row d-none" id="md5-file-hash-div">
						
					</div>
					<div class="form-group row">
						<div class="offset-xs-4 offset-sm-4 offset-md-4 offset-lg-4 col-xs-2 col-sm-2 col-md-2 col-lg-2">
								<button type="submit" id="<?= $page;?>_submit" name="<?= $page;?>_submit" value="Submit"  class="btn btn-primary btn-primary-hover">
									Submit							
								</button>
							</div>
							<div class="offset-xs-4 offset-sm-4 offset-md-4 offset-lg-4 col-xs-2 col-sm-2 col-md-2 col-lg-2">
								<a href="?page=setup_instruments" class="btn btn-primary btn-primary-hover" role="button">
									
									All Instruments
								</a>
							</div>
						</div>
					</div>				
				</form>
				
				
			</fieldset>
		
	</div>
</div>