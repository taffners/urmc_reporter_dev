<?php
if 	(
		isset($_GET['pool_chip_linker_id']) && 
		isset($poolStepTracker) && 
		isset($_GET['ngs_panel_id']) && 
		isset($library_pools) && 
		!empty($library_pools)
	)
{
?>
	<div class="container-fluid">
		<div class="row">
			<!-- report progress bar toggles -->	
			<?php
			require_once('templates/shared_layouts/report_toggle_show_status.php');
			?>

			<!-- Progress bar -->
			<?php
			require_once('templates/shared_layouts/pool_step_nav_bar.php')
			?>
			<div id="toggle-report-progress-bar" class="col-xs-10 col-sm-10 col-md-10 col-lg-10">
		<?php
		
			foreach ($library_pools as $key => $pool)
			{
				if (isset($pool['library_pool_id']) && !empty($pool['library_pool_id']))
				{
					$pending_pre_runs = $db->listAll('pending_pre_runs_library_pool', array('library_pool_id' => $pool['library_pool_id'], 'ngs_panel_id' => $_GET['ngs_panel_id']));		
		?>
					<fieldset>
						<legend id="legend_<?=$key;?>">
							<h4>Backup folder: <?= $API_Utils->ConvertBackupLocToTechFormat($pool['backup_location']);?></h4>
						</legend>
		<?php		
					// Show any errors that occurred.  Fatal errors need to be added to database so checking for transfer completion stops  
					if (isset($errors[$pool['library_pool_id']])  && !empty($errors[$pool['library_pool_id']]))
					{				
						foreach  ($errors[$pool['library_pool_id']] as $error_type => $msg)
						{	

							if (!isset($_GET['loader_only']) && $error_type === 'failed_qc')
							{
		?>						
								<div class="row error_occured" data-error_type="<?= $error_type;?>"
								data-library_pool_id="<?= $pool['library_pool_id'];?>">
								
									<div class="alert alert-danger">
										Failed QC run on Previous Page.
									</div>
								</div>
		<?php					
							}

							elseif (!isset($_GET['loader_only']) && $error_type === 'fatal_error')
							{			
		?>
							<div class="row error_occured" data-error_type="<?= $error_type;?>"
								data-library_pool_id="<?= $pool['library_pool_id'];?>">
								
								<div class="alert alert-danger">
									<h1><?= $utils->UnderscoreCaseToHumanReadable($error_type);?> Occurred</h1> <?php var_dump($msg);?>
								</div>
							</div>
		<?php
							}
							else if ($error_type === 'still_processing')
							{
								// $msg['percent_done'] = 17;
		?>

							<div class="row">
								<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">								
									<h2>Press refresh button <span class="fas fa-redo-alt"></span> to update progress bar.  Backup will be done shortly.</h2>
								</div> 
							</div>
							<div class="row still_processing alert alert-info" data-library_pool_id="<?= $pool['library_pool_id'];?>">
								<div class="col-xs col-sm-12 col-md-12 col-lg-12">
									<h1>Backup in progress</h1>

									<div class="progress" style="height:25px;">
										<div class="progress-bar progress-bar-striped" role="progressbar" aria-valuenow="<?= $msg['percent_done'];?>"
										aria-valuemin="0" aria-valuemax="100" style="width:<?= $msg['percent_done'];?>%;font-size:20px;">
											<?= $msg['percent_done'];?>% Complete
											
										</div>
									</div>
								</div>
							</div>
		<?php						
							}

						}
					}

					else
					{
		?>
						<div class="row">
							<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
							     <?php
							          require('templates/lists/pre_ngs_samples_list.php');
							     ?>
							</div>
						</div>
		<?php
					}	
		?>

					<!-- Add a btn to retransfer data 
						A better way to transfer the data might be with JavaScript ajax call.  

						Make a button here.  Will have to get the results_name name from db.  
						Not sure if it is in there yet.
						"results_name": "Auto_user_S5XL-0108-156-Run65_200_20200825_A_OFA_DNA_530_p200611_257"


					-->
					<!-- <div class="row">
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
							<button id="backup-ofa-data" class="btn btn-warning btn-warning-hover" data-chip="<?= $pool['chip_flow_cell'];?>" data-results_name="<?= $pool['chip_flow_cell'];?>">Retry Backing up data</button>

							$result_name, $chip, $page, $library_pool_id, $pool_info

							ThermoBackup($torrent_results_array[0]['chipB_results_name'], 'B', $page, $torrent_results_array[0]['library_pool_id_B'], $pool_info[0]);
						</div>
					</div> -->

					</fieldset>
		<?php		
				}
			}
					// add form submit, previous, and next buttons.
					require_once('templates/shared_layouts/form_pool_submit_nav_buttons.php');

		?>
				</form>
			</div>	
		</div>
	</div>
<?php
}
?>	
