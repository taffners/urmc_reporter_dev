<div class="container-fluid">

     <fieldset>
         
          <legend>All QC Pages</legend>
         

          <div class="row" style="overflow-x:auto;">
               <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <table class="formated_table sort_table_no_inital_sort_no_paging">
                         <thead>
                              
                              <th>User Name</th>
                              <th>Browser Version</th>
                              <th>Website Version</th>
                              <th>Time Stamp</th>                              
                              <th>Page Validation Report</th>
                         </thead>
                         <tbody>

                         <!-- iterate over all pending runs and add each entry as a new row in the table-->

               <?php

                    for($i=0;$i<sizeof($all_page_qc_validations);$i++)
                    {    
               ?>
                              <tr>
                                  
                                  <td><?= $all_page_qc_validations[$i]['user_name'];?></td>
                                  <td><?= $all_page_qc_validations[$i]['browser_version'];?></td>
                                  <td><?= $all_page_qc_validations[$i]['website_version'];?></td>
                                  <td><?= $all_page_qc_validations[$i]['time_stamp'];?></td>

                         
                                   <td>

                         
                                        <a href="?page=page_validation_report&page_id=<?= $all_page_qc_validations[$i]['page_id'];?>&page_validation_id=<?= $all_page_qc_validations[$i]['page_validation_id'];?>" class="btn btn-success btn-success-hover" role="button">
                                             <img src="images/qc_report.png" style="margin-left:5px;height:45px;width:45px;">
                                        </a>
                                 
                                   </td>

                              </tr>
               <?php 
                    }
               ?>
                         </tbody>
                    </table>
               </div>
          </div>
     </fieldset>
</div>
