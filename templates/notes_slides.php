<div class="contianer-fluid">
     <div class="row">
          <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
               <div class="offset-xs-1 offset-sm-1 offset-md-1 offset-lg-1 col-xs-10 col-sm-10 col-md-10 col-lg-10">
                    <h1 class="white-bkg-H1">Report a problem or make a suggestion for <?= SITE_TITLE;?></h1>
                    <ul style="font-size:20px;">
                         <?php
                              if(!empty($all_notes))
                              {
                                   foreach($all_notes as $i => $ea_arry)
                                   {                                      
                         ?>
                                   <li><?= str_replace("\n", '<br>',$ea_arry['note']);?><br>By: <?= $ea_arry['user_name'];?> at <?= $ea_arry['time_stamp'];?></li>
                         <?php
                                   }
                              }
                              else
                              {
                                   echo 'No Current issues or suggestions';
                              }
                         ?>

                    </ul>   
                    <form class="form" method="post" class="form-horizontal">
                         <div class="form-group row">  
                              <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                   <label for="soft_path_num" class="form-control-label" >Add a Note to the Application Developer <span class="required-field">*</span></label>
                              </div>  
                              <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12"> 
                                   <textarea class="form-control" name="note" rows="15" maxlength="65535" required><?php
if (isset($_GET['problem_id']) && !empty($_GET['problem_id']))
{                                        
?>
Problem ID:<?=$_GET['problem_id'];?>

---------------- DO NOT EDIT ABOVE ----------------
##################################################################
Please Describe What went wrong below. This will send Samantha an email.
##################################################################
<?php     
}
?></textarea>
                              </div>
                         </div>
                         <div class="row">
                              <button type="submit" name="submit" value="Submit"  class="btn btn-primary btn-primary-hover">
                                   Report a problem
                              </button>
                         </div>
                    </form>
               </div>
               
          </div>
     </div>
</div>