<div class="container-fluid">
	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="height:500px;">
			<form class="form" method="post" class="form-horizontal">
				<fieldset>
					<legend class='show'>
						Add a Sample to Log Book
					</legend>
					<div class="form-group row">
						<div class="col-xs-12 col-sm-4 col-md-4">
							<label for="sample-type-toggle" class="form-control-label">Sample Type Toggle: <span class="required-field">*</span></label>
						</div>
						<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
							<div class="radio">
								<label class="radio-inline"><input type="radio" name="sample_type_toggle" value="sample" required="required" <?= $utils->DefaultRadioBtn($sampleLogBookArray, 'sample_type_toggle', 'sample');?>>Sample</label>
							
								<label class="radio-inline"><input type="radio" name="sample_type_toggle" value="anonymous_donor"  <?= $utils->GetValueForUpdateRadioCBX($sampleLogBookArray, 'sample_type_toggle', 'anonymous_donor');?>>Anonymous Donor</label>

								<label class="radio-inline"><input type="radio" name="sample_type_toggle" value="cap_sample"  <?= $utils->GetValueForUpdateRadioCBX($sampleLogBookArray, 'sample_type_toggle', 'cap_sample');?>>CAP Sample</label>
								<label class="radio-inline"><input type="radio" name="sample_type_toggle" value="identity_testing"  <?= $utils->GetValueForUpdateRadioCBX($sampleLogBookArray, 'sample_type_toggle', 'identity_testing');?>>Identity testing</label>
							</div>
						</div>
					</div>
					<div class="form-group row">
						
				<?php					
					// anonymous update
					if 	(
							isset($sampleLogBookArray[0]['sample_type_toggle']) && 
							$sampleLogBookArray[0]['sample_type_toggle'] === 'anonymous_donor'
						)
					{
						$last_name_label_class = 'd-none';
						$donor_label_class = '';
						$required_toggle = '';
						$cap_sample_label = 'd-none';
						$soft_path_num_label = '';
						$dob_sex_label = 'd-none';
					}

					// CAP sample
					if 	(
							isset($sampleLogBookArray[0]['sample_type_toggle']) && 
							$sampleLogBookArray[0]['sample_type_toggle'] === 'cap_sample'
						)
					{
						$last_name_label_class = '';
						$donor_label_class = 'd-none';
						$required_toggle = 'required="required"';
						$cap_sample_label = '';
						$soft_path_num_label = 'd-none';
						$dob_sex_label = 'd-none';
					}

					// new log
					else if (!isset($sampleLogBookArray[0]['sample_type_toggle']))
					{
						$last_name_label_class = '';
						$donor_label_class = 'd-none';
						$required_toggle = 'required="required"';
						$cap_sample_label = 'd-none';
						$soft_path_num_label = '';
						$dob_sex_label = '';
					}

					// sample update
					else
					{
						$last_name_label_class = '';
						$donor_label_class = 'd-none';
						$required_toggle = 'required="required"';
						$cap_sample_label = 'd-none';
						$soft_path_num_label = '';
						$dob_sex_label = '';
					}
				?>
						<div class="col-xs-12 col-sm-12 col-md-10 col-lg-10">
							<div class="form-group row">
								<div id="donor-id-label" class="col-xs-12 col-sm-5 col-md-5 col-lg-5 <?= $donor_label_class;?>">
									<label for="last_name" class="form-control-label">Donor ID: <span class="required-field">*</span></label>
								</div>
						
								<div id="last-name-label" class="col-xs-12 col-sm-5 col-md-5 col-lg-5 <?= $last_name_label_class;?>">
									<label for="last_name" class="form-control-label">Last Name: <span class="required-field">*</span></label>
								</div>
													
								<div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
									<input type="text" class="form-control capitalize" id="last_name" name="last_name" maxlength="100" required="required" value="<?= $utils->GetValueForUpdateInput($sampleLogBookArray, 'last_name');?>"/>
								</div>
							</div>
							<div class="form-group row">
								<div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
									<label for="first_name" class="form-control-label">First Name: <span class="required-field">*</span></label>
								</div>
								<div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
									<input type="text" class="form-control capitalize" id="first_name" name="first_name" maxlength="100" required="required" value="<?= $utils->GetValueForUpdateInput($sampleLogBookArray, 'first_name');?>"/>
								</div>
							</div>
						</div>
						<div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
							<div class="form-group row">
								<button type="button" id="uncapitalize" class="btn btn-primary btn-primary-hover">
									Remove Auto formating 
								</button>
							</div>
						</div>
					</div>	
					<div class="form-group row">
						<div class="col-xs-12 col-sm-4 col-md-4">
							<label for="dob" class="form-control-label">Date of Birth: <span class="dob-sex-required-span required-field <?= $dob_sex_label;?>">*</span></label>
						</div>
						<div class="col-xs-12 col-sm-7 col-md-4 col-lg-3">
							<input class="form-control copy-paste-date" id="dob" name="dob" <?php
							if(isset($sampleLogBookArray[0]['dob']))
							{
								echo 'value="'.$dfs->ChangeDateFormatUS($sampleLogBookArray[0]['dob'], FALSE).'"'; 
								
							}
							?> placeholder="MM/DD/YYYY" pattern="\d{1,2}/\d{1,2}/\d{4}" required="required"/>
						</div>
					</div>
					<div class="form-group row">
						<div class="col-xs-12 col-sm-4 col-md-4">
							<label for="sex" class="form-control-label">Sex: <span class="dob-sex-required-span required-field <?= $dob_sex_label;?>">*</span></label>
						</div>
						<div class="col-xs-12 col-sm-7 col-md-4 col-lg-3">
							<div class="radio">
								<label class="radio-inline"><input type="radio" name="sex" required="required" value="M" <?php
								if(isset($sampleLogBookArray[0]['sex']) && $sampleLogBookArray[0]['sex'] == 'M')
								{
									echo 'checked'; 								
								}
								?>>Male</label>
							
								<label class="radio-inline"><input type="radio" name="sex" value="F" <?php
								if(isset($sampleLogBookArray[0]['sex']) && $sampleLogBookArray[0]['sex'] == 'F')
								{
									echo 'checked'; 
								}
								?>>Female</label>


								<label class="radio-inline"><input type="radio" name="sex" value="U" <?php
								if(isset($sampleLogBookArray[0]['sex']) && $sampleLogBookArray[0]['sex'] == 'U')
								{
									echo 'checked'; 
								}
								?>>Not Known</label>
							</div>
						</div>
						<div class="col-x-12 col-sm-1 col-md-2 col-lg-2">
							<button type="button" class="uncheck-radio-btn btn btn-primary btn-primary-hover" data-name_radio_input="sex">
								Uncheck
							</button>
						</div>
					</div>							
					<div class="form-group row">
						<div class="col-xs-12 col-sm-4 col-md-4">
							<label for="date_received" class="form-control-label">Received: </label>
						</div>
						<div class="col-xs-12 col-sm-8 col-md-4 col-lg-3">
							<input class="form-control" type="datetime-local" id="date_received" name="date_received" value="<?= $dfs->convertToUTC($utils->GetValueForUpdateInput($sampleLogBookArray, 'date_received', date('Y-m-d\TH:i:s')));?>" />
						</div>
					</div>	
				
					<div class="form-group row">
						<div id="soft-path-num-label" class="col-xs-12 col-sm-4 col-md-4 <?= $soft_path_num_label;?>">
							<label for="soft_path_num" class="form-control-label" >Soft Path #:</label>
						</div>

						<div id="cap-sample-label" class="col-xs-12 col-sm-4 col-md-4 <?= $cap_sample_label;?>">
							<label for="last_name" class="form-control-label">CAP ID: <span class="required-field">*</span></label>
						</div>
						<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
							<input type="text" class="form-control no-spaces" id="soft_path_num" name="soft_path_num" maxlength="50" value="<?= $utils->GetValueForUpdateInput($sampleLogBookArray, 'soft_path_num');?>"/>
						</div>
					</div>
					<div class="form-group row">
						<div class="col-xs-12 col-sm-4 col-md-4">
							<label for="outside_case_num" class="form-control-label" >Outside case #:</label>
						</div>
						<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
							<input type="text" class="form-control" id="outside_case_num" name="outside_case_num" maxlength="50" value="<?= $utils->GetValueForUpdateInput($sampleLogBookArray, 'outside_case_num');?>"/>
						</div>
					</div>
					<div class="form-group row">
						<div class="col-xs-12 col-sm-4 col-md-4">
							<label for="soft_lab_num" class="form-control-label" >Soft Lab #: <span id="soft-lab-required-span" class="required-field <?= $last_name_label_class;?>">*</span></label>
						</div>
						<div class="col-xs-12 col-sm-4 col-md-4 ">
							<input type="text" class="form-control no-spaces no-period" id="soft_lab_num" name="soft_lab_num" maxlength="50" <?= $required_toggle;?> value="<?= $utils->GetValueForUpdateInput($sampleLogBookArray, 'soft_lab_num');?>"/>
						</div>
						<div class="col-xs-12 col-sm-4 col-md-4 ">
							<button type="button" data-unk-field="soft_lab_num" class="assign-unk-num btn btn-primary btn-primary-hover">
								Assign Unknown #
							</button>
						</div>
					</div>
					<div class="form-group row">
						<div class="col-xs-12 col-sm-4 col-md-4">
							<label for="mol_num" class="form-control-label">MD#:<span class="required-field">*</span></label>
						</div>
						<div class="col-xs-12 col-sm-4 col-md-4 ">
							<input type="text" class="form-control no-spaces" id="mol_num" name="mol_num" maxlength="50" required="required" value="<?= $utils->GetValueForUpdateInput($sampleLogBookArray, 'mol_num', $full_next_md_num);?>" readonly="readonly" pattern="[0-9]*-[0-9]{2}"/>
						</div>
<?php 
	if (!isset($_GET['sample_log_book_id']))
	{
?>						
						<div class="col-xs-12 col-sm-4 col-md-4 ">
							<button type="button" id="remove-md-readonly" class="btn btn-primary btn-primary-hover">
								MD# was previously assigned
							</button>
						</div>					
<?php
	}
?>
					</div>
					<div class="form-group row">
						<div class="col-xs-12 col-sm-4 col-md-4">
							<label for="medical_record_num" class="form-control-label">MRN:<span id="mrn-required-span" class="required-field <?= $last_name_label_class;?>">*</span></label>
						</div>
						<div class="col-xs-12 col-sm-8 col-md-4">
							<input type="text" class="form-control no-spaces" id="medical_record_num" name="medical_record_num" maxlength="20" <?= $required_toggle;?> value="<?= $utils->GetValueForUpdateInput($sampleLogBookArray, 'medical_record_num');?>"/>
						</div>
						<div class="col-xs-12 col-sm-4 col-md-4 ">
							<button type="button" data-unk-field="medical_record_num" class="assign-unk-num btn btn-primary btn-primary-hover">
								Assign Unknown #
							</button>
						</div>
					</div>
					<div class="form-group row">
						<div class="col-xs-12 col-sm-4 col-md-4">
							<label for="mpi" class="form-control-label">MPI:</label>
						</div>
						<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
							<input type="text" class="form-control no-spaces" id="mpi" name="mpi" maxlength="100" value="<?= $utils->GetValueForUpdateInput($sampleLogBookArray, 'mpi');?>"/>
						</div>
					</div>
					<div class="form-group row">
						<div class="col-xs-12 col-sm-4 col-md-4">
							<label for="outside_mrn" class="form-control-label">Outside MRN:</label>
						</div>
						<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
							<input type="text" class="form-control no-spaces" id="outside_mrn" name="outside_mrn" maxlength="100" value="<?= $utils->GetValueForUpdateInput($sampleLogBookArray, 'outside_mrn');?>"/>
						</div>
					</div>
					<div class="form-group row">
						<div class="col-xs-12 col-sm-4 col-md-4">

						</div>
						<div class="col-xs-12 col-sm-8 col-md-8 alert alert-info">
						
							<h3>Explanation of <span style="color:red;font-weight:bold;">tissue type</span> vs <span style="color:green;font-weight:bold;">Sample Type</span>.</h3>
							<ul style="font-size:18px;">
								<li><b>For DNA</b>: <span style="color:red;font-weight:bold;">Tissue Tested</span>: Blood, Bone marrow or "Unknown" if that info isn’t given.  <span style="color:green;font-weight:bold;">Sample Type</span>: DNA</li>
								<li><b>For Bloods & Bone marrows</b>:  Example:<span style="color:red;font-weight:bold;">Tissue Tested</span>: "Blood" <span style="color:green;font-weight:bold;">Sample Type</span>: "Fresh"</li>
								<li><b>For FFPE</b>: Example:<span style="color:red;font-weight:bold;">Tissue Tested</span>: "Skin" <span style="color:green;font-weight:bold;">Sample Type</span>: Example "Paraffin Embedded Curls"</li>
								<li><b>For fresh tissue/cells</b>: Example:<span style="color:red;font-weight:bold;">Tissue Tested</span>: "CSF" <span style="color:green;font-weight:bold;">Sample Type</span>: "Fresh"</li>	
								<li>New <span style="color:red;font-weight:bold;">Tissue Tested</span> values can be entered at any time however, please use already entered values when ever possible.  This will decrease the likely hood of typos.</li>
								<li>In the unlikely event that you will need a new possible <span style="color:green;font-weight:bold;">Sample Type</span> please contact <?= $str_login_enhanced_users;?></li>							
							</ul>

						</div>
						<div class="col-xs-12 col-sm-4 col-md-4">
							<label for="test_tissue" class="form-control-label">Tissue Tested:<span class="required-field">*</span></label>
						</div>
						<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
							<input type="text" class="form-control" name="test_tissue" maxlength="30" list="test_tissue" required="required" value="<?= $utils->GetValueForUpdateInput($sampleLogBookArray, 'test_tissue');?>"/>
							<datalist name="test_tissue" id="test_tissue">
							<?php


								for($i=0;$i<sizeof($all_tissues);$i++)
		               			{
							?>
									<option value="<?= $all_tissues[$i]['tissue']; ?>">
										<?= $all_tissues[$i]['tissue']; ?>
									</option>
							<?php
								}
							?>
							</datalist>
						</div>
					</div>

					<div id="form-control-fields">
						<div class="form-group row">
							
							<div class="col-xs-12 col-sm-4 col-md-4">
								<label for="possible_sample_types_id" class="form-control-label">Sample Type: <span class="required-field">*</span></label>
							</div>
							<div class="col-xs-12 col-sm-4 col-md-4">
								<select class="form-control" name="possible_sample_types_id" id="possible_sample_types_id" required="required">
									<option value="" selected="selected"></option>
								<?php
									for($i=0;$i<sizeof($all_possible_sample_types);$i++)
			               			{	
								?>
										<option value="<?= $all_possible_sample_types[$i]['possible_sample_types_id']; ?>" <?= $utils->GetValueForUpdateSelect($sampleLogBookArray, 'possible_sample_types_id', $all_possible_sample_types[$i]['possible_sample_types_id'])?>>
											<?= $all_possible_sample_types[$i]['sample_type']; ?>
										</option>
								<?php
									}
								?>
								</select>
							</div>
							<div class="col-xs-12 col-sm-4 col-md-4">
					<?php
                         // turn off any adding for view only permissions
                         if (isset($user_permssions) &&  strpos($user_permssions, 'login_enhanced') !== false)
                         {
                    ?> 								
								<a href="?page=add_new_sample_type"  class="btn btn-primary btn-primary-hover">
									Add a new Sample Type
								</a>
					<?php
						}
					?>									
							</div>
						</div>
					</div>
					<div class="form-group row">
						<div class="col-xs-12 col-sm-4 col-md-4">
							<label for="h_and_e_circled" class="form-control-label">H&E Circled: <span class="required-field">*</span></label>
						</div>
						<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
							<div class="radio">
								<label class="radio-inline"><input type="radio" name="h_and_e_circled" value="yes" required="required" <?= $utils->GetValueForUpdateRadioCBX($sampleLogBookArray, 'h_and_e_circled', 'yes');?>>Yes</label>
							
								<label class="radio-inline"><input type="radio" name="h_and_e_circled" value="no"  <?= $utils->GetValueForUpdateRadioCBX($sampleLogBookArray, 'h_and_e_circled', 'no');?>>No</label>

								<label class="radio-inline"><input type="radio" name="h_and_e_circled" value="NA"  <?= $utils->GetValueForUpdateRadioCBX($sampleLogBookArray, 'h_and_e_circled', 'NA');?>>NA</label>

								<label class="radio-inline"><input type="radio" name="h_and_e_circled" value="Requested"  <?= $utils->GetValueForUpdateRadioCBX($sampleLogBookArray, 'h_and_e_circled', 'Requested');?>>Requested</label>
							</div>
						</div>
					</div>
					<div class="form-group row">
						<div class="col-xs-12 col-sm-4 col-md-4">
							<label for="block" class="form-control-label">Block: </label>
						</div>
						<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
							<input type="text" class="form-control" id="block" name="block" maxlength="100" value="<?= $utils->GetValueForUpdateInput($sampleLogBookArray, 'block');?>"/>
						</div>
					</div>
					<div class="form-group row">
						<div class="col-xs-12 col-sm-4 col-md-4">
							<label for="WBC_count" class="form-control-label">WBC Count: </label>
						</div>
						<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
							<input type="number" class="form-control" id="WBC_count" name="WBC_count" value="<?= $utils->GetValueForUpdateInput($sampleLogBookArray, 'WBC_count');?>" step=".01"/>
						</div>
					</div>
					<div class="form-group row">
						<div class="col-xs-12 col-sm-4 col-md-4">
							<label for="tumor_cellularity" class="form-control-label">Neoplastic Cellularity (% Tumor): </label>
						</div>
						<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
							<input type="number" class="form-control" id="tumor_cellularity" name="tumor_cellularity" min="0" max="100" value="<?= $utils->GetValueForUpdateInput($sampleLogBookArray, 'tumor_cellularity');?>" step=".01"/>
						</div>
					</div>
		<?php
			if (isset($previousProblems))
			{
		?>
					<div class="form-group row">
						<div class="col-xs-12 col-sm-4 col-md-4">
							<label for="problems" class="form-control-label">Previous Problems/Comments:</label>
						</div>

						<div class="col-xs-12 col-sm-8 col-md-8 pull-left">

							<ul>
			<?php
						foreach($previousProblems as $key => $problem)
						{
			?>
								<li><?= $problem['comment'];?> (<?= $problem['user_name'];?> <?= $problem['time_stamp'];?>) 
						<?php
		                    if (isset($user_permssions) && (strpos($user_permssions, 'admin') !== false || strpos($user_permssions, 'edit_data') !== false))
		                    {                        
		               	?>								
									<button type="button" class="rm-comment btn btn-danger" data-comment-id="<?= $problem['comment_id'];?>" data-comment-type="<?= $problem['comment_type'];?>" data-comment-ref="<?= $problem['comment_ref'];?>" data-comment="<?= $problem['comment'];?>"><span class="fa fa-times"></span></button></li>
			<?php
							}
						}
			?>
							</ul>
						</div>
					</div>
		<?php
			}
		?>
					<div class="form-group row">
						<div class="col-xs-12 col-sm-4 col-md-4">
							<label for="problems" class="form-control-label">Problems/Comments:</label>
						</div>

						<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
							<textarea class="form-control" name="problems" rows="4" maxlength="65535" style="margin-bottom: 10px;"></textarea>
						</div>
					</div>

					<div class="form-group row d-none">
						<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
							<input type="number" class="form-control" id="sample_log_book_id" name="sample_log_book_id" value="<?= $utils->GetValueForUpdateInput($sampleLogBookArray, 'sample_log_book_id');?>" />
						</div>
						<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
							<input type="number" class="form-control" id="initial_login_user_id" name="initial_login_user_id" value="<?= $utils->GetValueForUpdateInput($sampleLogBookArray, 'initial_login_user_id');?>" />
						</div>
					</div>
					<div class="form-group row">
						<div class="offset-xs-4 offset-sm-4 offset-md-4 offset-lg-4 col-xs-2 col-sm-2 col-md-2 col-lg-2">
							<button type="submit" id="<?= $page;?>_submit" name="<?= $page;?>_submit" value="Submit"  class="btn btn-primary btn-primary-hover">
								Submit
							</button>
						</div>
					</div>	
				</fieldset>
			</form>
		</div>
	</div>
</div>