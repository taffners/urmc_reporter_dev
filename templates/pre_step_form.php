<div class="container-fluid">

	<form class="form" method="post" class="form-horizontal">
		<fieldset>
			<legend id='new_legend' class='show'>
				<?= $legend_title;?>
			</legend>
			<input type="hidden" id="pre_step_visit_id" name="pre_step_visit_id" value="<?php 
				if(isset($step_status[0]['pre_step_visit_id']))
				{
					echo $step_status[0]['pre_step_visit_id'];
				}
			?>"/>
			<div class="form-group row">
				<div class="col-xs-12 col-sm-4 col-md-4">
					<label for="concentration" class="form-control-label">Concentration: <span class="required-field">*</span></label>
				</div>
				<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
					<input type="text" class="form-control" id="concentration" name="concentration" maxlength="50" value="<?php 
					if(isset($step_status[0]['concentration']))
					{
						echo $step_status[0]['concentration'];
					}
				?>" required/>
				</div>
			</div>

			<div class="form-group row">
				<div class="col-xs-12 col-sm-4 col-md-4">
					<label for="status" class="form-control-label">Status: <span class="required-field">*</span></label>
				</div>
				<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
					<div class="radio">
						<label class="radio-inline"><input type="radio" name="status" value="passed" <?php
						if(isset($step_status[0]['status']) && $step_status[0]['status'] == 'passed')
						{
							echo 'checked'; 
						}
						?>>Passed</label>
					
						<label class="radio-inline"><input type="radio" name="status" value="Flagged" <?php
						if(isset($step_status[0]['status']) && $step_status[0]['status'] == 'Flagged')
						{
							echo 'checked'; 
						}
						?>>Flagged</label>
						<?php
							// add an alert box to notify director that checking Flagged will mean the sample will need to be restarted.
							if (strpos($user_permssions, 'director') !== false)
							{
						?>
								<div class="alert alert-info" style="font-size: 18px;">
  									By checking <strong>Failed</strong> you will be confirming that this sample does not have enough <?= $conc_type; ?> and the sample will need to be re-logged in as a new visit. The information will not be deleted but you will no longer be able to make a report for this sample.
								</div>
						<?php
							}
						?>
						<?php
							if (strpos($user_permssions, 'director') !== false)
							{
						?>
								<label class="radio-inline"><input type="radio" name="status" value="Failed" <?php
								if(isset($step_status[0]['status']) && $step_status[0]['status'] == 'Failed')
								{
									echo 'checked'; 
								}
								?>>Failed</label>
						<?php
							}

							// add time and user that made the status change 
							if (isset($step_status[0]['status']) && $step_status[0]['time_stamp'] != null & $step_status[0]['user_name'] !== null)
							{
								echo '   ('.$step_status[0]['user_name'].' '.$step_status[0]['time_stamp'].')';
							}
						?>
					</div>
				</div>
			</div>


			<div class="row">
				<div class="offset-xs-4 offset-sm-4 offset-md-4 offset-lg-4 col-xs-2 col-sm-2 col-md-2 col-lg-2">
					<button type="submit" id="<?= $page;?>_submit" name="<?= $page;?>_submit" value="Submit"  class="btn btn-primary btn-primary-hover <?php
					if ($_GET['page'] !== 'review_report')
					{
						echo 'disabled-link';
					}
					?>">
						Submit
						
					</button>
				</div>
				<div class="offset-xs-4 offset-sm-4 offset-md-4 offset-lg-4 col-xs-2 col-sm-2 col-md-2 col-lg-2">
					<a href="?page=home" class="btn btn-primary btn-primary-hover" role="button">
						<img src="images/house.png" style="height:23px;width:23px;margin-right:5px;" alt="house icon">
						Home
					</a>
				</div>
				<!-- add the common hidden fields for every form -->
				<?php
					require_once('templates/shared_layouts/form_hidden_fields.php')
				?>
			</div>
		</fieldset>
	</form>
</div>