

<nav class="navbar navbar-expand-lg navbar-dark" style="background-color:#9b59b6;">
	<a class="navbar-brand" href="?page=training&training_step=main" title="Go to training main menu">Training Nav Bar</a>
	<div class="collapse navbar-collapse">

		<!-- Right menu -->
		<ul class="navbar-nav mr-auto">
			<li class="nav-item dropdown">
				<a class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" title="About uploading files">
				NGS Obtain Upload Files<span class="caret"></span>
				</a>
				<div class="dropdown-menu">
					<a class="dropdown-item" href="?page=training&training_step=obtain_upload_files#ofa-files">OFA Files</a>
					
					<a class="dropdown-item" href="?page=training&training_step=obtain_upload_files#myeloid-tsv-file">Myeloid TSV File</a>
					<a class="dropdown-item" href="?page=training&training_step=obtain_upload_files#myeloid-cov-file">Myeloid Coverage File</a>
				</div>
				
			</li>
			<li class="nav-item dropdown">
				<a class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" >
				NGS QC Run Metrics<span class="caret"></span>
				</a>
				<div class="dropdown-menu">
					<a class="dropdown-item" href="?page=training&training_step=qc_run#ofa-run-metrics">OFA</a>
					<a class="dropdown-item" href="?page=training&training_step=qc_run#myeloid-run-metrics">Myeloid</a>
				</div>				
			</li>
			<li class="nav-item dropdown">
				<a class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" >
				NGS Strand Bias<span class="caret"></span>
				</a>
				<div class="dropdown-menu">
					<a class="dropdown-item" href="?page=training&training_step=strand_bias#about-strand-bias">About Strand Bias</a>
					<a class="dropdown-item" href="?page=training&training_step=strand_bias#ofa-strand-bias">OFA</a>
					<a class="dropdown-item" href="?page=training&training_step=strand_bias#myeloid-strand-bias">Myeloid</a>
				</div>				
			</li>
			<li class="nav-item dropdown">
				<a class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" >
				SOPs<span class="caret"></span>
				</a>
				<div class="dropdown-menu">
					<a class="dropdown-item" href="?page=training&training_step=Approved_SOPs#infotrack-sop"><?= SITE_TITLE;?> SOP</a>
					<a class="dropdown-item" href="?page=training&training_step=Approved_SOPs#ofa-sop">OFA</a>
					<a class="dropdown-item" href="?page=training&training_step=Approved_SOPs#myeloid-sop">Myeloid</a>
					<a class="dropdown-item" href="?page=training&training_step=FAQ#permission_softpath">Scan Molecular NGS Results into SOFTPATH</a>

				</div>				
			</li>
			<li class="nav-item dropdown">
				<a class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" >
				Videos<span class="caret"></span>
				</a>
				<div class="dropdown-menu">
					<div class="dropdown-divider"></div> 
                    <div class="dropdown-header">NGS Bench</div>
					<a class="dropdown-item" href="https://www.youtube.com/watch?v=_yC0Bzw3WbQ" target="_blank">General Library prep (Barcodes are part of the adapters)</a> 
					<a class="dropdown-item" href="https://www.youtube.com/watch?v=DyijNS0LWBY" target="_blank">Ion torrent Sequencing</a>
					<a class="dropdown-item" href="https://www.youtube.com/watch?time_continue=16&v=fCd6B5HRaZ8&feature=emb_logo" target="_blank">Illumina sequencing (Barcodes are called indices)</a> 

					<div class="dropdown-divider"></div> 
                    <div class="dropdown-header">IGV</div>
                    <a class="dropdown-item" href="https://www.youtube.com/watch?v=E_G8z_2gTYM" target="_blank">IGV Sequencing Data Basics</a>
                    <a class="dropdown-item" href="https://www.youtube.com/watch?v=EpD2ZHM7Q8Q" target="_blank">IGV VCF Basics</a>
                    
                    
				</div>				
			</li>
			
			<!-- <li class="nav-item dropdown">
				<a class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" >
				Other<span class="caret"></span>
				</a>
				<div class="dropdown-menu">
					<a class="dropdown-item" href="?page=workflow_overview">Flow Diagram</a>
					<a class="dropdown-item" href="?page=training&training_step=issues">Issues</a>
				</div>				
			</li> -->
		</ul>
	</div>
</nav>


