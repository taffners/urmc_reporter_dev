<?php
     if ($page != 'add_variant_interpetation')
     {
?>
          <hr class="section-mark">
<?php
     }
     
?>
<table class="formated_table sort_table">

     <thead >
          <tr>
          	<!-- <th>Select 2 for comparison</th> -->
               <th><?= $variant_name;?> Interpretations</th>
          	<th>User Name</th>
          	<th>Date</th>
               <?php
               // add a data element for saving variant info here if the page is add_variant_interpetation
               if (isset($_GET['page'])  && $_GET['page'] === 'add_variant_interpetation')
               {
               ?>
                    <th>Delete</th>
               <?php
               }
               ?>
          </tr>
     </thead>
     <tbody>
     <!-- Add a new row to the table for each comment -->
     <?php
   
     	for($j=0;$j<sizeof($comments);$j++)
     	{

               // add_variant_interptation page names $variant keys differently change them to this convention
               if (isset($variant['Gene']))
               {
                    $variant['genes'] = $variant['Gene'];
                    unset($variant['Gene']);
               }

               if (isset($variant['DNA']))
               {
                    $variant['coding'] = $variant['DNA'];
                    unset($variant['DNA']);
               }

               if (isset($variant['Protein']))
               {
                    $variant['amino_acid_change'] = $variant['Protein'];
                    unset($variant['Protein']);
               }
               
     ?>     
     	<tr id="<?= $comments[$j]['comment_ref']; ?>_<?= $comments[$j]['comment_id']; ?>">
               <!-- <td><input class="comparision-ckbx" type="checkbox" name="include_in_report" value="1" checked="" data-interpt-text="<?= $comments[$j]['comment']; ?>"></td> -->
     		<td class="hover_success_on interp_table_row" title="Click here to use this interpretation." style="text-align: left;"<?php
               // add a data element for saving variant info here if the page is add_variant_interpetation
               if (isset($_GET['page'])  && $_GET['page'] === 'add_variant_interpetation')
               {
          ?>
                    data-alert-id="alert_<?= $utils->ReplaceSpecialChars($variant['genes']);?>_<?= $utils->ReplaceSpecialChars($variant['coding']);?>_<?= $utils->ReplaceSpecialChars($variant['amino_acid_change']);?>_<?= $utils->ReplaceSpecialChars($variant['observed_variant_id']);?>"
                    data-observed-variant-id="<?= $utils->ReplaceSpecialChars($variant['observed_variant_id']);?>"
                    data-interpt-id="<?= $comments[$j]['comment_id']; ?>"
                    data-knowledge-id="<?= $comments[$j]['comment_ref']; ?>"
                    data-interpt-text="<?= $comments[$j]['comment']; ?>"
          <?php
               }
          ?>>
     			<?= $utils->AddPMIDLinkUrls($comments[$j]['comment']); ?>
     			
     		</td>
     		<td>
     			<?= $comments[$j]['first_name']; ?> <?= $comments[$j]['last_name']; ?></td>
     		<td>
     			<?php echo $utils->MySQLTimeStampToDate($comments[$j]['comment_time_stamp']); ?>	
     		</td>
               
<?php
               // add a data element for saving variant info here if the page is add_variant_interpetation
               if (isset($_GET['page'])  && $_GET['page'] === 'add_variant_interpetation')
               {
?>                   
               <td>                 
                    <button class="btn btn-primary hide-variant" data-comment-id="<?= $comments[$j]['comment_id']; ?>" data-interpt-text="<?= $comments[$j]['comment']; ?>" aria-label="hide comment">
                         <span class="far fa-trash-alt"></span>
                    </button>
               </td>
<?php 
               }
?>
               
     	</tr>
     <?php
     	}
     ?>
     </tbody>
</table>