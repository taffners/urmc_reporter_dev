
<?php
     
     // The views are different if this table will be used for pools.  
     if (isset($_GET['pool_chip_linker_id']) && isset($_GET['ngs_panel_id']))
     {
          $pool_uploading = True;
     }
     else
     {
          $pool_uploading = False;
     }


     if ($_GET['page'] === 'home'  || $page === 'upload_NGS_data')
     {
?>
<fieldset id="pre-ngs-samples-fieldset">
     <legend>Pre NGS Samples</legend>

<?php
     }
     if (isset($user_permssions) &&  strpos($user_permssions, 'View_Only') === false && $_GET['page'] === 'home')
     {
          // set send out btn class
          if (isset($_GET['sample_type']))
          {
               $sendOutBtnClass = 'btn btn-success btn-success-hover pull-right';
          }
          else
          {
               $sendOutBtnClass = 'btn btn-warning btn-warning-hover pull-right';
          }

?>
     <div class="row">
          <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
               <a id="show-send-outs-btn" href="?page=home&sample_type=Sent_Out" type="button" class="<?= $sendOutBtnClass;?>">
                    Show Send Outs
               </a>
          </div>
     </div>
<?php
     }
?>

     <div class="row">
          <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
               <table class="formated_table sort_table_no_inital_sort_no_paging">
                    <thead>
                         <th>#</th>
                         <th>MD#</th>
                         <th>mol#</th>
                         <th>soft path#</th>
                         <th>soft lab#</th>
                         <th>MRN#</th>                                         
                         <th>NGS Panel</th>
                    	<th>Patient Name</th>
<?php
     if (!$pool_uploading)
     {
?>  
                         <th id="pre-ngs-editors-completed-steps" title="This coloumn shows the inital user that completed each step.  It does not reflect any updates.">Editors / Completed Steps</th> 
                         <th id="pre-ngs-login-time-col">Login Time</th> 
                         <th>Visit Notes</th>
<?php
     }
     if (isset($user_permssions) &&  strpos($user_permssions, 'softpath') !== false)
     {
?> 
                        <th>Collected</th> 
                        <th>Received</th>
                        <th>Specimen Type</th>
<?php
     }
?>


<?php
     if (isset($user_permssions) &&  strpos($user_permssions, 'View_Only') === false && !$pool_uploading)
     {
?>                    
                         <th id="pre-ngs-visit-col" title="Click on the buttons below to edit information in log book">Log Book</th>
                         
                         <th title="These circles indicate if the sample is in process or not.">Pending</th>
                         <th title="These circles indicate if the DNA step has been completed successfully or not.">DNA</th>
                         <th title="These circles indicate if the Amplicon concentration step has been completed successfully or not.">Amp</th>
<?php
     }

     if (isset($_GET['ngs_panel_id'])  && $_GET['ngs_panel_id'] !== 2 && $pool_uploading)
     {
?>
                         <th title="Sequencing sample name">Full Sample Name</th>
<?php
     }
?>    
<?php
     if   (
               isset($user_permssions) &&  
               strpos($user_permssions, 'View_Only') === false && 
               $page !== 'upload_NGS_data' &&
               (
                    (isset($_GET['sample_type']) && $_GET['sample_type'] !== 'Sent_Out') ||
                    (!isset($_GET['sample_type']))
               )
          )
     {
?>                 
                         <th id="data-upload-th" title="Press this button to upload finished NGS data."><span class="fas fa-cloud-upload-alt" aria-hidden="true"></span> Data</th>
                         <th id="pre-ngs-cancel-col" title="To cancel pre NGS tests use the Cancel red x each row in the pre ngs samples table. This will remove samples from any pools also." style="background:#d9534f;">Cancel</th>
<?php
     }
?>
                    </thead>
                    <tbody>
               	<?php
               		if (isset($pending_pre_runs))
               		{
                         
               			for($i=0;$i<sizeof($pending_pre_runs);$i++)
               			{ 
                                   
                                   $completed_pre_steps = $db->listAll('completed-pre-steps', $pending_pre_runs[$i]['visit_id']);
                                   $passed_steps = $pending_pre_runs[$i]['steps_passed'];
                                   $flagged_steps = $pending_pre_runs[$i]['flagged_steps']; 

               	?>
               			<tr id="visit_id_<?= $pending_pre_runs[$i]['visit_id']; ?>" class="hover_background_row">
               				<td><?= $pending_pre_runs[$i]['visit_id']; ?></td>
               				<td class="md-num-td"><?= $pending_pre_runs[$i]['mol_num']; ?></td>
                                   <td><?= $pending_pre_runs[$i]['order_num']; ?></td>
                                   <td class="soft-path-td"><?= $pending_pre_runs[$i]['soft_path_num']; ?></td>
                                   <td><?= $pending_pre_runs[$i]['soft_lab_num']; ?></td>
                                   <td><?= $pending_pre_runs[$i]['medical_record_num']; ?></td>
               				<td><?= $pending_pre_runs[$i]['ngs_panel']; ?></td>
               				<td><?= $pending_pre_runs[$i]['patient_name']; ?></td>
<?php
     if (!$pool_uploading)
     {
?>                                    
               				<td title="The editors / completed steps column shows the initial user that completed each step.  It does not reflect any updates."><?= str_replace(', ', '<br>', $pending_pre_runs[$i]['editors_per_step']); ?></td>
               				<td><?= $pending_pre_runs[$i]['time_stamp']; ?></td>
                                   <td><?= $pending_pre_runs[$i]['problems']; ?></td>
<?php
     }
     if (isset($user_permssions) &&  strpos($user_permssions, 'softpath') !== false)
     {
?> 
                                   <td><?= $pending_pre_runs[$i]['collected']; ?></td>
                                   <td><?= $pending_pre_runs[$i]['received']; ?></td>
                                   <td><?= $pending_pre_runs[$i]['test_tissue']; ?></td>
<?php
     }
?>


<?php
     if (isset($user_permssions) &&  strpos($user_permssions, 'View_Only') === false && !$pool_uploading)
     {
?>    

               <?php
                    // historical sendouts do not have an orderered_test_id or a sample_log_book_id therefore do not provide an update button just notify the user that this can not be updated
                    if   (
                              (
                                   !isset($pending_pre_runs[$i]['sample_log_book_id']) || 
                                   empty($pending_pre_runs[$i]['sample_log_book_id']) 
                              ) 
                              &&
                              isset($pending_pre_runs[$i]['sent_out_count']) &&
                              $pending_pre_runs[$i]['sent_out_count'] == 1
                         )
                    {

               ?>
                                   <td title="no update">
                                        not updatable. 

                                   </td>
               <?php
                    }

                    // controls will not have a sample_log_book_id therefore refer to add_visit page
                    elseif   (
                                   (
                                        !isset($pending_pre_runs[$i]['sample_log_book_id']) || 
                                        empty($pending_pre_runs[$i]['sample_log_book_id'])
                                   ) &&
                                   $pending_pre_runs[$i]['sent_out_count'] == 0
                              )
                    {
               ?>
                                   <td title="Click on this button to access test visit info. Yellow=pending, green=completed, red=flagged step">
                                        
                                        <a href="?page=add_visit&visit_id=<?=$pending_pre_runs[$i]['visit_id'];?>" role="button" class="btn btn-circle<?php
                                             
                                             // Add btn color depending on if step completed, failed, or pending

                                             echo $preStepTracker->BtnFlaggedPassPendingStatus($passed_steps, $flagged_steps, 'add_visit');                                             
                                        ?>">
                                                                                
                                        </a>
                                   </td>
               <?php
                    }
                    else
                    {
               ?>                               
               				<td title="Click on this button to access test patient info. Yellow=pending, green=completed, red=flagged step">
                                        <a href="?page=all_sample_tests&sample_log_book_id=<?=$pending_pre_runs[$i]['sample_log_book_id'];?>" role="button" class="btn btn-circle <?php
                                             
                                             // Add btn color depending on if step completed, failed, or pending
                                             echo $preStepTracker->BtnFlaggedPassPendingStatus($passed_steps, $flagged_steps, 'add_visit');                                             
                                        ?>">
                                                                                
                                        </a>
                                   </td>
               <?php
                    }
               ?>
               
                                   <td>
                                        <a id="pending_toggle_<?= $pending_pre_runs[$i]['visit_id'];?>" <?php
                                             
                                             // Add btn color depending on if step completed, failed, or pending
                                             echo $preStepTracker->BtnFlaggedPassPendingStatus($passed_steps, $flagged_steps, 'pre_pending');                                             
                                        ?> class="pending_pre_sample_toggle btn btn-circle <?php
                                             
                                             // Add btn color depending on if step completed, failed, or pending                                          
                                             echo $preStepTracker->BtnFlaggedPassPendingStatus($passed_steps, $flagged_steps, 'pre_pending');
                                        ?>"
                                        <?php
                                             if (!$preStepTracker->FindPreviousStepComplete($completed_pre_steps, 'pre_pending'))
                                             {

                                                  echo ' disabled';
                                             }  
                                          
                                        ?>>
                                                                                
                                        </a>  
                                   </td>
               				<td>                               
                                        <a id="add_dna_conc_btn_<?= $pending_pre_runs[$i]['visit_id'];?>" href="?page=add_DNA_conc&visit_id=<?php 
                                             echo $pending_pre_runs[$i]['visit_id'];
                                             if ($pending_pre_runs[$i]['patient_id'] !== null)
                                             {
                                                  echo "&patient_id=".$pending_pre_runs[$i]['patient_id'];
                                             }
                                             
                                        ?>" role="button" class="btn btn-circle <?php
                                             
                                             // Add btn color depending on if step completed, failed, or pending                                          
                                             echo $preStepTracker->BtnFlaggedPassPendingStatus($passed_steps, $flagged_steps, 'add_DNA_conc');
                                             
                                             if (!$preStepTracker->FindPreviousStepComplete($completed_pre_steps, 'add_DNA_conc'))
                                             {
                                                  echo ' disabled';
                                             }  
                                          
                                        ?>"
                                        >
                                                                                
                                        </a>                
                                   </td>
               				<td>
                                        <a href="?page=add_amplicon_conc&visit_id=<?php 
                                             echo $pending_pre_runs[$i]['visit_id'];
                                             if ($pending_pre_runs[$i]['patient_id'] !== null)
                                             {
                                                  echo "&patient_id=".$pending_pre_runs[$i]['patient_id'];
                                             }
                                             
                                        ?>" role="button" class="btn btn-circle <?php
                                             
                                             // Add btn color depending on if step completed, failed, or pending
                                             echo $preStepTracker->BtnFlaggedPassPendingStatus($passed_steps, $flagged_steps, 'add_amplicon_conc'); 
                                             
                                             if (!$preStepTracker->FindPreviousStepComplete($completed_pre_steps, 'add_amplicon_conc'))
                                             {
                                                  echo ' disabled';
                                             }                                                                              
                                        ?>">                                                                                
                                        </a>  
                                   </td>
<?php
     }

     if (isset($_GET['ngs_panel_id'])  && $_GET['ngs_panel_id'] !== 2 && $pool_uploading)
     {
?>
                                   <td><?= $pending_pre_runs[$i]['full_sample_name'];?></td>


<?php
     }
     if   (
               isset($user_permssions) &&  
               strpos($user_permssions, 'View_Only') === false && 
               $page !== 'upload_NGS_data' && 
               (
                    (isset($_GET['sample_type']) && $_GET['sample_type'] !== 'Sent_Out') ||
                    (!isset($_GET['sample_type']))
               )
          )
     {
?>
               				<td>
                                   <?php
                                        // Only provide buttons for the myeloid panel on home page since it is not in the 
                                        // updated version of backing up data yet.
                                        if   (
                                                  $_GET['page'] === 'home' &&
                                                  isset($pending_pre_runs[$i]['ngs_panel']) && 
                                                  $pending_pre_runs[$i]['ngs_panel'] !== 'Myeloid/CLL Mutation Panel'
                                             )
                                        {
                                   ?>
                                             refer to pool
                                   <?php
                                        }
                                        else
                                        {

                                   ?>                                        
                                        <a role="button" class="btn btn-circle <?php
                                             
                                             // Add btn color depending on if step completed, failed, or pending
                                             echo $preStepTracker->BtnFlaggedPassPendingStatus($passed_steps, $flagged_steps, 'upload_NGS_data');

                                             // disable step if previous step has not been completed yet or current step was already completed.  This will prevent uploading data twice.
                                             if (!$preStepTracker->FindPreviousStepComplete($completed_pre_steps, 'upload_NGS_data') || $preStepTracker->StepStatus($completed_pre_steps, 'upload_NGS_data') === 'completed')
                                             {
                                                  echo ' disabled';
                                             }                                              
                                        ?>"
                                        href="?page=upload_NGS_data&visit_id=<?php 
                                             echo $pending_pre_runs[$i]['visit_id'];
                                             if ($pending_pre_runs[$i]['patient_id'] !== null)
                                             {
                                                  echo "&patient_id=".$pending_pre_runs[$i]['patient_id'];
                                             }

                                             if (isset($_GET['pool_chip_linker_id']))
                                             {
                                                  echo "&pool_chip_linker_id=".$_GET['pool_chip_linker_id'];
                                             }

                                             if (isset($pool['library_pool_id']))
                                             {
                                                  echo "&library_pool_id=".$pool['library_pool_id'];
                                             }

                                             if (isset($_GET['ngs_panel_id']))
                                             {
                                                  echo "&ngs_panel_id=".$_GET['ngs_panel_id'];
                                             }
                                             
                                        ?>" >
                                                                                
                                        </a>  
                                   <?php
                                        }
                                   ?>             
                                   </td>
                                   <td>
                                        <a href="?page=stop_test_in_log_book&visit_id=<?= $pending_pre_runs[$i]['visit_id'];?><?= isset($pending_pre_runs[$i]['sample_log_book_id']) && !empty($pending_pre_runs[$i]['sample_log_book_id']) ? '&sample_log_book_id='.$pending_pre_runs[$i]['sample_log_book_id']:''; ?><?= isset($pending_pre_runs[$i]['ordered_test_id']) && !empty($pending_pre_runs[$i]['ordered_test_id']) ? '&ordered_test_id='.$pending_pre_runs[$i]['ordered_test_id']:''; ?>&ngs_stage=pre_ngs_stage" class="btn btn-danger" title="Cancel Test">
                                             <span class="fa fa-times" ></span>          
                                        </a>
                                   </td>
<?php
     }
?>                                   
               			</tr>
               	<?php
               			}
               		}
               	?>
                    </tbody>               
               </table>
          </div>
     </div>
<?php
     if ($_GET['page'] === 'home')
     {
?>     
</fieldset>
<?php
     }
?>