<?php 
     if (isset($runInfoArray))
     {

?>
          <div class="container-fluid">
               <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                         <table class="formated_table">
                              <thead>
                                   <th class="d-none">run_id</th>

                                   <!-- intially visit id will not be available -->
                                   <th>Reporter#</th>
                                   <th>MD#</th>
                                   <th>mol#</th>
                                   <th>Patient Name</th>
                                   <th>Status</th>
                                   <th>Sample Name</th>
                                   <th>Received</th>
                                   <th>Last Report Step Completed</th>
                                   <th>Report editor</th> 
                              </thead>
                              <tbody>

                                   <!-- iterate over all pending runs and add each entry as a new row in the table-->
                                   <?php

                                        for($i=0;$i<sizeof($runInfoArray);$i++)
                                        {

                                   ?>
                                        <tr id="run_id_<?= $runInfoArray[$i]['run_id']; ?>">
                                             <td class="d-none"><?= $runInfoArray[$i]['run_id']; ?></td>
                                             <td><?= $runInfoArray[$i]['visit_id']; ?></td>
                                             <td><?= $runInfoArray[$i]['mol_num']; ?></td>
                                             <td><?= $runInfoArray[$i]['order_num']; ?></td>
                                             <td><?= $runInfoArray[$i]['patient_name']; ?></td>
                                             <td><?= $runInfoArray[$i]['status']; ?></td>
                                             <td><?= $runInfoArray[$i]['sample_name']; ?></td>
                                             
                                             <td><?= $runInfoArray[$i]['received'] !== DEFAULT_DATE_TIME ? $runInfoArray[$i]['received'] : ''; ?></td>
                                              
                                             <td><?php 
                                             if(isset($completed_steps[0]['step']))
                                             {
                                                  echo $completed_steps[0]['step']; 
                                             }
                                             else
                                             {
                                                  echo 'N/A';
                                             }
                                             ?>      
                                             </td> 
                                             <td><?php 
                                             if(isset($completed_steps[0]['user_name']))
                                             {
                                                  echo $completed_steps[0]['user_name']; 
                                             }
                                             else
                                             {
                                                  echo 'N/A';
                                             }
                                             ?>          
                                             </td> 
                                             
                                        </tr>
                                   <?php 
                                        }
                                   ?>
                              </tbody>
                         </table>
                    </div>
               </div>
          </div>

<?php
     }
?>

