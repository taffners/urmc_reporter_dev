<?php 

if ( isset($pending_runs) )
{
     // var_dump($pending_runs);
?>
     <fieldset id="outstanding-reports-fieldset">
          <?php
               if( (isset($_REQUEST['page']) && $_REQUEST['page'] == 'home') || (isset($_REQUEST['page']) && $_REQUEST['page'] == 'reports_unfinished'))
               {
          ?>
                    <legend>Samples with Outstanding Reports</legend>
          <?php
               }
               else if ($page === 'historic_controls')
               {
          ?>
                    <legend>Controls not pending</legend>
          <?php
               }
          ?>

          <?php
          
          $baseUrl = basename($_SERVER['REQUEST_URI']);
         // find if most recent pools or historic pools are selected
          if (isset($_GET['all_pending_reports']) && $_GET['all_pending_reports'] === 'all')
          {
             $recentClass = 'btn btn-warning btn-warning-hover pull-right';
             $historicClass = 'btn btn-success btn-success-hover pull-right';
             $baseUrl = str_replace('&all_pending_reports=all', '', $baseUrl);
          }
          else
          {
               $recentClass = 'btn btn-success btn-success-hover pull-right';
               $historicClass = 'btn btn-warning btn-warning-hover pull-right';
          }

          if ($page !== 'historic_controls')
          {
          ?>   

          <div class="row">
               <div class="col-md-6 col-lg-6">

               </div>
               <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
                    <a href="<?= $baseUrl;?>#outstanding-reports-fieldset" type="button" class="<?= $recentClass;?>">
                         Recent Reports
                    </a>
               </div>

               <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
                    <a id="historic-pools-btn" href="<?= $baseUrl;?>&all_pending_reports=all#outstanding-reports-fieldset" type="button" class="<?= $historicClass;?>">
                         All Pending reports
                    </a>
               </div>
          </div>
          <?php
          }
          ?>
          <div class="row" style="overflow-x:auto;">
               <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <table class="formated_table sort_table_no_inital_sort_no_paging">
                         <thead>
                              <th class="d-none">run_id</th>

                              <!-- intially visit id will not be available -->
                              <th>Reporter#</th>
                              <th>MD#</th>
                              <th>mol#</th>
                              <th>soft path#</th>
                              <th>soft lab#</th>
                              <th>MRN#</th>
                              <th>NGS Panel</th> 
                              <th>Patient Name</th>
                              <th>Status</th>
                              <th>Sample Name</th>                         
                              <th>Run Date</th>  

                              <!-- <th>Reporter TimeStamp</th>  -->
                              <th id="outstanding-reports-editors-completed-steps">Editors / Completed Steps</th>
                              
<?php
     if (isset($user_permssions) &&  strpos($user_permssions, 'softpath') !== false)
     {
          // This is added here for users who only have soft path access they will not be able to access the report.  Example when Sue was adding reports to patient chart.
?> 
                              <th>Collected</th> 
                              <th>Received</th>
                              <th>Specimen Type</th>
<?php
     }
?>


                         </thead>
                         <tbody>

                         <!-- iterate over all pending runs and add each entry as a new row in the table-->

               <?php

                    for($i=0;$i<sizeof($pending_runs);$i++)
                    {    

                         $run_id = $pending_runs[$i]['run_id'];
                         $visit_id = $pending_runs[$i]['visit_id'];
                         $patient_id = $pending_runs[$i]['patient_id'];

                         $completed_steps = $db->listAll('steps-run', $run_id);
                         
                         // sometimes reports are added to report_table without
                         // linking a patient or a visit.  Probably should make a delete 
                         // function here to remove all of these from database.  
                         // for now I'm going to hide them and see if this doesn't 
                         // screw up anything then I will delete them
                         if (!empty($visit_id) && !empty($patient_id))
                         {
               ?>
                              <tr id="run_id_<?= $pending_runs[$i]['run_id']; ?>_visit_id_<?= $visit_id; ?>_patient_id_<?= $patient_id; ?>_ngs_panel_id_<?= $pending_runs[$i]['ngs_panel_id']; ?>" class="<?php

                         if (isset($user_permssions) &&  strpos($user_permssions, 'View_Only') === false)
                         {
                                   echo 'hover_success_on open_report';
                         }?>">
                                   <td class="d-none"><?= $pending_runs[$i]['run_id']; ?></td>
                                   <td id="visit_id_<?= $visit_id; ?>"><?= $visit_id; ?></td>
                                   <td><?= $pending_runs[$i]['mol_num']; ?></td>
                                   <td><?= $pending_runs[$i]['order_num']; ?></td>
                                   <td><?= $pending_runs[$i]['soft_path_num']; ?></td>
                                   <td><?= $pending_runs[$i]['soft_lab_num']; ?></td>
                                   <td><?= $pending_runs[$i]['medical_record_num']; ?></td>
                                   <td><?= $pending_runs[$i]['ngs_panel'];?></td>
                                   <td><?= $pending_runs[$i]['patient_name']; ?></td>
                                   <td><?= $pending_runs[$i]['status']; ?></td>
                                   <td><?= $pending_runs[$i]['sample_name']; ?></td>
                                   
                                   <td><?= $pending_runs[$i]['run_date']; ?></td>
                                                            
                                   
                                   <td><?= str_replace(', ', '<br>', $pending_runs[$i]['editors_per_step']); ?></td>
                                   
<?php
     if (isset($user_permssions) &&  strpos($user_permssions, 'softpath') !== false)
     {
?>                                   
                                   <td><?= $pending_runs[$i]['collected']; ?></td>
                                   <td><?= $pending_runs[$i]['received']; ?></td>
                                   <td><?= $pending_runs[$i]['test_tissue']; ?></td>
<?php
     }
?>                                   
                              </tr>
                    <?php 
                         }
                    }
                    ?>
                         </tbody>
                    </table>
               </div>
          </div>
     </fieldset>
<?php
}
?>
