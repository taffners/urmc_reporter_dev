
<?php 

	if (isset($patientArray))
	{

?>
		<!-- add all patient info -->
		<div class="row">
			<h2 class="raised-card-header">Patient Information</h2>
			<ul class="raised-card">
				<?php
		;
				if(isset($patientArray[0]))
				{
					foreach($patientArray[0] as $key => $value)
					{
						if(!$utils->InputAID($key) && !$utils->InputNotIgnore($key))
						{
					?>

						<li><b><?= $utils->UnderscoreCaseToHumanReadable($key);?></b>: <?= $value;?></li>
				<?php
						}
					}
				}
				else
				{
					echo 'Not completed';
				}
				?>
			</ul>

		</div>
<?php

	}
?>