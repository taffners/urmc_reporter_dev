<?php 

if ( isset($last_completed_reports) )
{

?>
     <fieldset>
          <legend>Completed Reports</legend>

          <div class="row" style="overflow-x:auto;">
               <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <table class="formated_table sort_table_no_inital_sort">
                         <thead>                         
                              <!-- intially visit id will not be available -->
                              <th id="pdf-th">PDF</th>
                              <th>Reporter#</th>
                              <th>MD#</th>
                              <th>mol#</th>
                              <th>Soft Path#</th>
                              <th>Soft Lab#</th>
                              <th>MRN</th>
                              <th>Patient Name</th>
                              <th>Panel</th>
                              <th>TimeStamp</th> 
                              <th>Pool</th>
                    <?php
                         if (isset($user_permssions) && strpos($user_permssions, 'revise') !== false)
                         {
                    ?>
                              <th id="revise-th">Revise</th>
                    <?php
                         }
                    ?>
                         </thead>
                         <tbody>

                              <!-- iterate over all pending runs and add each entry as a new row in the table-->

                              <?php
                                   for($i=0;$i<sizeof($last_completed_reports);$i++)
                                   {           

                                        $base_url = '&run_id='.$last_completed_reports[$i]['run_id'].'&visit_id='.$last_completed_reports[$i]['visit_id'].'&patient_id='.$last_completed_reports[$i]['patient_id'].'&header_status=on&revise=on';

                                        if (isset($last_completed_reports[$i]['ngs_panel_id']))
                                        {
                                             $base_url.='&ngs_panel_id='.$last_completed_reports[$i]['ngs_panel_id'];
                                        }

                                        if (isset($last_completed_reports[$i]['orderable_tests_id']))
                                        {
                                             $base_url.='&orderable_tests_id='.$last_completed_reports[$i]['orderable_tests_id'];
                                        }

                                        if (isset($last_completed_reports[$i]['ordered_test_id']))
                                        {
                                             $base_url.='&ordered_test_id='.$last_completed_reports[$i]['ordered_test_id'];
                                        }

                                        if (isset($last_completed_reports[$i]['sample_log_book_id']))
                                        {
                                             $base_url.='&sample_log_book_id='.$last_completed_reports[$i]['sample_log_book_id'];
                                        }

                                         if (isset($last_completed_reports[$i]['single_analyte_pool_id']) && !empty($last_completed_reports[$i]['single_analyte_pool_id']))
                                        {
                                             $base_url.='&single_analyte_pool_id='.$last_completed_reports[$i]['single_analyte_pool_id'];
                                        }



                              ?>
                                   <tr>
                                        <td><a href="?page=download_report<?= $base_url;?>" type="button" class="btn btn-success btn-success-hover"><img src="images/adobe-pdf-icon.png" alt="pdf icon" style="height:30px;width:30px"></a></td>
                                        <td><?= $last_completed_reports[$i]['visit_id']; ?></td>
                                        <td><?= $last_completed_reports[$i]['mol_num']; ?></td>
                                        <td><?= $last_completed_reports[$i]['order_num']; ?></td>
                                        <td><?= $last_completed_reports[$i]['soft_path_num']; ?></td>
                                        <td><?= $last_completed_reports[$i]['soft_lab_num']; ?></td>
                                        <td><?= $last_completed_reports[$i]['medical_record_num']; ?></td>
                                        <td><?= $last_completed_reports[$i]['patient_name']; ?></td>
                                        <td><?php 

                                        if (isset($last_completed_reports[$i]['test_name']) && !empty($last_completed_reports[$i]['test_name']))
                                        {
                                             echo $last_completed_reports[$i]['test_name']; 
                                        }
                                        else if (isset($last_completed_reports[$i]['panel']) && !empty($last_completed_reports[$i]['panel']))
                                        {
                                             echo $last_completed_reports[$i]['panel']; 
                                        }

                                        ?>
                                             
                                        </td>
                                        <td><?= $last_completed_reports[$i]['time_stamp']; ?></td>
                                        <td>
                              <?php
                                        if (isset($last_completed_reports[$i]['single_analyte_pool_id']) && !empty($last_completed_reports[$i]['single_analyte_pool_id']))
                                        {
                              ?>
                                             <a href="?page=single_analyte_pool_home&single_analyte_pool_id=<?= $last_completed_reports[$i]['single_analyte_pool_id'];?>&view_only=yes" class="btn  btn-primary btn-primary-hover" role="button">View Pool</a>
                              <?php
                                        }
                              ?>

                                        </td>                                    
                              <?php
                                   if (isset($user_permssions) && strpos($user_permssions, 'revise') !== false)
                                   {
                              ?>                
                                   <td><a href="?page=generate_report<?= $base_url;?>&revise=on" class="btn  btn-danger btn-danger-hover" role="button">Revise</a></td>
                                   
                              <?php
                                   }
                              ?>                                   
                                   </tr>
                              <?php 
                                   }
                              ?>
                         </tbody>
                    </table>
               </div>
          </div>
     </fieldset>
<?php
}
?>
