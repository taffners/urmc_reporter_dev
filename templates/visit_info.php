<?php
	require_once('templates/lists/run_info_list.php');
?>

<div class="container-fluid">

	<div class="row">
		<?php
			require_once('templates/shared_layouts/report_progress_bar.php');
		?>

		<!-- update area -->
		<div id="toggle-report-progress-bar" class="col-xs-12 col-sm-10 col-md-10 col-lg-10" style="height:500px;">
			<form id="add_visit_info" class="form" method="post" class="form-horizontal">
				<fieldset>
					<legend id="new_legend">
						Add Visit Info
					</legend>
					<legend id="update_legend" class="d-none">
						Update Visit Info
					</legend>
					<div class="form-group row">
						<div class="col-xs-12 col-sm-4 col-md-4">
							<label for="soft_lab_num" class="form-control-label">Soft Lab #:<span class="required-field">*</span> </label>
						</div>
						<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
							<input type="text" class="form-control" id="soft_lab_num" name="soft_lab_num" maxlength="50" value="<?php 
								if (isset($visitArray[0]['soft_lab_num'])) 
								{
									echo $visitArray[0]['soft_lab_num']; 
								}
							?>" required/>
						</div>
					</div>
					<div class="form-group row">
						<div class="col-xs-12 col-sm-4 col-md-4">
							<label for="soft_path_num" class="form-control-label" >Soft Path #: <span class="required-field">*</span></label>
						</div>
						<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
							<input type="text" class="form-control" id="soft_path_num" name="soft_path_num" maxlength="50" value="<?php 
								if (isset($visitArray[0]['soft_path_num'])) 
								{
									echo $visitArray[0]['soft_path_num']; 
								}
							?>" required/>
						</div>
					</div>
					<div class="form-group row">
						<div class="col-xs-12 col-sm-4 col-md-4">
							<label for="mol_num" class="form-control-label">Molecular #: <span class="required-field">*</span></label>
						</div>
						<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
							<input type="text" class="form-control" id="mol_num" name="mol_num" maxlength="50" value="<?php 
								if (isset($visitArray[0]['mol_num'])) 
								{
									echo $visitArray[0]['mol_num']; 
								}
							?>" required/>
						</div>
					</div>
					<div class="form-group row">
						<div class="col-xs-12 col-sm-4 col-md-4">
							<label for="collected" class="form-control-label">Collected: <span class="required-field">*</span></label>
						</div>
						<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
							<input class="form-control date-time-picker" type="text" id="collected" name="collected" <?php
							if(isset($visitArray[0]['collected']))
							{
								echo 'value="'.$dfs->ChangeDateTimeFormatUS($visitArray[0]['collected'], FALSE).'"'; 
							}
							?> placeholder="MM/DD/YYYY HH:MM:SS"/>
						</div>
					</div>						
					<div class="form-group row">
						<div class="col-xs-12 col-sm-4 col-md-4">
							<label for="primary_tumor_site" class="form-control-label">Primary Tumor Site: <span class="required-field">*</span></label>
						</div>
						<div class="col-xs-12 col-sm-8 col-md-8 pull-left">

							<select class="form-control" name="primary_tumor_site" id="primary_tumor_site" required>
							<?php

								$panel_not_found = true;
								for($i=0;$i<sizeof($all_tissues);$i++)
                         			{
							?>
									<option value="<?= $all_tissues[$i]['tissue']; ?>" 
									<?php 
										if(isset($visitArray[0]['primary_tumor_site']) && $visitArray[0]['primary_tumor_site'] == $all_tissues[$i]['tissue']) 
										{
											$panel_not_found = false;
											echo 'selected'; 
											
										}

									?>>
										<?= $all_tissues[$i]['tissue']; ?>
									</option>
							<?php
								}

								if ($panel_not_found)
								{
									echo '<option value="" selected="selected"></option>';
								}
							?>
							</select>

						</div>
					</div>
					<div class="form-group row">
						<div class="col-xs-12 col-sm-4 col-md-4">
							<label for="test_tissue" class="form-control-label">Tissue Tested: <span class="required-field">*</span></label>
						</div>
						<div class="col-xs-12 col-sm-8 col-md-8 pull-left">

							<select class="form-control" name="test_tissue" id="test_tissue" required>
							<?php

								$panel_not_found = true;
								for($i=0;$i<sizeof($all_tissues);$i++)
                         			{
							?>
									<option value="<?= $all_tissues[$i]['tissue']; ?>" 
									<?php 
										if(isset($visitArray[0]['test_tissue']) && $visitArray[0]['test_tissue'] == $all_tissues[$i]['tissue']) 
										{
											$panel_not_found = false;
											echo 'selected'; 
											
										}

									?>>
										<?= $all_tissues[$i]['tissue']; ?>
									</option>
							<?php
								}

								if ($panel_not_found)
								{
									echo '<option value="" selected="selected"></option>';
								}
							?>
							</select>
						</div>
					</div>
					
					<div class="form-group row">
						<div class="col-xs-12 col-sm-4 col-md-4">
							<label for="block" class="form-control-label">Block: <span class="required-field">*</span></label>
						</div>
						<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
							<input type="text" class="form-control" id="block" name="block" maxlength="10" value="<?php 
								if (isset($visitArray[0]['block'])) 
								{
									echo $visitArray[0]['block']; 
								}
							?>" required/>
						</div>
					</div>
					<div class="form-group row">
						<div class="col-xs-12 col-sm-4 col-md-4">
							<label for="tumor_cellularity" class="form-control-label">Tumor Cellularity: <span class="required-field">*</span></label>
						</div>
						<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
							<input type="number" class="form-control" id="tumor_cellularity" name="tumor_cellularity" min="0" max="100" value="<?php 
								if (isset($visitArray[0]['tumor_cellularity'])) 
								{
									echo $visitArray[0]['tumor_cellularity']; 
								}
							?>" required/>
						</div>
					</div>
					<div class="form-group row">
						<div class="col-xs-12 col-sm-4 col-md-4">
							<label for="ngs_panel_id" class="form-control-label">NGS Panel Used: <span class="required-field">*</span></label>
						</div>
						<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
							<select class="form-control" name="ngs_panel_id" id="ngs_panel_id">

							<?php

								$panel_not_found = true;
								for($i=0;$i<sizeof($ngs_panels);$i++)
                         			{	
							?>
									<option value="<?= $ngs_panels[$i]['ngs_panel_id']; ?>" 
									<?php 
										if(isset($visitArray[0]['ngs_panel_id']) && $visitArray[0]['ngs_panel_id'] == $ngs_panels[$i]['ngs_panel_id']) 
										{
											$panel_not_found = false;
											echo 'selected'; 
										}

									?>>
										<?= $ngs_panels[$i]['type']; ?>
									</option>

							<?php
								}

								if ($panel_not_found)
								{
									echo '<option value="" selected="selected"></option>';
								}
							?>
							</select>
						</div>
					</div>
					<div class="form-group row">
						<div class="col-xs-12 col-sm-4 col-md-4">
							<label for="diagnosis_id" class="form-control-label">Patient Diagnosis: <span class="required-field">*</span></label>
						</div>
						<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
							<select class="form-control" name="diagnosis_id" id="diagnosis_id" required>

							<?php

								for($i=0;$i<sizeof($possible_diagnosis);$i++)
                         			{	
							?>
									<option value="<?= $possible_diagnosis[$i]['diagnosis_id']; ?>" 
									<?php 
										if(isset($visitArray[0]['diagnosis_id']) && $visitArray[0]['diagnosis_id'] == $possible_diagnosis[$i]['diagnosis_id']) 
										{
											echo 'selected'; 
										}

									?>>
										<?= $possible_diagnosis[$i]['diagnosis']; ?>
									</option>

							<?php
								}

							?>
							</select>
						</div>
					</div>
					<!-- add the common hidden fields for every form -->
					<?php
						require_once('templates/shared_layouts/form_hidden_fields.php')
					?>
					
					<!-- add submit and nav buttons -->
					<?php
						require_once('templates/shared_layouts/form_submit_nav_buttons.php')
					?>
				</fieldset>
			</form>
		</div>
	</div>
</div