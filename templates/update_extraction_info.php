<div class="container-fluid">

	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

			<form class="form" method="post" class="form-horizontal">
				<fieldset class="alert alert-danger">
					<legend>
						Update extraction info and delete old extraction info.
					</legend>
<?php
					////////////////////////////////////////////////////////////////
					// Header
					////////////////////////////////////////////////////////////////
					require_once('templates/shared_layouts/sample_overview.php');
?>
					<hr class="section-mark">
					<div class="form-group row">
						<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
							<label for="extraction_method" class="form-control-label">Extraction Method:</label>
						</div>
						<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
							
							<label class="radio-inline">
								<input type="radio" name="extraction_method" value="Maxwell" <?= $utils->GetValueForUpdateRadioCBX($extraction_array, 'extraction_method', 'Maxwell');?>>
								Maxwell
							</label>
							<label class="radio-inline">
								<input type="radio" name="extraction_method" value="Qiagen" <?= $utils->GetValueForUpdateRadioCBX($extraction_array, 'extraction_method', 'Qiagen');?>>
								Qiagen
							</label>
						</div>
					</div>
				
					
					<hr class="section-mark">
			<?php
			
			if 	(
					isset($testInfo[0]['quantification_info']) && 
					$testInfo[0]['quantification_info'] === 'yes' &&
					isset($testInfo[0]['extraction_info']) && 
					$testInfo[0]['extraction_info'] === 'no' 
				)
			{
					$elution_vol_label = 'Volume Received';
			?>
					
					<hr class="section-mark">
			<?php
			}
			if 	(
					isset($testInfo[0]['quantification_info']) && $testInfo[0]['quantification_info'] === 'yes' 
				)
			{
			?>		
					<div class="form-group row">
						<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
							<label for="quantification_method" class="form-control-label">Quantification Method: <span class="required-field">*</span></label>
						</div>
						<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
							<label class="radio">
								<input type="radio" name="quantification_method" value="nanodrop" <?= $utils->DefaultRadioBtn($extraction_array, 'quantification_method', 'nanodrop');?>>
								Nanodrop
							</label>
							<label class="radio">
								<input type="radio" name="quantification_method" value="quantus" <?= $utils->GetValueForUpdateRadioCBX($extraction_array, 'quantification_method', 'quantus');?>>
								Quantus
							</label>
							<label class="radio">
								<input type="radio" name="quantification_method" value="qubit" <?= $utils->GetValueForUpdateRadioCBX($extraction_array, 'quantification_method', 'qubit');?>>
								Qubit
							</label>
							<label class="radio">
								<input type="radio" name="quantification_method" value="NA" <?= $utils->GetValueForUpdateRadioCBX($extraction_array, 'quantification_method', 'NA');?>>
								NA
							</label>						
						</div>

						<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
							<label for="dna_conc_control_quant" class="form-control-label">DNA Concentration Quantification Control (ng/&mu;l):</label>
						</div>
						<div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
							<input type="number" class="form-control" id="dna_conc_control_quant" name="dna_conc_control_quant" step="0.01" min="0" value="<?= $utils->GetValueForUpdateInput($extraction_array, 'dna_conc_control_quant');?>"/>
						</div>
					</div>
			<?php
			}
			?>

					<div class="form-group row">
						<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
							<label for="stock_conc" class="form-control-label">Stock Concentration (ng/&mu;l) (C<sub>1</sub>):</label>
						</div>
						<div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
							<input type="number" class="form-control" id="stock_conc" name="stock_conc" step="0.01" min="0" value="<?= $utils->GetValueForUpdateInput($extraction_array, 'stock_conc');?>"/>
						</div>
						<div class="col-xs-12 col-sm-12 col-md-1 col-lg-1">
							<label for="purity" class="form-control-label">260 / 280:</label>
						</div>
						<div class="col-xs-12 col-sm-12 col-md-1 col-lg-1">
							<input type="number" class="form-control" id="purity" name="purity" step="0.01" min="0" value="<?= $utils->GetValueForUpdateInput($extraction_array, 'purity');?>"/>
						</div>
						<div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
							<label for="elution_volume" class="form-control-label">Elution Volume: <span class="required-field">*</span></label>

						</div>
						<div class="col-xs-12 col-sm-12 col-md-1 col-lg-1">
							<input type="number" class="form-control" id="elution_volume" name="elution_volume" step="0.1" min="0" required="required" value="<?= $utils->GetValueForUpdateInput($extraction_array, 'elution_volume');?>"/>
						</div>

					</div>
					<div class="form-group row">
						<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
							<label for="dilution_exist" class="form-control-label">Will you be making a dilution?: <span class="required-field">*</span></label>
						</div>
						<div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
							<label class="radio">
								<input type="radio" name="dilution_exist" value="yes" required <?= $utils->GetValueForUpdateRadioCBX($extraction_array, 'dilution_exist', 'yes');?>>
								Yes
							</label>
							<label class="radio">
								<input type="radio" name="dilution_exist" value="no" <?= $utils->GetValueForUpdateRadioCBX($extraction_array, 'dilution_exist', 'no');?>>
								No
							</label>						
						</div>

						<span id="dilution-type-span" class="<?php 

						if 	(
									isset($extraction_array[0]['dilution_exist']) && 
									$extraction_array[0]['dilution_exist'] === 'no'
							)
						{
							echo 'd-none';
						}
						?> col-xs-12 col-sm-12 col-md-7 col-lg-7">
							<div class="row">
								<div class="col-xs-12 col-sm-12 col-md-5 col-lg-5">
									<label for="dilution_type" class="">What kind of dilution will you be making?: <span class="required-field">*</span></label>
								</div>
								<div class="col-xs-12 col-sm-12 col-md-7 col-lg-7">
									
									<div class="form-check">
									
										<input id="dilution-type-precise" type="radio" name="dilution_type" value="precise" <?= $utils->GetValueForUpdateRadioCBX($extraction_array, 'dilution_type', 'precise');?>>
										<label class="form-check-label">
											Precise dilution using <?= SITE_TITLE;?> dilution calculator
										</label>
									</div>
									<label class="radio">
										
										<input type="radio" name="dilution_type" value="x_dilution" <?= $utils->GetValueForUpdateRadioCBX($extraction_array, 'dilution_type', 'x_dilution');?>>
										<label class="form-check-label">
											X Dilution
										</label>			
									</label>			
								</div>
							</div>
						</span>

					</div>
					

					<div class="form-group <?php 

						if 	(
								(
									isset($extraction_array[0]['dilution_type']) && 
									$extraction_array[0]['dilution_type'] === 'x_dilution'
								)
								||
								(
									isset($extraction_array[0]['dilution_exist']) && 
									$extraction_array[0]['dilution_exist'] === 'no'
								)		
							)								
						{
							echo 'd-none';
						}
						?>" id="dilution-calculator">
						<div class="row">
							<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
								<hr class="section-mark">
							
								<h1>Dilution calculator</h1>
								<div class="alert alert-info">
									Equation used:<br> C<sub>1</sub>V<sub>1</sub> = C<sub>2</sub>V<sub>2</sub>
								</div>
								<div id="alert-stock-conc-empty" class="alert alert-danger d-none">
									<h1>Add Stock Concentration </h1>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
								<label for="vol_dna" class="form-control-label">Volume DNA (&mu;l) (V<sub>1</sub>):</label>
							</div>
							
							<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
								<label for="vol_eb" class="form-control-label">Volume EB (&mu;l):</label>
							</div>
							
							<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
								<label for="dilution_final_vol" class="form-control-label">Dilution final volume (&mu;l) (V<sub>2</sub>): <span class="required-field">*</span></label>
							</div>
							
							<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
								<label for="target_conc" class="form-control-label">Target Concentration (ng/&mu;l) (C<sub>2</sub>): <span class="required-field">*</span></label>
							</div>
						</div>
						
						<div class="row">

							<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
								
								<input type="number" class="form-control" id="vol_dna" name="vol_dna" step="0.1" min="0" readonly value="<?= $utils->GetValueForUpdateInput($extraction_array, 'vol_dna');?>"/>
													
							</div>

							<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
								
								<input type="number" class="form-control" id="vol_eb" name="vol_eb" step="0.1" min="0" readonly value="<?= $utils->GetValueForUpdateInput($extraction_array, 'vol_eb');?>"/>
													
							</div>

							<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
								
								<input type="number" class="form-control" id="dilution_final_vol" name="dilution_final_vol" step="0.1" min="0" value="<?= $utils->GetValueForUpdateInput($extraction_array, 'dilution_final_vol', 50);?>"/>
													
							</div>

							<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
								
								<input type="number" class="form-control" id="target_conc" name="target_conc" step="0.1" min="0" value="<?= $utils->GetValueForUpdateInput($extraction_array, 'target_conc', 50);?>"/>
													
							</div>
						</div>
						<div class="row">
							<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
								<label for="dilution_conc" class="form-control-label">Actual Dilution Final Concentration (ng/&mu;l) (C<sub>2</sub>): </label>
							</div>
							<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
								
								<input type="number" class="form-control" id="dilution_conc" name="dilution_conc" step="0.1" min="0" value="<?= $utils->GetValueForUpdateInput($extraction_array, 'dilution_conc');?>"/>
													
							</div>
						</div>
					</div>
					<div class="form-group <?php 

						if 	(
								(
									isset($extraction_array[0]['dilution_type']) && 
									$extraction_array[0]['dilution_type'] === 'precise'
								)
								||
								!isset($extraction_array[0]['dilution_type'])
							)
						{
							echo 'd-none';
						}
						?>" id="x-dilution">
						<div class="row">
							<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
								<hr class="section-mark">
								<h3>X Dilution</h3>
							</div>
						</div>
						<div class="row">
							<div class="col-xs-12 col-sm-12 col-md-5 col-lg-5">
								
								<div class="row">
									<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
										<label for="times_dilution" class="form-control-label">Number X Dilution <br>(ex. enter 25 for 25X dilution): <span class="required-field">*</span></label>
									</div>
									<div class="col-xs-12 col-sm-4 col-md-6 col-lg-6">
										<input type="number" class="form-control" id="times_dilution" name="times_dilution" min="0" value="<?= $utils->GetValueForUpdateInput($extraction_array, 'times_dilution');?>"/>
									</div>
								</div>
							</div>			
						</div>
					</div>
					<div class="form-group row">
						<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
							<label for="problems" class="form-control-label">Please explain why you are updating this extraction and what you updated.	<span class="required-field">*</span>
							</label>
							
						</div>

						<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
							<textarea class="form-control" name="comments" rows="4" maxlength="65535" style="margin-bottom: 10px;"></textarea>
						</div>
					</div>
					<div class="form-group row">
						<div class="offset-xs-4 offset-sm-4 offset-md-4 offset-lg-4 col-xs-2 col-sm-2 col-md-2 col-lg-2">
							<button type="submit" id="<?= $page;?>_submit" name="<?= $page;?>_submit" value="Submit"  class="btn btn-primary btn-primary-hover">
								Submit (Warning this will delete old info)
							</button>
						</div>
					</div>


				</fieldset>
			</form>
		</div>
	</div>
</div>