<?php
if (isset($_GET['pool_chip_linker_id']) && isset($poolStepTracker) && isset($_GET['ngs_panel_id']))
{

?>
	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			<?php
			     require_once('templates/tables/pending_ngs_library_pool_table.php');
			?>
		</div>
	</div>
	<div class="row">
		<!-- report progress bar toggles -->	
		<?php
		require_once('templates/shared_layouts/report_toggle_show_status.php');
		?>

		<!-- Progress bar -->
		<?php
		require_once('templates/shared_layouts/pool_step_nav_bar.php')
		?>
		<!-- update area -->
		<div id="toggle-report-progress-bar" class="col-xs-10 col-sm-10 col-md-10 col-lg-10">
			<form id="add_pool_conc" class="form" method="post" class="form-horizontal">
				<fieldset>

					<legend>
						<?= $utils->UnderscoreCaseToHumanReadable($page);?>
					</legend>
					<div class="row">
						<ul class="hover-card-list" style="font-size:20px;">
	<?php

		// add list entry of barcodes chosen previously
		if (isset($min_max_barcode))
		{
	?>						
							<li>
					<?php
						if (isset($min_max_barcode) && sizeOf($min_max_barcode) > 1)
						{
					?>
								Barcodes used for Pool: <?= $min_max_barcode[0]['reagent_name'];?> and <?= $min_max_barcode[1]['reagent_name'];?>
					<?php
						}
						else
						{
					?>
								Barcode used for Pool: <?= $min_max_barcode[0]['reagent_name'];?> 
					<?php
						}

					?>		
							</li>
	<?php
		}
	?>
						</ul>
					</div>
					
	<?php
		// have the user choose a start value
		if (!isset($_GET['start_barcode']) && isset($min_max_barcode) && empty($visits_in_pool[0]['barcode_ion_torrent']))
		{
	?>				
					<div class="row">
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 alert alert-info">
							<h1>Choose the barcode of the first sample in chip A1 and then all the rest will be automatically generated. It is recommended to keep barcodes in the order that they are automatically.  Unique Barcode values are required.  </h1>
						</div>
					</div>
					<div class="form-group row">
						<div class="col-xs-12 col-sm-4 col-md-4">
							<label for="start_barcode" class="form-control-label" >Start Barcode: <span class="required-field">*</span></label>
						</div>
						<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
							<input type="number" class="form-control" id="start_barcode" name="start_barcode" required="required" min="<?= $min_max_barcode[0]['start'];?>" max="<?= $min_max_barcode[0]['stop'];?>"/>
						</div>
					</div>

					<div class="row">
						<div class="col-xs-12 col-sm-4 col-md-4">

						</div>
						<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
	<?php
	$step_status = $poolStepTracker->StepStatus($completed_steps, 'pool_qc_backup_data');
			
	if ($step_status === 'not_complete')
	{
		?>
							<button type="submit" id="submit-start-barcode" name="submit-start-barcode" value="Submit" class="btn btn-primary btn-primary-hover">
								Submit
							</button>
	<?php
	}
	?>							
						</div>
					</div>
	<?php
		}

		// Only show table to add barcodes once a start barcode value was entered
		if ((isset($_GET['start_barcode']) && isset($min_max_barcode)) || !empty($visits_in_pool[0]['barcode_ion_torrent']))
		{
	?>					
						
					<div class="row">
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 alert alert-info">
							<h1>It is recommended to keep barcodes in the order that they are automatically.  Unique Barcode values are required. </h1>
						</div>
					</div>
					<div class="row">
						<h3 class="raised-card-subheader">Visits in Library Pool</h3>

						<table class="formated_table">
							<thead>
								<th>Reporter#</th>
								<th>Order #</th>
								<th>Chip / Flow Cell</th>			
								<th>Sample Name</th>
								<th>Patient Name</th>
								<th>MRN</th>
								<th>Barcode</th>
							</thead>
							<tbody>
			<?php
				if (isset($visits_in_pool))
				{
					// current start and stops of barcode

					if (!empty($visits_in_pool[0]['barcode_ion_torrent']))
					{
						$curr_start = intval($visits_in_pool[0]['barcode_ion_torrent']);
					}
					else
					{
						$curr_start = intval($_GET['start_barcode']);
					}

					
					$stop = intval($min_max_barcode[0]['stop']);
					
					$curr_barcode = $curr_start-1;

					foreach ($visits_in_pool as $key => $visit)
					{	
						$curr_barcode++;

						////////////////////////////////////////////////////
						// If barcode already set??
						////////////////////////////////////////////////////
						if (isset($visit['barcode_ion_torrent']) && !empty($visit['barcode_ion_torrent']))
						{
							$curr_barcode = intval($visit['barcode_ion_torrent']);

						}

						////////////////////////////////////////////////////
						// Restart at 1 if $curr_barcode === $curr_stop
						// Used if barcode information not available already visit
						////////////////////////////////////////////////////
	  			
						else if ($curr_barcode > $stop)
						{			
								echo 'here<br>';
							$curr_barcode = intval($min_max_barcode[0]['start']);
						}

						
			?>
								<tr>
									<td>
										<?= $visit['visit_id'];?>
										<input class="d-none" type="number" name="visit_<?=$key;?>[visit_id]" value="<?= $visit['visit_id'];?>" style="width:45px;"/>
										<input class="d-none" type="number" name="visit_<?=$key;?>[visits_in_pool_id]" value="<?= $visit['visits_in_pool_id'];?>" style="width:45px;"/>
									</td>
									<td><?= $visit['order_num'];?></td>

									<td><?= $visit['chip_flow_cell'];?></td>
									<td><?= $visit['sample_name'];?></td>
									<td><?= $visit['patient_name'];?></td>
									<td><?= $visit['medical_record_num'];?></td>
									<td>
										<span class="required-field">*</span> <input class="unique-value-required" type="number" name="visit_<?=$key;?>[barcode_ion_torrent]" value="<?= $curr_barcode;?>" style="width:65px;" required="required"/>
									</td>
									
								</tr>
			<?php
					}
				}
			?>			
							</tbody>
						</table>
					</div>
					
				<?php


					// add form submit, previous, and next buttons.
					require_once('templates/shared_layouts/form_pool_submit_nav_buttons.php');
				?>
	<?php
		} // table to add barcodes.  A separate submit btn is used to chose barcode start value
	?>
				</fieldset>
			</form>
		</div>


	<?php

	}
	?>
		

		</div>
	</div>
</div>