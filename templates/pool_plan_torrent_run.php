<?php
if (isset($_GET['pool_chip_linker_id']) && isset($poolStepTracker) && isset($_GET['ngs_panel_id']))
{

?>
	<!-- report progress bar toggles -->	
	<?php
	require_once('templates/shared_layouts/report_toggle_show_status.php');
	?>

	<!-- Progress bar -->
	<?php
	require_once('templates/shared_layouts/pool_step_nav_bar.php')
	?>
	<!-- update area -->
	<div id="toggle-report-progress-bar" class="col-xs-10 col-sm-10 col-md-10 col-lg-10">
		<form id="add_pool_conc" class="form" method="post" class="form-horizontal">
			<fieldset>

				<legend>
					Download Ion Torrent Run Templates
				</legend>
				
				<div class="row">
					<div class="alert alert-info">
						<h1><strong>IMPORTANT!!</strong> Use a barcode scanner for the following to fields.  This will ensure no typos are introduced.</h1>

						<div class="form-group row">
							<div class="col-xs-12 col-sm-4 col-md-4">
								<label for="entry-type-toggle" class="form-control-label">Entry Type Toggle: <span class="required-field">*</span></label>
							</div>
							<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
								<div class="radio">
									<label class="radio-inline"><input id="barcode-input-selection" type="radio" name="entry-type-toggle" value="scanner" required="required" checked>Barcode Scanner</label>
								
									<label class="radio-inline"><input id="human-input-selection" type="radio" name="entry-type-toggle" value="human" required="required" >Human Typed</label>
								</div>
							</div>
						</div>

						
<?php
	$chip_a_used = False;
	$chip_b_used = False;
	// only display if chip a being is being used
	if 	(
				isset($pending_pools[0]['count_chip_a']) &&
				is_numeric($pending_pools[0]['count_chip_a']) &&
				intval($pending_pools[0]['count_chip_a']) > 0
		)
	{
		$chip_a_used = True;
?>							
						<div class="form-group row">
							<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
								<label for="chip_flow_cellA" class="form-control-label" >Chip A Barcode: <span class="required-field">*</span> <br>(example: 21DAEJ00624241530v1)</label>
							</div>
							<div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">
								<input id="chip_flow_cellA" type="text" class="form-control turn-off-enter-key" name="chip_flow_cellA" required="required" pattern=".{19}"/>
							</div>
						</div>
<?php
	}

	// only display if chip b being is being used
	if 	(
				isset($pending_pools[0]['count_chip_b']) &&
				is_numeric($pending_pools[0]['count_chip_b']) &&
				intval($pending_pools[0]['count_chip_b']) > 0
		)
	{
		$chip_b_used = False;
?>						
						<div class="form-group row">
							<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
								<label for="chip_flow_cellB" class="form-control-label" >Chip B Barcode: <span class="required-field">*</span> <br>(example: 21DAEJ00624241530v1)</label>
							</div>
							<div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">
								<input id="chip_flow_cellB" type="text" class="form-control turn-off-enter-key" name="chip_flow_cellB" required="required" pattern=".{19}"/>
							</div>
						</div>
<?php
	}
	// only display if chip a or b being is being used
	if ($chip_a_used || $chip_b_used)
	{
?>						
						<div class="form-group row">
							<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
								<button type="button" id="download-ofa-template" class="btn btn-success btn-success-hover" data-pool_chip_linker_id="<?=$_GET['pool_chip_linker_id'];?>">
									JS Download OFA Run Templates
								</button>	
							</div>
							<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
								<button type="submit" id="download-ofa-run-templates-submit" name="download-ofa-run-templates-submit" value="Submit" class="btn btn-primary btn-primary-hover">
									PHP Download OFA Run Templates			
								</button>
							</div>
						</div>		
<?php
	}
?>					
						<div class="form-group row">
							<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">			
			  					<span><strong>Steps to run Ion Torrent:</strong></span>
			  					<ol>
			  						<li><strong>Scan</strong> chip barcodes.</li>
			  						<li><strong>Download</strong> OFA run templates</li>
			  						<li>Go to Torrent Server <?= $API_Utils->getServerLink('Torrent_Server');?></li>
			  						<li>Click on the <strong>Plan tab.</strong> Then click on <strong><?= OFA_WORKFLOW;?></strong><br><img src="images/Torrent_server_plan_template.png" alt="Image of Torrent Server and how to plan a run template." style="max-width:100%"></li>
			  						<li style="margin-top:15px;">Change <strong>Run Plan Name</strong>.  Before the default name (yellow highlighted below) add the date (YYMMDD) and chip (red underlined below).</li>
			  						<li>Click on <strong>Load Samples Table</strong> button.  <strong>Upload</strong> Template Chip A downloaded in step 1.  Then <strong>repeat steps 3, 4, and 5 for Template Chip B</strong>.  <br><img src="images/Torrent_server_plan_template2.png" alt="Image of Torrent Server and how to plan a run template." width="100%"></li>
			  						<li>Click the <strong>Done button</strong> below once everything is done.</li>
			  					</ol>
			  				</div>
			  			</div>
					</div>
				</div>


				<?php
				// add form submit, previous, and next buttons.
				require_once('templates/shared_layouts/form_pool_submit_nav_buttons.php');
				?>
<?php
	// This was turned off because an error occurred on a pool and I couldn't figure out why.  TempA and TempB were empty and no header was present even though a template was downloading.  This section is not essential so I will just turn it off.
	$skip = true;
	if (!$skip && isset($tempA) && isset($tempB) && isset($col_header))
	{

?>
				<div class="row" style="overflow-x:auto;">
					<h3 class="raised-card-subheader">Run Template A</h3>

					<table class="formated_table">
						<thead>
<?php
				foreach ($col_header as $key => $col)
				{
?>
							<th><?= $col;?></th>
<?php
				}
?>
						</thead>
						<tbody>
<?php
				foreach ($tempA as $key => $row)
				{
?>
							<tr>
<?php
					foreach ($row as $col_name => $row_data)
					{
?>
								<td><?= $row_data;?></td>
<?php
					}
?>
							</tr>
<?php
				}
?>
						</tbody>
					</table>
				</div>
				<div class="row">
					<h3 class="raised-card-subheader">Run Template B</h3>

					<table class="formated_table">
						<thead>
<?php
				foreach ($col_header as $key => $col)
				{
?>
							<th><?= $col;?></th>
<?php
				}
?>
						</thead>
						<tbody>
<?php
				foreach ($tempB as $key => $row)
				{
?>
							<tr>
<?php
					foreach ($row as $col_name => $row_data)
					{
?>
								<td><?= $row_data;?></td>
<?php
					}
?>
							</tr>
<?php
				}
?>
						</tbody>
					</table>
				</div>
<?php
	}
?>								
			</fieldset>
		</form>
	</div>


<?php

}
?>
	

	</div>
</div>