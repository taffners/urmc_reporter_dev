<div class="container-fluid">


		<fieldset class="alert alert-danger">
			<legend class="show">Update Run Number</legend>

			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
					<h1>Warning !!! You are about to update the single analyte pool run number<br>
					<br>
					This is permanent change and previous run number will not be kept!!.</h1>
				</div>
				<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
					<img src="images/warning.png">
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
					<hr class="thick-red-line">
				</div>
			</div>

			<?php
				require_once('templates/tables/single_analyte_pool_table.php');
			?>

			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
					<hr class="thick-red-line">
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
					<form class="form" method="post" class="form-horizontal">
						<div class="form-group row">
							
						
							<div class="col-xs-12 col-sm-4 col-md-4">
								<label for="run_number" class="form-control-label">New Run Number (DO NOT INCLUDE YEAR): <span class="required-field">*</span></label>
							</div>
							
							
							<div class="col-xs-12 col-sm-8 col-md-2 col-lg-2">							
								<div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
									<input type="number" name="run_number" min="0" required="required" step="1" />
								</div>
							</div>
						
						</div>

						<div class="form-group row d-print-none">
							<div class="col-xs-12 col-sm-4 col-md-4">
								<label for="comments" class="form-control-label">Add an explanation of why you are updating the run number:</label>
							</div>


							<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
								<textarea class="form-control" name="comments" rows="4" maxlength="62535" style="margin-bottom: 10px;"></textarea>
							</div>
						</div>				

						<div class="form-group row">
							<div class="offset-xs-4 offset-sm-4 offset-md-4 offset-lg-4 col-xs-2 col-sm-2 col-md-2 col-lg-2">
								<button type="submit" id="update_run_number_submit" name="update_run_number_submit" value="Submit"  class="btn-lg btn-danger btn-danger-hover">
									Submit
								</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</fieldset>
	</div>
</div>