<div class="container-fluid">
<?php
if (isset($_GET['pool_chip_linker_id']) && isset($poolStepTracker) && isset($_GET['ngs_panel_id']))
{

?>
	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			<?php
			     require_once('templates/tables/pending_ngs_library_pool_table.php');
			?>
		</div>
	</div>

	<div class="row">
		<!-- report progress bar toggles -->	
		<?php
		require_once('templates/shared_layouts/report_toggle_show_status.php');
		?>

		<!-- Progress bar -->
		<?php
		require_once('templates/shared_layouts/pool_step_nav_bar.php')
		?>
		<!-- update area -->
		<div id="toggle-report-progress-bar" class="col-xs-10 col-sm-10 col-md-10 col-lg-10">
			<form class="form" method="post" class="form-horizontal">
				<fieldset>

					<legend>
						<?= $page_title;?>
					</legend>			


			<?php
				///////////////////////////////////////////////////
				// Assign excel workbook number if not already assigned
				///////////////////////////////////////////////////
				if(!isset($pending_pools[0]['excel_workbook_num_id']) && empty($pending_pools[0]['excel_workbook_num_id']) )
				{
			?>

					<div class="alert alert-info" style="font-size:18px;">
						The only reason you should be reusing a <?= $name_file;?> is this pool is being made because a sample failed the intial pool. 
					</div>

					<div class="form-group row">
						<div class="col-xs-12 col-sm-4 col-md-4">
							<label for="status_excel_workbook" class="form-control-label">Will this pool be using a new <?= $name_file;?>? <span class="required-field">*</span></label>
						</div>
						<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
							<div class="radio">
								<label class="radio-inline"><input id="status_excel_workbook_new" type="radio" name="status_excel_workbook" required="required" value="new" checked>new</label>
							
								<label class="radio-inline"><input id="status_excel_workbook_old" type="radio" name="status_excel_workbook" value="old">old</label>
							</div>
						</div>
					</div>	

					<div id="excel-workbork-num-div" class="form-group row" style="display:none;">
						<div class="col-xs-12 col-sm-4 col-md-4">
							<label for="excel_workbook_num" class="form-control-label">Excel Workbook Num: <span class="required-field">*</span></label>
						</div>
						<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
							<input type="number" class="form-control capitalize" id="excel_workbook_num" name="excel_workbook_num" max="<?= $next_auto_num[0]['AUTO_INCREMENT'] - 1;?>" min="0"/>
						</div>
					</div>	
			<?php	
				}			
				///////////////////////////////////////////////////
				// Show excel workbook number if already assigned
				///////////////////////////////////////////////////
				else if(isset($pending_pools[0]['excel_workbook_num_id']) && !empty($pending_pools[0]['excel_workbook_num_id']) )
				{			
			?>

					<div class="alert alert-info" style="font-size:18px;">
						<b>Excel workbook number is</b>: <?= $pending_pools[0]['excel_workbook_num_id'];?>
						<br><br>
						<b>Library pool id for Chip A is</b>: <?= $pending_pools[0]['library_pool_id_A'];?>
						<br><br>
					<?php
						if (isset($pending_pools[0]['library_pool_id_B']) && !empty($pending_pools[0]['library_pool_id_B']))
						{
					?>
						<b>Library pool id for Chip B is</b>: <?= $pending_pools[0]['library_pool_id_B'];?>
					<?php
						}
					?>
					</div>
			<?php
				}
			?>
								
					

					<?php
					// add form submit, previous, and next buttons.
					require_once('templates/shared_layouts/form_pool_submit_nav_buttons.php');
					?>
					<div class="alert alert-info">
						Copy and paste the table below to fill in your excel workbook sample name and patient name.  Both columns can be copied at the same time.  
					</div>
					<table class="report_table">
						<tbody>

	<?php
		if (isset($visits_in_pool))
		{
			foreach ($visits_in_pool as $key => $visit)
			{		
	?>
					
							<tr>
								<td><?= $visit['sample_name'];?></td>
								<td><?= $visit['patient_name'];?></td>
							</tr>
						
	<?php
			}
		}
	?>
						</tbody>
					</table>

								
				</fieldset>
			</form>
		</div>

	<?php
		}
	?>	
	</div>
</div>