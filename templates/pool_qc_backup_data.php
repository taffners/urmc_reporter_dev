<?php
if (isset($_GET['pool_chip_linker_id']) && isset($poolStepTracker) && isset($_GET['ngs_panel_id']) )
{
?>
	<div class="container-fluid">
		<div class="row">
			<!-- report progress bar toggles -->	
			<?php
			require_once('templates/shared_layouts/report_toggle_show_status.php');
			?>

			<!-- Progress bar -->
			<?php
			require_once('templates/shared_layouts/pool_step_nav_bar.php')
			?>
			<div id="toggle-report-progress-bar" class="col-xs-10 col-sm-10 col-md-10 col-lg-10">
				<form id="make_run_template_form" class="form" method="post" class="form-horizontal">
							

		<?php
			
				require_once('templates/tables/pool_run_qc_tables.php');
					
				if (!$errors_found)
				{
					// add form submit, previous, and next buttons.
					require_once('templates/shared_layouts/form_pool_submit_nav_buttons.php');
				}	
		?>
				</form>
			</div>	
		</div>
	</div>
<?php
}
?>	
