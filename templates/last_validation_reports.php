<div class="container-fluid">
	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
<?php
	
if (isset($validations))
{
	require_once('templates/shared_layouts/page_validation_layout_header.php');

	foreach ($validations as $key => $validation)	
	{
		// since view expects page_validation_info to be the first element in an array add to a array
		$page_validation_info = array(0 => $validation);
		$pageArray = $qc_db->listAll('qc_db-qc-page-info', $validation['page_id']);
		$qc_checklist_items = $qc_db->listAll('qc_db-qc-checklist-items-for-validation', $validation['page_validation_id']);

		require('templates/shared_layouts/page_validation_layout.php');
?>
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
					<hr class="thick-blue-line">
				</div>
			</div>
<?php		
	}
}
?>
		</div>
	</div>
</div>