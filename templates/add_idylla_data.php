<?php
	require_once('templates/lists/run_info_list.php');


$step_status = $stepTracker->StepStatus($completed_steps, $page);

?>

<div class="container-fluid">
	<div class="row">
		<?php
			require_once('templates/shared_layouts/report_progress_bar.php');
		?>



		<!-- update area -->
		<div id="toggle-report-progress-bar" class="col-xs-10 col-sm-10 col-md-10 col-lg-10" style="height:500px;">

			<?php
				if ($step_status !== 'completed')
				{
					require_once('templates/update_test.php');	
				}
				else
				{
			?>

					<fieldset>
						<legend id='new_legend' class='show'>
							Idylla Data Summary
						</legend>
						
						<?php
							if (isset($patientArray)  && isset($visitArray))
							{
								require_once('templates/tables/patient_visit_report_table.php');
								require_once('templates/shared_layouts/panel_info_report.php');	
								require_once('templates/shared_layouts/sample_information.php');
							}

							// MUTANTS
							require_once('templates/tables/mutant_tables.php');

							// Low Coverage 
							require_once('templates/tables/low_cov_tables.php');

							// INTERPRETATION

							if (isset($num_snvs)  && $num_snvs > 0)
							{						
								require_once('templates/shared_layouts/interpretation_summary_report.php');
							}				

							// confirmation
							require_once('templates/shared_layouts/confirmation_info.php');

							// Wet Bench tech info
							if (isset($wet_bench_tech) && !empty($wet_bench_tech))
							{
								require_once('templates/shared_layouts/wet_bench_techs.php');
							}
						?>
			<?php
						require('templates/shared_layouts/test_summary_info.php');
			?>
					</fieldset>
			<?php
				}
			?>
	
		</div>
	</div>
</div>
