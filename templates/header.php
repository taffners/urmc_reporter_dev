<!DOCTYPE html>
<html lang="en">
     <head >
          <meta charset="UTF-8">
          <meta name="Description" content="<?= htmlspecialchars(SITE_TITLE); ?> is a tool to speed up Clinical Molecular Analysis at University of Rochester Medical Center.  It is transforming into a one stop, start to finish software tool where samples are tracked from login to report generation.  Single analyte and NGS tests are all logged into <?= htmlspecialchars(SITE_TITLE); ?> providing a test tracking system which allows managers the ability to easily produce monthly reports and track audits to evaluate laboratory productivity.  <?= htmlspecialchars(SITE_TITLE);?> transforms into an analysis and report building system for NGS tests.  Allowing users to access the growing knowledge base along with integrated publicly available databases.">
          <meta name="robots" content="INDEX,FOLLOW">
          <meta name="googlebot" content="INDEX,FOLLOW">
          <meta name="viewport" content="width=device-width, initial-scale=1">
          <meta name="google" content="notranslate" />

          <title><?= SITE_TITLE; ?></title>

          <link rel="icon" href="images/Cool_penguin.png">

<?php
     // I am trying to upgrade to bootstrap 4 and newer JQuery If this is on it is for testing the new versions
     if (JS_UPGRADE_TESTING)
     {
?>
          <link rel="stylesheet" type="text/css" media="all" href="css/libraries.min_v4.css"/>
<?php          
     }
     else
     {
?>
           <!-- <link rel="preload" as="font" href="fonts/glyphicons-halflings-regular.woff2" type="font/woff2" crossorigin="anonymous"> -->
          <link rel="stylesheet" type="text/css" media="all" href="css/libraries.min.css"/>
<?php
     }
?>                  
          <link rel="stylesheet" type="text/css" media="all" href="css/style.css?ver=<?= VERSION; ?>">          
<?php
          // include unit testing to admin page
          if (isset($page) && $page === 'admin')
          {
     ?>
               <link rel="stylesheet" type="text/css" media="all" href="css/qunit.css?ver=<?= VERSION; ?>">
     <?php
          }
     ?>

     </head>

     <body>
<?php
if   (
     !isset($_GET['nav_status']) || 
     (
          isset($_GET['nav_status']) &&
          $_GET['nav_status'] === 'on'
     )
)
{
?>          

          <?php
          // on some pages add training nav bar instead of header
          $training_pages = array('training','qc','qc_strand_bias', 'workflow_overview');
          if (in_array($page, $training_pages))
          {
          require_once('templates/lists/training_nav_bar.php');
          }
          else
          {
          ?>      
          <header id="page-head" class="container-fluid">         
               <div class="row d-print-none">
                    <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8 page_title d-print-none">
                         <?= $page_title; ?>
                    </div>
                    
                    <div class="hidden-xs hidden-sm col-md-4 col-lg-4 d-print-none">
                         <img src="images/urmc_logo_186_40.png" Height="40" align="right" style="margin-top:10px;" alt="urmc logo">
                    </div>
               </div>
          
               <div class="row d-none d-print-block" style="margin-top:10px;">
                    <div class="col-print-3 d-none d-print-block">
                         <img src="images/urmc_logo_186_40.png" Height="40" style="margin-top:20px;" alt="urmc_logo">
                    </div>
                    <div class="col-print-4 d-none d-print-block">
                    </div>
                    <div class="col-print-5 d-none d-print-block" style="text-align:right;">
                         <b>MOLECULAR DIAGNOSTIC LABORATORY</b><br>
                         UR MEDICINE LABS<br>
                         Department of Pathology and Laboratory Medicine<br>
                         601 Elmwood Avenue Rochester, New York 14642<br>
                         Phone: (585)275-7574 / Fax:(585)276-2272
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 page_title_print d-none d-print-block">
                         <hr class="section-mark">
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 page_title_print d-none d-print-block page_title">
                         <?= $page_title; ?>
                    </div>
               </div>
          <?php
          }
          ?>
          </header>
<?php
     if (isset($page) && $page !== 'login' && $page !== 'password_reset')
     {
?>   
          <nav class="navbar navbar-expand-lg navbar-dark bg-dark mb-3">
               <a class="navbar-brand" href="?page=new_home" title="Go to Main Page (Home)">
                    <img src="images/house.png" style="height:30px;width:30px;margin-right:5px;" alt="house icon">
               </a>
               <!-- <a href="?page=new_home" title="Get Current Summary Stats">
                    <span class="fas fa-chart-pie" style="color:#337AB7;font-size:30px;;margin-right:5px;"></span>
               </a> -->
               
               <div class="collapse navbar-collapse">

                    <!-- Right menu -->
                    <ul class="navbar-nav mr-auto">
          <?php
               /////////////////////////////////////////////////
               // Add NGS menu if NGS is provide to user
               /////////////////////////////////////////////////
               if (isset($user_permssions) && strpos($user_permssions, 'NGS') !== false)
               {
          ?>
                         <li class="nav-item dropdown">
                              <a id="ngs-dropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" title="The NGS menu will provide links to NGS samples.  This menu provides the ability to  add NGS controls and pools, search the NGS database, and show which samples are scanned into patient charts.">
                                   NGS<span class="caret"></span>
                              </a>
                              <div class="dropdown-menu">
                                   <div class="dropdown-divider"></div> 
                                   <div class="dropdown-header">Show</div>
                                   <a class="dropdown-item" href="?page=home&ngs_panel_id=2">
                                        <span id="myeloid-samples-nav">Myeloid Samples</span>
                                   </a>
                                   <a class="dropdown-item" href="?page=home&ngs_panel_id=1">
                                        OFA Samples
                                   </a>
                         <?php
                              // turn off any adding for view only permissions
                              if (isset($user_permssions) &&  strpos($user_permssions, 'View_Only') === false)
                              {
                         ?>                                    
                                   <div class="dropdown-divider"></div>                                   
                                   <div class="dropdown-header">Add</div>
                                   <a class="dropdown-item" href="?page=add_visit&sample_type=control">Add Control</a>
                                   <a class="dropdown-item" href="?page=add_library_pool">Add Library NGS Pool</a>                                   
                         <?php
                              }
                         ?>                                    
                                   <div class="dropdown-divider"></div> 
                                   <div class="dropdown-header">Searches</div>
                                   
                                   <a class="dropdown-item" href="?page=search_observed_variant">Search observed variants</a>
                                   <a class="dropdown-item" href="?page=search_patients">Search patients &amp; their reports</a>
                                   <a class="dropdown-item" href="?page=knowledge_search">Search Knowledge Base</a>
                              
                                   <a class="dropdown-item" href="?page=tissue_gene_stats">Tissue Gene Stats</a>
                         <?php
                              // turn off any adding for view only permissions
                              if (isset($user_permssions) &&  strpos($user_permssions, 'View_Only') === false)
                              {
                         ?>   
                                   <a class="dropdown-item" href="?page=search_control">Search Historic Control Stats</a>
                                   <div class="dropdown-divider"></div> 
                                   <div class="dropdown-header"></div>
                                   <a class="dropdown-item" href="?page=transfered_to_patient_chart&scan_status=pending">Scanned Transfer to Patient Chart</a>
                          <?php
                              }
                         ?> 

                              </div>
                         </li>
          <?php
               }
          ?>

          <?php
               /////////////////////////////////////////////////
               // Add NGS menu if NGS is provide to user
               /////////////////////////////////////////////////
               if (isset($user_permssions) && strpos($user_permssions, 'NGS') !== false && strpos($user_permssions, 'log_book') !== false)
               {
          ?>
                         <li class="nav-item dropdown">
                              <a id="completed-reports-dropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" title="The NGS menu will provide links to NGS samples.  This menu provides the ability to  add NGS controls and pools, search the NGS database, and show which samples are scanned into patient charts.">
                                   Reports<span class="caret"></span>
                              </a>
                              <div class="dropdown-menu">
                                   <?php
                                        if (isset($user_permssions) && strpos($user_permssions, 'revise') !== false)
                                        {
                                   ?>
                                   <a href="?page=completed_reports" class="dropdown-item">View or Revise All Completed Reports</a>
                                   <?php
                                        }
                                        else
                                        {
                                   ?>
                                   <a href="?page=completed_reports" class="dropdown-item">View All Completed Reports</a>
                                   <?php
                                        }
                                   ?>
                                   <a href="?page=confirmation_list&status=pending" class="dropdown-item">Confirmations</a>
                                   <div class="dropdown-divider"></div> 
                                   <div class="dropdown-header">Completed Searches</div>
                                   
                                   <a class="dropdown-item" href="?page=search_observed_variant">Search observed variants</a>
                                   <a class="dropdown-item" href="?page=search_patients">Search patients &amp; their reports</a>
                                   <a class="dropdown-item" href="?page=knowledge_search">Search Knowledge Base</a>
                              
                         <?php
                              // turn off any adding for view only permissions
                              if (isset($user_permssions) &&  strpos($user_permssions, 'View_Only') === false)
                              {
                         ?>   
                                   <a class="dropdown-item" href="?page=search_control">Search Historic Control Stats</a>
                                   <div class="dropdown-divider"></div> 
                                   <div class="dropdown-header"></div>
                                   <a class="dropdown-item" href="?page=transfered_to_patient_chart&scan_status=pending">Scanned Transfer to Patient Chart</a>
                          <?php
                              }
                         ?> 

                              </div>
                         </li>
          <?php
               }
          ?>



          <?php
               /////////////////////////////////////////////////
               // Add log_book menu if log_book is provide to user
               /////////////////////////////////////////////////
               if (isset($user_permssions) && strpos($user_permssions, 'log_book') !== false)
               {
          ?>
                         <li class="nav-item dropdown">
                              <a id="log-book-dropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" title="The Log Book Menu will provide links related to the log book for all samples entering the lab.  The show sub menu provides links to show samples with pending tasks or a link to search all samples. Samples just arriving in the lab can be logged into Infotrack under the add sub menu.">
                                   Log Book<span class="caret"></span>
                              </a>
                              <div class="dropdown-menu">
                                   <div class="dropdown-divider"></div> 
                                   <div class="dropdown-header">Show</div>
                                   <a class="dropdown-item" href="?page=sample_log_book&filter=pending" style="padding-top:5px;padding-bottom:5px;">Pending Samples</a>
                                   <a class="dropdown-item" href="?page=sample_log_book&search=" style="padding-top:5px;padding-bottom:5px;">Search All Samples</a>

                                   <a class="dropdown-item" href="?page=sample_log_book&filter=dna_extraction_only" style="padding-top:5px;padding-bottom:5px;">Samples without tests</a>

                                   <div class="dropdown-divider"></div>
                              
                         <?php
                              // turn off any adding for view only permissions
                              if (isset($user_permssions) &&  strpos($user_permssions, 'View_Only') === false)
                              {
                         ?>   
                                   <div class="dropdown-header">Add</div>
                                   <a class="dropdown-item" href="?page=add_sample_log_book">Add Sample to Log Book</a>
                                   <a class="dropdown-item" href="?page=add_single_analyte_pool">Add Single Analyte Pool</a>
                                   
                              </div>
                          <?php
                              }
                         ?> 
                         </li>
          <?php
               }
          ?>
          <?php
               /////////////////////////////////////////////////
               // turn off any set up for view only permissions because it is not necessary for them.
               /////////////////////////////////////////////////
               if (isset($user_permssions) && strpos($user_permssions, 'View_Only') === false)
               {
          ?>
                         <li class="nav-item dropdown">
                              <a id="setup-dropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" title="The setup menu provides links to show available tests and instruments.  Instruments can also be updated or added via the instruments link.">
                                   Setup<span class="caret"></span>
                              </a>
                              <div class="dropdown-menu">
                                   <a class="dropdown-item" href="?page=setup_tests">Tests</a>
                                   <a class="dropdown-item" href="?page=setup_reflex_trackers">Reflex Trackers</a>
                                   <a class="dropdown-item" href="?page=setup_instruments">Instruments</a>
                                   <a class="dropdown-item" href="?page=setup_home_page">Set up Your Home Page</a>
                                   
                              
                         <?php
                              // turn off any adding for view only permissions
                              if (isset($user_permssions) &&  strpos($user_permssions, 'login_enhanced') !== false)
                              {
                         ?>   
                                   <a class="dropdown-item" href="?page=add_new_sample_type">Add a new Sample Type</a>
                                   <a class="dropdown-item" href="?page=ngs_control_home">NGS controls</a>
                                   
                              </div>
                          <?php
                              }
                         ?>
                         </li> 
          <?php
               }
          ?>
                    </ul>

                    <!-- Left menu -->
                    <ul class="navbar-nav ml-auto">
                         <li class="nav-item mr-4" style="padding-top:9px;" title="When available the tour button will appear to provide more insight into the current page.">
                              <button id="take-a-tour-btn" class="btn btn-info btn-info-hover d-none" style="color:black">Tour</button>
                         </li>
                    <?php
                         if (isset($qc_page[0]) && !empty($qc_page[0]['page_description']))
                         {
                    ?>
                         <li class="nav-item mr-4" title="<?= htmlentities($qc_page[0]['page_description']);?>. Click here to find out more information about this page QC.">
                              <?php
                              // if user has access to the page_validation_reports make this a link otherwise just show a question icon
                              if (isset($user_permssions) && strpos($user_permssions, 'infotrack_QC') !== false)
                              {
                              ?>
                              <a href="?page=page_validation_reports&page_id=<?= $qc_page[0]['page_id'];?>" aria-label="page_validation_info_link" alt="Find out more information about this page QC." >
                              <?php
                              }
                              ?>
                                   <i class="fas fa-question" style="padding-top:9px;color:#337AB7;font-size:30px;"></i>
                              <?php
                              
                              if (isset($user_permssions) && strpos($user_permssions, 'infotrack_QC') !== false)
                              {
                              ?>    
                              </a>
                              <?php
                              }
                              ?>
                         </li>
                    <?php
                         }
                    ?>                         
                        
                         <li class="nav-item mr-4" title="There are canned emails for responding to doctors in certian situations.  Use these emails for that purpose.">                       
                              <a href="?page=mock_emails" >
                                       
                                   <img src="images/canned_email.png" style="margin-left:5px;" alt="canned emails" >                             
                              </a>
                         </li>
                         
               <?php
                    if (isset($user_permssions) && strpos($user_permssions, 'audits') !== false)
                    {
               ?>                          
                         <li class="nav-item mr-4" title="Audits provide a monthly report of tests performed every month and how long it took to perform these tests. It is required by CAP to record turnaround times on a monthly basis.">
                              <a id="performance-audit-btn" href="?page=audit_reports_home&filter=months&month=<?=CURR_MONTH;?>&year=<?=CURR_YEAR;?>" title="Performance Audit Reports" style="padding-top:5px;padding-bottom:5px;">
                                   <img src="images/performance.png" style="margin-left:5px;height:45px;width:45px;" alt="monthly audits">
                              </a>
                         </li>
               <?php
                    }
               ?>

                         <li class="nav-item mr-4">
                              <a id="qc_qa-btn" href="?page=qc_home" title="Quality Control validation documents for pages in <?= SITE_TITLE;?>.  Report a <?= SITE_TITLE;?> bug.  Find version control history of <?= SITE_TITLE;?>." style="padding-top:5px;padding-bottom:5px;">
                                   <img src="images/qc_qa.png" style="margin-left:5px;height:45px;width:45px;" alt="A green check mark indicating qc/qa">
                              </a>
                         </li>

               <?php
                    // turn off training for view only permissions because it is not necessary for them.
                    if (isset($user_permssions) &&  strpos($user_permssions, 'View_Only') === false)
                    {
               ?> 
                         <li class="nav-item mr-4" style="padding-top:7px">
                              <a id="training-btn" href="?page=training&training_step=main" title="Tutorials, how to's, SOPs, training info, and frequently asked questions can be found here.">
                                   Training                                   
                              </a>
                         </li>
               <?php
                    }
               ?>

               <?php
                    if   (
                              isset($user_permssions) && 
                              (
                                   strpos($user_permssions, 'admin') !== false ||
                                   strpos($user_permssions, 'manager') !== false
                              )
                         )
                    {                        
               ?>
                         <li class="nav-item mr-4" style="padding-top:7px">
                              <a id="admin-btn" href="?page=admin&access_filter=all" style="padding-top:13px;padding-bottom:13px;"><img src="images/Admin.png" style="margin-left:5px;height:30px;width:30px;" alt="admin icon">    
                                   
                              </a>
                         </li>
               <?php
                    }
               ?>
                         <li class="nav-item mr-4">
                              <a id="logout-btn" href="utils/logoff.php" title="Log Off" style="padding-top:5px;padding-bottom:5px;"><img src="images/log_out.png" style="margin-left:5px;height:45px;width:30px;" alt="log out icon">    
                                   Logout
                              </a>
                         </li>
                         
                    </ul>     
               </div>
          </nav>

<?php
     }
?>

<?php
               // insert modals here
               require_once('templates/modals/tier_modal.php');

               require_once('templates/modals/variant_comment_modal.php');
?>
          <!-- Jquery Dialogs -->
          <!-- All the confirm windows -->
          <div id='ConfirmId' title='Update'></div>

          <div id='AlertId' title='Ooops!'></div>

          <div id="dialog-temp-use-loc"></div>

          <form id="danger" class="form-horizontal" method="post">
               <?php
               // print_r($roleList);
                    if(isset($message)  && $message !== '')
                    {
               ?>
                         <div class="alert alert-danger" style="font-size:20px;font-weight: bold;">
                              <?= $message; ?>
                         </div>
               <?php
                    }
               ?>
          </form>
          <div id="init-hide-loader" <?php 
          if (!isset($_GET['loader_only']))
          {               
               echo 'class="hide d-none"';
          }
          ?>>
          <?php
               require_once('shared_layouts/uploader.php');
          ?>
          </div>

<?php
}
?>          
