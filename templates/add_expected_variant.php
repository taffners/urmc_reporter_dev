<div class="container-fluid">
	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" >
			<form class="form" method="post" class="form-horizontal">
				<fieldset>
					<legend>
						<?= $page_title;?>
					</legend>
					<div class="form-group row">
						<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
							<label for="genes" class="form-control-label">Gene: <span class="required-field">*</span></label>
						</div>
						<div class="col-xs-12 col-sm-7 col-md-8 col-lg-8">
							<input type="text" class="form-control" id="genes" name="genes" maxlength="30" required="required"  value="<?= $utils->GetValueForUpdateInput($expected_var, 'genes');?>"/>
						</div>
					</div>
					<div class="form-group row">
						<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
							<label for="coding" class="form-control-label">Coding: <span class="required-field">*</span></label>
						</div>
						<div class="col-xs-12 col-sm-7 col-md-8 col-lg-8">
							<input type="text" class="form-control" id="coding" name="coding" maxlength="255" required="required"   value="<?= $utils->GetValueForUpdateInput($expected_var, 'coding');?>"/>
						</div>
					</div>
					<div class="form-group row">
						<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
							<label for="amino_acid_change" class="form-control-label">Amino Acid Change: <span class="required-field">*</span></label>
						</div>
						<div class="col-xs-12 col-sm-7 col-md-8 col-lg-8">
							<input type="text" class="form-control" id="amino_acid_change" name="amino_acid_change" maxlength="255" required="required"   value="<?= $utils->GetValueForUpdateInput($expected_var, 'amino_acid_change');?>"/>
						</div>
					</div>
					<div class="form-group row">
						<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
							<label for="min_accepted_frequency" class="form-control-label">Min Accepted Frequency: <span class="required-field">*</span></label>
						</div>
						<div class="col-xs-12 col-sm-7 col-md-8 col-lg-8">
							<input type="number" class="form-control" id="min_accepted_frequency" name="min_accepted_frequency" min="0" step=".01" required="required"   value="<?= $utils->GetValueForUpdateInput($expected_var, 'min_accepted_frequency');?>"/>
						</div>
					</div>
					<div class="form-group row">
						<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
							<label for="max_accepted_frequency" class="form-control-label">Max Accepted Frequency: <span class="required-field">*</span></label>
						</div>
						<div class="col-xs-12 col-sm-7 col-md-8 col-lg-8">
							<input type="number" class="form-control" id="max_accepted_frequency" name="max_accepted_frequency" min="0" step=".01" required="required"   value="<?= $utils->GetValueForUpdateInput($expected_var, 'max_accepted_frequency');?>"/>
						</div>
					</div>
					<div class="form-group row">
						<div class="offset-xs-4 offset-sm-4 offset-md-4 offset-lg-4 col-xs-2 col-sm-2 col-md-2 col-lg-2">
							<button type="submit" id="<?= $page;?>_submit" name="<?= $page;?>_submit" value="Submit"  class="btn btn-primary btn-primary-hover">
								Submit
							</button>
						</div>
					</div>
				</fieldset>
			</form>
		</div>
	</div>
</div>