<div class="container-fluid">

	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="height:500px;">

			<form class="form" method="post" class="form-horizontal">
				<fieldset>

<?php 
				if (isset($_GET['instrument_id']))
				{
?>
					<legend>
						Update Instrument
					</legend>
<?php
				}
				else 
				{
?>
					<legend>
						Add an Instrument
					</legend>
<?php 
				}
?>
					<div class="form-group row">
						<div class="col-xs-12 col-sm-4 col-md-4">
							<label for="task" class="form-control-label">Task: <span class="required-field">*</span></label>
						</div>
				
						<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
							<!-- <input type="text" class="form-control capitalize"  list="task" name="task" maxlength="50" required="required" value="<?= $utils->GetValueForUpdateInput($instrumentArray, 'task');?>"/> -->
							<select class="selectpicker" data-live-search="true" data-width="100%" name="task" id="task">
									<option value="" selected="selected"></option>
							<?php 

								foreach ($tasks as $key => $task)
								{
							?>
									<option value="<?= $task['task']; ?>"   <?= $utils->GetValueForUpdateSelect($instrumentArray, 'task', $task['task'])?> ><?= $task['task']; ?></option>
							<?php 
								}
							?>									
							</select>
						</div>
					</div>
					<div class="form-group row">
						<div class="col-xs-12 col-sm-4 col-md-4">
							<label for="manufacturer" class="form-control-label">Manufacturer: <span class="required-field">*</span></label>
						</div>
						<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
							<input type="text" class="form-control"  list="manufacturer" name="manufacturer" maxlength="50" required="required" value="<?= $utils->GetValueForUpdateInput($instrumentArray, 'manufacturer');?>"/>
							<datalist name="manufacturer" id="manufacturer">
							<?php 

								foreach ($manufacturers as $key => $manufacturer)
								{
							?>
									<option value="<?= $manufacturer['manufacturer']; ?>"><?= $manufacturer['manufacturer']; ?></option>
							<?php 
								}
							?>

							</datalist>
						</div>
					</div>
					<div class="form-group row">
						<div class="col-xs-12 col-sm-4 col-md-4">
							<label for="model" class="form-control-label">Model: <span class="required-field">*</span></label>
						</div>
						<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
							<input type="text" class="form-control" id="model" name="model" maxlength="50" required="required" value="<?= $utils->GetValueForUpdateInput($instrumentArray, 'model');?>"/>
						</div>
					</div>
					<div class="form-group row">
						<div class="col-xs-12 col-sm-4 col-md-4">
							<label for="description" class="form-control-label">Description: </label>
						</div>
						<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
							<input type="text" class="form-control" id="description" name="description" maxlength="50" value="<?= $utils->GetValueForUpdateInput($instrumentArray, 'description');?>"/>
						</div>
					</div>
					<div class="form-group row">
						<div class="col-xs-12 col-sm-4 col-md-4">
							<label for="yellow_tag_num" class="form-control-label">Yellow Tag Number: </label>
						</div>
						<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
							<input type="number" class="form-control" id="yellow_tag_num" name="yellow_tag_num" value="<?= $utils->GetValueForUpdateInput($instrumentArray, 'yellow_tag_num');?>"/>
						</div>
					</div>
					<div class="form-group row">
						<div class="col-xs-12 col-sm-4 col-md-4">
							<label for="serial_num" class="form-control-label">Serial #: </label>
						</div>
						<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
							<input type="text" class="form-control" id="serial_num" name="serial_num" maxlength="50" value="<?= $utils->GetValueForUpdateInput($instrumentArray, 'serial_num');?>"/>
						</div>
					</div>
					<div class="form-group row">
						<div class="col-xs-12 col-sm-4 col-md-4">
							<label for="clinical_engineering_num" class="form-control-label">Clinical Engineering #: </label>
						</div>
						<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
							<input type="text" class="form-control" id="clinical_engineering_num" name="clinical_engineering_num" value="<?= $utils->GetValueForUpdateInput($instrumentArray, 'clinical_engineering_num');?>"/>
						</div>
					</div>
					<div class="form-group row">
						<div class="col-xs-12 col-sm-4 col-md-4">
							<label for="instrument_status" class="form-control-label">Instrument Status: <span class="required-field">*</span></label>
						</div>
						<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
							<label class="radio-inline">
								<input type="radio" name="instrument_status" value="active" required="required" <?= $utils->DefaultRadioBtn($instrumentArray, 'instrument_status', 'active');?>>
									Active
							</label>
							<label class="radio-inline">
								<input type="radio" name="instrument_status" value="inactive" required="required" <?= $utils->GetValueForUpdateRadioCBX($instrumentArray, 'instrument_status', 'inactive');?>>
								Inactive
							</label>
						</div>
					</div>
					<div class="form-group row">
						<div class="offset-xs-4 offset-sm-4 offset-md-4 offset-lg-4 col-xs-2 col-sm-2 col-md-2 col-lg-2">
							<button type="submit" id="<?= $page;?>_submit" name="<?= $page;?>_submit" value="Submit"  class="btn btn-primary btn-primary-hover">
								Submit
							</button>
						</div>
						<div class="offset-xs-4 offset-sm-4 offset-md-4 offset-lg-4 col-xs-2 col-sm-2 col-md-2 col-lg-2">
							<a href="?page=setup_instruments" class="btn btn-primary btn-primary-hover" role="button">
								
								All Instruments
							</a>
						</div>
					</div>
							

				</fieldset>
			</form>
		</div>
	</div>
</div>