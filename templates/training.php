<!-- <div class="container-fluid">
	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			<a href="https://www.youtube.com/watch?v=wNzFMV0Zhqw">NGS Acronyms and Terms</a>
		</div>

		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			New York State requires us to verify at least 10 variants per gene.  Why is that?  This video will explain more.
			<a href="https://www.youtube.com/watch?v=lSTYu70GW_o">Why Confirm Your NGS Variants</a>
		</div>
	</div>	
</div> -->

<?php
	if (isset($_GET['training_step']) && $_GET['training_step'] === 'main')
	{
?>
<div class="container-fluid">
	
	<!-- <div class="row">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			<h3 class="raised-card-subheader">Training Modules</h3>
			<ul class="hover-card-list" style="font-size:20px;">
				<li><a href="?page=training&training_step=obtain_upload_files">Obtain Upload Files</a></li>
				<li><a href="?page=training&training_step=qc_run">QC Run Metrics</a></li>
				<li><a href="?page=training&training_step=strand_bias">Strand Bias</a></li>
				<li><a href="?page=workflow_overview">NGS Work Flow Overview</a></li>
				<li><a href="?page=training&training_step=issues">Issues to work thru</a></li>
				<li><a href="?page=training&training_step=Approved_SOPs">Approved SOPs</a></li>
			</ul>
		</div>
	</div> -->
<?php
	require('templates/shared_layouts/faq_menu.php');
?>
</div>
<?php
	}
	else if (isset($_GET['training_step']) && $_GET['training_step'] === 'FAQ')
	{
		require_once('templates/FAQ.php');
	}
	else if (isset($_GET['training_step']) && $_GET['training_step'] === 'issues')
	{
?>
<div class="container-fluid">
	<h3 class="raised-card-subheader">OFA Issues which we need to solve</h3>
	<div class="raised-card" style="margin-bottom:15px;">
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">	
				<h3 class="raised-card-subheader">How do we deal with multiple variants in same row?</h3>			

				<strong>This complex variant is appearing in multiple samples.</strong>
				<img src="images/ofa_strand_bias_problem.png" class="zoom-img" alt="ofa_strand_bias_problem">

				<strong>During validation found in 17-hsp261 ran on January 22nd, 2019 but was not found in the same sample on August 8th, 2017</strong>

				<img src="images/17-hsp261_Jun_complex_variant_found_but_not_in_Andrews_run.png" class="zoom-img" alt="17-hsp261_Jun_complex_variant_found_but_not_in_Andrews_run">
				<strong>Is this complex variant a sequencing artifact?</strong>
			</div>
		</div>
	</div>

</div>
<?php
	}
	else if (isset($_GET['training_step']) && $_GET['training_step'] === 'obtain_upload_files')
	{
?>
<div id="obtain-upload-files" class="container-fluid">

	<div class="raised-card" style="margin-bottom:15px;">
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				<h3 id="ofa-files" class="raised-card-subheader">OFA Upload files</h3>
				<ul class="hover-card-list">
<?php
					require_once('templates/lists/login_ion_reporter_list.php');
					require_once('templates/lists/login_torrent_server_list.php');
?>	

		               <li>The last few steps of the OFA library pool will pull all data from torrent server and ion reporter automatically and place it on the N drive</li>
		               <li>Follow this tour for more information <button id="take-a-tour-btn-ofa-files" class="btn btn-info btn-info-hover d-none" style="color:black">Tour</button></li>
		          </ul>

	
			</div>

		</div>
	</div>

</div>
	<div class="raised-card" style="margin-bottom:15px;">
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				<h3 id="myeloid-tsv-file" class="raised-card-subheader">Myeloid TSV File</h3>
				<ul class="hover-card-list">
		               <li><b>Create a Reporting folder</b> within the run folder <br>(example: O:\\Clinical\\Myeloid\\new\\181102_C4MFK)</li>
		               <li>In VariantStudio, <b>apply the desired filter</b> to the current sample from the home tab.  This is typically the main filter. In cases that there are no reportable variants from this filter, but there is one or more variants with low VAF(s) you can choose to use the sensitivity filter.</li>
		               <li>Click on the <b>Reports tab</b> and choose <b>Filtered Variants</b> in the Export section.  Save the file in the Reporting folder within the correct run folder.  This is named with the MD_Soft# (example: 4442-18_H8312658.tsv)</li>
		               <li>Each Patient has their own NGS TSV file.</li>
		          </ul>

				<h3 id="myeloid-cov-file" class="raised-card-subheader">Myeloid Coverage File</h3>
			     <ul class="hover-card-list">
			          <li><b>Create a Reporting folder</b> within the run folder <br>(example: O:\\Clinical\\Myeloid\\new\\181102_C4MFK)</li>
			          <li>From the Data folder within the run folder <b>copy the depth coverage file</b> <br>(example: 181102_C4MFK_depth_coverage) and paste this into your newly created run folder.</li>
			          <li>For a given run all patients use the <b>same depth coverage file</b> when uploading to the reporter, but have their own tsv file.  These are all located in the reporting folder.</li> 
			     </ul>
			</div>

		</div>
	</div>

</div>
<?php
	}	
	else if (isset($_GET['training_step']) && $_GET['training_step'] === 'qc_run')
	{
?>
<div class="container-fluid">
	<div class="raised-card" style="margin-bottom:15px;">
		<div class="row">
			<h3 id="ofa-run-metrics" class="raised-card-subheader">QC Run Metrics OFA</h3>
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			<ul class="hover-card-list">
<?php
				require_once('templates/lists/login_torrent_server_list.php');
?>	
				<li>All of the metrics in the following table will be found completed runs &amp; reports.  Make sure all of the values fall in the range.</li>
			</ul>
			<h3 class="raised-card-subheader">Table 13.A.1. QC Metrics - Table of Acceptable Ranges (530 chip)</h3>
<?php
				require_once('templates/shared_layouts/qc_run_metrics_ofa_table.php');
?>
			<h3 class="raised-card-subheader">SOP</h3>
			<iframe src="downloads/OFA_SOP_04162018YD.pdf#page=104" style="width:90%; height:700px;" frameborder="0"></iframe>
			</div>
		</div>
	</div>
	<div class="raised-card" style="margin-bottom:15px;">
		<div class="row">
			<h3 id="myeloid-run-metrics" class="raised-card-subheader">QC Run Metrics Myeloid</h3>
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				<iframe src="downloads/2093_SOPM_and_Validation_Package_02142018_URMC Myeloid_CLL_Mutation_NGS_Panel.pdf#page=38" style="width:90%; height:700px;" frameborder="0"></iframe>
				<h3 class="raised-card-subheader">QC Metrics - Table of Acceptable Ranges</h3>

<?php
				require_once('templates/shared_layouts/qc_run_metrics_myeloid_table.php');
?>


			</div>
		</div>
	</div>	
</div>		
<?php
	}
	else if (isset($_GET['training_step']) && $_GET['training_step'] === 'Approved_SOPs')
	{
?>
<div class="container-fluid">
	<div id ="infotrack-sop" class="raised-card" style="margin-bottom:15px;">
		<div class="row">
			<h3 class="raised-card-subheader"><?= SITE_TITLE;?> SOP</h3>
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				<iframe src="downloads/MD_Infotrack_SOP.pdf" style="width:100%; height:700px;" frameborder="0"></iframe>
			</div>
		</div>
	</div>
	<div id ="ofa-sop" class="raised-card" style="margin-bottom:15px;">
		<div class="row">
			<h3 class="raised-card-subheader">Approved OFA SOP</h3>
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				<iframe src="downloads/OFA_SOP_04162018YD.pdf" style="width:100%; height:700px;" frameborder="0"></iframe>
			</div>
		</div>
	</div>
	<div id="myeloid-sop" class="raised-card" style="margin-bottom:15px;">
		<div class="row">
			<h3 class="raised-card-subheader">Approved Myeloid SOP</h3>
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				<iframe src="downloads/2093_SOPM_and_Validation_Package_02142018_URMC Myeloid_CLL_Mutation_NGS_Panel.pdf" style="width:100%; height:700px;" frameborder="0"></iframe>
			</div>
		</div>
	</div>	
</div>		
<?php
	}	
	else if (isset($_GET['training_step']) && $_GET['training_step'] === 'strand_bias')
	{
?>	
		

<div class="container-fluid">
	<div class="raised-card" style="margin-bottom:15px;">
		<div class="row">
			<h3 id="about-strand-bias" class="raised-card-subheader">What is Strand Bias?</h3>
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				Strand bias occurs when the genotype inferred from information presented by the forward strand and the reverse strand disagrees.  For example, at a given position in the genome, the reads mapped to the forward strand support a heterozygous genotype, while the reads mapped to the reverse strand support a homozygous genotype.
			</div>
		</div>
	</div>
	<div class="raised-card" style="margin-bottom:15px;">
		<div class="row">
			<h3 class="raised-card-subheader">Strand Bias Value Explained</h3>
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				There are different formulas to calculate Strand Bias.  Depending on your sequencing method some are better than others.  The formula used by <?= SITE_TITLE; ?> is <a href="https://software.broadinstitute.org/gatk/documentation/tooldocs/3.8-0/org_broadinstitute_gatk_tools_walkers_annotator_StrandOddsRatio.php" target="_blank">GATK strandOddsRatio</a> described below.
				<br><br>
				Sequencing data at a single position in the genome can be represented by a 2 by 2 table where a and b represent coverage counts related to the reference and c and d represent coverage counts of the variant allele.
				<br><br>
				<strong>
				a = positive strand counts of reference + 1<br>
				b = negative strand counts of reference + 1<br>
				c = positive strand counts of variant + 1<br>
				d = negative strand counts of variant + 1<br>
				<br><br>

				R = (a * d ) / ( b * c )<br>
				sum_R = ( R + ( 1 / R ) )<br>
				ref_ratio = min(a , b) / max(a, b)<br>
				alt_ratio = min(c , d) / max(c , d)<br>
				scale_factor = ( ref_ratio / alt_ratio )<br>
				scaled_sor = ( sum_R * scale_factor )<br>
				SOR = (ln(scaled_sor)) <br>
				</strong>
				<br><br>
				The <a href="https://gatkforums.broadinstitute.org/gatk/discussion/5533/strandoddsratio-computation" target="_blank">dev of GATK strandOddsRatio recommends flagging SNPs with SOR > 4.0 and indels with SOR > 10.0. </a>These are meant to be conservative.  SOR's between 0 and 4 passed.  In the image below you can see red rows are flagged, green rows passed, and grey rows do not currently have a value.
				<br><br>
				<img src="images/strand_bias_formula_explained.png?ver=<?= VERSION; ?>" alt="strand_bias_formula_explained">
				<br><br>
				Values of columns ref+ ref- Variant+ Variant- can be edited by clicking on them.  Make sure you remove the default value of __. They automatically save and calculate strand bias.
				<br>
				<img src="images/strand_bias_edit.png?ver=<?= VERSION; ?>" alt="strand_bias_edit">
			</div>
		</div>
	</div>
	<div id="ofa-strand-bias" class="raised-card" style="margin-bottom:15px;">
		<div class="row">
			<h3 class="raised-card-subheader">Protocol for OFA</h3>
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				The strand bias table is filled in automatically for the OFA panel with information from the TSV column Ref+/Ref-Var+/Var-.  The reporter uses an algorithm to obtain the columns ref+ ref- Variant+ Variant-.  
				<br><br>
				<strong>STEPS:</strong>
				<ul>
					<li>Check that data transfered correctly from Ref+/Ref-Var+/Var- to columns ref+ ref- Variant+ Variant-</li>
					<li>If something transfered incorrectly update it by clicking on value.</li>
					<li>All gray rows require the user to make decisions on what to put in columns ref+ ref- Variant+ Variant-.  Gray rows exist when there are more than two non zero alleles.  As we get more data this will be automated.  Currently I'm unsure why the Ion reporter is giving us so many variant alleles.</li>
					<li>So far it looks like we can choose the two largest ratios.  If ratio of non zero ratios are closer than this example seek the director for advice.  For example in this example the reference is the first ratio since it matches the ref allele column.  The rest are possible variant allele coverage counts.  Ratios of 0/0 can be ignored.  Therefore we have the following to consider:
						<ul>
							<li>TTCACTG=3276/2854</li>
							<li>CTCACTG=4/3</li>
							<li>GTCACTG=1/0</li>
							<li>TTCATTG=1/0</li>
							<li>TTCGCTG=4/2</li>
							<li>TTCTCTG=224/179</li>
						</ul>
					</li>
					<li>The reference allele is TTCACTG since it is the first one listed in ref+ ref- Variant+ Variant-.  The variant is TTCTCTG since it is the only one that is &ge;20 reads in both directions.  This is only an example.  The algorthim in the reporter was updated February 13th 2019 to automatically account for this.</li>					
				</ul>

				<img src="images/ofa_strand_bias.png?ver=<?= VERSION; ?>" class="zoom-img" alt="ofa_strand_bias">
				<br><br>
				<strong>How to fill in the variant</strong>
				<img src="images/ofa_strand_bias_answer.png?ver=<?= VERSION; ?>" class="zoom-img" alt="ofa_strand_bias_answer">
			</div>
		</div>
	</div>
	<div class="raised-card" style="margin-bottom:15px;">
		<div class="row">
			<h3 id="myeloid-strand-bias" class="raised-card-subheader">Protocol for Myeloid</h3>
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				Currently the strand bias table for Myeloid Panel needs to be filled in by hand. The data can be found by using IGV. 
				<br><br>
				<strong>STEPS:</strong>
				<ul>
					<li><img src="images/IGV_strand_bias1.png" class="zoom-img" alt="IGV_strand_bias1"></li>
					<li><img src="images/IGV_strand_bias2.png" alt="IGV_strand_bias2"></li>
				</ul>

			</div>
		</div>
	</div>
</div>
<?php
	}
?>