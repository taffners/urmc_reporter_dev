<div class="container-fluid">
	<?php
	require('templates/shared_layouts/faq_menu.php');
	?>
	<div class="row" style="font-size:20px;">

<?php 
	if (isset($faqs) && !empty($faqs))
	{	
?>		
		
	<?php
		foreach ($faqs as $subheader => $questions)
		{
			foreach($questions as $quest_id => $quest)
			{
	?>

		<div id="<?= $quest_id;?>" class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			<hr class="section-mark">
			<h3 class="raised-card-subheader"><?= $quest['Question'];?></h3>
			
			<?= $quest['Answer'];?>
	<?php
		if (isset($quest['img']) && !empty($quest['img']) && $quest['img'] !== 'none')
		{
	?>
			<img src="<?= $quest['img'];?>" width="100%" alt="<?= $quest['img'];?>">
	<?php
		}
	?>
	<?php
		if (isset($quest['sop_link']) && !empty($quest['sop_link']))
		{
	?>
			<?= $quest['sop_link'];?>
	<?php
		}
	?>

		</div>
	<?php
			}
	?>
	<?php
		}
	?>			
			
		</div>
	</div>
<?php
	}
?>			
		
</div>