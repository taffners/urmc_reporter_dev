<?php
if (isset($_GET['pool_chip_linker_id']) && isset($poolStepTracker) && isset($_GET['ngs_panel_id']))
{

?>
	<!-- report progress bar toggles -->	
	<?php
	require_once('templates/shared_layouts/report_toggle_show_status.php');
	?>

	<!-- Progress bar -->
	<?php
	require_once('templates/shared_layouts/pool_step_nav_bar.php')
	?>
	<div id="toggle-report-progress-bar" class="col-xs-10 col-sm-10 col-md-10 col-lg-10">
		<form id="make_run_template_form" class="form" method="post" class="form-horizontal">
			<fieldset>
				<legend>
					Select the results associated with this pool
				</legend>

			<?php
				if (ROOT_URL === 'devs')
				{
			?>
				<div class="row">
					<div class="alert alert-warning" style="font-size:20px;">	
						<b>Planned Infotrack update:</b> 
						<ul>
							<li>Include a data transfer button for each chip that passes qc</li>
							<li>From this button I can supply Bill's with run name, out put directories, and any other init data required to run ofabio_ionir_samples_v1.2.6.a.py
								<ul>
									<li>It would be best to remove versions from file name and use git for versions.  Updating versions like this will make it hard to keep softwares communicating.</li>
								</ul>
							</li>
							<li>
								Which of Bill's scripts makes the variant tsv?  I see the vcf and bam files are transfered with ofabio_ionir_samples_v1.2.6.a.py.  Is there a script that is used after ofabio_ionir_samples_v1.2.6.a.py?
							</li>
						</ul>
						
					</div>
				</div>
			<?php
				}
			?>	


			</fieldset>
		</form>

		<?php
			// add form submit, previous, and next buttons.
			require_once('templates/shared_layouts/form_pool_submit_nav_buttons.php');
		?>

	</div>


<?php
}
?>