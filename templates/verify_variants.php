<?php
	require_once('templates/lists/run_info_list.php');
?>

<div class="container-fluid">
	<div class="row">
		<?php
			require_once('templates/shared_layouts/report_progress_bar.php');		
		?>

		<!-- update area -->
		<div id="toggle-report-progress-bar" class="col-xs-10 col-sm-10 col-md-10 col-lg-10" style="height:500px;">

		<div class="row alert alert-warning" style="font-size:20px;">
			<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
				<?= $variants_removed_from_report;?>
			</div>
			<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
				<?= $variants_not_in_knowledge_base;?>
			</div>
		</div>
<?php	
		// Either allow access to page or not depending on the criteria above.
$allow_access = true; // turning off this function because it looks like Directors might be the only one accessing this.
		if ($allow_access)
		{		
?>				
			<form id="add_variants_form" class="form" method="post" class="form-horizontal">
				<fieldset>
					<!-- 
					/////////////////////////////////////////////////////////////
					// Only include toggles and table if Variants were found and uploaded
					/////////////////////////////////////////////////////////////
					 -->		

				<?php
					if (isset($variantArray))
               		{    
				?>
					<legend id='new_legend' class='show'>
						Choose which Variants to Include in Report.
					</legend>



					<!-- toggle buttons for included in report, filtered, passed filter -->
					<div class="row" style="margin-bottom: 2px;">
						<div class="offset-xs-6 offset-sm-6 offset-md-6 offset-lg-10 col-xs-2 col-sm-2 col-md-2 col-lg-2">
							<span><b>Filter Toggles</b>:</span> 
						</div>
					</div>

					<div class="row" style="margin-bottom: 20px;">
						<!-- -->
						<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3" title="Add a Variant not listed">
							<a href="?page=add_variant&amp;<?= EXTRA_GETS;?>&amp;previous_page=verify_variants" class="btn  btn-primary btn-primary-hover" role="button">
								Add a Variant
							</a>
						</div>
<?php
				if(isset($visitArray[0]['ngs_panel']) && $visitArray[0]['ngs_panel'] === 'Myeloid/CLL Mutation Panel')
				{
?>						
						<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3" title="Add FLT3 Internal Tandem Duplication (ITD)">
							<a href="?page=add_flt3&amp;<?= EXTRA_GETS;?>&amp;previous_page=verify_variants" class="btn  btn-primary btn-primary-hover" role="button">
								Add FLT3 ITD
							</a>
						</div>
<?php
				}
?>
						<div class="offset-lg-3 col-xs-2 col-sm-2 col-md-2 col-lg-1" title="Variants included in Report.  Use checkbox in table first column to toggle in and out of report.">
							
							<a href="?page=verify_variants&<?= EXTRA_GETS;?>&variant_filter=included" class="btn <?php 
								if(isset($_GET['variant_filter']) && $_GET['variant_filter'] === 'included')
								{
									echo "btn btn-success btn-success-hover";
								}
								else
								{
									echo "btn btn-primary btn-primary-hover";
								}

								;?>" role="button">
								Included
							</a>
						</div>

						<div class="col-xs-2 col-sm-2 col-md-2 col-lg-1" title="All variants uploaded.  Use checkbox in table first column to toggle in and out of report.">
							<a href="?page=verify_variants&<?= EXTRA_GETS;?>&variant_filter=passed" class="btn <?php 
								if(isset($_GET['variant_filter']) && $_GET['variant_filter'] === 'passed')
								{
									echo "btn btn-success btn-success-hover";
								}
								else
								{
									echo "btn btn-primary btn-primary-hover";
								}

								;?>" role="button">
								All Variants
							</a>
						</div>						
					</div>

			     <?php
			     	require_once('templates/shared_layouts/verify_variant_table.php');

				     }				     
				?>
					<!-- add the common hidden fields for every form -->
					<?php
						require_once('templates/shared_layouts/form_hidden_fields.php');
					?>

         				<!-- add submit and nav buttons -->
					<?php
						/////////////////////////////////////////////////////////////
						// Remove nav buttons and replace with an alert if all variants not 
						// in knowledge base table.  Need to account for no variants found
						/////////////////////////////////////////////////////////////
						
						// for no snvs just add navigation buttons
						if (isset($variantArray) && $num_snvs === 0)
						{
							require_once('templates/shared_layouts/form_submit_nav_buttons.php');
						}

						// do not add buttons if variant is not added to knowledge base
						else if (!$knowledge_ids_for_all)
						{
					?>
						<div class="alert alert-danger" role="alert">
  							<strong>Add all SNV's to Knowledge Base before continuing with report.</strong>
						</div>
					<?php
						}

						// Add navigation buttons if:
							// variant_filter == included
							// $num_snvs is not equal to 0
							// variant Array is set
							// All variants are in the knowledge base ($knowledge_ids_for_all)

						else if (isset($variantArray) && $num_snvs !== 0 && $knowledge_ids_for_all && $_GET['variant_filter'] === 'included')
						{
							require_once('templates/shared_layouts/form_submit_nav_buttons.php');
						}
						else if (isset($variantArray) && $num_snvs !== 0 && $knowledge_ids_for_all && $_GET['variant_filter'] !== 'included')
						{
					?>
						<div class="alert alert-danger" role="alert">
  							<strong>Nav buttons are only available under toggle Included</strong>
						</div>
					<?php
						}
						else
						{
							require_once('logic/shared_logic/problem_page.php');
						}
					?>  
  				
				</fieldset>
			</form>
<?php
			require_once('templates/shared_layouts/message_board.php')
?>			
			<!-- add the knowledge base for current view of variants for this view.  If  variant_filter=passed only show variants containing these -->
			<?php
				if (isset($variantArray) && $num_snvs !== 0)
          		{          			
					require_once('templates/knowledge_base.php');
				}
			?>
<?php
		} // QC needs to be done before this page can be completed.
		else
		{
?>
			<div class="alert alert-info row" style="font-size: 20px;">
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
					<strong>To avoid the introduction of incorrect data into a database your access has been temporarily denied to this page.  To gain access finish QC sections.  Below is the description of exactly why you were denied.</strong>
					<hr class="section-mark">
				</div>

				<div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
					QC Complete (<?= $qc_complete ? 'Yes':'No';?>)
				</div>

				<div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
					Director Level Permission (<?= strpos($user_permssions, 'director') !== False ? 'Yes':'No';?>)
				</div>

				<div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
					All Variants in knowledge base (<?= $variants_all_in_knowledge_base ? 'Yes':'No';?>)
				</div>

				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" id="chart_div" ></div>

				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
					<hr class="section-mark">
					The QC section that has been integrated into the system was intended to ensure that variant data is as accurate as possible before it is added to the knowledge base via <span class="fas fa-plus btn btn-danger btn-danger-hover" aria-hidden="true"> SNV</span>. If something is entered into the knowledge base incorrectly, not only could it negatively affect this specific case, it could affect countless other future cases before the error is detected. 
				</div>				
			</div>
<?php
		}
?>     
		</div>
	</div>
</div>
