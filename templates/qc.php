<!-- this is the summary for the report -->

<?php
	require_once('templates/lists/training_nav_bar.php');
?>

<?php
	require_once('templates/lists/run_info_list.php');
?>

<div class="container-fluid">
	<div class="row">
		<?php
			require_once('templates/shared_layouts/report_progress_bar.php');
		?>
		<!-- update area -->
		<div id="toggle-report-progress-bar" class="col-xs-10 col-sm-10 col-md-10 col-lg-10">
			
			<fieldset>
				<legend>
					QC Run
				</legend>
<?php

		if(isset($qc_run_step_status) && $qc_run_step_status == 'not_complete')
		{
			
			// Add a chart for run metrics for ofa
			if ($visitArray[0]['ngs_panel'] === 'Oncomine Focus Hotspot Assay')
			{
?>
				<h3 class="raised-card-subheader">Table 13.A.1. QC Metrics - Table of Acceptable Ranges (530 chip)</h3>
				
<?php
				require_once('templates/shared_layouts/qc_run_metrics_ofa_table.php');
?>
				

				<div class="alert alert-success" style="font-size:20px;">
					<strong>Clicking the Passed QC button below confirms you went to Torrent Server -> Data -> Completed Runs & Reports and clicked on the desired Report Name hypertext link and confirmed that the run passed all of the metrics in the table above.</strong> 
				</div>
<?php
			}
			else if ($visitArray[0]['ngs_panel'] === 'Myeloid/CLL Mutation Panel')
			{
?>
				<h3 class="raised-card-subheader">QC Metrics - Table of Acceptable Ranges</h3>
<?php
				require_once('templates/shared_layouts/qc_run_metrics_myeloid_table.php');
?>
				<div class="alert alert-success" style="font-size:20px;">
					<strong>Clicking the Passed QC button below confirms that you performed all of the tasks in the SOP starting on page 38 and the QC Metrics fall in the above acceptable ranges.</strong>
				</div>
<?php
			}
			else
			{
?>
				<div class="alert alert-success" style="font-size:20px;">
					<strong>Clicking the Passed QC button below confirms that this sample has passed all QC run metrics associated with the test.</strong> 
				</div>
<?php 		}
?>
				<div class="row">
					<form id="add_visit_info" class="form" method="post" class="form-horizontal">		
						<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
							<button type="submit" id="<?= $page;?>_submit" name="<?= $page;?>_submit" value="Submit"  class="btn-lg btn-primary btn-primary-hover">
								Passed QC					
							</button>
						</div>
					</form>
				</div>
<?php
		}
		else
		{
				// find who approved the qc
				foreach ($completed_steps as $key => $step)
				{
					if ($step['step'] === 'qc')
					{
?>
				<div class="alert alert-success" style="font-size:20px;">
					QC Run approved by <?= $step['user_name'];?> at <?= $step['time_stamp'];?>
				</div> 
<?php
					}
				}
		}

		// add QC run info if Thermofisher and completed in pool
		if (isset($qc_data['U']) && !empty($qc_data['U']))
		{
			require_once('templates/tables/pool_run_qc_tables.php');
		}
?>
			</fieldset>

		</div>
	</div>
</div>