<div class="container-fluid">	
	
<?php
	if ($view_type === 'alt_variant_found')
	{
?>
	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

			<form class="form" method="post" class="form-horizontal">
				<fieldset>
					<legend>
						<?= $page_title;?> 
					</legend>
<?php
				require_once('templates/knowledge_base.php');	
?>
					<div class="alert alert-danger" role="alert" style="font-size:20px;">

						When possible the URMC NGS report should default to cosmic nomenclature.  The variant in the table above was found in the in the URMC knowledge base.<br><br>
						Does the knowledge base variant describe the same variant as the imported variant name?  Please use the knowledge base variant if it describes the same variant.
						
						<br><br><u>Knowledge Base Variant Name</u>: 
						<ul>
							<li><b>Gene</b>: <?= $knowledge_base[0]['genes'];?></li>
							<li><b>Coding</b>: <?= $knowledge_base[0]['coding'];?></li>
							<li><b>Protein</b>: <?= $knowledge_base[0]['amino_acid_change'];?></li>
						</ul>
			
						<br><br><u>Imported Variant Name</u>:
						<ul>
							<li><b>Gene</b>: <?= $_GET['genes'];?></li>
							<li><b>Coding</b>: <?= $_GET['coding'];?></li>
							<li><b>Protein</b>: <?= $_GET['amino_acid_change'];?></li>
						</ul>					
					</div>
					<div class="form-group row">
						<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
							<button type="submit" id="alt_variant_found_submit" name="alt_variant_found_submit" value="Submit"  class="btn btn-primary btn-primary-hover">
								Change to <?= $knowledge_base[0]['genes'];?> <?= $knowledge_base[0]['coding'];?> (<?= $knowledge_base[0]['amino_acid_change'];?>)
							</button>
						</div>
						<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
					<?php
							// get current address
							$current_address = explode('?page=', $_SERVER['REQUEST_URI'])[1];
					?>
							<a class="btn btn-primary btn-primary-hover" href="?page=<?= $current_address;?>&view_type=add_SNV">
								Keep <?= $_GET['genes'];?> <?= $_GET['coding'];?> (<?= $_GET['amino_acid_change'];?>)
							</a>							
						</div>						
					</div>
				</fieldset>
			</form>
		</div>
	</div>
<?php	
	}

	else if ($view_type === 'add_SNV')
	{
?>
	<div class="row">
		<div id="toggle-report-progress-bar" class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="height:500px;">

			<form class="form" method="post" class="form-horizontal">
				<fieldset>
					<legend id='new_legend' class='show'>
						<?= $page_title; ?> 
					</legend>
					<div class="form-group row">
						<div class="col-xs-12 col-sm-4 col-md-4">
							<label for="genes" class="form-control-label">Gene Name: <span class="required-field">*</span></label>
						</div>
						<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
							<input type="text" class="form-control" id="genes" name="genes" maxlength="20" 
						<?php 
							
								echo 'value="'.$_GET['genes'].'"';
								echo ' readonly="readonly" ';
							
						?> required/>
						</div>
					</div>

					<div class="form-group row">
						<div class="col-xs-12 col-sm-4 col-md-4">
							<label for="coding" class="form-control-label">Coding: <span class="required-field">*</span></label>
						</div>

						<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
							<input type="text" class="form-control" id="coding" name="coding" maxlength="255" 
						<?php 
							
								echo 'value="'.$_GET['coding'].'"';
								echo ' readonly="readonly" ';
							
						?> required/>
						</div>
					</div>
					<div class="form-group row">
						<div class="col-xs-12 col-sm-4 col-md-4">
							<label for="amino_acid_change" class="form-control-label">Amino Acid Change: <span class="required-field">*</span></label>
						</div>
						<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
							<input type="text" class="form-control" id="amino_acid_change" name="amino_acid_change" maxlength="255" 
						<?php 
							if (isset($_GET['amino_acid_change']) && !empty($_GET['amino_acid_change']))
							{
								echo 'value="'.$_GET['amino_acid_change'].'"';
								echo ' readonly="readonly" ';
							}
							else
							{
								echo 'value="NA"';
								echo ' readonly="readonly" ';
							}
							
						?> required/>
						</div>
					</div>
					<div class="form-group row">
						<div class="col-xs-12 col-sm-4 col-md-4">
							<label for="amino_acid_change_abbrev" class="form-control-label">Amino Acid Change Abbrev: <span class="required-field">*</span></label>
						</div>
						<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
							<input type="text" class="form-control" id="amino_acid_change_abbrev" name="amino_acid_change_abbrev" maxlength="255" 
						<?php 
							if (isset($_GET['amino_acid_change']) && !empty($_GET['amino_acid_change']))
							{
								echo 'value="'.$utils->convert_AA_to_short_hand($_GET['amino_acid_change']).'"';
							}
							else
							{
								echo 'value="NA"';
								echo ' readonly="readonly" ';
							}
								
							
						?> required/>
						</div>
							
					</div>
					<div class="form-group row">
						<div class="col-xs-12 col-sm-4 col-md-4">
							<label for="chr" class="form-control-label">chr:</label>
						</div>
						<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
							<input type="text" class="form-control" id="chr" name="chr" maxlength="10" 
						<?php 
							if (isset($genesInfo[0]['chr']) && $genesInfo[0]['chr'] != '')
							{
								echo 'value="'.$genesInfo[0]['chr'].'"';
								echo ' readonly="readonly" ';
							}
						?>/>
						</div>
					</div>
					<div class="alert alert-success" role="alert">
  						Make sure you only include numbers and &#45; as loc.  For Example loc should look like: 130855044&#45;130855044
					</div>
					<div class="form-group row">
						<div class="col-xs-12 col-sm-4 col-md-4">
							<label for="loc" class="form-control-label">loc: </label>
						</div>
						<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
							<input type="text" class="form-control" id="loc" name="loc" maxlength="255" pattern="\d*[\-]\d*"/>
						</div>
					</div>
			
					<div class="form-group row">
						<div class="col-xs-12 col-sm-4 col-md-4">
							<label for="mutation_type" class="form-control-label">Mutation Type: </label>
						</div>
						<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
							<input type="text" list="mutationTypes" name="mutation_type">
							<datalist id="mutationTypes">
								
							<?php

								if (isset($mutationTypes))
								{
									for($i=0;$i<sizeof($mutationTypes);$i++)
			               			{
							?>
										<option value="<?= $mutationTypes[$i]['mutation_type']; ?>" >
											<?= $mutationTypes[$i]['mutation_type']; ?>
										</option>
							<?php
									}
								}
							?>
							</datalist>
						</div>
					</div>
					<div class="alert alert-success" role="alert">
  						Make sure you only include numbers and , as Reference PMIDs.  For Example Reference PMIDs should look like: 11853795,22299775. <br>
  						This link might be a good starting point: <a href="https://www.ncbi.nlm.nih.gov/pubmed/?term=<?= $_GET['genes'];?>+<?= $_GET['coding'];?>+<?= $_GET['amino_acid_change'];?>" target="_blank">pubmed link</a>
					</div>					
					<div class="form-group row">
						<div class="col-xs-12 col-sm-4 col-md-4">
							<label for="pmid" class="form-control-label">Reference PMIDs: </label>
						</div>
						<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
							<input type="text" class="form-control" id="pmid" name="pmid" maxlength="255" pattern="[0-9][\,][0-9]"/>
						</div>
					</div>
					<div class="alert alert-success" role="alert">
  						Make sure you only include numbers for Cosmic ID.  For Example Cosmic IDs should look like: 6407. <br>
  						This link might be a good starting point: <a href="https://cancer.sanger.ac.uk/cosmic/search?q=<?= $_GET['genes'];?>+<?= $_GET['coding'];?>+<?= $_GET['amino_acid_change'];?>" target="_blank">cosmic link</a>
					</div>						
					<div class="form-group row">
						<div class="col-xs-12 col-sm-4 col-md-4">
							<label for="cosmic" class="form-control-label">Cosmic id: </label>
						</div>
						<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
							<input type="number" class="form-control" id="cosmic" name="cosmic" min="0"/>
						</div>
					</div>
					<div class="form-group row">
						<div class="col-xs-12 col-sm-4 col-md-4">
							<label for="hgnc" class="form-control-label">hgnc:</label>
						</div>
						<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
							<input type="text" class="form-control" id="hgnc" readonly="readonly" name="hgnc"  
						<?php 
					;
							if (isset($genesInfo[0]['hgnc']) && $genesInfo[0]['hgnc'] != '')
							{
								echo 'value="'.$genesInfo[0]['hgnc'].'"';
								
							}
						?>/>
						</div>
					</div>

<?php
	if (isset($_GET['visit_id']))
	{
		require_once('templates/shared_layouts/message_board.php');
	}
?>						
					<div class="form-group row">
						<div class="offset-xs-4 offset-sm-4 offset-md-4 offset-lg-4 col-xs-2 col-sm-2 col-md-2 col-lg-2">
							<button type="submit" id="<?= $page;?>_submit" name="<?= $page;?>_submit" value="Submit"  class="btn btn-primary btn-primary-hover">
								Submit
							</button>
						</div>
					</div>
				</fieldset>
			</form>
		</div>
	</div>
<?php
	}
?>
</div>