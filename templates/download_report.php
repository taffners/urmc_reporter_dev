<?php
	if (isset($_GET) && isset($_GET['run_id']))
	{
		require_once('templates/lists/run_info_list.php');
	}
	
?>
<div class="container-fluid">

	<div class="row">
		<?php
			if (isset($_GET) && isset($_GET['run_id']))
			{				
				require_once('templates/shared_layouts/report_progress_bar.php');
		?>
		<div id="toggle-report-progress-bar" class="col-xs-12 col-sm-10 col-md-10 col-lg-10" style="height:500px;">
		<?php
			}
			else
			{
		?>
		<div id="toggle-report-progress-bar" class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="height:500px;">
		<?php
			}
		?>
		
<?php
	if (isset($revisionsArray) && empty($revisionsArray))
	{
?>
		<fieldset>
			<legend>Download Report <span class="fas fa-cloud-download-alt"></span></legend>

			<div class="row">
				<div class="offset-md-2 offset-lg-2 col-xs-12 col-sm-12 col-md-4 col-lg-4">
					<a href="?page=generate_report&<?= EXTRA_GETS;?>&header_status=on" class="btn  btn-primary btn-primary-hover" role="button" target="_blank">
						<img src="images/adobe-pdf-icon.png" alt="pdf icon" style="height:50px;width:50px"> With Header
					</a>
				</div>
				<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
					<a href="?page=generate_report&amp?page=generate_report&<?= EXTRA_GETS;?>&header_status=off" class="btn  btn-primary btn-primary-hover" role="button" target="_blank">
						<img src="images/adobe-pdf-icon.png" alt="pdf icon" style="height:50px;width:50px"> Without Header
					</a>
				</div>
			</div>
<?php
	}
?>
<!-- 	
		BUTTONS TO SHOW JPG IMAGES.  CURRENTLY NOT RUNNING BECAUSE 
		IMAGES ARE BLURRY AND I CAN'T FIGURE OUT HOW TO MAKE THEM
		NOT BLURY.
			<div class="row">
				<div class="col-md-offset-2 col-lg-offset-2 col-xs-12 col-sm-12 col-md-4 col-lg-4">
					<a href="?page=generate_report&amp?page=generate_report&amp;run_id=<?= $_GET['run_id'];?>&amp;visit_id=<?= $_GET['visit_id'];?>&amp;patient_id=<?= $_GET['patient_id'];?>&amp;header_status=on&view_type=jpg" class="btn  btn-primary btn-primary-hover" role="button" target="_blank">
					<img src="images/jpg-2.png" alt="jpg icon" style="height:50px;width:50px">	With Header
					</a>
				</div>				
				<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
					<a href="?page=generate_report&amp?page=generate_report&amp;run_id=<?= $_GET['run_id'];?>&amp;visit_id=<?= $_GET['visit_id'];?>&amp;patient_id=<?= $_GET['patient_id'];?>&amp;header_status=off&view_type=jpg" class="btn  btn-primary btn-primary-hover" role="button" target="_blank">
						<img src="images/jpg-2.png" alt="jpg icon" style="height:50px;width:50px">	Without Header
					</a>
				</div>
			</div> -->
		</fieldset>
<?php
	// Show previous versions of reports if they are available. 
	if (isset($revisionsArray) && !empty($revisionsArray))
	{
?>
		<fieldset>
			<legend>Download Previous Versions of Report <span class="fas fa-cloud-download-alt"></span></legend>
			<?php
			if 	(isset($visitArray[0]) && ($visitArray[0]['control_type_id'] === null || $visitArray[0]['control_type_id'] == 0) && isset($runInfoArray[0]['report_type']))
			{
					$change_btn_types = array(
						'reportable' => 'Change to Validation Report',
						'na' => 'Change to Reportable',
						'validation' => 'Change to Reportable',
						'default' => 'Change to Report type'
					);

					if (isset($runInfoArray[0]['report_type']) && in_array($runInfoArray[0]['report_type'], array_keys($change_btn_types)) )
					{
						$change_btn_name = $change_btn_types[$runInfoArray[0]['report_type']];
					}
					else
					{
						$change_btn_name = $change_btn_types['default'];
					}


			?>
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
					<h3>Report Type is: <b><?= ucfirst(htmlspecialchars($runInfoArray[0]['report_type']));?></b>

						<a href="?page=change_report_type&<?= EXTRA_GETS;?>" class="btn btn-primary btn-primary-hover" role="button" title="Change the status of the report.  Live reports will be added to the patient electronic medical record and validation reports will not be added.">
							<?= htmlspecialchars($change_btn_name);?> 
						</a>
					</h3>
				</div>
			
			<?php
			}
			?>
		
				<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
				<?php
				if 	(	
						isset($user_permssions) && 
						strpos($user_permssions, 'softpath') !== false && 
						isset($runInfoArray[0]['transferred_to_patient_chart']) &&
						isset($runInfoArray[0]['report_type']) && 
						$runInfoArray[0]['report_type'] === 'reportable'
					)
				{
				?>
					<h3 title="After reports are made in <?= SITE_TITLE;?> <?= $all_softpath;?> will download the report without a header, print the report, and scan it into the patient chart.">Report Scan Status:
						<div class="radio">
							<label class="radio-inline"><input type="radio" value="pending" name="scan_status" class="toggle-transferred-to-patient-chart-status" data-run_id="<?= $runInfoArray[0]['run_id']; ?>" data-val="pending" <?php
							if($runInfoArray[0]['transferred_to_patient_chart'] === 'pending')
							{
								echo 'checked="checked"'; 
							}
							?>>Pending</label>
						
							<label class="radio-inline"><input type="radio" value="yes" name="scan_status" class="toggle-transferred-to-patient-chart-status" data-run_id="<?= $runInfoArray[0]['run_id']; ?>" data-val="yes" <?php
							if($runInfoArray[0]['transferred_to_patient_chart'] === 'yes')
							{
								echo 'checked="checked"'; 
							}
							?>>Complete</label>
						</div>
					</h3>
				<?php
				}
				else if (
							isset($runInfoArray[0]['transferred_to_patient_chart']) &&
							isset($runInfoArray[0]['report_type']) && 
							$runInfoArray[0]['report_type'] === 'reportable'
						)
				{
				?>
					<h3 title="After reports are made in <?= SITE_TITLE;?> <?= $all_softpath;?> will download the report without a header, print the report, and scan it into the patient chart.">Report Scan Status:
						<?= $runInfoArray[0]['transferred_to_patient_chart'] === 'yes'? 'Complete':htmlspecialchars($runInfoArray[0]['transferred_to_patient_chart']); ?>
					</h3>
				<?php
				}
				?>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
					<h4>Current Version</h4>
					<hr class="section-mark">
				</div>
				<!-- include current versions of report -->
				<div class="offset-md-2 offset-lg-2 col-xs-12 col-sm-12 col-md-4 col-lg-4">
					<a href="?page=generate_report&amp;<?= EXTRA_GETS;?>&amp;header_status=on" class="btn  btn-primary btn-primary-hover" role="button" target="_blank">
						<img src="images/adobe-pdf-icon.png" alt="pdf icon" style="height:50px;width:50px"> With Header
					</a>
				</div>
				<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
					<a href="?page=generate_report&amp?page=generate_report&amp;<?= EXTRA_GETS;?>&amp;header_status=off" class="btn  btn-primary btn-primary-hover" role="button" target="_blank">
						<img src="images/adobe-pdf-icon.png" alt="pdf icon" style="height:50px;width:50px"> Without Header
					</a>
				</div>
			</div>
			<div class="row">


<?php
		// get all previous versions of files
		if (isset($previous_reports) && !empty($previous_reports))
		{
			if (sizeof($previous_reports) > 1)
			{
				$h4 = 'Previous Versions';
			}
			else
			{
				$h4 = 'Previous Version';
			}
?>
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				<h4><?= $h4;?></h4>
				<hr class="section-mark">
			</div>
<?php
			foreach ($previous_reports as $key => $f)
			{
				$ver = str_replace('.pdf', '', $f);
?>
				<div class="offset-md-2 offset-lg-2 col-md-4 col-lg-4 col-xs-12 col-sm-12">
					<a href="?page=previous_ver&amp;report_name=<?= $report_name;?>&amp;<?= EXTRA_GETS;?>&amp;ver=<?= $ver;?>" class="btn  btn-primary btn-primary-hover" role="button" target="_blank">
						<img src="images/adobe-pdf-icon.png" alt="pdf icon" style="height:50px;width:50px"> Version <?= $ver;?>
					</a>
				</div>
<?php
			}
		}
?>
			</div>
		</fieldset>

<?php
	}
?>
<?php
		require_once('templates/shared_layouts/message_board.php')
?>		
	</div>
</div>