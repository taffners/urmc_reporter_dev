<!-- add knowledge info -->
<?php
	require_once('templates/knowledge_base.php');
?>

<div class="container-fluid" style="margin-top:10px;">
	<div class="row">
	     <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12"> 
			<table class="formated_table sort_table">
				<thead>
					<th>Tier</th>
					<th>Tier Summary</th>
					<th>Tissue</th>
					<th>Add By</th>
					<th>TimeStamp</th>
					<th>Report</th>
				</thead>

				<tbody>
					<?php
					
						foreach($tiers as $i => $ea_array)
						{

					?>
							<tr>
								<td><?= $ea_array['tier'];?></td>
								<td><?= $ea_array['tier_summary'];?></td>
								<td><?= $ea_array['tissue'];?></td>
								<td><?= $ea_array['user_name'];?></td>
                                <td><?= $ea_array['time_stamp'];?></td>

                                <td>
                           <?php
                                if ($ea_array['report_link'] !== '')
                                {
                           ?>
                                     <a href="<?= $ea_array['report_link']?>" target="_blank" class="btn btn-success  btn-success-hover">
                                     	<img src="images/adobe-pdf-icon.png" alt="pdf icon" style="height:30px;width:30px">
                                     </a>
                           <?php
                                }

                           ?>

                                </td>
							</tr>
					<?php
						}
					?>
				</tbody>
			</table>
		</div>
	</div>
	
</div>