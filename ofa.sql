SELECT slbt.test_tissue, ovt.genes, kbt.*

FROM visit_table vt

JOIN ordered_test_table ott ON vt.visit_id = ott.visit_id

JOIN sample_log_book_table slbt ON slbt.sample_log_book_id = ott.sample_log_book_id

JOIN run_info_table rit ON rit.run_id = vt.run_id 

JOIN observed_variant_table ovt ON ovt.run_id = rit.run_id

JOIN knowledge_base_table kbt ON ovt.genes = kbt.genes AND ovt.coding =  kbt.coding AND ovt.amino_acid_change = kbt.amino_acid_change

WHERE ovt.include_in_report = "1" AND ovt.genes = "BRAF" AND slbt.test_tissue = "Appendix"



SELECT slbt.test_tissue, ovt.genes, COUNT(vt.visit_id) AS count

FROM visit_table vt

JOIN ordered_test_table ott ON vt.visit_id = ott.visit_id

JOIN sample_log_book_table slbt ON slbt.sample_log_book_id = ott.sample_log_book_id

JOIN run_info_table rit ON rit.run_id = vt.run_id 

JOIN observed_variant_table ovt ON ovt.run_id = rit.run_id

WHERE ovt.include_in_report = "1"

GROUP BY slbt.test_tissue, ovt.genes


SELECT cpr_vw.*, otst.test_name, np.type AS NGS_panel, rit.status, "0" AS num_reported_variants

FROM completed_patient_reports_vw cpr_vw 

LEFT JOIN ordered_test_table ott ON ott.visit_id = cpr_vw.visit_id 

LEFT JOIN sample_log_book_table slbt ON ott.sample_log_book_id = slbt.sample_log_book_id

LEFT JOIN orderable_tests_table otst ON otst.orderable_tests_id = ott.orderable_tests_id

LEFT JOIN ngs_panel np ON np.ngs_panel_id = cpr_vw.ngs_panel_id

JOIN run_info_table rit ON rit.run_id = cpr_vw.run_id

WHERE rit.status = "completed"  AND NOT EXISTS ( SELECT nrvpr_vw2.run_id FROM num_reported_variants_per_run_id_vw nrvpr_vw2 WHERE nrvpr_vw2.run_id = cpr_vw.run_id) AND slbt.test_tissue LIKE "%lymph%";



CREATE TABLE IF NOT EXISTS confirmation_table (
	confirmation_id int NOT NULL AUTO_INCREMENT,
	
	-- foreign key
	user_id INT,
	run_id INT,
	visit_id INT,
	observed_variant_id INT,
	

	-- data	
	outcome varchar(15),
	status varchar(10),
	message TEXT,

	time_stamp timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,

	PRIMARY KEY (confirmation_id),
	UNIQUE KEY confirmation_id (confirmation_id)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1;


SELECT vt.visit_id, vt.run_id, psv.time_stamp, rit.status, vt.soft_lab_num 

FROM visit_table vt

LEFT JOIN run_info_table rit ON rit.run_id = vt.run_id 

LEFT JOIN pre_step_visit psv ON vt.visit_id = psv.visit_id AND psv.step = "upload_NGS_data"

WHERE (rit.status = 'pending' OR rit.status = 'completed') AND vt.ngs_panel_id = 1





SELECT vt.visit_id, vt.run_id, vt.time_stamp 

FROM visit_table vt

LEFT JOIN run_info_table rit ON rit.run_id = vt.run_id 

WHERE rit.status = 'pending' AND vt.ngs_panel_id = 1


CREATE VIEW extraction_count_per_sample_vw AS 

SELECT group_concat(elt.stock_conc separator ',') AS stock_concentrations, group_concat(elt.dilutions_conc separator ',') AS dilution_concentrations, count(ott.sample_log_book_id) AS extraction_count, ott.sample_log_book_id AS sample_log_book_id, elt.ordered_test_id AS ordered_test_id 

FROM extraction_log_table elt 

Left Join ordered_test_table ott ON ott.ordered_test_id = elt.ordered_test_id 

GROUP BY ott.sample_log_book_id




INSERT INTO test_reporting_regulation_info_table (user_id, orderable_tests_id, method, limitations,disclaimer, go_live_date) VALUES
(1,40,'Tissue sections are reviewed by a pathologist and relevant tumor is selected for analysis.  FFPE tissue section(s) or extracted DNA is tested using the Idylla BRAF assay (real-time PCR assay) tested on the BioCartis Idylla system.', 'Clinical indication for use of this assay include:  (1) Guideline-recommended mutation evaluation for target therapy;  (2) Evaluation of potential gene targets for possible clinical trials;  (3) Interrogating actionable mutations in other human tumor types or poorly differentiated tumor/uncertain origin based on clinical pathological evaluation.', 'This test was developed by Biocartis US, Inc. (Jersey City, NJ) and its performance characteristics determined by UR Central Laboratories.  It has not been cleared or approved by the U.S. Food and Drug Administration (FDA).  The FDA has determined that such clearance or approval is not necessary for clinical use of this test.  The methodology used for this test was approved by the New York State Department of Health.  The results should not be used as a sole diagnostic or therapeutic criterion.', '2021-02-09');

INSERT INTO test_reporting_regulation_info_table (user_id, orderable_tests_id, method, limitations,disclaimer, go_live_date) VALUES
(1,'39', 'Tissue sections are reviewed by a pathologist and relevant tumor is selected for analysis.  FFPE tissue section(s) or extracted DNA is tested using the Idylla EGFR assay (real-time PCR assay) tested on the BioCartis Idylla system.  This assay detects EGFR mutations in Exons 18, 19, 20 and 21.', 'Clinical indication for use of this assay include:  (1) Guideline-recommended mutation evaluation for non-small cell lung cancer (NSCLC);  (2) Evaluation of potential gene targets for possible clinical trials;  (3) Interrogating actionable mutations in other human tumor types or poorly differentiated tumor/uncertain origin based on clinical pathological evaluation.', 'This test was developed by Biocartis US, Inc. (Jersey City, NJ) and its performance characteristics determined by UR Central Laboratories.  It has not been cleared or approved by the U.S. Food and Drug Administration (FDA).  The FDA has determined that such clearance or approval is not necessary for clinical use of this test.  The methodology used for this test was approved by the New York State Department of Health.  The results should not be used as a sole diagnostic or therapeutic criterion.', '2021-02-09');
INSERT INTO test_reporting_regulation_info_table (user_id, orderable_tests_id, method, limitations,disclaimer, go_live_date) VALUES
(1,'38', 'Tissue sections are reviewed by a pathologist and relevant tumor is selected for analysis.  FFPE tissue section(s) or extracted DNA is tested using the Idylla KRAS assay (real-time PCR assay) tested on the BioCartis Idylla system.  This assay detects KRAS mutations in Exons 2, 3 and 4.', 'Clinical indication for use of this assay include:  (1) Guideline-recommended mutation evaluation for non-small cell lung cancer (NSCLC);  (2) The updated Provisional Clinical Opinion (PCO) update published by ASCO on RAS gene mutation testing in mCRC;  (3) Evaluation of potential gene targets for possible clinical trials;  (4) Interrogating actionable mutations in other human tumor types or poorly differentiated tumor/uncertain origin based on    clinical pathological evaluation.', 'This test was developed by Biocartis US, Inc. (Jersey City, NJ) and its performance characteristics determined by UR Central Laboratories.  It has not been cleared or approved by the U.S. Food and Drug Administration (FDA).  The FDA has determined that such clearance or approval is not necessary for clinical use of this test.  The methodology used for this test was approved by the New York State Department of Health.  The results should not be used as a sole diagnostic or therapeutic criterion.', '2021-02-09');


				  38 | KRAS Idylla          |
|                 39 | EGFR Idylla          |
|                 40 | BRAF Idylla  

SELECT 			rit.*
FROM 			run_info_table rit 
INNER JOIN 		observed_variant_table ovt ON rit.run_id = ovt.run_id 
WHERE 			ovt.include_in_report = 1
OR 				NOT EXISTS 	(
								SELECT 	ovt2.run_id 
								FROM 	observed_variant_table ovt2
								WHERE 	ovt2.run_id = rit.run_id
							) 

SELECT vt.visit_id, rit.run_id, vt.soft_lab_num, rit.status
FROM visit_table vt 
JOIN run_info_table rit ON rit.run_id = vt.run_id
INNER JOIN observed_variant_table ovt ON vt.run_id = ovt.run_id 
WHERE rit.status = 'completed'  AND (vt.control_type_id = 0 OR vt.control_type_id IS NULL) AND ( ovt.include_in_report = 0 OR NOT EXISTS ( SELECT ovt2.run_id FROM observed_variant_table ovt2 WHERE ovt2.run_id = vt.run_id)) 

SELECT vt.visit_id, rit.run_id, vt.soft_lab_num, rit.status
FROM visit_table vt 
JOIN run_info_table rit ON rit.run_id = vt.run_id

WHERE rit.status = 'completed'  AND (vt.control_type_id = 0 OR vt.control_type_id IS NULL) AND NOT EXISTS ( SELECT nrvpr_vw2.run_id FROM num_reported_variants_per_run_id_vw nrvpr_vw2 WHERE nrvpr_vw2.run_id = vt.run_id)

CREATE VIEW num_reported_variants_per_run_id_vw AS
SELECT COUNT(ovt.observed_variant_id) AS num_reported_variants, ovt.run_id
FROM observed_variant_table ovt 
WHERE ovt.include_in_report = 1
GROUP BY ovt.run_id


SELECT cpr_vw.*, rit.status
FROM completed_patient_reports_vw cpr_vw 
JOIN run_info_table rit ON rit.run_id = cpr_vw.run_id

WHERE rit.status = 'completed'  AND NOT EXISTS ( SELECT nrvpr_vw2.run_id FROM num_reported_variants_per_run_id_vw nrvpr_vw2 WHERE nrvpr_vw2.run_id = cpr_vw.run_id)


SELECT cpr_vw.*, rit.status FROM completed_patient_reports_vw cpr_vw JOIN run_info_table rit ON rit.run_id = cpr_vw.run_id rit.status = "completed" AND NOT EXISTS ( SELECT nrvpr_vw2.run_id FROM num_reported_variants_per_run_id_vw nrvpr_vw2 nrvpr_vw2.run_id = cpr_vw.run_id)


CREATE TABLE IF NOT EXISTS test_reporting_regulation_info_table (
	test_reporting_regulation_info_id int NOT NULL AUTO_INCREMENT,
	
	-- foreign key
	user_id INT,
	orderable_tests_id INT,
	

	-- data	

	method TEXT,
	limitations TEXT,
	disclaimer TEXT,
	go_live_date DATE,

	time_stamp timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,

	PRIMARY KEY (test_reporting_regulation_info_id),
	UNIQUE KEY test_reporting_regulation_info_id (test_reporting_regulation_info_id)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1;


INSERT INTO test_reporting_regulation_info_table (user_id, orderable_tests_id, method, limitations,disclaimer, go_live_date) VALUES
(1,35,'Tissue sections are reviewed by a pathologist and relevant tumor is selected for analysis.  FFPE tissue section(s) or extracted DNA is tested using the Idylla NRAS-BRAF assay (real-time PCR assay) tested on the BioCartis Idylla system.  This assay detects NRAS mutations in Exons 2, 3, 4, and BRAF mutation in Exon 15.', 'Clinical indication for use of this assay include: (1) Guideline-recommended mutation evaluation for non-small cell lung cancer (NSCLC); (2) The updated Provisional Clinical Opinion (PCO) update published by ASCO on RAS gene mutation testing in mCRC; (3) Evaluation of potential gene targets for possible clinical trails; (4) Interrogating actionable mutations in other human tumor types or poorly differentiated tumor/uncertain origin based on clinical pathological evaluation.  * Due to lower sensitivity, the NRAS G12S variant will not be reported.', 'This test was developed by Biocartis US, Inc. (Jersey City, NJ) and its performance characteristics determined by UR Central Laboratories.  It has not been cleared or approved by the U.S. Food and Drug Administration (FDA).  The FDA had determined that such clearance or approval is not necessary for clinical use of this test.  The methodology used for this test was approved by the New York State Department of Health.  The results should not be used as a sole diagnostic or therapeutic criterion.', '2021-02-09');

CREATE VIEW assigned_directors_vw AS 
SELECT tipt.*, ott.assigned_director_user_id, CONCAT(ut.first_name," " ,ut.last_name) AS assigned_director_name  FROM tests_in_pool_table tipt LEFT JOIN ordered_test_table ott ON tipt.ordered_test_id = ott.ordered_test_id LEFT JOIN user_table ut ON ut.user_id = ott.assigned_director_user_id;

CREATE VIEW assigned_directors_in_single_analyte_pool_vw AS
SELECT single_analyte_pool_id, GROUP_CONCAT(assigned_director_name) AS all_assigned_directors FROM assigned_directors_vw GROUP BY single_analyte_pool_id;

CREATE VIEW test_hotspots_grouped_by_ordered_test_genetic_call_vw AS 
SELECT ordered_test_genetic_call,orderable_tests_id, genes, GROUP_CONCAT(coding) AS codings, GROUP_CONCAT(amino_acid_change) AS amino_acid_changes,exon, codon, reported, date_name_added, GROUP_CONCAT(test_hotspots_id) AS test_hotspots_ids FROM test_hotspots_table GROUP BY orderable_tests_id, ordered_test_genetic_call;

CREATE TABLE IF NOT EXISTS reporting_steps_by_test_table (
	reporting_steps_by_test_id int NOT NULL AUTO_INCREMENT,
	
	-- foreign key
	user_id INT,
	orderable_tests_id INT,
	reporting_flow_steps_id INT,
	ngs_panel_id INT,

	-- data	

	time_stamp timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,

	PRIMARY KEY (reporting_steps_by_test_id),
	UNIQUE KEY reporting_steps_by_test_id (reporting_steps_by_test_id)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1;

CREATE TABLE IF NOT EXISTS reporting_flow_steps_table (
	reporting_flow_steps_id int NOT NULL AUTO_INCREMENT,
	
	-- foreign key
	user_id INT,

	-- data	
	description TEXT,
	no_variant_status VARCHAR(4),
	step_name VARCHAR(20),
	flow_type VARCHAR(10),
	step_status VARCHAR(10),

	time_stamp timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,

	PRIMARY KEY (reporting_flow_steps_id),
	UNIQUE KEY reporting_flow_steps_id (reporting_flow_steps_id)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1;

CREATE TABLE IF NOT EXISTS user_homepage_table (
	user_homepage_id int NOT NULL AUTO_INCREMENT,
	
	-- foreign key
	user_id INT,

	-- data	
	
	feature VARCHAR(40),
	feature_choice VARCHAR(40),

	time_stamp timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,

	PRIMARY KEY (user_homepage_id),
	UNIQUE KEY user_homepage_id (user_homepage_id)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1;

CREATE TABLE IF NOT EXISTS extra_tracked_table (
	extra_tracked_id int NOT NULL AUTO_INCREMENT,
	
	-- foreign key
	ref_id INT,
	user_id INT,
	-- data	
	
	ref_table VARCHAR(30),
	field_name VARCHAR(30),
	field_val VARCHAR(30),
	field_type VARCHAR(10),

	time_stamp timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,

	PRIMARY KEY (extra_tracked_id),
	UNIQUE KEY extra_tracked_id (extra_tracked_id)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1;

CREATE TABLE IF NOT EXISTS observed_hotspots_table (
	observed_hotspots_id int NOT NULL AUTO_INCREMENT,
	
	-- foreign key
	ordered_test_id INT,
	test_hotspots_id INT,
	user_id INT,
	-- data	
	
	time_stamp timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,

	PRIMARY KEY (observed_hotspots_id),
	UNIQUE KEY observed_hotspots_id (observed_hotspots_id)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1;

CREATE TABLE IF NOT EXISTS test_hotspots_table (
	test_hotspots_id int NOT NULL AUTO_INCREMENT,
	
	-- foreign key
	orderable_tests_id INT,
	user_id INT,
	-- data	

	genes VARCHAR(30),
	amino_acid_change varchar(255),
	coding varchar(255),
	exon varchar(30),
	codon varchar(30),
	ordered_test_genetic_call varchar(20),
	mutation_abbr varchar(20),
	reported varchar(3),
	
	date_name_added DATE,
	
	time_stamp timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,

	PRIMARY KEY (test_hotspots_id),
	UNIQUE KEY test_hotspots_id (test_hotspots_id)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1;

INSERT INTO control_type_table (user_id, type, control_name, catalog_number, description, dummy_last_name, dummy_first_name, dummy_sex, dummy_dob, dummy_medical_record_num) VALUES (1, 'negative', 'NTC', 'NTC', 'The no template control (NTC) monitors contamination and primer-dimer formation that could produce false positive results.','NTC', 'NTC', '', 1901-01-01, 'NTC');
-- nras braf idylla
INSERT INTO test_hotspots_table (orderable_tests_id, user_id, genes, exon, codon, mutation_abbr, amino_acid_change, coding, ordered_test_genetic_call,  reported, date_name_added) VALUES
(35, 1, 'NRAS', 2, 12, 'G12D',  'p.Gly12Asp', 'c.35G>A', 'G12D','yes', '2020-12-22'),
(35, 1, 'NRAS', 2, 12, 'G12C',  'p.Gly12Cys', 'c.34G>T', 'G12C','yes', '2020-12-22'),
(35, 1, 'NRAS', 2, 12, 'G12S',  'p.Gly12Ser', 'c.34G>A', 'G12S','no', '2020-12-22'),

(35, 1, 'NRAS', 2, 12, 'G12A',  'p.Gly12Ala or p.Gly12Val', 'c.35G>C or c.35G>T', 'G12A/V','yes', '2020-12-22'),


(35, 1, 'NRAS', 2, 13, 'G13D',  'p.Gly13Asp', 'c.38G>A', 'G13D','yes', '2020-12-22'),

(35, 1, 'NRAS', 2, 13, 'G13R',  'p.Gly13Arg or p.Gly13Val', 'c.37G>C or c.38G>T', 'G13R/V','yes', '2020-12-22'),


(35, 1, 'NRAS', 3, 59, 'A59T',  'p.Ala59Thr', 'c.175G>A', 'A59T','yes', '2020-12-22'),
(35, 1, 'NRAS', 3, 61, 'Q61K',  'p.Gln61Lys', 'c.181C>A', 'Q61K','yes', '2020-12-22'),
(35, 1, 'NRAS', 3, 61, 'Q61R',  'p.Gln61Arg', 'c.182A>G', 'Q61R','yes', '2020-12-22'),
(35, 1, 'NRAS', 3, 61, 'Q61L',  'p.Gln61Leu', 'c.182A>T', 'Q61L','yes', '2020-12-22'),
(35, 1, 'NRAS', 3, 61, 'Q61H',  'p.Gln61His', 'c.183A>C', 'Q61H','yes', '2020-12-22'),
(35, 1, 'NRAS', 3, 61, 'Q61H',  'p.Gln61His', 'c.183A>T', 'Q61H','yes', '2020-12-22'),

(35, 1, 'NRAS', 4, 117, 'K117N',  'p.Lys117Asn', 'c.351G>C or c.351G>T', 'K117N','yes', '2020-12-22'),

(35, 1, 'NRAS', 4, 146, '',  'p.Ala146Thr or p.Ala146Val', 'c.436G>A or c.437C>T', 'A146T/V','yes', '2020-12-22'),

(35, 1, 'BRAF', 15, 600, '',  'p.Val600Glu or p.Val600Asp', 'c.1799T>A, c.1799_1800delinsAA or c.1799_1800delinsAC', 'V600E/D','yes', '2020-12-22'),

(35, 1, 'BRAF', 15, 600, '',  'p.Val600Lys or p.Val600Arg', 'c.1798_1799delinsAA', 'V600K/R','yes', '2020-12-22');


INSERT INTO test_hotspots_table (orderable_tests_id, user_id, genes, exon, codon, mutation_abbr, amino_acid_change, coding, ordered_test_genetic_call,  reported, date_name_added) VALUES
(37, 1, '', '', '', 'ACVR2A',  '', '', 'ACVR2A','yes', '2020-12-22'),
(37, 1, '', '', '' , 'BTBD7',  '', '', 'BTBD7','yes', '2020-12-22'),
(37, 1, '', '', '' , 'DIDO1',  '', '', 'DIDO1','yes', '2020-12-22'),
(37, 1, '', '', '' , 'MRE11',  '', '', 'MRE11','yes', '2020-12-22'),
(37, 1, '','', '' , 'RYR3',  '', '', 'RYR3','yes', '2020-12-22'),
(37, 1, '', '', '' , 'SEC31A',  '', '', 'SEC31A','yes', '2020-12-22'),
(37, 1, '', '', '' , 'SULF2',  '', '', 'SULF2','yes', '2020-12-22');

INSERT INTO test_hotspots_table (orderable_tests_id, user_id, genes, exon, codon, mutation_abbr, amino_acid_change, coding, ordered_test_genetic_call,  reported, date_name_added) VALUES
(36, 1, '', '', '', 'BAT-25',  '', '', 'BAT-25','yes', '2020-12-22'),
(36, 1, '', '', '', 'BAT-26',  '', '', 'BAT-26','yes', '2020-12-22'),
(36, 1, '', '', '', 'NR-21',  '', '', 'NR-21','yes', '2020-12-22'),
(36, 1, '', '', '' , 'NR-24',  '', '', 'NR-24','yes', '2020-12-22'),
(36, 1, '', '', '' , 'MONO-27',  '', '', 'MONO-27','yes', '2020-12-22');

INSERT INTO orderable_tests_table (user_id, assay_type_id , test_name, full_test_name , test_type , consent_required , custom_fields_available , notes_for_work_sheet , turnaround_time_depends_on_ordered_test , previous_positive_required , mol_num_required , test_status , ngs_panel_id , include_in_turn_around_calculation , time_depends_on_result , turnaround_time_tracked , extraction_info , quantification_info , thermal_cyclers_instruments , sanger_sequencing_instruments , wet_bench_techs , analysis_techs , test_result , send_out_test , max_num_samples_per_run) VALUES

(1, 12 , 'NRAS-BRAF Idylla', 'NRAS-BRAF Hotspot Analysis by Idylla' , 'oncology' , 'no' , 'no' , '' , 'no' , 'no' , 'yes' , 'Not Active' , 0 , 'yes' , 'no' , 'yes' , 'no' , 'no' , 'no' , 'no' , 'no' , 'no' , 'no' , 'no' , 'no');

INSERT INTO test_hotspots_table (orderable_tests_id, user_id, genes, exon, codon, mutation_abbr, amino_acid_change, coding, ordered_test_genetic_call,  reported, date_name_added) VALUES
(38, 1, 'KRAS', 2, 12, 'G12A',  'p.Gly12Ala', 'c.35G>C', 'G12A','yes', '2020-12-22'),
(38, 1, 'KRAS', 2, 12, 'G12C',  'p.Gly12Cys', 'c.34G>T', 'G12C','yes', '2020-12-22'),
(38, 1, 'KRAS', 2, 12, 'G12D',  'p.Gly12Asp', 'c.35G>A', 'G12D','yes', '2020-12-22'),
(38, 1, 'KRAS', 2, 12, 'G12R',  'p.Gly12Arg', 'c.34G>C', 'G12R','yes', '2020-12-22'),
(38, 1, 'KRAS', 2, 12, 'G12S',  'p.Gly12Ser', 'c.34G>A', 'G12S','yes', '2020-12-22'),
(38, 1, 'KRAS', 2, 12, 'G12V',  'p.Gly12Val', 'c.35G>T', 'G12V','yes', '2020-12-22'),
(38, 1, 'KRAS', 2, 13, 'G13D',  'p.Gly13Asp', 'c.38G>A', 'G13D','yes', '2020-12-22'),
(38, 1, 'KRAS', 3, 59, 'A59T/E/G',  'p.Ala59Glu, p.Ala59Gly, or p.Ala59Thr', 'c.176C>A, c.176C>G, or c.175G>A', 'A59T/E/G','yes', '2020-12-22'),
(38, 1, 'KRAS', 3, 61, 'Q61H',  'p.Gln61His', 'c.183A>C or c.183A>T', 'Q61H','yes', '2020-12-22'),
(38, 1, 'KRAS', 3, 61, 'Q61K',  'p.Gln61Lys', 'c.181C>A or c.180_181delinsAA', 'Q61K','yes', '2020-12-22'),
(38, 1, 'KRAS', 3, 61, 'Q61L/R',  'p.Gln61Leu or Gln61Arg', 'c.182A>T or c.182A>G', 'Q61L/R','yes', '2020-12-22'),
(38, 1, 'KRAS', 4, 117, 'K117N',  'p.Lys117Asn', 'c.351A>C or c.351A>T', 'K117N','yes', '2020-12-22'),
(38, 1, 'KRAS', 4, 146, 'A146P/T/V',  'p.Ala146Pro, p.Ala146Thr, or p.Ala146Val', 'c.436G>C, c.436G>A, or c.437C>T', 'A146P/T/V','yes', '2020-12-22');


INSERT INTO test_hotspots_table (orderable_tests_id, user_id, genes, exon, codon, mutation_abbr, amino_acid_change, coding, ordered_test_genetic_call,  reported, date_name_added) VALUES
(39, 1, 'EGFR', 18, '', 'G12A',  'p.Gly719Ala', 'c.2156G>C', 'G12A','yes', '2020-12-22'),
(39, 1, 'EGFR', 18, '', 'G719C',  'p.Gly719Cys', 'c.2155G>T or c.2154_2155delinsTT', 'G719C','yes', '2020-12-22'),
(39, 1, 'EGFR', 18, '', 'G719S',  'p.Gly719Ser', 'c.2155G>A', 'G719S','yes', '2020-12-22'),
(39, 1, 'EGFR', 19, '', 'Del 9',  'p.Leu747_Ala750delinsPro, p.Leu747_Ala750delinsSer, p.Leu747_Ala750delinsSer, or p.Leu747_Glu749del', 'c.2238_2248delinsGC, c.2239_2248delinsC, c.2240_2248del, or c.2239_2247del', 'Del 9','yes', '2020-12-22'),
(39, 1, 'EGFR', 19, '', 'Del 12',  'p.Leu747_Thr751delinsPro or p.Leu747_Thr751delinsSer', 'c.2239_2251delinsC or c.2240_2251del', 'Del 12','yes', '2020-12-22'),
(39, 1, 'EGFR', 19, '', 'Del 15',  'E746_A750del, L747_T751del, E746_T751delinsA, E746_T751delinsI, E746_T751delinsV, K745_A750delinsT, E746_T751delinsL, E746_T751delinsV, E746_T751delinsA, E746_T751delinsQ or I744_A750delinsVK'
, '2235_2249del, 2236_2250del, 2239_2253del, 2240_2254del, 2238_2252del, 2237_2251del, 2235_2252delinsAAT, 2237_2252delinsT, 2234_2248del, 2236_2253delinsCTA, 2237_2253delinsTA, 2235_2251delinsAG, 2236_2253delinsCAA, or 2230_2249delinsGTCAA', 'Del 15','yes', '2020-12-22'),



(39, 1, 'EGFR', 19, '', 'Del 18',  'L747_P753delinsS, E746_S752delinsV, L747_S752del, E746_T751del, L747_P753delinsQ, E746_S752delinsAla, E746_S752delinsD, E746_P753delinsVS, E746_S752delinsI, E746_S752delinsI, E746_S752delinsV, E746_S752delinsV, or E746_S752delinsV', 'c.2240_2257del, c.2237_2255delinsT, c.2239_2256del, c.2236_2253del, c.2239_2258delinsCA, c.2237_2254del, c.2238_2255del, c.2237_2257delinsTCT, c.2236_2255delinsAT, c.2236_2256delinsATC, c.2237_2256delinsTT, c.2237_2256delinsTC, or c.2235_2255delinsGGT', 'Del 18','yes', '2020-12-22'),


(39, 1, 'EGFR', 19, '', 'Del 21',  'p.Leu747_Pro753del or p.Glu746_Ser752del', 'c.2238_2258del or c.2236_2256del', 'Del 21','yes', '2020-12-22'),


(39, 1, 'EGFR', 19, '', 'Del 24',  'p.Ser752_Ile759del', 'c.2253_2276del', 'Del 24','yes', '2020-12-22'),


(39, 1, 'EGFR', 20, '', 'InsASV(11)',  'p.Val769_Asp770insAlaSerVal', 'c.2309_2310delinsCCAGCGTGGAT', 'InsASV(11)','yes', '2020-12-22'),


(39, 1, 'EGFR', 20, '', 'InsASV(9)',  'p.Val769_Asp770insAlaSerVal', 'c.2307_2308insGCCAGCGTG', 'InsASV(9)','yes', '2020-12-22'),


(39, 1, 'EGFR', 20, '', 'InsG',  'p.Asp770_Asn771insGly', 'c.2310_2311insGGT', 'InsG','yes', '2020-12-22'),


(39, 1, 'EGFR', 20, '', 'InsH',  'p.His773_Val774insHis', 'c.2319_2320insCAC', 'InsH','yes', '2020-12-22'),


(39, 1, 'EGFR', 20, '', 'InsSVD',  'p.Asp770_Asn771insSerValAsp', 'c.2311_2312insGCGTGGACA', 'InsSVD','yes', '2020-12-22'),


(39, 1, 'EGFR', 21, '', 'L858R',  'p.Leu858Arg', 'c.2573T>G, c.2573_2574delinsGT, c.2573_2574delinsGA', 'L858R','yes', '2020-12-22'),


(39, 1, 'EGFR', 21, '', 'L861Q',  'p.Leu861Gln', 'c.2582T>A', 'L861Q','yes', '2020-12-22'),


(39, 1, 'EGFR', 20, '', 'S768I',  'p.Ser768Ile', 'c.2303G>T', 'S768I','yes', '2020-12-22'),


(39, 1, 'EGFR', 20, '', 'T790M',  'p.Thr790Met', 'c.2369C>T', 'T790M','yes', '2020-12-22');






INSERT INTO test_hotspots_table (orderable_tests_id, user_id, genes, exon, codon, mutation_abbr, amino_acid_change, coding, ordered_test_genetic_call,  reported, date_name_added) VALUES
(40, 1, 'BRAF', 15, '', 'V600E/E2/D',  'p.Val600Glu or Val600Asp', 'c.1799T>A, c.1799_1800delinsAA, c.1799T_1800delinsAT, or c.1799T_1800delinsAC', 'V600E/E2/D','yes', '2020-12-22'),
(40, 1, 'BRAF', 15, '', 'V600K/R/M',  'p.Val600Lys, p.Val600Arg, or p.Val600Met', 'c.1798_1799delinsAA, c.1798_1799delinsAG, or c.1798G>A', 'V600K/R/M','yes', '2020-12-22');

CREATE VIEW single_analyte_unique_hotspots_genetic_call_vw AS
SELECT DISTINCT tht.ordered_test_genetic_call, tht.genes, tht.orderable_tests_id
FROM test_hotspots_table tht


CREATE VIEW single_analyte_pool_instruments_vw AS 

SELECT DISTINCT it.*, "added" AS sap_status, tipt.single_analyte_pool_id FROM tests_in_pool_table tipt LEFT JOIN ordered_test_table ott ON ott.ordered_test_id = tipt.ordered_test_id JOIN instrument_used_table iut ON iut.ref_id = ott.ordered_test_id AND	iut.ref_table = "ordered_test_table" LEFT JOIN instrument_table it ON it.instrument_id = iut.instrument_id

CREATE TABLE IF NOT EXISTS ngs_panel_names_table(
	ngs_panel_names_id int NOT NULL AUTO_INCREMENT,
	
	-- foreign key
	ngs_panel_id INT,
	user_id INT,
	-- data	

	full_name VARCHAR(100),
	title VARCHAR(100),
	abbrevation VARCHAR(10),

	date_name_added DATE,
	
	time_stamp timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,

	PRIMARY KEY (ngs_panel_names_id),
	UNIQUE KEY ngs_panel_names_id (ngs_panel_names_id)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1;

CREATE TABLE IF NOT EXISTS single_analyte_pool_table(
	single_analyte_pool_id int NOT NULL AUTO_INCREMENT,
	
	-- foreign key
	orderable_tests_id INT,
	user_id INT,
	-- data	

	status VARCHAR(20),
	start_date DATE,
	order_num INT,

	time_stamp timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,

	PRIMARY KEY (single_analyte_pool_id),
	UNIQUE KEY single_analyte_pool_id (single_analyte_pool_id)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1;

CREATE TABLE IF NOT EXISTS tests_in_pool_table(
	tests_in_pool_id int NOT NULL AUTO_INCREMENT,
	
	-- foreign key
	ordered_test_id INT,
	single_analyte_pool_id INT,
	-- data	

	time_stamp timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,

	PRIMARY KEY (tests_in_pool_id),
	UNIQUE KEY tests_in_pool_id (tests_in_pool_id)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1;

CREATE TABLE IF NOT EXISTS pages_bug_fix_xref_table(
	pages_bug_fix_xref_id int NOT NULL AUTO_INCREMENT,
	
	-- foreign key
	page_id INT,
	bug_fix_id INT,
	-- data	

	time_stamp timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,

	PRIMARY KEY (pages_bug_fix_xref_id),
	UNIQUE KEY pages_bug_fix_xref_id (pages_bug_fix_xref_id)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1;

CREATE TABLE IF NOT EXISTS bug_fix_table(
	bug_fix_id int NOT NULL AUTO_INCREMENT,
	
	-- foreign key
	bugs_reported_id INT,
	-- data	
	user_name VARCHAR(100),
	commit_id VARCHAR(10),
	website_name VARCHAR(100),
	summary_of_bug TEXT,
	resulted_in_downtime VARCHAR(3),
	downtime_type VARCHAR(10),
	downtime_start_time datetime,
	downtime_end_time datetime,

	time_stamp timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,

	PRIMARY KEY (bug_fix_id),
	UNIQUE KEY bug_fix_id (bug_fix_id)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1;

CREATE TABLE IF NOT EXISTS bugs_reported_table(
	bugs_reported_id int NOT NULL AUTO_INCREMENT,
	
	-- foreign key
	
	-- data	
	user_name VARCHAR(100),
	website_name VARCHAR(100),
	status VARCHAR(20),
	bug_description TEXT,

	time_stamp timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,

	PRIMARY KEY (bugs_reported_id),
	UNIQUE KEY bugs_reported_id (bugs_reported_id)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1;

CREATE TABLE IF NOT EXISTS status_linked_reflexes_xref_table(
	status_linked_reflexes_xref_id int NOT NULL AUTO_INCREMENT,
	
	-- foreign key
	user_id INT,
	ordered_reflex_test_id INT,
	reflex_test_link_id INT,
	-- data	
	status VARCHAR(20),

	time_stamp timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,

	PRIMARY KEY (status_linked_reflexes_xref_id),
	UNIQUE KEY status_linked_reflexes_xref_id (status_linked_reflexes_xref_id)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1;

CREATE TABLE IF NOT EXISTS ordered_reflex_test_table(
	ordered_reflex_test_id int NOT NULL AUTO_INCREMENT,
	
	-- foreign key
	user_id INT,
	reflex_id INT,
	sample_log_book_id INT,
	-- data	
	reflex_status VARCHAR(20),

	time_stamp timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,

	PRIMARY KEY (ordered_reflex_test_id),
	UNIQUE KEY ordered_reflex_test_id (ordered_reflex_test_id)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1;

CREATE TABLE IF NOT EXISTS user_previous_password_md5s_table(
	user_previous_password_md5s_id int NOT NULL AUTO_INCREMENT,
	
	-- foreign key
	user_id INT,

	-- data	
	password VARCHAR(60),

	time_stamp timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,

	PRIMARY KEY (user_previous_password_md5s_id),
	UNIQUE KEY user_previous_password_md5s_id (user_previous_password_md5s_id)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1;


CREATE TABLE IF NOT EXISTS reflex_test_link_table(
	reflex_test_link_id int NOT NULL AUTO_INCREMENT,
	
	-- foreign key
	user_id INT,
	reflex_id INT, 
	orderable_tests_id INT, 

	-- data	
	reflex_order INT,
	reflex_value VARCHAR(20),

	time_stamp timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,

	PRIMARY KEY (reflex_test_link_id),
	UNIQUE KEY reflex_test_link_id (reflex_test_link_id)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1;

CREATE TABLE IF NOT EXISTS reflex_table(
	reflex_id int NOT NULL AUTO_INCREMENT,
	
	-- foreign key
	user_id INT,
	-- data	
	reflex_name TEXT,

	time_stamp timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,

	PRIMARY KEY (reflex_id),
	UNIQUE KEY reflex_id (reflex_id)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1;


CREATE VIEW instruments_used_vw AS

SELECT DISTINCT iut.instrument_id, it.yellow_tag_num, it.task, it.manufacturer, it.serial_num, it.clinical_engineering_num, it.model, it.description

FROM instrument_used_table iut 

LEFT JOIN instrument_table it ON it.instrument_id = iut.instrument_id

WHERE it.yellow_tag_num IS NOT NULL

ORDER BY it.yellow_tag_num ASC

CREATE TABLE IF NOT EXISTS module_table(
	module_id int NOT NULL AUTO_INCREMENT,
	
	-- foreign key

	-- data	
	module_name varchar(100),
	module_diagram varchar(100),
	module_description TEXT,

	time_stamp timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,

	PRIMARY KEY (module_id),
	UNIQUE KEY module_id (module_id)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1;

CREATE VIEW qc_validations_per_page_vw AS

SELECT page_id, COUNT(page_validation_id) AS num_validations, GROUP_CONCAT(page_validation_id ORDER BY time_stamp DESC SEPARATOR ", ") as all_page_validation_ids

FROM page_validation_table 

GROUP BY page_id


CREATE TABLE IF NOT EXISTS page_validation_step_table(
	page_validation_step_id int NOT NULL AUTO_INCREMENT,
	
	-- foreign key
	page_validation_id INT,
	page_id INT,
	check_list_validation_step_id INT,

	-- data	
	actual_result TEXT,

	time_stamp timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,

	PRIMARY KEY (page_validation_step_id),
	UNIQUE KEY page_validation_step_id (page_validation_step_id)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1;

CREATE VIEW check_list_validation_step_ordered_vw AS

SELECT * FROM check_list_validation_step_table ORDER BY category, field_type


CREATE TABLE IF NOT EXISTS check_list_validation_step_table(
	check_list_validation_step_id int NOT NULL AUTO_INCREMENT,
	
	-- foreign key


	-- data	
	step_name VARCHAR(255),
	description TEXT,
	category TEXT, 
	field_type VARCHAR(30),

	time_stamp timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,

	PRIMARY KEY (check_list_validation_step_id),
	UNIQUE KEY check_list_validation_step_id (check_list_validation_step_id)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1;

CREATE TABLE IF NOT EXISTS page_validation_table(
	page_validation_id int NOT NULL AUTO_INCREMENT,
	
	-- foreign key


	-- data	
	user_name VARCHAR(100),
	browser_version VARCHAR(100),

	website_version VARCHAR(50),

	time_stamp timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,

	PRIMARY KEY (page_validation_id),
	UNIQUE KEY page_validation_id (page_validation_id)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1;




CREATE TABLE IF NOT EXISTS page_table(
	page_id int NOT NULL AUTO_INCREMENT,
	
	-- foreign key
	user_id INT,

	-- data	
	user_name VARCHAR(100),
	page_name VARCHAR(100),
	page_description TEXT, 
	how_to_access_page TEXT,
	page_assumption TEXT,

	time_stamp timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,

	PRIMARY KEY (page_id),
	UNIQUE KEY page_id (page_id)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1;

CREATE TABLE IF NOT EXISTS lock_record_table(
	lock_record_id int NOT NULL AUTO_INCREMENT,
	
	-- foreign key
	ref_id INT,
	table_name INT,
	user_id INT,
	lock_page_id INT,

	-- data	
	
	time_stamp timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,

	PRIMARY KEY (lock_record_id),
	UNIQUE KEY lock_record_id (lock_record_id)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1;

CREATE TABLE IF NOT EXISTS lock_page_table(
	lock_page_id int NOT NULL AUTO_INCREMENT,
	
	-- foreign key
	user_id INT,	

	-- data	
	lock_seconds_length INT,
	page_lock_name VARCHAR(100),
	ref_id_name VARCHAR(100),
	ref_id INT,
	table_name VARCHAR(100),

	time_stamp timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,

	PRIMARY KEY (lock_page_id),
	UNIQUE KEY lock_page_id (lock_page_id)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1;
#######################
SELECT DISTINCT gcbpt.gene, SUBSTRING_INDEX(gcbpt.accession_num, '.',1) As accessioin FROM genes_covered_by_panel_table gcbpt JOIN ngs_panel np ON np.ngs_panel_id = gcbpt.ngs_panel_id GROUP BY gene, accession_num;


CREATE VIEW user_update_task_vw AS
SELECT uutt.ref_id, uutt.ref, uutt.task, GROUP_CONCAT(ut.first_name, " ", ut.last_name, " (",uutt.time_stamp,"): ",uutt.after_val ORDER BY uutt.user_update_task_id ASC SEPARATOR ", ") AS change_log, GROUP_CONCAT(uutt.before_val ORDER BY uutt.user_update_task_id ASC SEPARATOR ", ") AS before_vals, GROUP_CONCAT(uutt.after_val ORDER BY uutt.user_update_task_id ASC SEPARATOR ", ") AS after_vals  

FROM user_update_task_table uutt 

LEFT JOIN user_table ut ON ut.user_id = uutt.user_id 

GROUP BY uutt.ref_id, uutt.ref, uutt.task 



#########################################
CREATE VIEW editors_per_step_pre_step_visit_vw AS
SELECT vt.visit_id,  GROUP_CONCAT(ut.first_name, "->", psv.step ORDER BY psv.time_stamp ASC SEPARATOR ", ") AS editors_per_step

FROM visit_table vt

INNER JOIN  pre_step_visit psv USING(visit_id)

LEFT JOIN user_table ut ON psv.user_id = ut.user_id

GROUP BY vt.visit_id

#####################################################
CREATE VIEW sent_out_pre_step_visit_vw AS
SELECT vt.visit_id,  COUNT(psv.pre_step_visit_id) AS sent_out_count

FROM visit_table vt

LEFT JOIN pre_step_visit psv ON psv.visit_id = vt.visit_id AND psv.step = "sent_out"

GROUP BY vt.visit_id

###############################################
CREATE VIEW pre_complete_pre_step_visit_vw AS
SELECT vt.visit_id,  COUNT(psv.pre_step_visit_id) AS pre_complete_count, GROUP_CONCAT(psv.step ORDER BY psv.time_stamp ASC SEPARATOR ", ") AS pre_complete_steps

FROM visit_table vt

LEFT JOIN pre_step_visit psv ON psv.visit_id = vt.visit_id AND psv.step = "pre_complete"

GROUP BY vt.visit_id


#######################################

CREATE VIEW flagged_pre_step_visit_vw AS
SELECT vt.visit_id,  COUNT(psv.pre_step_visit_id) AS flagged_count, GROUP_CONCAT(psv.step ORDER BY psv.time_stamp ASC SEPARATOR ", ") AS flagged_steps

FROM visit_table vt

LEFT JOIN pre_step_visit psv ON psv.visit_id = vt.visit_id AND psv.status = "Flagged"

GROUP BY vt.visit_id

#######################################

CREATE VIEW passed_pre_step_visit_vw AS
SELECT vt.visit_id,  COUNT(psv.pre_step_visit_id) AS pass_count, GROUP_CONCAT(psv.step ORDER BY psv.time_stamp ASC SEPARATOR ", ") AS steps_passed

FROM visit_table vt

LEFT JOIN pre_step_visit psv ON psv.visit_id = vt.visit_id AND psv.status = "passed"

GROUP BY vt.visit_id

########################

CREATE VIEW failed_pre_step_visit_vw AS
SELECT vt.visit_id,  COUNT(psv.pre_step_visit_id) AS fail_count, GROUP_CONCAT(psv.step ORDER BY psv.time_stamp ASC SEPARATOR ", ") AS steps_fail

FROM visit_table vt

LEFT JOIN pre_step_visit psv ON psv.visit_id = vt.visit_id AND psv.status = "Failed"

GROUP BY vt.visit_id



#####################################

SELECT vt.visit_id,  COUNT(psv_passed.pre_step_visit_id) AS pass_count, COUNT(psv_failed.pre_step_visit_id) AS fail_count, GROUP_CONCAT(psv_passed.step ORDER BY psv_passed.time_stamp ASC SEPARATOR ", ") AS steps_passed, GROUP_CONCAT(psv_failed.step ORDER BY psv_failed.time_stamp ASC SEPARATOR ", ") AS steps_failed 

FROM visit_table vt

LEFT JOIN pre_step_visit psv_passed ON psv_passed.visit_id = vt.visit_id AND psv_passed.status = "passed"

LEFT JOIN pre_step_visit psv_failed ON psv_failed.visit_id = vt.visit_id AND psv_failed.status = "Failed"

GROUP BY vt.visit_id, psv_passed.status, psv_failed.status

SELECT * FROM (

SELECT vtp.visit_id,  'Pass' AS status, 
COUNT(psvp.pre_step_visit_id) AS count, 
GROUP_CONCAT(psvp.step ORDER BY psvp.time_stamp ASC SEPARATOR ", ") AS steps

FROM visit_table vtp

INNER JOIN pre_step_visit psvp ON psvp.visit_id = vtp.visit_id AND psvp.status = "passed"

GROUP BY vtp.visit_id

UNION 

SELECT vtf.visit_id,  'Fail' AS status, 
COUNT(psvf.pre_step_visit_id) AS count, 
GROUP_CONCAT(psvf.step ORDER BY psvf.time_stamp ASC SEPARATOR ", ") AS steps 

FROM visit_table vtf

INNER JOIN pre_step_visit psvf ON psvf.visit_id = vtf.visit_id AND psvf.status = "Failed"

GROUP BY vtf.visit_id
) AS step_report
ORDER BY visit_id

#########################################

SELECT vt.visit_id, pt.last_name, pt.first_name, vt.soft_lab_num, pt.medical_record_num, vt.mol_num, pt.patient_id,  vt.visit_id, vt.sample_type, np.type, vt.time_stamp

FROM visit_table vt 

LEFT JOIN patient_table pt ON pt.patient_id = vt.patient_id 

LEFT JOIN ngs_panel np ON np.ngs_panel_id = vt.ngs_panel_id

WHERE vt.run_id = 0 AND vt.sample_type <> 'Sent Out'

ORDER BY vt.visit_id;



#############################
CREATE VIEW complete_myeloid_tests_vw AS
SELECT GROUP_CONCAT(vt.visit_id SEPARATOR ", ") AS all_visits, vt.visit_id, pt.patient_id, ott.ordered_test_id, vt.soft_lab_num

FROM patient_table pt

LEFT JOIN visit_table vt ON vt.patient_id = pt.patient_id 

LEFT JOIN ordered_test_table ott ON ott.visit_id = vt.visit_id

LEFT JOIN pre_step_visit psv ON psv.visit_id = vt.visit_id AND psv.status != 'passed'

WHERE ott.orderable_tests_id = 12 AND ott.test_status = 'complete'

GROUP BY psv.visit_id, vt.patient_id;

#############################
UPDATE ordered_test_table

SET ott.test_status = 'stop'

WHERE ott.ordered_test_id IN (

	SELECT ott.ordered_test_id, sample_log_book_id, pt.last_name, pt.first_name, vt.soft_lab_num, pt.medical_record_num, vt.mol_num, pt.patient_id,  vt.visit_id, vt.sample_type, ott.test_status

	FROM patient_table pt

	LEFT JOIN visit_table vt ON vt.patient_id = pt.patient_id 

	LEFT JOIN ordered_test_table ott ON ott.visit_id = vt.visit_id

	LEFT JOIN pre_step_visit psv ON psv.visit_id = vt.visit_id AND psv.status != 'passed'

	WHERE ott.orderable_tests_id = 12 AND ott.test_status = 'pending' AND psv.status = 'Failed' 

	GROUP BY pt.patient_id
	)

###############################
SELECT ott.sample_log_book_id, pt.last_name, pt.first_name, vt.soft_lab_num, pt.medical_record_num, vt.mol_num, GROUP_CONCAT(vt2.visit_id SEPARATOR", ") AS all_ordered_tests

FROM ordered_test_table ott 

LEFT JOIN visit_table vt ON vt.visit_id = ott.visit_id 

LEFT JOIN pre_step_visit psv ON psv.visit_id = vt.visit_id AND psv.status != 'passed'

LEFT JOIN patient_table pt ON pt.patient_id = vt.patient_id



LEFT JOIN visit_table vt2 ON pt.patient_id = vt2.patient_id

WHERE ott.orderable_tests_id = 12 AND ott.test_status = 'pending' AND psv.status = 'Failed' 

GROUP BY psv.visit_id, vt2.patient_id;
###############################
SELECT ott.ordered_test_id, ott.sample_log_book_id, vt.visit_id, vt.soft_lab_num, vt.mol_num, GROUP_CONCAT(psv.step, ':', psv.status SEPARATOR ", ") AS ngs_status

FROM ordered_test_table ott 

LEFT JOIN visit_table vt ON vt.visit_id = ott.visit_id 

LEFT JOIN pre_step_visit psv ON psv.visit_id = vt.visit_id AND psv.status != 'passed'

WHERE ott.orderable_tests_id = 12 AND ott.test_status = 'pending' AND psv.status = 'Failed' 

GROUP BY psv.visit_id;
###########################################



CREATE TABLE IF NOT EXISTS user_update_task_table(
	user_update_task_id int NOT NULL AUTO_INCREMENT,
	
	-- foreign key
	ref_id INT,
	user_id INT,

	-- data	
	task VARCHAR(20),
	ref VARCHAR(30),
	before_val VARCHAR(255),
	after_val VARCHAR(255),

	time_stamp timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,

	PRIMARY KEY (user_update_task_id),
	UNIQUE KEY user_update_task_id (user_update_task_id)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1;

CREATE TABLE IF NOT EXISTS excel_workbook_num_table(
	excel_workbook_num_id int NOT NULL AUTO_INCREMENT,
	
	user_id INT,

	time_stamp timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,

	PRIMARY KEY (excel_workbook_num_id),
	UNIQUE KEY excel_workbook_num_id (excel_workbook_num_id)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=50;

ALTER TABLE extraction_log_table ADD COLUMN dilution_type VARCHAR(10);
ALTER TABLE extraction_log_table ADD COLUMN vol_dna DECIMAL;
ALTER TABLE extraction_log_table ADD COLUMN vol_eb DECIMAL;
ALTER TABLE extraction_log_table ADD COLUMN dilution_final_vol DECIMAL;
ALTER TABLE extraction_log_table ADD COLUMN elution_volume DECIMAL;

ALTER TABLE library_pool_table ADD COLUMN ts_date DATE DEFAULT NULL;

ALTER TABLE library_pool_table ADD COLUMN ts_runid VARCHAR(10);

ALTER TABLE library_pool_table ADD COLUMN ts_filesystempath TEXT;	

ALTER TABLE library_pool_table ADD COLUMN ts_experiment_id INT;

-- DROP AND re add pending_samples_substring_order_num_vw
INSERT INTO control_step_regulator_table (control_type_id,user_id,step_regulator_name,status,field_name) values 
(1,1,'version_lot','on','Lot-use#'),
(2,1,'version_lot','on','Version-use#'),
(3,1,'version_lot','on','Version-use#'),
(4,1,'version_lot','on','Version-use#'),
(5,1,'version_lot','on','Version-use#'),
(6,1,'version_lot','on','Version-use#'),
(7,1,'version_lot','on','Version-use#');



ALTER TABLE sample_log_book_table ADD initial_login_user_id INT;

ALTER TABLE library_pool_table ADD status varchar(20) DEFAULT 'pending';

ALTER TABLE visits_in_pool_table ADD sample_name varchar(255);
ALTER TABLE visits_in_pool_table ADD patient_name varchar(255);

ALTER TABLE library_pool_table ADD library_prep_date DATE DEFAULT NULL;
ALTER TABLE library_pool_table ADD run_date DATE DEFAULT NULL;

ALTER TABLE library_pool_table ADD reagent_cartidge varchar(100);


ALTER TABLE ngs_panel ADD max_num_chips int DEFAULT 0;

UPDATE ngs_panel SET max_num_chips = 2 WHERE ngs_panel_id = 1;
UPDATE ngs_panel SET max_num_chips = 1 WHERE ngs_panel_id = 2;

ALTER TABLE ngs_panel ADD max_sample_per_chip int DEFAULT 0;
UPDATE ngs_panel SET max_sample_per_chip = 8 WHERE ngs_panel_id = 2;
UPDATE ngs_panel SET max_sample_per_chip = 13 WHERE ngs_panel_id = 1;

ALTER TABLE sample_log_book_table ADD sample_type_toggle VARCHAR(20) DEFAULT 'sample';

ALTER TABLE stock_dilution_table ADD order_num int DEFAULT 0;
MariaDB [urmc_reporter_db]> SELECT * FROM assay_type_table;
+---------------+-------------------------------------+
| assay_type_id | assay_name                          |
+---------------+-------------------------------------+
|             1 | Lightcycler (Capillary)             |
|             2 | Lightcycler (480)                   |
|             3 | Fragment analysis                   |
|             4 | gel                                 |
|             5 | extraction                          |
|             6 | ngs                                 |
|             7 | Sanger Sequencing                   |
|             8 | Lightcycler (Capillary)/Sanger Seq  |
|             9 | Send out                            |
|            10 | Fragment analysis/Sanger Sequencing |
+---------------+-------------------------------------+


INSERT INTO orderable_tests_table (user_id, assay_type_id, test_name, full_test_name, test_type, consent_required, custom_fields_available, turnaround_time_depends_on_ordered_test, previous_positive_required, mol_num_required, dependency_orderable_tests_id) VALUES
(1, 7, 'CEBPN', 'CEBPA', 'oncology', 'no', 'no', 'yes', 'no', 'no', 9);

INSERT INTO test_turnaround_table(user_id,orderable_tests_id,dependent_result,status,turnaround_time) VALUES (1,27,'any','include',7);


INSERT INTO assay_type_table (user_id,assay_name) VALUES (1, 'Quantification');

INSERT INTO orderable_tests_table (user_id, assay_type_id, test_name, full_test_name, test_type, consent_required, custom_fields_available, turnaround_time_depends_on_ordered_test, previous_positive_required, mol_num_required) VALUES
(1, 5, 'Nanodrop', 'Nanodrop', 'Quant', 'no', 'no', 'no', 'no', 'no');

UPDATE orderable_tests_table SET assay_type_id = 11 WHERE orderable_tests_id = 28;

INSERT INTO test_turnaround_table(user_id,orderable_tests_id,dependent_result,status,turnaround_time) VALUES (1,28,'NA','NA',0);

CREATE TABLE IF NOT EXISTS sequencing_platform_table(
	sequencing_platform_id int NOT NULL AUTO_INCREMENT,
	
	-- foreign key
	user_id INT,
	-- data	
	company VARCHAR(20),

	time_stamp timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,

	PRIMARY KEY (sequencing_platform_id),
	UNIQUE KEY sequencing_platform_id (sequencing_platform_id)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1;

ALTER TABLE ngs_panel ADD COLUMN sequencing_platform_id INT;

INSERT INTO sequencing_platform_table (company, user_id) VALUES
('Thermofisher', '1'),
('Illumina', '1');
UPDATE ngs_panel SET sequencing_platform_id = 1 WHERE ngs_panel_id =1;
UPDATE ngs_panel SET sequencing_platform_id = 2 WHERE ngs_panel_id =2;

CREATE TABLE IF NOT EXISTS reagent_version_table(
	reagent_version_id int NOT NULL AUTO_INCREMENT,
	
	-- foreign key
	ref_id INT,
	user_id INT,

	-- data	
	ref_type VARCHAR(10),
	reagent_version_field VARCHAR(30),
	reagent_version_value VARCHAR(10),

	time_stamp timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,

	PRIMARY KEY (reagent_version_id),
	UNIQUE KEY reagent_version_id (reagent_version_id)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1;



CREATE TABLE IF NOT EXISTS qc_run_metrics_table(
	qc_run_metrics_id int NOT NULL AUTO_INCREMENT,
	
	-- foreign key
	library_pool_id INT,
	user_id INT,

	-- data	
	metric VARCHAR(20),
	val DECIMAL,
	unit VARCHAR(4),
	status VARCHAR(10),
	accepted_range VARCHAR(50),

	time_stamp timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,

	PRIMARY KEY (qc_run_metrics_id),
	UNIQUE KEY qc_run_metrics_id (qc_run_metrics_id)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1;




CREATE TABLE IF NOT EXISTS torrent_results_table(
	torrent_results_id int NOT NULL AUTO_INCREMENT,
	
	-- foreign key
	pool_chip_linker_id INT,
	user_id INT,

	-- data	
	chipA_results_name VARCHAR(255),
	chipB_results_name VARCHAR(255),

	time_stamp timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,

	PRIMARY KEY (torrent_results_id),
	UNIQUE KEY torrent_results_id (torrent_results_id)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1;

CREATE TABLE IF NOT EXISTS sent_email_table(
	sent_email_id int NOT NULL AUTO_INCREMENT,
	
	-- foreign key
	sender_user_id INT,
	recipient_user_id INT,
	ref_id INT,

	-- data	
	ref_table VARCHAR(20),
	email_type VARCHAR(30),

	time_stamp timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,

	PRIMARY KEY (sent_email_id),
	UNIQUE KEY sent_email_id (sent_email_id)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1;


CREATE TABLE IF NOT EXISTS ngs_flow_steps_table(
	ngs_flow_steps_id int NOT NULL AUTO_INCREMENT,
	
	-- foreign key
	

	-- data	
	step_regulator_name VARCHAR(20),
	description TEXT,
	field_controller_required VARCHAR(3),

	time_stamp timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,

	PRIMARY KEY (ngs_flow_steps_id),
	UNIQUE KEY ngs_flow_steps_id (ngs_flow_steps_id)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1;

INSERT INTO ngs_flow_steps_table (step_regulator_name, description, field_controller_required) VALUES
('generate_run_template', 'Controls if the folder where run templates will be downloaded to instead of locally to the user.  If location is equal to local the file will be downloaded to the user and not saved on a remote drive.', 'yes');


INSERT INTO ngs_flow_steps_table (step_regulator_name, description, field_controller_required) VALUES
('hapmap', 'Controls if the HapMap Alt Variant Frequency runs in the QC Variant.  This column provides evidence of a site specific noise level across the genome negative controls were run.  Locations that have greater than 0% VAF will be shown in the HapMap Alt Variant Freq column. This does have a caveat that the normal sample could contain a very low cellularity of pathogenic cells so this is just supporting evidence in making a decision of pathogenicity.', 'no'),
('strand_bias_calc', 'If this step is turned on the strand bias table to automatically calculate strand bias will be present.  Otherwise directions will be provided on how to obtain strand bias from IGV.', 'no'),
('DNA_CONC_Instrument', 'This step will control if a quantification for a standard sample is required while measuring the DNA Concentration on the pool DNA Concentration page.  When using this flow step a field controller of the type of instrument is required.  For instance Quantus or Qubit.', 'yes');


CREATE TABLE IF NOT EXISTS doc_table(
	doc_id int NOT NULL AUTO_INCREMENT,
	
	-- foreign key
	user_id int,
	ref_id int,

	-- data	
	ref_table VARCHAR(20),
	file_loc VARCHAR(255),
	md5_hash VARCHAR(32),
	file_size INT,
	name_uploaded_as VARCHAR(255),

	time_stamp timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,

	PRIMARY KEY (doc_id),
	UNIQUE KEY doc_id (doc_id)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1;

CREATE TABLE IF NOT EXISTS primer_table(
	primer_id int NOT NULL AUTO_INCREMENT,
	
	-- foreign key
	user_id int,

	-- data	
	primer_name VARCHAR(20),
	primer_seq VARCHAR(100),
	uM VARCHAR(10),

	time_stamp timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,

	PRIMARY KEY (primer_id),
	UNIQUE KEY primer_id (primer_id)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1;

CREATE TABLE IF NOT EXISTS primers_lot_table(
	primers_lot_id int NOT NULL AUTO_INCREMENT,
	
	-- foreign key
	primer_id int,
	user_id int,

	-- data	
	lot VARCHAR(20),
	received_date DATE,
	expiration_date DATE,
	reconsitute_date DATE,

	time_stamp timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,

	PRIMARY KEY (primers_lot_id),
	UNIQUE KEY primers_lot_id (primers_lot_id)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1;

CREATE TABLE IF NOT EXISTS primer_in_group_table(
	primer_in_group_id int NOT NULL AUTO_INCREMENT,
	
	-- foreign key
	primer_id int,
	user_id int,
	primers_group_id int,

	-- data	
	x_vol INT,
	primer_type VARCHAR(10),

	time_stamp timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,

	PRIMARY KEY (primer_in_group_id),
	UNIQUE KEY primer_in_group_id (primer_in_group_id)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1;

CREATE TABLE IF NOT EXISTS primers_group_table(
	primers_group_id int NOT NULL AUTO_INCREMENT,
	
	-- foreign key
	ref_id int,
	user_id int,

	-- data	
	gene VARCHAR(20),
	exon VARCHAR(10),
	cdna_start INT,
	cdna_stop INT,
	refseq_mrna VARCHAR(20),
	size INT,
	dmso VARCHAR(3),
	tm DECIMAL(5,2),

	time_stamp timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,

	PRIMARY KEY (primers_group_id),
	UNIQUE KEY primers_group_id (primers_group_id)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1;

CREATE TABLE IF NOT EXISTS thermocycle_conditions_table(
	thermocycle_conditions_id int NOT NULL AUTO_INCREMENT,
	
	-- foreign key
	primers_group_id int,
	user_id int,

	-- data	
	protocol TEXT,

	time_stamp timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,

	PRIMARY KEY (thermocycle_conditions_id),
	UNIQUE KEY thermocycle_conditions_id (thermocycle_conditions_id)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1;

CREATE TABLE IF NOT EXISTS sequencing_conditions_table(
	sequencing_conditions_id int NOT NULL AUTO_INCREMENT,
	
	-- foreign key
	primers_group_id int,
	user_id int,

	-- data	
	protocol text,

	time_stamp timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,

	PRIMARY KEY (sequencing_conditions_id),
	UNIQUE KEY sequencing_conditions_id (sequencing_conditions_id)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1;

CREATE TABLE IF NOT EXISTS mm_table(
	mm_id int NOT NULL AUTO_INCREMENT,
	
	-- foreign key
	primers_group_id int,
	user_id int,

	-- data	
	water_1x_vol INT,
	buffer_nam VARCHAR(20),
	buffer_1x_vol INT,
	dmso_1x_vol INT,
	dntps_x_amount VARCHAR(10),
	dntps_x_vol INT,
	dmso INT,
	polymerase_nam VARCHAR(20),
	polymerase_u_ml VARCHAR(20),
	polymerase_1x_ul INT,
	protocol TEXT,

	time_stamp timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,

	PRIMARY KEY (mm_id),
	UNIQUE KEY mm_id (mm_id)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1;

CREATE TABLE IF NOT EXISTS instrument_validation_form_table(
	instrument_validation_form_id int NOT NULL AUTO_INCREMENT,
	
	-- foreign key
	instrument_validation_id int,
	user_id int,

	-- data	
	validation_file_loc VARCHAR(255),
	name_uploaded_as VARCHAR(50),
	file_size INT,
	md5_hash VARCHAR(32),

	time_stamp timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,

	PRIMARY KEY (instrument_validation_form_id),
	UNIQUE KEY instrument_validation_form_id (instrument_validation_form_id)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1;

CREATE TABLE IF NOT EXISTS instrument_validation_table(
	instrument_validation_id int NOT NULL AUTO_INCREMENT,
	
	-- foreign key
	instrument_id int,
	user_id int,

	-- data
	validation_date DATE,

	time_stamp timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,

	PRIMARY KEY (instrument_validation_id),
	UNIQUE KEY instrument_validation_id (instrument_validation_id)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1;


CREATE TABLE IF NOT EXISTS lab_address_table(
	lab_address_id int NOT NULL AUTO_INCREMENT,
	
	-- foreign key

	-- data
	active_date DATE,
	line_1 VARCHAR(50),
	line_2 VARCHAR(50),
	line_3 VARCHAR(50),
	line_4 VARCHAR(50),
	line_5 VARCHAR(50),

	time_stamp timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,

	PRIMARY KEY (lab_address_id),
	UNIQUE KEY lab_address_id (lab_address_id)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1;

INSERT INTO lab_address_table (active_date,line_1,line_2,line_3,line_4,line_5) VALUES
('2017-09-22','UR MEDICINE LABS','Strong Memorial Hospital','Department of Pathology and Laboratory Medicine','601 Elmwood Avenue Rochester, New York 14642','Phone: (585)275-7574 / Fax: (585)276-2272'),
('2019-07-08','UR MEDICINE LABS','Department of Pathology and Laboratory Medicine','211 Bailey Road West Henrietta, New York 14586','Phone: (585)275-7574 / Fax: (585)276-2272','');

CREATE TABLE IF NOT EXISTS automatic_email_table(
	automatic_email_id int NOT NULL AUTO_INCREMENT,
	
	-- foreign key

	-- data
	date_sent DATE,
	permission_group_emailed VARCHAR(20),
	automatic_email_type VARCHAR(10),
	message TEXT,
	
	time_stamp timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,

	PRIMARY KEY (automatic_email_id),
	UNIQUE KEY automatic_email_id (automatic_email_id)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1;


CREATE TABLE IF NOT EXISTS instrument_service_contract_table(
	instrument_service_contract_id int NOT NULL AUTO_INCREMENT,
	
	-- foreign key
	user_id int,
	instrument_id int,

	-- data
	expiration_date DATE,
	po VARCHAR(20),
	
	time_stamp timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,

	PRIMARY KEY (instrument_service_contract_id),
	UNIQUE KEY instrument_service_contract_id (instrument_service_contract_id)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1;

CREATE TABLE IF NOT EXISTS extraction_log_table(
	extraction_log_id int NOT NULL AUTO_INCREMENT,
	
	-- foreign key
	user_id int,
	ordered_test_id int,

	-- data
	date_extracted DATE,
	extraction_method VARCHAR(10),

	stock_conc INT,

	dilution_exist VARCHAR(3),
	dilutions_conc INT,
	
	time_stamp timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,

	PRIMARY KEY (extraction_log_id),
	UNIQUE KEY extraction_log_id (extraction_log_id)

) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1;

CREATE TABLE IF NOT EXISTS task_table(
	task_id int NOT NULL AUTO_INCREMENT,
	
	-- foreign key
	user_id int,
	
	-- data
	task VARCHAR(50),
	
	time_stamp timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,

	PRIMARY KEY (task_id),
	UNIQUE KEY task_id (task_id)

) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1;

INSERT INTO task_table (user_id, task) VALUES
(1, 'Aliquoted_for_extraction'),
(1, 'PE_PK_56_digestion'),
(1, 'Extraction');

CREATE TABLE IF NOT EXISTS users_performed_task_table(
	users_performed_task_id int NOT NULL AUTO_INCREMENT,
	
	-- foreign key
	user_id int,
	task_id INT,
	ref_id INT,

	-- data
	ref_table VARCHAR(50),
	date_performed DATE,
	time_performed TIME,
	
	time_stamp timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,

	PRIMARY KEY (users_performed_task_id),
	UNIQUE KEY users_performed_task_id (users_performed_task_id)

) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1;

CREATE TABLE IF NOT EXISTS instrument_table(
	instrument_id int NOT NULL AUTO_INCREMENT,
	
	-- foreign key
	user_id int,

	-- data
	yellow_tag_num INT,
	description VARCHAR(50),
	manufacturer VARCHAR(50),
	serial_num VARCHAR(50),
	clinical_engineering_num VARCHAR(50),
	
	time_stamp timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,

	PRIMARY KEY (instrument_id),
	UNIQUE KEY instrument_id (instrument_id)

) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1;

INSERT INTO instrument_table (user_id, yellow_tag_num, description, manufacturer, serial_num, clinical_engineering_num) VALUES
(1, 26, 'gDNA Extractor', 'Beckman SPRI-TE (A)', '2177340', ''),
(1, 27, 'gDNA Extractor', 'Beckman SPRI-TE (A)', '2177342', '');

INSERT INTO instrument_table (user_id, yellow_tag_num, description, manufacturer, serial_num, clinical_engineering_num) VALUES
(1, 46, 'Thermal cycler', 'ABI 9700', '', '00063427'),
(1, 60, 'Thermal cycler', 'Veriti', '', '00078158'),
(1, 84, 'Thermal cycler', 'Roche Perkin Elmer 9600', '501509', '00031757'),
(1, 124, 'Thermal cycler', 'ABI 9700', '80556051819', '51936'),
(1, 68, 'Thermal cycler', 'Eppendorf Nexus A', '', '00077354'),
(1, 69, 'Thermal cycler', 'Eppendorf Nexus B', '', '00084094'),
(1, 70, 'Thermal cycler', 'MJ', '', '00031228'),
(1, 71, 'Thermal cycler', 'ABI 9700 (Pluto)', '', '00068391'),
(1, 72, 'Thermal cycler', 'ABI 9700 (Mars)', '', '00068390'),
(1, 73, 'Thermal cycler', 'ABI 9700 (Venus)', '', '00068412'),
(1, 128, 'Sanger Sequencer', 'ABI 3500xl (old)', '', '00069983'),
(1, 0, 'Sanger Sequencer', 'ABI 3500xl (new)', '', '');


CREATE TABLE IF NOT EXISTS instrument_used_table(
	instrument_used_id int NOT NULL AUTO_INCREMENT,
	
	-- foreign key
	user_id int,
	instrument_id int,
	ref_id int,

	-- data
	ref_table VARCHAR(50),
	
	time_stamp timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,

	PRIMARY KEY (instrument_used_id),
	UNIQUE KEY instrument_used_id (instrument_used_id)

) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1;


CREATE TABLE IF NOT EXISTS previous_positive_table(
	previous_positive_id int NOT NULL AUTO_INCREMENT,
	
	-- foreign key
	user_id int,
	ordered_test_id int,

	-- data
	history_overview varchar(20),
	history_ordered_test_id INT DEFAULT 0,

	history_mol_num VARCHAR(100),
	history_soft_path_num VARCHAR(100),
	history_test_tissue VARCHAR(100),

	history_date DATE, 
	
	time_stamp timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,

	PRIMARY KEY (previous_positive_id),
	UNIQUE KEY previous_positive_id (previous_positive_id)

) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1;

CREATE TABLE IF NOT EXISTS ordered_test_table(
	ordered_test_id int NOT NULL AUTO_INCREMENT,
	
	-- foreign key
	user_id int,
	orderable_tests_id int,
	previous_positive_id INT DEFAULT 0,
	visit_id INT DEFAULT 0,

	-- data
	soft_path_num varchar(100),
	outside_case_num varchar(100),

	consent_info VARCHAR(20),
	consent_date DATE,
	consent_for_research VARCHAR(3),

	start_date DATE, 
	finish_date DATE,
	num_of_days_to_perform_test INT DEFAULT 0,

	expected_turnaround_time INT,
	test_status VARCHAR(20),
	
	time_stamp timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,

	PRIMARY KEY (ordered_test_id),
	UNIQUE KEY ordered_test_id (ordered_test_id)

) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1;



CREATE TABLE IF NOT EXISTS test_turnaround_table(
	test_turnaround_id int NOT NULL AUTO_INCREMENT,
	
	-- foreign key
	user_id int,
	orderable_tests_id int,

	-- data
	dependent_result varchar(30) NOT NULL,
	status varchar(10) NOT NULL,
	turnaround_time INT,
	
	time_stamp timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,

	PRIMARY KEY (test_turnaround_id),
	UNIQUE KEY test_turnaround_id (test_turnaround_id)

) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1;


INSERT INTO test_turnaround_table (user_id,orderable_tests_id,turnaround_time,status,dependent_result) VALUES
('1','1','10','include','any'),
('1','2','10','include','any'),
('1','3','10','include','any'),
('1','4','30','include','any'),
('1','5','4','include','any'),
('1','6','5','include','any'),
('1','7','5','include','any'),
('1','8','5','include','any'),
('1','9','7','include','any'),
('1','10','7','include','any'),
('1','11','0','NA','NA'),
('1','12','0','include','any'),
('1','13','0','include','any'),
('1','14','9','include','any'),
('1','15','10','include','Negative'),
('1','16','5','include','Positive'),
('1','15','7','include','Positive'),
('1','16','10','include','Negative'),
('1','16','10','include','Negative'),
('1','17','7','include','Positive'),
('1','18','0','NA','NA'),
('1','19','8','include','any'),
('1','20','14','include','any'),
('1','21','14','include','any'),
('1','22','0','include','any'),
('1','23','7','include','any'),
('1','24','30','include','any');



CREATE TABLE IF NOT EXISTS ngs_panel_step_regulator_table(
	ngs_panel_step_regulator_id int NOT NULL AUTO_INCREMENT,
	
	-- foreign key
	user_id int,
	ngs_panel_id int,

	-- data
	step_regulator_name varchar(20) NOT NULL,
	status varchar(3) NOT NULL,

	
	time_stamp timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,

	PRIMARY KEY (ngs_panel_step_regulator_id),
	UNIQUE KEY ngs_panel_step_regulator_id (ngs_panel_step_regulator_id)

) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1;

INSERT INTO ngs_panel_step_regulator_table (user_id, ngs_panel_id, step_regulator_name, status) VALUES
(1,1,'hapmap','off'),
(1,3,'hapmap','off'),
(1,2,'hapmap','on');

INSERT INTO ngs_panel_step_regulator_table (user_id, ngs_panel_id, step_regulator_name, status) VALUES
(1,1,'strand_bias_calc','on'),
(1,3,'strand_bias_calc','off'),
(1,2,'strand_bias_calc','off');



CREATE TABLE IF NOT EXISTS hapmap_table(
	hapmap_id int NOT NULL AUTO_INCREMENT,
	
	-- foreign key
	user_id int,
	ngs_panel_id int,

	-- data
	gene varchar(20) NOT NULL,
	hapmap_sample varchar(20) NOT NULL,
	chr varchar(10) NOT NULL,
	pos INT,
	vaf INT, 
	
	time_stamp timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,

	PRIMARY KEY (hapmap_id),
	UNIQUE KEY hapmap_id (hapmap_id)

) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1;

CREATE TABLE IF NOT EXISTS orderable_tests_table(
	orderable_tests_id int NOT NULL AUTO_INCREMENT,
	
	-- foreign key
	user_id int,
	assay_type_id int,

	-- data
	test_name varchar(20) NOT NULL,
	full_test_name varchar(100) NOT NULL,
	test_type varchar(10) NOT NULL,
	consent_required varchar(3) NOT NULL,
	custom_fields_available varchar(3) NOT NULL, 
	notes_for_work_sheet TEXT NOT NULL,

	turnaround_time_depends_on_ordered_test varchar(3) DEFAULT 'no',

	previous_positive_required varchar(3),
	mol_num_required varchar(3),
	test_status varchar(10) DEFAULT 'Active',

	time_stamp timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,

	PRIMARY KEY (orderable_tests_id),
	UNIQUE KEY orderable_tests_id (orderable_tests_id)

) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1;

INSERT INTO orderable_tests_table (user_id,test_name,full_test_name,test_type,consent_required,mol_num_required,custom_fields_available,previous_positive_required,assay_type_id,notes_for_work_sheet) VALUES
('1','chimerism pre recipient','chimerism pre recipient','oncology','no','no','yes','no','3','');



INSERT INTO orderable_tests_table (user_id,test_name,full_test_name,test_type,consent_required,mol_num_required,custom_fields_available,previous_positive_required,assay_type_id,notes_for_work_sheet,turnaround_time) VALUES
('1','HFE','Hemochromatosis','genetic','yes','no','no','no','1','',10),
('1','FVL','Factor V Leiden','genetic','yes','no','no','no','2','',10),
('1','PTG','Prothrombin','genetic','yes','no','no','no','2','',10),
('1','chimerism pre','chimerism pre (donor or recpient)','oncology','no','no','yes','no','3','',30),
('1','chimerism post','chimerism post (donor)','oncology','no','no','yes','no','3','',4),
('1','TCRG','TCR gamma (T-Cell Receptor)','oncology','no','no','yes','no','4','',5),
('1','IGH','IGH B-Cell Clonality','oncology','no','no','no','yes','4','',5),
('1','IGK','IGK B-Cell Clonality','oncology','no','no','no','yes','4','',5),
('1','FLT3 ITD','FMS Like Tyrosine Kinase 3 ITD','oncology','no','no','no','yes','4','',7),
('1','FLT3 835','FMS Like Tyrosine Kinase 3 Codon 835','oncology','no','no','no','yes','4','',7),
('1','DNA Hold','Prepare DNA / Hold for Orders','oncology','no','no','no','no','5','',0),
('1','NGS Myeloid','Myeloid/CLL Mutation Panel','oncology','no','yes','no','no','6','',0),
('1','NGS Solid Tumor','Oncomine Focus Hotspot Assay','oncology','no','yes','no','no','6','',0),
('1','EGFR','Epidermal Growth Factor Receptor','oncology','no','no','no','no','7','Day 1 begings at completion of EGFR if ordered together',9),
('1','KRAS','Kirsten RAt Sarcoma','oncology','no','no','no','no','8','if neg reflex to BRAF',5),
('1','BRAF','v-raf murine sarcoma viral oncogene homolog B1','oncology','no','no','no','no','8','if neg send out extended panel Day 1 begings at completion of EGFR if ordered together',7),
('1','BRAF Reflex ','Reflex to BRAF after Normal KRAS and/or EGFR','oncology','no','no','no','no','8','',7),
('1','NRAS Reflex ','Reflex to NRAS after In-house Lung panel Normal','oncology','no','no','no','no','', '9',0),
('1','JAK2','Janus kinase 2 V617F','oncology','no','no','no','no','2','if neg reflex CALR','8'),
('1','CALR','Calreticulin','oncology','no','no','no','no','10','',14),
('1','CALR Reflex','Reflex to CALR after Normal JAK2','oncology','no','no','no','no','3','',14),
('1','TERT','Telomerase reverse trascriptase','oncology','no','no','no','no','7','',0),
('1','IDH1','Isocitrate Dehydrogenase 1','oncology','no','no','no','no','1','',7);

UPDATE orderable_tests_table SET turnaround_time_dependency_result = ""


CREATE TABLE IF NOT EXISTS assay_type_table(
	assay_type_id int NOT NULL AUTO_INCREMENT,
	
	-- foreign key
	user_id int,

	-- data
	assay_name varchar(40) NOT NULL,

	time_stamp timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,

	PRIMARY KEY (assay_type_id),
	UNIQUE KEY assay_type_id (assay_type_id)

) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1;

INSERT INTO assay_type_table (user_id, assay_name) VALUES
('1', 'Lightcycler (Capillary)'),
('1', 'Lightcycler (480)'),
('1', 'Fragment analysis'),
('1', 'gel'),
('1', 'extraction'),
('1', 'ngs'),
('1', 'Sanger Sequencing'),
('1', 'Lightcycler (Capillary)/Sanger Seq'),
('1', 'Send out'),
('1', 'Fragment analysis/Sanger Sequencing');


CREATE TABLE IF NOT EXISTS custom_field_table(
	custom_field_id int NOT NULL AUTO_INCREMENT,
	
	-- foreign key
	user_id int,
	ref_table_id int,

	-- data
	ref_table varchar(40) NOT NULL,
	field_type varchar(10) NOT NULL,
	field_name varchar(20) NOT NULL,

	time_stamp timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,

	PRIMARY KEY (custom_field_id),
	UNIQUE KEY custom_field_id (custom_field_id)

) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1;

INSERT INTO custom_field_table (user_id, ref_table_id, ref_table, field_type, field_name) VALUES
('1', '4', 'orderable_tests_table', 'radio', 'Patient Type');

INSERT INTO custom_field_table (user_id, ref_table_id, ref_table, field_type, field_name) VALUES
('1', '0', 'consent_required', 'radio', 'Consent type');

INSERT INTO custom_field_options_table (user_id, custom_field_id, option) VALUES
('1', '2', 'Received with sample'),
('1', '2', 'File with provider'),
('1', '2', 'Onbase check found'),
('1', '2', 'No Consent');


CREATE TABLE IF NOT EXISTS custom_field_options_table(
	custom_field_options_id int NOT NULL AUTO_INCREMENT,
	
	-- foreign key
	user_id int,
	custom_field_id int,

	-- data
	option varchar(40) NOT NULL,

	time_stamp timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,

	PRIMARY KEY (custom_field_options_id),
	UNIQUE KEY custom_field_options_id (custom_field_options_id)

) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1;

INSERT INTO custom_field_options_table (user_id, custom_field_id, option) VALUES
('1', '1', 'Donor'),
('1', '1', 'Recipient');

CREATE TABLE IF NOT EXISTS sample_log_book_table(
	sample_log_book_id int NOT NULL AUTO_INCREMENT,
	
	-- foreign key
	user_id int,
	possible_sample_types_id int DEFAULT 0,

	-- data
	first_name varchar(100) NOT NULL,
	last_name varchar(100) NOT NULL,
	soft_path_num varchar(100) NOT NULL,
	soft_lab_num varchar(100) NOT NULL,
	mol_num varchar(100) NOT NULL, # actually md#
	medical_record_num varchar(100) NOT NULL,

	test_tissue varchar(100) NOT NULL,

	date_received DATETIME,
	block varchar(100) NOT NULL,
	WBC_count int,
	h_and_e_circled varchar(3),

	tumor_cellularity int(3) unsigned,

	time_stamp timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,

	PRIMARY KEY (sample_log_book_id),
	UNIQUE KEY sample_log_book_id (sample_log_book_id)

) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1;

CREATE TABLE IF NOT EXISTS possible_sample_types_table(
	possible_sample_types_id int NOT NULL AUTO_INCREMENT,
	
	-- foreign key
	user_id int,

	-- data
	sample_type varchar(100) NOT NULL,
	
	time_stamp timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,

	PRIMARY KEY (possible_sample_types_id),
	UNIQUE KEY possible_sample_types_id (possible_sample_types_id)

) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1;

INSERT INTO possible_sample_types_table (user_id, sample_type) VALUES
('1', 'Fresh'),
('1', 'Paraffin Embedded curls in microcentrifuge tube'),
('1', 'Paraffin Embedded slides');

CREATE TABLE IF NOT EXISTS stock_dilution_table(
	stock_dilution_id int NOT NULL AUTO_INCREMENT,
	
	-- foreign key
	user_id int,
	visits_in_pool_id int,

	-- data
	dilution_total int,
  	dna_vol int,
  	ae_vol int,

	time_stamp timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,

	PRIMARY KEY (stock_dilution_id),
	UNIQUE KEY stock_dilution_id (stock_dilution_id)

) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1;

CREATE TABLE IF NOT EXISTS genes_covered_by_panel_table(
	genes_covered_by_panel_id int NOT NULL AUTO_INCREMENT,
	
	-- foreign key
	user_id int,
	ngs_panel_id int,

	-- data
	gene varchar(7),
  	exon varchar(17),
  	accession_num varchar(16),
  	date_gene_added DATE,

	time_stamp timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,

	PRIMARY KEY (genes_covered_by_panel_id),
	UNIQUE KEY genes_covered_by_panel_id (genes_covered_by_panel_id)

) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1;

INSERT INTO genes_covered_by_panel_table (user_id, ngs_panel_id, gene, exon, accession_num, date_gene_added) VALUES
('1', '2', 'ASXL1', '12', 'NM_015338.5', '2017-09-22'),
('1', '2', 'BCOR', 'all', 'NM_017745.5', '2017-09-22'),
('1', '2', 'BRAF', '15', 'NM_004333.4', '2017-09-22'),
('1', '2', 'CSF3R', '14-17', 'NM_156039.3', '2017-09-22'),
('1', '2', 'DNMT3a', 'all', 'NM_022552.4', '2017-09-22'),
('1', '2', 'CBL', '8, 9', 'NM_005188.3', '2019-03-18'),
('1', '2', 'FBXW7', '9-11', 'NM_033632.3', '2019-03-18'),
('1', '2', 'GATA1', '2', 'NM_002049.3', '2019-03-18'),
('1', '2', 'ETV6', 'all', 'NM_001987.4', '2017-09-22'),
('1', '2', 'EZH2', 'all', 'NM_004456.4', '2017-09-22'),
('1', '2', 'FLT3', '14, 15, 20', 'NM_004119.2', '2017-09-22'),
('1', '2', 'GATA2', '2-6', 'NM_032638.4', '2017-09-22'),
('1', '2', 'IDH1', '4', 'NM_005896.2', '2017-09-22'),
('1', '2', 'IDH2', '4', 'NM_002168.2', '2017-09-22'),
('1', '2', 'JAK2', '12, 14', 'NM_004972.3', '2017-09-22'),
('1', '2', 'KIT', '2, 8-11, 13, 17', 'NM_000222.2', '2017-09-22'),
('1', '2', 'KRAS', '2, 3', 'NM_033360.2', '2017-09-22'),
('1', '2', 'MPL', '10', 'NM_005373.2', '2017-09-22'),
('1', '2', 'MYD88', '3-5', 'NM_002468.4', '2017-09-22'),
('1', '2', 'NOTCH1', '26-28, 34', 'NM_017617.3', '2017-09-22'),
('1', '2', 'NPM1', '12', 'NM_002520.6', '2017-09-22'),
('1', '2', 'NRAS', '2, 3', 'NM_002524.4', '2017-09-22'),
('1', '2', 'PHF6', 'all', 'NM_032458.2', '2017-09-22'),
('1', '2', 'PTPN11', '3, 13', 'NM_002834.3', '2017-09-22'),
('1', '2', 'RUNX1', 'all', 'NM_001754.4', '2017-09-22'),
('1', '2', 'SETBP1', '4', 'NM_015559.2', '2017-09-22'),
('1', '2', 'SF3B1', '13-16', 'NM_012433.2', '2017-09-22'),
('1', '2', 'SRSF2', '1', 'NM_001195427.1', '2017-09-22'),
('1', '2', 'STAG2', 'all', 'NM_001042749.1', '2017-09-22'),
('1', '2', 'TET2', '3-11', 'NM_001127208.2', '2017-09-22'),
('1', '2', 'TP53', '2-11', 'NM_000546.5', '2017-09-22'),
('1', '2', 'U2AF1', '2, 6', 'NM_001025203.1', '2017-09-22'),
('1', '2', 'WT1', '7, 9', 'NM_024426.4', '2017-09-22'),
('1', '2', 'ZRSR2', 'all', 'NM_005089.3', '2017-09-22'),
('1', '2', 'BCOR', 'all', 'NM_001123385.1', '2019-03-22');


-- OFA
INSERT INTO genes_covered_by_panel_table (user_id, ngs_panel_id, gene, accession_num, exon, date_gene_added) VALUES
('1', '1', 'AKT1', 'NM_005163.2', '3', '2017-09-22'),
('1', '1', 'ALK', 'NM_004304.4', '21-25', '2017-09-22'),
('1', '1', 'AR', 'NM_000044.4', '6,8', '2017-09-22'),
('1', '1', 'BRAF', 'NM_004333.4', '11,15', '2017-09-22'),
('1', '1', 'CDK4', 'NM_000075.3', '2', '2017-09-22'),
('1', '1', 'CTNNB1', 'NM_001904.3', '3', '2017-09-22'),
('1', '1', 'DDR2', 'NM_001014796.2', '5', '2017-09-22'),
('1', '1', 'EGFR', 'NM_005228.4', '3,7,12,15,18-21', '2017-09-22'),
('1', '1', 'ERBB2', '', '8,17-22', '2017-09-22'),
('1', '1', 'ERBB3', 'NM_001982.3', '2,3,6,8,9', '2017-09-22'),
('1', '1', 'ERBB4', 'NM_005235', '18', '2017-09-22'),
('1', '1', 'ESR1', 'NM_000125.3', '8-10', '2017-09-22'),
('1', '1', 'FGFR2', '', '7-9,12,14', '2017-09-22'),
('1', '1', 'FGFR3', 'NM_000142.4', '7,9,14,16', '2017-09-22'),
('1', '1', 'GNAI1', '', '4,5', '2017-09-22'),
('1', '1', 'GNAQ', 'NM_002072.4', '4,5', '2017-09-22'),
('1', '1', 'HRAS', 'NM_005343.3', '2,3', '2017-09-22'),
('1', '1', 'IDH1', 'NM_005896.2', '4', '2017-09-22'),
('1', '1', 'IDH2', 'NM_002168.2', '4', '2017-09-22'),
('1', '1', 'JAK1', 'NM_002227.3', '14-16', '2017-09-22'),
('1', '1', 'JAK2', 'NM_004972.3', '14', '2017-09-22'),
('1', '1', 'JAK3', 'NM_000215.3', '11,12,15', '2017-09-22'),
('1', '1', 'KIT', 'NM_000222.2', '8,9,11,13,17', '2017-09-22'),
('1', '1', 'KRAS', '', '2-4', '2017-09-22'),
('1', '1', 'MAP2KI', 'NM_002755.3', '2,3,6', '2017-09-22'),
('1', '1', 'MAP2K2', 'NM_030662.3', '2', '2017-09-22'),
('1', '1', 'MET', 'NM_001127500.2', '14,16,19', '2017-09-22'),
('1', '1', 'MTOR', 'NM_004958.3', '30,39,40,43,47,53', '2017-09-22'),
('1', '1', 'NRAS', 'NM_002524.4', '2-4', '2017-09-22'),
('1', '1', 'PDGFRA', 'NM_006206.5', '12,14,18', '2017-09-22'),
('1', '1', 'PIK3CA', 'NM_006218.3', '2,5,6,8,10,14,19,21', '2017-09-22'),
('1', '1', 'RAF1', 'NM_002880.3', '7,12', '2017-09-22'),
('1', '1', 'RET', '', '10,11,13,15,16', '2017-09-22'),
('1', '1', 'ROS1', 'NM_002944.2', '36,38', '2017-09-22'),
('1', '1', 'SMO', 'NM_005631.4', '4,6,8,9', '2017-09-22');

-- archer
INSERT INTO genes_covered_by_panel_table (user_id, ngs_panel_id, gene, accession_num, exon, date_gene_added) VALUES
('1', '3', 'ABL1', 'NM_005157', '4-10', '2017-09-22'),
('1', '3', 'ANKRD26', 'NM_014915', '1(c.-113-c.-134)', '2017-09-22'),
('1', '3', 'ASXL1', 'NM_015338.5', '1-13', '2017-09-22'),
('1', '3', 'ASXL1', 'NM_001164603.1', '5', '2017-09-22'),
('1', '3', 'ATRX', 'NM_000489', '8-11,17-32', '2017-09-22'),
('1', '3', 'BCOR', 'NM_017745', '2-7,9-15', '2017-09-22'),
('1', '3', 'BCOR', 'NM_001123385', '8', '2017-09-22'),
('1', '3', 'BCORL1', 'NM_021946', '1-12', '2017-09-22'),
('1', '3', 'BRAF', 'NM_004333', '3,10-13,15', '2017-09-22'),
('1', '3', 'BTK', 'NM_000061', '15', '2017-09-22'),
('1', '3', 'CALR', 'NM_004343', '8,9', '2017-09-22'),
('1', '3', 'CBL', 'NM_005188', '2-5,7-9,16', '2017-09-22'),
('1', '3', 'CBLB', 'NM_170662', '3,9,10', '2017-09-22'),
('1', '3', 'CBLC', 'NM_012116', '9,10', '2017-09-22'),
('1', '3', 'CCND2', 'NM_001759', '5', '2017-09-22'),
('1', '3', 'CDKN2A', 'NM_058197', '1', '2017-09-22'),
('1', '3', 'CDKN2A', ' NM_058195', '1', '2017-09-22'),
('1', '3', 'CDKN2A', 'NM_000077', '2,3', '2017-09-22'),
('1', '3', 'CDKN2A', 'NM_001195132', '3', '2017-09-22'),
('1', '3', 'CEBPA', 'NM_004364', '1', '2017-09-22'),
('1', '3', 'CSF3R', 'NM_156039', '17', '2017-09-22'),
('1', '3', 'CSF3R', 'NM_172313', '10,18', '2017-09-22'),
('1', '3', 'CSF3R', 'NM_000760', '14-16', '2017-09-22'),
('1', '3', 'CUX1', 'NM_001202543', '15-24', '2017-09-22'),
('1', '3', 'CUX1', 'NM_001913', '1-23', '2017-09-22'),
('1', '3', 'CUX1', 'NM_181552', '1', '2017-09-22'),
('1', '3', 'CXCR4', 'NM_003467', '1,2', '2017-09-22'),
('1', '3', 'DCK', 'NM_000788', '2,3', '2017-09-22'),
('1', '3', 'DDX41', 'NM_016222', '1-17', '2017-09-22'),
('1', '3', 'DHX15', 'NM_001358', '3', '2017-09-22'),
('1', '3', 'DNMT3A', 'NM_001358', '2,3,5-23', '2017-09-22'),
('1', '3', 'DNMT3A', 'NM_153759', '1,2', '2017-09-22'),
('1', '3', 'DNMT3A', 'NM_175630', '4', '2017-09-22'),
('1', '3', 'ETNK1', 'NM_018638', '3', '2017-09-22'),
('1', '3', 'ETV6', 'NM_001987', '1-8', '2017-09-22'),
('1', '3', 'EZH2', 'NM_004456', '2-20', '2017-09-22'),
('1', '3', 'FBXW7', 'NM_018315', '1-11', '2017-09-22'),
('1', '3', 'FLT3', 'NM_004119', '8-21', '2017-09-22'),
('1', '3', 'GATA1', 'NM_002049', '2', '2017-09-22'),
('1', '3', 'GATA2', 'NM_032638', '2-6', '2017-09-22'),
('1', '3', 'GNAS', 'NM_000516', '8-11', '2017-09-22'),
('1', '3', 'HRAS', 'NM_005343', '2-4', '2017-09-22'),
('1', '3', 'IDH1', 'NM_005896', '3,4', '2017-09-22'),
('1', '3', 'IDH2', 'NM_002168', '4,6', '2017-09-22'),
('1', '3', 'IKZF1', 'NM_001220769', '5', '2017-09-22'),
('1', '3', 'IKZF1', 'NM_001220767', '2-5,7', '2017-09-22'),
('1', '3', 'IKZF1', 'NM_001220771', '4', '2017-09-22'),
('1', '3', 'IKZF1', 'NM_001291845', '4', '2017-09-22'),
('1', '3', 'IKZF1', 'NM_001291847', '5', '2017-09-22'),
('1', '3', 'JAK2', 'NM_004972', '12-16,19-25', '2017-09-22'),
('1', '3', 'JAK3', 'NM_000215', '3,11,13,15,18,19', '2017-09-22'),
('1', '3', 'KDM6A', 'NM_021140', '1-29', '2017-09-22'),
('1', '3', 'KDM6A', 'NM_001291415', '14', '2017-09-22'),
('1', '3', 'KIT', 'NM_000222', '1,2,5,8-15,17,18', '2017-09-22'),
('1', '3', 'KMT2A', 'NM_005933', '1-13,15-36', '2017-09-22'),
('1', '3', 'KMT2A', 'NM_001197104', '14', '2017-09-22'),
('1', '3', 'KRAS', 'NM_004985', '2-4', '2017-09-22'),
('1', '3', 'LUC7L2', 'NM_016019', '1-10', '2017-09-22'),
('1', '3', 'LUC7L2', 'NM_001244585', '2', '2017-09-22'),
('1', '3', 'MAP2K1', 'NM_002755', '2,3', '2017-09-22'),
('1', '3', 'MPL', 'NM_005373', '10,12', '2017-09-22'),
('1', '3', 'MYC', 'NM_002467', '1-3', '2017-09-22'),
('1', '3', 'MYD88', 'NM_002468.4', '4,5', '2017-09-22'),
('1', '3', 'MYD88', 'NM_001172567', '3', '2017-09-22'),
('1', '3', 'NF1', 'NM_000267', '1-14,16-57', '2017-09-22'),
('1', '3', 'NF1', 'NM_001128147', '15', '2017-09-22'),
('1', '3', 'NF1', 'NM_001042492', '31', '2017-09-22'),
('1', '3', 'NOTCH1', 'NM_017617', '16-28', '2017-09-22'),
('1', '3', 'NOTCH1', 'NM_017617', 'c.*370-c.*380', '2017-09-22'),
('1', '3', 'NPM1', 'NM_002520', '11', '2017-09-22'),
('1', '3', 'NRAS', 'NM_002524', '2-5', '2017-09-22'),
('1', '3', 'PDGFRA', 'NM_006206', '12,14,15,18', '2017-09-22'),
('1', '3', 'PHF6', 'NM_032335', '2-8', '2017-09-22'),
('1', '3', 'PHF6', 'NM_001015877', '10', '2017-09-22'),
('1', '3', 'PHF6', 'NM_032458', '9', '2017-09-22'),
('1', '3', 'PPM1D', 'NM_003620', '6', '2017-09-22'),
('1', '3', 'PTEN', 'NM_000314', '1-9 ', '2017-09-22'),
('1', '3', 'PTPN11', 'NM_002834', '3,4,7,8,12,13', '2017-09-22'),
('1', '3', 'PTPN11', 'NM_080601', '11', '2017-09-22'),
('1', '3', 'RAD21', 'NM_006265', '2-14', '2017-09-22'),
('1', '3', 'RBBP6', 'NM_006910', 'p.1444,p.1451', '2017-09-22'),
('1', '3', 'RBBP6', 'NM_006910', 'p.1569,1654,1673', '2017-09-22'),
('1', '3', 'RUNX1', 'NM_001754', '2,3,5-9', '2017-09-22'),
('1', '3', 'RUNX1', 'NM_001122607', '1,5', '2017-09-22'),
('1', '3', 'SETBP1', 'NM_015559', '4(p.799-p.950)', '2017-09-22'),
('1', '3', 'SF3B1', 'NM_012433', '13-21', '2017-09-22'),
('1', '3', 'SH2B3', 'NM_005475', '2-8', '2017-09-22'),
('1', '3', 'SLC29A1', 'NM_001078175', '4,13', '2017-09-22'),
('1', '3', 'SMC1A', 'NM_006306', '1-25', '2017-09-22'),
('1', '3', 'SMC1A', 'NM_001281463', '2', '2017-09-22'),
('1', '3', 'SMC3', 'NM_005445', '10,13,19,23,25,28', '2017-09-22'),
('1', '3', 'SRSF2', 'NM_003016', '1,2', '2017-09-22'),
('1', '3', 'STAG2', 'NM_006603', '2-33', '2017-09-22'),
('1', '3', 'STAG2', 'NM_001042749', '32', '2017-09-22'),
('1', '3', 'STAT3', 'NM_003150', '20', '2017-09-22'),
('1', '3', 'STAT3', 'NM_139276', '21', '2017-09-22'),
('1', '3', 'TET2', 'NM_001127208', '4-11', '2017-09-22'),
('1', '3', 'TET2', 'NM_017628', '3', '2017-09-22'),
('1', '3', 'TP53', 'NM_000546', '1-11', '2017-09-22'),
('1', '3', 'TP53', 'NM_001276696', '10', '2017-09-22'),
('1', '3', 'TP53', 'NM_001276695', '10', '2017-09-22'),
('1', '3', 'U2AF1', 'NM_006758', '2,6,7', '2017-09-22'),
('1', '3', 'WT1', 'NM_000378', '1-6,7,9', '2017-09-22'),
('1', '3', 'WT1', 'NM_001198552', '8', '2017-09-22'),
('1', '3', 'XPO1', 'NM_003400', '15,16,18', '2017-09-22'),
('1', '3', 'ZRSR2', 'NM_005089', '1-11', '2017-09-22');






CREATE TABLE IF NOT EXISTS ntc_table(
	ntc_id int NOT NULL AUTO_INCREMENT,
	
	-- foreign key
	user_id int,
	pool_chip_linker_id int,

	-- data
	barcode_ion_torrent int,
  	i7_index_miseq varchar(10),
  	i5_index_miseq varchar(10),

	time_stamp timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,

	PRIMARY KEY (ntc_id),
	UNIQUE KEY ntc_id (ntc_id)

) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1;


CREATE TABLE IF NOT EXISTS pool_chip_linker_table(
	pool_chip_linker_id int NOT NULL AUTO_INCREMENT,
	
	-- foreign key
	user_id int,
	library_pool_id_A int,
	library_pool_id_B int DEFAULT 0,
	ngs_panel_id int,

	-- data
	status varchar(30) DEFAULT 'pending',
  	library_pool_name varchar(50),
	library_prep_date DATE DEFAULT NULL,
	ngs_run_date DATE DEFAULT NULL,

	time_stamp timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,

	PRIMARY KEY (pool_chip_linker_id),
	UNIQUE KEY pool_chip_linker_id (pool_chip_linker_id)

) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1;

CREATE TABLE IF NOT EXISTS library_pool_table(
	library_pool_id int NOT NULL AUTO_INCREMENT,
	
	-- foreign key
	user_id int,

	-- data
	chip_flow_cell varchar(20),

  	
	time_stamp timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,

	PRIMARY KEY (library_pool_id),
	UNIQUE KEY library_pool_id (library_pool_id)

) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1;


CREATE TABLE IF NOT EXISTS used_reagents_table(
	used_reagents_id int NOT NULL AUTO_INCREMENT,
	
	-- foreign key
	user_id int,
	ngs_panel_id int,
	reagent_list_id int,

	-- data
	lot varchar(30),
  	expiration DATE,
  	
	time_stamp timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,

	PRIMARY KEY (used_reagents_id),
	UNIQUE KEY used_reagents_id (used_reagents_id)

) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1;


ALTER TABLE reagent_list_table ADD start int DEFAULT 0;
ALTER TABLE reagent_list_table ADD stop int DEFAULT 0;

CREATE TABLE IF NOT EXISTS reagent_list_table(
	reagent_list_id int NOT NULL AUTO_INCREMENT,
	
	-- foreign key
	user_id int,
	ngs_panel_id int,

	-- data
	reagent_name varchar(30),
  	purpose varchar(20),
  	
	time_stamp timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,

	PRIMARY KEY (reagent_list_id),
	UNIQUE KEY reagent_list_id (reagent_list_id)

) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1;

INSERT INTO reagent_list_table (user_id, ngs_panel_id, reagent_name, purpose) VALUES
('1', '1', 'IonXpress 1-16', 'barcode'),
('1', '1', 'IonXpress 17-32', 'barcode'),
('1', '1', 'IonXpress 33-48', 'barcode'),
('1', '1', 'IonXpress 49-64', 'barcode'),
('1', '1', 'IonXpress 65-80', 'barcode'),
('1', '1', 'IonXpress 81-96', 'barcode');




CREATE TABLE IF NOT EXISTS visits_in_pool_table(
	visits_in_pool_id int NOT NULL AUTO_INCREMENT,
	
	-- foreign key
	user_id int,
	library_pool_id int,
	visit_id int,

	-- data
	barcode_ion_torrent int,
  	i7_index_miseq varchar(10),
  	i5_index_miseq varchar(10),
  	tube_well varchar(10),
  	order_num int,

	time_stamp timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,

	PRIMARY KEY (visits_in_pool_id),
	UNIQUE KEY visits_in_pool_id (visits_in_pool_id)

) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1;

CREATE TABLE IF NOT EXISTS control_type_table(
	control_type_id int NOT NULL AUTO_INCREMENT,
	
	-- foreign key
	user_id int,

	-- data
	type varchar(20),
  	control_name varchar(40),
  	catalog_number varchar(40),
	description TEXT NOT NULL,
	dummy_last_name varchar(100) NOT NULL,
	dummy_first_name varchar(100) NOT NULL,
	dummy_sex varchar(1) NOT NULL,
	dummy_dob date NOT NULL,
	dummy_medical_record_num varchar(100) NOT NULL,
	time_stamp timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,

	PRIMARY KEY (control_type_id),
	UNIQUE KEY control_type_id (control_type_id)

) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1;

INSERT INTO control_type_table (user_id, type, control_name, catalog_number, description, dummy_last_name, dummy_first_name, dummy_sex, dummy_dob, dummy_medical_record_num)
SELECT user_id, type, 'SENSD', 'SENSD', description, 'SENSD', 'SENSD', dummy_sex, dummy_dob, 'SENSD'
FROM control_type_table WHERE control_type_id = 6;


INSERT INTO expected_control_variants_table (user_id, control_type_id, genes, coding, amino_acid_change, target_frequency_range, min_accepted_frequency, max_accepted_frequency)
SELECT user_id, 7, genes, coding, amino_acid_change, target_frequency_range, min_accepted_frequency, max_accepted_frequency
FROM expected_control_variants_table WHERE control_type_id = 6;

INSERT INTO control_type_table (user_id, type, control_name, catalog_number, description, dummy_last_name, dummy_first_name, dummy_sex, dummy_dob, dummy_medical_record_num) VALUES
('1', 'positive', 'AcroMetrix', '969056', 'The Thermo Fisher Scientific AcroMetrix Oncology Hotspot Control is used for both a positive and sensitivity control to assess day-to-day test variation of somatic mutation panels. The control contains 555 mutations however, there are only 119 in Hotspot regions for the OFA.', 'AcroMetrix', 'AcroMetrix', '', '1901-01-01', '969056'),
('1', 'negative', 'NA12877', 'NA12877', 'Peripheral vein blood B-lymphocyte Negative Control from a Caucasian Mormon Male from Utah.', 'NA12877', 'NA12877', 'M', '1901-01-01', 'NA12877'),
('1', 'negative', 'NA12878', 'NA12878', 'Peripheral vein blood B-lymphocyte Negative Control from a Caucasian Mormon Female from Utah. Donor subject has a single bp (G-to-A) transition at nucleotide 681 in exon 5 of the CYP2C19 gene (CYP2C19*2) which creates an aberrant splice site. The change altered the reading frame of the mRNA starting with amino acid 215 and produced a premature stop codon 20 amino acids downstream, resulting in a truncated, nonfunctional protein. Because of the aberrant splice site, a 40-bp deletion occurred at the beginning of exon 5 (from bp 643 to bp 682), resulting in deletion of amino acids 215 to 227. The truncated protein had 234 amino acids and would be catalytically inactive because it lacked the heme-binding region.', 'NA12878', 'NA12878', 'F', '1901-01-01', 'NA12878'),
('1', 'negative', 'NA19240', 'NA19240', 'Peripheral vein blood B-lymphocyte Negative Control from a Yoruba Nigerian Female.', 'NA19240', 'NA19240', 'F', '1901-01-01', 'NA19240');

INSERT INTO control_type_table (user_id, type, control_name, catalog_number, description, dummy_last_name, dummy_first_name, dummy_sex, dummy_dob, dummy_medical_record_num) VALUES
('1', 'positive', 'SENSC', 'SENSC', 'Unknown', 'SENSC', 'SENSC', '', '1901-01-01', 'SENSC');

INSERT INTO control_type_table (user_id, type, control_name, catalog_number, description, dummy_last_name, dummy_first_name, dummy_sex, dummy_dob, dummy_medical_record_num) VALUES
('1', 'positive', 'Test_AcroMetrix', 'Test_AcroMetrix', 'Subset of The Thermo Fisher Scientific AcroMetrix Oncology Hotspot Control for testing.', 'Test_AcroMetrix', 'Test_AcroMetrix', '', '1901-01-01', 'Test_AcroMetrix');

ALTER TABLE visit_table ADD COLUMN control_type_id int DEFAULT 0; 
ALTER TABLE visit_table ADD COLUMN lot varchar(40);
ALTER TABLE visit_table ADD COLUMN expiration_date DATE DEFAULT NULL;
ALTER TABLE visit_table ADD COLUMN version varchar(40);

CREATE TABLE IF NOT EXISTS expected_control_variants_table(
	expected_control_variants_id int NOT NULL AUTO_INCREMENT,
	
	-- foreign key
	user_id int,
	control_type_id int,

	-- data
	genes varchar(30),
	coding varchar(255) NOT NULL,
	amino_acid_change varchar(255) NOT NULL,
	min_accepted_frequency varchar(20),
	max_accepted_frequency varchar(20),
	target_frequency_range varchar(20),
	time_stamp timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,

	PRIMARY KEY (expected_control_variants_id),
	UNIQUE KEY expected_control_variants_id (expected_control_variants_id)

) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1;

INSERT INTO expected_control_variants_table (user_id, control_type_id, genes, coding, amino_acid_change, target_frequency_range, min_accepted_frequency, max_accepted_frequency) VALUES
('1', '5', 'AKT1', 'c.49G>A', 'p.Glu17Lys', '5-15%', '5', '40'),
('1', '5', 'ALK', 'c.3522C>A', 'p.Phe1174Leu', '5-15%', '5', '40'),
('1', '5', 'ALK', 'c.3824G>A', 'p.Arg1275Gln', '5-15%', '5', '40'),
('1', '5', 'BRAF', 'c.1391G>T', 'p.Gly464Val', '15-35%', '5', '50');

INSERT INTO expected_control_variants_table (user_id, control_type_id, genes, coding, amino_acid_change, min_accepted_frequency, max_accepted_frequency) VALUES
('1', '4', 'ALK', 'c.4381A>G', 'p.Ile1461Val', '0', '100');


INSERT INTO expected_control_variants_table (user_id, control_type_id, genes, coding, amino_acid_change, target_frequency_range, min_accepted_frequency, max_accepted_frequency) VALUES
('1', '1', 'AKT1', 'c.49G>A', 'p.Glu17Lys', '5-15%', '5', '40'),
('1', '1', 'ALK', 'c.3522C>A', 'p.Phe1174Leu', '5-15%', '5', '40'),
('1', '1', 'ALK', 'c.3824G>A', 'p.Arg1275Gln', '5-15%', '5', '40'),
('1', '1', 'BRAF', 'c.1391G>T', 'p.Gly464Val', '15-35%', '5', '50'),
('1', '1', 'BRAF', 'c.1742A>G', 'p.Asn581Ser', '15-35%', '5', '50'),
('1', '1', 'BRAF', 'c.1781A>G', 'p.Asp594Gly', '15-35%', '5', '50'),
('1', '1', 'BRAF', 'c.1790T>G', 'p.Leu597Arg', '15-35%', '5', '50'),
('1', '1', 'BRAF', 'c.1799T>A', 'p.Val600Glu', '15-35%', '5', '50'),
('1', '1', 'CTNNB1', 'c.110C>T', 'p.Ser37Phe', '5-15%', '5', '40'),
('1', '1', 'CTNNB1', 'c.121A>G', 'p.Thr41Ala', '5-15%', '5', '40'),
('1', '1', 'CTNNB1', 'c.134C>T', 'p.Ser45Phe', '5-15%', '5', '40'),
('1', '1', 'CTNNB1', 'c.98C>G', 'p.Ser33Cys', '5-15%', '5', '40'),
('1', '1', 'EGFR', 'c.1793G>T', 'p.Gly598Val', '15-35%', '5', '50'),
('1', '1', 'EGFR', 'c.2092G>A', 'p.Ala698Thr', '15-35%', '5', '50'),
('1', '1', 'EGFR', 'c.2156G>C', 'p.Gly719Ala', '15-35%', '5', '50'),
('1', '1', 'EGFR', 'c.2170G>A', 'p.Gly724Ser', '15-35%', '5', '50'),
('1', '1', 'EGFR', 'c.2197C>T', 'p.Pro733Ser', '15-35%', '5', '50'),
('1', '1', 'EGFR', 'c.2203G>A', 'p.Gly735Ser', '15-35%', '5', '50'),
('1', '1', 'EGFR', 'c.2222C>T', 'p.Pro741Leu', '15-35%', '5', '50'),
('1', '1', 'EGFR', 'c.2235_2249delGGAATTAAGAGAAGC', 'p.Glu746_Ala750del', '15-35%', '5', '50'),
('1', '1', 'EGFR', 'c.2293G>A', 'p.Val765Met', '15-35%', '5', '50'),
('1', '1', 'EGFR', 'c.2375T>C', 'p.Leu792Pro', '15-35%', '5', '50'),
('1', '1', 'EGFR', 'c.2485G>A', 'p.Glu829Lys', '15-35%', '5', '50'),
('1', '1', 'EGFR', 'c.2497T>G', 'p.Leu833Val', '15-35%', '5', '50'),
('1', '1', 'EGFR', 'c.2504A>T', 'p.His835Leu', '15-35%', '5', '50'),
('1', '1', 'EGFR', 'c.2515G>A', 'p.Ala839Thr', '15-35%', '5', '50'),
('1', '1', 'EGFR', 'c.2573T>G', 'p.Leu858Arg', '15-35%', '5', '50'),
('1', '1', 'EGFR', 'c.2582T>A', 'p.Leu861Gln', '15-35%', '5', '50'),
('1', '1', 'EGFR', 'c.2588G>A', 'p.Gly863Asp', '15-35%', '5', '50'),
('1', '1', 'EGFR', 'c.323G>A', 'p.Arg108Lys', '15-35%', '5', '50'),
('1', '1', 'EGFR', 'c.340G>A', 'p.Glu114Lys', '15-35%', '5', '50'),
('1', '1', 'EGFR', 'c.866C>T', 'p.Ala289Val', '15-35%', '5', '50'),
('1', '1', 'EGFR', 'c.874G>T', 'p.Val292Leu', '15-35%', '5', '50'),
('1', '1', 'ERBB2', 'c.2264T>C', 'p.Leu755Ser', '5-15%', '5', '40'),
('1', '1', 'ERBB2', 'c.2305G>T', 'p.Asp769Tyr', '5-15%', '5', '40'),
('1', '1', 'ERBB2', 'c.2324_2325insATACGTGATGGC', 'p.Glu770_Ala771insAlaTyrValMet', '5-15%', '5', '40'),
('1', '1', 'ERBB2', 'c.2524G>A', 'p.Val842Ile', '5-15%', '5', '40'),
('1', '1', 'ERBB2', 'c.2570A>G', 'p.Asn857Ser', '5-15%', '5', '40'),
('1', '1', 'ERBB2', 'c.2632C>T', 'p.His878Tyr', '5-15%', '5', '40'),
('1', '1', 'FGFR2', 'c.1108A>G', 'p.Thr370Ala', '5-15%', '5', '40'),
('1', '1', 'FGFR2', 'c.1124A>G', 'p.Tyr375Cys', '5-15%', '5', '40'),
('1', '1', 'FGFR2', 'c.1144T>C', 'p.Cys382Arg', '5-15%', '5', '40'),
('1', '1', 'FGFR2', 'c.1647T>A', 'p.Asn549Lys', '5-15%', '5', '40'),
('1', '1', 'FGFR2', 'c.755C>G', 'p.Ser252Trp', '5-15%', '5', '40'),
('1', '1', 'FGFR3', 'c.1108G>T', 'p.Gly370Cys', '5-15%', '5', '40'),
('1', '1', 'FGFR3', 'c.1138G>A', 'p.Gly380Arg', '5-15%', '5', '40'),
('1', '1', 'FGFR3', 'c.1150T>C', 'p.Phe384Leu', '5-15%', '5', '40'),
('1', '1', 'FGFR3', 'c.1172C>A', 'p.Ala391Glu', '5-15%', '5', '40'),
('1', '1', 'FGFR3', 'c.1928A>G', 'p.His643Arg', '5-15%', '5', '40'),
('1', '1', 'FGFR3', 'c.1948A>G', 'p.Lys650Glu', '5-15%', '5', '40'),
('1', '1', 'FGFR3', 'c.2089G>T', 'p.Gly697Cys', '5-15%', '5', '40'),
('1', '1', 'FGFR3', 'c.746C>G', 'p.Ser249Cys', '5-15%', '5', '40'),
('1', '1', 'GNA11', 'c.547C>T', 'p.Arg183Cys', '5-15%', '5', '40'),
('1', '1', 'GNA11', 'c.626A>T', 'p.Gln209Leu', '5-15%', '5', '40'),
('1', '1', 'GNAQ', 'c.523A>T', 'p.Thr175Ser', '15-35%', '5', '50'),
('1', '1', 'GNAQ', 'c.548G>A', 'p.Arg183Gln', '15-35%', '5', '50'),
('1', '1', 'GNAQ', 'c.680_682delTGTinsGCA', 'p.Met227_Phe228delinsSerIle', '15-35%', '5', '50'),
('1', '1', 'HRAS', 'c.175G>A', 'p.Ala59Thr', '5-15%', '5', '40'),
('1', '1', 'HRAS', 'c.182A>G', 'p.Gln61Arg', '5-15%', '5', '40'),
('1', '1', 'HRAS', 'c.35G>T', 'p.Gly12Val', '5-15%', '5', '40'),
('1', '1', 'IDH1', 'c.367G>A', 'p.Gly123Arg', '5-15%', '5', '40'),
('1', '1', 'IDH1', 'c.388A>G', 'p.Ile130Val', '5-15%', '5', '40'),
('1', '1', 'IDH1', 'c.395G>A', 'p.Arg132His', '5-15%', '5', '40'),
('1', '1', 'IDH2', 'c.419G>A', 'p.Arg140Gln', '5-15%', '5', '40'),
('1', '1', 'IDH2', 'c.515G>A', 'p.Arg172Lys', '5-15%', '5', '40'),
('1', '1', 'JAK2', 'c.1849G>T', 'p.Val617Phe', '15-35%', '5', '50'),
('1', '1', 'JAK2', 'c.1860C>A', 'p.Asp620Glu', '15-35%', '5', '50'),
('1', '1', 'KIT', 'c.1509_1510insGCCTAT', 'p.Ser501_Ala502insAlaTyr', '5-15%', '5', '40'),
('1', '1', 'KIT', 'c.1516T>C', 'p.Phe506Leu', '5-15%', '5', '40'),
('1', '1', 'KIT', 'c.1526A>T', 'p.Lys509Ile', '5-15%', '5', '40'),
('1', '1', 'KIT', 'c.1535A>G', 'p.Asn512Ser', '5-15%', '5', '40'),
('1', '1', 'KIT', 'c.1588G>A', 'p.Val530Ile', '5-15%', '5', '40'),
('1', '1', 'KIT', 'c.1924A>G', 'p.Lys642Glu', '5-15%', '5', '40'),
('1', '1', 'KIT', 'c.1961T>C', 'p.Val654Ala', '5-15%', '5', '40'),
('1', '1', 'KIT', 'c.2410C>T', 'p.Arg804Trp', '5-15%', '5', '40'),
('1', '1', 'KRAS', 'c.104C>T', 'p.Thr35Ile', '5-15%', '5', '40'),
('1', '1', 'KRAS', 'c.175G>A', 'p.Ala59Thr', '5-15%', '5', '40'),
('1', '1', 'KRAS', 'c.183A>C', 'p.Gln61His', '5-15%', '5', '40'),
('1', '1', 'KRAS', 'c.351A>C', 'p.Lys117Asn', '5-15%', '5', '40'),
('1', '1', 'KRAS', 'c.35G>A', 'p.Gly12Asp', '5-15%', '5', '40'),
('1', '1', 'MAP2K1', 'c.171G>T', 'p.Lys57Asn', '5-15%', '5', '40'),
('1', '1', 'MAP2K1', 'c.199G>A', 'p.Asp67Asn', '5-15%', '5', '40'),
('1', '1', 'MET', 'c.3370C>G', 'p.His1124Asp', '15-35%', '5', '50'),
('1', '1', 'MET', 'c.3757T>G', 'p.Tyr1253Asp', '15-35%', '5', '50'),
('1', '1', 'MET', 'c.3778G>T', 'p.Gly1260Cys', '15-35%', '5', '50'),
('1', '1', 'MET', 'c.3785A>G', 'p.Lys1262Arg', '15-35%', '5', '50'),
('1', '1', 'MET', 'c.3803T>C', 'p.Met1268Thr', '15-35%', '5', '50'),
('1', '1', 'NRAS', 'c.182A>G', 'p.Gln61Arg', '5-15%', '5', '40'),
('1', '1', 'NRAS', 'c.29G>A', 'p.Gly10Glu', '5-15%', '5', '40'),
('1', '1', 'NRAS', 'c.35G>A', 'p.Gly12Asp', '5-15%', '5', '40'),
('1', '1', 'NRAS', 'c.52G>A', 'p.Ala18Thr', '5-15%', '5', '40'),
('1', '1', 'PDGFRA', 'c.1977C>A', 'p.Asn659Lys', '5-15%', '5', '40'),
('1', '1', 'PDGFRA', 'c.2525A>T', 'p.Asp842Val', '5-15%', '5', '40'),
('1', '1', 'PDGFRA', 'c.2544C>A', 'p.Asn848Lys', '5-15%', '5', '40'),
('1', '1', 'PIK3CA', 'c.1035T>A', 'p.Asn345Lys', '5-15%', '5', '40'),
('1', '1', 'PIK3CA', 'c.1258T>C', 'p.Cys420Arg', '5-15%', '5', '40'),
('1', '1', 'PIK3CA', 'c.1370A>G', 'p.Asn457Ser', '5-15%', '5', '40'),
('1', '1', 'PIK3CA', 'c.1616C>G', 'p.Pro539Arg', '5-15%', '5', '40'),
('1', '1', 'PIK3CA', 'c.1624G>A', 'p.Glu542Lys', '5-15%', '5', '40'),
('1', '1', 'PIK3CA', 'c.1633G>A', 'p.Glu545Lys', '5-15%', '5', '40'),
('1', '1', 'PIK3CA', 'c.1640A>G', 'p.Glu547Gly', '5-15%', '5', '40'),
('1', '1', 'PIK3CA', 'c.2102A>C', 'p.His701Pro', '5-15%', '5', '40'),
('1', '1', 'PIK3CA', 'c.2702G>T', 'p.Cys901Phe', '5-15%', '5', '40'),
('1', '1', 'PIK3CA', 'c.2725T>C', 'p.Phe909Leu', '5-15%', '5', '40'),
('1', '1', 'PIK3CA', 'c.3110A>G', 'p.Glu1037Gly', '5-15%', '5', '40'),
('1', '1', 'PIK3CA', 'c.3140A>G', 'p.His1047Arg', '5-15%', '5', '40'),
('1', '1', 'PIK3CA', 'c.93A>G', 'p.Ile31Met', '5-15%', '5', '40'),
('1', '1', 'PIK3CA', 'c.971C>T', 'p.Thr324Ile', '5-15%', '5', '40'),
('1', '1', 'RET', 'c.1852T>C', 'p.Cys618Arg', '15-35%', '5', '50'),
('1', '1', 'RET', 'c.1858T>C', 'p.Cys620Arg', '15-35%', '5', '50'),
('1', '1', 'RET', 'c.1894_1906delGAGCTGTGCCGCAinsAGCT', 'p.Glu632_Thr636delinsSerSer', '15-35%', '5', '50'),
('1', '1', 'RET', 'c.1942G>A', 'p.Val648Ile', '15-35%', '5', '50'),
('1', '1', 'RET', 'c.2304G>C', 'p.Glu768Asp', '15-35%', '5', '50'),
('1', '1', 'RET', 'c.2647_2648delGCinsTT', 'p.Ala883Phe', '15-35%', '5', '50'),
('1', '1', 'RET', 'c.2701G>A', 'p.Glu901Lys', '15-35%', '5', '50'),
('1', '1', 'RET', 'c.2753T>C', 'p.Met918Thr', '15-35%', '5', '50'),
('1', '1', 'SMO', 'c.1234C>T', 'p.Leu412Phe', '15-35%', '5', '50'),
('1', '1', 'SMO', 'c.1604G>T', 'p.Trp535Leu', '15-35%', '5', '50');

CREATE TABLE IF NOT EXISTS observed_control_status_table(
	observed_control_status_id int NOT NULL AUTO_INCREMENT,
	
	-- foreign key
	user_id int,
	observed_variant_id int,
	expected_control_variants_id int,
	
	-- data
	status varchar(10),

	time_stamp timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,

	PRIMARY KEY (observed_control_status_id),
	UNIQUE KEY observed_control_status_id (observed_control_status_id)

) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1;




CREATE TABLE IF NOT EXISTS suggested_revision_table(
	suggested_revision_id int NOT NULL AUTO_INCREMENT,
	
	-- foreign key
	user_id int,

	-- data
	status varchar(10) DEFAULT 'active',
  	run_id int(11) DEFAULT NULL,
	suggested_revision TEXT NOT NULL,
	time_stamp timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,

	PRIMARY KEY (suggested_revision_id),
	UNIQUE KEY suggested_revision_id (suggested_revision_id)

) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1;


CREATE TABLE IF NOT EXISTS training_table(
	training_id int NOT NULL AUTO_INCREMENT,
	
	-- foreign key
	user_id int,

	-- data
	training_step varchar(20), 

	time_stamp timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,

	PRIMARY KEY (training_id),
	UNIQUE KEY training_id (training_id)

) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1;


CREATE TABLE IF NOT EXISTS low_coverage_status_table(
	low_coverage_status_id int NOT NULL AUTO_INCREMENT,
	
	-- foreign key
	user_id int(11) DEFAULT NULL,
	run_id int(11) DEFAULT NULL,

	-- data
	status varchar(20),

	time_stamp timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,

	PRIMARY KEY (low_coverage_status_id),
	UNIQUE KEY low_coverage_status_id (low_coverage_status_id)

) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1;

CREATE TABLE IF NOT EXISTS strand_bias_table(
	strand_bias_id int NOT NULL AUTO_INCREMENT,
	
	-- foreign key
	observed_variant_id int(11) DEFAULT NULL,
	user_id int(11) DEFAULT NULL,

	-- data
	ref_pos varchar(20),
	ref_neg varchar(20),
	variant_neg varchar(20),
	variant_pos varchar(20),
	igv_strain_bias varchar(20), 

	time_stamp timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,

	PRIMARY KEY (strand_bias_id),
	UNIQUE KEY strand_bias_id (strand_bias_id)

) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1;

CREATE TABLE IF NOT EXISTS revise_table(
	revise_id int NOT NULL AUTO_INCREMENT,
	
	-- foreign key
	user_id int(11) DEFAULT NULL,
	visit_id int(11) DEFAULT NULL,

	-- data
	change_summary TEXT NOT NULL,

	time_stamp timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,

	PRIMARY KEY (revise_id),
	UNIQUE KEY revise_id (revise_id)

) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1;
CREATE TABLE IF NOT EXISTS run_sheet_general_table(
	run_sheet_general_id int NOT NULL AUTO_INCREMENT,
	
	-- foreign key
	user_id int,

	-- data
	sheet_panel_type varchar(10),
	library_prep_date DATE NOT NULL,
	ngs_run_date DATE NOT NULL,

	time_stamp timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,

	PRIMARY KEY (run_sheet_general_id),
	UNIQUE KEY run_sheet_general_id (run_sheet_general_id)

) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1;

CREATE TABLE IF NOT EXISTS version_history_table(
	version_history_id int NOT NULL AUTO_INCREMENT,
	
	-- foreign key
	user_id int,

	-- data
	version_comment varchar(255),
	version_num_a int,
	version_num_b int,
	version_num_c int,
	
	time_stamp timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,

	PRIMARY KEY (version_history_id),
	UNIQUE KEY version_history_id (version_history_id)

) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1;


CREATE TABLE IF NOT EXISTS run_sheet_rows_table(
	run_sheet_row_id int NOT NULL AUTO_INCREMENT,
	
	-- foreign key
	run_sheet_general_id int,

	-- data
	ae_vol FLOAT,
	dil_total_vol int,
	dna_vol FLOAT,
	index_i5 varchar(20),
	index_i7 varchar(20),
	library_tube int,
	qubit int,
	sample_ids varchar(30),
	patient_name varchar(100),
	time_stamp timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,

	PRIMARY KEY (run_sheet_row_id),
	UNIQUE KEY run_sheet_row_id (run_sheet_row_id)

) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1;



CREATE INDEX idx_user_id
ON suggested_revision_table (user_id);
CREATE INDEX idx_user_id
ON reviewed_suggested_xref (user_id);
CREATE INDEX idx_suggested_suggested_revision_id
ON reviewed_suggested_xref (suggested_suggested_revision_id);
CREATE INDEX idx_reviewed_suggested_revision_id
ON reviewed_suggested_xref (reviewed_suggested_revision_id);

CREATE TABLE IF NOT EXISTS reviewed_suggested_xref(
	reviewed_suggested_xref_id int NOT NULL AUTO_INCREMENT,
	
	-- foreign key
	suggested_suggested_revision_id int(11) DEFAULT NULL,
  	reviewed_suggested_revision_id int(11) DEFAULT NULL,
  	user_id int(11) DEFAULT NULL,

	-- data
	time_stamp timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,

	PRIMARY KEY (reviewed_suggested_xref_id),
	UNIQUE KEY reviewed_suggested_xref_id (reviewed_suggested_xref_id)

) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Table structure for table patient
--

CREATE TABLE IF NOT EXISTS patient_table(
	patient_id int NOT NULL AUTO_INCREMENT,
	
	-- foreign key
	user_id int,

	-- data
	last_name varchar(100) NOT NULL,
	middle_name varchar(100) NOT NULL,
	first_name varchar(100) NOT NULL,
	sex varchar(1) NOT NULL,
	dob date NOT NULL,
	medical_record_num varchar(100) NOT NULL, 
	time_stamp timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,

	PRIMARY KEY (patient_id),
	UNIQUE KEY patient_id (patient_id)

) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS gene_update_table(
	gene_update_id int NOT NULL AUTO_INCREMENT,
	-- data
	gene varchar(30),
	time_stamp timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,

	PRIMARY KEY (gene_update_id),
	UNIQUE KEY gene_update_id (gene_update_id)

) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;


CREATE TABLE IF NOT EXISTS classification_table(
	classify_id int NOT NULL AUTO_INCREMENT,
	
	-- foreign key
	user_id int DEFAULT 0,

	-- data
	genes varchar(30) NOT NULL,
	coding varchar(255) NOT NULL,
	amino_acid_change varchar(255) NOT NULL,
	classification varchar(100) NOT NULL,
	panel varchar(50) NOT NULL,
	 
	time_stamp timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,

	PRIMARY KEY (classify_id),
	UNIQUE KEY classify_id (classify_id)

) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

CREATE TABLE user_run_task_xref (
  user_run_task_id int(11) NOT NULL AUTO_INCREMENT,
  user_id int(11) DEFAULT 0,
  run_id int(11) DEFAULT 0,
  task varchar(30) NOT NULL,
  time_stamp timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (user_run_task_id),
  UNIQUE KEY user_run_task_id (user_run_task_id),
  KEY idx_user_id (user_id),
  KEY idx_run_id (run_id)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;




CREATE TABLE IF NOT EXISTS low_coverage_table(
	low_coverage_id int NOT NULL AUTO_INCREMENT,
	
	-- foreign key
	run_id int,

	-- data
	gene varchar(30) NOT NULL,
	Amplicon varchar(50) NOT NULL,
	exon varchar(50) NOT NULL,
	codons varchar(50) NOT NULL,
	depth varchar(4) NOT NULL,
	 
	time_stamp timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,

	PRIMARY KEY (low_coverage_id),
	UNIQUE KEY low_coverage_id (low_coverage_id)

) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;
--
-- Table structure for table user_table
--

CREATE TABLE IF NOT EXISTS login_table (
	login_id int NOT NULL AUTO_INCREMENT,
	email_address varchar(40) NOT NULL,
	password varchar(60) NOT NULL,
	ip_address varchar(16),
	time_stamp timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	PRIMARY KEY (login_id),
	UNIQUE KEY login_id (login_id)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1;

CREATE TABLE IF NOT EXISTS top_passwords_used_table (
	top_password_id int NOT NULL AUTO_INCREMENT,
	email_address varchar(40) NOT NULL,
	password varchar(60) NOT NULL,
	time_stamp timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	PRIMARY KEY (top_password_id),
	UNIQUE KEY top_password_id (top_password_id)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1;

CREATE TABLE IF NOT EXISTS user_table (
	user_id int NOT NULL AUTO_INCREMENT,
	first_name varchar(50) NOT NULL,
	last_name varchar(50) NOT NULL,
	email_address varchar(40) NOT NULL,
	password varchar(60) NOT NULL,
	status varchar(8) NOT NULL,
	time_stamp timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	security_question tinyint(3) unsigned NOT NULL,
	security_answer varchar(32) DEFAULT NULL,
	security_question2 tinyint(3) unsigned DEFAULT NULL,
	security_answer2 varchar(32) DEFAULT NULL,
	PRIMARY KEY (user_id),
	UNIQUE KEY user_id (user_id),
	UNIQUE KEY email_address (email_address)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1;

CREATE TABLE IF NOT EXISTS permission_table (
	permission_id INT NOT NULL AUTO_INCREMENT,
	permission varchar(15),
	PRIMARY KEY (permission_id),
	UNIQUE KEY permission_id (permission_id)

) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1;;

CREATE TABLE IF NOT EXISTS permission_user_xref (
	permission_user_xref_id int NOT NULL AUTO_INCREMENT,
	permission_id INT ,
	user_id INT ,
	PRIMARY KEY (permission_user_xref_id),
	UNIQUE KEY permission_user_xref_id (permission_user_xref_id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1;
--
-- Table structure for table visit
--

CREATE TABLE IF NOT EXISTS visit_table (
	visit_id int NOT NULL AUTO_INCREMENT,

	-- Foreign keys
	run_id int NOT NULL,
	ngs_panel_id int NOT NULL, 
	user_id int NOT NULL,
	patient_id int,
	diagnosis_id int,

	-- data
	soft_path_num varchar(100) NOT NULL,
	soft_lab_num varchar(100) NOT NULL,
	mol_num varchar(100) NOT NULL,
	primary_tumor_site varchar(20) NOT NULL, -- lung, liver, etc
	test_tissue varchar(100) NOT NULL,
	req_physician varchar(100) NOT NULL,
	req_loc varchar(100) NOT NULL,
	collected DATETIME,
	block varchar(100) NOT NULL,
	tumor_cellularity int(3) unsigned,
	time_stamp timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,

	-- control info
	PRIMARY KEY (visit_id),
	UNIQUE KEY visit_id (visit_id)

) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1;

CREATE TABLE IF NOT EXISTS qc_variant_step_table (
	qc_variant_step_id int NOT NULL AUTO_INCREMENT,

	-- Foreign keys
	xref_id int NOT NULL,
	user_id int NOT NULL,

	-- data
	step varchar(30) NOT NULL,
	status varchar(50) NOT NULL,
	comment TEXT,
	type_xref varchar(30) NOT NULL,
	time_stamp timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	
	-- control info
	PRIMARY KEY (qc_variant_step_id),
	UNIQUE KEY qc_variant_step_id (qc_variant_step_id)

) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1;

CREATE TABLE IF NOT EXISTS pool_steps_table (
	pool_steps_id int NOT NULL AUTO_INCREMENT,

	-- Foreign keys
	pool_chip_linker_id int NOT NULL,
	user_id int NOT NULL,

	-- data
	step varchar(30) NOT NULL,
	status varchar(50) NOT NULL,

	time_stamp timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	
	-- control info
	PRIMARY KEY (pool_steps_id),
	UNIQUE KEY pool_steps_id (pool_steps_id)

) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1;


CREATE TABLE IF NOT EXISTS pre_step_visit (
	pre_step_visit_id int NOT NULL AUTO_INCREMENT,

	-- Foreign keys
	visit_id int NOT NULL,
	user_id int NOT NULL,
	comment_id int NOT NULL,

	-- data
	step varchar(30) NOT NULL,
	status varchar(50) NOT NULL,
	concentration varchar(50) NOT NULL,
	time_stamp timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	
	-- control info
	PRIMARY KEY (pre_step_visit_id),
	UNIQUE KEY pre_step_visit_id (pre_step_visit_id)

) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1;

CREATE TABLE IF NOT EXISTS diagnosis_table (
	diagnosis_id int NOT NULL AUTO_INCREMENT,

	-- data
	diagnosis varchar(20) DEFAULT 'unknown',

	-- control info
	PRIMARY KEY (diagnosis_id),
	UNIQUE KEY diagnosis_id (diagnosis_id)

) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1;

CREATE TABLE IF NOT EXISTS problem_pages_table (
	problem_id int NOT NULL AUTO_INCREMENT,

	-- foreign keys
	user_id int NOT NULL,
	
	-- data
	problem_page TEXT,

	time_stamp timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	-- control info
	PRIMARY KEY (problem_id),
	UNIQUE KEY problem_id (problem_id)

) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1;


--
-- Table structure for run_info_table
--
-- to add 
ALTER TABLE run_info_table MODIFY COLUMN sample_name varchar(100);
ALTER TABLE run_info_table ADD COLUMN filter_chain varchar(100);
ALTER TABLE run_info_table ADD COLUMN work_flow_name varchar(100);

CREATE TABLE IF NOT EXISTS run_info_table (
	run_id int NOT NULL AUTO_INCREMENT,

	-- data
	work_flow_name varchar(30) NOT NULL,
	sample_name varchar(20) NOT NULL, 
	run_date_chip varchar(10) NOT NULL,
	pipeline varchar(30) NOT NULL,
	reference varchar(10) NOT NULL,
	filter_chain varchar(30) NOT NULL,
	total_variant_count int(5) NOT NULL,
	filtered_out_count int(5) NOT NULL,
	status varchar(20) NOT NULL,
	summary TEXT,
	run_date DATE,

	-- control info
	time_stamp timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	PRIMARY KEY (run_id),
	UNIQUE KEY run_id (run_id)

) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1;

--
-- Table structure for report_table
--

CREATE TABLE IF NOT EXISTS report_table (
	report_id int NOT NULL AUTO_INCREMENT,

	-- Foreign keys
	interp_id int,
	report_results_id int,
	user_id int NOT NULL, 

	-- data
	approval_status varchar(20) NOT NULL,

	-- control info
	time_stamp timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,

	PRIMARY KEY (report_id),
	UNIQUE KEY report_id (report_id)

) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1;

CREATE TABLE IF NOT EXISTS no_snv_summary_table (
	no_snv_summary_id int NOT NULL AUTO_INCREMENT,

	-- Foreign keys
	user_id int NOT NULL, 

	-- data
	summary TEXT,

	-- control info
	time_stamp timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,

	PRIMARY KEY (no_snv_summary_id),
	UNIQUE KEY no_snv_summary_id (no_snv_summary_id)

) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1;


--
-- Table structure for knowledge_base_table
--

CREATE TABLE IF NOT EXISTS knowledge_base_table (
	knowledge_id int NOT NULL AUTO_INCREMENT,

	-- Foreign keys
	
	-- data
	genes varchar(20) NOT NULL,
	coding varchar(255) NOT NULL,
	amino_acid_change varchar(255) NOT NULL,
	amino_acid_change_abbrev varchar(255) NOT NULL,
	chr varchar(10),
	loc varchar(255),	
	
	
	mutation_type varchar(100),

	pmid	TEXT,
	cosmic varchar(255),
	transcript varchar(30),	
	fathmm_prediction varchar(255),
	fathmm_score varchar(255),
	hgnc varchar(10),

	notes varchar(255),

	exon varchar(10),
	dbsnp varchar(255),
	pfam varchar(255),

	-- control info
	time_stamp timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,

	PRIMARY KEY (knowledge_id),
	UNIQUE KEY knowledge_id (knowledge_id)

) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1;

--
-- Table structure for comment_table
--

CREATE TABLE IF NOT EXISTS comment_table (
	comment_id int NOT NULL AUTO_INCREMENT,

	-- Foreign keys
	user_id int,
	comment_ref int,

	-- data
	comment_type varchar(30) NOT NULL,
	comment TEXT NOT NULL,

	-- control info
	time_stamp timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,

	PRIMARY KEY (comment_id),
	UNIQUE KEY comment_id (comment_id)

) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1;

-- INSERT INTO comment_table (user_id, comment_ref, comment_type, comment) VALUES
-- ('1', '1', 'observed_vafriant', 'heres a comment'),
-- ('1', '1', 'observed_variant', 'heres a another comment'),
-- ('1', '2', 'observed_variant', 'heres a comment'),
-- ('1', '2', 'observed_variant', 'heres a another comment');

-- INSERT INTO comment_table (user_id, comment_ref, comment_type, comment, time_stamp) VALUES
-- ('1', '40443', 'knowledge_base_table', 'APC mutations are common in colorectal cancers, but are also identified in a small population of lung adenocarcinomas as well (Ding, L., et.al. Nature 455, 1069-1075 (23 October 2008).  We have seen this particular mutation twice before at Emory, and both times the possibility of a germline variant was raised. Although the allele frequency here suggests a somatic variation, given our clinical experience with allele variability there is still a possibility this represents a germline variation. We could test patient peripheral blood at your request to confirm this suspicion. If confirmed, however, whether or not this variation carries significance in this patient will be uncertain.', '2016-09-10');

--
-- Table structure for tier_table
--

CREATE TABLE IF NOT EXISTS tier_table (
	tier_id int NOT NULL AUTO_INCREMENT,

	-- Foreign keys
	

	-- data
	tier varchar(10) NOT NULL,
	tier_summary varchar(50) NOT NULL,

	-- control info

	PRIMARY KEY (tier_id),
	UNIQUE KEY tier_id (tier_id)

) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1;


CREATE TABLE IF NOT EXISTS tissue_type_table (
	tissue_id int NOT NULL AUTO_INCREMENT,

	-- Foreign keys
	

	-- data
	tissue varchar(30) NOT NULL,

	-- control info

	PRIMARY KEY (tissue_id),
	UNIQUE KEY tissue_id (tissue_id)

) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1;

CREATE TABLE IF NOT EXISTS tumor_type_table (
	tumor_id int NOT NULL AUTO_INCREMENT,

	-- Foreign keys
	

	-- data
	tumor varchar(30) NOT NULL,

	-- control info

	PRIMARY KEY (tumor_id),
	UNIQUE KEY tumor_id (tumor_id)

) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1;


--
-- Table structure for variant_tier_xref
--

CREATE TABLE IF NOT EXISTS variant_tier_xref (
	vt_xref_id int NOT NULL AUTO_INCREMENT,

	-- Foreign keys
	tier_id int,
	knowledge_id int,
	tissue_id int,
	user_id int,

	-- data
	status varchar(10) NOT NULL DEFAULT 'active',

	-- control info
	time_stamp timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,

	PRIMARY KEY (vt_xref_id),
	UNIQUE KEY vt_xref_id (vt_xref_id)

) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1;

--
-- Table structure for report_steps_table
--

CREATE TABLE IF NOT EXISTS report_steps_table (
	step_id int NOT NULL AUTO_INCREMENT,

	-- Foreign keys
	
	-- data
	step varchar(30) NOT NULL,
	-- control info
	time_stamp timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,

	PRIMARY KEY (step_id),
	UNIQUE KEY step_id (step_id)

) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1;

--
-- Table structure for step_run_xref_id
--

CREATE TABLE IF NOT EXISTS step_run_xref (
	step_run_xref_id int NOT NULL AUTO_INCREMENT,

	-- Foreign keys
	run_id int,
	step_id int,
	user_id int,

	-- data

	-- control info
	time_stamp timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,

	PRIMARY KEY (step_run_xref_id),
	UNIQUE KEY step_run_xref_id (step_run_xref_id)

) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1;

CREATE TABLE IF NOT EXISTS run_xref_gene_interpt (
	run_xref_gene_interpt_id int NOT NULL AUTO_INCREMENT,

	-- Foreign keys
	user_id int,
	gene_interpt_id int,

	-- data

	-- control info
	time_stamp timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,

	PRIMARY KEY (run_xref_gene_interpt_id),
	UNIQUE KEY run_xref_gene_interpt_id (run_xref_gene_interpt_id)

) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1;

CREATE TABLE IF NOT EXISTS gene_interpt_table (
	gene_interpt_id int NOT NULL AUTO_INCREMENT,

	-- Foreign keys
	run_id int,
	user_id int,

	-- data
	interpt TEXT,
	gene varchar(40),
	
	-- control info
	time_stamp timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,

	PRIMARY KEY (gene_interpt_id),
	UNIQUE KEY gene_interpt_id (gene_interpt_id)

) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1;

--
-- Table structure for ngs_panel
--

CREATE TABLE IF NOT EXISTS ngs_panel (
	ngs_panel_id int NOT NULL AUTO_INCREMENT,

	-- Foreign keys
	
	-- data
	type varchar(50) NOT NULL,
	method text,
	url varchar(255), 
	disclaimer text, 
	limitations TEXT,
	-- control info
	time_stamp timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,

	PRIMARY KEY (ngs_panel_id),
	UNIQUE KEY ngs_panel_id (ngs_panel_id)

) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1;

--
-- Table structure for observed_variant_table
--

CREATE TABLE IF NOT EXISTS observed_variant_table (
	observed_variant_id int NOT NULL AUTO_INCREMENT,

	-- Foreign keys
	run_id int,
	interpt_id int NOT NULL DEFAULT '0',

	-- data
	genes varchar(30),
	coding varchar(255) NOT NULL,
	amino_acid_change varchar(255) NOT NULL,
	frequency varchar(255),
	genotype varchar(255),
	
	
	phred_qual_score float DEFAULT '0',
	allele_coverage varchar(255),
	allele_ratio varchar(255),
	coverage int NOT NULL DEFAULT '0',
	include_in_report tinyint(1) unsigned NOT NULL DEFAULT '1',
	qc_status varchar(10),
	filter_status varchar(10),
	confirm_status varchar(10),
	tier varchar(15),


	-- control info
	time_stamp timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,

	PRIMARY KEY (observed_variant_id),
	UNIQUE KEY observed_variant_id (observed_variant_id)

) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1;

-- INSERT INTO observed_variant_table (run_id, genes, frequency, genotype, amino_acid_change, coding, phred_qual_score, allele_coverage, allele_ratio, coverage, qc_status, filter_status) VALUES
-- ('1', 'EGFR','CAG=0.05,CGG=24.20', 'CTG/CGG', 'p.Leu858Arg', 'c.2573T>G', '3561.94', 'CTG=1512,CAG=1,CGG=483', 'CTG=0.7575,CAG=5.0E-4,CGG=0.242', '1996', 'passed', 'passed'),
-- ('1', 'IDH1','CAG=0.05,CGG=24.20', 'CTG/CGG', 'p.Arg132Cys', 'c.394C>T', '2014.50', 'CTG=1512,CAG=1,CGG=483', 'CTG=0.7575,CAG=5.0E-4,CGG=0.242', '5000', 'passed', 'failed'),
-- ('2', 'CTNNB1','CAG=0.05,CGG=24.20', 'CTG/CGG', 'p.Thr41Asn', 'c.122C>A', '2054.50', 'CTG=1512,CAG=1,CGG=483', 'CTG=0.7575,CAG=5.0E-4,CGG=0.242', '5000', 'passed', 'passed');

REPLACE run_info_table SET run_id = "1", work_flow_name = "OFA_DNA_wec170817", sample_name = "17_SCN3255", run_date_chip = "170809B", pipeline = "v17080", reference = "hg19", filter_chain = "OFAv170807", total_variant_count = "500", filtered_out_count = "499", status = "complete", time_stamp = "2017-11-06 14:17:48", summary = "extra comments here", run_date = "2017-08-09"
--
-- Table structure for log_table
--

CREATE TABLE IF NOT EXISTS log_table (
	log_id int NOT NULL AUTO_INCREMENT,

	-- Foreign keys
	user_id int,

	-- data
	ip_address varchar(16),
	action varchar(20),
	msg varchar(255),

	-- control info
	time_stamp timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,

	PRIMARY KEY (log_id),
	UNIQUE KEY log_id (log_id)

) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1;

--
-- Table structure for detailed_log_table
--

CREATE TABLE IF NOT EXISTS detailed_log_table (
	d_log_id int NOT NULL AUTO_INCREMENT,

	-- Foreign keys
	log_id int,

	-- data
	details text,

	-- control info
	time_stamp timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,

	PRIMARY KEY (d_log_id),
	UNIQUE KEY d_log_id (d_log_id)

) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1;

--
-- Table structure for detailed_log_table
--

CREATE TABLE IF NOT EXISTS ip_info_table (
	ip_info_id int NOT NULL AUTO_INCREMENT,

	-- Foreign keys
	
	-- data
	ip_address varchar(16),
	continent varchar(50),
	country varchar(50),
	state varchar(50),
	city varchar(50),

	-- control info
	time_stamp timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,

	PRIMARY KEY (ip_info_id),
	UNIQUE KEY ip_info_id (ip_info_id)

) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1;


--
-- Table structure for confirmed_table
--

CREATE TABLE IF NOT EXISTS confirmed_table (
	confirm_id int NOT NULL AUTO_INCREMENT,

	-- Foreign keys
	observed_variant_id int,
	user_id int,

	-- data
	genes varchar(20),
	mutation_type varchar(10),
	confirmation_type varchar(20),
	confirmation_doc varchar(20),
	confirmation_status tinyint(1) unsigned NOT NULL DEFAULT '1',

	-- control info
	time_stamp timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,

	PRIMARY KEY (confirm_id),
	UNIQUE KEY confirm_id (confirm_id)

) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1;

INSERT INTO confirmed_table (genes, mutation_type, confirmation_type, confirmation_doc, ngs_panel_id) VALUES
('EGFR', 'SNP', 'Sanger', 'doc file', '1'),
('EGFR', 'SNP', 'Sanger', 'doc file', '1'),
('EGFR', 'SNP', 'Sanger', 'doc file', '1'),
('EGFR', 'SNP', 'Sanger', 'doc file', '1'),
('EGFR', 'SNP', 'Sanger', 'doc file', '1'),
('EGFR', 'SNP', 'Sanger', 'doc file', '1'),
('EGFR', 'SNP', 'Sanger', 'doc file', '1'),
('EGFR', 'SNP', 'Sanger', 'doc file', '1'),
('EGFR', 'SNP', 'Sanger', 'doc file', '1');

SELECT isct.expiration_date, isct.po,isct.instrument_id,isct.instrument_service_contract_id, isct.description, isct.contract_num, isct.quote_num,isct.start_date, isct.price, MAX(isct.expiration_date) AS max_expiration_date

FROM instrument_service_contract_table isct 

GROUP BY isct.instrument_id,isct.instrument_service_contract_id, isct.description, isct.contract_num, isct.quote_num,isct.start_date, isct.price


CREATE VIEW gene_covered_on_date_vw AS
SELECT ngs_panel_id, MIN(date_gene_added) AS date_gene_added, gene FROM genes_covered_by_panel_table GROUP BY ngs_panel_id, gene;


###################################################################
CREATE VIEW num_samples_per_library_pool_vw AS
SELECT lpt.library_pool_id, COUNT(*) AS num_samples_in_pool
FROM library_pool_table lpt 
LEFT JOIN visits_in_pool_table vipt ON lpt.library_pool_id = vipt.library_pool_id
GROUP BY vipt.library_pool_id

###################################################################
CREATE VIEW pending_NGS_completed_steps_vw AS
SELECT srx.run_id, GROUP_CONCAT(ut.first_name, "->", rst.step ORDER BY srx.time_stamp ASC SEPARATOR ", ") AS editors_per_step, GROUP_CONCAT(rst.step ORDER BY srx.time_stamp ASC SEPARATOR ", ") AS completed_steps 
FROM step_run_xref srx
LEFT JOIN report_steps_table rst ON rst.step_id = srx.step_id
LEFT JOIN user_table ut ON ut.user_id = srx.user_id

GROUP BY srx.run_id

###################################################################
CREATE VIEW turnaround_time_test_vw AS
SELECT DATEDIFF(ott.finish_date, ott.start_date) AS actual_turnaround_time, otst.test_name, ott.orderable_tests_id

FROM ordered_test_table ott 

LEFT JOIN orderable_tests_table otst ON otst.orderable_tests_id = ott.orderable_tests_id
 #$$$$$$$$$$$$$$$$$$$$$$$$$
CREATE VIEW turnaround_time_test_stats_vw AS
SELECT ROUND(AVG(ttt.actual_turnaround_time),1) AS avg_turnaround_time, MAX(ttt.actual_turnaround_time) AS max_turnaround_time, ttt.test_name, ttt.orderable_tests_id, COUNT(ttt.orderable_tests_id) AS num_tests

FROM turnaround_time_test_vw ttt 

GROUP BY ttt.orderable_tests_id

###################################################################

-- CREATE VIEW ngs_tests_per_sample_vw AS
-- SELECT ott.sample_log_book_id, GROUP_CONCAT(otst.test_name SEPARATOR ",") AS ordered_tests, GROUP_CONCAT(otst.test_name, " : ", ott.consent_for_research SEPARATOR ", ") AS ordered_tests_by_reserach_consent, GROUP_CONCAT(otst.test_name, " : ", ott.consent_info SEPARATOR ", ") AS ordered_tests_by_consent_received, GROUP_CONCAT(ott.orderable_tests_id SEPARATOR ",") AS ordered_test_ids, GROUP_CONCAT(ott.test_status SEPARATOR ",") AS test_status_concat

-- FROM ordered_test_table ott 

-- LEFT JOIN orderable_tests_table otst ON otst.orderable_tests_id = ott.orderable_tests_id

-- GROUP BY ott.sample_log_book_id

###################################################################
CREATE VIEW ordered_reflexes_per_sample_vw AS
SELECT ortt.sample_log_book_id, GROUP_CONCAT(rt.reflex_name SEPARATOR ",") AS ordered_reflexes,  GROUP_CONCAT(rt.reflex_id SEPARATOR ",") AS ordered_reflex_ids, GROUP_CONCAT(ortt.reflex_status SEPARATOR ",") AS reflex_status_concat

FROM ordered_reflex_test_table ortt  

LEFT JOIN reflex_table rt ON rt.reflex_id = ortt.reflex_id

GROUP BY ortt.sample_log_book_id


###################################################################
CREATE VIEW ordered_tests_per_sample_vw AS
SELECT ott.sample_log_book_id, GROUP_CONCAT(otst.test_name SEPARATOR ",") AS ordered_tests, GROUP_CONCAT(otst.test_name, " : ", ott.consent_for_research SEPARATOR ", ") AS ordered_tests_by_reserach_consent, GROUP_CONCAT(otst.test_name, " : ", ott.consent_info SEPARATOR ", ") AS ordered_tests_by_consent_received, GROUP_CONCAT(ott.orderable_tests_id SEPARATOR ",") AS ordered_test_ids, GROUP_CONCAT(ott.test_status SEPARATOR ",") AS test_status_concat, GROUP_CONCAT(CASE WHEN ott.test_result <> "" THEN CONCAT(otst.test_name, " : ", ott.test_result) END SEPARATOR ", ") AS ordered_tests_by_test_result

FROM ordered_test_table ott 

LEFT JOIN orderable_tests_table otst ON otst.orderable_tests_id = ott.orderable_tests_id

GROUP BY ott.sample_log_book_id;

##################################################################
CREATE VIEW non_stop_ordered_tests_per_sample_vw AS
SELECT ott.sample_log_book_id, GROUP_CONCAT(otst.test_name SEPARATOR ",") AS ordered_tests, GROUP_CONCAT(otst.test_name, " : ", ott.consent_for_research SEPARATOR ", ") AS ordered_tests_by_reserach_consent, GROUP_CONCAT(otst.test_name, " : ", ott.consent_info SEPARATOR ", ") AS ordered_tests_by_consent_received, GROUP_CONCAT(ott.orderable_tests_id SEPARATOR ",") AS ordered_test_ids, GROUP_CONCAT(ott.test_status SEPARATOR ",") AS test_status_concat

FROM ordered_test_table ott 

LEFT JOIN orderable_tests_table otst ON otst.orderable_tests_id = ott.orderable_tests_id

WHERE ott.test_status <> 'stop'

GROUP BY ott.sample_log_book_id

#############################################################
CREATE VIEW tasks_per_users_extraction_vw AS 
SELECT GROUP_CONCAT(tt.task, ": ", ut.first_name, " ", ut.last_name ORDER BY tt.task_id SEPARATOR ", ") AS user_tasks, elt.extraction_log_id, elt.ordered_test_id

FROM extraction_log_table elt

LEFT JOIN users_performed_task_table uptt ON uptt.ref_id = elt.extraction_log_id AND uptt.ref_table = "extraction_log_table"

LEFT JOIN task_table tt ON uptt.task_id = tt.task_id

LEFT JOIN user_table ut ON ut.user_id = uptt.user_id 

GROUP BY elt.extraction_log_id



#############################################################
CREATE VIEW tech_test_history_vw AS 
SELECT slbt.sample_log_book_id, otst.test_name, tt.task, CONCAT(ut.first_name," " ,ut.last_name) AS users_name, ott.expected_turnaround_time, DATEDIFF(finish_date, start_date) AS actual_turnaround_time, (ott.expected_turnaround_time - DATEDIFF(finish_date, start_date)) AS diff_turnaround_time, uptt.time_stamp, uptt.user_id

FROM users_performed_task_table uptt 

LEFT JOIN ordered_test_table ott ON ott.ordered_test_id = uptt.ref_id 

LEFT JOIN task_table tt ON tt.task_id = uptt.task_id

LEFT JOIN user_table ut ON ut.user_id = uptt.user_id

LEFT JOIN orderable_tests_table otst ON otst.orderable_tests_id = ott.orderable_tests_id

LEFT JOIN sample_log_book_table slbt ON ott.sample_log_book_id = slbt.sample_log_book_id

WHERE uptt.ref_table = 'ordered_test_table' AND ott.test_status = 'complete'




#############################################################
CREATE VIEW visit_table_concat_comments_vw AS
SELECT vt.visit_id, GROUP_CONCAT(ct.comment SEPARATOR ",") AS problems,GROUP_CONCAT(ct.comment SEPARATOR "&#*") AS problems_special_separator

FROM visit_table vt  

LEFT JOIN comment_table ct ON ct.comment_ref = vt.visit_id AND ct.comment_type = "add_visit"

GROUP BY ct.comment_ref

#############################################################
CREATE VIEW ordered_test_table_concat_comments_vw AS
SELECT ott.ordered_test_id, GROUP_CONCAT(ct.comment SEPARATOR ",") AS problems,GROUP_CONCAT(ct.comment SEPARATOR "&#*") AS problems_special_separator

FROM ordered_test_table ott  

LEFT JOIN comment_table ct ON ct.comment_ref = ott.ordered_test_id AND ct.comment_type = "ordered_test_table"

GROUP BY ct.comment_ref

#############################################################
CREATE VIEW sample_log_book_concat_comments_vw AS
SELECT slbt.sample_log_book_id, GROUP_CONCAT(ct.comment SEPARATOR ",") AS problems,GROUP_CONCAT(ct.comment SEPARATOR "&#*") AS problems_special_separator

FROM sample_log_book_table slbt  

LEFT JOIN comment_table ct ON ct.comment_ref = slbt.sample_log_book_id AND ct.comment_type = "sample_log_book_table"

GROUP BY ct.comment_ref
 
####################################################									
CREATE VIEW turnaround_time_concat_vw AS
	SELECT ttt.orderable_tests_id, MIN(ttt.turnaround_time) AS min_turnaround_time, MAX(ttt.turnaround_time) AS max_turnaround_time FROM test_turnaround_table ttt GROUP BY ttt.orderable_tests_id;


CREATE VIEW library_pool_A_vw AS
	SELECT vt.visit_id, lpt.chip_flow_cell, vipt.sample_name, vipt.patient_name, pclt.pool_chip_linker_id, vipt.order_num, vipt.visits_in_pool_id, vipt.barcode_ion_torrent, vipt.i7_index_miseq, vipt.i5_index_miseq, vt.version, vt.sample_type, pt.medical_record_num, psv_dna.pre_step_visit_id AS pool_dna_conc_id, psv_dna.concentration AS dna_conc, psv_dna.status AS dna_status, psv_amp.pre_step_visit_id AS pool_amp_conc_id, psv_amp.concentration AS amp_conc, psv_amp.status AS amp_status, sdt.dilution_total FROM pool_chip_linker_table pclt LEFT JOIN library_pool_table lpt ON lpt.library_pool_id = pclt.library_pool_id_A LEFT JOIN visits_in_pool_table vipt ON vipt.library_pool_id = lpt.library_pool_id LEFT JOIN visit_table vt ON vt.visit_id = vipt.visit_id LEFT JOIN patient_table pt ON vt.patient_id = pt.patient_id LEFT JOIN pre_step_visit psv_dna ON psv_dna.step = "add_DNA_conc" AND psv_dna.visit_id = vt.visit_id LEFT JOIN pre_step_visit psv_amp ON psv_amp.step = "add_amplicon_conc" AND psv_amp.visit_id = vt.visit_id LEFT JOIN stock_dilution_table sdt ON sdt.visits_in_pool_id = vipt.visits_in_pool_id ORDER BY pclt.pool_chip_linker_id ASC, vipt.order_num ASC;

CREATE VIEW library_pool_B_vw AS
	SELECT vt.visit_id, lpt.chip_flow_cell, vipt.sample_name, vipt.patient_name, pclt.pool_chip_linker_id, vipt.order_num, vipt.visits_in_pool_id, vipt.barcode_ion_torrent, vipt.i7_index_miseq, vipt.i5_index_miseq, vt.version, vt.sample_type, pt.medical_record_num, psv_dna.pre_step_visit_id AS pool_dna_conc_id, psv_dna.concentration AS dna_conc, psv_dna.status AS dna_status, psv_amp.pre_step_visit_id AS pool_amp_conc_id, psv_amp.concentration AS amp_conc, psv_amp.status AS amp_status, sdt.dilution_total FROM pool_chip_linker_table pclt LEFT JOIN library_pool_table lpt ON lpt.library_pool_id = pclt.library_pool_id_B LEFT JOIN visits_in_pool_table vipt ON vipt.library_pool_id = lpt.library_pool_id LEFT JOIN visit_table vt ON vt.visit_id = vipt.visit_id LEFT JOIN patient_table pt ON vt.patient_id = pt.patient_id LEFT JOIN pre_step_visit psv_dna ON psv_dna.step = "add_DNA_conc" AND psv_dna.visit_id = vt.visit_id LEFT JOIN pre_step_visit psv_amp ON psv_amp.step = "add_amplicon_conc" AND psv_amp.visit_id = vt.visit_id LEFT JOIN stock_dilution_table sdt ON sdt.visits_in_pool_id = vipt.visits_in_pool_id WHERE vt.visit_id IS NOT NULL ORDER BY pclt.pool_chip_linker_id ASC, vipt.order_num ASC;

CREATE VIEW confirmed_count_vw AS
	SELECT DISTINCT	genes,
					mutation_type,
					COUNT(genes) as confirmation_count,
					IF (COUNT(genes) >= 10, 'yes', 'no') AS met_minimium,
					np.ngs_panel_id 
	        
	FROM 			confirmed_table ct

	JOIN 			ngs_panel np
	ON 				np.ngs_panel_id = ct.ngs_panel_id

	WHERE 			ct.confirmation_status = 1

	GROUP BY 			genes, mutation_type, np.ngs_panel_id;

INSERT INTO confirmed_table (observed_variant_id, genes, mutation_type, ngs_panel_id, confirmation_status, user_id,confirmation_type) VALUES
(0,'EGFR','SNP',1,1,1,'Sanger'),
(0,'EGFR','SNP',1,1,1,'Sanger'),
(0,'EGFR','SNP',1,1,1,'Sanger'),
(0,'EGFR','SNP',1,1,1,'Sanger'),
(0,'EGFR','SNP',1,1,1,'Sanger'),
(0,'EGFR','SNP',1,1,1,'Sanger'),
(0,'EGFR','SNP',1,1,1,'Sanger'),
(0,'EGFR','SNP',1,1,1,'Sanger'),
(0,'EGFR','SNP',1,1,1,'Sanger'),
(0,'EGFR','SNP',1,1,1,'Sanger'),
(0,'EGFR','SNP',1,1,1,'Sanger'),
(0,'EGFR','SNP',1,1,1,'Sanger'),
(0,'EGFR','SNP',1,1,1,'Sanger'),
(0,'EGFR','SNP',1,1,1,'Sanger'),
(0,'EGFR','SNP',1,1,1,'Sanger'),
(0,'EGFR','SNP',1,1,1,'Sanger'),
(0,'EGFR','SNP',1,1,1,'Sanger'),
(0,'EGFR','SNP',1,1,1,'Sanger'),
(0,'EGFR','SNP',1,1,1,'Sanger'),
(0,'EGFR','SNP',1,1,1,'Sanger'),
(0,'EGFR','SNP',1,1,1,'Sanger'),
(0,'EGFR','SNP',1,1,1,'Sanger');

CREATE VIEW reviewed_suggestions_vw AS
	
	SELECT 	srt.*,
			rsx.suggested_suggested_revision_id,
			rsx.reviewed_suggested_revision_id,
			CONCAT(ut_s.first_name," " ,ut_s.last_name) AS suggestor_user_name,
			CONCAT(ut_r.first_name," " ,ut_r.last_name) AS reviwer_user_name,
			str_r.suggested_revision AS reviewer_comment 

	FROM 	suggested_revision_table srt 

	LEFT JOIN reviewed_suggested_xref rsx 
	ON 		rsx.suggested_suggested_revision_id = srt.suggested_revision_id

	LEFT JOIN user_table ut_s
	ON 		srt.user_id = ut_s.user_id

	LEFT JOIN user_table ut_r
	ON 		rsx.user_id = ut_r.user_id

	LEFT JOIN suggested_revision_table str_r
	ON 		str_r.suggested_revision_id = rsx.reviewed_suggested_revision_id

	WHERE 	srt.status = "reviewed"

CREATE VIEW pending_samples_substring_order_num_vw AS

SELECT CAST(SUBSTRING(vt.order_num, 7) AS INT) AS order_num_substring, CAST(SUBSTRING(vt.order_num, 1,2) AS INT) AS order_num_year, np.ngs_panel_id, vt.visit_id, vt.patient_id, vt.order_num, CONCAT(vt.mol_num,"_",vt.soft_lab_num) AS mol_num_soft_lab, vt.control_type_id, vt.sample_type, CONCAT(pt.first_name,"_",vt.version) AS control_name, CONCAT(pt.first_name," " , pt.middle_name," ",pt.last_name) AS patient_name, psv.concentration AS qubit, ( SELECT GROUP_CONCAT(psv5.step ORDER BY psv5.time_stamp ASC SEPARATOR ", ") AS steps_passed1 FROM pre_step_visit psv5 WHERE psv5.visit_id = vt.visit_id AND psv5.status = "passed" GROUP BY vt.visit_id )AS steps_passed, ( SELECT GROUP_CONCAT(psv5.status ORDER BY psv5.time_stamp ASC SEPARATOR ", ") AS steps_status FROM pre_step_visit psv5 WHERE psv5.visit_id = vt.visit_id GROUP BY vt.visit_id )AS steps_status FROM visit_table vt LEFT JOIN ngs_panel np ON np.ngs_panel_id = vt.ngs_panel_id LEFT JOIN patient_table pt ON pt.patient_id = vt.patient_id LEFT JOIN pre_step_visit psv ON psv.visit_id = vt.visit_id AND psv.step = "add_DNA_conc" ORDER BY order_num_year ASC, order_num_substring ASC;



CREATE VIEW active_suggestions_vw AS SELECT srt.*, CONCAT(ut.first_name," " ,ut.last_name) AS suggestor_user_name FROM suggested_revision_table srt LEFT JOIN user_table ut ON srt.user_id = ut.user_id WHERE srt.status = "active";


--
-- Table structure for mutation_type_ref
--

CREATE TABLE IF NOT EXISTS mutation_type_ref (
	mutation_type_id int NOT NULL AUTO_INCREMENT,

	-- Foreign keys
	ngs_panel_id int,

	-- data
	cosmic_mutation_type VARCHAR(35),
	snp_or_indel VARCHAR(7),

	-- control info
	time_stamp timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,

	PRIMARY KEY (mutation_type_id),
	UNIQUE KEY mutation_type_id (mutation_type_id)

) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1;

INSERT INTO mutation_type_ref (cosmic_mutation_type, snp_or_indel) VALUES
('Substitution - Missense', 'SNP'),
('Substitution - Nonsense', 'SNP'),
('Substitution - coding silent', 'SNP'),
('Complex - frameshift', 'SNP'),
('Insertion - In frame', 'Indel'),
('Complex - insertion inframe', 'Indel'),
('Complex - deletion inframe', 'Indel'),
('Insertion - Frameshift', 'Indel'),
('Complex - compound substitution', 'Unknown'),
('Unknown', 'Unknown'),
('Nonstop extension', 'Unknown');

-- comment_table
CREATE INDEX idx_user_id
ON comment_table (user_id);
CREATE INDEX idx_comment_ref
ON comment_table (comment_ref);
CREATE INDEX idx_comment_type
ON comment_table (comment_type);

-- confirmed_table
CREATE INDEX idx_user_id
ON confirmed_table (user_id);
CREATE INDEX idx_observed_variant_id
ON confirmed_table (observed_variant_id);
CREATE INDEX idx_ngs_panel_id
ON confirmed_table (ngs_panel_id);

-- detailed_log_table
CREATE INDEX idx_log_id
ON detailed_log_table (log_id);

-- knowledge_base_table
CREATE INDEX idx_genes
ON knowledge_base_table (genes);
CREATE INDEX idx_coding
ON knowledge_base_table (coding);
CREATE INDEX idx_amino_acid_change
ON knowledge_base_table (amino_acid_change);

-- mutation_type_ref
CREATE INDEX idx_snp_or_indel
ON mutation_type_ref (snp_or_indel);

-- notes
CREATE INDEX idx_user_id
ON notes_table (user_id);

--observed_variant_table
CREATE INDEX idx_run_id
ON observed_variant_table (run_id);
CREATE INDEX idx_interpt_id
ON observed_variant_table (interpt_id);
CREATE INDEX idx_genes
ON observed_variant_table (genes);
CREATE INDEX idx_coding
ON observed_variant_table (coding);
CREATE INDEX idx_amino_acid_change
ON observed_variant_table (amino_acid_change);

-- patient_table
CREATE INDEX idx_user_id
ON patient_table (user_id);

-- permission_user_xref
CREATE INDEX idx_user_id
ON permission_user_xref (user_id);
CREATE INDEX idx_permission_id
ON permission_user_xref (permission_id);

-- pre_step_visit
CREATE INDEX idx_user_id
ON pre_step_visit (user_id);
CREATE INDEX idx_visit_id
ON pre_step_visit (visit_id);

-- problem_pages_table
CREATE INDEX idx_user_id
ON problem_pages_table (user_id);

-- step_run_xref
CREATE INDEX idx_user_id
ON step_run_xref (user_id);
CREATE INDEX idx_step_id
ON step_run_xref (step_id);
CREATE INDEX idx_run_id
ON step_run_xref (run_id);

-- variant_tier_xref
CREATE INDEX idx_user_id
ON variant_tier_xref (user_id);
CREATE INDEX idx_tier_id
ON variant_tier_xref (tier_id);
CREATE INDEX idx_knowledge_id
ON variant_tier_xref (knowledge_id);
CREATE INDEX idx_tissue_id
ON variant_tier_xref (tissue_id);

-- visit_table
CREATE INDEX idx_user_id
ON visit_table (user_id);
CREATE INDEX idx_run_id
ON visit_table (run_id);
CREATE INDEX idx_ngs_panel_id
ON visit_table (ngs_panel_id);
CREATE INDEX idx_patient_id
ON visit_table (patient_id);
CREATE INDEX idx_diagnosis_id
ON visit_table (diagnosis_id);
CREATE INDEX idx_primary_tumor_site
ON visit_table (primary_tumor_site);

-- tissue_type_table
CREATE INDEX idx_tissue 
ON tissue_type_table (tissue);

CREATE INDEX idx_user_id
ON no_snv_summary_table (user_id);

-- INSERT INTO ngs_panel (type, method, url) VALUES
-- ('Oncomine Focus Assay', 'This Cancer Gene Mutation Panel is designed to target 2,855 mutations in the following 52 key cancer genes: ABL1, ERBB2, GNAQ, MYC, AKT1, ERBB4, HRAS, MYCN, AKT3, ERBB3, IDH1, NRAS, ALK, ERG, IDH2, NTRK1, AR, ESR1, JAK1, NTRK2, AXL, ETV1, JAK2, NTRK3, BRAF, ETV4, JAK3, PDGFRA, CCND1, ETV5, KIT, PIK3CA, CDK4, FGFR1, KRAS, PPARG, CDK6, FGFR2, MAP2K1, RAF1, CTNNB1, FGFR3, MAP2K2, RET, DDR2, FGFR4, MET, ROS1, EGFR, GNA11, MTOR, and SMO.

-- This mutation panel is designed to detect targeted mutations only. The 52 genes are not sequenced in their entirety. Mutations outside the targeted regions will not be detected. The limit of detection is 5% at 500X coverage and 10% at 200X coverage. This technology cannot reliably detect mutations at coverage below 100X. Confirmation of mutations is performed by castPCR™, pyrosequencing, or Sanger sequencing.', 'https://www.thermofisher.com/us/en/home/clinical/preclinical-companion-diagnostic-development/oncomine-oncology/oncomine-focus-assay.html'),
-- ('myeloid', '', '');


-- Table structure for notes_table


CREATE TABLE IF NOT EXISTS notes_table (
	note_id int NOT NULL AUTO_INCREMENT,

	-- Foreign keys
	user_id int,

	-- data
	note text,

	-- control info
	time_stamp timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,

	PRIMARY KEY (note_id),
	UNIQUE KEY note_id (note_id)

) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1;


--
-- Table structure for report_table
--

-- CREATE TABLE IF NOT EXISTS report_table (
-- 	report_id int NOT NULL AUTO_INCREMENT,

-- 	-- Foreign keys
	

-- 	-- data

-- 	-- control info
-- 	time_stamp timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,

-- 	PRIMARY KEY (run_id),
-- 	UNIQUE KEY run_id (run_id)

-- ) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1;

-- Fake Data

-- INSERT INTO report_steps_table (step) VALUES
-- ('patient_info'),
-- ('visit_info'),
-- ('confirmed'),
-- ('pick_variant_interpretation');

-- INSERT INTO tier_table (tier, tier_summary) VALUES
-- ('Tier 1', 'Variants of strong Clinical significance'),
-- ('Tier 2', 'Variants of Potential Clinical Significance'),
-- ('Tier 3', 'Variants of Unknown Clinical Significance'),
-- ('Tier 4', 'Benign or likely benign Variants');

-- INSERT INTO tissue_type_table (tissue) VALUES
-- ('Liver'),
-- ('Lung'),
-- ('Brain'),
-- ('Colorectal'),
-- ('Melanoma'),
-- ('Ameloblastoma');

-- INSERT INTO variant_tier_xref(tier_id, knowledge_id, tissue_id) VALUES
-- ('1', '2', '2'),
-- ('2', '2', '1'),
-- ('4', '1', '3');

CREATE VIEW report_ids_vw AS
	SELECT 		rif.run_id,
				vt.visit_id,
				vt.patient_id,
				ngs_panel_id

	FROM			run_info_table rif

	LEFT JOIN 	visit_table vt
	ON 			vt.run_id = rif.run_id;


CREATE VIEW tier_tissue_knowledge_vw AS
	
    SELECT 		kbt.*,

    				tt.tier_id,
    				tt.tier,
    				tt.tier_summary,

    				ttt.tissue_id,
    				ttt.tissue,

    				vtx.vt_xref_id,
    				vtx.status,
    				vtx.user_id as vtx_user_id    				

    	FROM 		knowledge_base_table kbt

    	LEFT JOIN		variant_tier_xref vtx
    	ON 			vtx.knowledge_id = kbt.knowledge_id

    	LEFT JOIN		tier_table tt
    	ON 			vtx.tier_id = tt.tier_id

    	LEFT JOIN 	tissue_type_table ttt
    	ON 			vtx.tissue_id = ttt.tissue_id;


-- need each visit separate this groups them
-- CREATE VIEW patient_visit_vw AS
	
-- 	SELECT 		pt.patient_id,
-- 				CONCAT(pt.first_name, " ", pt.middle_name, " ", pt.last_name) AS patient_name,
-- 				pt.dob,
-- 				pt.medical_record_num,
-- 				GROUP_CONCAT(vt.soft_path_num SEPARATOR ", ") AS soft_path_numbers,
-- 				GROUP_CONCAT(vt.soft_lab_num SEPARATOR ", ") AS soft_lab_numbers,
-- 				GROUP_CONCAT(vt.mol_num SEPARATOR ", ") AS molecular_numbers,
-- 				GROUP_CONCAT(vt.primary_tumor_site SEPARATOR ", ") AS primary_tumor_sites,
-- 				GROUP_CONCAT(vt.test_tissue SEPARATOR ", ") AS test_tissues,
-- 				GROUP_CONCAT(vt.run_id SEPARATOR ", ") AS run_ids,
-- 				GROUP_CONCAT(ovt2.reported_genes SEPARATOR ", ") AS reported_mutated_genes,
-- 				GROUP_CONCAT(ovt2.codings SEPARATOR ", ") AS reported_codings,
-- 				GROUP_CONCAT(ovt2.amino_acid_changes SEPARATOR ", ") AS reported_amino_acid_changes,
-- 				GROUP_CONCAT(ovt2.tiers SEPARATOR ", ") AS reported_tier_levels,
-- 				COUNT(vt.visit_id) AS number_visits
					
-- 	FROM 		patient_table pt 

-- 	LEFT JOIN 	visit_table vt
-- 	ON 			vt.patient_id = pt.patient_id

-- 	LEFT JOIN 	(	
-- 					SELECT 		GROUP_CONCAT(ovt.genes SEPARATOR ", ") AS reported_genes,
-- 								GROUP_CONCAT(ovt.coding SEPARATOR ", ") AS codings,
-- 								GROUP_CONCAT(ovt.amino_acid_change SEPARATOR ", ") AS amino_acid_changes,
-- 								GROUP_CONCAT(ovt.tier SEPARATOR ", ") AS tiers,
-- 								ovt.run_id

-- 					FROM 		observed_variant_table ovt

-- 					WHERE 		ovt.include_in_report = "1"

-- 					GROUP BY 		ovt.run_id
-- 				) AS ovt2
-- 	ON 			vt.run_id = ovt2.run_id

-- 	GROUP BY 		vt.patient_id, pt.patient_id				

CREATE OR REPLACE VIEW concat_variants_vw AS select group_concat(ovt.genes separator ', ') AS genes,group_concat(ovt.coding separator ', ') AS codings,group_concat(ovt.amino_acid_change separator ', ') AS amino_acid_changes,group_concat(ovt.tier separator ', ') AS tiers,group_concat(ovt.frequency separator ', ') AS VAF,ovt.run_id AS run_id from observed_variant_table ovt where (ovt.include_in_report = '1') group by ovt.run_id;


 CREATE OR REPLACE VIEW completed_patient_reports_vw AS select pt.patient_id AS patient_id,rit.run_id AS run_id,concat(pt.first_name,' ',pt.middle_name,' ',pt.last_name) AS patient_name,pt.first_name AS first_name,pt.middle_name AS middle_name,pt.last_name AS last_name,pt.dob AS DOB,pt.medical_record_num AS medical_record_num,vt.soft_path_num AS soft_path_num,vt.soft_lab_num AS soft_lab_num,vt.mol_num AS mol_num,vt.primary_tumor_site AS primary_tumor_site,vt.ngs_panel_id AS ngs_panel_id,vt.test_tissue AS test_tissue,vt.block AS block,vt.tumor_cellularity AS tumor_cellularity,vt.visit_id AS visit_id,rit.run_date AS run_date,cv_vw.genes AS genes,cv_vw.codings AS codings,cv_vw.amino_acid_changes AS amino_acid_changes,cv_vw.tiers AS tiers, cv_vw.VAF AS VAFS from (((patient_table pt left join visit_table vt on((vt.patient_id = pt.patient_id))) left join run_info_table rit on((rit.run_id = vt.run_id))) left join concat_variants_vw cv_vw on((vt.run_id = cv_vw.run_id))) where (rit.status = 'completed');





CREATE VIEW concat_variants_vw AS 

	select 	group_concat(ovt.genes separator ', ') AS genes,
			group_concat(ovt.coding separator ', ') AS codings,
			group_concat(ovt.amino_acid_change separator ', ') AS amino_acid_changes,
			group_concat(ovt.tier separator ', ') AS tiers,
			group_concat(ovt.frequency separator ', ') AS VAF,
			ovt.run_id AS run_id 
	
	from 	observed_variant_table ovt 

	where 	(ovt.include_in_report = '1') group by ovt.run_id

CREATE VIEW completed_patient_reports_vw AS
	
SELECT pt.patient_id,
rit.run_id,
CONCAT(pt.first_name, " ", pt.middle_name, " ", pt.last_name) AS patient_name,
pt.first_name,
pt.middle_name,
pt.last_name,
pt.dob AS DOB,
pt.medical_record_num,
vt.soft_path_num,
vt.soft_lab_num,
vt.mol_num,
vt.primary_tumor_site,
vt.test_tissue,
vt.block,
vt.tumor_cellularity,
vt.visit_id,
rit.run_date,
cv_vw.genes,
cv_vw.codings,
cv_vw.amino_acid_changes,
cv_vw.tiers

FROM patient_table pt

LEFT JOIN visit_table vt
ON vt.patient_id = pt.patient_id

LEFT JOIN run_info_table rit
ON rit.run_id = vt.run_id

LEFT JOIN concat_variants_vw cv_vw
ON vt.run_id = cv_vw.run_id

WHERE rit.status = 'completed'



-- INSERT INTO run_info_table (work_flow_name, sample_name, run_date_chip, pipeline, reference, filter_chain, total_variant_count, filtered_out_count, status) VALUES
-- ('OFA_DNA_wec170817', '17_SCN3255', '170809B', 'v17080', 'hg19', 'OFAv170807', '500', '499', 'pending'),
-- ('OFA_DNA_wec170817', '17_SCN4000', '190809B', 'v17080', 'hg19', 'OFAv170807', '500', '498', 'complete');

-- INSERT INTO knowledge_base_table (genes, chr, amino_acid_change, coding, type, locus, location, exon, refs, cosmic, dbsnp, pfam, transcript, notes) VALUES
-- ('EGFR', 'chr7', 'p.Leu858Arg', 'c.2573>G', 'SNV', '5529514', 'exon', '21', '27070783,23777544', '12366:24268:13553:26129', 'rs397517129:rs121434568:rs121913443', 'PF00757:PF01030:PF00069:PF07714', 'NM_005228.3', 'notes'),
-- ('IDH1', '', 'p.R132C', 'c.394C>T', 'SNV', '', 'exon', '4', '', '28747', '', '', '', 'notes'),
-- ('SRSF2', '', 'p.P95L', 'c.284C>T', 'SNV', '', 'exon', '1', '', '146288', '', '', '', 'notes');

-- INSERT INTO comment_table (user_id, comment_ref, comment_type, comment) VALUES
-- ('1', '1', 'knowledge_base_table', 'here is a comment'),
-- ('1', '2', 'knowledge_base_table', '258 long comment here with lots of data about this variant.  Some interesting info and more so much more more llllllllll llllllllllllll llllllllllllll llllllllll lllllllllll lllllllll lllllllll lllllllllll lllllllllll llllllll llllllll lllllllll llllllllllll'),
-- ('1', '1', 'knowledge_base_table', '258 long comment here with lots of data about this variant.  Some interesting info and more so much more more llllllllll llllllllllllll llllllllllllll llllllllll lllllllllll lllllllll lllllllll lllllllllll lllllllllll llllllll llllllll lllllllll llllllllllll'),
-- ('1', '1', 'knowledge_base_table', '768 long comment here with lots of data about this variant.  Some interesting info and more so much more more llllllllll llllllllllllll llllllllllllll llllllllll lllllllllll lllllllll lllllllll lllllllllll lllllllllll llllllll llllllll lllllllll llllllllllll long comment here with lots of data about this variant.  Some interesting info and more so much more more llllllllll llllllllllllll llllllllllllll llllllllll lllllllllll lllllllll lllllllll lllllllllll lllllllllll llllllll llllllll lllllllll llllllllllll long comment here with lots of data about this variant.  Some interesting info and more so much more more llllllllll llllllllllllll llllllllllllll llllllllll lllllllllll lllllllll lllllllll lllllllllll lllllllllll llllllll llllllll lllllllll llllllllllll');

-- INSERT INTO user_table (first_name, last_name, email_address) VALUES
-- ('samantha', 'Taffner', 'taffners@gmail.com');

