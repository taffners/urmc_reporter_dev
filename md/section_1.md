

#General Overview of Molecular Diagnostics Infotrack (MD Infotrack)

- ## About

	1. MD Infotrack is an electronic medical record system built for the University of Rochester Clinical Molecular Diagnostics Laboratory.
	2. It is transforming into a one stop, start to finish software tool, where samples are tracked from login to report generation. 
	3. Single analyte and NGS tests are all logged into MD Infotrack providing a test tracking system which allows managers the ability to easily produce monthly reports and track audits to evaluate laboratory productivity. 
	4. MD Infotrack transforms into an analysis and report building system for NGS tests. Allowing users to access the growing knowledge base along with integrated publicly available databases.
	5. Created by: Samantha Taffner
	6. Administrator: Samantha Taffner

- ## General Software Notes

	1. Use Chrome internet browser to access all web based software in this document.

- ##  Login Information

	1. Request account with MD Infotrack Administrator (Section II.A6)
	2. Using Chrome browser navigate to: <http://pathlamp.urmc-sh.rochester.edu/urmc_reporter>

- ## Permissions

	1. 	Admin
	
		- Access to Admin Page (Section II.J)
		- Create accounts
		- Unlock accounts
		- Lock accounts
		- Grant access
		- Update knowledge base
		- Emailed any bugs reported (Section II.M)
		- Access to change the version of MD Infotrack. (Section II.K2)
		- Admins will be notified via email when certain problems occur. 

	2. Audits
	
		- Allows user to access performance audits (Section II.G).
		- Allows users the ability to edit turnaround times of tests.

	3. Director
		
		- Directors can see verify variants without QC being completed only if all variants are in knowledge base.
		- Only Directors have permissions to change a report from a draft to a finalized report.  Once a director finalizes a report soft path users will be automatically notified that the report is finished and read to be added to soft.
		- Directors can notify reviewers when a report is able to be reviewed.
		- This permission adds the users name to the message board as a possible person to notify.
		- Directors have permissions to cancel NGS tests from pre NGS list.
		- Can only be a reviewer or director.
		- Can not be a tech and a director.

	4. Log book 

		-  Allows user to view and edit logbook 
		- Changes homepage to log book if NGS permissions are not activated.

	5. Manager 
	
		- Managers are sent automatic emails to notify them of expiring service contracts (Section II.N2). 
		
	6. NGS
	
		- Provides access to NGS report building and the progress of all NGS samples.
		- NGS drop down menu is available. [Image II.D6.ii.1](Image II.D6.ii.1)
		- Changes homepage to NGS workflow samples.

	7. Non NGS Wet Bench
	
		- Users with this permission will have their name added to Wet bench techs and Analysis Techs when single analyte tests are signed out  

	8. Reviewer
	
		- This permission puts the users name on the notify reviewers list and listed in the message board
		- Can not be a director and a reviewer

	9. Revise
	
		- Allows user the ability to revise report (Sections VB6, VG3) ONCE READDED UPDATE

	10. Softpath
	
		- Users are automatically emailed when NGS tests are requested and completed. 
		- Softpath users are responsible for scanning printed NGS report without header and attaching it to patient medical record.  

	11. Tech

		- Tech permissions allows a user to change the report to tech approved.  
		- If a director notified reviewers and once a report is changed to tech approved the director will be emailed that the report was completed.

	12. View Only

		-Limits view on NGS homepage so edits can not be made.  This will allow the user to view NGS reports and print them out but this user will not be active and producing the report.  This permission is useful for clerical staff. 
		
		
	13. Wet bench
	
		- This permission will add the users name to a list of possible NGS wet bench techs.

- ## Formatting of MD Infotrack

	1. The color scheme was chosen with red green color blind accessibility in mind.  In some instances fields which are extremely important contain a flashing border to emphasize difference for red green color blind users.  Image II.E1.1 contains an explanation of the color scheme found in MD Infotrack. [Image II.E1.1.II.E2. Required field](Image II.E1.1.II.E2. Required field)

	2. Multiple fields can be selected
	
	3. One field can be selected

- ## List types

	1. There are different list types and the way they look are determined by chrome.  Therefore the look might change as the browser is updated.

	2. Datalist

		-	The datalist and the drop down menu have very similar appearances.  As you can see in Image II.E5.1 the initial appearance does not have an arrow like the drop down menu does.  Also the menu that appears has rounded corners not square corners.
		- 	This list provides a pre-defined options for however, the user can add any values that are not in the list.
		-	Autocomplete can be used to reduce spelling errors.
		-	The list will reset once the input field is blank.

	3. Drop down menu

	4.	Provides a list of pre-defined options however, user input is restricted to values already present in the list.   

		- Multiple select list

			1.	This list allows the user to select multiple values in a predefined list.  
			2.	Selecting multiple options vary in different operating systems:

				- For windows: Hold down the control (ctrl) button to select multiple options with your mouse.
				- For Mac:  Hold down the command button to select multiple options with your mouse.
 
- ##	MD Infotrack Security

	1. Database administrator (Section II.A6) grants access to users
	2. The user will have a username and password
	3. Passwords are saved in database using the md5 one-way encryption algorithm for heightened security.
	4. Temporarily lock account if more than 5 failed login attempts occurred in a 30 minute interval.
	5. Automatically records all login attempts including failed and successful logins.
	6. Automatically logs users off when idle for longer than 1400 seconds.
	7. SQL Injection - All standard practices for preventing SQL injection attacks are implemented.
	8. Drive Encryption – The virtual pathlamp server drive is encrypted.
	9. Recommended:

		- Need a development server so new Red Hat patches and updates can be installed and tested before affecting live server. 
		- Need a file server to save uploaded files so they are partitioned away from live server. (Possible solution would be to save on SQLCLINNGS)
		
- ## Monthly audit reports

	1. Audits provide a monthly report of tests performed every month and how long it took to perform these tests.  An example of an audit report can be found at Section IX.C.  It is required by CAP to record turnaround times on a monthly basis.  
	2. Audits are accessible for users with audit permissions using   on the tool bar.  Make sure access to the audit permission are very restricted since turnaround times can be edited with this permission.
	3. Generate monthly report.

		- Click on  
		- Use appropriate toggle button to access the appropriate month.  Toggle buttons shown in Image II.G3.i.1 will filter data based on the month highlighted with a green button.  Toggle buttons are only present for past months of the current year.
		- Since the start and finish date can sometimes be incorrect, confirm all failed turnaround times are correct by confirming in soft.  These times could be incorrect if the technician forgets to change the status of the test in MD Infotrack to complete on time.  To edit turnaround times in MD Infotrack:

			1.	Edit the start or finish date by clicking on the date and edit it.  The new value will be automatically saved once clicking away (Image II.G3.iii.1).  If the test now falls at or below the expected turnaround time the test will no longer appear on the list.
 
		- Print report by right clicking on   in the upper right hand corner of the chrome browser and then print the report.  
		- Give the report to the Molecular Diagnostics Director to sign.  Once signed keep the report in a safe place for CAP inspections.

- ## Sample Log Book

	1. The sample book is used to keep track of all samples which enter the laboratory.  Enter into the sample log book ASAP after the sample enters lab.
	2. Toggle between samples which have pending samples and all samples using the buttons Image II.H.1. (1).
	3. The sample log book can be sorted by any column by clicking the up and down arrows in Image II.H.1. (2).
	4. All fields in the sample log book can be searched at the same time via the search box highlighted in Image II.H.1. (3).
	5. Test status is color coded with dots in the sample log book Image II.H1.5.1.  Hovering over the dot will make the information in II.H1.5.1. appear.
	6. Add Sample to logbook 

		- Access log book log book -> (Pending Samples or All Samples)
		- Click Add Sample to log Book button.
		- Fill in all information

			1.	Special Cases:
			
				- Cap Samples 
					
					1.	First name Cap 
					2.	Last Name Sample 
					3.	Cap # - in softpath#

				- Anonymous Donor

					1.	Click on Anonymous Donor toggle
					2.	Last name will not be donor ID
					3.	First name will now by anonymous-donor
					4.	Fields soft lab # and MRN number will no longer be required

				- Samples assigned an MD# before MD Infotrack started

					1.	Click on MD# was previously assigned

						-	This will allow you to edit the field however, it will only allow you to enter numbers not previously assigned and will not allow you to assign numbers greater than the last number plus 1.
		- Update log book by clicking

			1.	Edited forms for NGS samples need to be edited in the log book and in the visit and patient forms for the NGS.  It is planned to fix this.


- ## Admin

	1. To access the admin page click on   on the tool bar.  Only users with admin permissions will have access.
	2. Add a User
 
		-	New users can be added using the admin page.  A Captcha is implemented for added security of adding users. 
		-	Once the new user account is created the new user and admins will automatically be emailed with the user’s new temporary password.  The user will be automatically prompted to change their password when logging in.
		-	It is the admin’s responsibility to lock any account where their password was not reset within 7 days.  

	3. User Account permission settings
 
		- All past and present users of Infotrack will be present in a table on the admin page. 
		- Account status
		
			1. Press the lock button to lock an account.  It is the admin’s responsibility to promptly lock out any employee no longer needing access to MD Infotrack for their job.
			2.	To unlock or reset a password click on unlock. 

		- Permissions
		
			1. Permissions can be easily granted or revoked from a user by clicking on the permission button near the users name 
			2. Once a permission is granted the button will be green
			3. Denied permissions will be shown in red
			4. Example:  This user does not have admin access but is granted access to audits.   

	4. Access Summary
 
		- Access summary will allow the admin to see all passed and failed logins by all users.  Shown in Image II.4.1.

	5. Update knowledge base with Cosmic new releases

		- Download Cosmic TSV (https://cancer.sanger.ac.uk/cosmic/download) cosmic mutation data CosmicMutantExport.tsv.gz
		- Replace old file at /var/www/html/cosmic_tsvs
		- Decompress file by $ gunzip file.gz
 
- ## Version Information

	1. Version History

		- All users of MD Infotrack have access to version history of the software.  
		- To access the Version history page click on  .  Example shown in Image II.J1.1.

			1.	Shows all versions since the software started tracking version history on 2018-10-30.

		- Any changes that occurred in the last 7 days are shown on the homepage (Image II.J1.2).
		- Commit refers to the commit number in the software’s git version control. 

	2. Add Version
 
		- Only accessible with admin permissions
		- Update software code using git version control
		- Version Comment

			1.	Add a summary of what was updated in version Comment.
			2.	If applicable add person that reported a bug or requested the software change.  
			3.	Add bitbucket commit number to each version comment. 

		- Version Number (ex v2.1.67) (first.second.third)

			1.	First

				-	This would be a very large update to the software.  The last time this number was updated was when single analyte tests were added to the software.
				-	When updating this number reset second and third to 0

			2.	Second

				-	Medium size new functionality has been added to the software.
				-	When updating reset third to zero

			3.	Third

				-	Small updates and bug fixes

			4.	Most of the time just keep these number as default.  These categories are really up to the admin and are somewhat arbitrary. 

- ## Available Tests

	1. 1. All the tests available to request are located on the available tests page Image II.K.1.1.  Go to setup > tests.
 
	2. Column information: 

		- Test# - Auto incrementing number to keep track of tests.
		- Test Name
		- Full Name
		- Assay Type
		- Consent Required
		- Previous History Required
		- Mol Number Required (Required for NGS tests only)
		- Turnaround Time
		- Include in turn around calculation
		- Clock starts after another test finishes
		- Test status
		- User name

	3. Add Test

		- Refer to MD Infotrack ReadMe inside the source code.

- ##	Training

	1. More complicated NGS tasks are explained in further detail in the training section.
	2. Approved SOPs are also available here.	

- ## Bugs

	1. Access bug reporting page by clicking   on the tool bar.   
	2. Any software glitches or MD Infotrack Suggestions can be submitted to the admin users via this form.  This form will automatically email admins.

- ## Instruments

	1. Access

		- Instruments can be accessed under the setup menu -> instruments on the tool bar 

	2. Add and update instrument

		- Task and Manufacturer

			1.	When adding or updating an instrument whenever possible use a predefined task and manufacturer

		- Yellow Tag number started out as yellow tape with a hand written number.  Hence why it is called yellow tag number.  Now the number can be also written on the instrument in black Sharpe.  

	3. Service Contract

		- Service contract information on instruments can be tracked in MD Infotrack and MD Infotrack will notify users with manger level permissions when a service contract is 3 months from expiring.

	4. Validation

		- Validation documents of instruments can be scanned and uploaded to for safe keeping. 

	5. Issues – planned
	6. Repairs – planned
	7. Maintenance – planned

- ##	Soft Path

	1. Notification
	2. Download report
	3. Print NGS Report
	4. Scan Report
	5. Attach to Medical record

- ##	Sample Tests Page
 
	1. Access Sample Tests page (Image II.P.1) by clicking on    Image II.H.1
	2. Print DNA Extraction ordered Test sheet.   (Image II.P.1)
	3. Extract DNA (bench work)
	4. Change status of the test to complete for the DNA extraction using the button  
	5. After DNA extraction status is complete print any pending ordered test worksheets for this sample.  For example in Image II.P.1 the Ordered test sheet for PTG will only be printed after the DNA extraction was completed.  This is because the DNA extraction information will automatically be filled into the PTG sheet.  Example ordered test worksheets can be found in Section IX.A.
	6. Mark test Complete in MD Infotrack

		- Single Analyte Test

			1.	Change Test Status.    This will change the test to complete
			2. 	NGS Test
			
		-	Status of the test will be automatically changed to complete when the report is signed out by a director.

	7. Cancel a test and remove it from the pending list by clicking.   Currently this does not work for NGS tests.

# Sample login
 
- ## When a sample arrives to the MD lab, add the sample and requested test to the sample Log Book (Section II.H1.)
- ## On the tool bar click on log Book -> Add Sample to Log Book Image II.A.1. Log book permissions required.  Fill in sample log book form
- ## After submitting the sample log book form the patient will be added to the sample log book.  (Section II.H.1.) 
- ## Select add test next to the patient in the log book. For test select NGS Solid Tumor (Oncomine Focus Hotspot Assay).  Fill in the rest of the add test form and press submit.  This will add all of the information to the NGS reporting section of MD Infotrack and the sample log book.

# Add a test

## After adding a sample to MD Infotrack add either a single analyte test or an NGS test to the sample by clicking add test next to the sample in the sample log book. (Image II.H.1)
## Extra information required depending on test settings (Section II.K2)

	1. Consent Required

		- If consent required is turned on for the selected test the user will be asked to fill in the fields in Image IV.B1.1
		- If consent has not been received then a large No Consent (Section IX.A2)

	2. Previous history required

		- If previous history required is turned on for the selected test the user will be asked to fill in the fields in Image IV.B2.1
 
	3. Mol Number Required

		- If Mol number required is turned on for the test this test is an NGS test and a report will be generated by MD Infotrack.  Here is an overview of some of the extra fields required for NGS tests.

			1.	Sample Type

				-	Patient Sample

					1.	Use for all reported samples which are not CAP samples.

				-	CAP Sample

					1.	Use for CAP samples.

				- Validation
				
					1.	This will allow you to use MD Infotrack to Validate an NGS test and will change the mol# to start with 00

				- Sent out
				
					1.	Only use if URMC will not be completing this test (Section VII.B3). 

			2.	Primary Site
			
				- Describe the original, or first, tumor in the body.  Cancer cells from a primary tumor may spread to other parts of the body and form new, or secondary, tumors.
				- This is a restricted list.  To add more items to this list ask Admin
			
			3.	Tumor Type

				- Tumor Disease type
				- This is a restricted list.  To add more items to this list ask Admin

			4.	Tissue Tested

				- When the lab received it what type of tissue did we receive.  For example blood.

	4. Make sure the first test added is DNA extraction if it is necessary.  

# Single Analyte tests

## Analysis Flow

	1. Add sample (Section III)
	2. Add DNA extraction test if necessary to sample (Section IV)
	3. Complete DNA extraction test (Section IV)
	4. Add test to sample (Section IV)
	5. Print ordered Test sheet.   (Image II.P.1) 
	6. Update status of test to complete ASAP after test is reported out. (Section II.P)

# Oncomine Focus Assay (OFA) software steps and Specific Test Settings.
 
##	Wet bench OFA SOP

	1. Procedure Manual for tumor gene mutation analysis by oncomine focus hotspot assay

## Additional software/hardware necessary to complete the OFA steps

	1. Ion Torrent S5XL

		- Refer to the wet bench OFA SOP for more information about this instrument
		- Instrument information

			1.	IP – 192.168.201.1
			2.	Model – S5XL
			3.	Instrument Serial # – 2772816100108

	2. Chef

		- Instrument information

			1.	IP – 192.168.203.1

	3. DataSafe

		- Instrument information

			1.	IP – 172.18.126.41
			2.	Name – torrenttnas
			3.	Dell Service Tag – 8BXMMD2
			4.	Model – ????
			5.	Location – ????
			6.	Alias – IONds
			7.	MAC – 00:07:b4:00:01:01
			8.	Access Website – http://172.18.126.41:8457

	4. Torrent Server

		- Purpose

			1.	Plan and monitor sequencing runs 
			2.	Calls bases, demultiplexes, and maps to the human genome
			3.	Provides general run metrics of raw mapped sequencing data and coverage analysis.

		- Instrument information

			1.	IP – 10.149.58.208
			2.	Name – H28TPD2
			3.	Dell Service Tag – H28TPD2
			4.	Model – T630
			5.	Location – Bailey Road 
			6.	Alias – ions5
			7.	MAC – 18:66:da:63:7c:50
			8.	Access Website -  https://10.149.58.208/home/

		- Settings used 

			1.	Run Plan - Oncomine_Focus_DNA_for_S5_08062019

				- A run planned or planned run transfers data automatically to the appropriate Ion Reporter Server and uses the linked workflow to perform the analysis.
				- Find our settings for the workflow in the Wet bench OFA SOP Section 12.D.II (create a Run Plan – Ion Torrent Browser)
				- After logging into the torrent server through documentation of the torrent server can be found under the help menu in the upper right hand coroner of the webpage.

	5. Ion Reporter 

		- Purpose: The Ion Reporter is a suite of bioinformatics tools to streamline and simplify the data analysis, annotation, and filtering of variants.
		- Instrument information

			1.	IP – 10.149.59.209
			2.	Name – 2F65182
			3.	Dell Service Tag – 2F65182
			4.	Model – T630
			5.	Location – Bailey Road 
			6.	Alias – ionir
			7.	MAC – 14:18:77:2d:09:76
			8.	Access website - http://10.149.59.209/ir/login.html

		- Settings used
		
			1. Workflow – OFA_5-10_complex_v3_filter_updated

				-	Analysis workflows in the Ion Reporter are sets of instructions that determine how analysis results are produced. 
				-	Find our settings for the workflow in the Wet bench OFA SOP Appendix 22.C.

			2.	Filter – OFAv190712_5-10

		- Find our settings for the workflow in the Wet bench OFA SOP Appendix 22.C.IV  

		- After logging into the Ion Reporter through documentation of the torrent server can be found under the help menu in the upper right hand coroner of the webpage.

	6. ThermoFisher Contacts for help with the above instruments and OFA assay

		- John Zhang – Bioinformatics (John.Zhang@thermofisher.com)
		- Ricardo Costa – Bench Help (RicardoDallaCosta@thermofisher.com)
		- Matthew Goodman – Local Repair Tech (Matt.Goodman@thermofisher.com)
		- Instrument service call – 1-800-955-6288

## OFA Specific Pre NGS Steps

	1. Add all samples as they arrive (Section III)
	2. Add DNA extraction test if necessary to sample (Section IV)
	3. Complete DNA extraction test (Section IV)
	4. Add NGS test to sample (Section IV)
	5. Print ordered Test sheet.   (Image II.P.1) 
	6. Add Control Samples (Section VIII.D)
	7. Each run will have a Positive/sensitivity control and a Negative control (Section IX.D).
	8. Make NGS Library Pool.  IMPORTANT DO THIS STEP BEFORE ANY BENCH WORK HAS BEEN STARTED!!!!!

		- NGS -> Add Library NGS pool 
		- Choose Oncomine Focus Hotspot Assay.  Click Submit.
		- Choose the number of Chips in the pool.  Click Submit.
		- Next you will be setting up how the chips are organized.  It is best to keep the samples in the order that they are automatically added.  For this reason complete the Pool set up before any bench work has started.

			1.	Use the include checkbox to add and remove samples from the pool.
			2.	Once the Pool is created it will be found in the Pre NGS Library Pools table on the homepage. 

	9. Complete Pre NGS steps via the Library Pool

		- Click on pool in pre NGS Library Pools table on the homepage.
		- Library Pool Steps
 
			1.	DNA Concentration (Wet bench OFA SOP 12.A2.1, 12.A2.2) 

				- Add the concentration of the quantus control.
				- Add all of the DNA concentrations for each sample.

			2.	Amplicon Concentration (Wet bench OFA SOP 12.C.VII, Table 12C.3, 12D.1)

				- Add all of the Amplicion concentrations for each sample

			3.	Add Barcode

				- For each sample.  MD Infotrack will default add the barcodes.  It is best to not edit if possible.  This will reduce the possibility of errors.

			4.	Pool Download Run Template

				- When do I perform this step related to wet bench protocol?

					1.	Complete this step after library preparation (Wet bench OFA SOP 12B) and quantitation (Wet bench OFA SOP 12C) but before chip loading (Wet bench OFA SOP 12D) and sequencing (Wet bench OFA SOP 12E).

				- Download Run template Chip A and B (Green buttons at top of page 
				- Go to Torrent Server
				- Click on the plan Tab.  Then click on Oncomine_Focus_DNA_for_S5_08062019
				- Change Run Plan Name.  Use the default name and add the date (YYMMDD) and chip.  DO NOT ADD ANY SPACES to the name.  Image VIC9.d.v.1 is an example of how to create a plan.  The default name is highlighted in pink and the added name is highlighted in blue.    
				- After the run plan name is updated click load samples table.  (Image VIC9.d.v.1)
				- Upload Template Chip A downloaded in step 1.
				- Then repeat steps iii - vi for Template Chip B.
				
			5.	Once your plans are set up in Torrent Server and you will no need access the library pool in MD Infotrack the done button can be pressed at the bottom of the Pool Download Run Template page.  This will remove the library pool from the Pre NGS Library Pools table on the homepage.
	
	10. Upload Variant TSV and Coverage File. 

		- When do I perform this step related to wet bench protocol?

			1.	Complete this step after library preparation (Wet bench OFA SOP 12B), quantitation (Wet bench OFA SOP 12C), chip loading (Wet bench OFA SOP 12D), and sequencing (Wet bench OFA SOP 12E).

				- Confirm data transfer from Torrent Server to Ion Reporter (Ion Reporter, Torrent Server)

					1.	You will know that this occurred because your new runs will be present in the Ion Reporter.
					2.	Manually transfer data if the automatic transfer did not occur

						- Open the Torrent Server.
						- Go to Data -> Complete Runs & Reports 
						- Open run which did not transfer.
						- Click upload to IR button.
 
					3. Here are the reasons why we have encountered a problem with the automatic transfer of data from the Torrent Server to the Ion Reporter:

						- The Ion Reporter Account is no longer selected in the run template.
						- Sometimes the workflow becomes untagged for IRU in Ion Reporter.  I’m unsure how this happens.  

							1. Fix by going to Ion Reporter -> Workflows -> Check box and Tag for IRU 
 
						- Confirm that the QC metrics fall within specifications (Torrent Server)

							1.	Go to Torrent Server -> Data
							2.	Completed Runs & Reports -> Click on desired report by clicking on the hypertext run name
							3.	Confirm that the QC metrics fall within the recommended specification listed in in Table IXE.1
							4.	If the run Metrics do not meet the acceptable ranges outlined in Table IXE.11 notify director before proceeding further.  Metrics outside of the acceptable ranges may be acceptable as long as coverage and base quality for each sample are acceptable and results for the control meet the assay specifications.
							5.	The OFA_5-10_complex_v3_filter_updated workflow and filter chain has been set so that that the quality of base scoring meets a minimum of Q20 or equivalent per base. The number of bases meeting this requirement for each sample can be viewed on the Torrent Server under the Data tab for each sample 

						- Download amplicon coverage file (Torrent Server)

							1.	Go to the Torrent Server
							2.	Click on Data and Completed Runs & Reports
							3.	Click on the run name hyperlink to open the report
							4.	Scroll down to the Coverage Analysis and click on the barcode name. Image VI.C10.iv.d.1
							5.	Scroll down to the bottom of the page and click on download the amplicon coverage summary file. Image VI.C10.iv.e.1
 
						- Download filtered variant tsv file (Ion Reporter)

							1.	Open Ion Reporter
							2.	Click on the Analyses Tab and check to make sure that the workflow is OFA_5-10_complex_v3_filter_updated.
							3.	Select a sample and make sure the filter is set to OFAv190712_5-10 (5.10)
							4.	Click on download -> Current Results TSV. This will get the TSV ready to download.
							5.	Click Home -> Notifications -> download the TSV here.

						- Upload variant TSV and coverage file for each sample to MD Infotrack

							1.	Login to MD Infotrack
							2.	In the Pre NGS Samples table on the homepage click on the last yellow dot for each sample (Image VI.C10.vi.b.1).  This will open up a page to upload the coverage and variant TSV file.
							3.	Locate the variant TSV and coverage file for the current sample and upload them.  
							4.	The upload process might take a little bit of time if there are many variants (For instance positive control).  Please be patient.
							5.	After uploading files a confirmation page will appear asking the user to confirm that everything uploaded correctly.  Once you have reviewed the entire page scroll down to the bottom of the page and press continue.
							6.	Automatic checks performed by MD Infotrack to ensure accuracy and integrity of uploaded files.

								- Variant TSV file

									1.	2 identifiers are checked from sample name molecular number and MRN number.
									2.	Validation of complete file transfer is confirmed via a MD5 checksum hash.
									3.	Format of the file is checked.
									4.	Workflow and filter set are confirmed.

								- Amplicon Coverage Summary file 

									1.	Barcode of sample is checked against barcode entered in plan run template
									2.	Validation of complete file transfer is confirmed via a MD5 checksum hash.
									3.	Format of the file is checked.

## OFA specific controls 

1. Acceptable criteria for controls

	- Acrometrix Oncology Hotspot Control (Positive/sensitivity control): as mentioned in the New York State NGS guidelines for somatic variant detection (published in Jan 2018), the error rate for observed variants should be <2% to pass the run. The acceptable range of VAFs for observed variants should meet the following criteria.

		1.	For Variants with expected VAFs between 5-15%, the observed VAFs should not be greater than 40%.
		2.	For variants with expected VAFs between 15-35%, the observed VAFs should not be greater than 50% or lower than 5%.
		3.	See Section IVD for a table of all of the acceptable criteria for the AcroMetrix Oncology Hotspot Control.

	- If Positive/sensitivity control is a pooled patient specimen is used, the observed VAFs should be the expected VAFs±20%.

	- Negative Control should be negative for detectable variants.  

	- If any controls do not meet expected conditions contact the director for further investigation and instructions.  The specimen/run may need to be repeated. 

2. The minimum criteria for reporting percentage of variant reads in a background of normal reads is 5% VAF and greater.  IF there variants in the query below 5%, contact the Director for further investigation and instructions.  The specimen may need to be repeated.  All low variant alleles will also be documented in the report by the director.  Any variants of interest below 5% must be confirmed by an orthogonal method before it can be called “detected”.

## OFA Specific Reporting Flow

1. Access QC of each Variant (MD InfoTrack)

2. 

3. 

4. Access QC of each variant (MD Infotrack)


## Specific Test Settings
	
# Myeloid software steps

# General NGS Steps

## NGS Section Access (Section II.D6)
## NGS homepage overview

	1. The samples show on the NGS home page can be toggle using the buttons pictured in Image IVB1.
	2. Pre NGS table send out not pressed (Image VIII.B1.1. Lower left hand corner)

		- Send out button will be yellow as shown in Image VII.B1.1.

			1. The samples waiting for sequencing are found in this table.
			2. The color coded buttons (Image VIII.B2.i.b.1.) will show you the progress of the test. Refer to Image II.E1.1. for more information about the colors. Press on the button to access more information about the step.
			3. Buttons are not accessible with view only permissions. (Section II.D12)
			4. Samples can be added to this table by adding a NGS test to the sample in the log book. (Section IV)

	3. Pre NGS table send out pressed (Image VIII.B2.i.b.1. Lower left hand corner)

		- Send out button will be green
		- The only purpose of this option is to keep track of send outs.  While the buttons shown in Image VIII.B2.i.b.1. are present do not complete these steps. 
		- Click on All samples in the Toggle Samples which appear by NGS panel section (Image VIII.B1.1) to visualize non send out samples again.
		- Send out samples can be added to this table by adding a NGS test to the sample in the log book and choosing Sample type Send out.  (Section IV)

	4. Pre NGS Library Pools table

		- All of the pending pools can be accessed from this table.  
		- Table not visible in view only permissions. (Section II.D12)

	5. Outstanding Reports table

		- All samples which have completed sequencing but are waiting for a report be completed can be found here
		- Hover over a sample row in this table if it turns green this indicates you can click on the row.  (Not available for view only permissions Section II.D12)  After clicking on the row it will show the NGS report building section of MD Infotrack. (Section VIII.G) 

	6. Completed Reports Table
 
		- This table contains all of the reports which have been approved and signed out by a director.
		- The green pdf button is a link to the report. (Image VIII.B6.1)
		- Use the revise button to change a previously completed report. (Image VIII.B6.1) The blue Revise button is only visible with revise permissions (Section II.D9)

## Overview of Pre NGS Flow

	1. Add all samples as they arrive (Section III)
	2. Add DNA extraction test if necessary to sample (Section IV)
	3. Complete DNA extraction test (Section IV) 
	4. Add NGS test to sample (Section IV)
	5. Add Control Samples (Section VIII.D)
	6. Add NGS Library Pool (Section VIII.E)
	7. Upload Variant TSV and Coverage File. (refer to test specific section)

## NGS Controls

	1. Add Control

		- NGS Controls are added via NGS -> Add Control
		- Choose if Positive or Negative Control
		- If your control is not listed in the drop down menu please notify Admin to add a new control.  

	2. Report out control

		- It is recommended to review all controls before proceeding with sample reports. 
		- Technician responsibilities

			1.	Add all control variant tsvs and coverage files 
			2.	Notify directors that the control is ready for them to review
 
		- Director responsibilities

			1.	After reviewing the control MD Infotrack will provide a recommendation on if the control passed or failed however, it is left completely up to the Director if the control is passed or failed. Click either   to remove the control from the outstanding reports queue.  Currently a failed control will not be noted in the sample report or reporting section.  For failed controls all of the pending reports will need to be stopped from reporting. 
			2.	MD Infotrack recommendation 

				- Depending on the number of tracked variants in the positive control sometimes a few errors are allowed while keeping the error rate still below the NYS NGS guidelines of 2%.
 
			3.	Basis of MD Infotrack recommendation for Positive control

				- Variants not found
				- Number of variants with VAF’s outside of expected range
				- Expected number of variants
				- Observed Number of variants

					1.	Does not bear any weight on Positive control pass/fail recommendation

				- Total number of unexpected variants 

					1.	Does not bear any weight on Positive control pass/fail recommendation
					
			4. Basis of MD Infotrack recommendation for Negative control
			
				- Variants not found
				
					1.	Sometimes negative controls do contain artifact variants.  This is why Negative controls can have tracked variants however, it does not bear any weight on Negative control pass/fail recommendation
					
				- Number of variants with VAF’s outside of expected range

					1.	Does not bear any weight on Negative control pass/fail recommendation

				- Expected number of variants

					1.	Does not bear any weight on Negative control pass/fail recommendation
				
				- Observed Number of variants 

					1.	Does not bear any weight on Negative control pass/fail recommendation
					
				- Total number of unexpected variants 

	3. Adjust Control
	
		-With director level permissions expected VAF ranges can be updated.  To update the range:
		
			
			1.	Open a reporting section of a control still in the samples with outstanding reports table on the homepage.  
			2.	Click on review control in the report step track bar 
			3.	Click on the   next to the variant you would like to update
			4.	This will provide the director with statics on this variant for the control and a form to adjust the min and max of the range
 
		- It is planned to add to director level permissions the ability to add a variant to the list of tracked variants. However, the current buttons   in the control reporting section do not work.  Notify admin to add/delete variants

	4. Historic Control Statistics

		- Access Historic Control Statistics from the NGS menu -> Search Historic Control Stats
 
			1. Contents of historic control Statistics

			a.	Information on how to make or order the control again. Here’s an example:
 
b.	Observed Statics of control.  Here’s an example:
 

VIII.E.	NGS Pool
VIII.E1. It is very IMPORTANT to start making a library pool before starting any bench work!!!
VIII.E2. Library Pools are used to set up and organize your samples for the flow cell or chips
VIII.E3. Benefits of Making a Library Pool
i. A library pool consists of all the samples on a flow cell for the myeloid panel or on both chips for the OFA panel.
ii. Batch add DNA quantification.
iii. Batch add indexes
iv. Built in Dilution calculations (Myeloid Panel only)
v. Batch add Amplicon concentration
vi. Make instrument run templates which reduces typing errors and increases speed.
VIII.E4. Refer to NGS test section for more detailed instructions on NGS Pool.
VIII.F.	Low Coverage areas
VIII.F1. What is an Amplicon?
i. A piece of DNA which is the product of PCR amplification.  
ii. The URMC molecular diagnostics laboratory uses amplicon NGS sequencing.  Amplicon sequencing is a highly targeted approach to analyze genetic variation in specific genomic regions.  The ultra-deep sequencing of PCR products (amplicons) allows efficient variant identification and characterization.  This method uses oligonucleotide probes designed to target and capture regions of the interest, followed by next-generation sequencing (NGS).
VIII.F2. What is Amplicon Coverage?
 
VIII.F3. After uploading the corresponding variant tsv and coverage files for the specific NGS assay a low-coverage table will be made and automatically added to the final report. 
VIII.F4. The minimum criteria for depth and uniformity of coverage is 500 reads or greater.  If there are amplicons below 500x they will be added to the low coverage table.  
VIII.F5. Any variants of interest in regions with read depth below 500X must be confirmed by an orthogonal method.
VIII.F6. Technologist’s Low Coverage Responsibilities
i. Inform the director for further investigation and instructions via the report message board (Section VII.G3)
VIII.F7. Director’s Low Coverage Responsibilities
i. Director will determine if there’s a problem with the run and if the specimen needs to be repeated.  
VIII.F8. Example Low coverage table in NGS report
 
VIII.F9. Example No Low Coverage found in NGS report
 
VIII.G.	NGS Reporting
VIII.G1. Report step tracker bar
i. The report step tracker bar is present on the left of every report building step.   The main purposes of this bar are:
a.	Notifies the user of completed steps via color coding the steps. (Section II.E) 
b.	Provides buttons to switch between steps of the report
c.	Automatically changes required steps depending on sample type
ii. There are 3 versions of the report step tracker bar depending on sample type. Image VII.G1.i.1 shows the varying versions
 
Image VII.G1.1 Refer to Section VIIG2 for more information
VIII.G2. What is (C, SwM, S)
i. MD Infotrack will automatically assign steps during report building however, (C, SwM, S) will help you follow this guide and see which step is appropriate for which sample type.
ii. C – Control Samples
iii. SwM – Sample without reportable mutations
iv. S – Sample with reportable mutations 
VIII.G3. Message Board – (C, SwM, S)
 
Image VIII.G3.1 An example image of the message board being used.
i. The message board is used to notify colleagues of anything found about the sample.
ii. The message board has the appearance of a text messaging app.  It allows any user with director or reviewer level permissions to send emails to each other and leave messages inside the board.
iii. How to access the message board?
a.	Click on the   in the report step tracker bar.
b.	The message board is sometimes available inside some of the steps if you scroll to the bottom of the page.
VIII.G4. Summary – (C, SwM, S)
i. After clicking on a sample in the outstanding reports table the summary page of the report building section will open.
ii. The summary page shows how the final report currently looks.
VIII.G5. Stop Reporting – (C, SwM, S)
i. Provides a way to remove the report from the outstanding reports table without signing it out.
ii. Previously this section has been useful for sample repeats where the reports will not be completed, if controls failed, or if the patient dies before the report was completed.
VIII.G6. QC Run  – (SwM, S)
i. This page will ask the user to confirm the QC metrics for the entire run have meet acceptable ranges determined by the NGS assay.  Refer to the assay QC Run section for more test specific information.
ii. This is a required step to proceed past add patient step.
VIII.G7. QC Variant – (SwM, S)
VIII.G8. Add Visit– (SwM, S)
VIII.G9. Add Patient – (SwM, S)
VIII.G10. Verify Variants– (SwM, S)
VIII.G11. Add Gene Interpretation – (SwM)
VIII.G12. Add Variant Interpretation– (SwM)
VIII.G13. Add Tiers – (SwM)
VIII.G14. Add Summary – (SwM, S)
VIII.G15. Review Report – (SwM, S)
VIII.G16. Download Report – (SwM, S)
VIII.H.	Report Types
VIII.H1. Draft
VIII.H2. Completed
VIII.H3. Revised
VIII.I.	NGS Knowledge Base and Searches
IX.	Appendix
IX.A.	Example Ordered Test Work Sheet
IX.A1. DNA extraction work sheet
 
IX.A2. No Consent ordered test work sheet
 
IX.A3. Consent obtained work sheet
 
IX.B.	Example NGS Report
IX.B1. Draft
IX.B2. Completed
IX.B3. Revised

IX.C.	Performance Audit Example Report
 
 
IX.D.	Table IXD.1 Acceptable criteria for Acrometrix Oncology Hotspot Control DNA for the NGS Oncomine Focus Assay
Gene	Coding	Protein	Acrometrix Standard Target Frequency	Accepted Min Frequency	Accepted Max Frequency
AKT1	c.49G>A	p.Glu17Lys	5-15%	5	40
ALK	c.3522C>A	p.Phe1174Leu	5-15%	5	40
ALK	c.3824G>A	p.Arg1275Gln	5-15%	5	40
BRAF	c.1391G>T	p.Gly464Val	15-35%	5	50
BRAF	c.1742A>G	p.Asn581Ser	15-35%	5	50
BRAF	c.1781A>G	p.Asp594Gly	15-35%	5	50
BRAF	c.1790T>G	p.Leu597Arg	15-35%	5	50
BRAF	c.1799T>A	p.Val600Glu	15-35%	5	50
CTNNB1	c.110C>T	p.Ser37Phe	5-15%	5	40
CTNNB1	c.121A>G	p.Thr41Ala	5-15%	5	40
CTNNB1	c.134C>T	p.Ser45Phe	5-15%	5	40
CTNNB1	c.98C>G	p.Ser33Cys	5-15%	5	40
EGFR	c.1793G>T	p.Gly598Val	15-35%	5	50
EGFR	c.2092G>A	p.Ala698Thr	15-35%	5	50
EGFR	c.2156G>C	p.Gly719Ala	15-35%	5	50
EGFR	c.2170G>A	p.Gly724Ser	15-35%	5	50
EGFR	c.2197C>T	p.Pro733Ser	15-35%	5	50
EGFR	c.2203G>A	p.Gly735Ser	15-35%	5	50
EGFR	c.2222C>T	p.Pro741Leu	15-35%	5	50
EGFR	c.2235_2249delGGAATTAAGAGAAGC	p.Glu746_Ala750del	15-35%	5	50
EGFR	c.2293G>A	p.Val765Met	15-35%	5	50
EGFR	c.2375T>C	p.Leu792Pro	15-35%	5	50
EGFR	c.2485G>A	p.Glu829Lys	15-35%	5	50
EGFR	c.2497T>G	p.Leu833Val	15-35%	5	50
EGFR	c.2504A>T	p.His835Leu	15-35%	5	50
EGFR	c.2515G>A	p.Ala839Thr	15-35%	5	50
EGFR	c.2573T>G	p.Leu858Arg	15-35%	5	50
EGFR	c.2582T>A	p.Leu861Gln	15-35%	5	50
EGFR	c.2588G>A	p.Gly863Asp	15-35%	5	50
EGFR	c.323G>A	p.Arg108Lys	15-35%	5	50
EGFR	c.340G>A	p.Glu114Lys	15-35%	5	50
EGFR	c.866C>T	p.Ala289Val	15-35%	5	50
EGFR	c.874G>T	p.Val292Leu	15-35%	5	50
ERBB2	c.2264T>C	p.Leu755Ser	5-15%	5	40
ERBB2	c.2305G>T	p.Asp769Tyr	5-15%	5	40
ERBB2	c.2324_2325insATACGTGATGGC	p.Glu770_Ala771insAlaTyrValMet	5-15%	5	40
ERBB2	c.2524G>A	p.Val842Ile	5-15%	5	40
ERBB2	c.2570A>G	p.Asn857Ser	5-15%	5	40
ERBB2	c.2632C>T	p.His878Tyr	5-15%	5	40
FGFR2	c.1108A>G	p.Thr370Ala	5-15%	5	40
FGFR2	c.1124A>G	p.Tyr375Cys	5-15%	5	40
FGFR2	c.1144T>C	p.Cys382Arg	5-15%	5	40
FGFR2	c.1647T>A	p.Asn549Lys	5-15%	5	40
FGFR2	c.755C>G	p.Ser252Trp	5-15%	5	40
FGFR3	c.1108G>T	p.Gly370Cys	5-15%	5	40
FGFR3	c.1138G>A	p.Gly380Arg	5-15%	5	40
FGFR3	c.1150T>C	p.Phe384Leu	5-15%	5	40
FGFR3	c.1172C>A	p.Ala391Glu	5-15%	5	40
FGFR3	c.1928A>G	p.His643Arg	5-15%	5	40
FGFR3	c.1948A>G	p.Lys650Glu	5-15%	5	40
FGFR3	c.2089G>T	p.Gly697Cys	5-15%	5	40
FGFR3	c.746C>G	p.Ser249Cys	5-15%	5	40
GNA11	c.547C>T	p.Arg183Cys	5-15%	5	40
GNA11	c.626A>T	p.Gln209Leu	5-15%	5	40
GNAQ	c.523A>T	p.Thr175Ser	15-35%	5	50
GNAQ	c.548G>A	p.Arg183Gln	15-35%	5	50
GNAQ	c.680_682delTGTinsGCA	p.Met227_Phe228delinsSerIle	15-35%	5	50
HRAS	c.175G>A	p.Ala59Thr	5-15%	5	40
HRAS	c.182A>G	p.Gln61Arg	5-15%	5	40
HRAS	c.35G>T	p.Gly12Val	5-15%	5	40
IDH1	c.367G>A	p.Gly123Arg	5-15%	5	40
IDH1	c.388A>G	p.Ile130Val	5-15%	5	40
IDH1	c.395G>A	p.Arg132His	5-15%	5	40
IDH2	c.419G>A	p.Arg140Gln	5-15%	5	40
IDH2	c.515G>A	p.Arg172Lys	5-15%	5	40
JAK2	c.1849G>T	p.Val617Phe	15-35%	5	50
JAK2	c.1860C>A	p.Asp620Glu	15-35%	5	50
KIT	c.1509_1510insGCCTAT	p.Ser501_Ala502insAlaTyr	5-15%	5	40
KIT	c.1516T>C	p.Phe506Leu	5-15%	5	40
KIT	c.1526A>T	p.Lys509Ile	5-15%	5	40
KIT	c.1535A>G	p.Asn512Ser	5-15%	5	40
KIT	c.1588G>A	p.Val530Ile	5-15%	5	40
KIT	c.1924A>G	p.Lys642Glu	5-15%	5	40
KIT	c.1961T>C	p.Val654Ala	5-15%	5	40
KIT	c.2410C>T	p.Arg804Trp	5-15%	5	40
KRAS	c.104C>T	p.Thr35Ile	5-15%	5	40
KRAS	c.175G>A	p.Ala59Thr	5-15%	5	40
KRAS	c.183A>C	p.Gln61His	5-15%	5	40
KRAS	c.351A>C	p.Lys117Asn	5-15%	5	40
KRAS	c.35G>A	p.Gly12Asp	5-15%	5	40
MAP2K1	c.171G>T	p.Lys57Asn	5-15%	5	40
MAP2K1	c.199G>A	p.Asp67Asn	5-15%	5	40
MET	c.3370C>G	p.His1124Asp	15-35%	5	50
MET	c.3757T>G	p.Tyr1253Asp	15-35%	5	50
MET	c.3778G>T	p.Gly1260Cys	15-35%	5	50
MET	c.3785A>G	p.Lys1262Arg	15-35%	5	50
MET	c.3803T>C	p.Met1268Thr	15-35%	5	50
NRAS	c.182A>G	p.Gln61Arg	5-15%	5	40
NRAS	c.29G>A	p.Gly10Glu	5-15%	5	40
NRAS	c.35G>A	p.Gly12Asp	5-15%	5	40
NRAS	c.52G>A	p.Ala18Thr	5-15%	5	40
PDGFRA	c.1977C>A	p.Asn659Lys	5-15%	5	40
PDGFRA	c.2525A>T	p.Asp842Val	5-15%	5	40
PDGFRA	c.2544C>A	p.Asn848Lys	5-15%	5	40
PIK3CA	c.1035T>A	p.Asn345Lys	5-15%	5	40
PIK3CA	c.1258T>C	p.Cys420Arg	5-15%	5	40
PIK3CA	c.1370A>G	p.Asn457Ser	5-15%	5	40
PIK3CA	c.1616C>G	p.Pro539Arg	5-15%	5	40
PIK3CA	c.1624G>A	p.Glu542Lys	5-15%	5	40
PIK3CA	c.1633G>A	p.Glu545Lys	5-15%	5	40
PIK3CA	c.1640A>G	p.Glu547Gly	5-15%	5	40
PIK3CA	c.2102A>C	p.His701Pro	5-15%	5	40
PIK3CA	c.2702G>T	p.Cys901Phe	5-15%	5	40
PIK3CA	c.2725T>C	p.Phe909Leu	5-15%	5	40
PIK3CA	c.3110A>G	p.Glu1037Gly	5-15%	5	40
PIK3CA	c.3140A>G	p.His1047Arg	5-15%	5	40
PIK3CA	c.93A>G	p.Ile31Met	5-15%	5	40
PIK3CA	c.971C>T	p.Thr324Ile	5-15%	5	40
RET	c.1852T>C	p.Cys618Arg	15-35%	5	50
RET	c.1858T>C	p.Cys620Arg	15-35%	5	50
RET	c.1894_1906delGAGCTGTGCCGCAinsAGCT	p.Glu632_Thr636delinsSerSer	15-35%	5	50
RET	c.1942G>A	p.Val648Ile	15-35%	5	50
RET	c.2304G>C	p.Glu768Asp	15-35%	5	50
RET	c.2647_2648delGCinsTT	p.Ala883Phe	15-35%	5	50
RET	c.2701G>A	p.Glu901Lys	15-35%	5	50
RET	c.2753T>C	p.Met918Thr	15-35%	5	50
SMO	c.1234C>T	p.Leu412Phe	15-35%	5	50
SMO	c.1604G>T	p.Trp535Leu	15-35%	5	50

IX.E.	Table IXE.1 QC Metrics – Table of Acceptable Run Metric Ranges for the Oncomine Focus Assay 530 chip
IX.E1. ISP loading – Percent of microcell wells loaded with an Ion Sphere Particle
IX.E2. Clonal – Amplification of a single DNA template per microcell well.
IX.E3. Polyclonal – Simultaneous amplification of multiple DNA templates per microcell well. 

Metric	Range
Unaligned Reads > ISP Summary
Total Reads	10-22 million
Usable Reads	> 30%
ISP loading	> 60%
Enrichment	> 98%
Clonal	> 50%
Polyclonal	30-45% or less if Quality < 10%
Final Library	> 40%
Test Fragment	≤ 3%
Adapter Dimer	< 10%
Low Quality	< 30%
Unaligned Reads > Read Length
Median Read length	100-130 bp
Aligned Reads > Mean Raw Accuracy 1x
Mean Raw Accuracy 1x	≥ 99%













































