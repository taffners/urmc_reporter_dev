<?php

	class DataBase 
	{
		private $conn;
		private $ip;
		private $ip_loc;

		// cryptor methods
		private $cipher = 'AES-128-CBC'; // advanced encryption standard
		private $cryp_key;
	
		public function __construct($conn, $cryp_key)
		{
			$this->conn = $conn;
			$this->cryp_key = $cryp_key;
		}

		public function SetIP()
		{

			if(isset($_SERVER['HTTP_CLIENT_IP']))
			{
			// This will fetch the IP address when user is from Shared Internet services.
				$this->ip = $_SERVER['HTTP_CLIENT_IP'];
			}

			elseif (isset($_SERVER['REMOTE_ADDR']))
			{			
				// This contains the real IP address of the client. That is the most reliable value you can find from the user.
				$this->ip = $_SERVER['REMOTE_ADDR'];
			}

			else
			{
				$this->ip = 'unknown';
			}	

			if ($this->ip == '::1')
			{
				$this->ip = '128.151.71.16';
			}	

			// get location of ip 
			$this->ip_loc = $this->IPInfo($this->ip);
			return $this->ip_loc;
			// if not in the Rochester, if unknown, if format not ip == logoff
		}

		private function IPInfo($ip, $purpose = "location") 
		{
			$output = NULL;

			$purpose = str_replace(array("name", "\n", "\t", " ", "-", "_"), NULL, strtolower(trim($purpose)));
			$support    = array("country", "countrycode", "state", "region", "city", "location", "address");
			$continents = array(
				"AF" => "Africa",
				"AN" => "Antarctica",
				"AS" => "Asia",
				"EU" => "Europe",
				"OC" => "Australia (Oceania)",
				"NA" => "North America",
				"SA" => "South America"
			);
var_dump(filter_var($ip, FILTER_VALIDATE_IP));
			if (filter_var($ip, FILTER_VALIDATE_IP) && in_array($purpose, $support)) 
			{
				// 
				$ipdat = @json_decode(file_get_contents("http://www.geoplugin.net/json.gp?ip=" . $this->ip));
				if (@strlen(trim($ipdat->geoplugin_countryCode)) == 2) 
				{
					switch ($purpose) 
					{
						case "location":
							$output = array(
							    "city"           => @$ipdat->geoplugin_city,
							    "state"          => @$ipdat->geoplugin_regionName,
							    "country"        => @$ipdat->geoplugin_countryName,
							    "country_code"   => @$ipdat->geoplugin_countryCode,
							    "continent"      => @$continents[strtoupper($ipdat->geoplugin_continentCode)],
							    "continent_code" => @$ipdat->geoplugin_continentCode,
							    "ip" 			 => $ip
							);
						break;

						case "address":
							$address = array($ipdat->geoplugin_countryName);
							if (@strlen($ipdat->geoplugin_regionName) >= 1)
							{
								$address[] = $ipdat->geoplugin_regionName;
							}
		    
							if (@strlen($ipdat->geoplugin_city) >= 1)
							{
								$address[] = $ipdat->geoplugin_city;
							}

							$output = implode(", ", array_reverse($address));
							break;
		
						case "city":
							$output = @$ipdat->geoplugin_city;
							break;
						
						case "state":
							$output = @$ipdat->geoplugin_regionName;
							break;
						
						case "region":
							$output = @$ipdat->geoplugin_regionName;
							break;
		
						case "country":
							$output = @$ipdat->geoplugin_countryName;
							break;
		
						case "countrycode":
							$output = @$ipdat->geoplugin_countryCode;
							break;
					}
				}
			}
		
			return $output;
		}

		private function AddLog($action, $msg, $details)
		{
			// Every action to the database needs to add a log to log_table
			// possible actions: 

			// What to log:
				// IP Address
				// user id
				// action (login, listAll, addOrModifyRecord, Delete)
				// msg
				// time_stamp

			if (defined('USER_ID'))
			{
				$user_id = $this->sanitize(USER_ID);				
			}	
			else
			{
				$user_id = 0;
			}		
			
			$log_insert = 'INSERT INTO log_table (user_id, ip_address, action, msg) VALUES';
			$log_insert .= '("'.$user_id.'", ';
			$log_insert .= '"'.isset($this->ip) ? $this->sanitize($this->ip): ''.'", ';
			$log_insert .= '"'.$this->sanitize($action).'", ';
			$log_insert .= '"'.$this->sanitize($msg).'")';
			$result = mysqli_query($this->conn, $log_insert);
			// get the Id of the inserted record
			$lastId = mysqli_insert_id($this->conn);
			// listAll, addOrModifyRecord, Delete will have more info in the detailed_log_table
			// make sure $details is not null
			if ($details !== NULL)
			{
				if (!is_string($details) && !is_int($details))
				{
					// convert $where to string
					$details = implode(',', $details);					
				}

				$details_insert = 'INSERT INTO detailed_log_table (details, log_id) VALUES';
				$details_insert .= '("'.$this->sanitize($details).'", ';
				$details_insert .= '"'.$this->sanitize($lastId).'") ';
				$result = mysqli_query($this->conn, $details_insert);
			}

			// see if this ip address has visited before
			$ip_exist = $this->listAll('ip-info-exist',$this->ip);

			// if the ip address has not visited before add the location to ip_info_table
				// store ip info if not already in db in ip_info
					// continent 
					// country
					// state
					// city
			
			if (empty($ip_exist))
			{
				$ip_loc_insert = 'INSERT INTO ip_info_table (ip_address, continent, country, state, city) VALUES';
				$ip_loc_insert .= '("'.$this->sanitize($this->ip).'", ';
				$ip_loc_insert .= '"'.$this->sanitize($this->ip_loc['continent']).'", ';
				$ip_loc_insert .= '"'.$this->sanitize($this->ip_loc['country']).'", ';
				$ip_loc_insert .= '"'.$this->sanitize($this->ip_loc['state']).'", ';
				$ip_loc_insert .= '"'.$this->sanitize($this->ip_loc['city']).'") ';
				$result = mysqli_query($this->conn, $ip_loc_insert);
			}
		}

		public function GetTableColNames($table)
		{
			$cols = $this->listAll('get-table-col-names', $table);

			$collasped_cols = array();

			foreach ($cols as $key => $value)
			{

				array_push($collasped_cols, $value['COLUMN_NAME']);
			}

			return $collasped_cols;
		}

		public function addOrModifyRecord($table, $inputs)
		{
			
			$count = 0;
			$replace = 'REPLACE '.$this->sanitize($table).' SET ';


			foreach($inputs as $name => $value)
			{
				// only include column if it is present in table
				$table_cols = $this->GetTableColNames($table);
				$count++;

				if (in_array($name, $table_cols))
				{
					$replace .= $this->sanitize($name).' = "'.$this->sanitize($value).'"';

					if($count < sizeof($inputs))
					{
						$replace .= ', ';
					}
				}
			}

			// add to log
			$this->AddLog('addOrModifyRecord', $table, $replace);

			// remove comma at the end of replace statement
			$replace = rtrim($replace, ', ');

			$result = mysqli_query($this->conn, $replace) or die('Oops!! Died '.$replace);

			// get the Id of the inserted record
			$lastId = mysqli_insert_id($this->conn);

			return [$result, $lastId];
		}

		public function deleteRecord($table, $inputs)
		{
			$count = 0;
			$delete = 'DELETE FROM '.$this->sanitize($table).' WHERE ';

			foreach($inputs as $name => $value)
			{
				$delete .= $this->sanitize($name).' = "'.$this->sanitize($value).'"';

				$count++;

				if($count < sizeof($inputs))
				{
					$delete .= ' AND ';
				}
			}

			// add to log
			$this->AddLog('Delete', $table, $delete);

			$result = mysqli_query($this->conn, $delete) or die($delete);
			$lastId = mysqli_insert_id($this->conn);

			return [$result, $lastId];
		}

		public function listAll($query, $where=NULL)
		{

			// log query
			// only search queries will be logged
			if (strpos($query, 'search') !== false)
			{
				$this->AddLog('Search', $query, $where);
			}
			
			switch($query)
			{
				case 'ip-num-failed-attempts':
					$select =
					'
					SELECT 		COUNT(lt.login_id) as login_count

					FROM 		login_table lt

					WHERE 		lt.ip_address = "'.$this->sanitize($where).'" AND
								lt.status = "failed" AND 
								lt.time_stamp > (now() - interval 30 minute)
					';
					
					break;
				
				case 'user-num-failed-attempts':
					$select =
					'
					SELECT 		COUNT(lt.login_id) as login_count

					FROM 		login_table lt

					WHERE 		lt.email_address = "'.$this->sanitize($where).'" AND
								lt.status = "failed" AND 
								lt.time_stamp > (now() - interval 30 minute)
					';
					
					break;

				case 'low-coverage-by-run-id':
					$select = 
					'
					SELECT 	lct.*

					FROM 	low_coverage_table lct

					WHERE 	lct.run_id =  "'.$this->sanitize($where).'"  
					';
					break;

				case 'classify-by-panel-gene-coding-protein':
					$select = 
					'
					SELECT 	ct.*

					FROM 	classification_table ct

					WHERE 	ct.genes = "'.$this->sanitize($where['genes']).'" AND 
							ct.coding = "'.$this->sanitize($where['coding']).'" AND
							ct.amino_acid_change = "'.$this->sanitize($where['amino_acid_change']).'" AND
							ct.panel = "'.$this->sanitize($where['panel']).'"
					';
					break;

				case 'classification-variant':
					$select = 
					'
					SELECT 		ct.classification

					FROM 		classification_table ct

					WHERE 		ct.genes = "'.$this->sanitize($where['genes']).'" AND
								ct.coding = "'.$this->sanitize($where['coding']).'" AND
								ct.amino_acid_change = "'.$this->sanitize($where['amino_acid_change']).'" AND
								ct.panel =  "'.$this->sanitize($where['panel']).'"	
					';
					break;

				case 'low-coverage-by-run-id-gene-amplicon':
					$select =
					'
					SELECT 	lct.*

					FROM 	low_coverage_table lct

					WHERE 	lct.gene =  "'.$this->sanitize($where['gene']).'" AND 
							lct.run_id =  "'.$this->sanitize($where['run_id']).'" AND
							lct.Amplicon =  "'.$this->sanitize($where['Amplicon']).'" 
					';
					break;

				case 'query-variant-tier-xref-by-observed-variant-id':
					$select =
					'
					SELECT 	vtx.* 

					FROM 	variant_tier_xref vtx 

					WHERE 	vtx.observed_variant_id = "'.$this->sanitize($where).'"
					';
					break;

				case 'all-notes':
					$select = 
					'
					SELECT 	nt.note,
							nt.time_stamp,
							CONCAT(ut.first_name," " ,ut.last_name) AS user_name

					FROM 	notes_table nt

					JOIN  	user_table ut
					ON 		ut.user_id = nt.user_id
					';
					break;

				case 'ip-info-exist':
					// find the ip info which pertains to ip address
					$select = 
					'
					SELECT 	* 

					FROM 	ip_info_table

					WHERE 	ip_address = "'.$this->sanitize($where).'"
					';
					break;

				case 'observed-variant-by-id':
					$select =
					'
					SELECT 	*

					FROM 	observed_variant_table

					WHERE 	observed_variant_id = "'.$this->sanitize($where).'"
					';
					break;
				
				case 'observed-variant-by-run-id':
					$select =
					'
					SELECT 	*

					FROM 	observed_variant_table

					WHERE 	run_id = "'.$this->sanitize($where).'"
					';
					break;					
				
				case 'ngs-panel-info':
					$select = 
					'
					SELECT 	*

					FROM 	ngs_panel
					';
					break;

				case 'possible-diagnosis':
					$select = 
					'
					SELECT 	*

					FROM 	tumor_type_table

					ORDER BY 	tumor ASC
					';
					break;

				case 'run-info':
					$select = 
					'
					SELECT 		rit.*,
								vt.visit_id,
								vt.received,
								vt.mol_num,
								CONCAT(pt.first_name," " ,pt.middle_name, " ", pt.last_name) AS patient_name

					FROM 		run_info_table rit

					LEFT JOIN  	visit_table vt
					ON 			vt.run_id = rit.run_id

					LEFT JOIN 	patient_table pt 
					ON 			pt.patient_id = vt.patient_id

					WHERE 		rit.run_id = "'.$this->sanitize($where).'"
					';					
					break;

				case 'pending-tech-runs':

					$select = 
					'
					SELECT 		rit.*,
								vt.visit_id,
								vt.mol_num,
								CONCAT(pt.first_name," " ,pt.middle_name, " ", pt.last_name) AS patient_name,
								pt.patient_id

					FROM 		run_info_table rit

					LEFT JOIN  	visit_table vt
					ON 			vt.run_id = rit.run_id

					LEFT JOIN 	patient_table pt 
					ON 			pt.patient_id = vt.patient_id

					WHERE 		rit.status = "pending" OR rit.status = "tech approved"
					';

					break;

				case 'tech-approved-runs':

					$select = 
					'
					SELECT 	*

					FROM 	run_info_table rit

					WHERE 	rit.status = "tech approved"
					';

					break;

				case 'steps-run':
					$select = 
					'
					SELECT 	rst.step,
							srx.time_stamp,
							CONCAT(ut.first_name," " ,ut.last_name) AS user_name

					FROM 	step_run_xref srx

					JOIN 	report_steps_table rst
					ON 		srx.step_id = rst.step_id

					JOIN 	user_table ut
					ON 		ut.user_id = srx.user_id

					WHERE 	srx.run_id = "'.$this->sanitize($where).'"
					
					ORDER BY 	srx.time_stamp DESC

					';
				
					break;
				
				case 'completed-pre-steps':
					$select = 
					'
					SELECT 	psv.step

					FROM 	pre_step_visit psv

					WHERE 	psv.visit_id = "'.$this->sanitize($where).'"
					';
					break;

				case 'this-step':
					$select = 
					'
					SELECT 	*

					FROM 	report_steps_table

					WHERE 	step = "'.$this->sanitize($where).'"
					';
					break;

				case 'this-pre-step':
					$select = 
					'
					SELECT 	*

					FROM 	report_steps_table

					WHERE 	step = "'.$this->sanitize($where).'"
					';
					break;

				case 'step-added':
					$select =
					'
					SELECT 	step_id 

					FROM 	'.$this->sanitize($where['table_name']).' 

					WHERE 	step="'.$this->sanitize($where['step']).'"
					';				
					break;

				case 'patient-info':
					$select =
					'	
					SELECT 		CONCAT(pt.first_name," " ,pt.middle_name, " ", pt.last_name) AS patient_name,
								rivw.*,
								pt.*,
								CONCAT(ut.first_name," " ,ut.last_name) AS user_name

					FROM 		report_ids_vw rivw

					JOIN 		patient_table pt
					ON 			pt.patient_id = rivw.patient_id

					LEFT JOIN 	user_table ut
					ON 			pt.user_id = ut.user_id

					WHERE 		rivw.run_id = "'.$this->sanitize($where).'"
					';
					break;

				case 'find-patient':
					$select =
					'
					SELECT 	*

					FROM 	patient_table

					WHERE ';

					$first_key = true;
					
					foreach($where as $key => $value)
					{
						if(!$first_key)
						{
							$select.= ' AND ';
						}
					
						$select .= $this->sanitize($key).' = "'.$this->sanitize($value).'"';
						$first_key = false;						
					}
					break;

				case 'select-patient-from-id':
					$select = 
					'
					SELECT 		CONCAT(pt.first_name," " ,pt.middle_name, " ", pt.last_name) AS patient_name,
								pt.*,
								CONCAT(ut.first_name," " ,ut.last_name) AS user_name

					FROM 		patient_table pt

					LEFT JOIN 	user_table ut
					ON 			pt.user_id = ut.user_id

					WHERE 		pt.patient_id = "'.$this->sanitize($where).'"
					';
					break;
					
				case 'all-patient-visits':
					$select = 
					'
					SELECT 		vt.*,
								rit.*

					FROM 		visit_table vt

					LEFT JOIN 	run_info_table rit
					ON 			rit.run_id = vt.run_id

					WHERE 		vt.patient_id = "'.$this->sanitize($where).'"
					';
				
					break;
				
				case 'visit-info':
					$select =
					'	
					SELECT 		vt.*,
								CONCAT(ut.first_name," " ,ut.last_name) AS user_name,
								tt.tumor AS tumor_type,
								ngp.type as panel_type,
								ngp.method,
								ngp.url,
								ngp.disclaimer,
								ngp.limitations

					FROM 		visit_table vt

					LEFT JOIN 	user_table ut
					ON 			vt.user_id = ut.user_id

					LEFT JOIN  	ngs_panel ngp
					ON 			vt.ngs_panel_id = ngp.ngs_panel_id

					LEFT JOIN 	tumor_type_table tt
					ON 			tt.tumor_id = vt.diagnosis_id

					WHERE 		vt.run_id = "'.$this->sanitize($where).'"
					';
					
					break;

				case 'last-ten-completed-reports':
					$select =
					'
					SELECT 		rit.run_id,
								vt.visit_id,
								vt.patient_id,
								CONCAT(pt.first_name," " , pt.middle_name," ",pt.last_name) AS patient_name,
								pt.medical_record_num,
								rit.time_stamp,
								vt.mol_num,
								np.type AS panel


					FROM 		run_info_table rit 

					LEFT JOIN 	visit_table vt
					ON 			rit.run_id = vt.run_id

					LEFT JOIN 	patient_table pt 
					ON 			pt.patient_id = vt.patient_id

					LEFT JOIN 	ngs_panel np 
					ON 			np.ngs_panel_id = vt.ngs_panel_id

					WHERE 		rit.status = "completed"

					LIMIT 		10
					';				
					break;

				case 'ajax-visit-info':
					$select 	=
					'
					SELECT 	*

					FROM 	visit_table vt

					WHERE 	vt.visit_id = "'.$this->sanitize($where).'"
					';
					break;

				case 'get-gene-interpt-by-interpt':
					$select =
					'
					SELECT 	* 

					FROM 	gene_interpt_table git 

					WHERE 	git.interpt = "'.$this->sanitize($where).'"
					';
					break;

				case 'all-gene-interpts-for-gene':
					$select =
					'
					SELECT 		git.interpt,
								git.time_stamp,
								CONCAT(ut.first_name," " ,ut.last_name) AS user_name 

					FROM 		gene_interpt_table git

					LEFT JOIN  	user_table ut 
					ON 			ut.user_id = git.user_id

					WHERE 		git.gene = "'.$this->sanitize($where).'"

					ORDER BY 		git.time_stamp ASC
					';
					break;					

				case 'run-xref-gene-interpt-by-run-id-gene':
					$select = 
					'
					SELECT 		rxgi.*,
								git.interpt

					FROM 		run_xref_gene_interpt rxgi

					LEFT JOIN 	gene_interpt_table git 
					ON 			git.gene_interpt_id = rxgi.gene_interpt_id

					WHERE 		rxgi.gene = "'.$this->sanitize($where['gene']).'" AND 
								rxgi.run_id = "'.$this->sanitize($where['run_id']).'"
					';
					break;

				case 'add-visit-info':
					$select =
					'	
					SELECT 		vt.*,
								np.type AS ngs_panel,
								tt.tumor_id,
								tt.tumor AS tumor_type,
								ttt.tissue_id

					FROM 		visit_table vt

					LEFT JOIN 	ngs_panel np
					ON 			np.ngs_panel_id = vt.ngs_panel_id

					LEFT JOIN 	tumor_type_table tt
					ON 			tt.tumor_id = vt.diagnosis_id

					LEFT JOIN 	tissue_type_table ttt 
					ON 			ttt.tissue = vt.primary_tumor_site

					WHERE 		vt.visit_id = "'.$this->sanitize($where).'"
					';
					break;

				case 'add-patient-info':
					$select =
					'	
					SELECT 		pt.*,
								CONCAT(pt.first_name," " , pt.middle_name," ",pt.last_name) AS patient_name

					FROM 		patient_table pt

					WHERE 		pt.patient_id = "'.$this->sanitize($where).'"
					';
					break;

				case 'pre-step-complete':
					$select = 
					'
					SELECT 	* 

					FROM 	pre_step_visit

					WHERE 	visit_id = "'.$this->sanitize($where['visit_id']).'" AND 
							step = "'.$this->sanitize($where['step']).'"
					';
					break;

				case 'all-complete-pre-steps':	
					$select =
					'
					SELECT 	*

					FROM 	pre_step_visit

					WHERE 	visit_id = "'.$this->sanitize($where).'"
					';
					break;

				case 'all-complete-steps':
					$select = 
					'
					SELECT 	step,
							user_name
						
					FROM 	(

								SELECT 	*

								FROM
										(
											SELECT 		psv.step,
														CONCAT(ut.first_name," " ,ut.last_name) AS user_name,
														psv.time_stamp

											FROM 		visit_table vt 

											LEFT JOIN 	pre_step_visit psv 
											ON 			vt.visit_id = psv.visit_id AND
														vt.visit_id = "'.$this->sanitize($where).'"

											LEFT JOIN		user_table ut 
											ON 			ut.user_id = psv.user_id

											WHERE 		psv.step != "NULL"

										) 	AS step

								UNION 
								SELECT 	*

								FROM
										(
											SELECT 		rst.step,
														CONCAT(ut.first_name," " ,ut.last_name) AS user_name,
														srx.time_stamp

											FROM 		visit_table vt 

											LEFT JOIN 	run_info_table rit 
											ON 			rit.run_id = vt.run_id AND 
														vt.visit_id = "'.$this->sanitize($where).'"

											LEFT JOIN 	step_run_xref srx 
											ON 			rit.run_id = srx.run_id

											LEFT JOIN		user_table ut 
											ON 			ut.user_id = srx.user_id

											LEFT JOIN 	report_steps_table rst 
											ON 			rst.step_id = srx.step_id

											WHERE 		rst.step != "NULL"

										) 	AS step
							) 	AS step2

					ORDER BY 		time_stamp DESC
					';
					break;

				case 'this-pre-step-status':
					$select =
					'
					SELECT 		psv.*,
								CONCAT(ut.first_name," " ,ut.last_name) AS user_name

					FROM 		pre_step_visit psv

					LEFT JOIN 	user_table ut
					ON 			ut.user_id = psv.user_id

					WHERE 		psv.visit_id = "'.$this->sanitize($where['visit_id']).'" AND
								psv.step = "'.$this->sanitize($where['step']).'"

					';
					break;

				case 'select-knowledge-base':
					$select =
					'
					SELECT 	*

					FROM 	knowledge_base_table kbt
					
					WHERE 	knowledge_id IN ('.$this->sanitize($where).')';
					break;

				case 'all-knowledge-base':
					$select =
					'
					SELECT 	*

					FROM 	knowledge_base_table kbt
					';

					break;

				case 'knowledge-base-passed-filter':
					$select =
					'
					SELECT 		kbt.*

					FROM			observed_variant_table ovt

					JOIN 		knowledge_base_table kbt
					ON 			ovt.genes = kbt.genes AND ovt.coding = kbt.coding AND ovt.amino_acid_change = kbt.amino_acid_change

					WHERE 		ovt.run_id = "'.$this->sanitize($where).'" 
					';
					break;

				case 'knowledge-base-failed-filter':
					$select =
					'
					SELECT 		kbt.*

					FROM			observed_variant_table ovt

					JOIN 		knowledge_base_table kbt
					ON 			ovt.genes = kbt.genes AND ovt.coding = kbt.coding AND ovt.amino_acid_change = kbt.amino_acid_change

					WHERE 		ovt.run_id = "'.$this->sanitize($where).'" AND ovt.filter_status = "failed"
					';
					break;

				case 'knowledge-base-included-filter':
					$select =
					'
					SELECT 		kbt.*

					FROM			observed_variant_table ovt

					JOIN 		knowledge_base_table kbt
					ON 			ovt.genes = kbt.genes AND ovt.coding = kbt.coding AND ovt.amino_acid_change = kbt.amino_acid_change

					WHERE 		ovt.run_id = "'.$this->sanitize($where).'" AND ovt.include_in_report = "1"
					';
					break;

				case 'all-tiers':
					$select = 
					'
					SELECT 	*

					FROM 	tier_table
					';
					break;
				case 'find-tier':
					$select = 
					'
					SELECT 	*

					FROM 	variant_tier_xref

					WHERE 	tier_id = "'.$this->sanitize($where['tier_id']).'" AND
							knowledge_id = "'.$this->sanitize($where['knowledge_id']).'" AND
							tissue_id = "'.$this->sanitize($where['tissue_id']).'" 

					';
					break;
				case 'all-tissues':
					$select = 
					'
					SELECT 	*

					FROM 	tissue_type_table

					ORDER BY 	tissue
					';
					break;

				case 'report-interpt':
					$select = 
					'
					SELECT 		ct.comment,
								kbt.pmid,
								ovt.genes,
								ovt.coding,
								ovt.amino_acid_change,
								ovt.tier,
								kbt.exon,
								kbt.cosmic,

								CONCAT(ovt.genes, " ", ovt.coding, " (", ovt.amino_acid_change,")") AS result_mutant


					FROM 		observed_variant_table ovt

					LEFT JOIN 	comment_table ct 
					ON 			ct.comment_id = ovt.interpt_id

					LEFT JOIN 	knowledge_base_table kbt
					ON 			ovt.genes = kbt.genes AND 
								ovt.coding = kbt.coding AND 
								ovt.amino_acid_change = kbt.amino_acid_change

					WHERE 		ovt.run_id = "'.$this->sanitize($where).'"  AND 
								ovt.include_in_report = 1

					';	
					break;

				case 'run-variants-passed-filter':
					$select =
					'
					SELECT 	va.*,
							kbt.knowledge_id,
							kbt.cosmic,
							vt.comment AS variant_interpt

					FROM	(
						SELECT 		ovt.*,
									GROUP_CONCAT(ct.comment_id ORDER BY ct.comment_id ASC SEPARATOR "-") AS comment_ids

						FROM			observed_variant_table ovt

						LEFT JOIN 	comment_table ct 
						ON 			ct.comment_ref = ovt.observed_variant_id AND 
									ct.comment_type = "observed_variant"

						WHERE 		ovt.run_id = "'.$this->sanitize($where).'" 

						GROUP BY 		ovt.genes, ovt.observed_variant_id, ovt.run_id
					) va

					LEFT JOIN 	knowledge_base_table kbt 
					ON 			va.genes = kbt.genes AND
								va.coding =  kbt.coding AND
								va.amino_acid_change = kbt.amino_acid_change 

					LEFT JOIN 	comment_table vt 
					ON 			vt.comment_id = va.interpt_id AND 
								vt.comment_type = "knowledge_base_table"
					';			
					break;
				
				case 'chr-gene-on':
					$select = 
					'
					SELECT DISTINCT 	kbt.chr,
									kbt.hgnc

					FROM 			knowledge_base_table kbt

					WHERE 			kbt.genes = "'.$this->sanitize($where).'"
					';
					break;
				
				case 'mutation-types':
					$select = 'SELECT DISTINCT mutation_type FROM knowledge_base_table';
					break;

				case 'report-status':
					$select = 
					'
					SELECT 	rit.status AS approval_status

					FROM 	run_info_table rit

					WHERE 	rit.run_id = "'.$this->sanitize($where).'"
					';
					break;

				case 'approval-steps':
					$select = 
					'
					SELECT 	rst.step,
							srx.time_stamp,
							CONCAT(ut.first_name," " ,ut.last_name) AS user_name,
							ut.credentials,
							ut.title

					FROM 	step_run_xref srx

					JOIN 	report_steps_table rst

					ON 		srx.step_id = rst.step_id AND
                    			(rst.step = "tech_approved" OR rst.step = "confirmed")

					JOIN 	user_table ut

					ON 		ut.user_id = srx.user_id

					WHERE 	srx.run_id = "'.$this->sanitize($where).'"';
					break;

				case 'run-variants-failed-filter':
					$select =
					'
					SELECT 	va.*,
							kbt.knowledge_id
					FROM	(
						SELECT 		ovt.*,
									GROUP_CONCAT(ct.comment_id ORDER BY ct.comment_id ASC SEPARATOR "-") AS comment_ids

						FROM			observed_variant_table ovt

						LEFT JOIN 	comment_table ct 
						ON 			ct.comment_ref = ovt.observed_variant_id AND 
									ct.comment_type = "observed_variant"

						WHERE 		ovt.run_id = "'.$this->sanitize($where).'" AND ovt.filter_status = "failed"

						GROUP BY 		ovt.genes, ovt.observed_variant_id, ovt.run_id
					) va

					LEFT JOIN 	knowledge_base_table kbt 
					ON 			va.genes = kbt.genes AND
								va.coding =  kbt.coding AND
								va.amino_acid_change = kbt.amino_acid_change 
					';
					break;

				case 'kbt-snv':
					$select =
					'
					SELECT 	kbt.knowledge_id

					FROM 	knowledge_base_table kbt 

					WHERE 	kbt.genes = "'.$this->sanitize($where['genes']).'" AND
							kbt.coding = "'.$this->sanitize($where['coding']).'" AND
							kbt.amino_acid_change = "'.$this->sanitize($where['amino_acid_change']).'"
					';
					break;

				case 'run-variants-included-filter':
					$select =
					'
					SELECT 	va.*,
							kbt.knowledge_id,
							kbt.cosmic,
							vt.comment AS variant_interpt
					FROM	(
						SELECT 		ovt.*,
									GROUP_CONCAT(ct.comment_id ORDER BY ct.comment_id ASC SEPARATOR "-") AS comment_ids

						FROM			observed_variant_table ovt

						LEFT JOIN 	comment_table ct 
						ON 			ct.comment_ref = ovt.observed_variant_id AND 
									ct.comment_type = "observed_variant"

						WHERE 		ovt.run_id = "'.$this->sanitize($where).'" AND ovt.include_in_report = "1"

						GROUP BY 		ovt.genes, ovt.observed_variant_id, ovt.run_id
					) va

					LEFT JOIN 	knowledge_base_table kbt 
					ON 			va.genes = kbt.genes AND
								va.coding =  kbt.coding AND
								va.amino_acid_change = kbt.amino_acid_change 

					LEFT JOIN 	comment_table vt 
					ON 			vt.comment_id = va.interpt_id AND 
								vt.comment_type = "knowledge_base_table"			
					';
					break;
				
				case 'pending-pre-samples':
					$select = 
					'
					SELECT 	*
					FROM 	(
							
							SELECT 	vt.visit_id,
									vt.patient_id,
									vt.mol_num,
									vt.time_stamp,
									np.type AS ngs_panel,
									dt.diagnosis,
									CONCAT(pt.first_name," " , pt.middle_name," ",pt.last_name) AS patient_name,
									GROUP_CONCAT(ut.first_name, "->", psv.step ORDER BY psv.time_stamp ASC SEPARATOR ", ") AS editors_per_step,

									-- passed steps
									(
										SELECT 	GROUP_CONCAT(psv5.step ORDER BY psv5.time_stamp ASC SEPARATOR ", ") AS steps_passed1

										FROM 		pre_step_visit psv5

										WHERE 		psv5.visit_id =  vt.visit_id AND 
													psv5.status = "passed"

										GROUP BY 		vt.visit_id
									
									)	AS 			steps_passed,

									-- add failed count and remove samples which have failed
								     (
										SELECT 	COUNT(*)

										FROM 	pre_step_visit psv2

										WHERE 	psv2.visit_id =  vt.visit_id AND 
												psv2.status = "Failed"
								     
								     ) 	AS fail_count,

								     -- add flagged count

								     (
								     	SELECT 	psv3.step AS flagged_step

								     	FROM 	pre_step_visit psv3 

								     	WHERE 	psv3.visit_id = vt.visit_id AND 
								     			psv3.status = "Flagged"
								     
								     ) AS flagged_steps,

								     -- remove completed pre
								     (
										SELECT 	COUNT(*)

										FROM 	pre_step_visit psv4

										WHERE 	psv4.visit_id =  vt.visit_id AND 
												psv4.step = "pre_complete"
								     
								     ) 	AS pre_complete_count

							FROM 		visit_table vt
							
							INNER JOIN  	pre_step_visit psv USING(visit_id)
							
							LEFT JOIN 	diagnosis_table dt 
							ON 			vt.diagnosis_id = dt. diagnosis_id

							LEFT JOIN 	patient_table pt 
							ON 			vt.patient_id = pt.patient_id

							LEFT JOIN 	user_table ut
							ON 			ut.user_id = psv.user_id

							LEFT JOIN 	ngs_panel np 
							ON 			np.ngs_panel_id = vt.ngs_panel_id

							GROUP BY 		vt.visit_id

							ORDER BY 		np.type ASC, vt.visit_id ASC
					) 	as sub1

					WHERE 	fail_count = 0 AND
							pre_complete_count = 0;


					';
					break;
				
				case 'pending-pre-samples-by-visit-id':
					$select = 
					'
					SELECT 	*
					FROM 	(
							
							SELECT 	vt.visit_id,
									vt.patient_id,
									vt.mol_num,
									vt.time_stamp,
									np.type AS ngs_panel,
									dt.diagnosis,
									CONCAT(pt.first_name," " , pt.middle_name," ",pt.last_name) AS patient_name,
									GROUP_CONCAT(ut.first_name, "->", psv.step ORDER BY psv.time_stamp ASC SEPARATOR ", ") AS editors_per_step,

									-- passed steps
									(
										SELECT 	GROUP_CONCAT(psv5.step ORDER BY psv5.time_stamp ASC SEPARATOR ", ") AS steps_passed1

										FROM 		pre_step_visit psv5

										WHERE 		psv5.visit_id =  vt.visit_id AND 
													psv5.status = "passed"

										GROUP BY 		vt.visit_id
									
									)	AS 			steps_passed,

									-- add failed count and remove samples which have failed
								     (
										SELECT 	COUNT(*)

										FROM 	pre_step_visit psv2

										WHERE 	psv2.visit_id =  vt.visit_id AND 
												psv2.status = "Failed"
								     
								     ) 	AS fail_count,

								     -- add flagged count

								     (
								     	SELECT 	psv3.step AS flagged_step

								     	FROM 	pre_step_visit psv3 

								     	WHERE 	psv3.visit_id = vt.visit_id AND 
								     			psv3.status = "Flagged"
								     
								     ) AS flagged_steps,

								     -- remove completed pre
								     (
										SELECT 	COUNT(*)

										FROM 	pre_step_visit psv4

										WHERE 	psv4.visit_id =  vt.visit_id AND 
												psv4.step = "pre_complete"
								     
								     ) 	AS pre_complete_count

							FROM 		visit_table vt
							
							INNER JOIN  	pre_step_visit psv USING(visit_id)
							
							LEFT JOIN 	diagnosis_table dt 
							ON 			vt.diagnosis_id = dt. diagnosis_id

							LEFT JOIN 	patient_table pt 
							ON 			vt.patient_id = pt.patient_id

							LEFT JOIN 	user_table ut
							ON 			ut.user_id = psv.user_id

							LEFT JOIN 	ngs_panel np 
							ON 			np.ngs_panel_id = vt.ngs_panel_id

							GROUP BY 		vt.visit_id

							ORDER BY 		np.type ASC, vt.visit_id ASC
					) 	as sub1

					WHERE 	fail_count = 0 AND
							visit_id = "'.$this->sanitize($where).'";


					';
					break;

				case 'variant-comments':
					$select =
					'
					SELECT 		CONCAT(ut.first_name," " ,ut.last_name) AS user_name,
								ct.comment_id,
								ct.comment,
								ct.time_stamp as comment_time_stamp

					FROM 		comment_table ct

					LEFT JOIN 	user_table ut
					ON 			ut.user_id = ct.user_id

					WHERE 		ct.comment_type = "observed_variant" AND
								ct.comment_id = "'.$this->sanitize($where).'"
					';
					break;

				// get knowledge comments
				case 'knowledge-comments':
					$select =
					'
					SELECT 		ut.first_name, 
								ut.last_name,
								ct.comment_id,
								ct.comment,
								ct.comment_ref,
								ct.time_stamp as comment_time_stamp

					FROM 		comment_table ct

					LEFT JOIN 	user_table ut
					ON 			ut.user_id = ct.user_id

					WHERE 		ct.comment_type = "knowledge_base_table" AND
								ct.comment_ref = "'.$this->sanitize($where).'"
					';
					break;

				case 'knowledge-comments-join-observed':
					$select =
					'
					SELECT 		ut.first_name,
								ut.last_name,
								ct.comment_id,
								ct.comment,
								ct.time_stamp as comment_time_stamp

					FROM 		observed_variant_table ovt

					JOIN 		knowledge_base_table kbt
					ON 			ovt.genes = kbt.genes AND 
								ovt.coding = kbt.coding AND 
								ovt.amino_acid_change = kbt.amino_acid_change

					LEFT JOIN	 	comment_table ct
					ON 			kbt.knowledge_id = ct.comment_ref

					LEFT JOIN 	user_table ut

					ON 			ut.user_id = ct.user_id

					WHERE 		ct.comment_type = "knowledge_base_table" AND 
								ovt.genes = "'.$this->sanitize($where['genes']).'" AND 
								ovt.coding = "'.$this->sanitize($where['coding']).'" AND 
								ovt.amino_acid_change = "'.$this->sanitize($where['amino_acid_change']).'" AND
								ovt.run_id = "'.$this->sanitize($where['run_id']).'"

					ORDER BY 		ct.time_stamp DESC
					';
					
					break;

				case 'comment-by-id':
					$select =
					'
					SELECT 	comment

					FROM 	comment_table

					WHERE 	comment_id = "'.$this->sanitize($where).'"
					';
					break;
				case 'comment-by-ref-and-comment':
					$select =
					'
					SELECT 	ct.comment,
							ct.time_stamp,
							ct.comment_id,
							CONCAT(ut.first_name," " ,ut.last_name) AS user_name

					FROM 	comment_table ct

					JOIN 	user_table ut
					ON 		ut.user_id = ct.user_id

					WHERE 	ct.comment_ref = "'.$this->sanitize($where['comment_ref']).'" AND 
							ct.comment = "'.$this->sanitize($where['comment']).'" 
					';
					break;
				case 'knowledge-tier':
					$select = 
					'
					SELECT 	
							vt_xref_id,
							tier,
							tier_summary,
							tissue				

					FROM 	tier_tissue_knowledge_vw

					WHERE 	knowledge_id = "'.$this->sanitize($where).'"
					';
					break;

				case 'summary-saved':
					$select =
					'
					SELECT 		*

					FROM 		no_snv_summary_table

					WHERE 		summary = "'.$this->sanitize($where).'"
					';

					break;

				case 'all-summary-saved':
					$select =
					'
					SELECT 		nsst.*,
								CONCAT(ut.first_name," " ,ut.last_name) AS user_name

					FROM 		no_snv_summary_table nsst

					LEFT JOIN 	user_table ut
					ON 			ut.user_id = nsst.user_id

					ORDER BY 		nsst.time_stamp DESC
					';

					break;		

				case 'tier-user':
					$select = 
					'	
					SELECT         vtx.vt_xref_id,
								ttt.tissue,
					               CONCAT(ut.first_name," " ,ut.last_name) AS user_name,
					               vtx.time_stamp,
					               tt.tier,
					               tt.tier_summary

					FROM           variant_tier_xref vtx

					LEFT JOIN      tier_table tt
					ON             vtx.tier_id = tt.tier_id

					LEFT JOIN      user_table ut
					ON             ut.user_id = vtx.user_id

					LEFT JOIN      tissue_type_table ttt
					ON             ttt.tissue_id = vtx.tissue_id

					WHERE          vtx.knowledge_id = "'.$this->sanitize($where).'"

					ORDER BY 		tt.tier ASC, ttt.tissue ASC 
					';
					break;

				case 'tier-knowledge-id-overview':
					$select =
					'
					SELECT 		tt.tier,
								COUNT(tt.tier) AS count

					FROM           variant_tier_xref vtx

					LEFT JOIN      tier_table tt
					ON             vtx.tier_id = tt.tier_id

					WHERE          vtx.knowledge_id = "'.$this->sanitize($where).'" 

					GROUP BY 		tt.tier

					ORDER BY 		tt.tier ASC
					';
					break;

				case 'tier-by-vt-xref':
					$select = 
					'	
					SELECT         vtx.vt_xref_id,
								ttt.tissue,
					               CONCAT(ut.first_name," " ,ut.last_name) AS user_name,
					               vtx.time_stamp,
					               tt.tier,
					               tt.tier_summary

					FROM           variant_tier_xref vtx

					JOIN           tier_table tt
					ON             vtx.tier_id = tt.tier_id

					JOIN           user_table ut
					ON             ut.user_id = vtx.user_id

					JOIN           tissue_type_table ttt
					ON             ttt.tissue_id = vtx.tissue_id

					WHERE          vtx.vt_xref_id = "'.$this->sanitize($where).'"

					ORDER BY 		tt.tier ASC, ttt.tissue ASC 
					';
					break;

				case 'knowledge-by-id':
					$select = 
					'
					SELECT 	*

					FROM 	knowledge_base_table

					WHERE 	knowledge_id = "'.$this->sanitize($where).'"
					';
					break;
				
				case 'search-knowledge':
					$select = 
					'	
					SELECT DISTINCT  	* 

					FROM 			knowledge_base_table 

					WHERE 			';
					
					$select = $this->MakeWhereClauseFromArray($select, $where);
					break;

				case 'search-observed-variants':
					$select =
					'
					SELECT 	ovt.run_id,
							ovt.genes,
							ovt.amino_acid_change,
							ovt.coding,
							ovt.tier,
							ovt.include_in_report,
							ovt.qc_status,
							ovt.filter_status,
							rit.status,
							rit.run_date_chip,
							rit.work_flow_name,
							rit.sample_name

					FROM 	observed_variant_table ovt

					JOIN 	run_info_table rit
					ON 		ovt.run_id = rit.run_id

					WHERE 	';	

					$select = $this->MakeWhereClauseFromArray($select, $where, 'rit', 'ovt');
			
					$select .= ' AND rit.status = "completed"';
				
					break;

				case 'search-patient-reports':
					$select =
					'
						SELECT 	*

						FROM 	completed_patient_reports_vw 

						WHERE 	';
					
					$where_keys = array_keys($where);

					$select = $this->MakeWhereClauseFromArray($select, $where);
		
					break;

				case 'find-variant-tier-xref':
					$select =
					'
					SELECT 	vt_xref_id

					FROM 	variant_tier_xref

					WHERE 	tier_id ="'.$this->sanitize($where['tier_id']).'"	AND
							knowledge_id ="'.$this->sanitize($where['knowledge_id']).'"		
					';					
					if ($where['tissue_id'] !== '')
					{
						$select .= 
						'		AND
								tissue_id ="'.$this->sanitize($where['tissue_id']).'"';
					}
					
					break;

				case 'find-variant-tissue-xref':
					$select =
					'
					SELECT 	vt_xref_id

					FROM 	variant_tier_xref

					WHERE 	knowledge_id ="'.$this->sanitize($where['knowledge_id']).'" AND
							tissue_id ="'.$this->sanitize($where['tissue_id']).'"		
					';			
					break;

				case 'get-table-col-names':
					$select =
					'
					SELECT 	COLUMN_NAME 

					FROM 	information_schema.columns 

					WHERE 	table_name = "'.$this->sanitize($where).'"
					';
					
					break;

				case 'unique-gene-list':
					$select = 
					'
					SELECT DISTINCT 	genes

					FROM 			knowledge_base_table kbt

					ORDER BY 			kbt.genes ASC
					';
					break;

				case 'user-exist':
					$select = 
					'
					SELECT 			*

					FROM 			user_table ut

					WHERE 			ut.email_address = "'.$this->sanitize($where['email_address']).'" AND 
									ut.password = "'.$this->sanitize($where['password']).'"
					';
					break;					

				case 'user-permissions':
					$select = 
					'
					SELECT 		GROUP_CONCAT(pt.permission SEPARATOR ", ") AS permissions

					FROM 		user_table ut

					LEFT JOIN 	permission_user_xref pux
					ON 			pux.user_id = ut.user_id

					LEFT JOIN 	permission_table pt
					ON 			pux.permission_id = pt.permission_id

					WHERE 		ut.user_id = "'.$this->sanitize($where).'"

					GROUP BY 		ut.user_id
					';
					break;
				case 'user-initials':
					$select = 
					'
					SELECT 	CONCAT(SUBSTRING(ut.first_name, 1,1), "", SUBSTRING(ut.last_name, 1,1)) AS initials

		
					FROM 	user_table ut

					WHERE 	ut.user_id = "'.$this->sanitize($where).'"
					';				
					break;

				case 'confirmed-variant-included':
					$select = 
					'
					SELECT 		ovt.genes,
								ovt.coding,
								ovt.amino_acid_change,
								ovt.observed_variant_id,

								cc_vw.mutation_type,
								COALESCE(cc_vw.confirmation_count, 0) AS current_confirmation_count,
								COALESCE(cc_vw.met_minimium, "no") AS met_minimium,	

								IF (cc_vw.met_minimium = "yes",  "na",  IF (ISNULL(ovt.confirm_status) OR ovt.confirm_status = "", "pending", ovt.confirm_status) ) AS confirm_status,
								kbt.mutation_type,
								mtr.snp_or_indel
								
					FROM 		run_info_table rit

					LEFT JOIN		visit_table vt
					ON 			vt.run_id = rit.run_id

					LEFT JOIN		observed_variant_table ovt
					ON  			rit.run_id = ovt.run_id

					LEFT JOIN 	knowledge_base_table kbt
					ON 			kbt.genes = ovt.genes AND
								kbt.coding = ovt.coding AND
								kbt.amino_acid_change = ovt.amino_acid_change

					LEFT JOIN		mutation_type_ref mtr
					ON 			mtr.cosmic_mutation_type = kbt.mutation_type

					LEFT JOIN 	confirmed_count_vw cc_vw
					ON 			cc_vw.genes = ovt.genes AND
								cc_vw.mutation_type = mtr.snp_or_indel AND 
								cc_vw.ngs_panel_id = "'.$this->sanitize($where['ngs_panel_id']).'"


					WHERE 		ovt.include_in_report = 1 AND
								ovt.run_id = "'.$this->sanitize($where['run_id']).'" AND
								vt.ngs_panel_id = "'.$this->sanitize($where['ngs_panel_id']).'" 


					ORDER BY 		confirm_status DESC
					';
				
					break;

				case 'get-ngs-panel-id':
					$select = 
					'
					SELECT 	ngs_panel_id

					FROM 	run_info_table rit

					JOIN 	visit_table vt
					ON 		rit.run_id = vt.run_id

					WHERE 	rit.run_id = "'.$this->sanitize($where).'"
					';
					break;

				case 'get-confirm-info-for-gene':
					$select = 
					'
					SELECT 	*

					FROM 	confirmed_count_vw cc_vw 

					WHERE 	cc_vw.ngs_panel_id = "'.$this->sanitize($where["ngs_panel_id"]).'" AND
							cc_vw.genes = "'.$this->sanitize($where["genes"]).'" AND
							cc_vw.mutation_type = "'.$this->sanitize($where["snp_or_indel"]).'"
					';					
					break;

				case 'confirm-status-with-ovt-info':
					$select = 
					'
					SELECT 	ovt.genes,
							ovt.coding,
							ovt.amino_acid_change,
							ct.mutation_type,
							IF(ct.confirmation_status = "0", "pending", IF(ct.confirmation_status = "1", "confirmed", IF(ct.confirmation_status = "2", "not confirmed", "error") ) ) AS confirmation_status,
							CONCAT(ut.first_name," " ,ut.last_name) AS user_name

					FROM 	run_info_table rit	

					JOIN 	observed_variant_table ovt	
					ON 		rit.run_id = ovt.run_id

					JOIN 	confirmed_table ct
					ON 		ovt.observed_variant_id = ct.observed_variant_id

					JOIN 	user_table ut
					ON 		ut.user_id = ct.user_id

					WHERE 	rit.run_id = "'.$this->sanitize($where).'"
					';
					break;

				case 'pending-confirmations':
					$select = 
					'
					SELECT 		ct.confirm_id,
								ct.genes,
								ovt.coding,
								ovt.amino_acid_change,
								ct.mutation_type,
								ct.confirmation_status,								
								np.type,
								CONCAT(ut.first_name," " ,ut.last_name) AS user_name

					FROM 		confirmed_table ct

					JOIN 		ngs_panel np
					ON 			np.ngs_panel_id = ct.ngs_panel_id

					LEFT JOIN 	observed_variant_table ovt 
					ON 			ovt.observed_variant_id = ct.observed_variant_id

					JOIN 		user_table ut
					ON 			ut.user_id = ct.user_id

					WHERE 		ct.confirmation_status = 0

					ORDER BY 		ct.time_stamp DESC
					';
					break;
				case 'pending-confirmations-by-id':
					$select = 
					'
					SELECT 	ct.*,
							np.type

					FROM 	confirmed_table ct

					JOIN 	ngs_panel np
					ON 		np.ngs_panel_id = ct.ngs_panel_id

					WHERE 	ct.confirmation_status = 0 AND 
							ct.confirm_id = "'.$this->sanitize($where).'"
					';
					break;
				case 'confirmed-count-vw':
					$select =
					'
					SELECT 	cc_vw.genes,
							cc_vw.mutation_type,
							cc_vw.confirmation_count,
							cc_vw.met_minimium,
							np.type 

					FROM 	confirmed_count_vw cc_vw

					JOIN 	ngs_panel np
					ON 		np.ngs_panel_id = cc_vw.ngs_panel_id

					';
					break;
			}

			if(isset($select))
			{
				$results = array();

				// perform query
				$searchResult = mysqli_query($this->conn, $select);

				if(!$searchResult)
				{
					return false;
				}

				// make results array
				else
				{

					while($row = mysqli_fetch_array($searchResult, $result=MYSQLI_ASSOC))
					{

						$results[] = $row;
					}

					return $results;
				}
			}
		}

		private function MakeWhereClauseFromArray($select, $where, $date_table_name='', $radio_filter_table_name='')
		{
			$where_keys = array_keys($where);

			if ($date_table_name !== '')
			{
				$date_table_name .= '.';
			}
			if ($radio_filter_table_name !== '')
			{
				$radio_filter_table_name .= '.';
			}

			for ($i = 0; $i < sizeOf($where_keys); $i++)
			{
				$key = $where_keys[$i];
				$val = $where[$key];
								
				if ($key === 'start_date')
				{
					$select .= $date_table_name.'run_date >=  STR_TO_DATE("'.$this->sanitize($val).'", "%m/%d/%Y")';
				}

				elseif ($key === 'end_date')
				{
					$select .= $date_table_name.'run_date <= STR_TO_DATE("'.$this->sanitize($val).'", "%m/%d/%Y")';
				}

				elseif ($key === 'radio_filter')
				{
					if ($val !== 'all' && $val !== 'included')
					{
						$select .= $radio_filter_table_name.'filter_status = "'.$this->sanitize($val).'"';
					}
					elseif ($val === 'included')
					{
						$select .= $radio_filter_table_name.'include_in_report = 1';
					}
				}

				elseif ($key === 'genes' || $key === 'coding' || $key === 'amino_acid_change')
				{
					$val_l = explode(', ', $val);

					for ($j=0; $j < sizeOf($val_l); $j++)
					{
						$select .=  $this->sanitize($key).' LIKE "%'.$this->sanitize($val_l[$j]).'%"';

						if ($j < sizeOf($val_l)-1)
						{
							$select .= ' AND ';
						}
					}

				}

				else
				{
					$select .=  $this->sanitize($key).' LIKE "%'.$this->sanitize($val).'%"';
				}

				if ($i < sizeOf($where_keys) - 1 && $key !== 'page')
				{

					// make sure this key and next key is not equal to radio_filter and all
					if (! ($key === 'radio_filter' && $val === 'all')  && 
						!($where_keys[$i+1] === 'radio_filter' && $where[$where_keys[$i+1]] === 'all') )
					{
						$select .= ' AND ';
					}
					
				}
			}

			return $select;
		}


		public function ExportGridCSV($query, $where=NULL)
		{
			switch($query)
			{

				case 'box-grid':
					$select =
					'
					SELECT         	ti.sampleName,
									ti.xLoc,
									ti.yLoc,
									bt.boxName,
									bt.xSize

					FROM           tube_info_vw ti

					LEFT JOIN 		boxTable bt
					ON				ti.boxID = bt.boxID

					WHERE          ti.boxID = "'.$this->sanitize($where).'"
					ORDER BY ti.xLoc, ti.yLoc
					';
					break;
			}
			if(isset($select))
			{
				// perform query
				$searchResult = mysqli_query($this->conn, $select);

				if(!$searchResult)
				{
					return false;
				}

				// make grid array
				else
				{
					$search_array = array();
					$results = array();
					$i = 0;
					while ($row = mysqli_fetch_array($searchResult))
					{
						if ($i == 0)
						{
							$box_size = (int)$row['xSize'];
							$results['box_size'] = $box_size;
							$box_name = $row['boxName'];
							$results['box_name'] = $box_name;

						}

						$i++;

						$search_array[] = $row;
					}

					// make an empty square matrix of the size of the box
					$square_matrix = array();
					$alphabet = range('A', 'Z');

					for($i=0; $i < $box_size+1; $i++)
					{
						// if $i is 0 change index make the first row list of numbers the size of the box
						if ($i === 0)
						{
							$curr_array = range(0,$box_size);
						}
						// Make empty array the size of the size of the box
						else
						{
							$curr_array = array_fill(0, $box_size+1, 0);
							$curr_array[0] = $alphabet[$i-1];

							for ($k=0; $k<sizeof($search_array); $k++)
							{
								$curr_samp = $search_array[$k];

								if ($curr_samp['xLoc'] == $i)
								{
									$curr_array[$curr_samp['yLoc']] = $curr_samp['sampleName'];
								}
							}
						}
						$square_matrix[$i] = $curr_array;
					}
					// add grid to results
					$results['grid'] = $square_matrix;

					return $results;
				}
			}
		}

		public function logoff($user_info)
		{
			session_start();

			$user_info = $_SESSION['user'];

			session_write_close();

			$this->setLoginCookie($user_info, false);

			header('Location:'.REDIRECT_URL.'?page=login');
		}

		public function logLogin($user_info)
		{

			$this->addOrModifyRecord('login_table', $user_info);
		}

		public function login($inputs)
		{
			$emailAddress = $inputs['email_address'];
			$password = md5($inputs['password']);

			$select =
			'
			SELECT 	*

			FROM 	user_table

			WHERE 	email_address = "'.$this->sanitize($emailAddress).'"
			AND 		password = "'.$password.'"
			';

			//////////////////////////////////////////////////////////////////
			// log login in login_table
			//////////////////////////////////////////////////////////////////
			$login_array = array(
					'email_address' 	=> $inputs['email_address'],
					'ip_address'		=> $this->ip_loc['ip'] 		
				);


			$result = array();

			$searchResult = mysqli_query($this->conn, $select);

			///////////////////////////////////////////////////////////
			// Find if there have been more than 5 attempts from email address
			///////////////////////////////////////////////////////////
			$user_login_attempts = $this->listAll('user-num-failed-attempts', $inputs['email_address']);


			if($searchResult->num_rows == 0)
			{
				$login_array['status'] = 'failed';
				$this->logLogin($login_array);
				return false;
			}

			else if (!empty($user_login_attempts) && $user_login_attempts[0]['login_count'] > 5)
			{
				require_once('templates/to_many_login_attempts.php');
			}

			else
			{
				$login_array['status'] = 'passed';
				$this->logLogin($login_array);

				// !!!!!!!!!!!!!Why aren't all cols in the user table present here??????
				$row = mysqli_fetch_array($searchResult);

				// !!!!!!!!!!!NEED TO CHECK OF ACCOUNT IS STATUS LOCKED BEFORE ADDING COOKIE
				$cookie_set = $this->setLoginCookie($row);
				
				define('USER_ID', $_SESSION['user']['user_id']);

				// add to log
				$this->AddLog('login', $emailAddress, $select);

				return $cookie_set;
			}
		}

		public function setLoginCookie($user_info, $create = true)
		{
			if($create)
			{
				// session_start();

					$_SESSION['user'] = $user_info;

				session_write_close();
			}

			else
			{

				$_SESSION = array();

				if(ini_get('session.use_cookies'))
				{
					$params = session_get_cookie_params();
					setcookie(session_name(), '', time() - 42000, $params['path'], $params['domain'],$params['secure'], $params['httponly']);
				}

				session_destroy();
			}
		     return true;
		}

		public function findDuplicate()
		{
			$searchQuery = 'SELECT '.$this->colName.' FROM '.$this->tableName.' WHERE '.$this->colName.' = "'.$this->postInput[$this->colName].'";';

			$searchResult = mysqli_num_rows(mysqli_query($this->conn, $searchQuery));
			return $searchResult;
		}

		private function sanitize($dirty_string)
		{
			$no_ticks_string = str_replace("`","'",$dirty_string);
			$clean_string = mysqli_real_escape_string($this->conn, $no_ticks_string);

			return $clean_string;
		}

		public function TwoWayEncrypt($in_string)
		{

			//  Gets the cipher iv length
			$iv_len = openssl_cipher_iv_length($this->cipher);
			$iv = openssl_random_pseudo_bytes($iv_len);
			$cipher_text_raw = openssl_encrypt($in_string, $this->cipher, $this->cryp_key, $options=OPENSSL_RAW_DATA, $iv);

			// hmac would be nice to add to the encryption but it makes search difficult
			$hmac = hash_hmac('sha256', $cipher_text_raw, $this->cryp_key, $as_binary=true);
			$cipher_text = base64_encode( $iv.$hmac.$cipher_text_raw );

			// $cipher_text = base64_encode( $iv.$cipher_text_raw );
			return $cipher_text;
		}

		public function decrypt($cipher_text)
		{
			// Decode a base64 encoded data.
			$c = base64_decode($cipher_text);

			// Gets the cipher iv length
			$ivlen = openssl_cipher_iv_length($this->cipher);

			// separate encrypted string 
			$iv = substr($c, 0, $ivlen);

			// get hmac by separating encyrpted string
			$hmac = substr($c, $ivlen, $sha2len=32);


			$cipher_text_raw = substr($c, $ivlen+$sha2len);


			$original_plain_text = openssl_decrypt($cipher_text_raw, $this->cipher, $this->cryp_key, $options=OPENSSL_RAW_DATA, $iv);

			$calcmac = hash_hmac('sha256', $cipher_text_raw, $this->cryp_key, $as_binary=true);
			if (hash_equals($hmac, $calcmac))//PHP 5.6+ timing attack safe comparison
			{
			    return $original_plain_text;
			}
		}

		public function decrypt_array($encrypted_array)
		{
			// there might be many values to decrypt in an array.  Use this function to decrypt all values

			$decrypted_array = array();

			for ($i = 0; $i < sizeof($encrypted_array); $i++)
			{

				foreach($encrypted_array[$i] as $key => $value)
				{				
					// some variables are concatenated strings of encryption.  These need to be each decrypted separately
					if ($key === 'patient_name')
					{
						$decrypt_concat = '';
						$split_str = explode(' ', $value);
					
						for($k = 0; $k < sizeof($split_str); $k++)
						{
							$decrypt_concat .= $this->decrypt($split_str[$k]).' ';
						}

						$decrypted_array[$i][$key] = $decrypt_concat;
					
					}

					// encrypted strings will be len 88 so decrypt them
					elseif (strlen($value) === 88)
					{
						$decrypted_val = $this->decrypt($value);
						$decrypted_array[$i][$key] = $decrypted_val;
					}
					else
					{
						$decrypted_array[$i][$key] = $value;
					}
				}
			}

			return $decrypted_array;

		}

		function mysqli_field_name($result, $field_offset)
		{
		    $properties = mysqli_fetch_field_direct($result, $field_offset);
		    return is_object($properties) ? $properties->name : null;
		}



	}

?>
