-- MySQL dump 10.13  Distrib 5.7.19, for Linux (x86_64)
--
-- Host: localhost    Database: urmc_reporter_dev_db
-- ------------------------------------------------------
-- Server version	5.7.19-0ubuntu0.16.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Current Database: `urmc_reporter_dev_db`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `urmc_reporter_dev_db` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `urmc_reporter_dev_db`;

--
-- Table structure for table `classification_table`
--

DROP TABLE IF EXISTS `classification_table`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `classification_table` (
  `classify_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT '0',
  `genes` varchar(30) NOT NULL,
  `coding` varchar(255) NOT NULL,
  `amino_acid_change` varchar(255) NOT NULL,
  `classification` varchar(100) NOT NULL,
  `panel` varchar(50) NOT NULL,
  `time_stamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`classify_id`),
  UNIQUE KEY `classify_id` (`classify_id`),
  KEY `idx_user_id` (`user_id`),
  KEY `idx_genes` (`genes`),
  KEY `idx_coding` (`coding`),
  KEY `idx_amino_acid_change` (`amino_acid_change`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `comment_table`
--

DROP TABLE IF EXISTS `comment_table`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `comment_table` (
  `comment_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `comment_ref` int(11) DEFAULT NULL,
  `comment_type` varchar(30) NOT NULL,
  `comment` text NOT NULL,
  `time_stamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`comment_id`),
  UNIQUE KEY `comment_id` (`comment_id`),
  KEY `idx_user_id` (`user_id`),
  KEY `idx_comment_ref` (`comment_ref`),
  KEY `idx_comment_type` (`comment_type`)
) ENGINE=InnoDB AUTO_INCREMENT=256 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Temporary table structure for view `completed_patient_reports_vw`
--

DROP TABLE IF EXISTS `completed_patient_reports_vw`;
/*!50001 DROP VIEW IF EXISTS `completed_patient_reports_vw`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `completed_patient_reports_vw` AS SELECT 
 1 AS `patient_id`,
 1 AS `run_id`,
 1 AS `patient_name`,
 1 AS `first_name`,
 1 AS `middle_name`,
 1 AS `last_name`,
 1 AS `DOB`,
 1 AS `medical_record_num`,
 1 AS `soft_path_num`,
 1 AS `soft_lab_num`,
 1 AS `mol_num`,
 1 AS `primary_tumor_site`,
 1 AS `test_tissue`,
 1 AS `block`,
 1 AS `tumor_cellularity`,
 1 AS `run_date`,
 1 AS `genes`,
 1 AS `codings`,
 1 AS `amino_acid_changes`,
 1 AS `tiers`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `confirmed_count_vw`
--

DROP TABLE IF EXISTS `confirmed_count_vw`;
/*!50001 DROP VIEW IF EXISTS `confirmed_count_vw`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `confirmed_count_vw` AS SELECT 
 1 AS `genes`,
 1 AS `mutation_type`,
 1 AS `confirmation_count`,
 1 AS `met_minimium`,
 1 AS `ngs_panel_id`*/;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `confirmed_table`
--

DROP TABLE IF EXISTS `confirmed_table`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `confirmed_table` (
  `confirm_id` int(11) NOT NULL AUTO_INCREMENT,
  `observed_variant_id` int(11) DEFAULT NULL,
  `genes` varchar(20) DEFAULT NULL,
  `mutation_type` varchar(10) DEFAULT NULL,
  `confirmation_type` varchar(20) DEFAULT NULL,
  `confirmation_doc` varchar(20) DEFAULT NULL,
  `time_stamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `ngs_panel_id` int(11) DEFAULT NULL,
  `confirmation_status` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`confirm_id`),
  UNIQUE KEY `confirm_id` (`confirm_id`),
  KEY `idx_user_id` (`user_id`),
  KEY `idx_observed_variant_id` (`observed_variant_id`),
  KEY `idx_ngs_panel_id` (`ngs_panel_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `detailed_log_table`
--

DROP TABLE IF EXISTS `detailed_log_table`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `detailed_log_table` (
  `d_log_id` int(11) NOT NULL AUTO_INCREMENT,
  `log_id` int(11) DEFAULT NULL,
  `details` text,
  `time_stamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`d_log_id`),
  UNIQUE KEY `d_log_id` (`d_log_id`),
  KEY `idx_log_id` (`log_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1598 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `diagnosis_table`
--

DROP TABLE IF EXISTS `diagnosis_table`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `diagnosis_table` (
  `diagnosis_id` int(11) NOT NULL AUTO_INCREMENT,
  `diagnosis` varchar(20) DEFAULT 'unknown',
  PRIMARY KEY (`diagnosis_id`),
  UNIQUE KEY `diagnosis_id` (`diagnosis_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `gene_interpt_table`
--

DROP TABLE IF EXISTS `gene_interpt_table`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gene_interpt_table` (
  `gene_interpt_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `interpt` text,
  `gene` varchar(40) DEFAULT NULL,
  `time_stamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`gene_interpt_id`),
  UNIQUE KEY `gene_interpt_id` (`gene_interpt_id`),
  KEY `idx_user_id` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ip_info_table`
--

DROP TABLE IF EXISTS `ip_info_table`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ip_info_table` (
  `ip_info_id` int(11) NOT NULL AUTO_INCREMENT,
  `ip_address` varchar(16) DEFAULT NULL,
  `continent` varchar(50) DEFAULT NULL,
  `country` varchar(50) DEFAULT NULL,
  `state` varchar(50) DEFAULT NULL,
  `city` varchar(50) DEFAULT NULL,
  `time_stamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`ip_info_id`),
  UNIQUE KEY `ip_info_id` (`ip_info_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `knowledge_base_table`
--

DROP TABLE IF EXISTS `knowledge_base_table`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `knowledge_base_table` (
  `knowledge_id` int(11) NOT NULL AUTO_INCREMENT,
  `genes` varchar(20) NOT NULL,
  `coding` varchar(255) NOT NULL,
  `amino_acid_change` varchar(255) NOT NULL,
  `amino_acid_change_abbrev` varchar(255) NOT NULL,
  `chr` varchar(10) DEFAULT NULL,
  `loc` varchar(255) DEFAULT NULL,
  `mutation_type` varchar(100) DEFAULT NULL,
  `pmid` text,
  `cosmic` varchar(255) DEFAULT NULL,
  `transcript` varchar(30) DEFAULT NULL,
  `fathmm_prediction` varchar(255) DEFAULT NULL,
  `fathmm_score` varchar(255) DEFAULT NULL,
  `hgnc` varchar(10) DEFAULT NULL,
  `notes` varchar(255) DEFAULT NULL,
  `exon` varchar(10) DEFAULT NULL,
  `dbsnp` varchar(255) DEFAULT NULL,
  `pfam` varchar(255) DEFAULT NULL,
  `time_stamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`knowledge_id`),
  UNIQUE KEY `knowledge_id` (`knowledge_id`),
  KEY `idx_genes` (`genes`),
  KEY `idx_coding` (`coding`),
  KEY `idx_amino_acid_change` (`amino_acid_change`)
) ENGINE=InnoDB AUTO_INCREMENT=33309 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `log_table`
--

DROP TABLE IF EXISTS `log_table`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `log_table` (
  `log_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `ip_address` varchar(16) DEFAULT NULL,
  `action` varchar(20) DEFAULT NULL,
  `msg` varchar(255) DEFAULT NULL,
  `time_stamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`log_id`),
  UNIQUE KEY `log_id` (`log_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1525 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `login_table`
--

DROP TABLE IF EXISTS `login_table`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `login_table` (
  `login_id` int(11) NOT NULL AUTO_INCREMENT,
  `email_address` varchar(40) NOT NULL,
  `ip_address` varchar(16) DEFAULT NULL,
  `time_stamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status` varchar(20) DEFAULT 'failed',
  PRIMARY KEY (`login_id`),
  UNIQUE KEY `login_id` (`login_id`)
) ENGINE=InnoDB AUTO_INCREMENT=43 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `low_coverage_table`
--

DROP TABLE IF EXISTS `low_coverage_table`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `low_coverage_table` (
  `low_coverage_id` int(11) NOT NULL AUTO_INCREMENT,
  `run_id` int(11) DEFAULT NULL,
  `gene` varchar(30) NOT NULL,
  `Amplicon` varchar(50) NOT NULL,
  `exon` varchar(50) NOT NULL,
  `codons` varchar(50) NOT NULL,
  `depth` varchar(4) NOT NULL,
  `time_stamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`low_coverage_id`),
  UNIQUE KEY `low_coverage_id` (`low_coverage_id`)
) ENGINE=InnoDB AUTO_INCREMENT=89 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `mutation_type_ref`
--

DROP TABLE IF EXISTS `mutation_type_ref`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mutation_type_ref` (
  `mutation_type_id` int(11) NOT NULL AUTO_INCREMENT,
  `cosmic_mutation_type` varchar(35) DEFAULT NULL,
  `snp_or_indel` varchar(7) DEFAULT NULL,
  `time_stamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`mutation_type_id`),
  UNIQUE KEY `mutation_type_id` (`mutation_type_id`),
  KEY `idx_snp_or_indel` (`snp_or_indel`)
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ngs_panel`
--

DROP TABLE IF EXISTS `ngs_panel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ngs_panel` (
  `ngs_panel_id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(50) NOT NULL,
  `method` text,
  `url` varchar(255) DEFAULT NULL,
  `time_stamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `disclaimer` text,
  `limitations` text,
  PRIMARY KEY (`ngs_panel_id`),
  UNIQUE KEY `ngs_panel_id` (`ngs_panel_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `no_snv_summary_table`
--

DROP TABLE IF EXISTS `no_snv_summary_table`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `no_snv_summary_table` (
  `no_snv_summary_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `summary` text,
  `time_stamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`no_snv_summary_id`),
  UNIQUE KEY `no_snv_summary_id` (`no_snv_summary_id`),
  KEY `idx_user_id` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `notes_table`
--

DROP TABLE IF EXISTS `notes_table`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `notes_table` (
  `note_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `note` text,
  `time_stamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`note_id`),
  UNIQUE KEY `note_id` (`note_id`),
  KEY `idx_user_id` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `observed_variant_table`
--

DROP TABLE IF EXISTS `observed_variant_table`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `observed_variant_table` (
  `observed_variant_id` int(11) NOT NULL AUTO_INCREMENT,
  `run_id` int(11) DEFAULT NULL,
  `interpt_id` int(11) NOT NULL DEFAULT '0',
  `genes` varchar(30) DEFAULT NULL,
  `coding` varchar(255) DEFAULT NULL,
  `amino_acid_change` varchar(255) DEFAULT NULL,
  `frequency` varchar(255) DEFAULT NULL,
  `genotype` varchar(255) DEFAULT NULL,
  `allele_coverage` varchar(255) DEFAULT NULL,
  `coverage` int(11) NOT NULL DEFAULT '0',
  `include_in_report` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `tier` varchar(15) DEFAULT NULL,
  `time_stamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `confirm_status` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`observed_variant_id`),
  UNIQUE KEY `observed_variant_id` (`observed_variant_id`),
  KEY `idx_run_id` (`run_id`),
  KEY `idx_interpt_id` (`interpt_id`),
  KEY `idx_genes` (`genes`),
  KEY `idx_coding` (`coding`),
  KEY `idx_amino_acid_change` (`amino_acid_change`)
) ENGINE=InnoDB AUTO_INCREMENT=84 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `patient_table`
--

DROP TABLE IF EXISTS `patient_table`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `patient_table` (
  `patient_id` int(11) NOT NULL AUTO_INCREMENT,
  `last_name` varchar(100) NOT NULL,
  `middle_name` varchar(100) NOT NULL,
  `first_name` varchar(100) NOT NULL,
  `dob` date NOT NULL,
  `medical_record_num` varchar(100) NOT NULL,
  `time_stamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `user_id` int(11) DEFAULT NULL,
  `sex` varchar(1) NOT NULL,
  PRIMARY KEY (`patient_id`),
  UNIQUE KEY `patient_id` (`patient_id`),
  KEY `idx_user_id` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `permission_table`
--

DROP TABLE IF EXISTS `permission_table`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `permission_table` (
  `permission_id` int(11) NOT NULL AUTO_INCREMENT,
  `permission` varchar(15) DEFAULT NULL,
  PRIMARY KEY (`permission_id`),
  UNIQUE KEY `permission_id` (`permission_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `permission_user_xref`
--

DROP TABLE IF EXISTS `permission_user_xref`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `permission_user_xref` (
  `permission_user_xref_id` int(11) NOT NULL AUTO_INCREMENT,
  `permission_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`permission_user_xref_id`),
  UNIQUE KEY `permission_user_xref_id` (`permission_user_xref_id`),
  KEY `idx_user_id` (`user_id`),
  KEY `idx_permission_id` (`permission_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `pre_step_visit`
--

DROP TABLE IF EXISTS `pre_step_visit`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pre_step_visit` (
  `pre_step_visit_id` int(11) NOT NULL AUTO_INCREMENT,
  `visit_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `step` varchar(30) NOT NULL,
  `time_stamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status` varchar(50) NOT NULL,
  `concentration` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`pre_step_visit_id`),
  UNIQUE KEY `pre_step_visit_id` (`pre_step_visit_id`),
  KEY `idx_user_id` (`user_id`),
  KEY `idx_visit_id` (`visit_id`)
) ENGINE=InnoDB AUTO_INCREMENT=150 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `problem_pages_table`
--

DROP TABLE IF EXISTS `problem_pages_table`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `problem_pages_table` (
  `problem_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `problem_page` text,
  `time_stamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`problem_id`),
  UNIQUE KEY `problem_id` (`problem_id`),
  KEY `idx_user_id` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Temporary table structure for view `report_ids_vw`
--

DROP TABLE IF EXISTS `report_ids_vw`;
/*!50001 DROP VIEW IF EXISTS `report_ids_vw`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `report_ids_vw` AS SELECT 
 1 AS `run_id`,
 1 AS `visit_id`,
 1 AS `patient_id`,
 1 AS `ngs_panel_id`*/;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `report_steps_table`
--

DROP TABLE IF EXISTS `report_steps_table`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `report_steps_table` (
  `step_id` int(11) NOT NULL AUTO_INCREMENT,
  `step` varchar(30) NOT NULL,
  `time_stamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`step_id`),
  UNIQUE KEY `step_id` (`step_id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `run_info_table`
--

DROP TABLE IF EXISTS `run_info_table`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `run_info_table` (
  `run_id` int(11) NOT NULL AUTO_INCREMENT,
  `sample_name` varchar(20) DEFAULT NULL,
  `run_date_chip` varchar(30) DEFAULT NULL,
  `status` varchar(30) NOT NULL DEFAULT 'pending',
  `time_stamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `summary` text,
  `run_date` date DEFAULT NULL,
  PRIMARY KEY (`run_id`),
  UNIQUE KEY `run_id` (`run_id`)
) ENGINE=InnoDB AUTO_INCREMENT=94 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `run_xref_gene_interpt`
--

DROP TABLE IF EXISTS `run_xref_gene_interpt`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `run_xref_gene_interpt` (
  `run_xref_gene_interpt_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `gene_interpt_id` int(11) DEFAULT NULL,
  `time_stamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `run_id` int(11) DEFAULT NULL,
  `gene` varchar(40) DEFAULT NULL,
  PRIMARY KEY (`run_xref_gene_interpt_id`),
  UNIQUE KEY `run_xref_gene_interpt_id` (`run_xref_gene_interpt_id`),
  KEY `gene_interpt_id` (`gene_interpt_id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `step_run_xref`
--

DROP TABLE IF EXISTS `step_run_xref`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `step_run_xref` (
  `step_run_xref_id` int(11) NOT NULL AUTO_INCREMENT,
  `run_id` int(11) DEFAULT NULL,
  `step_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `time_stamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`step_run_xref_id`),
  UNIQUE KEY `step_run_xref_id` (`step_run_xref_id`),
  KEY `idx_user_id` (`user_id`),
  KEY `idx_step_id` (`step_id`),
  KEY `idx_run_id` (`run_id`)
) ENGINE=InnoDB AUTO_INCREMENT=100 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tier_table`
--

DROP TABLE IF EXISTS `tier_table`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tier_table` (
  `tier_id` int(11) NOT NULL AUTO_INCREMENT,
  `tier` varchar(10) NOT NULL,
  `tier_summary` varchar(50) NOT NULL,
  PRIMARY KEY (`tier_id`),
  UNIQUE KEY `tier_id` (`tier_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Temporary table structure for view `tier_tissue_knowledge_vw`
--

DROP TABLE IF EXISTS `tier_tissue_knowledge_vw`;
/*!50001 DROP VIEW IF EXISTS `tier_tissue_knowledge_vw`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `tier_tissue_knowledge_vw` AS SELECT 
 1 AS `knowledge_id`,
 1 AS `genes`,
 1 AS `coding`,
 1 AS `amino_acid_change`,
 1 AS `chr`,
 1 AS `loc`,
 1 AS `mutation_type`,
 1 AS `pmid`,
 1 AS `cosmic`,
 1 AS `transcript`,
 1 AS `fathmm_prediction`,
 1 AS `fathmm_score`,
 1 AS `hgnc`,
 1 AS `notes`,
 1 AS `exon`,
 1 AS `dbsnp`,
 1 AS `pfam`,
 1 AS `time_stamp`,
 1 AS `tier_id`,
 1 AS `tier`,
 1 AS `tier_summary`,
 1 AS `tissue_id`,
 1 AS `tissue`,
 1 AS `vt_xref_id`,
 1 AS `status`,
 1 AS `vtx_user_id`*/;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `tissue_type_table`
--

DROP TABLE IF EXISTS `tissue_type_table`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tissue_type_table` (
  `tissue_id` int(11) NOT NULL AUTO_INCREMENT,
  `tissue` varchar(30) NOT NULL,
  PRIMARY KEY (`tissue_id`),
  UNIQUE KEY `tissue_id` (`tissue_id`),
  KEY `idx_tissue` (`tissue`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `top_passwords_used_table`
--

DROP TABLE IF EXISTS `top_passwords_used_table`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `top_passwords_used_table` (
  `top_password_id` int(11) NOT NULL AUTO_INCREMENT,
  `email_address` varchar(40) NOT NULL,
  `password` varchar(60) NOT NULL,
  `time_stamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`top_password_id`),
  UNIQUE KEY `top_password_id` (`top_password_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tumor_type_table`
--

DROP TABLE IF EXISTS `tumor_type_table`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tumor_type_table` (
  `tumor_id` int(11) NOT NULL AUTO_INCREMENT,
  `tumor` varchar(30) NOT NULL,
  PRIMARY KEY (`tumor_id`),
  UNIQUE KEY `tumor_id` (`tumor_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `user_table`
--

DROP TABLE IF EXISTS `user_table`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_table` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(50) NOT NULL,
  `last_name` varchar(50) NOT NULL,
  `email_address` varchar(40) NOT NULL,
  `credentials` varchar(255) DEFAULT NULL,
  `title` varchar(50) DEFAULT NULL,
  `time_stamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `password_need_reset` varchar(1) DEFAULT '1',
  `password` varchar(60) DEFAULT 'ba816f76d6c8402851d3b849fa690892',
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `user_id` (`user_id`),
  UNIQUE KEY `email_address` (`email_address`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `variant_tier_xref`
--

DROP TABLE IF EXISTS `variant_tier_xref`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `variant_tier_xref` (
  `vt_xref_id` int(11) NOT NULL AUTO_INCREMENT,
  `tier_id` int(11) DEFAULT NULL,
  `knowledge_id` int(11) DEFAULT NULL,
  `tissue_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `status` varchar(10) NOT NULL DEFAULT 'active',
  `time_stamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `observed_variant_id` int(11) DEFAULT '0',
  PRIMARY KEY (`vt_xref_id`),
  UNIQUE KEY `vt_xref_id` (`vt_xref_id`),
  KEY `idx_user_id` (`user_id`),
  KEY `idx_tier_id` (`tier_id`),
  KEY `idx_knowledge_id` (`knowledge_id`),
  KEY `idx_tissue_id` (`tissue_id`)
) ENGINE=InnoDB AUTO_INCREMENT=39 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `visit_table`
--

DROP TABLE IF EXISTS `visit_table`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `visit_table` (
  `visit_id` int(11) NOT NULL AUTO_INCREMENT,
  `run_id` int(11) NOT NULL,
  `ngs_panel_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `soft_path_num` varchar(100) NOT NULL,
  `soft_lab_num` varchar(100) NOT NULL,
  `mol_num` varchar(100) NOT NULL,
  `primary_tumor_site` varchar(100) NOT NULL,
  `test_tissue` varchar(100) NOT NULL,
  `block` varchar(100) NOT NULL,
  `tumor_cellularity` int(3) unsigned DEFAULT NULL,
  `time_stamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `patient_id` int(11) DEFAULT NULL,
  `diagnosis_id` int(11) DEFAULT NULL,
  `req_physician` varchar(100) NOT NULL,
  `req_loc` varchar(100) NOT NULL,
  `collected` datetime DEFAULT '1970-01-01 12:00:00',
  `received` datetime DEFAULT '1970-01-01 12:00:00',
  `order_num` varchar(100) NOT NULL,
  `account_num` varchar(100) NOT NULL,
  `chart_num` varchar(100) NOT NULL,
  PRIMARY KEY (`visit_id`),
  UNIQUE KEY `visit_id` (`visit_id`),
  KEY `idx_user_id` (`user_id`),
  KEY `idx_run_id` (`run_id`),
  KEY `idx_ngs_panel_id` (`ngs_panel_id`),
  KEY `idx_patient_id` (`patient_id`),
  KEY `idx_diagnosis_id` (`diagnosis_id`),
  KEY `idx_primary_tumor_site` (`primary_tumor_site`)
) ENGINE=InnoDB AUTO_INCREMENT=42 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Current Database: `urmc_reporter_dev_db`
--

USE `urmc_reporter_dev_db`;

--
-- Final view structure for view `completed_patient_reports_vw`
--

/*!50001 DROP VIEW IF EXISTS `completed_patient_reports_vw`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_unicode_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `completed_patient_reports_vw` AS select `pt`.`patient_id` AS `patient_id`,`rit`.`run_id` AS `run_id`,concat(`pt`.`first_name`,' ',`pt`.`middle_name`,' ',`pt`.`last_name`) AS `patient_name`,`pt`.`first_name` AS `first_name`,`pt`.`middle_name` AS `middle_name`,`pt`.`last_name` AS `last_name`,`pt`.`dob` AS `DOB`,`pt`.`medical_record_num` AS `medical_record_num`,`vt`.`soft_path_num` AS `soft_path_num`,`vt`.`soft_lab_num` AS `soft_lab_num`,`vt`.`mol_num` AS `mol_num`,`vt`.`primary_tumor_site` AS `primary_tumor_site`,`vt`.`test_tissue` AS `test_tissue`,`vt`.`block` AS `block`,`vt`.`tumor_cellularity` AS `tumor_cellularity`,`rit`.`run_date` AS `run_date`,`ovt2`.`genes` AS `genes`,`ovt2`.`codings` AS `codings`,`ovt2`.`amino_acid_changes` AS `amino_acid_changes`,`ovt2`.`tiers` AS `tiers` from (((`urmc_reporter_dev_db`.`patient_table` `pt` left join `urmc_reporter_dev_db`.`visit_table` `vt` on((`vt`.`patient_id` = `pt`.`patient_id`))) left join `urmc_reporter_dev_db`.`run_info_table` `rit` on((`rit`.`run_id` = `vt`.`run_id`))) left join (select group_concat(`ovt`.`genes` separator ', ') AS `genes`,group_concat(`ovt`.`coding` separator ', ') AS `codings`,group_concat(`ovt`.`amino_acid_change` separator ', ') AS `amino_acid_changes`,group_concat(`ovt`.`tier` separator ', ') AS `tiers`,`ovt`.`run_id` AS `run_id` from `urmc_reporter_dev_db`.`observed_variant_table` `ovt` where (`ovt`.`include_in_report` = '1') group by `ovt`.`run_id`) `ovt2` on((`vt`.`run_id` = `ovt2`.`run_id`))) where (`rit`.`status` = 'completed') */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `confirmed_count_vw`
--

/*!50001 DROP VIEW IF EXISTS `confirmed_count_vw`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_unicode_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `confirmed_count_vw` AS select `ct`.`genes` AS `genes`,`ct`.`mutation_type` AS `mutation_type`,count(`ct`.`genes`) AS `confirmation_count`,if((count(`ct`.`genes`) >= 10),'yes','no') AS `met_minimium`,`np`.`ngs_panel_id` AS `ngs_panel_id` from (`confirmed_table` `ct` join `ngs_panel` `np` on((`np`.`ngs_panel_id` = `ct`.`ngs_panel_id`))) where (`ct`.`confirmation_status` = 1) group by `ct`.`genes`,`ct`.`mutation_type`,`np`.`ngs_panel_id` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `report_ids_vw`
--

/*!50001 DROP VIEW IF EXISTS `report_ids_vw`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_unicode_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `report_ids_vw` AS select `rif`.`run_id` AS `run_id`,`vt`.`visit_id` AS `visit_id`,`vt`.`patient_id` AS `patient_id`,`vt`.`ngs_panel_id` AS `ngs_panel_id` from (`run_info_table` `rif` left join `visit_table` `vt` on((`vt`.`run_id` = `rif`.`run_id`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `tier_tissue_knowledge_vw`
--

/*!50001 DROP VIEW IF EXISTS `tier_tissue_knowledge_vw`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_unicode_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `tier_tissue_knowledge_vw` AS select `kbt`.`knowledge_id` AS `knowledge_id`,`kbt`.`genes` AS `genes`,`kbt`.`coding` AS `coding`,`kbt`.`amino_acid_change` AS `amino_acid_change`,`kbt`.`chr` AS `chr`,`kbt`.`loc` AS `loc`,`kbt`.`mutation_type` AS `mutation_type`,`kbt`.`pmid` AS `pmid`,`kbt`.`cosmic` AS `cosmic`,`kbt`.`transcript` AS `transcript`,`kbt`.`fathmm_prediction` AS `fathmm_prediction`,`kbt`.`fathmm_score` AS `fathmm_score`,`kbt`.`hgnc` AS `hgnc`,`kbt`.`notes` AS `notes`,`kbt`.`exon` AS `exon`,`kbt`.`dbsnp` AS `dbsnp`,`kbt`.`pfam` AS `pfam`,`kbt`.`time_stamp` AS `time_stamp`,`tt`.`tier_id` AS `tier_id`,`tt`.`tier` AS `tier`,`tt`.`tier_summary` AS `tier_summary`,`ttt`.`tissue_id` AS `tissue_id`,`ttt`.`tissue` AS `tissue`,`vtx`.`vt_xref_id` AS `vt_xref_id`,`vtx`.`status` AS `status`,`vtx`.`user_id` AS `vtx_user_id` from (((`knowledge_base_table` `kbt` left join `variant_tier_xref` `vtx` on((`vtx`.`knowledge_id` = `kbt`.`knowledge_id`))) left join `tier_table` `tt` on((`vtx`.`tier_id` = `tt`.`tier_id`))) left join `tissue_type_table` `ttt` on((`vtx`.`tissue_id` = `ttt`.`tissue_id`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-06-01 15:45:09
