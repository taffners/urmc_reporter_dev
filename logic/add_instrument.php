<?php
	
	$manufacturers = $db->listAll('manufacturers');
	$tasks = $db->listAll('tasks');

	if ($utils->validateGetInt('instrument_id'))
	{
		$instrumentArray = $db->listAll('instrument-by-id', $_GET['instrument_id']);
		$page_title = 'Update Instrument';
	}
	else
	{
		$instrumentArray = array();

		$page_title = 'Add a New Instrument';
	}

	// Make sure Description numbers are unique
	if 	(
			isset($_POST['add_instrument_submit']) && 
			!isset($_GET['instrument_id']) &&
			!empty($_POST['serial_num']) &&
			sizeOf($db->listAll('required-unique-instrument-field', array('field'=>'serial_num', 'val' =>$_POST['serial_num']))) > 0
		)
	{
		$message = 'Serial Number is required to be Unique.  You entered '.$_POST['serial_num'].' which is already in the database';
	}

	else if 	(
			isset($_POST['add_instrument_submit']) &&
			!isset($_GET['instrument_id']) && 
			!empty($_POST['yellow_tag_num']) &&
			sizeOf($db->listAll('required-unique-instrument-field', array('field'=>'yellow_tag_num', 'val' =>$_POST['yellow_tag_num']))) > 0
		)
	{
		$message = 'Yellow Tag Number is required to be Unique.  You entered '.$_POST['yellow_tag_num'].' which is already in the database';
	}

	else if 	(
			isset($_POST['add_instrument_submit']) && 
			!isset($_GET['instrument_id']) &&
			!empty($_POST['clinical_engineering_num']) &&
			sizeOf($db->listAll('required-unique-instrument-field', array('field'=>'clinical_engineering_num', 'val' =>$_POST['clinical_engineering_num']))) > 0
		)
	{
		$message = 'Clinical Engineering # is required to be Unique.  You entered '.$_POST['clinical_engineering_num'].' which is already in the database';
	}


	else if (isset($_POST['add_instrument_submit']))
	{
		$insert_array = array(
			'task'			=>	$_POST['task'],
			'yellow_tag_num'	=> 	$_POST['yellow_tag_num'],
			'manufacturer'		=> 	$_POST['manufacturer'],
			'model'			=> 	$_POST['model'],
			'description'		=> 	$_POST['description'],
			'serial_num'		=> 	$_POST['serial_num'],
			'clinical_engineering_num'	=> 	$_POST['clinical_engineering_num'],
			'instrument_status'	=> 	$_POST['instrument_status'],
			'user_id'	=> 	USER_ID
		);

		// for updating add instrument_id
		if (isset($_GET['instrument_id']))
		{
			$insert_array['instrument_id'] = $_GET['instrument_id'];
		}

		$add_result = $db->addOrModifyRecord('instrument_table',$insert_array);

		if ($add_result[0] == 1)
		{
			// redirect to list of all instruments
			header('Location:'.REDIRECT_URL.'?page=setup_instruments');
		}
		else
		{
			var_dump($insert_array);
			var_dump($add_result);
			$email_utils->emailAdminsProblemURL(__FILE__.' Error!! Line #: '.__LINE__.' $insert_array: '.implode(',', $insert_array).' $add_result: '.implode(',', $add_result));
		}
	}
?>