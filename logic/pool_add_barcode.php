<?php
	$page_title = 'Add Barcode';

	require_once('logic/shared_logic/library_pool.php');

	// The barcode range was chosen by the user when making the the pool.  Get the 
	// Min and max that the barcode can be based on what was chosen.  This is only 
	// relevant for the OFA panel
	if (isset($_GET['pool_chip_linker_id']) && isset($_GET['ngs_panel_id']) && $_GET['ngs_panel_id'] === '1')
	{
		$min_max_barcode = $db->listAll('min-max-barcode', $_GET['pool_chip_linker_id']);	
// 		$last_used_barcode = $db->listAll('last-used-barcode', array('ngs_panel_id' => $_GET['ngs_panel_id'],'pool_chip_linker_id' => $_GET['pool_chip_linker_id']));
// var_dump($last_used_barcode);
	}
	
	// Add a start barcode
	if (isset($_POST['submit-start-barcode']) && isset($_GET['ngs_panel_id']) && isset($_GET['pool_chip_linker_id']) && !isset($_GET['start_barcode']) && isset($_POST['start_barcode']))
	{
		header('Location:'.REDIRECT_URL.'?page=pool_add_barcode&pool_chip_linker_id='.$_GET['pool_chip_linker_id'].'&ngs_panel_id='.$_GET['ngs_panel_id'].'&start_barcode='.$_POST['start_barcode']);
	}

	// add all barcodes
	if (isset($_POST['pool_add_barcode_submit']))
	{
		for ($i=0; $i<sizeOf($_POST); $i++)
		{
			if (isset($_POST['visit_'.$i]))
			{
				$curr_barcode = $_POST['visit_'.$i];
				$update_result = $db->updateRecord('visits_in_pool_table', 'visits_in_pool_id', $curr_barcode['visits_in_pool_id'], 'barcode_ion_torrent', $curr_barcode['barcode_ion_torrent']);
			}
		}

		// add step completed to step_run_xref if not already completed
		$url = $poolStepTracker->UpdatePoolStepInDb($_GET['page'], $completed_steps, $_GET['pool_chip_linker_id'], USER_ID, $_GET['ngs_panel_id']);

		header('Location:'.REDIRECT_URL.$url);
	}

?>