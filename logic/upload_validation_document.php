<?php
	$allowed = array('pdf', 'PDF','jpg', 'JPG','jpeg','JPEG', 'png', 'PNG', 'doc', 'DOC', 'docx', 'DOCX');

	if (isset($_GET['instrument_id']))
	{
		$instrumentArray = $db->listAll('instrument-by-id', $_GET['instrument_id']);
	}
	else
	{
		$message = 'Something is wrong and instrument was not selected. Do not proceed.';
		$instrumentArray = array();
	}	

	if (isset($_GET['instrument_validation_id']))
	{
		$page_title = 'Update Validation';

		$validationArray = $db->listAll('validations-by-instrument_validation_id', $_GET['instrument_validation_id']);
	}
	else
	{
		$page_title = 'Add a Validation';

		$validationArray = array();
	}

	// submit form
	if 	(	
			isset($_POST['upload_validation_document_submit']) && 
			count($_FILES['validation_files']['name']) >= 1 && 
			$utils->validateGetInt('instrument_id') && 
			isset($_POST['md5_file_hash_array']) &&
			!empty($_POST['md5_file_hash_array']) &&
			$utils->CheckSecurityOfFile($_FILES['validation_files'], $allowed)
		)
	{		
		//////////////////////////////////////////////////////////
		// Validation_dates for each instrument_id are required to be 
		// unique.  If not unique and not instrument_validation_id not provided
		// then do not proceed
		//////////////////////////////////////////////////////////
		$search_array = array(
			'instrument_id' 	=>	$_GET['instrument_id'],
			'validation_date'	=>	$_POST['validation_date']
		);

		$dup_validation_date = $db->listAll('instrument-validations-by-val-date', $search_array);

		if (!empty($dup_validation_date) && !isset($_GET['instrument_validation_id']))
		{
			$message = 'The validation date '.$_POST['validation_date'].' was already used for this instrument.  Please update the validation if more documents need to be added';
		}
		else
		{
			//////////////////////////////////////////////////////////
			// add or update validation date to instrument_validation_table
			//////////////////////////////////////////////////////////
			$add_validation = array(
				'validation_date'	=>	$_POST['validation_date'],
				'user_id'			=> 	USER_ID,	
				'instrument_id'	=>	$_GET['instrument_id']
			);

			if (isset($_GET['instrument_validation_id']))
			{
				$add_validation['instrument_validation_id'] = $_GET['instrument_validation_id'];
			}

			$add_result = $db->addOrModifyRecord('instrument_validation_table',$add_validation);

			// make sure add_result occurred without error
			if ($add_result[0] == 1)
			{
				$instrument_validation_id = $add_result[1];

				foreach ($_FILES['validation_files']['name'] as $key => $filename)
				{
					
					$instrument_val_folder = SERVER_SAVE_LOC.'validation_documents/'.ROOT_URL.'/'.$instrument_validation_id;

					//////////////////////////////////////////////////////
					// Move file to server in a folder named with the instrument_validation_id
					//////////////////////////////////////////////////////

					// Make folder with instrument_validation_id.  All of the 
					// validation files for this validation will be stored here.
					if (!file_exists($instrument_val_folder))
					{
						mkdir($instrument_val_folder);
					}

					// Move file to server
					$destination = $instrument_val_folder.'/'.$filename;
					$temp_file = $_FILES['validation_files']['tmp_name'][$key];

					// make sure not to replace a file.
					if (file_exists($destination))
					{
						if (!isset($message))
						{
							$message = 'The following file(s) are already uploaded.<br>';
						}

						$message .= '&emsp; - '.$filename.'<br>';
					}

					// move uploaded file is a default php function
					// This function checks to ensure that the file designated by filename is a valid upload file (meaning that it was uploaded via PHP's HTTP POST upload mechanism). If the file is valid, it will be moved to the filename given by destination.
					else if (move_uploaded_file($temp_file, $destination))
					{
						//////////////////////////////////////////////////////
						// Check if file transfered correctly
						//////////////////////////////////////////////////////

						// get md5 hash of file moved to server
						$server_md5 = md5_file($destination);

						// Check md5 hash against hashes made in javascript 
						if ($server_md5 === $_POST['md5_file_hash_array'][$filename])
						{
							///////////////////////////////////////////////////
							// Add doc to instrument_validation_doc_table. 
								// Since duplicate files of the same name 
								// will not be allowed to be uploaded this 
								// table will not be updated.
							///////////////////////////////////////////////////
							$insert_array = array(
								'instrument_validation_id' => $instrument_validation_id,
								'user_id'				=> 	USER_ID,
								'validation_file_loc'	=> 	$destination,
								'name_uploaded_as'		=> 	$filename,
								'md5_hash'			=> 	$server_md5,
								'file_size'			=> 	$_FILES['validation_files']['size'][$key]
							);


							$add_result = $db->addOrModifyRecord('instrument_validation_doc_table', $insert_array);

							if ($add_result[0] == 1)
							{
								header('Location:'.REDIRECT_URL.'?page=validation&instrument_id='.$_GET['instrument_id']);
							}
							else
							{
								$email_utils->emailAdminsProblemURL(__FILE__.' Error!! Line #: '.__LINE__.' $insert_array: '.implode(',', $insert_array).' $add_result: '.implode(',', $add_result));
							}
						}
						else
						{
							$email_utils->emailAdminsProblemURL(__FILE__.' Error uploading validation file!! MD5 hashes do not match!! Line #: '.__LINE__.' $server_md5: '.$server_md5.' $_POST["md5_file_hash_array"][$filename]: '.$_POST['md5_file_hash_array'][$filename]);
						}					
					}
					else
					{
						$email_utils->emailAdminsProblemURL(__FILE__.' Error uploading validation file!! Line #: '.__LINE__.' $destination: '.$destination.' $temp_file: '.$temp_file);
					}						
				}
			}
			else
			{
				var_dump($add_validation);
				var_dump($add_result);
				$email_utils->emailAdminsProblemURL(__FILE__.' Error!! Line #: '.__LINE__.' $Add_validation: '.implode(',', $add_validation).' $add_result: '.implode(',', $add_result));
			}			
		}		
	}

?>