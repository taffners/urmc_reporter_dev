<?php
	

	if (!isset($_GET['ngs_panel_id']))
	{
		header('Location:'.REDIRECT_URL.'?page=new_home'); 
	}

	$covered_genes = $db->listAll('genes-in-panel-now', $_GET['ngs_panel_id']);
	
	$ngs_panel_array = $db->listAll('ngs-panel-info-orderable-tests-table', $_GET['ngs_panel_id']);

	if (empty($ngs_panel_array) || !isset($ngs_panel_array[0]['full_test_name']))
	{
		header('Location:'.REDIRECT_URL.'?page=new_home'); 
	}

	$page_title = 'View and add genes to the '.$ngs_panel_array[0]['full_test_name'];


?>