<?php
	$page_title = 'Add FLT3 ITD';

	if 	(
			!$utils->validateGetInt('run_id') ||
			!$utils->validateGetInt('visit_id') ||
			!$utils->validateGetInt('patient_id')
		)
	{
		$fatal_err_occured = True;
	}

	else if (isset($_POST['add_flt3_submit']))
	{
		unset($_POST['add_flt3_submit']);

		$result = $db->addOrModifyRecord('observed_variant_table',$_POST);
		
		// redirect back to the correct page
		if (isset($_GET['previous_page']) && !empty($_GET['previous_page']))
		{
			$previous_page = $_GET['previous_page'];
		}
		else
		{
			$previous_page = 'home';
		}

		


		if (isset($_GET['previous_page']) && $_GET['previous_page'] === 'check_tsv_upload' && $utils->validateGetInt('num_variants'))
		{
			header('Location:'.REDIRECT_URL.'?page='.$previous_page.'&'.EXTRA_GETS);
		}
		else if (isset($_GET['previous_page']) && $_GET['previous_page'] === 'verify_variants')
		{
			header('Location:'.REDIRECT_URL.'?page='.$previous_page.'&'.EXTRA_GETS.'&variant_filter=included');
		}
	}

// http://pathlamp.urmc-sh.rochester.edu/devs/urmc_reporter/?page=check_tsv_upload&run_id=108&visit_id=49&patient_id=36&num_variants=2&ngs_file_name=2291-18_180608_BV6N6.tsv
?>