<?php

	if (isset($_GET['page_id']))
	{

		$pageArray = $qc_db->listAll('qc_db-qc-page-info', $_GET['page_id']);
		$page_title = 'Add a new page validation document for '.$pageArray[0]['page_name'];
		$all_qc_checklist_items = $qc_db->listAll('qc_db-all-qc-checklists');
	
	}
	else
	{
		$page_title = 'Something went wrong!';	
	}

	///////////////////////////////
	// submit form
	///////////////////////////////

	if (
			isset($_POST['add_a_validation_submit']) && 
			isset($_GET['page_id']) && 
			isset($_POST['actual_result']) &&
			!empty($_POST['actual_result'])
		)
	{
		
		///////////////////////////////////////
		// Add a page validation
		/////////////////////////////////////// 
		$add_array = array();

		$add_array['page_id'] = $_GET['page_id'];
		$add_array['user_name'] = $_POST['user_name'];
		$add_array['browser_version'] = $_POST['browser_version'];
		$add_array['website_version'] = $_POST['website_version'];
		$add_array['comment'] = $_POST['comment'];
		$add_array['out_come'] = $_POST['out_come'];

		$page_validation = $qc_db->addOrModifyRecord('page_validation_table',$add_array);


		///////////////////////////////////////
		// Add each step result to page_validation_step_table from $_POST['actual_result']
		/////////////////////////////////////// 
		if ($page_validation[0] )
		{
			
			foreach ($_POST['actual_result'] as $check_list_validation_step_id => $actual_result)
			{
				$add_array = array();
				$add_array['page_validation_id'] = $page_validation[1];	
				$add_array['page_id'] = $_GET['page_id'];	
				$add_array['check_list_validation_step_id'] = $check_list_validation_step_id;
				$add_array['actual_result'] = $actual_result;
				$page_val_result = $qc_db->addOrModifyRecord('page_validation_step_table',$add_array);			
			}

			header('Location:'.REDIRECT_URL.'?page=all_qc_pages');
		}


	}
?>