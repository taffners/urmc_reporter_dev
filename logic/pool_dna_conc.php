<?php

	$page_title = 'Add DNA Concentration to All Pool Visits';
	
	require_once('logic/shared_logic/library_pool.php');

	// DNA_CONC_Instrument has a field_controller.  
	if 	(
			isset($onPanelStepRegulators[0]['on_panel_steps'])&&
			strpos($onPanelStepRegulators[0]['on_panel_steps'],'DNA_CONC_Instrument') !== False && 
			isset($_GET['ngs_panel_id'])
		)
	{
		$search_arr = array(
			'ngs_panel_id' 		=> 	$_GET['ngs_panel_id'],
			'step_regulator_name'	=>	'DNA_CONC_Instrument'

		);

		$dna_conc_field_controller = $db->listAll('dna-conc-field-controller', $search_arr);
	}

	if ($utils->validateGetInt('pool_chip_linker_id'))
	{
		$pool_dna_conc_control_quant = $db->listAll('dna-conc-control-quant', $_GET['pool_chip_linker_id']);
	}

	if (isset($_POST['pool_dna_conc_submit']) || isset($_POST['pool_amp_conc_submit']))
	{
		// this is the submit form for both pages pool_dna_conc and pool_amp_conc
		// arrays are set up with the page as array visit_(i)
		// example for visit: 
			// array (size=4)
			//   'visit_id' => string '82' (length=2)
			//   'pre_step_visit_id' => string '734' (length=3)
			//   'dna_conc' => string '10' (length=2)
			//   'dna_status' => string 'Passed' (length=6)
		// if pool_dna_conc_id is set update that row in the pre_step_visit table that is the pre_step_visit_id.

		if ($_GET['page'] === 'pool_dna_conc')
		{
			$pre_step = 'add_DNA_conc';
		}
		else if ($_GET['page'] === 'pool_amp_conc')
		{
			$pre_step = 'add_amplicon_conc';
		}

		// add or update conc in pre_step_visit table

		for ($i=0; $i < sizeOf($_POST); $i++)
		{

			// update previously added pre_step_visit
			if (isset($_POST['visit_'.$i]) && isset($_POST['visit_'.$i]['conc']) && isset($_POST['visit_'.$i]['pre_step_visit_id']) && !empty($_POST['visit_'.$i]['pre_step_visit_id']))
			{
				$curr_visit = $_POST['visit_'.$i];

				// find data already in database and update
				$search_arr = array(
					'visit_id' 	=> 	$curr_visit['visit_id'],
					'step' 		=>	$pre_step
				);
				$previous = $db->listAll('pre-step-complete', $search_arr);

				$update_arr = $previous[0];
				$update_arr['user_id'] = USER_ID;
				$update_arr['concentration'] = $curr_visit['conc'];
				$update_arr['status'] = $curr_visit['dna_status'];

				$update_result = $db->addOrModifyRecord('pre_step_visit', $update_arr);
			}

			// Add new entry into pre_step_visit 
			// allow empty conc to complete step for controls.  concentration is a varchar(50)
			else if (isset($_POST['visit_'.$i]) && isset($_POST['visit_'.$i]['conc']))
			{
				$curr_visit = $_POST['visit_'.$i];

				$add_arr = array(
					'visit_id' 	=>	$curr_visit['visit_id'],
					'user_id' 	=>	USER_ID,
					'status'		=>	$curr_visit['dna_status'],
					'concentration'=>	$curr_visit['conc'],
					'step'		=>	$pre_step
				);

				$add_result = $db->addOrModifyRecord('pre_step_visit', $add_arr);
			}
		}

		// update control dna_conc_control_quant
		if (isset($_POST['dna_conc_control_quant']) && $_POST['dna_conc_control_quant'] !== $pool_dna_conc_control_quant[0]['dna_conc_control_quant'])
		{

			$db->updateRecord('pool_chip_linker_table', 'pool_chip_linker_id', $_GET['pool_chip_linker_id'], 'dna_conc_control_quant', $_POST['dna_conc_control_quant']);
		}

		// add step completed to step_run_xref if not already completed
		$url = $poolStepTracker->UpdatePoolStepInDb($_GET['page'], $completed_steps, $_GET['pool_chip_linker_id'], USER_ID, $_GET['ngs_panel_id']);

		header('Location:'.REDIRECT_URL.$url);
	}	

?>