<?php
	$page_title = 'Add Sample to Log Book';

	// get all possible sample types
	$all_possible_sample_types = $db->listAll('all-possible-sample-types');
	$all_tissues = $db->listAll('all-tissues');
	$all_login_enhanced_users = $db->listAll('get-all-users-with-a-permission', 'login_enhanced');
	$str_login_enhanced_users = $utils->ConvertAllUsersWithAPermissionToStr($all_login_enhanced_users);

	/////////////////////////////////////////////////////////////////////
	// Add information if this sample was already entered
	/////////////////////////////////////////////////////////////////////
	if (isset($_GET['sample_log_book_id']))
	{
		$searchSampleLogBookArray = array();
		$searchSampleLogBookArray['sample_log_book_id'] = $_GET['sample_log_book_id'];

		if (isset($_GET['visit_id']) && isset($_GET['patient_id']) && !empty($_GET['patient_id']) && !empty($_GET['visit_id']))
		{
			$searchSampleLogBookArray['visit_id'] = $_GET['visit_id'];
			$searchSampleLogBookArray['patient_id'] = $_GET['patient_id'];
		}

		$sampleLogBookArray = $db->listAll('sample-log-book-by-id', $searchSampleLogBookArray);

		require_once('logic/shared_logic/backwards_compatablity_dob_sex.php');

		// get any problems previously reported
		$previousProblems = $db->listAll('problems-sample-log-book', $_GET['sample_log_book_id']);

		$allVistsArray = $db->listAll('all-sample-visits', $_GET['sample_log_book_id']);
	}
	else
	{
		$sampleLogBookArray = array();
	}
	/////////////////////////////////////////////////////////////////////
	// Get next md number
		// format of number is count-year ex. 100-19
		// unfortunately this number is named mol_num in db
	/////////////////////////////////////////////////////////////////////
	$y = date('y');	
	$md_nums_for_year = $db->listAll('get-all-md-num', date($y));
	$last_md_num = 0;

	//  get the largest md number
	foreach ($md_nums_for_year as $key => $md) 
	{
		$curr_md = explode('-', $md['mol_num']);

		if (intval($curr_md[0]) > $last_md_num)
		{
			$last_md_num = intval($curr_md[0]);
		}
	}

	$next_md_num = $last_md_num + 1;
	$full_next_md_num = $next_md_num.'-'.$y;

	/////////////////////////////////////////////////////////////////////////////
	// Make sure whitespace does not occur in the important fields soft_lab_num
		// turn off if this check if sample_type_toggle === anonymous_donor Also make sure 
		// anonymous_donor can not add any test which is not chimerism donor.  Otherwise it 
		// will cause problems.
		// This should be taken care of in javascript file views/events/forms/common_form_classes 
		// but this is another check because whitespace will be very problematic for running OFA.
	/////////////////////////////////////////////////////////////////////////////
	// make sure soft_lab_num does not have white space
	if 	(
			isset($_POST['sample_type_toggle']) &&
			$_POST['sample_type_toggle'] !== 'anonymous_donor' && 
			isset($_POST['add_sample_log_book_submit']) && 
			isset($_POST['soft_lab_num']) && 
			strpos($_POST['soft_lab_num'], ' ') !== false
		)
	{
		$message = 'Soft Lab Number can not contain white spaces.';
	}

	/////////////////////////////////////////////////////////////////////////////
	// CAP SAMPLES:
		// cap samples with a sample_type_toggle == cap_sample 
			// soft_path_num is used as CAP ID and it is required to be unique
	/////////////////////////////////////////////////////////////////////////////
	else if 	(
				isset($_POST['add_sample_log_book_submit']) && 
				!isset($_GET['sample_log_book_id']) && 
				$_POST['sample_type_toggle'] === 'cap_sample' &&
				sizeof($db->listAll('soft-path-num-in-db', $_POST['soft_path_num'])) > 0
			)
	{
		$message = 'The CAP ID '.$_POST['soft_path_num'].' has already been used';
	}

	/////////////////////////////////////////////////////////////////////////////
	// restrictions on MD# which is mol_num in database because I was told they were named opposite 
          // if new sample check not present in db already
          // restrict format to digits-2 num year
          // make sure year is not greater than current year 
          // make sure number not greater than last used number for the year entered.
	/////////////////////////////////////////////////////////////////////////////
	else if 	(
				isset($_POST['add_sample_log_book_submit']) && 
				(
					!isset($_POST['mol_num']) ||
					empty($_POST['mol_num'])
				)
			)
	{
		$message = 'Add MD#';
	}

	// check if the format is correct for mol_num (digits-2 digit year)
	else if (isset($_POST['add_sample_log_book_submit']) && !preg_match('/([0-9])-([0-9]{2})/', $_POST['mol_num']))
	{
		$message = 'The MD# '.$_POST['mol_num'].' does not contain the correct format';
	}

	// check if mol num already in db
	else if 	(
				isset($_POST['add_sample_log_book_submit']) && 
				!isset($_GET['sample_log_book_id']) && 
				sizeof($db->listAll('mol-num-in-db', $_POST['mol_num'])) > 0
			)
	{
		$message = 'The MD# '.$_POST['mol_num'].' has already been used';
	}

	// Check that year is less than or equal to current year
	else if 	(
				isset($_POST['add_sample_log_book_submit']) && 
				!isset($_GET['sample_log_book_id']) && 
				intval(substr($_POST['mol_num'], -2)) > intval(date('y'))
			)
	{
		$message = 'The MD# '.$_POST['mol_num'].' contains a year greater than the current year';
	}

	// if the year is the current one make sure number is <= the last number + 1
	else if 	(
				isset($_POST['add_sample_log_book_submit']) && 
				!isset($_GET['sample_log_book_id']) && 
				intval(substr($_POST['mol_num'], -2)) == intval(date('y')) && 
				intval(explode('-', $_POST['mol_num'])[0]) > $next_md_num

			)
	{
		$message = 'The MD# '.$_POST['mol_num'].' results in skipping a number for the current year.  The next number should be no larger than: '.$full_next_md_num;
	}	

	// make sure mol_num does not have white space
	else if(isset($_POST['add_sample_log_book_submit']) && isset($_POST['mol_num']) && strpos($_POST['mol_num'], ' ') !== false)
	{
		$message = 'MD# can not contain white spaces.';
	}

	/////////////////////////////////////////////////////////////////////////////
	// Make sure the soft_lab_num is unique only if the form is not being updated and sample_type_toggle != anonymous_donor
	/////////////////////////////////////////////////////////////////////////////
	else if (
			isset($_POST['sample_type_toggle']) &&
			$_POST['sample_type_toggle'] !== 'anonymous_donor' && 

			isset($_POST['add_sample_log_book_submit']) &&
			!isset($_GET['possible_sample_types_id']) && 
			!isset($_GET['sample_log_book_id']) && 			 
			isset($_POST['soft_lab_num']) && 
			sizeof($db->listAll('unique-sample-fields', array('field_name' => 'soft_lab_num', 'value' => $_POST['soft_lab_num']))) > 0 )
	{	
		$message = 'The Soft Lab Number needs to be unique.';
	}

	////////////////////////////////////////////////////////////////////////
	// Add a new or update sample to the log book
	//////////////////////////////////////////////////////////////////////// 
	else if 	(	
				isset($_POST['add_sample_log_book_submit']) &&
				isset($_POST['sample_type_toggle'])
			)
	{
		do 
		{
			//////////////////////////////
			// While updating check if any sample has a visit.  If this is true they will need to be 
			// updated also.
				// Update visit_table
					// currently no sample_log_book_id or ordered_test_id is in the visit_table.  For backwards compatibility I will need to use the ordered_test_table to find visit_tables for this sample.  The ordered_test_table table has a sample_log_book_id and a visit_id.  visit_ids equal to zero do not have have a linked visit
						// fields in visit_table but not sample_log_book
							// Account # (account_num) -387 occurrences 
							// mol#: * (order_num)
								// Sample specific taken care of in ajax call.
							// Chart #: (chart_num) - 3 instances 
							// Collected: (collected) - 531 occurrences 
							// Primary Site: (primary_tumor_site) - 594 occurrences
							// Tumor Type: (diagnosis_id) - 1751 occurrences 
							// Requesting physician: (req_physician) 
								// test specific
							// Requesting Location: (req_loc)
								// test specific

				// Update patient_table 
					// check if anything changed for the patient info
						// patient fields
							// last_name 
							// first_name
							// dob
							// medical_record_num
							// sex
						// if patient info did change and no previous report was signed out under this patient then update the info.
						// if patient info did change and previous reports were signed out under this patient then do not allow any updates to occur.
						// If patient info did not changed proceed
			/////////////////////////////	
			if (isset($allVistsArray[0]))
			{
				$patient_break_occured = False;
				$patient_break_occured_identifers_changing = False;
				foreach($allVistsArray as $key => $curr_visit)
				{
					/////////////////////////////
					// Update patient_table associated fields
						// find how many finished reports the patient appears in.  If any do not update.
					/////////////////////////////
					$updating_patient = False;
					// find if patient info different than current
					if 	(
							$curr_visit['last_name'] !== $_POST['last_name'] ||
							$curr_visit['first_name'] !== $_POST['first_name'] ||
							$curr_visit['dob'] !== $dfs->ChangeDateFormatSQL($_POST['dob']) ||
							$curr_visit['medical_record_num'] !== $_POST['medical_record_num'] ||
							$curr_visit['sex'] !== $_POST['sex'] 
						)
					{
						$updating_patient = True;
					}

					$identifiers_modified = False;
					if ($curr_visit['dob'] !== $dfs->ChangeDateFormatSQL($_POST['dob']) ||
							$curr_visit['medical_record_num'] !== $_POST['medical_record_num'])
					{
						$identifiers_modified = True;
					}
						
					if ($updating_patient && isset($curr_visit['patient_id']))
					{
						// find how many finished reports the patient appears in.  If any do not update.
						$previousReports = $db->listAll('num-completed-reports-per-patient', $curr_visit['patient_id']);
				
						if (empty($previousReports))
						{
							$searchArray = array();
							$searchArray['medical_record_num'] = $_POST['medical_record_num'];
							$searchArray['dob'] = $dfs->ChangeDateFormatSQL($_POST['dob']);		
							$searchArray['last_name'] = $_POST['last_name'];
							$searchArray['first_name'] = $_POST['first_name'];
							$searchArray['sex'] = $_POST['sex'];

							// check if patient already added
							$patient_found_array = $db->listAll('find-patient-by-mrn-dob', $searchArray);
			
							// make sure the new patient is not already added.  If a patient is found by identifiers make sure the rest of the patient also matches.  If it doesn't match then break and do not allow submission.  This will need an admin to look into it because there shouldn't be a match that isn't the current for identifiers that doesn't match.  
							if ($identifiers_modified)
							{
		
								// Make sure that the rest of the patient is the same before changing patient ID to the found patient.
								if (!empty($patient_found_array) && isset($patient_found_array[0]['patient_id']) && $curr_visit['patient_id'] !== $patient_found_array[0]['patient_id'])
								{
									$searchArray['patient_id'] = $patient_found_array[0]['patient_id'];

									$patient_found_new_array = $db->listAll('match-patient-by-name-sex', $searchArray);

									if (empty($patient_found_new_array))
									{
										$patient_break_occured_identifers_changing = True;
										break;
									}

									// update patient to the patient already found.
									else
									{
										$db->updateRecord('visit_table', 'visit_id', $curr_visit['visit_id'], 'patient_id', $patient_found_array[0]['patient_id']);
									}
								}

								// If the patient with new identifiers does not exist then make a new patient
								else
								{
									$add_patient_array = $searchArray;
									$add_patient_array['user_id'] = USER_ID;
									$add_result = $db->addOrModifyRecord('patient_table', $add_patient_array);

									if ($add_result[0] == 1)
									{
										$db->updateRecord('visit_table', 'visit_id', $curr_visit['visit_id'], 'patient_id', $add_result[1]);
									}
								}							
							}

							// if we are not updating identifiers then we should be able to just update 
								// we know it will not affect signed out reports because there are none.
							else
							{

								// Make sure that the rest of the patient is the same before changing patient ID to the found patient.
								if (!empty($patient_found_array) && isset($patient_found_array[0]['patient_id']) && $curr_visit['patient_id'] !== $patient_found_array[0]['patient_id'])
								{
									$searchArray['patient_id'] = $patient_found_array[0]['patient_id'];

									$patient_found_new_array = $db->listAll('match-patient-by-name-sex', $searchArray);

									if (empty($patient_found_new_array))
									{
										$patient_break_occured_identifers_changing = True;
										break;
									}

									// update patient to the patient already found.
									else
									{
										$db->updateRecord('visit_table', 'visit_id', $curr_visit['visit_id'], 'patient_id', $patient_found_array[0]['patient_id']);
									}
								}
								else
								{
									$add_patient_array = $searchArray;
									$add_patient_array['user_id'] = USER_ID;
									$add_result = $db->addOrModifyRecord('patient_table', $add_patient_array);

									if ($add_result[0] == 1)
									{
										$db->updateRecord('visit_table', 'visit_id', $curr_visit['visit_id'], 'patient_id', $add_result[1]);
									}
								}								
							}
						}
						else
						{
							$patient_break_occured = True;
							break;
						}
					}

					/////////////////////////////
					// Update visit_table associated fields
						// 'soft_path_num' => string '' (length=0)
						// 'soft_lab_num' => string 'UNK25' (length=5)
						// 'test_tissue' => string 'Colon' (length=5)
						// 'block' => string '' (length=0)
						// 'tumor_cellularity' => null
						// DO NOT UPDATE RECEIVED.  THIS IS LINKED TO TEST NOT SAMPLE.	
					/////////////////////////////		      	
					if (isset($curr_visit['visit_id']) && !empty($curr_visit['visit_id']))
					{
						if (
								$curr_visit['soft_path_num'] !== $_POST['soft_path_num']
							)
						{
							$db->updateRecord('visit_table', 'visit_id', $curr_visit['visit_id'], 'soft_path_num', $_POST['soft_path_num']);
						}

						if (
								$curr_visit['soft_lab_num'] !== $_POST['soft_lab_num']
							)
						{
							$db->updateRecord('visit_table', 'visit_id', $curr_visit['visit_id'], 'soft_lab_num', $_POST['soft_lab_num']);
						}

						if (
								$curr_visit['test_tissue'] !== $_POST['test_tissue']
							)
						{
							$db->updateRecord('visit_table', 'visit_id', $curr_visit['visit_id'], 'test_tissue', $_POST['test_tissue']);
						}

						if (
								$curr_visit['block'] !== $_POST['block']
							)
						{
							$db->updateRecord('visit_table', 'visit_id', $curr_visit['visit_id'], 'block', $_POST['block']);
						}

						if (
								$curr_visit['tumor_cellularity'] !== $_POST['tumor_cellularity']
							)
						{						
							$db->updateRecord('visit_table', 'visit_id', $curr_visit['visit_id'], 'tumor_cellularity', $_POST['tumor_cellularity']);
						}
					}
				}
			}

			if ($patient_break_occured)
			{
				$email_utils->emailAdminsProblemURL(__FILE__.' Error!! Line #: '.__LINE__.' Error: The user is trying to update a patient that has a completed report.  This needs to be addressed by an admin.  The completed report will need to be revised to save the old report a hard PDF copy version.  This hard copy is sometimes placed in a folder that does not link it to the report.  This happens when folder ids is updated such as last name.  Make sure the saved report is linked and sign out the new report stating that the patient info was updated.  If the original report does not appear on the download_report page then the MRN, patient last name, or MD number was changed.  This will need to be changed via command line.  Go to the folder /var/www/urmc_reporter_data/report_versions/.  There will be a bunch of folders inside of this folder.  Each with looking like MRN_E3491040_MD_6190-21_Rettig.  For this example the MRN was updated so infotrack could no longer find the correct report.  To fix this issue I ran the command $ cp -R MRN_E3491040_MD_6190-21_Rettig MRN_E2421395_MD_6190-21_Rettig.  Refer to Report was revised but old report is not visible on download_report page in readme for more info. sample_log_book_id:'.$_POST['sample_log_book_id'].' User id: '.USER_ID.'. Patient ID: '.$curr_visit['patient_id'].'.  New data: last_name: '.$_POST['last_name'].'  first_name: '.$_POST['first_name'].'  dob: '.$_POST['dob'].'  medical_record_num: '.$_POST['medical_record_num'].'  sex: '.$_POST['sex']);

			
				$message =  'Error: This patient has reports that are signed out. To ensure previous reports are not changed the last_name, first_name, dob, MRN, and sex uneditable when previous reports have been completed for this patient. Admins have been notified to fix the issue. <br><br>At this time last_name, first_name, dob, MRN, and sex will not be editable.';
				break;
			}

			if ($patient_break_occured_identifers_changing)
			{
				$email_utils->emailAdminsProblemURL(__FILE__.' Error!! Line #: '.__LINE__.' Error: The user is trying to update a patient that but from patient id: '.$curr_visit['patient_id'].' to patient id '.$patient_found_array[0]['patient_id'].'.  This needs to be addressed by an admin because the name and sex do not match.  sample_log_book_id:'.$_POST['sample_log_book_id'].' User id: '.USER_ID.'. Patient ID: '.$curr_visit['patient_id'].'.  New data: last_name: '.$_POST['last_name'].'  first_name: '.$_POST['first_name'].'  dob: '.$_POST['dob'].'  medical_record_num: '.$_POST['medical_record_num'].'  sex: '.$_POST['sex']);

			
				$message =  'Error: This patient can not be updated at this time.  Admins have been notified to fix the issue. <br><br>At this time last_name, first_name, dob, MRN, and sex will not be editable.';
				break;
			}
		
			$message = '';

			$insertArray = array();
			$insertArray['sample_type_toggle'] = $_POST['sample_type_toggle'];
			$insertArray['possible_sample_types_id'] = $_POST['possible_sample_types_id'];
			$insertArray['user_id'] = USER_ID;

			$insertArray['first_name'] = $_POST['first_name'];
			$insertArray['last_name'] = $_POST['last_name'];
			$insertArray['sex'] = $_POST['sex'];
				
			// change date format for inserting
			$insertArray['dob'] = $dfs->ChangeDateFormatSQL($_POST['dob']);
			$insertArray['soft_path_num'] = $_POST['soft_path_num'];
			$insertArray['soft_lab_num'] = $_POST['soft_lab_num'];
			$insertArray['mol_num'] = $_POST['mol_num'];
			$insertArray['medical_record_num'] = $_POST['medical_record_num'];
			$insertArray['test_tissue'] = $_POST['test_tissue'];		
			$insertArray['date_received'] = $_POST['date_received'];
			$insertArray['block'] = $_POST['block'];
			$insertArray['WBC_count'] = empty($_POST['WBC_count'])? NULL: $_POST['WBC_count'];
			$insertArray['h_and_e_circled'] = $_POST['h_and_e_circled'];
			$insertArray['tumor_cellularity'] = empty($_POST['tumor_cellularity'])? NULL: $_POST['tumor_cellularity'];
			$insertArray['outside_case_num'] = $_POST['outside_case_num'];
			$insertArray['outside_mrn'] = $_POST['outside_mrn'];
			$insertArray['mpi'] = $_POST['mpi'];

			// for updating sample log book
			if ($_POST['sample_log_book_id'] !== '')
			{
				$insertArray['sample_log_book_id'] = $_POST['sample_log_book_id'];
			}

			////////////////////////////////////////////////////////////////////////
			// Add initial login person.
				// Case sample was already logged in before this field was added to software.  It is required to keep the initial login person NULL.
					// $_POST['sample_log_book_id'] === '' (FALSE)
					// $_POST['initial_login_user_id'] === '' (TRUE)
					// update will not occur 
				// Case new sample is being logged in.  Add current user as initial_login_user_id
					// $_POST['sample_log_book_id'] === '' (TRUE)
					// $_POST['initial_login_user_id'] === '' (TRUE)
					// update will occur
				// Case initial_login_user_id is already set.  The sample is being updated.  Set initial_login_user_id as already set id.
					// $_POST['sample_log_book_id'] === '' (FALSE)
					// $_POST['initial_login_user_id'] === '' (FALSE)
					// update will occur
			////////////////////////////////////////////////////////////////////////
			
			// for new samples set initial login user id
			if ($_POST['sample_log_book_id'] === '' && $_POST['initial_login_user_id'] === '')
			{
				$insertArray['initial_login_user_id'] = USER_ID;
			}

			// Set current initial_login_user_id otherwise
			else
			{
				$insertArray['initial_login_user_id'] = $_POST['initial_login_user_id'];
			}


			// Add or update sample to sample_log_book_table
			$add_result = $db->addOrModifyRecord('sample_log_book_table', $insertArray);
			
			if ($add_result[0] == 1)
			{
				// add any problems to the comment table
				if (isset($_POST['problems']) && !$utils->textareaEmpty($_POST['problems']))
				{	
					$commentInsertArray = array();
					$commentInsertArray['user_id'] = USER_ID;
					$commentInsertArray['comment_ref'] =  $add_result[1];
					$commentInsertArray['comment_type'] =  'sample_log_book_table';
					$commentInsertArray['comment'] =  $_POST['problems'];

					$comment_result = $db->addOrModifyRecord('comment_table', $commentInsertArray);
				}
				
				// redirect to log book
				// header('Location:'.REDIRECT_URL.'?page=sample_log_book&filter=all');
				header('Location:'.REDIRECT_URL.'?page=sample_log_book&search='.$_POST['mol_num']);
			}
			else
			{
				var_dump($insertArray);
				var_dump($add_result);
				$email_utils->emailAdminsProblemURL('$add_result[0] != 1');
			}
		} while(false);
	}
?>