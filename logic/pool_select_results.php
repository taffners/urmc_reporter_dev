<?php
	$page_title = 'Transfer Data for Backup and further Analysis';

	require_once('logic/shared_logic/library_pool.php');

	//////////////////////////////////////////////////////////////////////////
	// Use the torrent server API display all of the runs in the last two months and
	// add them to chip A or Chip B on the form depending on if chefSamplePos is 1 or 2.
	//////////////////////////////////////////////////////////////////////////
	if (!isset($_POST['pool_select_results_submit']))
	{

		// obtain all runs in the last two months.
		$most_recent_runs = $API_Utils->TorrentServerRecentResults();
	}

	// notify user if $most_recent_runs is not showing
	if (!$most_recent_runs)
	{
		// $email_utils->emailAdminsProblemURL(__FILE__.' Error accessing torrent server!! Line #: '.__LINE__.' doc_table info not returned.');
	}

	if (isset($_GET['pool_chip_linker_id']))
	{
		// find if there's already an entry in the torrent_results_table for this pool_chip_linker_id
		$torrent_results_array = $db->listAll('torrent-results-pool-chip-linker', $_GET['pool_chip_linker_id']);
	}


	// Fields are checked if filled out via html.  Redirect to the pool  page to obtain 
	// pool QC run
	if 	(
			isset($_POST['pool_select_results_submit']) &&
			isset($_GET['pool_chip_linker_id']) && 
			isset($_GET['ngs_panel_id']) && 
			isset($_GET['page'])
		)
	{

		if (empty($torrent_results_array))
		{
			$insert_array = array();
		}

		else
		{
			$insert_array = $torrent_results_array[0];
		}
		
		$insert_array['user_id'] = USER_ID;
		$insert_array['pool_chip_linker_id'] = $_GET['pool_chip_linker_id'];
		$insert_array['chipA_results_name'] = $_POST['chipA_results_name'];
		$insert_array['chipB_results_name'] = $_POST['chipB_results_name'];


		$torrent_result = $db->addOrModifyRecord('torrent_results_table',$insert_array);

          if($torrent_result[0] == 1)
          { 
          	$url = $poolStepTracker->UpdatePoolStepInDb($_GET['page'], $completed_steps, $_GET['pool_chip_linker_id'], USER_ID, $_GET['ngs_panel_id']);

			// redirect to pool_qc_run
			header('Location:'.REDIRECT_URL.$url);

          }
          else
          {
          	$message = 'Something went wrong with selecting results';
          }	
	}

?>