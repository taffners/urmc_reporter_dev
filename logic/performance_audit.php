<?php

	$page_title = 'Performance Audits';
	
	$utils->doesUserHavePermission($user_permssions, 'audits');

	$current_address = explode('?page=', $_SERVER['REQUEST_URI'])[1];
	$productionDays = new WorkDays();
	$tests = $db->listAll('tests-with-turnaround-time-tracked');
	////////////////////////////////////////////////////////////
	// Page title Changes based on filter.  Above is just a default title.
	////////////////////////////////////////////////////////////

	// Filter based on month and year
	if 	(
			isset($_GET['filter']) && $_GET['filter'] === 'months' &&
			isset($_GET['month']) && isset($_GET['year']) 
		)
	{
		$date_obj   = DateTime::createFromFormat('!m', $_GET['month']);
		$selected_month_name = $date_obj->format('F');

		$page_title = 'Performance Audits: '.$selected_month_name.' '.$_GET['year'];

		// set up search array
		$search_array = array(
			'filter'	=> 	$_GET['filter'],
			'month'	=>	$_GET['month'],
			'year'	=> 	$_GET['year']
		);

		$failed_turnaround = $db->listAll('audit-tests-failed-turnaround-curr-month', $search_array);

		if (isset($_GET['all_turn_around_times']) || isset($_GET['tracked_tests']))
		{
			$failed_turnaround = $db->listAll('audit-tests-turnaround-curr-month', $search_array);
		}
	
	}

	// Get all data
	else if (isset($_GET['filter']) && $_GET['filter'] === 'all')
	{
		$page_title = 'Performance Audits: All Time';

		$failed_turnaround = $db->listAll('audit-tests-failed-turnaround-all');

		if (isset($_GET['all_turn_around_times']) || isset($_GET['tracked_tests']))
		{
			$failed_turnaround = $db->listAll('audit-tests-turnaround-all');
		}
		
		// set up search array
		$search_array = array(
			'filter'	=> 	$_GET['filter'],
			'month'	=>	'',
			'year'	=> 	''
		);
	}

	////////////////////////////////////////////////////////////
	// Make all the queries for the tables and single line stats here.
	// All charts are queried via an ajax to use google charts.
	////////////////////////////////////////////////////////////
	if (isset($_GET['filter']))
	{	
		// get total number of tests
		$num_samples = $db->listAll('audit-num-samples', $search_array);

		$num_tests = $db->listAll('audit-num-tests', $search_array);

		$turnaround_counts = $db->listAll('audit-month-counts-turn-around-time',$search_array);	
		$turnaround_stats = $db->listAll('audit-month-turn-around-time-test-stats',$search_array);	
		
		$num_tissue_counts = $db->listAll('audit-tissue-counts-turn-around-time', $search_array);

		$samples_per_test = $db->listAll('audit-samples-per-test', $search_array);
		$tissues_per_test = $db->listAll('audit-samples-tissues-per-test', $search_array);		
	}


	


?>