<?php
	$page_title = 'Report a bug fix';
	
	$all_qc_pages = $qc_db->listAll('qc_db-all-qc-pages', SITE_TITLE);

	if (isset($_GET['bugs_reported_id']))
	{
		$bug_info = $all_bugs = $qc_db->listAll('qc_db-bug-by-id', $_GET['bugs_reported_id']);	
	}
	else
	{
		header('Location:'.REDIRECT_URL.'?page=home');
	}


	if (isset($_POST['report_bug_fix_submit']))
	{

		$add_array = array();
		$add_array['bugs_reported_id'] = $_GET['bugs_reported_id'];
		$add_array['commit_id'] = $_POST['commit_id'];
		$add_array['user_name'] =$_SESSION['user']['first_name'].' '.$_SESSION['user']['last_name'];
		$add_array['website_name'] = SITE_TITLE;
		$add_array['summary_of_bug'] = $_POST['summary_of_bug'];
		$add_array['resulted_in_downtime'] = $_POST['resulted_in_downtime'];
		$add_array['downtime_type'] = $_POST['downtime_type'];
		$add_array['downtime_start_time'] = $_POST['downtime_start_time'];
		$add_array['downtime_end_time'] = $_POST['downtime_end_time'];
		$add_result = $qc_db->addOrModifyRecord('bug_fix_table',$add_array);

		if($add_result[0] == 1)
		{
			$bug_fix_id = $add_result[1];

			foreach ($_POST['page_id'] as $key => $page_id)
			{
				$add_array['bug_fix_id'] = $bug_fix_id;
				$add_array['page_id'] = $page_id;

				$add_result = $qc_db->addOrModifyRecord('pages_bug_fix_xref_table',$add_array);
			}

			$qc_db->updateRecord('bugs_reported_table', 'bugs_reported_id', $_GET['bugs_reported_id'], 'status', 'complete');

			header('Location:'.REDIRECT_URL.'?page=bug_list');
		}
		else
		{
			$message = 'Something went wrong';
		}

var_dump($add_result);


	}


?>