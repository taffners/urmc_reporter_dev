<?php
	$page_title = 'Upload Data';

	require_once('logic/shared_logic/library_pool.php');

	$curr_epoch = time();

	if (isset($_GET['pool_chip_linker_id']))
	{
		$pool_info = $db->listAll('pool-chip-linker-by-id', $_GET['pool_chip_linker_id']);

		// find if there's already an entry in the torrent_results_table for this pool_chip_linker_id
		$torrent_results_array = $db->listAll('torrent-results-pool-chip-linker', $_GET['pool_chip_linker_id']);

		$library_pools = $db->listAll('library-pools', $_GET['pool_chip_linker_id']);
	
		$errors = array();
		
		//////////////////////////////////////////////////////////////////////////////
		// If this form was already loaded once and class.thermo_check_backup determined transfer was fine 
		// it will redirect to a page with $_GET['data_backup_complete'] == 1. 
		// Under this mode the data can be uploaded
		//////////////////////////////////////////////////////////////////////////////
		if (isset($_GET['data_backup_complete']) && $_GET['data_backup_complete'] == 1)
		{

		}

		//////////////////////////////////////////////////////////////////////////////
		// The class.thermo_check_backup will redirect to a data_backup_complete 0 if something went wrong with the back up
		// Data upload will not be possible at this point. Just notify user that about the issue.  In the future 
		// I need to find a way to start over.  At tpending_pre_runshis point just update the failed pool library backup_status to failed
		//////////////////////////////////////////////////////////////////////////////
		else if (isset($_GET['data_backup_complete']) && $_GET['data_backup_complete'] == 0)
		{

		}

		//////////////////////////////////////////////////////////////////////////////
		// access class.thermo_check_backup for each chip and access data_backup status
		//////////////////////////////////////////////////////////////////////////////
		else if (	
					!isset($_GET['data_backup_complete']) && 
					isset($pool_info[0]['data_backup_epoch_time']) &&
					isset($library_pools) &&
					!empty($library_pools)
				)
		{
	
			$Thermo_check_backup = new ThermoCheckBackup($library_pools, $pool_info[0]['data_backup_epoch_time']);

			$chip_errors = $Thermo_check_backup->getErrors();

			// get library id and add errors to errors array
			if (!empty($chip_errors))
			{
				$library_pool_id = array_keys($chip_errors)[0];

				$errors[$library_pool_id] = $chip_errors[$library_pool_id];

				// if fatal_error occurred update database.  Add fatal_error to backup_status

				
			}
		}			
	}
	else
	{
		$message = 'Something went wrong.';
	}
	
?>