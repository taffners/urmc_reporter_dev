<?php
	$page_title = 'Update Report';
	require_once('logic/shared_logic/report_shared_logic.php');
	
	if (isset($_GET['run_id']) )
	{
		// find ngs_panel_id from run_id
		$ngs_panel_id = $db->listAll('get-ngs-panel-id', $_GET['run_id']);
		$ngs_panel_id = $ngs_panel_id[0]['ngs_panel_id'];

		$query_variables = array(
				'run_id' 		=> 	$_GET['run_id'],
				'ngs_panel_id' => 	$ngs_panel_id
		);

		$variants_needing_confirm = $db->listAll('confirmed-variants-minimum-not-met', $query_variables);
	}

	//////////////////////////////////////////////////////////////////////
	// Add variants to confirmed_table.  Since a new variant could always be
	// added to the report after the Done btn is pushed this step running can not be
	// dependent on the $curr_step_complete variable.  Therefore a check will 
	// be performed to see if observed variant is already in the confirmed_table
	// before adding it.  This will ensure variants are not added to confirmed_table
	// more than once.
	////////////////////////////////////////////////////////////////////// 
	if 	(
			isset($_POST['check_required_confirm_submit']) &&
			isset($variants_needing_confirm) &&
			!empty($variants_needing_confirm)
		)
	{

		// add each group as a new pending row in the confirmed_table
		foreach($variants_needing_confirm as $key => $variant)
		{
			// check if observed_variant_id in confirmed_table
			$search_check = $db->listAll('confirmed-by-observed-variant-id', $variant['observed_variant_id']);

			if (empty($search_check))
			{
				// when the mutation_type is unknown the user will be required to answer find observed_variant_id = update_snp_or_indel
				if (empty($variant['snp_or_indel']))
				{
					$variant['snp_or_indel'] = $_POST['update_snp_or_indel'][$variant['observed_variant_id']];
				}

				if ($variant['snp_or_indel'] !== 'Skip Classify/Validation')
				{
					$add_array = array(
						'observed_variant_id' 	=> 	$variant['observed_variant_id'],
						'genes' 				=> 	$variant['genes'],
						'mutation_type' 		=> 	$variant['snp_or_indel'],
						'user_id'				=>	USER_ID,
						'confirmation_status' 	=>	'0', // for pending confirmation
						'ngs_panel_id' 		=>	$ngs_panel_id
					);
					// add new confirmed row to the confirmed_table
					$confirm_result = $db->addOrModifyRecord('confirmed_table', $add_array);
				}				
			}
		}

		if (!$curr_step_complete)
		{
			// add step completed to step_run_xref if not already completed
     		$url = $stepTracker->UpdateStepInDb($page, $completed_steps, RUN_ID, USER_ID);

     		header('Location:'.REDIRECT_URL.$url.'?page='.$next_step.'&run_id='.$_GET['run_id'].'&patient_id='.$_GET['patient_id'].'&visit_id='.$_GET['visit_id']);
		}
		else
		{
			$next_step = $stepTracker->FindNextStep($page);
			header('Location:'.REDIRECT_URL.'?page='.$next_step.'&run_id='.$_GET['run_id'].'&patient_id='.$_GET['patient_id'].'&visit_id='.$_GET['visit_id']);
		}     	
	}

	// if the done button is pushed and 
	// there are not any variants to confirm complete step and proceed to next step
	else if (	
			isset($_POST['check_required_confirm_submit']) &&
			!$curr_step_complete &&
			isset($variants_needing_confirm) &&
			empty($variants_needing_confirm)
		   )
	{
		// add step completed to step_run_xref if not already completed
     	$url = $stepTracker->UpdateStepInDb($page, $completed_steps, RUN_ID, USER_ID);

     	header('Location:'.REDIRECT_URL.$url.'?page='.$next_step.'&run_id='.$_GET['run_id'].'&patient_id='.$_GET['patient_id'].'&visit_id='.$_GET['visit_id']);
	}
	// if done btn pushed and step compete proceed to next step
	else if (isset($_POST['check_required_confirm_submit']) &&
			$curr_step_complete)
	{
		$next_step = $stepTracker->FindNextStep($page);
		header('Location:'.REDIRECT_URL.'?page='.$next_step.'&run_id='.$_GET['run_id'].'&patient_id='.$_GET['patient_id'].'&visit_id='.$_GET['visit_id']);
	}

?>