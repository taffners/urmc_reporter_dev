<?php

	require_once('logic/shared_logic/report_shared_logic.php');

	$stopReportingOptions = $db->listAll('all-possible-stop-run-reasons');

	// query db for all report_steps for this run 
	if (isset($_GET) && isset($_GET['visit_id']) && isset($_GET['run_id']) && isset($_GET['patient_id']))
	{
		// add interpretation 
		$reportArray = $db->listAll('report-interpt', $_GET['run_id']);	
	}
	else
	{
		header('Location:'.REDIRECT_URL.'?page=home');
	}


	if (isset($_POST['stop_reporting_submit']) && isset($_POST['run_id']) && !empty($_POST['run_id']))
	{
		// find run_info_table record associate with the run_id
		$run_info_record =  $db->listAll('run-info-record-only', $_POST['run_id']);

		if(!empty($run_info_record))
		{
			$run_info_record[0]['status'] = $_POST['status'];

			$visit_result = $db->addOrModifyRecord('run_info_table',$run_info_record[0]);
			header('Location:'.REDIRECT_URL.'?page=home');
		}
          else
          {
			$message = 'Can not find previous record';
          }
	}
	else if (isset($_POST['stop_reporting_submit']) && empty($_POST['run_id']))
	{
		$message = 'Something went wrong with run_id';
	}
?>