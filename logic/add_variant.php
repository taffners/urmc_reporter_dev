<?php
	$page_title = 'Add Variant';
	$gene_list = $db->listAll('unique-gene-list');
	
	if (isset($_POST['add_variant_submit']))
	{
		unset($_POST['add_variant_submit']);
		unset($_POST['unknown_protien']);
		
		//////////////////////////////////////////////////////////////
		// Make sure all coding starts with c. and all protien starts with p. 
		// otherwise throw and error
		//////////////////////////////////////////////////////////////
		if (!preg_match("/^c./", $_POST['coding']))
		{
			$message = 'Please Make sure coding starts with c.';
		}
		else if (!preg_match("/^p./", $_POST['amino_acid_change']))
		{
			$message = 'Please Make sure protein starts with p.';
		}

		//////////////////////////////////////////////////////////////
		// Add Variant to run and redirect back to check_tsv_upload page
		//////////////////////////////////////////////////////////////
		else
		{
			
			// tab characters were being copied into the coding field.  So here I am scrubbing input of 
			// tab characters.
			$_POST['coding'] = trim(preg_replace('/\t/', '', $_POST['coding']));
			$_POST['amino_acid_change'] = trim(preg_replace('/\t/', '', $_POST['amino_acid_change']));
			
			$result = $db->addOrModifyRecord('observed_variant_table',$_POST);

			// redirect back to the correct page
			$previous_page = $_GET['previous_page'];


			if (isset($_GET['previous_page']) && $_GET['previous_page'] === 'check_tsv_upload')
			{
				header('Location:'.REDIRECT_URL.'?page='.$previous_page.'&'.EXTRA_GETS.'&num_variants='.$_GET['num_variants'].'&ngs_file_name='.$_GET['ngs_file_name']);
			}
			else if (isset($_GET['previous_page']) && $_GET['previous_page'] === 'verify_variants')
			{
				header('Location:'.REDIRECT_URL.'?page='.$previous_page.'&'.EXTRA_GETS.'&variant_filter=included');
			}
		}
	}

// http://pathlamp.urmc-sh.rochester.edu/devs/urmc_reporter/?page=check_tsv_upload&run_id=108&visit_id=49&patient_id=36&num_variants=2&ngs_file_name=2291-18_180608_BV6N6.tsv
?>