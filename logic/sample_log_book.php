<?php
	$page_title = 'Sample Log Book';
	
	// If the user does not have NGS permissions the sample log book is shown on 
	// the home page.  Therefore _GET['filter'] will not be set.  Therefore set
	// it to pending since that is default.
	$sampleLogBookArray = '';
	
	if 	(
			(
				isset($_GET['search']) &&  !empty($_GET['search']) 
			) ||
			(
				isset($_GET['search_start_date']) &&  !empty($_GET['search_start_date']) 
			) ||
			(
				isset($_GET['search_end_date']) &&  !empty($_GET['search_end_date']) 
			) 

		)
	{
		$search_arr = array();

		$search_arr['search'] = isset($_GET['search']) ? $_GET['search'] : '';
		$search_arr['search_start_date'] = isset($_GET['search_start_date']) ? $_GET['search_start_date'] : '';
		$search_arr['search_end_date'] = isset($_GET['search_end_date']) ? $_GET['search_end_date'] : '';

		$sampleLogBookArray = $db->listAll('search-samples-in-log-book', $search_arr);
	}
	// get all of the samples with status of test chosen.
	else if (isset($_GET['filter']) && $_GET['filter'] === 'pending')
	{
		if (isset($_GET['test_filter']))
		{
			$sampleLogBookArray = $db->listAll('filtered-samples-in-log-book', 'pending');
		}
		else
		{
			$sampleLogBookArray = $db->listAll('filtered-samples-in-log-book', 'pending');	
		}
		
	}
	else if (isset($_GET['filter']) && $_GET['filter'] === 'waiting')
	{
		$sampleLogBookArray = $db->listAll('filtered-samples-in-log-book', 'waiting');
	}

	else if (isset($_GET['filter']) && $_GET['filter'] === 'in_progress')
	{
		$sampleLogBookArray = $db->listAll('filtered-samples-in-log-book', 'In progress');
	}
	else if (isset($_GET['filter']) && $_GET['filter'] === 'dna_extraction_only')
	{
		$sampleLogBookArray = $db->listAll('filter-no-tests-ordered-samples-in-log-book');		
	}
	else if (isset($_GET['filter']) &&  $_GET['filter'] === 'all')
	{
		$sampleLogBookArray = $db->listAll('all-samples-in-log-book');
	}

	require_once('logic/shared_logic/search_sample_log_book_submit.php');
?>