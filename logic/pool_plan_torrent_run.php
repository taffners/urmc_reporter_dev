<?php
	$page_title = 'Add Barcode';

	require_once('logic/shared_logic/library_pool.php');

	$plan_date = date('Ymj');

	$template_name = 'Ion Chef S530 V2';
	$library_kit = 'Ion AmpliSeq 2.0 Library Kit';
	$sequence_kit = 'Ion S5 Sequencing Kit';

// /opt/ion/iondb/templates/rundb/plan
	// This is used to make the run template tables on the html page
	if (isset($visits_in_pool))
	{
		$plan_A = array(
			'library'			=>	'hg19',
			'planName'		=>	'OFA_DNA_530_p190806',
			'sample'			=>	$plan_date.'A_'.$template_name,
			'chipType'		=>	'530',
			'sequencekitname'	=>	$sequence_kit,
			'librarykitname'	=>	$library_kit,
			'templatingKitName'	=>	$template_name,
			'barcodeId'		=>	'IonXpress',
			'barcodedSamples'	=>	array()

			// 'irOptions'			=>	'5.10',
			// 'irworkflow'			=>	'OFA_5-10_complex_v3_filter_updated',
			// 'sampleGrouping'		=> 	'2',
			// 'iru_UploadMode'		=>	'no_check',
			// 'applicationGroupName'	=>	'DNA',
			// 'runType'				=>	'2',	
			// 'flows'				=>	'400',
			// 'instrumentType'		=>	'S5',
			// 'templatekitname'		=>	'Ion 510 & Ion 520 & Ion 530 Kit-Chef',
			// 'templatekitType'		=>	'IonChef',
			// 'advancedSettingsChoice'	=>	'default',
			// 'non_export_plugin'		=>	'58',
			// 'non_export_plugin'		=>	'70'	
		);

		$plan_B = array(
			'library'			=>	'hg19',
			'planName'		=>	$plan_date.'B_'.$template_name,
			'sample'			=>	$plan_date.'B_'.$template_name,
			'chipType'		=>	'530',
			'sequencekitname'	=>	$sequence_kit,
			'librarykitname'	=>	$library_kit,
			'templatingKitName'	=>	$template_name,
			'barcodeId'		=>	'IonXpress',
			'barcodedSamples'	=>	array()
		);

		foreach ($visits_in_pool as $key => $visit)
		{
			$curr_samp = array(
				$visit['sample_name']	=>	array(
					'barcodeSampleInfo'	=> 	array(
					intval($visit['barcode_ion_torrent']) > 9 ? 'IonXpress_0'.$visit['barcode_ion_torrent'] : 'IonXpress_00'.$visit['barcode_ion_torrent'] => array(
							'description' 			=>	'',
							'hotSpotRegionBedFile'	=>	'/results/uploads/BED/9/hg19/unmerged/detail/Oncomine_Focus.20160219.hotspots.bed',
							'nucleotideType'		=>	'DNA',
							'reference'			=>	'hg19',
							'targetRegionBedFile'	=>	'/results/uploads/BED/2/hg19/unmerged/detail/Oncomine_Focus_DNA_20160219_designed.bed'
						)
					)
				)
			);
			
			if ($visit['chip_flow_cell'] == 'A')
			{
				array_push($plan_A['barcodedSamples'], $curr_samp);	
			}
			
			else if ($visit['chip_flow_cell'] == 'B')
			{
				array_push($plan_B['barcodedSamples'], $curr_samp);	
			}
        	}

        	//$test_headers = {"Authorization": "ApiKey " + USERNAME + ":" + API_KEY}

		$plan_B = array(
		    'library' => 'hg19',
		    'planName' => 'DOCS_my_plan',
		    'sample' => 'my_sample',
		    'chipType' => '520',
		    'sequencekitname' => 'Ion S5 Sequencing Kit',
		    'librarykitname' => 'Ion Xpress Plus Fragment Library Kit',
		    'templatingKitName' => 'Ion 520/530 Kit-OT2',
		    'barcodeId' => 'IonXpress',
		    'barcodedSamples' => array(
		        'demo sample 1' => array(
		            'barcodeSampleInfo' => array(
		                'IonXpress_003' => array(
		                    'description' => 'description here',
		                    'hotSpotRegionBedFile' => '',
		                    'nucleotideType' => 'DNA',
		                    'reference' => 'hg19',
		                    'targetRegionBedFile' => ''
		                )
		            ),
		            'barcodes' => array('IonXpress_003')
		        ),
		        'demo sample 2' => array(
		            'barcodeSampleInfo' => array(
		                'IonXpress_004' => array(
		                    'description' => 'description here',
		                    'hotSpotRegionBedFile' => '',
		                    'nucleotideType' => 'DNA',
		                    'reference' => 'hg19',
		                    'targetRegionBedFile' => ''
		                )
		            ),
		            'barcodes' => array('IonXpress_004')
		        )
		    )
		);

		$plan_c = array(
			'library' => 'hg19',
			'planName' => 'DOCS_my_plan',
			'sample' => 'my_sample',
			'chipType' => '520',
			'sequencekitname' => 'Ion S5 Sequencing Kit',
			'librarykitname' => 'Ion Xpress Plus Fragment Library Kit',
			'templatingKitName' => 'Ion 520/530 Kit-OT2'
		);

        	// Post Chip A plan
		function post_request($url, array $params) 
		{
			$query_content = http_build_query($params);

			$ch = curl_init($url);

			curl_setopt($ch,CURLOPT_URL, $url);
			curl_setopt($ch,CURLOPT_POST, true);
			//curl_setopt($ch,CURLINFO_HEADER_OUT, true);
			curl_setopt($ch,CURLOPT_POSTFIELDS, $query_content);
			curl_setopt($ch,CURLOPT_RETURNTRANSFER, true); 

			$result = curl_exec($ch);

			var_dump($result);

/*
			$fp = fopen($url, 'r', FALSE, // do not use_include_path
			stream_context_create(
			[
				'http' => 
				[
					'header'  => 
					[ // header array does not need '\r\n'
						'Content-type: application/x-www-form-urlencoded'
					],
					'method'  => 'POST',
					'content' => $query_content
				]
			]));

			if ($fp === FALSE) 
			{
				return json_encode(['error' => 'Failed to get contents...']);
			}
			
			$result = stream_get_contents($fp); // no maxlength/offset
			fclose($fp);
*/
			return $result;
		}

		$url = 'http://10.149.58.208/rundb/api/v1/plannedexperiment/?username=ionadmin&password=ionadmin&api_key=3f2cdec72f8b1783dfc1222cb4f4164c79de53a7';
		
		post_request($url,$plan_A);
		post_request($url,$plan_B);
		post_request($url,$plan_c);
var_dump($plan_A['barcodedSamples'][5]['2291-18_H3245719']);
// var_dump($plan_B);        	
	}

	//////////////////////////////////////////////////////////
	// Generate the sample run sheet to upload to the torrent server 
	// to plan a run. Save file at location in field_controller in the ngs_panel_step_regulator_table for step_regulator_name in ngs_flow_steps_table is generate_run_templat
	// make sure the both chip_A barcode and chip B barcode are not empty and are 19 characters long

	// bhw-vm001.circ.rochester.edu/fs-ofa/run_templates
	//////////////////////////////////////////////////////////
 	// if 	(
 	// 		isset($_POST['pool_download_run_template_submit']) && 
 	// 		isset($_GET['pool_chip_linker_id']) && 
 	// 		isset($_GET['ngs_panel_id']) && 
 	// 		isset($_GET['page']) &&
 	// 		isset($_POST['chip_flow_cellA']) &&
 	// 		isset($_POST['chip_flow_cellB'])
 	// 	)
 	// {

 	// }

	if (isset($_POST['pool_download_run_template_submit']) && isset($_GET['pool_chip_linker_id']) && isset($_GET['ngs_panel_id']) && isset($_GET['page']))
	{		
		// add step completed to step_run_xref if not already completed
		$url = $poolStepTracker->UpdatePoolStepInDb($_GET['page'], $completed_steps, $_GET['pool_chip_linker_id'], USER_ID, $_GET['ngs_panel_id']);
		// header('Location:'.REDIRECT_URL.'?page=home');

		// 


		
	}

	if (isset($_POST['download-ofa-run-templates-submit']))
	{

	}

?>