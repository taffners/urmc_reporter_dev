<?php

	$page_title = 'Add a Single Analyte Pool';
	// There are multiple gets that change what the user sees on the page. 
		// 	No gets
			// the user will be asked to pick a run
		//  pool_type and max_num_samples_per_run
			// The user will be asked to select number samples up to the maximum for the test.  If no maximum provided 
		
	// This page is not meant to be updatable.
	


	// get all tests that are not ngs, extraction, Quantification in assay_type_table assay_name
	if (!isset($_GET['pool_type']))
	{
		$all_single_analyte_tests = $db->listAll('all-single-analyte-tests');		
	}

	// if pool type is provided find all pending tests for the test type selected under pool_type
	else
	{
		$pending_tests = $db->listAll('pending-single-analyte-tests', $_GET['pool_type']);
		
		$last_run_number = $db->listAll('last-single-analyte-pool-run-number', $_GET['pool_type']);

		$test_info = $db->listAll('orderable-tests', $_GET['pool_type']);

		if (isset($test_info[0]['infotrack_report']) && $test_info[0]['infotrack_report'] === 'yes')
		{
			$infotrack_reportable = True;
		}
		else
		{
			$infotrack_reportable = False;
		}

		if (!empty($pending_tests))
		{
			$page_title = 'Add a '.$pending_tests[0]['test_name'].' Pool';
		}
		else
		{
			$page_title = 'There are no pending tests for the selected test samples.  Can not proceed.';
		}		 
	}

	// Add pool_type
	if (
			isset($_POST['add_single_analyte_pool_submit']) && 
			!isset($_GET['pool_type']) &&
			isset($_POST['pool_type']) && 
			!empty($_POST['pool_type'])
		)
	{
		$max_num_samples_per_run_present = False;
		// find the max_num_samples_per_run then add to gets
		foreach ($all_single_analyte_tests as $key => $test)
		{
			if ($_POST['pool_type'] == $test['orderable_tests_id']  && !empty($test['max_num_samples_per_run']))
			{
				$max_num_samples_per_run_present = True;
				header('Location:'.REDIRECT_URL.'?page=add_single_analyte_pool&pool_type='.$_POST['pool_type'].'&max_num_samples_per_run='.$test['max_num_samples_per_run']);					
			}
		}

		if (!$max_num_samples_per_run_present)
		{
			header('Location:'.REDIRECT_URL.'?page=add_single_analyte_pool&pool_type='.$_POST['pool_type']);	
		}
	}

	else if (
			isset($_POST['add_single_analyte_pool_submit']) && 
			isset($_GET['pool_type']) 
		)
	{

		do
		{
			// Make sure the user selected at least one sample
			if (!isset($_POST['selected-samples']) || sizeOf($_POST['selected-samples']) === 0)
			{
				$message = 'No sample tests were selected to add to the pool.  Please use the select check box on the right hand side of the grey sample test box.';
				break;
			}
			// Make sure there are not more samples selected than max_num_samples_per_run
			else if (isset($_GET['max_num_samples_per_run']) && sizeOf($_POST['selected-samples']) > intval($_GET['max_num_samples_per_run']))
			{
				$message = 'You selected '.strval(sizeOf($_POST['selected-samples'])).' and the maximum samples for this test is '.$_GET['max_num_samples_per_run'];
				break;
			}

			// Make the single analyte pool
				// if test is idylla set single_analyte_pool_table status = "pending"  Otherwise set status = "complete"
			$add_array = array();
			$add_array['user_id'] = USER_ID;
			$add_array['status'] = 'pending';
			
			$add_array['start_date'] = date('Y-m-d');
			$add_array['orderable_tests_id'] = $_GET['pool_type'];
			$add_array['run_number'] = $_POST['run_number'];

			// Add tests to the pool
			$add_result = $db->addOrModifyRecord('single_analyte_pool_table', $add_array);	

			if ($add_result[0] == 1)
			{
				$single_analyte_pool_id = $add_result[1];

				//////////////////////
				// Add comments
				////////////////////
				if (isset($_POST['comments']) && !empty($_POST['comments']))
				{
					$add_comment = array();
					$add_comment['user_id'] = USER_ID;
					$add_comment['comment_ref'] = $single_analyte_pool_id;
					$add_comment['comment_type'] = 'single_analyte_pool_table';
					$add_comment['comment'] = $_POST['comments'];

					$add_comment_result = $db->addOrModifyRecord('comment_table', $add_comment);	
				}

				$break_error_occured = False;

				foreach ($_POST['selected-samples'] as $key => $ordered_test_id)
				{

					$add_array = array();
					$add_array['ordered_test_id'] = $ordered_test_id;
					$add_array['user_id'] = USER_ID;
					$add_array['single_analyte_pool_id'] = $single_analyte_pool_id;
					$add_array['order_num'] = $key;

					// update each test in orderable_tests_table to in_progress
					$add_tests_result = $db->addOrModifyRecord('tests_in_pool_table', $add_array);	

					if ($add_tests_result[0] == 1)		
					{			
						// add to run_info_table and update visit_table with run_id
						if ($infotrack_reportable)
						{
							// find visit_array
							$visit_array = $db->listAll('visit-from-ordered-test-table-id', $ordered_test_id);
							
							if (isset($visit_array[0]['sample_name']) && isset($visit_array[0]['visit_id']))
							{
								$run_info_array = array();
								$run_info_array['sample_name'] = $visit_array[0]['sample_name'];
								$run_info_array['status'] = 'pending';
								$run_info_array['single_analyte_pool_id'] = $single_analyte_pool_id;
								$run_info_array['tests_in_pool_id'] = $add_tests_result[1];

								$run_id_result = $db->addOrModifyRecord('run_info_table', $run_info_array);

								if ($run_id_result[0] != 1)		
								{
									$break_error_occured = True;

									$email_utils->emailAdminsProblemURL(__FILE__.' Error!! Line #: '.__LINE__.' Error: Something went wrong with adding a single analyte pool. The run info array did not add.  $single_analyte_pool_id='.$single_analyte_pool_id.' $ordered_test_id='.$ordered_test_id.' $infotrack_reportable='.$infotrack_reportable.' $_GET["pool_type"]='.$_GET['pool_type']);

				
									$message =  'Error: Something went wrong. Admins have been notified to fix the issue.';
									break;
								}
								
								$db->updateRecord('visit_table', 'visit_id', $visit_array[0]['visit_id'], 'run_id',$run_id_result[1]);
							}
							else
							{
								$break_error_occured = True;

								$email_utils->emailAdminsProblemURL(__FILE__.' Error!! Line #: '.__LINE__.' Error: Something went wrong with adding a single analyte pool. The visit could not be found. $single_analyte_pool_id='.$single_analyte_pool_id.' $ordered_test_id='.$ordered_test_id.' $infotrack_reportable='.$infotrack_reportable.' $_GET["pool_type"]='.$_GET['pool_type']);

				
								$message =  'Error: Something went wrong. Admins have been notified to fix the issue.';
								break;
							}
						}
						$db->updateRecord('ordered_test_table', 'ordered_test_id', $ordered_test_id, 'test_status', 'In Progress');		
					}	
					else
					{
						$email_utils->emailAdminsProblemURL(__FILE__.' Error!! Line #: '.__LINE__.' Error: Something went wrong with adding a single analyte pool. Tests did not add to pool.  $single_analyte_pool_id='.$single_analyte_pool_id.' $ordered_test_id='.$ordered_test_id.' $infotrack_reportable='.$infotrack_reportable.' $_GET["pool_type"]='.$_GET['pool_type']);

				
						$message =  'Error: Something went wrong. Admins have been notified to fix the issue.';

						$break_error_occured = True;
						break;
					}		
				}

				if ($break_error_occured)
				{					
					break;
				}
				else
				{
					header('Location:'.REDIRECT_URL.'?page=new_home');	
				}
			}
			else
			{
				$message = 'Something went wrong';
			}

			

			
		} while(false);
	}

?>