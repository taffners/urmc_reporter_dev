<?php
	$page_title = 'Add Barcode';

	require_once('logic/shared_logic/library_pool.php');
// $lockPageStatus = 'show';
	if (isset($_GET['pool_chip_linker_id']) && isset($_GET['ngs_panel_id']))
	{
		/////////////////////////////////////////////////////////////////////////
		// Get the last used indexes for the myeloid panel.  
		// SUGGESTION!! - Build a system during panel set up index type can be picked
		/////////////////////////////////////////////////////////////////////////
		$last_used_indexes = $db->listAll('last-used-indexes-by-visits', $_GET['ngs_panel_id']);			
	}
	else
	{
		header('Location:'.REDIRECT_URL.'?page=home');
	}
	
	// add all barcodes
	if (isset($_POST['pool_dilution_worksheet_submit']) && isset($_GET['pool_chip_linker_id']))
	{
		/////////////////////////////////////////////////////////
		// Add or Update run Date in pool_chip_linker_table
		/////////////////////////////////////////////////////////
		if (isset($_POST['ngs_run_date']))
		{
			$update_run_date = $db->updateRecord('pool_chip_linker_table', 'pool_chip_linker_id', $_GET['pool_chip_linker_id'], 'ngs_run_date', $_POST['ngs_run_date']);
		}

		///////////////////////////////////////////////////////
		// Add dilution and indexes 
		// 	find all visit_0 -> visit_.. to add
		// 	Add indexes to visits_in_pool_table by visits_in_pool_id
		// 	Add or update dilution in stock_dilution table by visits_in_pool_id 
		///////////////////////////////////////////////////////
		for ($i=0; $i<sizeOf($_POST); $i++)
		{
			if (isset($_POST['visit_'.$i]))
			{
				$curr_visit = $_POST['visit_'.$i];
				
				// update indexes i7 and i5
				$curr_visit_in_pool = $db->listAll('visits-in-pool-by-id', $curr_visit['visits_in_pool_id']);
				$update_index_array = $curr_visit_in_pool[0];
				$update_index_array['user_id'] = USER_ID;
				$update_index_array['i7_index_miseq'] = $curr_visit['i7_index_miseq'];
				$update_index_array['i5_index_miseq'] = $curr_visit['i5_index_miseq'];
				$update_index_result = $db->addOrModifyRecord('visits_in_pool_table', $update_index_array);

				// update dilution or 
				$curr_stock_dilution = $db->listAll('stock-dilution-by-visits-in-pool-id', $curr_visit['visits_in_pool_id']);
			
				if (empty($curr_stock_dilution) && isset($curr_visit['dilution_total']))
				{
					$add_dilution_array = array(
						'user_id'			=> 	USER_ID,
						'visits_in_pool_id'	=> 	$curr_visit['visits_in_pool_id'],
						'dilution_total'	=> 	$curr_visit['dilution_total']
					);
					$update_dilution_result = $db->addOrModifyRecord('stock_dilution_table', $add_dilution_array);
				}
				else if (isset($curr_visit['dilution_total']))
				{
					$update_dilution_array = $curr_stock_dilution[0];
					$update_dilution_array['user_id'] = USER_ID;
					$update_dilution_array['dilution_total'] = $curr_visit['dilution_total'];
					$update_dilution_result = $db->addOrModifyRecord('stock_dilution_table', $update_dilution_array);
				}
			}
		}

		// add step completed to step_run_xref if not already completed
		$url = $poolStepTracker->UpdatePoolStepInDb($_GET['page'], $completed_steps, $_GET['pool_chip_linker_id'], USER_ID, $_GET['ngs_panel_id']);

		header('Location:'.REDIRECT_URL.$url);
	}

?>