<?php

	if (isset($_GET['chip']) && isset($_GET['pool_chip_linker_id']))
	{
		// get data from database for run template.  This will only return data related 
		// to the chip currently downloading.
		$search_array = array(
			'chip'	=> 	$_GET['chip'],
			'pool_chip_linker_id' => $_GET['pool_chip_linker_id']
		);

		$chip_data = $db->listAll('ofa-run-template-by-chip', $search_array);
		
		// All the columns expected by the ion torrent for the run template
		$col_header_str = 'Barcode,Control Type,Sample Name (required),Sample ID,Sample Description,DNA/RNA/Fusions,Reference library,Target regions BED file,Hotspot regions BED file,Cancer Type,Cellularity %,Biopsy Days,Couple ID,Embryo ID,IR Workflow,IR Relation,IR Gender,IR Set ID';

		// locations of files so data can be analyzed 
		$target_regions_file = '/results/uploads/BED/2/hg19/unmerged/detail/Oncomine_Focus_DNA_20160219_designed.bed';
		$hotspot_regions_file = '/results/uploads/BED/9/hg19/unmerged/detail/Oncomine_Focus.20160219.hotspots.bed​';

		// array to save all csv output data
		$csv = array(
			'CSV Version (required),2',
			$col_header_str
		);

		// iterate of the data for this chip and add it to the csv output array.
		foreach ($chip_data as $key => $row)
		{
			$barcode = intval($row['barcode_ion_torrent']) > 9 ? 'IonXpress_0'.$row['barcode_ion_torrent'] : 'IonXpress_00'.$row['barcode_ion_torrent'];

			$curr_row = $barcode.','.','.$row['sample_name'].','.$row['sample_name'];
			$curr_row.= ','.'DNA,'.'hg19,'.$target_regions_file.','.$hotspot_regions_file.',';
			$curr_row.= ','.'100,'.','.','.','.OFA_WORKFLOW.','.'self,'.','.$key;

			array_push($csv, $curr_row);
		}

		header('Content-Type: text/csv');
		header('Content-Disposition: attachment; filename="ofa_run_template.csv"');
		$fp = fopen('php://output', 'wb');
		
		foreach ($csv as $line) 
		{
		    $val = explode(",", $line);
		    fputcsv($fp, $val);
		}
		
		fclose($fp);		
	}


	
?>