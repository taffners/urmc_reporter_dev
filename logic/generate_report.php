<?php

	require_once('logic/shared_logic/report_shared_logic.php');
	
	$reportArray = $db->listAll('report-interpt', $_GET['run_id']);
	$tiers = $db->listAll('all-tiers');
	$approval_status = $db->listAll('report-status', $_GET['run_id']);
	$approval_steps = $db->listAll('approval-steps', $_GET['run_id']);
	$completed_confirmation_status = $db->listAll('confirm-status-with-ovt-info', $_GET['run_id']);
	$download_user = $db->listAll('user-initials', USER_ID);
	$visitArray = $db->listAll('visit-info', $_GET['run_id']);


	/////////////////////////////
	// NGS tests and single analyte reportable tests store information about the panel differently.
	// NGS tests use the ngs_panel table while the single analyte tests use orderableTestArray and the
	// test_reporting_regulation_info_table.  If the following information is not provided via the visitArray 
	// find it via the orderableTestArray and test_reporting_regulation_info_table
	/////////////////////////////
	if (empty($visitArray[0]['panel_type']) && isset($orderableTestArray[0]['test_name']))
	{
		$visitArray[0]['panel_type'] = $orderableTestArray[0]['test_name'];
	}

	if (empty($visitArray[0]['method']) && isset($_GET['orderable_tests_id']))
	{
		$test_regs = $db->listAll('test-reporting-regulation-info', $_GET['orderable_tests_id']);


		$visitArray[0]['method'] = isset($test_regs[0]['method']) ? $test_regs[0]['method'] : '';
		$visitArray[0]['limitations'] = isset($test_regs[0]['limitations']) ? $test_regs[0]['limitations'] : '';
		$visitArray[0]['disclaimer'] = isset($test_regs[0]['disclaimer']) ? $test_regs[0]['disclaimer'] : '';
	}

	//////////////////////////////////////////////////////////////////////
	// Find the address that is appropriate.  When we moved to Bailey road this 
	// became important.  Since all reports are automatically generated reports 
	// generated after moving to bailey road need to pull up the correct address.
	// The date is either the last timestamp of an approval step or the current date
	//////////////////////////////////////////////////////////////////////
	$report_date = isset($approval_status[0]['time_stamp']) ? $approval_status[0]['time_stamp'] : date('Y-m-d');
	$address_result = $db->listAll('lab-address-now', $report_date);

	if (isset($address_result[0]))
	{
		$address_array = array();
		foreach($address_result[0] as $col_name => $val)
		{
			if (substr($col_name, 0, 4) === 'line' && !empty($val))
			{
				array_push($address_array, $val);
			}
		}
	}
	
	// Find all genes in panel before the upload_NGS_date
	$searchArray = array(
		'ngs_panel_id'		=>	$visitArray[0]['ngs_panel_id'],
		'upload_NGS_date'	=>	$upload_NGS_date
	);

	if (
			isset($orderableTestArray[0]['hotspot_variants']) && 
			!empty($orderableTestArray[0]['hotspot_variants']) &&
			$orderableTestArray[0]['hotspot_variants'] === 'yes'
		)
	{

		$genesCoveredArray = $db->listAll('hotspots-in-panel', $orderableTestArray[0]['orderable_tests_id']);
	}
	else
	{
		$genesCoveredArray = $db->listAll('genes-in-panel-as-of-upload-ngs-date', $searchArray);	
	}
	
	$accesion_num_present = $db->listAll('accession-num-present-for-ngs-panel', $visitArray[0]['ngs_panel_id']);
	
	// get all revise information about report
	$revisionsArray = $db->listAll('revisions', $_GET['visit_id']);

	// toggle header in report
	if (isset($_GET['header_status']) && $_GET['header_status'] == 'off')
	{
		$header_footer_array = array(
			'footer_status' 	=> 	True,
			'header_status'	=> 	False,
			'address_array'	=>	$address_array
		);

		$pdf = new PDF('P', 'mm', 'a4', $header_footer_array);
	}
	else
	{
		// add wet bench tech to visit array if identified
		if (!empty($wet_bench_tech))
		{
			$techs = '';

			foreach ($wet_bench_tech as $key => $ea_tech)
			{

				if ($techs != '')
				{
					$techs.= ', ';
				}

				$techs.= $ea_tech['first_name'].' '.$ea_tech['last_name'].' '.$ea_tech['credentials'];
			}
			$visitArray[0]['wet_bench_tech'] = $techs;
		}

		$header_footer_array = array(
			'footer_status' 	=> 	True,
			'header_status'	=> 	True,
			'address_array'	=>	$address_array
		);
		$pdf = new PDF('P', 'mm', 'a4', $header_footer_array);	
	}

	$pdf->addNGSReportData($patientArray[0], $visitArray[0], $reportArray, $tiers, $runInfoArray, $approval_status[0]['approval_status'], $completed_confirmation_status, $approval_steps, $download_user[0]['initials'], $reportMutantTables, 'INTERPRETATION:<br><br>'.$full_interpt, $num_snvs, $revisionsArray, $genesCoveredArray);

	$pdf->AliasNbPages();
	$pdf->AddPage();

	// add patient info	
	$pdf->PatientInfo();

	// add panel info
	if 	(
			isset($orderableTestArray[0]['ngs_panel_id']) && 
			$orderableTestArray[0]['ngs_panel_id'] != '0' && 
			!empty($orderableTestArray[0]['ngs_panel_id'])
		)
	{
		$pdf->Panel('Cancer Gene Mutation Panel by Next Generation Sequencing');	
	}
	else
	{
		$pdf->Panel('None');
	}
	
	// this is for adding sample type to report.  Only useful for samples which
	// have multiple tissues types like ofa
	$pdf->SampleInfo();

	// add revision info 
	if (isset($revisionsArray) && !empty($revisionsArray))
	{
		$pdf->addRevisionInfo();
	}

	// change order if no snvs to report
	if (isset($num_snvs)  && $num_snvs > 0)
	{
		// mutations
		if (
				isset($orderableTestArray[0]['hotspot_variants']) && 
				!empty($orderableTestArray[0]['hotspot_variants']) &&
				$orderableTestArray[0]['hotspot_variants'] === 'yes'
			)
		{
			$pdf->MutantsTableRowHeightVaries('partial');
		}
		else
		{
			$pdf->Mutants('full');
		}


		
		
		///////////////////////////////////////////////////////////////
		// Include low coverage information.  If a lowCoverageArray is present 
		// and not empty add a low Coverage table via (lowCoverage) otherwise add
		// a sentence saying low coverage data not missed just no low coverage found
		// using (noLowCoverage)
		///////////////////////////////////////////////////////////////	
		if (isset($lowCoverageArray) && !empty($lowCoverageArray) && isset($low_cov_status))
		{			
			
			if (isset($low_cov_status[0]['type']) && $low_cov_status[0]['type'] === 'chr_start_stop')
			{
				$pdf->LowChrStartFinishCoverage($lowCoverageArray);
			}
			// make a exon and codons included low coverage table
			else
			{
				$pdf->LowExonCodonsCoverage($lowCoverageArray);
			}			
		}
		else if(isset($low_cov_status[0]['status']) && $low_cov_status[0]['status'] == 'no_low_amp')
		{
			$pdf->noLowCoverage();
		}
		// interpretation
		$pdf->Interpretation();
	}
	else
	{
		// interpretation
		$pdf->Interpretation();

		if (isset($lowCoverageArray) && !empty($lowCoverageArray) && isset($low_cov_status[0]['type']))
		{			
			
			if ($low_cov_status[0]['type'] === 'chr_start_stop')
			{
				$pdf->LowChrStartFinishCoverage($lowCoverageArray);
			}
			// make a exon and codons included low coverage table
			else
			{
				$pdf->LowExonCodonsCoverage($lowCoverageArray);
			}			
		}
		else if(isset($low_cov_status[0]['status']) && $low_cov_status[0]['status'] == 'no_low_amp')
		{
			$pdf->noLowCoverage();
		}
	}
	
	// Methodology:
	$pdf->Methodology();
	
	// find if accession number is present in ngs panel covered genes.  Then add Table of genes to PDF

	if (
			isset($orderableTestArray[0]['hotspot_variants']) && 
			!empty($orderableTestArray[0]['hotspot_variants']) &&
			$orderableTestArray[0]['hotspot_variants'] === 'yes'
		)
	{
		$pdf->HotSpotsCovered();
	}
	else
	{
		$pdf->CoveredGenes(!empty($accesion_num_present[0]['accession_num']));	
	}
	
	$pdf->Limitations();
	
	// Disclaimer
	$pdf->Disclaimer();

	// signatures: 
	$pdf->AddSignature();

	//////////////////////////////////////////////////////////////////
	// NOT CURRENTLY BEING USED.  Tried to convert to image and download zip 
	// folder of all images.  The jpgs ended up being blurry.  I'm unsure
	// how to fix this.
	//////////////////////////////////////////////////////////////////
	if (isset($_GET['view_type']) && $_GET['view_type'] === 'jpg')
	{
		$sample_name = 'MRN'.$patientArray[0]['medical_record_num'].'_MD_'.$visitArray[0]['mol_num'].$_GET['header_status'];
		
		$out_folder = SERVER_SAVE_LOC.'jpg_reports/'.$sample_name.'/';
		if (!file_exists($out_folder)) 
		{
    			mkdir($out_folder, 0777, true);
		}

		$pdf_file = $out_folder.$sample_name.'.pdf';
		$pdf->Output($pdf_file, 'F');

		$img = new imagick($pdf_file);

		$num_pages = $img->getNumberImages();
		$img->setResolution(10000,10000);
		// Compress Image Quality
		$img->setImageCompressionQuality(100);

		// read multiple pages 
		for ($i = 0; $i < $num_pages; $i++)
		{
			$img->setIteratorIndex($i);

			//set new format
			$img->setImageFormat('png');
			$img->magnifyImage(); 
			
			$imgName = $out_folder.$sample_name.'_'.$i.'.png';
			$var = $img->writeImage($imgName); 
		}

		//////////////////////////////////////////////////////////////////
		// Zip folder and download 
		//////////////////////////////////////////////////////////////////
		$zip_file = SERVER_SAVE_LOC.'jpg_reports/'.$sample_name.'.zip';

		$utils->zipFolderDownload($out_folder, $zip_file);
	}
	else
	{
		do
		{
			
			//////////////////////////////////////////////////////////////////
			// produce pdfs
			//////////////////////////////////////////////////////////////////
			$report_name = 'MRN_'.$patientArray[0]['medical_record_num'].'_MD_'.$visitArray[0]['mol_num'].'_'.$patientArray[0]['last_name'];

			// save current version of pdf on server at /var/www/urmc_reporter_report_versions.  
			if (isset($_GET['revise']) && $_GET['revise'] == 'on')
			{

				if (isset($_GET['single_analyte_pool_id']) && !empty($_GET['single_analyte_pool_id']))
				{
					$db->updateRecord('single_analyte_pool_table', 'single_analyte_pool_id', $_GET['single_analyte_pool_id'], 'status','pending');					
				}

				// if patient folder does not exist in urmc_reporter_report_versions make it
				$patient_dir = SERVER_SAVE_LOC.'report_versions/'.$report_name;
	 			if (!file_exists($patient_dir))
	 			{
					mkdir($patient_dir);	
	 			}			

	 			$visit_dir = $patient_dir.'/'.$visitArray[0]['visit_id'];
	 			if (!file_exists($visit_dir))
	 			{
					mkdir($visit_dir);	
	 			}
	 			// count how many files in visit_dir.  The new version number will be the count of files plus one
				$curr_ver = $utils->countInDir($visit_dir) + 1;

	 			// Make a copy of the current report and save in the visit_dir folder.
	 			$save_ver_file = $visit_dir.'/'.$curr_ver.'.pdf';
				$pdf->Output('F', $save_ver_file);

				// Change run_info_table status to revising.  This way it will be added to pending-tech-runs.
	 			$runInfoArray[0]['status'] = 'revising';
	 			$db->addOrModifyRecord('run_info_table', $runInfoArray[0]);

	 			// For single analyte tests the single_analyte_pool needs to be set back to pending so 
	 			// the report can be edited. 

	 			// change status on ordered_test
	 			$ordered_test = $db->listAll('ordered-test-by-visit-id', $_GET['visit_id']);

				if (isset($ordered_test[0]) && !empty($ordered_test[0]))
				{
					$ordered_test_update_array = $ordered_test[0];
					$ordered_test_update_array['test_status'] = 'revising';
					$db->addOrModifyRecord('ordered_test_table', $ordered_test_update_array);
				}	

				// redirect to summary page 
				header('Location:'.REDIRECT_URL.'?page=show_edit_report&'.EXTRA_GETS);
			}

			// force download of report if header is missing
			else if (isset($_GET['header_status']) && $_GET['header_status'] == 'off')
			{
				$file_name = $report_name.'.pdf';
				$pdf->Output('D', $file_name);
			}
			else
			{
				$pdf->Output('I');
			}	
		}while(False);
	}



?>
