<?php
	$page_title = 'panel info';

	$tmpfname = 'example_upload_files/ofa_cov_file.cov.xls';
	$excelReader = PHPExcel_IOFactory::createReaderForFile($tmpfname);
	$excelObj = $excelReader->load($tmpfname);
	$worksheet = $excelObj->getSheet(0); // or can use getActiveSheet() to get the active sheet and not just the first sheet.;
	$lastRow = $worksheet->getHighestRow();

	// get entire worksheet in an array.  First parameter is a the value to be 
	// returned if the cell doesn't exist.  Second parameter is a boolean.
	// It is used to specify whether formulas on the sheet should be calculated.
	// Third parameter is used to specify if the cell format should be applied
	// to the returned data or not.  The last parameter is to specify whether the 
	// array index should be a simple running number or the actual cell number on 
	// the sheet
	$excel_array = $worksheet->toArray(null,true,true,true);
	
	// for ($row = 1; $row <= $lastRow; $row++)
	// {
	// 	var_dump($worksheet->getCell('A'.$row)->getValue());
	// }


// 	// ini_set('memory_limit', '256M');
// 	// $ensembl_conn = mysqli_connect('ensembldb.ensembl.org','anonymous') or die ("Couldn't connect to server.");

// 	// $ensembl_db = mysqli_select_db($ensembl_conn, 'homo_sapiens_core_75_37') or die("Couldn't select database.");
	
// 	// $ensembl_db = new DataBase($ensembl_conn, md5('md5 code not active'));

// // var_dump($ensembl_db->listAll('query_ensemble_exon'));

// 	////////////////////////////////////////////////////////////////
// 	// This only works for ofa currently.  
// 	// The file 'cosmic/genes_in_assays.csv' says what genes are in the panel
// 	////////////////////////////////////////////////////////////////
// 	$file_reader_array = array(
// 		'file'   		=>	'cosmic/genes_in_assays.csv',
// 		'type'		=> 	'genes_in_assays',
// 		'panel_type'	=> 	$_GET['panel']
// 	);		

// 	$genesFileReader = new FileReader($file_reader_array);

// 	// find genes which are of interest to find accession numbers
// 	$upload_genes = $genesFileReader->return_data();

// 	$accession_numbers = array();

// 	// foreach ($upload_genes as $key => $gene)
// 	// {
// 	// 	// read cosmic file and find all transcripts related to gene
// 	// 	$cosmic_tsv = '/var/www/html/cosmic_tsvs/CosmicMutantExport.tsv';

// 	// 	$file_reader_array = array(
// 	// 		'file'   		=>	$cosmic_tsv,
// 	// 		'type'		=> 	'accession_in_cosmic',
// 	// 		'gene'		=> 	$gene
// 	// 	);	
// 	// 	$accessionFileReader = new FileReader($file_reader_array);
// 	// 	$ENST_accession_num = $accessionFileReader->getAccessionForGene();

// 	// 	# query ensemble to get NM transcript Numbers
// 	// 	$NM_accession_num = $ensembl_db->listAll('query_ensemble_transcrpits', $ENST_accession_num[0]);

// 	// 	$accession_out_array = array(
// 	// 		'gene' 	=> 	$gene,
// 	// 		'ENST'	=> 	$ENST_accession_num[0],
// 	// 		'NM' 	=> 	isset($NM_accession_num[0]['display_label']) ? $NM_accession_num[0]['display_label']: 'none',
// 	// 		'seq_region_start' => isset($NM_accession_num[0]['seq_region_start']) ? $NM_accession_num[0]['seq_region_start'] : 'none',
// 	// 		'seq_region_end' => isset($NM_accession_num[0]['seq_region_end']) ? $NM_accession_num[0]['seq_region_end'] : 'none'
// 	// 	);
// 	// 	array_push($accession_numbers, $accession_out_array);
// 	// }

// 	// find all amplicons for the panel
// 	$panels_amp_files_array = array(
// 		'ofa' 	=> 	'cosmic/Oncomine_Focus_091515.designed.bed'
// 	);

// 	$amp_file = $panels_amp_files_array[$_GET['panel']];

// 	$file_reader_array = array(
// 		'file'   		=>	$amp_file,
// 		'type'		=> 	'ofa_amplicons'
// 	);		

// 	$ampFileReader = new FileReader($file_reader_array);
// 	$amp_info = $ampFileReader->getOFAAmplicons();
// 	// var_dump(sizeof($amp_info));
// 	// var_dump($amp_info);
// 	// sort($amp_info);
// 	// var_dump($amp_info);
// // 	$ucsc_conn = mysqli_connect('genome-mysql.cse.ucsc.edu','genome') or die ("Couldn't connect to server.");

// // 	$ucsc_db = mysqli_select_db($ucsc_conn, 'hg19') or die("Couldn't select database.");
	
// // 	$ucsc_db = new DataBase($ucsc_conn, md5('md5 code not active'));
// // var_dump($ucsc_db->listAll('query_ucsc_exon', 'NM_004333'));


// 	// exons
// 	// https://www.biostars.org/p/2005/
// 	// https://www.biostars.org/p/167162/
// 	// https://www.biostars.org/p/14776/
// 	// https://www.biostars.org/p/15960/
// // https://www.biostars.org/p/4058/

// 	//https://www.biostars.org/p/6068/#6069

?>