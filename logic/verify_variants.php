<?php
	require_once('logic/shared_logic/report_shared_logic.php');

	//////////////////////////////////////////////////////////////////
	// run message board
	//////////////////////////////////////////////////////////////////
	// find everyone with reviewer permission
	$all_reviewers = $db->listAll('users-review-permissions');
	$all_directors = $db->listAll('users-director-permissions');

	require_once('logic/shared_logic/message_board.php');

	// Make sure QC of variants is done before variants are added to knowledge base permanently the user will be locked out of this page if they do not have director permissions.
	
	// QC status
	$qc_complete = False;
	if (
		isset($qc_run_step_status) && 
		$qc_run_step_status == 'completed' &&
		isset($qc_variant_step_status)  && 
		$qc_variant_step_status == 'completed'

	)
	{
		$qc_complete = True;
	}

	///////////////////////////////////////////////////////////
	// Interate thru all variants in report and find if any variants have been
	// removed from the report and if any variants are not in the knowledge base.
	///////////////////////////////////////////////////////////
	$variants_removed_from_report = '<b>Variants Removed from Report</b>:<br>';
	$variants_not_in_knowledge_base = '<b>Variants Not in Knowledge Base and not classified as Benign or Seq Error</b>:<br>';
	$variants_all_in_knowledge_base = True;
	// All Variants
	$AllVariantArray = $db->listAll('variant-info-for-access-verify-variants', $_GET['run_id']);

	if (isset($AllVariantArray) && $num_snvs !== 0)
     {  
          for ($i = 0; $i < sizeOf($AllVariantArray); $i++)
          {
          	// Variants removed
          	if($AllVariantArray[$i]['include_in_report'] != 1)
          	{
          		$variants_removed_from_report.= '<span class="alert-flagged-qc" style="padding:5px;">'.$AllVariantArray[$i]['genes'].' '.$AllVariantArray[$i]['coding'].' '.$AllVariantArray[$i]['amino_acid_change'].'</span><br><br>';
          	}
          	
          	// Variants not in knowledge base and not Benign or Seq Error
          	if 	(
          			$AllVariantArray[$i]['knowledge_id'] == null &&
          			(
          				strpos($AllVariantArray[$i]['classification'], 'Seq Error') !== False 
          				||
          				strpos($AllVariantArray[$i]['classification'], 'Benign') !== False 
          			)
          			
          		)
          	{
          		$variants_not_in_knowledge_base.= $AllVariantArray[$i]['genes'].' '.$AllVariantArray[$i]['coding'].' '.$AllVariantArray[$i]['amino_acid_change'].'<br>';
          		$variants_all_in_knowledge_base = False;
          	}
          }
     }

     // update if no variants were found
     if ($variants_removed_from_report === '<b>Variants Removed from Report</b>:<br>')
     {
     	$variants_removed_from_report = '<b>Variants Removed from Report</b>:<br>All variants are included in report.';
     }

     if ($variants_not_in_knowledge_base === '<b>Variants Not in Knowledge Base and not classified as Benign or Seq Error</b>:<br>')
     {
		$variants_not_in_knowledge_base = '<b>Variants Not in Knowledge Base and not classified as Benign or Seq Error</b>:<br>No variants.';
     }

	///////////////////////////////////////////////////////////
	// Find if the user can see this page
	// Condition to always see report
		// QC done 
	// Conditions to not see page
		// QC not done 
     		// No director permissions -> do not allow access
     	// QC not done
     		// Director Permission
     			// variants_all_in_knowledge_base = False -> do not allow access
     			// variants_all_in_knowledge_base = True -> Allow access
	///////////////////////////////////////////////////////////
	$allow_access = True;
	if 	(	
			!$qc_complete &&
			isset($user_permssions) && 
			strpos($user_permssions, 'director') !== False && 
			!$variants_all_in_knowledge_base
		)
	{		
		$allow_access = False;	
	}
	else if (	
			!$qc_complete &&
			isset($user_permssions) && 
			strpos($user_permssions, 'director') === False
		)
	{
		$allow_access = False;
	}


	// get all variants for this run
	if (!isset($_GET['run_id']) && !isset($_GET['patient_id']) && !isset($_GET['visit_id']))
	{
		header('Location:'.REDIRECT_URL.'?page=home');
	}

	if (!isset($_GET['variant_filter']))
	{
		$_GET['variant_filter'] = 'included';
	}

	// get all columns in observed_variant_table
	$cols = $db->GetTableColNames('observed_variant_table');

	///////////////////////////////////////////////////////////////////////////// 
	// submit form
	/////////////////////////////////////////////////////////////////////////////
	if(isset($_POST['verify_variants_submit']) && isset($_GET) && $_GET['variant_filter'] === 'included')
	{

     	// add step completed to step_run_xref if not already completed
     	$url = $stepTracker->UpdateStepInDb($page, $completed_steps, RUN_ID, USER_ID);

     	///////////////////////////////////////////////////////////////////////////// 
     	// Add interpretation and add tiers can be skipped if no SNVs found
     	///////////////////////////////////////////////////////////////////////////// 
		header('Location:'.REDIRECT_URL.$url.'&patient_id='.$_GET['patient_id'].'&visit_id='.$_GET['visit_id']);
     	
	}

?>
