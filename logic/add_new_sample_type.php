<?php
	$page_title = 'Add a new sample type';
	$all_possible_sample_types = $db->listAll('all-possible-sample-types-off-also');

	if (isset($_POST['add_new_sample_type_submit']) && empty($_POST['sample_type']))
	{
		$message = 'Please enter a sample type';
	}

	else if (isset($_POST['add_new_sample_type_submit']) && !empty($_POST['sample_type']))
	{
		
		do
		{
			// check if already added if not add
			$check = $db->listAll('sample-type-search', $_POST['sample_type']);

			if (!empty($check))
			{
				$message = $_POST['sample_type'].' was already added by '.$check[0]['user_name'].' on '.$check[0]['time_stamp'];
				break;
			}

			$add_array = array();
			$add_array['user_id'] = USER_ID;
			$add_array['sample_type'] = $_POST['sample_type'];

			$add_result = $db->addOrModifyRecord('possible_sample_types_table', $add_array);

			if ($add_result[0])
			{
				header('Location:'.REDIRECT_URL.'?page=sample_log_book');
			}	
			else
			{
				$email_utils->emailAdminsProblemURL('$add_result[0] != 1');
			}



		} while(false);	
	}
?>