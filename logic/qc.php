<?php
	require_once('logic/shared_logic/report_shared_logic.php');

	// Obtain JSON information saved on fs-ngs to show all of the qc metrics displayed on the pool_qc_backup_data page.  If the 
	// file can not be found do not show.
	if (isset($_GET['run_id']))
	{
		$run_info = $db->listAll('run-info-record-only', $_GET['run_id']);

		if (isset($run_info[0]['library_pool_id']) && !empty($run_info[0]['library_pool_id']))
		{
			$library_pool = $db->listAll('library_pool_by_id', $run_info[0]['library_pool_id']);

			if (isset($library_pool[0]['backup_location']) && !empty($library_pool[0]['backup_location']))
			{
				$library_folder_name = basename($library_pool[0]['backup_location']);

				$select_api_dump_file = $library_pool[0]['backup_location'].'/qc/'.$library_folder_name.'select_api_dump.json';

				$qc_data = array();
				$qc_data['U'] = json_decode($utils->returnFileContents($select_api_dump_file), true);

			}	
		}
		
	}


	if (isset($_POST['qc_submit']))
	{
		$url = $stepTracker->UpdateStepInDb($page, $completed_steps, RUN_ID, USER_ID);	
		header("Refresh:0");
	}

?>