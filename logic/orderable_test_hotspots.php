<?php

	if (!isset($_GET['orderable_tests_id']))
	{
		header('Location:'.REDIRECT_URL.'?page=new_home');	
	}

	$page_title = 'Hotspot Variants';

	$all_hotspots = $db->listAll('all-hotspots-by-knowledge-base', $_GET['orderable_tests_id']);

	$test_reporting_regulation_info = $db->listAll('test-reporting-regulation-info', $_GET['orderable_tests_id']);


	if (!empty($all_hotspots))
	{
		$page_title = 'Hotspot Variants '.$all_hotspots[0]['test_name'];
	}
?>