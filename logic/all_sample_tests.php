<?php
	$page_title = 'Sample Tests';

	if (isset($_GET['sample_log_book_id']))
	{
		$sampleLogBookArray = $db->listAll('all-samples-in-log-book-by-sample-log-book-id',$_GET['sample_log_book_id']);

		require_once('logic/shared_logic/backwards_compatablity_dob_sex.php');

		$reflexArray = $db->listAll('all-sample-reflexes', $_GET['sample_log_book_id']);

		$testArray = $db->listAll('all-sample-tests', $_GET['sample_log_book_id']);

		if (isset($sampleLogBookArray[0]['sample_log_book_id']))
		{
			$page_title = '<p class="d-print-none">Sample Tests for '.$sampleLogBookArray[0]['mol_num'].'</p>';
		}
	}	
	else
	{
		// if sample_log_book_id is not supplied then this page will not work.  Therefore redirect user back to home page since something went wrong
		header('Location:'.REDIRECT_URL.'?page=new_home');
	}
?>