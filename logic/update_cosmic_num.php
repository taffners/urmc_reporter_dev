<?php
	$page_title = 'Update Cosmic number in Knowledge base';

	if (isset($_GET['knowledge_id']) && !empty($_GET['knowledge_id']))
	{
		$knowledge_array = $db->listAll('select-knowledge-base', $_GET['knowledge_id']);

		$gets_exploded = explode('redirect_page=',$_SERVER['QUERY_STRING']);
		
		if (isset($knowledge_array[0]['genes']) && isset($knowledge_array[0]['coding']) && isset($knowledge_array[0]['amino_acid_change']))
		{

			$genes = $knowledge_array[0]['genes'];
			$coding = $knowledge_array[0]['coding'];
			$aa = $knowledge_array[0]['amino_acid_change'];

			$variant = $genes.' '.$coding.' '.$aa;

			$page_title = 'Update Cosmic number in Knowledge base for:<br>'.$variant;

			// get all instances that this variant was used for other reports.  If it was used for a report that was already signed out then do not allow the user to proceed.  The admin will have to deal with this edit.  Reports will have to be sent to revision to save signed out reports first and then the cosmic ID can be updated.
			$search_arr = array(
				'genes' 				=> 	$genes,
				'coding' 				=>	$coding,
				'amino_acid_change'		=> 	$aa
			);

			$reports_using_variant = $db->listAll('reports-using-variant', $search_arr); 

			if (isset($reports_using_variant) && empty($reports_using_variant))
			{
				$edit_allowed = True;
			}
			else
			{
				$edit_allowed = False;
				$all_admin_users = $db->listAll('get-all-users-with-a-permission', 'admin');
				$admins = $email_utils->getEmailAddresses($all_admin_users);
				
				$message = 'The cosmic number for this variant can not be edited since there are reports already signed out using it.  Please ask '.SITE_TITLE.' admins for help: '.$admins;
			}
		}
		else
		{
			$message = 'Can not find Variant';
		}
		
	}
	else
	{
		$message = 'Can not find Variant';
	}

	if (isset($_POST['update_cosmic_num_submit']) && isset($knowledge_array) && isset($knowledge_array[0]['cosmic']) && !isset($_POST['cosmic']))
	{
		$message = 'Make sure a cosmic number is added';
	}	

	else if (isset($_POST['update_cosmic_num_submit']) && isset($knowledge_array) && isset($knowledge_array[0]['cosmic']) && $knowledge_array[0]['cosmic'] == $_POST['cosmic'])
	{
		$message = 'Make sure the cosmic number is different than previously.<br>Current: '.$knowledge_array[0]['cosmic'].'<br>New: '.$_POST['cosmic'];
	}

	// update page
	else if (isset($_POST['update_cosmic_num_submit']))
	{
		$update_array = $knowledge_array[0];
		$update_array['cosmic'] = $_POST['cosmic'];

		$update_result = $db->addOrModifyRecord('knowledge_base_table', $update_array);

		if (isset($gets_exploded) && isset($gets_exploded[1]) && !empty($gets_exploded[1]))
		{
			header('Location:'.REDIRECT_URL.'?page='.$gets_exploded[1]);
		}
		else
		{
			header('Location:'.REDIRECT_URL.'?page=home');
		}
		
	}
?>