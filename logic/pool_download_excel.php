<?php
	$name_file = 'Run OFA Library Prep Excel sheet';
	$page_title = 'Download '.$name_file;

	require_once('logic/shared_logic/library_pool.php');

	// Obtain the next auto number for the excel_workbook_num_table.  However, do not use this number directly.  This is only t put a max on a number which can be entered by a user if this is a reuse of excel work book.  If the number is used directly it could cause conflicts of two users working the same time
	$next_auto_num = $db->listAll('next-auto-increment-num', array('TABLE_SCHEMA' => DB_NAME, 'TABLE_NAME'=> 'excel_workbook_num_table'));

	if (isset($_GET['pool_chip_linker_id']))
	{
		$pool_chip_linker_array = $db->listAll('pool-chip-linker-by-id', $_GET['pool_chip_linker_id']);	
	}

	/////////////////////////////////////////////////////////////////////////////////////////
	// Download an excel workbook with an excel_workbook_num
		// Steps:
			// assign an excel_workbook_num if not already assigned.  
				// four cases:
					// Not downloaded before and requesting new number
						// request new number from excel_workbook_num_table.  Request a new one now to have the least amount of time that two users are going to check out the same number
						// Add new number to pool_chip_linker_table
						// download downloads/Run_OFA_Library_Prep.xlsm file and rename it with the excel_workbook_num_id in the name.
					// Not downloaded before and requesting using old number
						// Make sure excel_workbook_num is filled out
						// Make sure excel_workbook_num is not greater than $next_auto_num - 1.  This should be taken care of in HTML by recheck here too.
						// Add excel_workbook_num to pool_chip_linker_id record
						// Since this is not recommended keep track of this in the user_update_task_table
						// Do not download an excel workbook since the user should already have one.

					// Not implementing right now these cases (this is because of time and it might not be necessary submit btn will not appear for these cases):
						// Downloaded before using previously assigned number
						// excel workbook number assigned an old number but now want a new number 
			// Rename excel workbook file with excel_workbook_num in title 
			// Download file
	/////////////////////////////////////////////////////////////////////////////////////////
	if 	(
			isset($_POST['pool_download_excel_submit']) 
			&& 
			(
				!isset($_GET['pool_chip_linker_id']) ||
				!isset($_GET['ngs_panel_id']) ||
				!isset($_GET['page']) ||
				!isset($pool_chip_linker_array) ||
				!isset($_POST['status_excel_workbook']) ||
				empty($_POST['status_excel_workbook']) ||
				!isset($pool_chip_linker_array) || 
				empty($pool_chip_linker_array)
			)
		)
	{	
		$email_utils->emailAdminsProblemURL(__FILE__.' Error downloading '.$name_file.'!! Line #: '.__LINE__.' important variables missing.');
	}

	////////////////////////////////////////////////////////////////////////
	// Not downloaded before and requesting new number
	////////////////////////////////////////////////////////////////////////
	else if (
				isset($_POST['pool_download_excel_submit']) && 
				$_POST['status_excel_workbook'] === 'new' && 
				empty($pool_chip_linker_array[0]['excel_workbook_num_id'])
			)
	{
		///////////////////////////////////////////
		// request new number from excel_workbook_num_table.  Request a new one now to have the least amount of time that two users are going to check out the same number
		///////////////////////////////////////////
		$new_num_request = array('user_id' => USER_ID);
		$new_num = $db->addOrModifyRecord('excel_workbook_num_table', $new_num_request);

		if ($new_num[0])
		{
			$excel_workbook_num_id = $new_num[1];

			///////////////////////////////////////////
			// Add new number to pool_chip_linker_table
			///////////////////////////////////////////
			$update_array = $pool_chip_linker_array[0];
			$update_array['excel_workbook_num_id'] = $excel_workbook_num_id;

			$update_result = $db->addOrModifyRecord('pool_chip_linker_table', $update_array);
			
			
			///////////////////////////////////////////
			// Download blank workbook and Change name to include excel_workbook_num
			///////////////////////////////////////////

			$file = 'downloads/Run_OFA_Library_Prep.xlsm';
			header('Content-Disposition: attachment; filename="'.$excel_workbook_num_id.'_Run_OFA_Library_Prep.xlsm"');

			header('Content-type: application/vnd.ms-excel.sheet.macroEnabled.12');
			
			header('Content-Length: ' . filesize($file));

			header('Content-Transfer-Encoding: binary');
			
			header('Cache-Control: must-revalidate');
			
			header('Pragma: public');
			ob_clean();
			flush(); 

			// read the original file.
			readfile($file);
var_dump('here');
			// add step completed to step_run_xref if not already completed
			$url = $poolStepTracker->UpdatePoolStepInDb($_GET['page'], $completed_steps, $_GET['pool_chip_linker_id'], USER_ID, $_GET['ngs_panel_id']);
var_dump($url);			
			header('Location:'.REDIRECT_URL.$url);			
		}
		else
		{
			$email_utils->emailAdminsProblemURL(__FILE__.' Error requesting a new excel workbook num '.$name_file.'!! Line #: '.__LINE__);		
		}

	}

	//////////////////////////////////////////////////////////////////////
	// Not downloaded before and requesting using old number
	//////////////////////////////////////////////////////////////////////

	// Not downloaded but user did not supply excel workbook num
	else if (
				isset($_POST['pool_download_excel_submit']) && 
				$_POST['status_excel_workbook'] === 'old' && 
				(
					!isset($_POST['excel_workbook_num']) || 
					empty($_POST['excel_workbook_num'])
				)
			)
	{
		$message = 'Add an excel workbook number';
	}

	// Make sure excel_workbook_num is not greater than $next_auto_num - 1.  This should be taken care of in HTML by recheck here too.
	else if (
				isset($_POST['pool_download_excel_submit']) && 
				$_POST['status_excel_workbook'] === 'old' && 
				isset($_POST['excel_workbook_num']) &&
				!empty($_POST['status_excel_workbook']) &&
				$_POST['status_excel_workbook'] >= $next_auto_num
			)
	{
		$message = 'The excel workbook number can not be greater than the next assigned number which is '.$next_auto_num;
	}


	else if (
				isset($_POST['pool_download_excel_submit']) && 
				$_POST['status_excel_workbook'] === 'old' && 
				isset($_POST['excel_workbook_num']) &&
				empty($pool_chip_linker_array[0]['excel_workbook_num_id'])
			)
	{
		// Add excel_workbook_num to pool_chip_linker_id record
		// since the number should already be in excel_workbook_num_table do not backfill
		$update_array = $pool_chip_linker_array[0];
		$update_array['excel_workbook_num_id'] = $_POST['excel_workbook_num'];

		$update_result = $db->addOrModifyRecord('pool_chip_linker_table', $update_array);
		
		// Since this is not recommended keep track of this in the user_update_task_table
		// Do not download an excel workbook since the user should already have one.
		$tattle_array = array();
		$tattle_array['user_id'] = USER_ID;
		$tattle_array['ref_id'] = $pool_chip_linker_array[0]['pool_chip_linker_id'];
		$tattle_array['task'] = 'reuse excel workbook num';
		$tattle_array['ref'] = 'pool_chip_linker_table';
		$tattle_array['before_val'] = $pool_chip_linker_array[0]['excel_workbook_num_id'];
		$tattle_array['after_val'] = $_POST['excel_workbook_num'];

		$tattle_result = $db->addOrModifyRecord('user_update_task_table', $tattle_array);

		if ($tattle_result[0] && $update_result[0])
		{
			// add step completed to step_run_xref if not already completed
			$url = $poolStepTracker->UpdatePoolStepInDb($_GET['page'], $completed_steps, $_GET['pool_chip_linker_id'], USER_ID, $_GET['ngs_panel_id']);
			header('Location:'.REDIRECT_URL.$url);	
		}
		else
		{
			$email_utils->emailAdminsProblemURL(__FILE__.' Error assigning excel workbook number. Not downloaded before and requesting using old number. $update_result: '.json_encode($update_result).' $tattle_array: '.json_encode($tattle_result).$name_file.'!! Line #: '.__LINE__);
		}
	}

?>