<?php

	$page_title = 'Add Library Pool';

	$ngs_panels = $db->listAll('ngs-panel-info');

	///////////////////////////////////////////////////////////
	// Get all panel samples 
	///////////////////////////////////////////////////////////
	if ($utils->validateGetInt('ngs_panel_id'))
	{
		$available_samples =$db->listAll('pre-samples-not-in-library-pool-by-ngs-panel', $_GET['ngs_panel_id']);
		
		$current_ngs_panel = $db->listAll('ngs-panel-info-by-id', $_GET['ngs_panel_id']);

		$max_num_samples = intval($current_ngs_panel[0]['max_num_chips']) * intval($current_ngs_panel[0]['max_sample_per_chip']);	


		///////////////////////////
		// Add an extra sample spot if there's one NTC.  Since NTC does not take up space on the chip or flow cell
		//////////////////////////
		if (isset($_POST['submit-add-library-pool']) && $utils->findIfVisitIncludedIsNTC($_POST))
		{
			$max_num_samples = $max_num_samples + 1;
		}

		$possible_barcodes = $db->listAll('possible-barcodes-by-ngs-panel', $_GET['ngs_panel_id']);

		// $last_barcode = $db->listAll('last-used-barcode', array('ngs_panel_id' => $_GET['ngs_panel_id'],'pool_chip_linker_id' => ''));	
	}

	///////////////////////////////////////////////////////////
	// for editing pool set up of library pool get information already in the library_pool if get library_pool_id is set
	///////////////////////////////////////////////////////////
	if ($utils->validateGetInt('library_pool_id'))
	{
		$library_pool_array = array();
	}

	///////////////////////////////////////////////////////////
	// choose a panel to make a library pool for
	///////////////////////////////////////////////////////////
	if 	(
			isset($_POST['submit-add-library-pool']) && 
			(
				(
					!isset($_GET['ngs_panel_id']) && 
					isset($_POST['ngs_panel_id'])
				) || 
				isset($_GET['ngs_panel_id']) &&
				$_GET['ngs_panel_id'] !== $_POST['ngs_panel_id']
			)
		)
	{
		// This function does not add anything to the database it just redirects to 
		// a page where relevant fields will load pertaining to the ngs_panel_id 
		// added under get

		// Myeloid panel only contains one chip therefor note this.
		$url = 'Location:'.REDIRECT_URL.'/?page=add_library_pool&ngs_panel_id='.$_POST['ngs_panel_id'];
		
		if ($_POST['ngs_panel_id'] === '2')
		{
			$url.= '&num_chips=1&barcode_min=1';
		}
		header($url);						
	}
	///////////////////////////////////////////////////////////
	// Give the user for the ofa a possibility to only run 1 chip
	///////////////////////////////////////////////////////////
	else if (isset($_POST['submit-add-library-pool']) && isset($_GET['ngs_panel_id']) && ( (!isset($_GET['num_chips']) && isset($_POST['num_chips'])) || (isset($_POST['num_chips']) && $_POST['num_chips'] !== $_GET['num_chips'])   ))
	{
		$url = 'Location:'.REDIRECT_URL.'/?page=add_library_pool&ngs_panel_id='.$_POST['ngs_panel_id'].'&num_chips='.$_POST['num_chips'];
		header($url);		
	}

	///////////////////////////////////////////////////////////
	// Make sure there's at least one sample chosen 
	///////////////////////////////////////////////////////////
	else if 	(
					isset($_POST['submit-add-library-pool']) && 
					$utils->countKeysThatStartWithStrWithSubArrayKey($_POST, 'visit_', 'include_in_pool') < 1  
				)
	{
		$message = 'It looks like you did not choose any samples.  The number of samples chosen are: '.strval($utils->countKeysThatStartWithStrWithSubArrayKey($_POST, 'visit_', 'include_in_pool'));
	}

	///////////////////////////////////////////////////////////
	// Make the number of samples selected were not greater than the max number of samples
	///////////////////////////////////////////////////////////
	else if 	(
					isset($_POST['submit-add-library-pool']) && 
					isset($max_num_samples) && 
					$utils->countKeysThatStartWithStrWithSubArrayKey($_POST, 'visit_', 'include_in_pool') > $max_num_samples
				)
	{

		$message = 'It looks like you chose to many samples.  The number of samples chosen are: '.strval($utils->countKeysThatStartWithStrWithSubArrayKey($_POST, 'visit_', 'include_in_pool')).' This panel allows '.strval($max_num_samples).' samples.';
	}

	///////////////////////////////////////////////////////////
	// Make library pool
	///////////////////////////////////////////////////////////
	else if 	(
					isset($_POST['submit-add-library-pool']) && 
					isset($_GET['num_chips']) 
				)
	{	
		do 
		{
				
			///////////////////////////////////////////////////////////
			// create as many rows in library_pool_table as there are chips $_GET['num_chips']
				// for pools with more than one chip
					// Since there can be more than one chip add lib_pool_arr 
						// $lib_pool_ids = {'Chip' => 'library_pool_id', 'A' => 121, 'B' => 122}
				// pools with one chip
					// Set chip_flow_cell to A
			///////////////////////////////////////////////////////////

			$lib_id_by_chip = array();

			$lib_pool_arr = array();
			$lib_pool_arr['user_id'] = USER_ID;

			// for one chip add array
			if ($_GET['num_chips'] === '1')
			{
				// Add for Thermofisher because there are two chips keep blank for illumina
				if (isset($current_ngs_panel[0]['sequencing_platform_id']) && $current_ngs_panel[0]['sequencing_platform_id'] == 1)
				{
					$lib_pool_arr['chip_flow_cell'] = 'A';				
				}

				$add_lib_pool = $db->addOrModifyRecord('library_pool_table', $lib_pool_arr);
				
				// add pool_chip_linker since there is only one chip chip B will be 0
				if ($add_lib_pool[0])
				{

					$lib_id_by_chip[$add_lib_pool[1]] = 'A';

					$linker_array = array();
					$linker_array['user_id'] = USER_ID;
					$linker_array['library_pool_id_A'] = $add_lib_pool[1];	
					$linker_array['library_pool_name'] = isset($_POST['library_pool_name']) ? $_POST['library_pool_name'] : '';
					$linker_array['library_prep_date'] = isset($_POST['library_prep_date']) ? $_POST['library_prep_date'] : Null;
					$linker_array['ngs_panel_id'] = isset($_POST['ngs_panel_id']) ? $_POST['ngs_panel_id'] : 0;
					$linker_array['infotrack_version'] = VERSION;
					
					$add_linker = $db->addOrModifyRecord('pool_chip_linker_table', $linker_array);
				}			
			}

			// add an alphabetical order of chips for each chip
			else
			{
				$alphabet = range('A', 'Z');
				
				$lib_pool_ids = array();

				for ($i=1; $i<=$_GET['num_chips']; $i++)
				{
					$chip = $alphabet[$i-1];
					
					$lib_pool_arr['chip_flow_cell'] = $chip;


					$add_lib_pool = $db->addOrModifyRecord('library_pool_table', $lib_pool_arr);
					
					// if lib_pool_arr added to db successfully add to lib_pool_ids
					if ($add_lib_pool[0])
					{
						$lib_pool_ids[$chip] = $add_lib_pool[1];

						$lib_id_by_chip[$add_lib_pool[1]] = $chip;
					}
				}	

				$linker_array = array();
				$linker_array['user_id'] = USER_ID;
				$linker_array['library_pool_id_A'] = $lib_pool_ids['A'];	
				$linker_array['library_pool_id_B'] = $lib_pool_ids['B'];
				$linker_array['library_pool_name'] = isset($_POST['library_pool_name']) ? $_POST['library_pool_name'] : '';
				$linker_array['library_prep_date'] = isset($_POST['library_prep_date']) ? $_POST['library_prep_date'] : Null;
				$linker_array['ngs_panel_id'] = isset($_POST['ngs_panel_id']) ? $_POST['ngs_panel_id'] : 0;
				
				$add_linker = $db->addOrModifyRecord('pool_chip_linker_table', $linker_array);
			}

			for ($i=0; $i < sizeOf($_POST); $i++)
			{
				///////////////////////////////////////////////////////////
				// Add barcodes to used_reagents_table
				///////////////////////////////////////////////////////////
				if (isset($_POST['barcodes_'.$i]))
				{
					$curr_barcode = $_POST['barcodes_'.$i];
					$add_barcode_arr = array();
					$add_barcode_arr['pool_chip_linker_id'] = $add_linker[1];
					$add_barcode_arr['reagent_list_id'] = $curr_barcode['reagent_list_id'];
					$add_barcode_arr['ngs_panel_id'] = $_POST['ngs_panel_id'];
					$add_barcode_arr['user_id'] = USER_ID;
					
					$used_reagent_result = $db->addOrModifyRecord('used_reagents_table', $add_barcode_arr);
				}

				/////////////////////////////////////////////////////////
				// find all visits to include in pool
				/////////////////////////////////////////////////////////
				if (isset($_POST['visit_'.$i]))
				{
					$curr_visit = $_POST['visit_'.$i];

					// Exclude visits not included in the pool
					if (isset($curr_visit['include_in_pool']))
					{
						$add_visit_arr = array();
						$add_visit_arr['user_id'] = USER_ID;
						$add_visit_arr['visit_id'] = $curr_visit['visit_id'];
						$add_visit_arr['order_num'] = $curr_visit['order_num'];
						$add_visit_arr['sample_name'] = $curr_visit['sample_name'];
						$add_visit_arr['patient_name'] = $curr_visit['patient_name'];
						
						// find library pool id.  For pools with only one chip access
						// result of adding to db add_lib_pool.  Otherwise use $lib_pool_ids
						if (isset($lib_pool_ids))
						{
							$add_visit_arr['library_pool_id'] = $lib_pool_ids[$curr_visit['chip_flow_cell']];
							
						}
						else
						{
							$add_visit_arr['library_pool_id'] = $add_lib_pool[1];
						}

						$chip = $lib_id_by_chip[$add_visit_arr['library_pool_id']];
					
						$add_visit_to_pool_result = $db->addOrModifyRecord('visits_in_pool_table', $add_visit_arr);


						// since the visits in pool ID is not known until after it is added to the table update with the visits_in_pool_id so full_sample_name can be stored

						if($add_visit_to_pool_result[0] == 1)
						{

							$add_visit_arr['visits_in_pool_id'] = $add_visit_to_pool_result[1];
							$add_visit_arr['full_sample_name'] = $chip.'_'.$curr_visit['sample_name'].'_'.$curr_visit['visit_id'].'_'.$add_visit_to_pool_result[1].'_'.$add_linker[1];
							$add_visit_to_pool_result = $db->addOrModifyRecord('visits_in_pool_table', $add_visit_arr);						
						}
					
						// add Visit as pre_pending step stataus
						$where_array = array(
								'visit_id' 	=> 	$curr_visit['visit_id'],
								'step' 		=> 	'pre_pending',
								'status' 		=>	'passed',
								'user_id'		=> 	USER_ID
							);

						$pre_step_status = $db->listAll('pre-step-complete', $where_array);

						///////////////////////////////////////////////////////////////
					     // If visit_id not in pre_step_visit table toggle to pending.
					          // Add to db
						///////////////////////////////////////////////////////////////
						if (empty($pre_step_status))
						{	
							$result = $db->addOrModifyRecord('pre_step_visit', $where_array);
						}

						///////////////////////////////////////////////////////////////
					     // Set ordered_test_table test_status to In Progress
						///////////////////////////////////////////////////////////////
						$ordered_test_array = $db->listAll('ordered-test-by-visit-id', $curr_visit['visit_id']);

						if (isset($ordered_test_array[0]['ordered_test_id']))
						{
							$db->updateRecord('ordered_test_table', 'ordered_test_id', $ordered_test_array[0]['ordered_test_id'], 'test_status', 'In Progress');
						}

					}				
				}
			}

			header('Location:'.REDIRECT_URL.'?page=home#pre-ngs-library-pool-row');
		} while(False);
		
	}
?>