<?php
	$page_title = 'Add Comment';

	// This page requires both comment_ref and comment_type.  The page title can be updated by database queries to notify the user what they are tagging a comment onto but it is not necessary

	if (!isset($_GET['comment_ref']) || !isset($_GET['comment_type']))
	{
		header('Location:'.REDIRECT_URL.'?page=new_home');
	}

	$gets_exploded = explode('redirect_page=',$_SERVER['QUERY_STRING']);

	if ($_GET['comment_type'] == 'ordered_test_table')
	{
		$orderedTestArray = $db->listAll('ordered-test-plus-sample-info', $_GET['comment_ref']);
		
		if (isset($orderedTestArray[0]['page_title']))
		{
			$page_title = $orderedTestArray[0]['page_title'];
		}
	}

	if (isset($_POST['add_comment_submit']))
	{
		$commentInsertArray = array();
		$commentInsertArray['user_id'] = USER_ID;
		$commentInsertArray['comment_ref'] =  $_GET['comment_ref'];
		$commentInsertArray['comment_type'] =  $_GET['comment_type'];
		$commentInsertArray['comment'] =  $_POST['comments'];

		$comment_result = $db->addOrModifyRecord('comment_table',$commentInsertArray);

		if (isset($gets_exploded) && isset($gets_exploded[1]) && !empty($gets_exploded[1]))
		{
			header('Location:'.REDIRECT_URL.'?page='.$gets_exploded[1]);
		}
		else
		{
			header('Location:'.REDIRECT_URL.'?page=new_home');
		}
	}
?>