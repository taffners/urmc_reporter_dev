<?php
	
	$page_title = 'Report a Bug';

	$all_users = $db->listAll('get-all-active-users');


	if (isset($_POST['report_a_bug_submit']))
	{
		$add_array = array();

		///////////////////////////////
		// Add fields that are only present with admin permissions
			// user_name, report_type, maintenance_start, maintenance_end
		//////////////////////////////
		if (isset($_POST['user_name']) && !empty($_POST['user_name']))
		{
			$add_array['user_name'] = $_POST['user_name'];
		}
		else
		{
			$add_array['user_name'] = $_SESSION['user']['first_name'].' '.$_SESSION['user']['last_name'];
		}
		
		if (isset($_POST['report_type']) && !empty($_POST['report_type']))
		{
			$add_array['report_type'] = $_POST['report_type'];
		}
		else
		{
			$add_array['report_type'] = 'bug';
		}

		if (isset($_POST['maintenance_start']) && !empty($_POST['maintenance_start']))
		{
			$add_array['maintenance_start'] = $_POST['maintenance_start'];
		}
		
		if (isset($_POST['maintenance_end']) && !empty($_POST['maintenance_end']))
		{
			$add_array['maintenance_end'] = $_POST['maintenance_end'];
		}

		if (isset($_POST['downtime_occurs']) && !empty($_POST['downtime_occurs']))
		{
			$add_array['downtime_occurs'] = $_POST['downtime_occurs'];
		}

		$add_array['website_name'] = SITE_TITLE;
		$add_array['status'] = 'pending';
		$add_array['bug_description'] = $_POST['bug_description'];
		$page_result = $qc_db->addOrModifyRecord('bugs_reported_table',$add_array);

		$subject = SITE_TITLE.' Bug to report.';
		$body = "Hello,\r\n\r\n".$add_array['user_name']." would like to notify the ".SITE_TITLE." administrators of the following bug. \r\n\r\n".'"'.$_POST['bug_description'].'"';

		$mail_result = $email_utils->emailAdmins($body, $subject);

		if ($mail_result) 
		{
			header('Location:'.REDIRECT_URL.'?page=bug_list&status=pending');
		} 
		else 
		{
			$message = '<p>Email delivery failed…</p>';
		}

		
	}

?>
