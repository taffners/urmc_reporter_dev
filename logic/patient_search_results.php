<?php

	$page_title = 'Search Finished Patient Reports';

	// search gene, coding change, amino acid change, & tier in observed_variant_table
	if (isset($_GET))
	{
		unset($_GET['page']);

		
		if (isset($_GET['no_variants_found']) && $_GET['no_variants_found'] == 'on')
		{

			$search_result = $db->listAll('search-patient-reports-without-variants', $_GET);
			
		}
		else if (!empty($_GET))
		{
			$search_result = $db->listAll('search-patient-reports', $_GET);

		}
		else
		{
			$message = 'ERROR: $_GET empty';
		}


	}

?>