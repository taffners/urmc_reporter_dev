<?php
	$page_title = 'Add a New Orderable Test';
	
	// all test types array data looks like
	// +---------------+---------+-------------------------------------+---------------------+
	// | assay_type_id | user_id | assay_name                          | time_stamp          |
	// +---------------+---------+-------------------------------------+---------------------+
	// |             1 |       1 | Lightcycler (Capillary)             | 2019-04-19 14:44:42 |
	// |             2 |       1 | Lightcycler (480)                   | 2019-04-19 14:44:42 |
	// |             3 |       1 | Fragment analysis                   | 2019-04-19 14:44:42 |
	// |             4 |       1 | gel                                 | 2019-04-19 14:44:42 |
	$allAssayTypes = $db->listAll('all-assay-types');
	$allTestTypes = $db->listAll('all-test-types');
	$allTestsAvailable = $db->listAll('all-tests-available');
	// $all_genes = $db->listAll('unique-gene-list');


	// find data for updating orderable test
	if (isset($_GET['orderable_tests_id']))
	{
		$orderableTestArray = $db->listAll('orderable-tests', $_GET['orderable_tests_id']);
		$current_TOTs = $db->listAll('turn-around-time', $_GET['orderable_tests_id']);
	}
	else
	{
		$orderableTestArray = array();	
	}


	/////////////////////
	// Add send out tests.
	/////////////////////
	if (isset($_POST['add_new_orderable_test_submit']) && $_POST['assay_type_id'] == 9)
	{
		do 
		{
			/////////////////////////////////////////////////////
			// Make sure a test name and full test name is provided
			////////////////////////////////////////////////////
			if (
					!isset($_POST['test_name']) || 
					empty($_POST['test_name']) || 
					!isset($_POST['full_test_name']) || 
					empty($_POST['full_test_name'])
				)
			{			
				$message = 'Please provide a Test Short Name and Full Test Name';
				break;
			}

			/////////////////////////////////////////////////////
			// Do not allow submission of a test with the same test_name OR full_test_name
			////////////////////////////////////////////////////
			$search_array = array(
				'test_name'			=> 	$_POST['test_name'],
				'full_test_name'	=>	$_POST['full_test_name']
			);
			$orderable_test_name_result = $db->listAll('orderable-tests-by-name', $search_array);

			if (!isset($_GET['orderable_tests_id']) && !empty($orderable_test_name_result))
			{
				$message = 'Test Short Name and Full Test Name need to be unique';
				break;
			}

			////////////////////////////////////////////////////////
			// Add orderable_tests_table
			////////////////////////////////////////////////////////

			$orderable_tests_add_array = array();
			$orderable_tests_add_array['user_id'] = USER_ID;
			$orderable_tests_add_array['assay_type_id'] = $_POST['assay_type_id'];
			$orderable_tests_add_array['test_name'] = $_POST['test_name'];


			$orderable_tests_add_array['full_test_name'] = $_POST['full_test_name'];
			$orderable_tests_add_array['test_type'] = $_POST['test_type'];
			$orderable_tests_add_array['consent_required'] = 'no';

			// custom_fields_available currently not being used
			$orderable_tests_add_array['custom_fields_available'] = 'no';

			$orderable_tests_add_array['notes_for_work_sheet'] = $_POST['notes_for_work_sheet'];


			$orderable_tests_add_array['turnaround_time_depends_on_ordered_test'] = 'no';
			$orderable_tests_add_array['previous_positive_required'] = 'no';

			// NGS tests can not be added via this form.
			$orderable_tests_add_array['mol_num_required'] = 'no';
			$orderable_tests_add_array['ngs_panel_id'] = 0;

			$orderable_tests_add_array['test_status'] = $_POST['test_status'];

			// Since removing the value of the dependent test when it hides via javascript is not working I will remove it here when ever the turnaround_time_depends_on_ordered_test = no
			$orderable_tests_add_array['dependency_orderable_tests_id'] = '';
			
			$orderable_tests_add_array['include_in_turn_around_calculation'] = 'no';
			$orderable_tests_add_array['time_depends_on_result'] = 'no';
			$orderable_tests_add_array['turnaround_time_tracked'] = 'no';

			if (isset($_GET['orderable_tests_id']))
			{
				$orderable_tests_add_array['orderable_tests_id'] = $_GET['orderable_tests_id'];
			}

			$orderable_tests_add_array['extraction_info'] = 'no';
			$orderable_tests_add_array['quantification_info'] = 'no';
			$orderable_tests_add_array['thermal_cyclers_instruments'] = 'no';
			$orderable_tests_add_array['sanger_sequencing_instruments'] = 'no';
			$orderable_tests_add_array['wet_bench_techs'] = 'no';
			$orderable_tests_add_array['analysis_techs'] = 'no';
			$orderable_tests_add_array['test_result'] = 'no';
			$orderable_tests_add_array['send_out_test'] = 'yes';
			
			

			$orderable_result = $db->addOrModifyRecord('orderable_tests_table', $orderable_tests_add_array);

			header('Location:'.REDIRECT_URL.'?page=setup_tests');


		} while(false);
	}


	/////////////////////////////////////////////////////
	// Submit form
		// This form allows most fields to be updatable.  Changing the name will affect previously signed out tests so this is not updatable. Changing the turnaround time does not affect backwards compatibility.
	/////////////////////////////////////////////////////
	else if (isset($_POST['add_new_orderable_test_submit']))
	{

		do 
		{
			/////////////////////////////////////////////////////
			// Do not allow submission of an ngs assay type.  That requires Admin to do
			////////////////////////////////////////////////////
			if (!isset($_POST['assay_type_id']) || empty($_POST['assay_type_id']))
			{
				$message = 'Please select an assay type';
				break;
			}

			$is_ngs_test = $db->listAll('is-assay-type-ngs', $_POST['assay_type_id']);

			if (isset($is_ngs_test[0]['is_ngs_assay']) && $is_ngs_test[0]['is_ngs_assay'] === 'yes')
			{
				$message = 'NGS tests can not be added via this page.';
				break;
			}

			/////////////////////////////////////////////////////
			// Make sure a test name and full test name is provided
			////////////////////////////////////////////////////
			if (
					!isset($_POST['test_name']) || 
					empty($_POST['test_name']) || 
					!isset($_POST['full_test_name']) || 
					empty($_POST['full_test_name'])
				)
			{			
				$message = 'Please provide a Test Short Name and Full Test Name';
				break;
			}

			/////////////////////////////////////////////////////
			// Do not allow submission of a test with the same test_name OR full_test_name
			////////////////////////////////////////////////////
			$search_array = array(
				'test_name'			=> 	$_POST['test_name'],
				'full_test_name'	=>	$_POST['full_test_name']
			);
			$orderable_test_name_result = $db->listAll('orderable-tests-by-name', $search_array);

			if (!isset($_GET['orderable_tests_id']) && !empty($orderable_test_name_result))
			{
				$message = 'Test Short Name and Full Test Name need to be unique';
				break;
			}

			//////////////////////////////////////////////////////
			// Need to add the data to multiple different tables making sure the form can be updated
				// orderable_tests_table
				// turnaround_time
			//////////////////////////////////////////////////////
 			

			////////////////////////////////////////////////////////
			// Add orderable_tests_table
			////////////////////////////////////////////////////////

			$orderable_tests_add_array = array();
			$orderable_tests_add_array['user_id'] = USER_ID;
			$orderable_tests_add_array['assay_type_id'] = $_POST['assay_type_id'];
			$orderable_tests_add_array['test_name'] = $_POST['test_name'];


			$orderable_tests_add_array['full_test_name'] = $_POST['full_test_name'];
			$orderable_tests_add_array['test_type'] = $_POST['test_type'];
			$orderable_tests_add_array['consent_required'] = $_POST['consent_required'];

			// custom_fields_available currently not being used
			$orderable_tests_add_array['custom_fields_available'] = 'no';

			$orderable_tests_add_array['notes_for_work_sheet'] = $_POST['notes_for_work_sheet'];


			$orderable_tests_add_array['turnaround_time_depends_on_ordered_test'] = $_POST['turnaround_time_depends_on_ordered_test'];
			$orderable_tests_add_array['previous_positive_required'] = $_POST['previous_positive_required'];

			// NGS tests can not be added via this form.  mol_num_required is labeled as infotrack_report in the form.
			// This is because mol_num_required is an old field and I want to move forward infotrack_report
			$orderable_tests_add_array['mol_num_required'] = $_POST['mol_num_required'];
			$orderable_tests_add_array['ngs_panel_id'] = 0;

			$orderable_tests_add_array['test_status'] = $_POST['test_status'];

			// Since removing the value of the dependent test when it hides via javascript is not working I will remove it here when ever the turnaround_time_depends_on_ordered_test = no
			$orderable_tests_add_array['dependency_orderable_tests_id'] = $_POST['turnaround_time_depends_on_ordered_test'] === 'no' ? '' : $_POST['dependency_orderable_tests_id'];
			
			$orderable_tests_add_array['include_in_turn_around_calculation'] = $_POST['include_in_turn_around_calculation'];
			$orderable_tests_add_array['time_depends_on_result'] = $_POST['time_depends_on_result'];
			$orderable_tests_add_array['turnaround_time_tracked'] = $_POST['turnaround_time_tracked'];

			if (isset($_GET['orderable_tests_id']))
			{
				$orderable_tests_add_array['orderable_tests_id'] = $_GET['orderable_tests_id'];
			}

			$orderable_tests_add_array['extraction_info'] = $_POST['extraction_info'];
			$orderable_tests_add_array['quantification_info'] = $_POST['quantification_info'];
			$orderable_tests_add_array['thermal_cyclers_instruments'] = $_POST['thermal_cyclers_instruments'];
			$orderable_tests_add_array['sanger_sequencing_instruments'] = $_POST['sanger_sequencing_instruments'];
			$orderable_tests_add_array['wet_bench_techs'] = $_POST['wet_bench_techs'];
			$orderable_tests_add_array['analysis_techs'] = $_POST['analysis_techs'];
			$orderable_tests_add_array['test_result'] = $_POST['test_result'];
			$orderable_tests_add_array['send_out_test'] = 'no';
			$orderable_tests_add_array['max_num_samples_per_run'] = $_POST['max_num_samples_per_run'];
			$orderable_tests_add_array['hotspot_variants'] = $_POST['hotspot_variants'];
			$orderable_tests_add_array['hotspot_type'] = $_POST['hotspot_type'];
			$orderable_tests_add_array['infotrack_report'] = $_POST['mol_num_required'];
			$orderable_tests_add_array['idylla_instruments'] = $_POST['idylla_instruments'];
			$orderable_tests_add_array['result_note'] = $_POST['result_note'];
			$orderable_tests_add_array['result_type'] = $_POST['result_type'];
			$orderable_tests_add_array['hotspot_val_type'] = $_POST['hotspot_val_type'];		
			$orderable_tests_add_array['assign_directors'] = $_POST['assign_directors'];		
				
			$orderable_result = $db->addOrModifyRecord('orderable_tests_table', $orderable_tests_add_array);
	
			////////////////////////////////////////////////////
			// Add turnaround_time to test_turnaround_table.
				// There can be multiple entries depending on result
				// any, positive, negative
			////////////////////////////////////////////////////

			if ($orderable_result[0])
			{

				// send an email to admins to let admins know about hotspot variant test added
			if ($_POST['hotspot_variants'] === 'yes')
			{
				$email_utils->emailAdminsProblemURL(__FILE__.' hotspot varints need to be added!! Line #: '.__LINE__.'An orderable test was updated or newly added that has hotspot variants and hotspot variants will need to be added to the test_hotspots_table. orderable_tests_id: '.$orderable_result[1].' test_name: '.$_POST['test_name'].' User id: '.USER_ID.'. ');

				// email user to remind them to send the hotspot variants	
				$subject = 	SITE_TITLE_ABBR.' hotspot reminder';
				$body = 'Hello '.$_SESSION['user']['first_name'].',

This is an automated email to remind you to send admin ('.$all_admins_str.') an email with all the info needed to add the hotspots for the '.$_POST['test_name'].' test you just added or updated.  This should be provided in an excel sheet and include Gene, Coding, Amino Acid Change, Exon, Codon, Test Genetic Call (required), and Mutation Abbreviation (required).  

Thank you.  '.SITE_TITLE.' Admins';		
				$email_utils->sendEmail($_SESSION['user']['email_address'], $subject, $body, 'skip');
				$email_utils->emailAdmins($body, $subject);
			}


				foreach ($_POST['expected_turnaround_time'] as $dependent => $value)
				{
					// find if turn around time already exists.  If the turnaround time is now empty then it should be deleted.  
					$search_array = array(
						'orderable_tests_id' 	=>	$orderable_result[1],
						'dependent_result'		=> 	$dependent

					);
					$turnaround_present = $db->listAll('turn-around-time-by-dependent-result',$search_array);

					if (!empty($value['turnaround_time']))
					{
						$turnaround_time_array = array();
						$turnaround_time_array['user_id'] = USER_ID;
						$turnaround_time_array['orderable_tests_id'] = $orderable_result[1];
						$turnaround_time_array['status'] = 'include';
						$turnaround_time_array['dependent_result'] = $dependent;
						$turnaround_time_array['turnaround_time'] = $value['turnaround_time'];

						// add test_turnaround_id if already added so it can be updated
						if (isset($turnaround_present[0]['test_turnaround_id']))
						{
							$turnaround_time_array['test_turnaround_id'] = $turnaround_present[0]['test_turnaround_id'];
						}

						$add_result = $db->addOrModifyRecord('test_turnaround_table', $turnaround_time_array);
					}

					// Delete if turnaround_time with dependent result previously existed
					elseif (!empty($turnaround_present) && isset($turnaround_present[0]['test_turnaround_id'])) 
					{
						$del_record = $db->deleteRecordById('test_turnaround_table', 'test_turnaround_id', $turnaround_present[0]['test_turnaround_id']);					
					}
					
				}

				header('Location:'.REDIRECT_URL.'?page=setup_tests');
			}
			else
			{
				$message = 'Something went wrong';
			}

		} while(false);
		
	}


?>