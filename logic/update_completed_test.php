<?php
	$page_title = 'Sample Tests';

	if (isset($_GET['sample_log_book_id']) && isset($_GET['ordered_test_id']) && isset($_GET['update_type']))
	{
		$sampleLogBookArray = $db->listAll('all-samples-in-log-book-by-sample-log-book-id',$_GET['sample_log_book_id']);

		require_once('logic/shared_logic/backwards_compatablity_dob_sex.php');

		$reflexArray = $db->listAll('all-sample-reflexes', $_GET['sample_log_book_id']);

		$testArray = $db->listAll('specific-sample-test', $_GET['ordered_test_id']);

		if (!empty($testArray) && isset($testArray[0]) && isset($testArray[0]['orderable_tests_id']))
		{
			$test = $testArray[0];

			$testInfo = $db->listAll('orderable-tests', $testArray[0]['orderable_tests_id']);

			
		}
		else
		{
			header('Location:'.REDIRECT_URL.'?page=home');
		}


		if (isset($sampleLogBookArray[0]['sample_log_book_id']))
		{
			$page_title = '<p class="d-print-none">Sample Tests for '.$sampleLogBookArray[0]['mol_num'].'</p>';
		}
	}	
	else
	{
		// if sample_log_book_id is not supplied then this page will not work.  Therefore redirect user back to home page since something went wrong
		header('Location:'.REDIRECT_URL.'?page=home');
	}

	if 	(
			isset($_POST['update_completed_test_submit']) && 
			empty($_POST['comments']) 
		)
	{
		$message = 'Please fill in a reason of why you are updating this test';
	}


	// update test results
	else if 	(
			isset($_POST['update_completed_test_submit']) && 
			!empty($_POST['comments']) && 
			isset($_POST['test_result']) &&
			!empty($_POST['test_result']) &&
			$_GET['update_type'] === 'test_result'
		)
	{

		$commentInsertArray = array();
		$commentInsertArray['user_id'] = USER_ID;
		$commentInsertArray['comment_ref'] =  $_GET['ordered_test_id'];
		$commentInsertArray['comment_type'] =  'ordered_test_table';
		$commentInsertArray['comment'] =  'The test result was updated from '.$test['test_result'].' to '.$_POST['test_result'].' by '.$_SESSION['user']['first_name'].' '.$_SESSION['user']['last_name'].' because: '.$_POST['comments'];

		$comment_result = $db->addOrModifyRecord('comment_table',$commentInsertArray);

		$db->updateRecord('ordered_test_table', 'ordered_test_id', $_GET['ordered_test_id'], 'test_result', $_POST['test_result']);


		header('Location:'.REDIRECT_URL.'?page=all_sample_tests&sample_log_book_id='.$_GET['sample_log_book_id']);



	}


		
?>