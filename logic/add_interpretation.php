
<?php
     

     if ($utils->validateGetInt('knowledge_id'))
     {

     	$knowledge_base = $db->listAll('knowledge-by-id', $_GET['knowledge_id']);	

   		$ttk_vw_cols = $db->listAll('get-table-col-names', 'knowledge_base_table');
   		// $ttk_vw_cols = $ttk_vw_cols[0]; 	
   		// var_dump($ttk_vw_cols)	;

   		$variant_name = $knowledge_base[0]['genes'].' '.$knowledge_base[0]['coding'].' ('.$knowledge_base[0]['amino_acid_change'].')';

   		$page_title = 'Variant Interpretations for: <br>'.$variant_name;
     }

     else
     {
     	$message = 'Search did not proceed';
     	$page_title = 'Variant Interpretation';	
     }
     
     
     if (isset($_POST['submit']))
     {
          // make sure the user added a comment before submitting
          if (empty($_POST['comment']))
          {
               $message = 'Add an Interpretation before submitting';              
          }
          else
          {
               // make sure the comment isn't already added 
               $comment_adding = array(
                                   'comment_ref'  =>   $_POST['knowledge_id'],
                                   'comment'      =>   trim($_POST['comment']),
                                   'user_id'      =>   USER_ID,
                                   'comment_type' =>   'knowledge_base_table'
                              );
               $comment_found = $db->listAll('comment-by-ref-and-comment', $comment_adding);

               // if the comment wasn't found add a new comment otherwise report to user that the comment was already added
               if (empty($comment_found))
               {
                    $response = $db->addOrModifyRecord('comment_table',$comment_adding);

                    if (strpos($response[0], 'Oops!! Died'))
                    {
                         $message = $response;
                    }
                    else
                    {
                         header("Refresh:0");
                    }
               }
               else
               {
                    $message  = '<strong>Oops!! This interpretation was added previously</strong>.<br>';
                    $message .= 'The interpretation that was found is:<br>';
                    $message .= '<div class="tab2">'.$comment_found[0]['comment'].'</div>';
                    $message .= 'It was added by:<br>';
                    $message .= '<div class="tab2">'.$comment_found[0]['user_name'].'</div>';
                    $message .= 'On:<br>';
                    $message .= '<div class="tab2">'.$comment_found[0]['time_stamp'].'</div>';
               }
          }
     }

?>