<?php

	$page_title = 'Stop Library Pool';
	
	require_once('logic/shared_logic/library_pool.php');

	$stopReportingOptions = $db->listAll('all-possible-stop-pool-reasons');

	if (isset($_POST['submit-stop-library-pool']) && isset($_GET['pool_chip_linker_id']))
	{
		// update status in pool_chip_linker_table if $_POST['status'] is empty add stop
		$status = !empty($_POST['status']) ? $_POST['status'] : 'stop';


		$update_result = $db->updateRecord('pool_chip_linker_table', 'pool_chip_linker_id', $_GET['pool_chip_linker_id'], 'status', $status);

		header('Location:'.REDIRECT_URL.'?page=home');
	}

?>