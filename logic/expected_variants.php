<?php
	if (
			!isset($_GET['control_type_id']) || 
			!isset($user_permssions) ||
			strpos($user_permssions, 'login_enhanced') == false
		)
	{
		header('Location:'.REDIRECT_URL.'?page=new_home');
	}

	$curr_control = $db->listAll('current-controls', $_GET['control_type_id']);

	$page_title = 'Expected variants for '.$curr_control[0]['control_name'];

	$expected_vars = $db->listAll('all-info-expected_control-variants-for-control', $_GET['control_type_id']);
?>