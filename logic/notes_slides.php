<?php
	$page_title = 'Bugs &amp; Suggestions';

	$all_notes = $db->listAll('all-notes');
	
	if (isset($_POST['submit']))
	{
		if (empty($_POST['note']))
		{
			$message = 'Add a note before submitting';
		}
		else
		{
			// add suggestion/bug to notes table
			$add_array = array(
					'user_id' 	=>	USER_ID,
					'note'		=>	$_POST['note']
							);
			$db->addOrModifyRecord('notes_table', $add_array);
			
			$user_info = $db->listAll('user', USER_ID);
			$subject = 'New Note added in MD Infotrack';
			$body = "Hello,\r\n\r\n".$user_info[0]['first_name']." ".$user_info[0]['last_name'].' ('.$user_info[0]['email_address'].") would like to notify the ".SITE_TITLE." administrators of the following bug or suggestion. \r\n\r\n".'"'.$_POST['note'].'"';

			$mail_result = $email_utils->emailAdmins($body, $subject);

			if ($mail_result) 
			{
				header('Refresh:0');
			} 
			else 
			{
				$message = '<p>Email delivery failed…</p>';
			}

		}
		
	}
?>