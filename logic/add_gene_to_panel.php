<?php
	

	if (!isset($_GET['ngs_panel_id']))
	{
		header('Location:'.REDIRECT_URL.'?page=new_home'); 
	}

	$covered_genes = $db->listAll('genes-in-panel-now', $_GET['ngs_panel_id']);
	
	$ngs_panel_array = $db->listAll('ngs-panel-info-orderable-tests-table', $_GET['ngs_panel_id']);

	if (empty($ngs_panel_array) || !isset($ngs_panel_array[0]['full_test_name']))
	{
		header('Location:'.REDIRECT_URL.'?page=new_home'); 
	}

	// There are a few genes that I just increased the date_gene_added to very far in the future because it was decided after added them 
	// but before we went live that we were not going to go live with the entire panel.  If the user is just updating the date_gene_added then a 
	// genes_covered_by_panel_id will be supplied in $_GET.  
	if (isset($_GET['genes_covered_by_panel_id']))
	{
		$genes_covered_by_panel_array = $db->listAll('one-genes-covered_by_panel',$_GET['genes_covered_by_panel_id']);
	}
	else
	{
		$genes_covered_by_panel_array = array();
	}


	$page_title = 'Add gene to the '.$ngs_panel_array[0]['full_test_name'].' panel';


	if (isset($_POST['add_gene_to_panel_submit']))
	{

		do 
		{

			$date_now = new DateTime();
			$date_added = new DateTime($_POST['date_gene_added']);
			// Make sure the date is not in the the past.  Allowing back dating could possibly edit previously completed reports
			if ($date_now >= $date_added)
			{
				$message = 'The date gene added can not be todays date or before today.  This ensures that previously signed out and reports currently being written are not affected by this change.';
				break;
			}

			$add_array = array();	

			$add_array['user_id'] = USER_ID;
			$add_array['ngs_panel_id'] = $_GET['ngs_panel_id'];

			if (isset($_GET['genes_covered_by_panel_id']))
			{
				$add_array['genes_covered_by_panel_id'] = $_GET['genes_covered_by_panel_id'];				
			}
			
			$add_array['gene'] = $_POST['gene'];
			$add_array['exon'] = $_POST['exon'];
			$add_array['accession_num'] = $_POST['accession_num'];
			$add_array['date_gene_added'] = $_POST['date_gene_added'];

			$add_result = $db->addOrModifyRecord('genes_covered_by_panel_table',$add_array);		

			header('Location:'.REDIRECT_URL.'?page=report_covered_genes&ngs_panel_id='.$_GET['ngs_panel_id']);

		}while(false);


		

		// '' => string '281' (length=3)
  //     'user_id' => string '1' (length=1)
  //     'ngs_panel_id' => string '1' (length=1)
	}

?>