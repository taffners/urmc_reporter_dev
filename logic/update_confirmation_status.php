<?php
     $page_title = 'Update Pending Confirmations';
     
     if (!$utils->validateGetInt('confirm_id'))
	{
		$fatal_err_occured = True;
	}
     else 
     {
     	$pending_confirmations = $db->listAll('pending-confirmations-by-id', $_GET['confirm_id']);
     	$pending_confirmations = $pending_confirmations[0];  	
     }    

	if (isset($_POST['submit']))
	{
// var_dump($_POST);		
		$add_array = $_POST;
		unset($add_array['submit']);
		$add_array['user_id'] = USER_ID;

		$confirm_update = $db->addOrModifyRecord('confirmed_table', $add_array);

		header('Location:'.REDIRECT_URL.'?page=pending_confirmations');
	}     

?>