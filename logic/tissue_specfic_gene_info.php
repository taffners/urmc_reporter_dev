<?php

	$page_title = $_GET['genes'].' variants Found in the Test Tissue: '.$_GET['test_tissue'];


	if (isset($_GET))
	{
		$knowledge_base = $db->listAll('search-knowledge-by-genes-tissue', $_GET);	

		$ttk_vw_cols = $db->listAll('get-table-col-names', 'knowledge_base_table');

		array_push($ttk_vw_cols, array('COLUMN_NAME' => 'test_tissue'));
		array_push($ttk_vw_cols, array('COLUMN_NAME' => 'frequency'));
		array_push($ttk_vw_cols, array('COLUMN_NAME' => 'genotype'));
		array_push($ttk_vw_cols, array('COLUMN_NAME' => 'allele_coverage'));
		array_push($ttk_vw_cols, array('COLUMN_NAME' => 'coverage'));
		array_push($ttk_vw_cols, array('COLUMN_NAME' => 'tier'));
		array_push($ttk_vw_cols, array('COLUMN_NAME' => 'variant_allele'));
		array_push($ttk_vw_cols, array('COLUMN_NAME' => 'ref_allele'));
		array_push($ttk_vw_cols, array('COLUMN_NAME' => 'ref_var_strand_counts'));
		array_push($ttk_vw_cols, array('COLUMN_NAME' => 'strand'));
		array_push($ttk_vw_cols, array('COLUMN_NAME' => 'genetic_call'));
			
	}

	else
	{
		$message = 'Search did not proceed';	
	}
?>