<?php

	$page_title = 'Tech Test History';

	

	if (isset($_GET['time_frame']) && !empty($_GET['time_frame']))
	{		
		$dna_extraction = $db->listAll('dna-extraction-per-tech', $_GET['time_frame']);
		$tech_tests = $db->listAll('tech-test-performance-history', $_GET['time_frame']);
	}

	else
	{
		$dna_extraction = $db->listAll('dna-extraction-per-tech');
		$tech_tests = $db->listAll('tech-test-performance-history');
	}

?>