<?php
	$page_title = 'Assess if chip passed run qc.  Then back up data.';

	require_once('logic/shared_logic/library_pool.php');

	// not currently being used
	$html_infos = array(
		'total_reads'	=> 	array(
							'title'	=> 	'Total number of filtered and trimmed reads independent of length reported in the output BAM file'
						),
		'useable_reads'=>	array(
							'title'	=> 	'Percentage of library ISPs that pass the polyclonal, low quality, and primer-dimer filters. This percentage is calculated by dividing final library ISPs (analysismetrics["libFinal"]) by library ISPs (analysismetrics["lib"]).',
							'link'	=>	'https://10.149.58.208/ion-docs/GUID-488EC9A6-256E-4415-90F4-00117FD8904D.html'
						),
		'loading'	=> 	array(
					'title'	=> 	'Percentage of chip wells that contain a live ISP. (The percentage value considers only potentially addressable wells.)'
				),
		'enrichment'	=> 	array(
					'title'	=> 	'Predicted number of live ISPs that have a key signal identical to the library key signal. The Percent Enrichment value reported is the number of loaded ISPs that are Library ISPs, after taking out Test Fragment (TF) ISPs. This number is calculated by dividing library ISPs by the number of loaded ISPs minus TF ISPs.'
				),
		'final_library'	=> 	array(
					'title'	=> 	'(library reads passing filters / Total Number of reads) * 100',
					'link'	=>	'https://10.149.58.208/ion-docs/GUID-488EC9A6-256E-4415-90F4-00117FD8904D.html'
				),
		'polyclonal'	=> 	array(
					'title'	=> 	'Filters reads from ISPs with >1 unique library template population. Occasionally, low or strange signal ISPs can also get caught in this filter.',
					'link'	=>	'https://10.149.58.208/ion-docs/GUID-488EC9A6-256E-4415-90F4-00117FD8904D.html'
				),
		'test_fragment'	=> 	array(
					'title'	=> 	'Test fragments are known genetic sequences that are used to measure the quality of your chip loading and sequencing run. You can include test fragments in your sequencing run and, after the run completes, review the Details section of the run report to evaluate the quality of your loading and sequencing run. For example, TF_1 is a single known sequence fragment that is added along with the customer library at the beginning of templating and is processed through sequencing. TF_C is already templated on ISPs and added after enrichment so goes through the loading and sequencing portions of the workflow.

Test fragments are displayed when there are at least 1,000 high-quality reads, with an 85% match against the appropriate template in the Test Fragment list

Number of wells containing test fragment ISP with signal.',
					'link'	=>	'https://10.149.58.208/ion-docs/GUID-4B009919-5F2E-49D5-B2F7-C2AB99EA15FB.html'
				),
		'adapter_dimer'	=> 	array(
					'title'	=> 	'The primer dimer filter removes reads with an insert length of < 8 bp, which is generally indicative of adapter dimers (Ion A Adapter annealed directly to an Ion P1 Adapter with no DNA target insert present).',
					'link'	=>	'https://10.149.58.208/ion-docs/GUID-488EC9A6-256E-4415-90F4-00117FD8904D.html'
				),
		'low_qc'	=> 	array(
					'title'	=> 	'Filters reads with low or unrecognizable key signal and reads trimmed to < 8 bases.',
					'link'	=>	'https://10.149.58.208/ion-docs/GUID-488EC9A6-256E-4415-90F4-00117FD8904D.html'
				),
		'median_read_len'	=> 	array(
					'title'	=> 	'Median length, in base pairs, of called reads. In API qualitymetrics[0][q0_median_read_length] matches the correct values in Web APP',
					'link'	=>	'https://10.149.58.208/ion-docs/GUID-964D0161-0A59-4F69-B46E-E3A3BF5DB7F6.html'
				),
		'mean_raw_accuracy'	=> 	array(
					'title'	=> 	'The mean raw accuracy across each individual base position in a read calculated as, 1− (Total errors in the sequenced reads)/Total bases sequenced.',
					'link'	=>	'https://10.149.58.208/ion-docs/GUID-DD4FB467-73E5-46CC-BF59-74FCD2EBFA31.html'
				),
	);

	if (isset($_GET['pool_chip_linker_id']) && !isset($_POST['pool_qc_run_submit']))
	{

		$pool_info = $db->listAll('pool-chip-linker-by-id', $_GET['pool_chip_linker_id']);	

		// find if there's already an entry in the torrent_results_table for this pool_chip_linker_id
		$torrent_results_array = $db->listAll('torrent-results-pool-chip-linker', $_GET['pool_chip_linker_id']);

		$library_pools = $db->listAll('library-pools', $_GET['pool_chip_linker_id']);

		// use chip status array to update the html
		$chip_status = array();
		foreach ($library_pools as $key => $pool)
		{
			$chip_status[$pool['chip_flow_cell']] = $pool['run_qc_status'];
		}

		////////////////////////////////////////////////////////////////////////////////////////
		// Find the access the torrent API and obtain QC info for both chips if present
		////////////////////////////////////////////////////////////////////////////////////////
		if (empty($torrent_results_array) && isset($torrent_results_array[0]))
		{
			$message = 'ERROR Occurred: Results have not been selected.';
		}

		else
		{
			////////////////////////////////////////////////////////////
			// Extract information from both the torrent server API and JSON files saved
			// on the torrent server in the run folder basecaller folder.  This information will
			// be used to determine if chips individually passed general chip run metrics for QC.
			// Then it will be stored in the database once submitted.  The form will not be updatable.
				// array (size=2)
				//   'A' => 
				//     array (size=4)
				//       'cutoffs' => 
				//         array (size=12)
				//       'header' => 
				//         array (size=5)
				//       'lots' => 
				//         array (size=25)
				//       'versions' => 
				//         array (size=18)
				// 		'cutoff_pass' => boolean true
				//   'B' => 
				//     array (size=4)
				//       'cutoffs' => 
				//         array (size=12)        
				//       'header' => 
				//         array (size=5)
				//       'lots' => 
				//         array (size=25)
				//       'versions' => 
				//         array (size=18)
				// 		'cutoff_pass' => boolean true
			////////////////////////////////////////////////////////////
			$qc_data = array();
			if 	(
					isset($torrent_results_array[0]['chipA_results_name']) && 
					!empty($torrent_results_array[0]['chipA_results_name']) && 
					isset($torrent_results_array[0]['library_pool_id_A']) && 
					!empty($torrent_results_array[0]['library_pool_id_A']) &&
					!empty($pool_info)
				)
			{
				$Thermo_A_Backup = new ThermoBackup($torrent_results_array[0]['chipA_results_name'], 'A', $page, $torrent_results_array[0]['library_pool_id_A'], $pool_info[0]);

				$Thermo_A_Backup->run();
		
				$qc_data['A'] = $Thermo_A_Backup->getQCData();

				$qc_data['A']['errors'] = $Thermo_A_Backup->assessErrors();		

			}

			if 	(
					isset($torrent_results_array[0]['chipB_results_name']) && 
					!empty($torrent_results_array[0]['chipB_results_name']) && 
					isset($torrent_results_array[0]['library_pool_id_B']) && 
					!empty($torrent_results_array[0]['library_pool_id_B']) &&
					!empty($pool_info)
				)
			{
				$Thermo_B_Backup = new ThermoBackup($torrent_results_array[0]['chipB_results_name'], 'B', $page, $torrent_results_array[0]['library_pool_id_B'], $pool_info[0]);

				$Thermo_B_Backup->run();
			
				$qc_data['B'] = $Thermo_B_Backup->getQCData();

				$qc_data['B']['errors'] = $Thermo_B_Backup->assessErrors();
			}	
		}
	}

	/////////////////////////////////////////////////////////////////////////////////
	// Submit form, update library pool table if necessary, add metrics to qc_run_metrics_table, add 
	// lots and versions to reagent_version_table if necessary, start data transfer if necessary. 
	///////////////////////////////////////////////////////////////////////////////// 
	if (isset($_GET['pool_chip_linker_id']) && isset($_POST['pool_qc_backup_data_submit']))
	{
		///////////////////////////////////////////////////////////////////////
		// Update library pool for each chip to include run_qc_status, save_loc, chip_barcode
		///////////////////////////////////////////////////////////////////////
		$pass_count = 0;
		if (!empty($library_pools))
		{
			foreach ($library_pools as $key => $pool)
			{
				///////////////////////////////////////////////////////////////////
				// If there is only one chip the union to make library_pools will lead to a null array
				// Example:
				// array (size=10)
				// 	'library_pool_id' => null
				// 	'user_id' => null
				// 	'chip_flow_cell' => null
				// 	'time_stamp' => null
				// 	'reagent_cartidge' => null
				// 	'run_qc_status' => null
				// 	'backup_status' => null
				// 	'backup_location' => null
				// 	'chip_barcode' => null
				// 	'backup_tsv_status' => null
				///////////////////////////////////////////////////////////////////
				if (empty($pool['library_pool_id']))
				{
					break;
				}

				///////////////////////////////////////////////////////////////////
				// Make the array to update the current library pool in the library pool table
				// to contain the information from the $qc_data[$chip]['header']
				// Since this form is not updatable check to see if $pool already has a 
				// run_qc_status.  
				// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! PUTTING REAGENT_VERSION_TABLE ON HOLD.  INFO is in api_dump and backedup.  
				// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! Put on hold since this will add time adding data to db and I'm unsure if
				// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! it will be used in the future.
				// run_qc_status NOT empty
					// Pass -> notify user that the data was assigned pass
					// failed -> 
						// new status pass
							// update status to pass, fill in the reagent_version_table with 
							// versions and lots. Then activate data transfer.  DO NOT add info
							// to qc_run_metrics_table
				// run_qc_status empty
					// pass -> 
						// update status to pass, fill in the reagent_version_table with versions and lots. Then activate data transfer.
					// failed -> 
						// Add failed to library_pool_table
				// backup_status EMPTY AND run_qc_status == pass
					// Start data transfer
				// backup_status NOT EMPTY
					// DO NOT START DATA TRANSFER
				///////////////////////////////////////////////////////////////////
				$library_pool_update_array = $pool;

				$library_pool_id = $pool['library_pool_id'];
				$curr_chip = $pool['chip_flow_cell'];
				$curr_qc_data = $qc_data[$curr_chip];
				$backup_status = $pool['backup_status'];

				// find out current status in the library pool table and new status
				$curr_status = $pool['run_qc_status'];
				$new_status = $_POST['run_qc_status_'.$curr_chip];

				$library_pool_update_array['run_qc_status'] = $new_status;
				$library_pool_update_array['user_id'] = USER_ID;

				// ignore if status is already pass.  Transfer already occurred if that is the case
				if ($curr_status === 'pass')
				{		
					$pass_count++;
					continue;
				}

				// Update library pool table
				else 
				{				
					///////////////////////////////////////////////////////////
					// Only add backup location for $new_status pass otherwise equal it to 
					// empty string
					///////////////////////////////////////////////////////////
					if ($new_status === 'pass')
					{
						$pass_count++;
						$backup = true;
						$backup_location = $curr_qc_data['header']['backup_location'];

						$library_pool_update_array['backup_location'] = $backup_location;
					}
					else
					{
						$backup = false;
						$curr_qc_data['header']['backup_location'] = '';

						$library_pool_update_array['backup_location'] = '';	
					}					

					//////////////////////////////////////////////////////////////////////
					// if backup_status is empty and $new_status === 'pass' start
				 	// update backup_status to started 
					//////////////////////////////////////////////////////////////////////
					
					if ($new_status === 'pass' && empty($backup_status))
					{
						$pass_count++;
						$start_backup = true;						
						$library_pool_update_array['backup_status'] = 'started';
					}
					else
					{
						$start_backup = false;						
					}				
					
					$library_pool_update_array['chip_barcode'] = $curr_qc_data['header']['chip_barcode'];					
					$library_pool_update_array['ts_date'] = date('Y-m-d H:i:s', strtotime($curr_qc_data['header']['time_stamp']));
					$library_pool_update_array['ts_runid'] = $curr_qc_data['header']['run_id'];
					$library_pool_update_array['ts_filesystempath'] = $curr_qc_data['header']['filesystempath'];
					$library_pool_update_array['ts_experiment_id'] = $curr_qc_data['header']['experiment_id'];
					
					$mod_result = $db->addOrModifyRecord('library_pool_table', $library_pool_update_array);
			
					///////////////////////////////////////////////////////////////
					// check to see if data for this library pool is already added to 
					// qc_run_metrics_table.  This is necessary since data is being stored
					// in db if chip passes or fails
					///////////////////////////////////////////////////////////////
					$metric_data_in_db = $db->listAll('library-pool-qc-metrics', $library_pool_id);

					if ($mod_result[0] == 1 && empty($metric_data_in_db) && isset($curr_qc_data['cutoffs']))
					{
						// Add QC data to qc_run_metrics_table
						foreach($curr_qc_data['cutoffs'] as $metric => $result)
						{
							$to_add_qc = array();
							$to_add_qc['library_pool_id'] = $library_pool_update_array['library_pool_id'];
							$to_add_qc['user_id'] = USER_ID;
							$to_add_qc['metric'] = $metric;
							$to_add_qc['val'] = $result['val'];
							$to_add_qc['unit'] = $result['unit'];
							$to_add_qc['status'] = $result['status'];
							$to_add_qc['accepted_range'] = $result['range'];
	
							$add_qc_result = $db->addOrModifyRecord('qc_run_metrics_table', $to_add_qc);
						}
					}
					
					// ////////////////////////////////////////////////////////
					// // Add lots and versions to the reagent_version_table.  Since these are 
					// // only added to db for chips that pass just add data
					// // reagent_version_table
					// 	// | Field                 | Type       
					// 	// +-----------------------+------------
					// 	// | reagent_version_id    | int(11)    
					// 	// | ref_id                | int(11)    
					// 	// | user_id               | int(11) 
					// 	// | ref_table             | varchar(30)   
					// 	// | ref_type              | varchar(10)
					// 	// | reagent_version_field | varchar(30)
					// 	// | reagent_version_value | varchar(10)
					// 	// | time_stamp            | timestamp  
					// ////////////////////////////////////////////////////////
					// if 	(
					// 		$mod_result[0] == 1 && 
					// 		isset($curr_qc_data['lots']) &&
					// 		isset($curr_qc_data['versions'])
					// 	)
					// {
					// 	$to_add_types = array('lots', 'versions');

					// 	foreach ($to_add_types as $key => $add_type)
					// 	{
					// 		foreach($curr_qc_data[$add_type] as $field => $val)
					// 		{
					// 			$to_add_array = array();
					// 			$to_add_array['ref_id'] = $library_pool_id;
					// 			$to_add_array['user_id'] = USER_ID;
					// 			$to_add_array['ref_table'] = 'library_pool_table';
					// 			$to_add_array['ref_type'] = $add_type;
					// 			$to_add_array['reagent_version_field'] = $field;
					// 			$to_add_array['reagent_version_value'] = $val;
					// 			$to_add_array['user_id'] = USER_ID;

					// 			$db->addOrModifyRecord('reagent_version_table', $to_add_array);
					// 		}
					// 	}
					// }

					///////////////////////////////////////////////////////////
					// store epoch time in pool_chip_linker_table and add infotrack_version
					//////////////////////////////////////////////////////////
					// Get pool_chip_linker_table info then update epoch					
					$update_pool_array = $pool_info[0];
					$update_pool_array['infotrack_version'] = VERSION;
					$update_pool_array['data_backup_epoch_time'] = time();

					$db->addOrModifyRecord('pool_chip_linker_table', $update_pool_array);

					$pool_info = $db->listAll('pool-chip-linker-by-id', $_GET['pool_chip_linker_id']);			
					
					
					////////////////////////////////////////////////////////////
					// Prepare to backup data
					////////////////////////////////////////////////////////////				
					if (isset($Thermo_A_Backup))
					{
						$Thermo_A_Backup->backupIonReporterData();
					}

					if (isset($Thermo_B_Backup))
					{
						$Thermo_B_Backup->backupIonReporterData();
					}	
				}			

			}
		}

		// If pass count isn't at least 1 then stop do not proceed

		if ($pass_count > 1)
		{
			// mark page as complete.
			$url = $poolStepTracker->UpdatePoolStepInDb($_GET['page'], $completed_steps, $_GET['pool_chip_linker_id'], USER_ID, $_GET['ngs_panel_id']);

			// on 8/25/2020 I changed the way the python script was being called from system() to exec().
			// This could back fire because it could time out.
			// header('Location:'.REDIRECT_URL.$url.'&loader_only=30000');

			header('Location:'.REDIRECT_URL.$url);
		}
		else
		{
			$message = 'ERROR:  At least once chip needs have a Run QC which passed!';
		}
	}

?>