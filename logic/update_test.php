<?php
	
	$page_title = 'Update Test Status';


	// find everyone with reviewer permission
	$log_book_users = $db->listAll('users-permissions-log-book');

	$users_log_book = $db->listAll('users-permissions-non-ngs-wet-bench');

	if (isset($_GET['ordered_test_id']) && isset($_GET['sample_log_book_id']))
	{
		$sampleLogBookArray = $db->listAll('all-samples-in-log-book-by-sample-log-book-id',$_GET['sample_log_book_id']);

		require_once('logic/shared_logic/backwards_compatablity_dob_sex.php');

		// Get info about data entered about the test
		$orderedTestArray = $db->listAll('ordered-test-info', $_GET['ordered_test_id']);

		if (isset($orderedTestArray[0]['orderable_tests_id']))
		{
			///////////////////////////
			// Get all reflex info 
			//////////////////////////
			$searchArray = array(
				'orderable_tests_id' 	=>	$orderedTestArray[0]['orderable_tests_id'],
				'sample_log_book_id'	=> 	$_GET['sample_log_book_id']
			);
			$reflexTestArray = $db->listAll('reflex-link-sample-test', $searchArray);
	
			if (!empty($reflexTestArray)  && isset($reflexTestArray[0]['status_linked_reflexes_xref_id']))
			{
				// find next reflex
				$searchArray['status_linked_reflexes_xref_id'] = $reflexTestArray[0]['status_linked_reflexes_xref_id'];
				$searchArray['reflex_order'] = $reflexTestArray[0]['reflex_order'];

				$nextReflexes = $db->listAll('nextReflexes', $searchArray);
			}

			// Get info about the test that was ordered
			$testInfo = $db->listAll('orderable-tests', $orderedTestArray[0]['orderable_tests_id']);	

			if 	(
					isset($testInfo[0]['extraction_info']) && isset($testInfo[0]['quantification_info']) && 
					(
						$testInfo[0]['extraction_info'] === 'yes' ||
						$testInfo[0]['quantification_info'] === 'yes'
					)					
				)
			{
				$extractors = $db->listAll('instrument-groups-description', 'extractor');

				// For updating get extraction_array
				if (isset($_GET['extraction_log_id']))
				{
					$extraction_array = $db->listAll('extraction-log-by-id', $_GET['extraction_log_id']);
				}
				else
				{
					$extraction_array = array();
				}

				$quantifiers = $db->listAll('instrument-groups-description-quantifiers');
			}
			
			
			if (isset($_GET['single_analyte_pool_id']) && isset($testInfo[0]['thermal_cyclers_instruments']) && $testInfo[0]['thermal_cyclers_instruments'] == 'yes')
			{
				$search_array = array('instrument' => 'Thermal cycler', 'single_analyte_pool_id' => $_GET['single_analyte_pool_id'] );

				$thermal_cyclers = $db->listAll('instrument-used-in-single-analyte-pool', $search_array);
			}
			else if (isset($testInfo[0]['thermal_cyclers_instruments']) && $testInfo[0]['thermal_cyclers_instruments'] == 'yes')
			{
				$thermal_cyclers = $db->listAll('instrument-groups-description', 'Thermal cycler');
			}

			// find all of the sanger sequencing instruments for if Sanger was used
			if (isset($_GET['single_analyte_pool_id']) && isset($testInfo[0]['sanger_sequencing_instruments']) && $testInfo[0]['sanger_sequencing_instruments'] == 'yes')	
			{
				$search_array = array('instrument' => 'Sanger Sequencer', 'single_analyte_pool_id' => $_GET['single_analyte_pool_id'] );

				$sanger_sequencers = $db->listAll('instrument-used-in-single-analyte-pool', $search_array);
			}	
			else if (isset($testInfo[0]['sanger_sequencing_instruments']) && $testInfo[0]['sanger_sequencing_instruments'] == 'yes')
			{
				$sanger_sequencers = $db->listAll('instrument-groups-description', 'Sanger Sequencer');
			}
		}
	}
	else
	{
		// if sample_log_book_id is not supplied then this page will not work.  Therefore redirect user back to home page since something went wrong
		header('Location:'.REDIRECT_URL.'?page=new_home');
	}

	// submit comments for single analyte tests, DNA extraction, and Nanopore.
	// NGS tests do not submit via this form therefore these tests do not need to be
	// accounted for.
	if 	(
			isset($_POST['update_test_submit']) && 
			isset($_POST['comments']) && 
			!$utils->textareaEmpty($_POST['comments']) && 
			isset($_GET['ordered_test_id']))
	{	
		$commentInsertArray = array();
		$commentInsertArray['user_id'] = USER_ID;
		$commentInsertArray['comment_ref'] =  $_GET['ordered_test_id'];
		$commentInsertArray['comment_type'] =  'ordered_test_table';
		$commentInsertArray['comment'] =  $_POST['comments'];

		$comment_result = $db->addOrModifyRecord('comment_table',$commentInsertArray);
	}

	// submit extraction tests or Nanodrop
	if 	(
			isset($_POST['update_test_submit']) && 
			isset($testInfo[0]['extraction_info']) && 
			isset($testInfo[0]['quantification_info']) && 
			(
				$testInfo[0]['extraction_info'] === 'yes' ||
				$testInfo[0]['quantification_info'] === 'yes'
			) && 
			isset($_GET['ordered_test_id']) && 
			isset($_GET['sample_log_book_id'])
		)
	{

		do
		{

			// instruments[thermal_cyclers]


			// Example POST
				// 'task_date' => 
				//   array (size=3)
				//     'extraction_aliquoted' => string '2019-04-29' (length=10)
				//     'pk_digestion' => string '2019-05-06' (length=10)
				//     'extraction_performed_by' => string '2019-05-06' (length=10)
				// 'task_initials' => 
					// array (size=3)
					//  'extraction_aliquoted' => 
					//    array (size=3)
					//      0 => string '12' (length=2)
					//      1 => string '8' (length=1)
					//      2 => string '1' (length=1)
					//  'pk_digestion' => 
					//    array (size=1)
					//      0 => string '12' (length=2)
					//  'extraction_performed_by' => 
					//    array (size=1)
					//      0 => string '12' (length=2)
				// 'task_time' => 
				//   array (size=1)
				//     'pk_digestion' => string '13:25' (length=5)
				// 'extraction_method' => string 'SPRI-TE' (length=7)
				// 'extractors' => string '1' (length=1)
				// 'stock_conc' => string '10.2' (length=4)
				// 'dilution_exist' => string 'yes' (length=3)
				// 'dilution_conc' => string '2.5' (length=3)
				// 'update_test_submit' => string 'Submit' (length=6)
				// 'instruments' => 
				//    array (size=1)
				//      'quantifiers' => 
				//        array (size=1)
				//          0 => string '27' (length=2)


			///////////////////////////////////////////////////////////
			// This data is going to be put into 4 different tables
				// extraction_log_table
					// extraction_method
					// stock_conc
					// dilution_exist
					// dilution_conc
				// users_performed_task_table
					// data in task arrays.  Start with task_date array and find corresponding data in task_initials and task_time
						// user_id -> task_initials array
						// task_id -> find in db task_table
						// ref_id  -> extraction_log_id
						// ref_table -> extraction_log_table
						// date_performed -> task_date array
						// time_performed -> task_time array
				// task_table Search to use in users_performed_task_table
					// +---------+-------------------------+
					// | task_id | task                    |
					// +---------+-------------------------+
					// |       1 | extraction_aliquoted    |
					// |       2 | pk_digestion            |
					// |       3 | extraction_performed_by |
					// +---------+-------------------------+
				// instrument_used_table
					// use extractors 
						// user_id -> user of web app
						// instrument_id -> extractors val
						// ref_id -> extraction_log_id
						// ref_table -> extraction_log_table
				// ordered_test_table
					// Mark test as finished
						// search for ordered_test
						// update:
							// finish_date
							// test_status
			///////////////////////////////////////////////////////////

			///////////////////////////////////////////////////////////
			// Add extraction_log_table
					// user_id -> web app user
					// extraction_method
					// stock_conc
					// dilution_exist
					// dilution_conc
			///////////////////////////////////////////////////////////
			$add_extraction_array = array(
				'ordered_test_id'	=> 	$_GET['ordered_test_id'],
				'user_id'			=>  	USER_ID,
				'stock_conc'		=>	$_POST['stock_conc'],
				'dilution_exist'	=>	$_POST['dilution_exist'],
				'dilutions_conc'	=>	$_POST['dilution_conc'],
				'times_dilution'	=> 	$_POST['times_dilution'],
				'purity'			=> 	$_POST['purity'],
				'dilution_type'		=> 	isset($_POST['dilution_type']) ? $_POST['dilution_type']: '',
				'vol_dna'			=> 	isset($_POST['vol_dna']) ? $_POST['vol_dna']: '',
				'vol_eb'			=> 	isset($_POST['vol_eb']) ? $_POST['vol_eb']: '',
				'dilution_final_vol'=> 	isset($_POST['dilution_final_vol']) ? $_POST['dilution_final_vol']: '',
				'elution_volume'	=> 	isset($_POST['elution_volume']) ? $_POST['elution_volume']: '',
				'target_conc'		=>	isset($_POST['target_conc']) ? $_POST['target_conc']: '',
				'dna_conc_control_quant'=>	isset($_POST['dna_conc_control_quant']) ? $_POST['dna_conc_control_quant']: '',
				'quantification_method'	=>	isset($_POST['quantification_method']) ? $_POST['quantification_method']: ''
			);

			if ($testInfo[0]['extraction_info'] === 'no')
			{
				$add_extraction_array['extraction_method'] = 'NA';
			}
			else
			{
				$add_extraction_array['extraction_method'] = $_POST['extraction_method'];	
			}

			$add_extraction_result = $db->addOrModifyRecord('extraction_log_table',$add_extraction_array);

			if ($add_extraction_result[0])
			{	
				///////////////////////////////////////////////////////////
				// users_performed_task_table
					// data in task arrays.  Start with task_date array and find corresponding data in task_initials and task_time
						// user_id -> task_initials array
						// task_id -> find in db task_table
						// ref_id  -> extraction_log_id
						// ref_table -> extraction_log_table
						// date_performed -> task_date array
						// time_performed -> task_time array

					// 'task_date' => 
					//   array (size=3)
					//     'extraction_aliquoted' => string '2019-04-29' (length=10)
					//     'pk_digestion' => string '2019-05-06' (length=10)
					//     'extraction_performed_by' => string '2019-05-06' (length=10)
					// 'task_initials' => 
						// array (size=3)
						//  'extraction_aliquoted' => 
						//    array (size=3)
						//      0 => string '12' (length=2)
						//      1 => string '8' (length=1)
						//      2 => string '1' (length=1)
						//  'pk_digestion' => 
						//    array (size=1)
						//      0 => string '12' (length=2)
						//  'extraction_performed_by' => 
						//    array (size=1)
						//      0 => string '12' (length=2)
					// 'task_time' => 
					//   array (size=1)
					//     'pk_digestion' => string '13:25' (length=5)
				///////////////////////////////////////////////////////////

				// iterate over all tasks in task_date array and find corresponding info in 
				// task_initials and task_time array
				foreach ($_POST['task_date'] as $task => $date_performed)
				{
					// skip adding task if data performed not entered
					if (!empty($date_performed))
					{
						// find task id
						// task_table Search to use in users_performed_task_table
						// +---------+-------------------------+
						// | task_id | task                    |
						// +---------+-------------------------+
						// |       1 | extraction_aliquoted    |
						// |       2 | pk_digestion            |
						// |       3 | extraction_performed_by |
						// +---------+-------------------------+
						$task_id = $db->listAll('task-id-by-task', $task);
						$task_id = isset($task_id[0]['task_id']) ? $task_id[0]['task_id'] : 0;
						
						$time_performed = null;

						// time_performed -> task_time array
						if (isset($_POST['task_time'][$task]))
						{
							$time_performed = $_POST['task_time'][$task];
						}

						// Add a task per user in users_performed_task_table
						if (isset($_POST['task_initials'][$task]))
						{
							foreach ($_POST['task_initials'][$task] as $key => $user_id)
							{
								// if no task_initials are selected add task 
								$add_performed_task_array = array(
									'user_id'			=>	$user_id,
									'task_id' 		=>	$task_id,
									'ref_id'  		=>	$add_extraction_result[1],
									'ref_table' 		=> 	'extraction_log_table',
									'date_performed' 	=>	$date_performed,
									'time_performed' 	=>	$time_performed
								);
								
								$performed_task_result = $db->addOrModifyRecord('users_performed_task_table',$add_performed_task_array);
							
							}
						}

						// Add a task with out a user
						else
						{
							// if no task_initials are selected add task 
							$add_performed_task_array = array(
								'user_id'			=>	0,
								'task_id' 		=>	$task_id,
								'ref_id'  		=>	$add_extraction_result[1],
								'ref_table' 		=> 	'extraction_log_table',
								'date_performed' 	=>	$date_performed,
								'time_performed' 	=>	$time_performed
							);
							
							$performed_task_result = $db->addOrModifyRecord('users_performed_task_table',$add_performed_task_array);
						}
					}				
				}

				///////////////////////////////////////////////////////////
				// instrument_used_table ONLY APPLYS IF EXTRACTION METHOD IS SPRITE
					// use extractors 
						// user_id -> user of web app
						// instrument_id -> extractors val
						// ref_id -> extraction_log_id
						// ref_table -> extraction_log_table
				///////////////////////////////////////////////////////////

				if (isset($_POST['extractors']))
				{
					$instrument_add_array = array(
						'user_id'			=> 	USER_ID,
						'instrument_id'	=> 	$_POST['extractors'],
						'ref_id'  		=>	$add_extraction_result[1],
						'ref_table' 		=> 	'extraction_log_table'
					);

					$instrument_result = $db->addOrModifyRecord('instrument_used_table',$instrument_add_array);		
				}

				// add quantifiers
				if (isset($_POST['instruments']))
				{
					foreach ($_POST['instruments'] as $instrument_type => $instruments)
					{
						
						foreach ($instruments as $key => $instrument_id)
						{
							$add_instrument_used_array = array(
								'user_id'		=> 	USER_ID,
								'instrument_id' => 	$instrument_id,
								'ref_id'		=>	$add_extraction_result[1],
								'ref_table' 	=> 	'extraction_log_table'
							);
							
							$usedInstrumentResult = $db->addOrModifyRecord('instrument_used_table',$add_instrument_used_array);
						}
					}
				}

				///////////////////////////////////////////////////////////
				// ordered_test_table
					// Mark test as finished
						// search for ordered_test
						// update:
							// finish_date
							// test_status
				///////////////////////////////////////////////////////////
				$orderedTestResult = $db->listAll('ordered-test-only-by-id', $_GET['ordered_test_id']);

				if (isset($orderedTestResult) && !empty($orderedTestResult))
				{
					$update_array = $orderedTestResult[0];
					$update_array['finish_date'] = date('Y-m-d');
					$update_array['test_status'] = 'complete';	
					$instrument_result = $db->addOrModifyRecord('ordered_test_table',$update_array);
				}
				
				// redirect to log book
				header('Location:'.REDIRECT_URL.'?page=all_sample_tests&sample_log_book_id='.$_GET['sample_log_book_id']);
			}
		} while(false);
	}

	// submit all other types of tests except NGS and extraction
	else if 	(
				
					(
						isset($_POST['update_test_submit']) ||
						isset($_POST['add_idylla_data_submit'])
					)
					&& 
					isset($testInfo[0]['test_name']) && 
					$testInfo[0]['test_name'] !== 'DNA Extraction' && 
					isset($_GET['ordered_test_id']) &&
					isset($_GET['sample_log_book_id'])
				)
	{

		do 
		{

			if (
					isset($thermal_cyclers) && 
					!empty($thermal_cyclers) && 
					(
						!isset($_POST['instruments']['thermal_cyclers']) ||
						empty($_POST['instruments']['thermal_cyclers'])
					)
				)
			{
				$message = 'Please select a thermal cycler';
				break;
			}


			// Make sure a hotspots is not empty if present
			if (
					isset($testInfo[0]['hotspot_variants']) && 
					$testInfo[0]['hotspot_variants'] === 'yes' &&
					(
						!isset($_POST['hotspots']) ||
						empty($_POST['hotspots']) 
					)
				)
			{
				$message = 'Please select a '.$testInfo[0]['hotspot_type'];
				break;
			}
			else
			{
				
				///////////////////////
				// Add observed hotspots to the observed_hotspots_table,
				// Since there are three different types of hotspots signing out via this form this functions needs to be able to adapt for each

					// Idylla reportable tests example.  This example does not have observed and normal values but it does contain test hotspots in a list sometimes.  If test_hotspots_id is not provide it was not selected.  It does provide genes, codings, and amino_acid_changes to add to observed_variant_table.
						// 'hotspots' => 
						//     array (size=15)
					      // 0 => 
					      //   array (size=5)
					      //     'test_hotspots_id' => string '51' (length=2)
					      //     'genetic_call' => string 'A146T/V' (length=7)
					      //     'genes' => string 'NRAS' (length=4)
					      //     'codings' => string 'c.436G>A or c.437C>T' (length=20)
					      //     'amino_acid_changes' => string 'p.Ala146Thr or p.Ala146Val' (length=26)
					      // 1 => 
					      //   array (size=5)
					      //     'test_hotspots_id' => string '44' (length=2)
					      //     'genetic_call' => string 'A59T' (length=4)
					      //     'genes' => string 'NRAS' (length=4)
					      //     'codings' => string 'c.175G>A' (length=8)
					      //     'amino_acid_changes' => string 'p.Ala59Thr' (length=10)
					      // 2 => 
					      //   array (size=5)
					      //     'test_hotspots_id' => string '41' (length=2)
					      //     'genetic_call' => string 'G12A/V' (length=6)
					      //     'genes' => string 'NRAS' (length=4)
					      //     'codings' => string 'c.35G>C or c.35G>T' (length=18)
					      //     'amino_acid_changes' => string 'p.Gly12Ala or p.Gly12Val' (length=24)
					      // 3 => 
					      //   array (size=4)
					      //     'genetic_call' => string 'G12C' (length=4)
					      //     'genes' => string 'NRAS' (length=4)
					      //     'codings' => string 'c.34G>T' (length=7)
					      //     'amino_acid_changes' => string 'p.Gly12Cys' (length=10)
					// This is example is a promega MSI test.  It contains observed and normal and it 
						// 'hotspots' => 
						//     array (size=7)
						//       0 => 
						//         array (size=3)
						//           'test_hotspots_id' => string '31' (length=2)
						//           'observed' => string '100' (length=3)
						//           'normal' => string '120' (length=3)
						//       1 => 
						//         array (size=3)
						//           'test_hotspots_id' => string '32' (length=2)
						//           'observed' => string '90' (length=2)
						//           'normal' => string '' (length=0)
						//       2 => 
						//         array (size=2)
						//           'observed' => string '' (length=0)
						//           'normal' => string '' (length=0)
				///////////////////////			
				foreach ($_POST['hotspots'] as $key => $hotspot)
				{
			
					if (isset($hotspot['test_hotspots_id']) && !empty($hotspot['test_hotspots_id']))
					{
						// create a new array each time to ensure no left over values from the previous hotspot 
						$add_hotspot_array = array();
						$add_hotspot_array['ordered_test_id'] = $_GET['ordered_test_id'];
						$add_hotspot_array['user_id'] = USER_ID;					
						$add_hotspot_array['test_hotspots_id'] = explode(',', $hotspot['test_hotspots_id'])[0];
						
						if (isset($hotspot['observed']) && !empty($hotspot['observed']))
						{
							$add_hotspot_array['observed_val'] = $hotspot['observed'];
						}

						if (isset($hotspot['normal']) && !empty($hotspot['normal']))
						{
							$add_hotspot_array['observed_control_val'] = $hotspot['normal'];
						}

						$add_result = $db->addOrModifyRecord('observed_hotspots_table',  $add_hotspot_array);

						// for reportable tests add to observed variant table also.
						if (isset($_GET['run_id']))
						{
							$observed_variant = array();
							$observed_variant['run_id'] = $_GET['run_id'];
							$observed_variant['genes'] = $hotspot['genes'];
							$observed_variant['coding'] = $hotspot['codings'];
							$observed_variant['amino_acid_change'] = $hotspot['amino_acid_changes'];

							if (isset($hotspot['genetic_call']) && !empty($hotspot['genetic_call']))
							{
								$observed_variant['genetic_call'] = $hotspot['genetic_call'];
							}
							
							$add_result = $db->addOrModifyRecord('observed_variant_table',  $observed_variant);
						}

					}					
				}
			}

			// Add extra tracked fields
			if (isset($_POST['extra_tracked_fields']) && !empty($_POST['extra_tracked_fields']))
			{
				$extra_fields_array = array();
				$extra_fields_array['user_id'] = USER_ID;
				$extra_fields_array['ref_id'] = $_GET['ordered_test_id'];
				$extra_fields_array['ref_table'] = 'ordered_test_table';

				foreach($_POST['extra_tracked_fields'] as $field_name => $field_val)
				{
					$extra_fields_array['field_name'] = $field_name;
					$extra_fields_array['field_val'] = $field_val;

					$add_result = $db->addOrModifyRecord('extra_tracked_table', $extra_fields_array);
				}
			}

			////////////////////////////////////////////////////////////////////	
			// This data is going to be put into 3 different tables
			// ordered_test_table 
				// NOTE: make sure the test start date is not dependent on the end of another test orderable_tests_table (turnaround_time_depends_on_ordered_test == yes)
					// start_date
					// finish_date
					// expected_turnaround_time
					// test_status
					// test_result
			// users_performed_task_table
				// data in task_initials array.
					// user_id -> task_initials array
					// task_id -> find in db task_table
					// ref_id  -> ordered_test_id
					// ref_table -> ordered_test_table
					// date_performed -> empty
					// time_performed -> empty
			// instrument_used_table
					// use $_POST[instruments] array
						// 'instruments' => 
						//     array (size=2)
						//       'thermal_cyclers' => 
						//         array (size=2)
						//           0 => string '3' (length=1)
						//           1 => string '4' (length=1)
						//       'sanger_sequencers' => 
						//         array (size=1)
						//           0 => string '14' (length=2)
					// fields
						// user_id -> user of web app
						// instrument_id -> val of nested array example $_POST[instruments][thermal_cyclers][0]
						// ref_id -> ordered_test_id
						// ref_table -> ordered_test_table
			////////////////////////////////////////////////////////////////////
			
			////////////////////////////////////////////////////////////////////
			// ordered_test_table 
			// NOTE: make sure the test start date is not dependent on the end of another test orderable_tests_table (turnaround_time_depends_on_ordered_test == yes)
				// start_date
				// finish_date
				// expected_turnaround_time
				// test_status
				// test_result
				// assigned_director_user_id
				// qc_status
				// qc_overview
			////////////////////////////////////////////////////////////////////

			$orderedTestResult = $db->listAll('ordered-test-only-by-id', $_GET['ordered_test_id']);

			if (isset($orderedTestResult) && !empty($orderedTestResult))
			{
				$update_array = $orderedTestResult[0];
				
				////////////////////////////////////////////////////////////////////
				// Add all data which would finish the test for data submitted via the update_test page.
				// The reason why this data is not provided for data submitted via other pages such as the
				// add_idylla_data page is because the test is not done yet.  Now the report needs to be built.
				////////////////////////////////////////////////////////////////////
				if (isset($_POST['update_test_submit']))
				{
					$update_array['test_status'] = 'complete';
					$update_array['finish_date'] = date('Y-m-d');

					////////////////////////////////////////////////////////////////////	
					// set expected_turnaround_time from test_turnaround_table				
						// time_depends_on_result
					////////////////////////////////////////////////////////////////////
					$turn_around_time = $db->listAll('turn-around-time', $update_array['orderable_tests_id']);
					
					$expected_turnaround_time = 0;
					if 	(	
							isset($testInfo[0]['previous_positive_required']) && 
							$testInfo[0]['time_depends_on_result'] === 'yes' 
						)
					{				
						// find if test_result is in $turn_around_time[key]['dependent_result']
						foreach ($turn_around_time as $key => $times)
						{
							if (strpos($_POST['test_result'], $times['dependent_result']) !== false)
							{
								$update_array['expected_turnaround_time'] = $times['turnaround_time'];
							}
						}
					}
					else if (sizeOf($turn_around_time) == 1)
					{
						$update_array['expected_turnaround_time'] = $turn_around_time[0]['turnaround_time'];
					}				
				}				

				if (isset($_POST['test_result']))
				{
					$update_array['test_result'] = $_POST['test_result'];
				}
				
				/////////////////////////////////
				// email assigned_director_user_id and add assigned_director_user_id to ordered_test_table
				/////////////////////////////////
				if (
						isset($_POST['assigned_director_user_id']) &&
						!empty($_POST['assigned_director_user_id']) && 
						$_POST['assigned_director_user_id'] !== 'None' 
					)
				{
					$update_array['assigned_director_user_id'] = $_POST['assigned_director_user_id'];	
				
					$test_data = array_merge($orderedTestArray[0], $sampleLogBookArray[0]);

					if (isset($_GET['single_analyte_pool_id']))
					{
						$samples_in_single_analyte_pool = $db->listAll('single-analyte-pool-by-id', $_GET['single_analyte_pool_id']);
						$test_data = array_merge($samples_in_single_analyte_pool[0], $test_data);
					}

					$admin_id = USER_ID;
					$tech_name = $db->listAll('user', USER_ID);

					$email_utils->sendAssignAdminEmail($test_data, $tech_name, $admin_id);
				}
							
				////////////////////////////////////////////////////////////////////
				// update start_date 
					// table abbreviations
						// orderable_tests_table otst
						// sample_log_book_table slbt
					// case
						// - otst.turnaround_time_depends_on_ordered_test == yes
						// - a test ordered for otst.dependency_orderable_tests_id with this slbt.sample_log_book_id within 1 day of this tests start_date
						// - if test ordered for otst.dependency_orderable_tests_id with this slbt.sample_log_book_id is completed
					// result
						// If meets case above change start_date to otst.dependency_orderable_tests_id with this slbt.sample_log_book_id end_date  
					// current tests otst.turnaround_time_depends_on_ordered_test == yes
						// BRAF depends on EGFR 
				////////////////////////////////////////////////////////////////////
				if (isset($testInfo[0]['previous_positive_required']) && 
					$testInfo[0]['time_depends_on_result'] === 'yes')
				{
					$searchArray = array(
						'sample_log_book_id' => $_GET['sample_log_book_id'],
						'dependency_orderable_tests_id' => $testInfo[0]['dependency_orderable_tests_id'],
						'test_start_date' => $orderedTestArray[0]['start_date']
					);			
					
					$dependencySearch = $db->listAll('dependency-ordered-within-day-completed', $searchArray);

					// if a result is return it means all of the cases are met and start date
					// should be updated to dependency_test finish date
					if (!empty($dependencySearch))
					{			
						$update_array['start_date'] = $dependencySearch[0]['finish_date'];
					}
				}

				///////////////////
				// Add QC section.  This is not provided for most tests.  It is used in hotspot tests
				///////////////////
				if (isset($_POST['qc_status']) && !empty($_POST['qc_status']))
				{
					$update_array['qc_status'] = $_POST['qc_status'];
				}

				if (isset($_POST['qc_overview']) && !empty($_POST['qc_overview']))
				{
					$update_array['qc_overview'] = $_POST['qc_overview'];
				}

				$orderedTestResult = $db->addOrModifyRecord('ordered_test_table',$update_array);

				////////////////////////////////////////////////////////////////////
				// users_performed_task_table
				// data in task_initials array.
					// user_id -> task_initials array
					// task_id -> find in db task_table
					// ref_id  -> ordered_test_id
					// ref_table -> ordered_test_table
					// date_performed -> empty
					// time_performed -> empty
				// iterate over task_initials adding each user and task to 
				// user_performed_task_table
					// 'task_initials' => 
					//     array (size=2)
					//       'wet_bench' => 
					//         array (size=1)
					//           0 => string '12' (length=2)
					//       'analysis' => 
					//         array (size=1)
					//           0 => string '12' (length=2)
				////////////////////////////////////////////////////////////////////
				if (isset($_POST['task_initials']))
				{
					foreach ($_POST['task_initials'] as $task => $users)
					{
						$task_id = $db->listAll('task-id-by-task', $task);
						$task_id = isset($task_id[0]['task_id']) ? $task_id[0]['task_id'] : 0;
						foreach ($users as $key => $user_id)
						{
							$add_performed_task_array = array(
								'user_id'		=> 	$user_id,
								'task_id'		=> 	$task_id,
								'ref_id'		=>	$_GET['ordered_test_id'],
								'ref_table'	=>	'ordered_test_table'
							);

							$performedTaskResult = $db->addOrModifyRecord('users_performed_task_table',$add_performed_task_array);
						}
					}
				}

				////////////////////////////////////////////////////////////////////
				// instrument_used_table
					// use $_POST[instruments] array
						// 'instruments' => 
						//     array (size=2)
						//       'thermal_cyclers' => 
						//         array (size=2)
						//           0 => string '3' (length=1)
						//           1 => string '4' (length=1)
						//       'sanger_sequencers' => 
						//         array (size=1)
						//           0 => string '14' (length=2)
					// fields
						// user_id -> user of web app
						// instrument_id -> val of nested array example $_POST[instruments][thermal_cyclers][0]
						// ref_id -> ordered_test_id
						// ref_table -> ordered_test_table
				////////////////////////////////////////////////////////////////////
				
				// add all instruments
				if (isset($_POST['instruments']))
				{
					foreach ($_POST['instruments'] as $instrument_type => $instruments)
					{
						
						foreach ($instruments as $key => $instrument_id)
						{
							$add_instrument_used_array = array(
								'user_id'		=> 	USER_ID,
								'instrument_id'=> 	$instrument_id,
								'ref_id'		=>	$_GET['ordered_test_id'],
								'ref_table'	=>	'ordered_test_table'
							);
							
							$usedInstrumentResult = $db->addOrModifyRecord('instrument_used_table',$add_instrument_used_array);
						}
					}
				}

				////////////////////////////////
				// Update reflex tracker status
				/////////////////////////////////
				if (isset($_POST['status_linked_reflexes_xref_id'])  && !empty($_POST['status_linked_reflexes_xref_id']))
				{
					$db->updateRecord('status_linked_reflexes_xref_table', 'status_linked_reflexes_xref_id', $_POST['status_linked_reflexes_xref_id'], 'status', 'complete');
							

					// If this is the Last Reflex Test or if the trigger_result does not equal test_result then set ordered_reflex_test_id entry to reflex_status = "complete"
					if (
							(
								(
									isset($_POST['trigger_result']) &&
									$_POST['trigger_result'] == 'Last Reflex Test'
								)
								||
								(
									isset($_POST['trigger_result']) && 
									!empty($_POST['trigger_result']) &&
									isset($_POST['test_result']) &&
									$_POST['test_result'] !== $_POST['trigger_result'] 
								)
							)
							&&
							isset($_POST['ordered_reflex_test_id'])
						)
					{
						$db->updateRecord('ordered_reflex_test_table', 'ordered_reflex_test_id', $_POST['ordered_reflex_test_id'], 'reflex_status', 'complete');
					}
				}

				if (isset($_POST['add_idylla_data_submit']))
				{
					// add step completed to step_run_xref if not already completed
			     	$url = $stepTracker->UpdateStepInDb($page, $completed_steps, RUN_ID, USER_ID);

			     	///////////////////////////////////////////////////////////////////////////// 
			     	// Add interpretation and add tiers can be skipped if no SNVs found
			     	///////////////////////////////////////////////////////////////////////////// 
					header("Refresh: 0");
				}
				else if (isset($_GET['single_analyte_pool_id']))
				{
					header('Location:'.REDIRECT_URL.'?page=single_analyte_pool_home&single_analyte_pool_id='.$_GET['single_analyte_pool_id']);
				}
				else
				{
					header('Location:'.REDIRECT_URL.'?page=all_sample_tests&sample_log_book_id='.$_GET['sample_log_book_id']);	
				}
				
			}
			else
			{
				$message = 'Something went wrong';
			}

		} while(False);
	}

?>