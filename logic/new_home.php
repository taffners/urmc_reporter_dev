<?php
	$page_title = 'Welcome to '.SITE_TITLE;
	

	//////////////////////////////////////////////////
	// Set up the homepage.  If single_analyte_pool_view is set then the user selected a button on the screen.  Otherwise see if the user has set up their homepage default list in the user_homepage_table
	/////////////////////////////////////////////////
	if (!isset($_GET['single_analyte_pool_view']))
	{
		$user_homepage_single_analyte_pool_list = $single_analyte_pool_view = $db->listAll('single-analyte-pool-view', USER_ID);

		if (empty($user_homepage_single_analyte_pool_list))
		{
			$_GET['single_analyte_pool_view'] = 'all_pools';
		}
		else
		{
			$_GET['single_analyte_pool_view'] = $user_homepage_single_analyte_pool_list[0]['feature_choice'];
		}
	}

	if ($_GET['single_analyte_pool_view'] === 'my_pools')
	{
		$single_analyte_pools = $db->listAll('pending-single-analyte-pools-my-pools', USER_ID);	
	}
	else if ($_GET['single_analyte_pool_view'] === 'all_pools')
	{
		$single_analyte_pools = $db->listAll('pending-single-analyte-pools');
	}
	else if ($_GET['single_analyte_pool_view'] === 'reportable_pools')
	{
		$single_analyte_pools = $db->listAll('pending-single-analyte-pools-reportable');	
	}
	
    $recent_version_history_update = $db->listAll('version-history-within-7-days');

    $last_2_months_version_update_count = $db->version_count(60);

    $last_month_version_update_count = $db->version_count(30);
    $last_7_days_version_update_count = $db->version_count(7);

    $maintenance_pending = $qc_db->listAll('qc_db-pending-maintenance',SITE_TITLE);

	$ngs_panels = $db->listAll('all-ngs-panels');

	

	require_once('logic/shared_logic/search_sample_log_book_submit.php');

?>