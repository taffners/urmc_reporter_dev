<?php
	require_once('logic/shared_logic/report_shared_logic.php');

	if 	(
			!$utils->validateGetInt('run_id') ||
			!$utils->validateGetInt('visit_id') ||
			!$utils->validateGetInt('patient_id')
		)
	{
		$fatal_err_occured = True;
	}

	else if(isset($_POST['add_gene_interpetation_submit']))
	{
		$error_found = False;
		
		foreach($_POST as $entry)
		{
			/////////////////////////////////////////////////
			// add all interpts into gene_interpt_table
			/////////////////////////////////////////////////
			if (is_array($entry)  && $entry['interpt'] !== '' && $entry['empty_data'] !== $entry['interpt'])
			{	
				////////////////////////////////////////////////////
				// check if interpt is already in gene_interpt_table	
				////////////////////////////////////////////////////
				$interpt_found = $db->listAll('get-gene-interpt-by-interpt', $entry['interpt']);

				// if interpt already in gene_interpt_table get gene_interpt_id
				if (!empty($interpt_found) && isset($interpt_found[0]['gene_interpt_id']) )
				{
					$gene_interpt_id = $interpt_found[0]['gene_interpt_id'];
				}

				// if interpt not present add to gene_interpt_table and get gene_interpt_id
				else if (empty($interpt_found))
				{
					$insertArray = array(
							'user_id' 	=> 	USER_ID,
							'interpt' 	=> 	$entry['interpt'],
							'gene'		=> 	$entry['gene_name']
						);

					$gene_interpt_result = $db->addOrModifyRecord('gene_interpt_table',$insertArray);				
					
					if ($gene_interpt_result[0] == 1)
					{

						$gene_interpt_id = $gene_interpt_result[1];
					}	
				}

				////////////////////////////////////////////////////
				// Add gene_id, gene, and run_id to run_xref_gene_interpt_table
				////////////////////////////////////////////////////
				// make sure gene_interpt_id is set and known otherwise throw error
				if (!isset($gene_interpt_id) || empty($gene_interpt_id))
				{
					$message = 'Something has gone wrong with adding gene interptations.  Please contact Samantha about this issue.';
					$error_found = True;
					break;
				}

				// Check if run_id and gene already in table.  
				$search_array = array(
								'run_id' 	=> RUN_ID,
								'gene' 	=> $entry['gene_name']
							);

				$xref_found = $db->listAll('run-xref-gene-interpt-by-run-id-gene', $search_array);
				
				// If run_id and gene already has interpt update gene_interpt_id if not the same then get run_xref_gene_interpt_id
				if (!empty($xref_found) && $xref_found[0]['gene_interpt_id'] !== $gene_interpt_id)
				{
					$xref_found[0]['user_id'] = USER_ID;
					$xref_found[0]['gene_interpt_id'] = $gene_interpt_id;
					$xref_result = $db->addOrModifyRecord('run_xref_gene_interpt',$xref_found[0]);
					
					if ($xref_result[0] == 1)
					{

						$run_xref_gene_interpt_id = $xref_result[1];
					}
					else
					{
						$message = 'Something went wrong while adding to run_xref_gene_interpt_table';
						$error_found = True;
						break;
					}
				}

				// If run_id and gene already has interpt and gene_interpt_id is the same then get run_xref_gene_interpt_id
				else if (!empty($xref_found) && $xref_found[0]['gene_interpt_id'] === $gene_interpt_id)
				{
					$run_xref_gene_interpt_id = $xref_found[0]['run_xref_gene_interpt_id'];
				}
				
				// If run_id and gene not already in table add a new row.  then get run_xref_gene_interpt_id 
				else if (empty($xref_found))
				{
					$insertArray = array(
						'user_id' 		=> 	USER_ID,
						'gene_interpt_id' 	=> 	$gene_interpt_id,
						'run_id'			=> 	$_POST['run_id'],
						'gene' 			=> 	$entry['gene_name']
					);

					$xref_result = $db->addOrModifyRecord('run_xref_gene_interpt',$insertArray);	

					if ($xref_result[0] == 1)
					{

						$run_xref_gene_interpt_id = $xref_result[1];
					}
					else
					{
						$message = 'Something went wrong while adding to run_xref_gene_interpt_table';
						$error_found = True;
						break;
					}					
				}
			}
		}

		if (!$error_found)
		{

			// only update if the step has not been completed 
			$step_status = $stepTracker->StepStatus($completed_steps, $page);

			if ($step_status != 'completed')
			{
				// add step completed to step_run_xref if not already completed
	     		$url = $stepTracker->UpdateStepInDb($page, $completed_steps, RUN_ID, USER_ID);
			} 			
			
			$next_step = $stepTracker->FindNextStep($page);
			header('Location:'.REDIRECT_URL.'?page='.$next_step.'&'.EXTRA_GETS);  
		}

	}

	if (isset($_POST['add_gene_interpetation_skip_submit']))
	{

		$next_step = $stepTracker->SkipStep($completed_steps, $page, RUN_ID, USER_ID);
		header('Location:'.REDIRECT_URL.'?page='.$next_step.'&'.EXTRA_GETS);  
	}

?>