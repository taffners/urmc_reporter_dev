<?php

	$page_title = 'Delete SNVS for Gene Knowledge Base';


	////////////////////////////////////////////////////////////////
	// Get list of genes from cosmic/genes_in_assays.csv
	////////////////////////////////////////////////////////////////
	$file_reader_array = array(
		'db' 		=> 	$db,
		'file'   		=>	'cosmic/genes_in_assays.csv',
		'type'		=> 	'genes_in_assays'
	);	

	$genesFileReader = new FileReader($file_reader_array);

	$upload_genes = $genesFileReader->return_data();

	$gene_list = $db->listAll('unique-gene-list');

	$genesFileReader->close_file();

?>