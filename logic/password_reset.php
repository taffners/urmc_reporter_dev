<?php
	$page_title = 'Reset Password';


	if (isset($_POST['password_reset_submit']))
	{
		/////////////////////////////////////////////////////////////////
		// Check if password1 == password2
		/////////////////////////////////////////////////////////////////
		if ($_POST['new_password1']  !== $_POST['new_password2'])
		{
			$message = 'Make sure password 1 equals password 2';
		}

		/////////////////////////////////////////////////////////////////
		// Check if password1 == current_password
		////////////////////////////////////////////////////////////////
		else if ($_POST['new_password1']  === $_POST['current_password'])
		{
			$message = 'Can not use current password.';
		}

		// Make sure a valid email address
		else if (	
					!filter_var($_POST['email_address'], FILTER_VALIDATE_EMAIL) 
				)
		{
			$message = 'Enter an email address';
		}

		// Make sure either my gmail account or the email account contains urmc.rochester.edu
		else if (	
					$_POST['email_address']	!== 'taffners@gmail.com' &&
					strpos($_POST['email_address'], '@urmc.rochester.edu') === false &&
					strpos($_POST['email_address'], '@URMC.Rochester.edu') === false
				)
		{
			$message = 'Enter an email address approved email address';
		}

		/////////////////////////////////////////////////////////////////
		// Make sure password is at least 7 chars long and no more than 60 chars
		////////////////////////////////////////////////////////////////
		else if (strlen($_POST['new_password1']) < 7)
		{
			$message = 'Password too short';
		}

		else if (strlen($_POST['new_password1']) > 60)
		{
			$message = 'Password too long';
		}
		/////////////////////////////////////////////////////////////////
		// Check if password in common passwords
		/////////////////////////////////////////////////////////////////
		else
		{
			$file_reader_array = array(
				'db' 		=> 	$db,
				'file'   		=>	'common_passwords.csv',
				'type'		=> 	'common_passwords',
				'password' 	=> 	$_POST['new_password1']
			);	

			$commonPasswordFileReader = new FileReader($file_reader_array);

			$password_found = $commonPasswordFileReader->readCommonPasswords();

			$commonPasswordFileReader->close_file();
			/////////////////////////////////////////////////////////////
			// Make sure user does not enter a common password.
			/////////////////////////////////////////////////////////////
			if ($password_found !== False)
			{
				$message = $password_found;

				/////////////////////////////////////////////////////////////
				// add the $_POST variable to a password table.  This will be
				// fun to find out who uses awful passwords. 
				/////////////////////////////////////////////////////////////
				$add_array = array(
					'email_address' 	=> 	$_POST['email_address'],
					'password' 		=> 	$_POST['new_password1']
				);
				
				$top_password_result = $db->addOrModifyRecord('top_passwords_used_table', $add_array);
			}
			
			else if ($password_found === False)
			{

				do
				{


					///////////////////////////////////////////////////////////////
					// Check that user email_address is in database and it is not locked and does not
					// have > 5 attempts exist in the last 30 mins. This needs to be done since any
					// user could find this page without being directed here. 
					///////////////////////////////////////////////////////////////
					$login_array = array(
							'email_address'	=> $_POST['email_address'],
							'status' 		=> 'failed',
							'password'		=> md5($_POST['new_password1'])
						);

					$user_info = $db->listAll('user-by-email', $login_array);




					$user_login_attempts = $db->listAll('user-num-failed-attempts', $_POST['email_address']);
					
					// if user can not be found notify user of incorrect email address
					if (!isset($user_info) || empty($user_info) || empty($user_info[0]['password_need_reset']) || !isset($user_info[0]['user_id'])) 
					{
						$message = 'Email address incorrect.  Try again.';
						break;
					}

					if ($user_info[0]['password_need_reset'] == 2)
					{
						header('Location:'.REDIRECT_URL.'/templates/account_locked.php');
						break;
					}
					
					if (((!empty($user_login_attempts) && $user_login_attempts[0]['login_count'] > 5)))
					{
						// add a failed login attempt
						$db->logLogin($login_array);
						header('Location:'.REDIRECT_URL.'/templates/to_many_login_attempts.php');
						break;
					}

					//////////////////////////////////////
					// Make sure user did not already use this password.
					/////////////////////////////////////
					$search_array = array(
						'user_id'	=> 	$user_info[0]['user_id'],
						'password'	=>	md5($_POST['new_password1'])
					);
					
					$previous_passwords = $db->listAll('previous-passwords', $search_array);
					if (!empty($previous_passwords))
					{
						$message = 'This password was already used.  Please try again.';
						break;
					}

					////////////////////////////////////////////////////////////////////////
					// allow user to reset password.  They are already a user and do not have
					// a locked account
					////////////////////////////////////////////////////////////////////////
				
					///////////////////////////////////////////////////////////////
					// Check if Current Email address and password are in database.
					///////////////////////////////////////////////////////////////
					$check_array = array(
							'email_address' 	=> $_POST['email_address'],
							'password' 	=> md5($_POST['current_password'])
						);

					$check_result = $db->listAll('user-exist', $check_array);
					

					///////////////////////////////////////////////////////////////
					// If user found update password and password_need_reset
					///////////////////////////////////////////////////////////////
					if (sizeof($check_result) == 1 && !empty($check_result))
					{
						$update_user = $check_result[0];
						$update_user['password_need_reset'] = 0;
						$update_user['password'] = md5($_POST['new_password1']);
						unset($update_user['time_stamp']);

						$update_result = $db->addOrModifyRecord('user_table', $update_user);

						if ($update_result[0] == 1)
						{
							header('Location:'.REDIRECT_URL.'?page=login');
						}
						else
						{
							$message ='Something went wrong with resetting your password.  Please notify Samantha';
							break;
						}					
					}
					
					///////////////////////////////////////////////////////////////
					// add all attempts to log_fails
					///////////////////////////////////////////////////////////////
					else
					{
						$ip = $db->SetIP();
						$failed_array = array(
								'email_address' 	=> $_POST['email_address'],
								'ip_address'		=> $ip['ip'],
								'status' 			=> 'failed'
							);
						$db->logLogin($failed_array);
						
						$message = 'email address or password not found.  Try again';
						break;
					}
						
				} while(false);		
			}
		}



	}
?>