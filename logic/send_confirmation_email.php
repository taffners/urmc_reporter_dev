<?php

	$extra_gets = $utils->allGetsInStr(array('page', 'ngs_stage', 'variant_filter', 'previous_page', 'genes', 'coding', 'amino_acid_change', 'header_status', 'revise', 'observed_variant_id'));

	if 	(
			!isset($_GET['observed_variant_id']) ||
			!isset($_GET['run_id']) ||
			!isset($_GET['visit_id'])
		)
	{

		header('Location:'.REDIRECT_URL.'?page=confirmations&'.$extra_gets);		
	}

	require_once('logic/shared_logic/report_shared_logic.php');

	// find everyone with reviewer permission
	$all_confirmation = $db->listAll('get-all-users-with-a-permission', 'confirmation');
	
	//////////////////////////////////////////////////////////////////
	// send confirmation, add confirmation to pending confirmation list, 
	//////////////////////////////////////////////////////////////////
	# Get all previous messages in comment table with run_id and comment_type message_board
	$all_messages = $db->listAll('message-board-comments-plus-visit-comments', RUN_ID);

	# submit message and send email to people to notify
	if 	(
			isset($_POST['send_confirmation_email_submit']) && 
			!empty($_POST['send_confirmation_email_submit']) && 
			!empty($_POST['send_confirmation_email_submit']) && 
			$utils->validateGetInt('visit_id') &&
			!empty($all_confirmation)
		)
	{
		
		/////////////////////////////////////////////////////
		# Find all confirmation 
		/////////////////////////////////////////////////////
		$to = '';
		$to_names = '';
		
		foreach($all_confirmation as $key => $confirmation_member)
		{

			if ($confirmation_member['email_address'] === 'taffners@gmail.com')
			{
				$confirmation_member['email_address'] = 'samantha_taffner@urmc.rochester.edu';
			}

			if ($to !== '')
			{
				$to.=',';
				$to_names.=', ';
			}

			$to.=$confirmation_member['email_address'];
			$to_names.=$confirmation_member['user_name'];
		}

		$to = $to;
		$subject = 'New Confirmation # '.$_GET['visit_id'];
		$body = 'Hello '.$to_names.',

			'.$_SESSION['user']['first_name'].' would like to request the following confirmation:

			"'.$_POST['message'].'"';
		

		$email_utils->sendEmail($to, $subject, $body, 'skip');
			
		/////////////////////////////////////////////////////
		# Add message to comment table so it will show in the message board for record keeping.
		/////////////////////////////////////////////////////
		$message_comment_array = array(
			'user_id'		=> 	USER_ID,
			'comment_ref'	=> 	RUN_ID,
			'comment_type'	=> 	'message_board',
			'comment'		=> 	$_POST['message']	
		);

		$comment_add_result =$db->addOrModifyRecord('comment_table', $message_comment_array);

		/////////////////////////////////////////////////////
		// add to confirmation_table
		/////////////////////////////////////////////////////
		$confirmation_array = array(
			'user_id'					=> 	USER_ID,
			'run_id'					=> 	RUN_ID,
			'visit_id'					=> 	$_GET['visit_id'],
			'observed_variant_id'		=> 	$_GET['observed_variant_id'],
			'message'					=> 	$_POST['message'],
			'status'					=> 	'pending'
		);

		$confirmation_result =$db->addOrModifyRecord('confirmation_table', $confirmation_array);

		header('Location:'.REDIRECT_URL.'?page=confirmations&'.$extra_gets);

	}
	else if (isset($_POST['send_confirmation_email_submit']) && empty($_POST['send_confirmation_email_submit']))
	{
		$message = 'Select at least one person to notify of the message ';
	}
	else if (isset($_POST['send_confirmation_email_submit']) && empty($_POST['send_confirmation_email_submit']))
	{
		$message = 'Add a message to submit';
	}
?>