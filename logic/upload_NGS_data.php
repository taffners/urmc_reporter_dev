<?php

	$page_title = 'Upload NGS Data';

	if (isset($_GET) && isset($_GET['visit_id']))
	{
		$completed_steps = $db->listAll('all-complete-pre-steps', $_GET['visit_id']);

		// see if visit already added
		$visitArray = $db->listAll('add-visit-info', $_GET['visit_id']);	

		$ngs_panel = $visitArray[0]['ngs_panel'];

		// find if test is live
		$ngs_panel_full_info = $db->listAll('ngs-panel-info-by-id', $visitArray[0]['ngs_panel_id']);

		if( strtotime($ngs_panel_full_info[0]['go_live_date']) > strtotime('now') ) 
		{
			$is_test_live = 'no';
		}
		else
		{
			$is_test_live = 'yes';
		}
	}
	else
	{
		header('Location:'.REDIRECT_URL.'?page=home');
	}

	if (isset($_GET['library_pool_id']))
	{
		$library_pool = $db->listAll('library_pool_by_id', $_GET['library_pool_id']);
	}

	/////////////////////////////////////////////////////////////////////////////////////////////
	// Adjust for the old non pool uploading vs the new pool uploading system.
	/////////////////////////////////////////////////////////////////////////////////////////////
	if (isset($_GET['pool_chip_linker_id']) && isset($_GET['ngs_panel_id']))
	{
		$pending_pre_runs = $db->listAll('pending_pre_runs_library_pool_visit_id', array('library_pool_id' => $_GET['library_pool_id'], 'ngs_panel_id' => $_GET['ngs_panel_id'], 'visit_id' => $_GET['visit_id']));				
	}
	else
	{
		$pending_pre_runs = $db->listAll('pending-pre-samples-by-visit-id', $_GET['visit_id']);			
	}
	
	$all_wet_bench_users = $db->listAll('all_wet_bench_users');
	
	////////////////////////////////////////////////////////////////
	// Get list of genes from cosmic/genes_in_assays.csv
	////////////////////////////////////////////////////////////////
	$file_reader_array = array(
		'db' 		=> 	$db,
		'file'   		=>	'cosmic/genes_in_assays.csv',
		'type'		=> 	'genes_in_assays'
	);		

	$genesFileReader = new FileReader($file_reader_array);

	$upload_genes = $genesFileReader->return_data();
	
	$genesFileReader->close_file();
	//////////////////////////////////////////////////////////////////////////////
	// Check submit button was pushed and no file was uploaded
	//////////////////////////////////////////////////////////////////////////////
	if (isset($_POST['upload_NGS_data_submit']) && (empty($_FILES['NGS_file']['tmp_name']) || empty($_FILES['NGS_file']['name']) && !empty($_FILES['NGS_file']['type']) ))
	{
		var_dump(htmlspecialchars($_FILES['NGS_file']));
		$message = 'Make sure you selected a TSV file to upload';
	}

	//////////////////////////////////////////////////////////////////////////////
	// Check that files completely transfered.  This is done by a md5 hash of the
	// file performed by both javascript and php
	//////////////////////////////////////////////////////////////////////////////	
	else if (isset($_POST['upload_NGS_data_submit']) && (empty($_POST['ngs-tsv-md5']) || empty($_POST['amp-cov-md5'])) )
	{
		$message = 'The md5 hash of a file was not made.  Please contact Samantha.';
	}
	
	// check ngs tsv of coverage file.
	else if (isset($_POST['upload_NGS_data_submit']) && isset($_POST['ngs-tsv-md5']) && md5_file($_FILES['NGS_file']['tmp_name']) !== $_POST['ngs-tsv-md5'] )
	{

		$message = 'The NGS TSV file does not look like it completely uploaded.  Please contact Samantha.';
	}

	// check md5 of coverage file.
	else if (isset($_POST['upload_NGS_data_submit']) && isset($_POST['amp-cov-md5']) && md5_file($_FILES['depth_thres']['tmp_name']) !== $_POST['amp-cov-md5'] )
	{

		$message = 'The coverage file does not look like it completely uploaded.  Please contact Samantha.';
	}

	//////////////////////////////////////////////////////////////////////////////
	// Make sure wet bench tech is filled in
	//////////////////////////////////////////////////////////////////////////////
	else if (isset($_POST['upload_NGS_data_submit']) && !isset($_POST['wet_bench_techs']))
	{
		$message = 'Please choose at least one wet bench tech';
	}

	//////////////////////////////////////////////////////////////////////////////
	// Check if file is tab separated. This function skips any lines that starts
	// with a #
	//////////////////////////////////////////////////////////////////////////////
	else if (isset($_POST['upload_NGS_data_submit']) && isset($_FILES['NGS_file']['tmp_name']) && !empty($_FILES['NGS_file']['name']) && !empty($_FILES['NGS_file']['type']) && !$utils->IsFileDelimiterSeperated("\t", $_FILES['NGS_file']['tmp_name'], $_FILES['NGS_file']['type']))
	{
		$message = 'The file '.htmlspecialchars($_FILES['NGS_file']['name']).' that was uploaded does not appear to be a tsv file.';
	}

	//////////////////////////////////////////////////////////////////////////////
	// Make sure that coverage file is uploaded.  Name of file was changed from 
	// depth_tres to depth_coverage.  This is the same file. All sample uploads
	// requires a depth_thres file.  If no low coverage Amplicons are found an 
	// empty file with headers will be uploaded.  
	//////////////////////////////////////////////////////////////////////////////
	else if (isset($_POST['upload_NGS_data_submit']) && empty($_FILES['depth_thres']['tmp_name']))
	{
		$message = 'A coverage file is required.';
	}
	//////////////////////////////////////////////////////////////////////////////
	// Check that the Coverage File is the correct format.  Need to make sure 
	// that an empty file will pass this check and that samples not found in 
	// coverage file will pass check.    
	//////////////////////////////////////////////////////////////////////////////
	else if 	(
					isset($_POST['upload_NGS_data_submit']) && 
					$_POST['not_found'] !== 'no_low_amp' && 
					$utils->CovFileTypeCorrect($_FILES['depth_thres']['tmp_name'], $upload_genes, $_POST['sample_name']) !== True)
	{
		$message = '<strong>check cov file '.htmlspecialchars($utils->CovFileTypeCorrect($_FILES['depth_thres']['tmp_name'], $upload_genes, $_POST['sample_name'])).'</strong>';
	}


	//////////////////////////////////////////////////////////////////////////////
	// Check that $_POST includes sample_name
	//////////////////////////////////////////////////////////////////////////////
	else if 	(
					isset($_POST['upload_NGS_data_submit']) && 
					(
						!isset($_POST['sample_name']) || empty($_POST['sample_name'])
					)
				)
	{
		$message = '<strong>Please enter a sample name</strong>';	
	}

	//////////////////////////////////////////////////////////////////////////////
	// Check that the tsv file name selected includes the sample name
	// Turn this off for testing purposes
	//////////////////////////////////////////////////////////////////////////////
	else if 	(
					isset($_POST['upload_NGS_data_submit']) &&  
					strpos($_FILES['NGS_file']['name'], $_POST['sample_name']) === false
				)
	{	
		$message = '<h1>The filename of the uploaded tsv appears to be incorrect:</h1><br>Sample Name: '.htmlspecialchars($_POST['sample_name']).'<br><br>File Name: '.htmlspecialchars($_FILES['NGS_file']['name']);

		if ($ngs_panel === 'Oncomine Focus Hotspot Assay' && isset($user_permssions) &&  strpos($user_permssions, 'ngs_samp_nam_ov') !== false)
     	{

     		$message.='<br><br>

     		<h2>You can use the Manual Override Sample Name button below if you are sure that '.htmlspecialchars($_POST['sample_name']).' is not the correct sample name.  After clicking the button add the sample name as it is in the file name.</h2>
     		';
     	}			
	}

	//////////////////////////////////////////////////////////////////////////////
	// For ofa make sure the NGS tsv file name contains _filtered_SB_added.tsv 
	//////////////////////////////////////////////////////////////////////////////
	else if 	(
					isset($_POST['upload_NGS_data_submit']) &&  
					$_POST['ngs-panel'] == 'Oncomine Focus Hotspot Assay' &&
					strpos($_FILES['NGS_file']['name'], 'filtered_SB_added.tsv') === false 
				)
	{	
		$message = '<strong>The NGS tsv file format is incorrect.  It needs to be the filtered_SB_added.tsv:</strong><br><br>Sample Name: '.$_POST['sample_name'].'<br><br>File Name: '.$_FILES['NGS_file']['name'];			
	}

	//////////////////////////////////////////////////////////////////////////////
	// For ofa make sure the Amplicon Coverage file name is all_coverage_amplicons.tsv
	//////////////////////////////////////////////////////////////////////////////
	else if 	(
					isset($_POST['upload_NGS_data_submit']) &&  
					$_POST['ngs-panel'] == 'Oncomine Focus Hotspot Assay' &&
					$_FILES['depth_thres']['name'] !== 'all_coverage_amplicons.tsv'
				)
	{	
		$message = '<strong>The amplicon coverage file format is incorrect.  It needs to be the all_coverage_amplicons.tsv:</strong><br><br>Sample Name: '.$_POST['sample_name'].'<br><br>File Name: '.$_FILES['depth_thres']['name'];			
	}

	//////////////////////////////////////////////////////////////////////////////
	// upload NGS file as tmp file
	//////////////////////////////////////////////////////////////////////////////
	else if (isset($_POST['upload_NGS_data_submit']) && !empty($_FILES['NGS_file']['tmp_name']) && isset($visitArray))
	{
		do 
		{

			/////////////////////////////////////////////////////////////////////////
			// After submission $_FILES['NGS_file'] array looks like:	
				// 'name' => string '17-SCN3255_170809B_Filt-full.tsv' (length=32)
				// 'type' => string 'text/tab-separated-values' (length=25)
				// 'tmp_name' => string '/tmp/phpzjIMUX' (length=14)
				// 'error' => int 0
				// 'size' => int 5314
			/////////////////////////////////////////////////////////////////////////

			/////////////////////////////////////////////////////////////////////////
			// Need to insert into run_info table, observed_variant table and update visit table with run_id
				// steps:
					// insert into run_info table and get run_id
					// insert each variant into observed_variant table using run_id
					// update visit_table to include the run_id
			/////////////////////////////////////////////////////////////////////////		
			/////////////////////////////////////////////////////////////////////////
			// Get info for the run_info table.  OFA has information in header of
			// NGS file.  Myeloid is user input.
			/////////////////////////////////////////////////////////////////////////
			$run_info_array = array();

			/////////////////////////////////////////////////////////////////////////
			// For OFA add header info.  Open file and close.  
			// !!!!!!!!!!!!!This code is turned off because there's no longer a header to the tsv file.
			/////////////////////////////////////////////////////////////////////////
			// if ($_POST['ngs-panel'] == 'Oncomine Focus Hotspot Assay')
			// {

			// 	$file_reader_array = array(
			// 		'db' 			=> 	$db,
			// 		'file'   		=>	$_FILES['NGS_file']['tmp_name'],
			// 		'type'			=> 	'torrent_header'
			// 	);		

			// 	$TorrentHeaderReader = new FileReader($file_reader_array);
			// 	$header = $TorrentHeaderReader->GetTorrentheader();	
				
			// 	$run_info_array['filter_chain'] = isset($header['filter_chain']) ? $header['filter_chain'] : '';
				
			// 	$run_info_array['work_flow_name'] = isset($header['work_flow_name']) ? $header['work_flow_name'] : '';	
					
			// }

			/////////////////////////////////////////////////////////////////////////
			// Add run_info_table report_type and transfered_to_patient_chart
				// Control Samples
					// visitArray[0][control_type_id] != null or 0 
					// set report_type = na
					// set transfered_to_patient_char = na

				// Sample is a test but not a live test
					// set report_type = validation
					// set transfered_to_patient_char = pending

				// default
					// set report_type = reportable
					// set transfered_to_patient_char = pending
			/////////////////////////////////////////////////////////////////////////
			if (
				isset($visitArray[0]) && 
				(
					$visitArray[0]['control_type_id'] === null || 
					$visitArray[0]['control_type_id'] == 0
				) &&
				$is_test_live === 'yes'
			)
			{
				$run_info_array['report_type'] = 'reportable';
				$run_info_array['transferred_to_patient_chart'] = 'pending';
			}
			else if (
				isset($visitArray[0]) && 
				(
					$visitArray[0]['control_type_id'] === null || 
					$visitArray[0]['control_type_id'] == 0
				) &&
				$is_test_live === 'no'
			)
			{
				$run_info_array['report_type'] = 'validation';
				$run_info_array['transferred_to_patient_chart'] = 'pending';
			}
			else
			{
				$run_info_array['report_type'] = 'na';
				$run_info_array['transferred_to_patient_chart'] = 'na';	
			}			

			/////////////////////////////////////////////////////////////////////////
			// Add links to pool.  This is new (2/11/2020) and might not be used for all panels so I need to check
			// if they are present before adding.
			/////////////////////////////////////////////////////////////////////////
			if 	( isset($_GET['pool_chip_linker_id']) && isset($_GET['library_pool_id']) )
			{
				$run_info_array['pool_chip_linker_id'] = $_GET['pool_chip_linker_id'];
				$run_info_array['library_pool_id'] = $_GET['library_pool_id'];
			}
			
			/////////////////////////////////////////////////////////////////////////
			// Add user input to run_info_array
			/////////////////////////////////////////////////////////////////////////
			if (isset($_POST['sample_name']) || isset($_POST['run_date']))
			{
				$run_info_array['sample_name'] = isset($_POST['sample_name']) ? $_POST['sample_name'] : '';

				$run_info_array['run_date'] = isset($_POST['run_date']) ? $_POST['run_date'] : '';
			}

			/////////////////////////////////////////////////////////////////////////
			// Can not proceed if run info array was not produced. 
			/////////////////////////////////////////////////////////////////////////
			if (!empty($run_info_array))
			{
				// This will produce a new entry in run_info_table since run_id
				// is not being added to run_info_array
				
				$run_id_result = $db->addOrModifyRecord('run_info_table', $run_info_array);
				
				if ($run_id_result[0] === true)
				{				

					//////////////////////////////////////////////////////////////////
					// Add a tracking of when the manual override is used and record it in the user_update_task_table
					/////////////////////////////////////////////////////////////////
					if (isset($_POST['manual_override_performed']) && $_POST['manual_override_performed'] === 'yes')
					{
						$user_update_task_array = array();
						$user_update_task_array['ref_id'] = $run_id_result[1];
						$user_update_task_array['user_id'] = USER_ID;
						$user_update_task_array['task'] = 'run_info_sample_name';
						$user_update_task_array['ref'] = 'run_info_table';
						$user_update_task_array['before_val'] = $_POST['manual_override_original_name'];
						$user_update_task_array['after_val'] = $_POST['sample_name'];

						$user_update_task_result = $db->addOrModifyRecord('user_update_task_table', $user_update_task_array);						
					}

					///////////////////////////////////////////////////////////////
					// Add wet bench tech user_id and run_id to user_run_task_xref with task 'wet_bench'
					///////////////////////////////////////////////////////////////
					foreach ($_POST['wet_bench_techs'] as $key => $wet_user_id)
					{
						// add wet bench tech 
						$wet_bench_add_array = array(
							'user_id' 	=> 			$wet_user_id,
							'run_id' 		=> 			$run_id_result[1],
							'task' 		=> 			'wet_bench'
						);

						
						$wet_id_result = $db->addOrModifyRecord('user_run_task_xref', $wet_bench_add_array);
									
					}

					///////////////////////////////////////////////////////////////
					// Add status to low_coverage_status_table
						// $cov_type determines what type low coverage data will be added.  For instance the myeloid panel displays exon locations and the ofa displays start and stop locations in the genome.
					///////////////////////////////////////////////////////////////
					if (isset($_POST['cov_file_type_id']) && $_POST['cov_file_type_id'] === 'torrent_coverage_file')
					{
						$cov_type = 'chr_start_stop';
					}
					else
					{
						$cov_type = 'exon_codons';
					}

					$low_cov_status = array(
						'user_id' 	=>	USER_ID,
						'run_id' 		=> 	$run_id_result[1],
						'status'		=>   $_POST['not_found'],
						'type'		=> 	$cov_type
					);	

					
					$low_cov_status_result = $db->addOrModifyRecord('low_coverage_status_table', $low_cov_status);	
							
					
					/////////////////////////////////////////////////////////////
					// Add coverage data need to account for empty or missing
					// sample.  This means that the sample does not have low
					// coverage amplicons.  This happens very often for the OFA. Should be taken care of in JavaScript since not_found will not 
					// equal low_amp_present.  This step will be skipped
					/////////////////////////////////////////////////////////////
					if ($_POST['not_found'] === 'low_amp_present')
					{

						$file_reader_array = array(
							'db' 			=> 	$db,
							'file'   		=>	$_FILES['depth_thres']['tmp_name'],

							// type tells FileReader what type of format to expect.  Example ofa torrent_coverage_file, myeloid depth_coverage
							'type'			=> 	isset($_POST['cov_file_type_id']) ? $_POST['cov_file_type_id'] : 'depth_coverage',
							'run_id'		=>	$run_id_result[1],
							'sample_name' 	=> 	$_POST['sample_name'],

						);		

						// read low coverage file depending on the $file_reader_array[type] and add < 500 cov to low_coverage_table in db.
						$covFileReader = new FileReader($file_reader_array);
					}				

					// Get variant data from tsv file
					$file_reader_array = array(
						'db' 			=> 	$db,
						'file'   		=>	$_FILES['NGS_file']['tmp_name'],
						'type'			=> 	$_POST['ngs-panel'], // example Oncomine Focus Hotspot Assay
						'run_id'		=>	$run_id_result[1],
						'sample_name' 	=> 	$_POST['sample_name'],
						'sample_type'	=> 	$visitArray[0]['sample_type']
					);		

					$variantFileReader = new FileReader($file_reader_array);

					// add to visit table	
					$visitArray[0]['run_id'] = $run_id_result[1];

					$visit_result = $db->addOrModifyRecord('visit_table', $visitArray[0]);

					if ($visit_result[0] == 1 && is_int($visit_result[1]))
					{
						// update steps and add pre_complete
						$url = $preStepTracker->UpdatePreStepInDb($page, $completed_steps, $visit_result[1], USER_ID, $run_id_result[1], $page, 'passed');

						$url = $preStepTracker->UpdatePreStepInDb($page, $completed_steps, $visit_result[1], USER_ID, $run_id_result[1], 'pre_complete', 'passed');

						// if qc run was completed and passed in the library pool update step for run
						if (isset($library_pool[0]['run_qc_status']) && $library_pool[0]['run_qc_status'] === 'pass')
						{
							$stepTracker = new StepTracker($db, $stepsForSNVReporting, 'step_run_xref');
							$completed_steps = $db->listAll('all-complete-steps', $_GET['visit_id']);
		     				$stepTracker->UpdateStepInDb('qc', $completed_steps, $run_id_result[1], $library_pool[0]['user_id']);
						}

						// find number of variants added
						$added_variants = $db->listAll('observed-variant-by-run-id', $run_id_result[1]);

						$num_variants = sizeof($added_variants);


						/////////////////////////////////////////////////////////////////////////
						// If the sample is being uploaded by a pool add pool_chip_linker_id and ngs_panel_id so the user is redirected to pool_upload_data page
						/////////////////////////////////////////////////////////////////////////
						if 	( isset($_GET['pool_chip_linker_id']) && isset($_GET['ngs_panel_id']) )
						{
							// redirect to page so the user can review what was uploaded and confirm
							header('Location:'.REDIRECT_URL.'?page=check_tsv_upload&run_id='.$run_id_result[1].'&visit_id='.$visit_result[1].'&num_variants='.$num_variants.'&patient_id='.$_GET['patient_id'].'&ngs_file_name='.$_FILES['NGS_file']['name'].'&pool_chip_linker_id='.$_GET['pool_chip_linker_id'].'&ngs_panel_id='.$_GET['ngs_panel_id']);
						}

						else
						{
							// redirect to page so the user can review what was uploaded and confirm
							header('Location:'.REDIRECT_URL.'?page=check_tsv_upload&run_id='.$run_id_result[1].'&visit_id='.$visit_result[1].'&num_variants='.$num_variants.'&patient_id='.$_GET['patient_id'].'&ngs_file_name='.$_FILES['NGS_file']['name']);
						}					
					}
					else
					{
						$message = 'something when wrong with adding your tsv to the database!';
						var_dump($visit_result);
					}
				}
				else
				{
					$message = 'something when wrong with adding your tsv to the database at the run_table Step';
					var_dump($run_id_result);			
				}
			}
			else
			{
				$message = 'Something went wrong.  Run info was not found';
			}
		} while(false);		
	}





		
	
	
?>