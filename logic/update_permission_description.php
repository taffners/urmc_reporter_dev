<?php
	if (
			!isset($user_permssions) || 
			strpos($user_permssions, 'admin') === false
		)
	{
		$db->logoff();
		header('Location:'.REDIRECT_URL.'?page=login');
	}

	if (
			!isset($_GET['permission_id']) || 
			empty($_GET['permission_id']) 
		)
	{
		$permission_info = array();
		$page_title = 'Add New Permission';
	}
	else
	{
		$permission_info = $db->listAll('permission-info-by-id', $_GET['permission_id']);		
		$page_title = 'Update '.$permission_info[0]['permission'].'  Description';
	}

	if 	(
			isset($_POST['update_permission_description_submit']) &&
			isset($_POST['description']) &&
			!empty($_POST['description'])
		)
	{
		$add_array = array();

		if (isset($_GET['permission_id']) && !empty($_GET['permission_id']))
		{
			$add_array['permission_id'] = $_GET['permission_id'];
		}

		$add_array['permission'] = $_POST['permission'];
		$add_array['description'] = $_POST['description'];
		
		$add_result = $db->addOrModifyRecord('permission_table', $add_array);
		header('Location:'.REDIRECT_URL.'?page=admin');
	}

?>