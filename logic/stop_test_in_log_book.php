<?php
	
	$page_title = 'Cancel Test';


	if (isset($_GET['ordered_test_id']) && isset($_GET['sample_log_book_id']))
	{
		$sampleLogBookArray = $db->listAll('all-samples-in-log-book-by-sample-log-book-id',$_GET['sample_log_book_id']);

		require_once('logic/shared_logic/backwards_compatablity_dob_sex.php');
		
		$patient_name = $sampleLogBookArray[0]['first_name'].' '.$sampleLogBookArray[0]['last_name'];
		// Get info about data entered about the test
		$orderedTestArray = $db->listAll('ordered-test-info', $_GET['ordered_test_id']);

		$test_name = $orderedTestArray[0]['test_name'];
		$full_test_name = $orderedTestArray[0]['full_test_name'];

		$orderedTestOnlyArray = $db->listAll('ordered-test-only-by-id', $_GET['ordered_test_id']);
		
	}

	// This is for tests like controls to set everything up.  Default variable set up should be via sample log book
	else if (isset($_GET['visit_id']))
	{
		$visitArray = $db->listAll('patient-by-visit-id', $_GET['visit_id']);
		$patient_name = $visitArray[0]['patient_name'];
		$test_name = $visitArray[0]['test_name'];
		$full_test_name = '';
	}

	// for all NGS tests find pools
	if (isset($_GET['visit_id']) && isset($_GET['ngs_stage']) && $_GET['ngs_stage'] == 'pre_ngs_stage')
	{
		$pools_in = $db->listAll('pools-visit-in', $_GET['visit_id']);	
	}

	if (isset($_POST['stop_test_in_log_book_submit']) && empty($_POST['comments']))
	{
		$message = 'Please add a reason for canceling this test.';
	}

	else if (isset($_POST['stop_test_in_log_book_submit']))
	{
		do 
		{

			///////////////////////////////////////////////////////
			// Change test_status in ordered_test_table to stop if ordered_test_id is provided.  Not all tests will have a ordered test id.  For instance tests which are NGS controls will not have a ordered test id.
			///////////////////////////////////////////////////////
			if (isset($_GET['ordered_test_id']) && isset($orderedTestOnlyArray[0]))
			{
				$addArray = $orderedTestOnlyArray[0];
				$addArray['test_status'] = 'stop';
				$addArray['user_id'] = USER_ID;

				$addResult = $db->addOrModifyRecord('ordered_test_table',$addArray);

				if (!$addResult[0])
				{
					$email_utils->emailAdminsProblemURL('$addResult[0] != 1');
					break;
				}
			}

			//////////////////////////////////////////
			// Add comment 
			//////////////////////////////////////////
			if (isset($_POST['comments']) && !$utils->textareaEmpty($_POST['comments']))
			{
				$commentInsertArray = array();
				$commentInsertArray['user_id'] = USER_ID;
				$commentInsertArray['comment'] =  'Reason for Canceling: '.$_POST['comments'];

				//////////////////////////////////////////
				// Add comment if ordered_test_id single analyte
				//////////////////////////////////////////
				if (isset($_GET['ordered_test_id']))
				{										
					$commentInsertArray['comment_ref'] =  $_GET['ordered_test_id'];
					$commentInsertArray['comment_type'] =  'ordered_test_table';									
				}

				//////////////////////////////////////////
				// Add comment if no ordered_test_id was provided (ngs)
				//////////////////////////////////////////
				else if (isset($_GET['visit_id']))
				{
					$commentInsertArray['comment_ref'] =  $_GET['visit_id'];
					$commentInsertArray['comment_type'] =  'visit_table';										
				}

				$comment_result = $db->addOrModifyRecord('comment_table',$commentInsertArray);
			}
							
			//////////////////////////////////////////////
			// Cancel NGS tests
			/////////////////////////////////////////////

			////////////////////////////////////////////////////////////////
			// Cancel Pre-NGS tests
			////////////////////////////////////////////////////////////////
			if (isset($_GET['visit_id']) && isset($_GET['ngs_stage']) && $_GET['ngs_stage'] == 'pre_ngs_stage')
			{
				$pre_ngs_update_array = array();
				$pre_ngs_update_array['visit_id'] = $_GET['visit_id'];
				$pre_ngs_update_array['user_id'] = USER_ID;
				$pre_ngs_update_array['step'] = 'pre_complete';
				$pre_ngs_update_array['status'] = 'Failed';

				$pre_step_result = $db->addOrModifyRecord('pre_step_visit',$pre_ngs_update_array);

				///////////////////////////////////////////////
				// Remove from pool
				///////////////////////////////////////////////	
				foreach ($pools_in as $key => $pool)
				{
					if (isset($pool['visits_in_pool_id']) && isset($pool['visit_id']) && $pool['visit_id'] === $_GET['visit_id'])
					{
						$db->deleteRecordById('visits_in_pool_table','visits_in_pool_id', $pool['visits_in_pool_id']);	
					}			
				}

				header('Location:'.REDIRECT_URL.'?page=home');
			}

			////////////////////////////////////////////////////////////////
			// Cancel ngs_report_building NGS tests
			////////////////////////////////////////////////////////////////
			if (isset($_GET['run_id']) && !empty($_GET['run_id']) && isset($_GET['ngs_stage']) && $_GET['ngs_stage'] == 'ngs_report_building')
			{
				// find run_info_table record associate with the run_id
				$run_info_record =  $db->listAll('run-info-record-only', $_GET['run_id']);

				if(!empty($run_info_record))
				{
					$run_info_record[0]['status'] = 'hide';

					$visit_result = $db->addOrModifyRecord('run_info_table',$run_info_record[0]);
					header('Location:'.REDIRECT_URL.'?page=home');
				}
			}

			else
			{
				header('Location:'.REDIRECT_URL.'?page=sample_log_book');	
			}
			
		} while(false);
	}
