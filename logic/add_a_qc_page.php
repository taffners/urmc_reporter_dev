<?php

	$page_title = 'Add or update a page in QC documentation';

	$all_modules = $qc_db->listAll('qc_db-all-modules');
	$reporter_permission_list = $db->listAll('reporter-permission-list');
	
	///////////////////////////////
	// set up form
	///////////////////////////////
	if (isset($_GET['page_id']))
	{
		$pageArray = $qc_db->listAll('qc_db-qc-page-info', $_GET['page_id']);	
	}
	else
	{
		$pageArray = array();
	}

	///////////////////////////////
	// submit form
	///////////////////////////////
	if (isset($_POST['add_a_qc_page_submit']) && !isset($_POST['page_name']))
	{
		$message = 'Please provide a page name.';
	}

	else if (isset($_POST['add_a_qc_page_submit']) && !isset($_GET['page_id']))
	{
		$message = 'This page only works for updating.  Use add_all_missing_qc_pages to add a page.  This ensures no typos in naming and all current pages are added to validation document';
	}

	else if (isset($_POST['add_a_qc_page_submit']) )
	{
		

		$add_array = array();

		if (isset($_GET['page_id']))
		{
			$add_array['page_id'] = $_GET['page_id'];
		}

		$add_array['user_id'] = USER_ID;
		$add_array['user_name'] = $_SESSION['user']['first_name'].' '.$_SESSION['user']['last_name'];

		$add_array['page_name'] = $_POST['page_name'];
		$add_array['module_id'] = $_POST['module_id'];
		$add_array['page_description'] = $_POST['page_description'];
		$add_array['how_to_access_page'] = $_POST['how_to_access_page'];
		$add_array['page_assumption'] = $_POST['page_assumption'];
		$add_array['website_name'] = SITE_TITLE;
		$add_array['page_type'] = $_POST['page_type'];
		$add_array['printing_necessary'] = $_POST['printing_necessary'];

		// Make all restricted_permissions into a comma separated string.
		$permissions = '';
		foreach ($_POST['restricted_permissions'] as $key => $permission)
		{
			if ($permissions !== '')
			{
				$permissions.=', ';
			}

			$permissions.= $permission;
		}
		$add_array['permissions'] = $permissions;


		$page_result = $qc_db->addOrModifyRecord('page_table',$add_array);

		header('Location:'.REDIRECT_URL.'?page=all_qc_pages');


	}

?>