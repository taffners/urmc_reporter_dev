<?php
	require_once('logic/shared_logic/qc_variant_shared_logic.php');
	
	require_once('logic/shared_logic/strand_bias.php');
	
	// find everyone with reviewer permission
	$all_reviewers = $db->listAll('users-review-permissions');
	$all_directors = $db->listAll('users-director-permissions');
	
	//////////////////////////////////////////////////////////////////
	// run message board
	//////////////////////////////////////////////////////////////////
	require_once('logic/shared_logic/message_board.php');

	if (isset($_POST['qc_strand_bias_submit']))
	{
				// make sure the form only updates completed once
				// only update if the step has not been completed 
		$step_status = $qcVarStepTracker->StepStatus($completed_steps, $page);

		if ($step_status != 'completed')
		{
			
			// add step completed to step_run_xref if not already completed
     		$url = $qcVarStepTracker->UpdateStepInDb($page, $completed_steps, RUN_ID, USER_ID);
     		header('Location:'.REDIRECT_URL.$url.'&patient_id='.$_GET['patient_id'].'&visit_id='.$_GET['visit_id']);
		}    
		else
		{
			$next_step = $qcVarStepTracker->FindNextStep($page);
			header('Location:'.REDIRECT_URL.'?page='.$next_step.'&run_id='.$_GET['run_id'].'&patient_id='.$_GET['patient_id'].'&visit_id='.$_GET['visit_id']);
		}
	}
		
	/////////////////////////////////////////////////////////////////////
	// The Bill wants to save IGV images of the variants (annoyed).  
	// Add images to the doc_table.  Use run_id as ref_id and igv_image as ref_table
		// check that the format of the files are accepted file type
	/////////////////////////////////////////////////////////////////////
	if 	(		
			isset($_POST['upload_igv_images_sumbit']) &&
			isset($_FILES['IGV_files']) &&
			$utils->CheckSecurityOfFile($_FILES['IGV_files'], array('png','PNG'))&&
			$utils->validateGetInt('run_id')
		)
	{
		
		///////////////////////////////////////////////////////////////////////
		// Upload files to server while checking md5 of file to ensure that the files uploaded completely.  
			// Function returns an array to add to doc_table 
			// If message is returned in array this means something went wrong 
		///////////////////////////////////////////////////////////////////////
		$uploadResult =$utils->uploadMultipleFiles($_GET['run_id'], 'igv_image', $_FILES['IGV_files'], 'igv', $_POST['md5_file_hash_array']);

		$err_message = '';

		// Each file uploaded will have information in the uploadResult.  
		// If anything was returned notify user.  If no errors are found refresh page
		// so it updates user of documents uploaded and provides a button to download 
		// the images.
		foreach ($uploadResult as $key => $f)
		{
			// Messages are for the user update
			if (isset($f['message']))
			{
				$err_message.=$f['message'].'<br>';
			}

			// file_name provides the user with the file that has a message
			if (isset($f['file_name']))
			{
				$err_message.='&emsp;- '.$f['file_name'].'<br>';
			}

			// admin_message is returned when a more serious error occurs and 
			// the admin needs to be notified.
			// If an admin_message is provided a message will also be provided
			if (isset($f['admin_message']))
			{
				$email_utils->emailAdminsProblemURL($f['admin_message']);
			}

			// add file info if all doc_table is 
			$all_fields_found = True;

			if 	(
					isset($f['user_id']) && 
					isset($f['ref_id']) && 
					isset($f['file_loc']) && 
					isset($f['name_uploaded_as']) && 
					isset($f['md5_hash']) && 
					isset($f['file_size'])  
				)
			{
				$insert_array = array(
				'user_id'		=> 	$f['user_id'],
				'ref_id'		=> 	$f['ref_id'],
				'file_loc'	=> 	$f['file_loc'],
				'name_uploaded_as'	=> $f['name_uploaded_as'],
				'md5_hash'	=> 	$f['md5_hash'],
				'file_size'	=> 	$f['file_size']
				);

				$add_result = $db->addOrModifyRecord('doc_table', $insert_array);
			}
			else if ($err_message === '')
			{
				$err_message.= 'Something went wrong with file upload.';
				$email_utils->emailAdminsProblemURL(__FILE__.' Error uploading IGV files!! Line #: '.__LINE__.' doc_table info not returned.');
			}
		}

		// if no error occurred refresh page so user can download files.
		if ($err_message === '')
		{
			header("Refresh:0");
		}

		else 
		{
			$message = $err_message;
		}

	}	
	else if 	(
				isset($_POST['upload_igv_images_sumbit']) &&
				isset($_FILES['IGV_files']) &&
				!$utils->CheckSecurityOfFile($_FILES['IGV_files'], array('png','PNG'))
			)
	{
		$message = 'Error occurred while uploading file.  Please see '.SITE_TITLE.' Administrator';
		$email_utils->emailAdminsProblemURL(__FILE__.' Error uploading IGV files!! Line #: '.__LINE__.' Not secure file.');
	}

	else if 	(
				isset($_POST['upload_igv_images_sumbit']) &&
				isset($_FILES['IGV_files']) &&
				!$utils->validateGetInt('run_id')
			)
	{
		$message = 'Error occurred while uploading file.  Please see '.SITE_TITLE.' Administrator';
		$email_utils->emailAdminsProblemURL(__FILE__.' Error uploading IGV files!! Line #: '.__LINE__.'  Run_id not valid int: '.$_GET['run_id']);
	}
?>