<?php
	if (isset($_GET['status']) && $_GET['status'] === 'pending')
	{
		$page_title = 'Pending Confirmations';	
		$confirmations = $db->listAll('pending-confirmation-list');	
	}
	else
	{
		$page_title = 'All Confirmations';
		$confirmations = $db->listAll('all-confirmation-list');	
	}	

?>