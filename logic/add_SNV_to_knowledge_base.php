<?php
	//////////////////////////////////////////////////////////////////
	// run message board
	//////////////////////////////////////////////////////////////////
	// find everyone with reviewer permission
	$all_reviewers = $db->listAll('users-review-permissions');
	$all_directors = $db->listAll('users-director-permissions');

	if (isset($_GET['visit_id']))
	{
		require_once('logic/shared_logic/message_board.php');
	}	

	// For hot spots this form can be used from the test setup_tests > orderable_test_hotspots.  Or if this 
	// is an NGS test it will be used from the verfiy variants page which will require run_id, visit_id, patient_id, 
	// variant_filter.  This is used to run the message board but turned off otherwise.
	if (
			isset($_GET) && 
			isset($_GET['genes']) && 
			isset($_GET['coding']) && 
			isset($_GET['amino_acid_change']) && 
			(
				(
					isset($_GET['run_id']) && 
					isset($_GET['visit_id']) && 
					isset($_GET['patient_id']) && 
					isset($_GET['variant_filter'])
				)
				||
				isset($_GET['orderable_tests_id'])
			) 
		)
	{
		$page_title = 'Add SNV '.$_GET['genes'].' '.$_GET['coding'].'('.$_GET['amino_acid_change'].')';

		///////////////////////////////////////////////////////////////////////////
		// Find out information already known about gene
		///////////////////////////////////////////////////////////////////////////

		$genesInfo = $db->listAll('chr-gene-on', $_GET['genes']);
		$mutationTypes = $db->listAll('mutation-types');

		/////////////////////////////////////////////////////////////////////////
		// Find if variant coding needs to be updated.  
		// For example:
			// c.1900_1922delAGAGAGGCGGCCACCACTGCCAT found in db is c.1900_1922del23
		/////////////////////////////////////////////////////////////////////////
		
		$view_type = 'add_SNV';
		$skip_alt_coding = False;

		// if $_GET[view_type] is set and equal to add_SNV skip alternate coding
		if (isset($_GET['view_type']) && $_GET['view_type'])
		{
			$skip_alt_coding = True;
		}
		

		if (!$skip_alt_coding)
		{
			$alternate_coding = $utils->possibleAlternateCoding($_GET['coding']);
		}

		if (!empty($alternate_coding) && !$skip_alt_coding)
		{
			// check if knowledge base table has the $alternate_coding.  
			// if found_alt_variant not empty set $view = alt_variant_found
			$search_array = array(
				'genes' 				=> 		$_GET['genes'],
				'coding' 				=> 		$alternate_coding
			);

			$knowledge_base = $db->listAll('kbt-snv-gene-coding', $search_array);

			// sometimes the protein could also be a different name.  Just 
			// search for the gene and coding only could produce more than
			// one result so the alt_variant_found view will only be used
			// when there's only one variant in the $knowledge_base result
			if (!empty($knowledge_base) && sizeof($knowledge_base) == 1)
			{
				$view_type = 'alt_variant_found';
				$ttk_vw_cols = $db->listAll('get-table-col-names', 'knowledge_base_table');
				$page_title =  $_GET['coding'].' possible alternate name '.$knowledge_base[0]['coding'];
			}
		}
	}	
	else
	{
		require_once('logic/shared_logic/problem_page.php');
	}


	//////////////////////////////////////////////////////
	// add snv form submit
	//////////////////////////////////////////////////////
	if (isset($_POST['add_SNV_to_knowledge_base_submit']))
	{
		//////////////////////////////////////////////////////
		// Double check variant not already in db
		//////////////////////////////////////////////////////
		$SNV = array(
				'genes' 				=>	$_POST['genes'],
				'coding' 				=>	$_POST['coding'],
				'amino_acid_change' 	=>	$_POST['amino_acid_change']
			);

		$snvFound = $db->listAll('kbt-snv', $SNV);

		if (empty($snvFound))
		{
			$SNV['amino_acid_change_abbrev']	= 	$_POST['amino_acid_change_abbrev'];
			$SNV['chr'] 						= 	$_POST['chr'];
			$SNV['loc']							= 	$_POST['loc'];
			$SNV['pmid']						= 	$_POST['pmid'];
			$SNV['cosmic']						= 	$_POST['cosmic'];
			$SNV['hgnc']						= 	$_POST['hgnc'];
			$SNV['mutation_type']				= 	$_POST['mutation_type'];		

			//////////////////////////////////////////////////////
			// insert into knowledge_base_table
			////////////////////////////////////////////////////
			$snv_result = $db->addOrModifyRecord('knowledge_base_table',$SNV);

			// insert failed 
			if ($snv_result[0] != 1)
			{
				$message = 'Submission failed: '.$snv_result[0];
			}

			// successfully added
			else if ($snv_result[0] == 1)
			{

				if (isset($_GET['previous_page']))
				{
					header('Location:'.REDIRECT_URL.'?page='.$_GET['previous_page'].'&'.EXTRA_GETS);
				}

				else if (
							isset($_GET['run_id']) && 
							isset($_GET['visit_id']) && 
							isset($_GET['patient_id']) && 
							isset($_GET['variant_filter'])
						)
				{
					//////////////////////////////////////////////////////
					// redirect back to report
					//////////////////////////////////////////////////////
					header('Location:'.REDIRECT_URL.'?page=verify_variants&'.EXTRA_GETS.'&variant_filter='.$_GET['variant_filter']);
				}
			}
		}
		else
		{	

			if (isset($_GET['previous_page']))
			{
				header('Location:'.REDIRECT_URL.'?page='.$_GET['previous_page'].'&'.EXTRA_GETS);
			}
			else  if (
							isset($_GET['run_id']) && 
							isset($_GET['visit_id']) && 
							isset($_GET['patient_id']) && 
							isset($_GET['variant_filter'])
						)
			{
				header('Location:'.REDIRECT_URL.'?page=verify_variants&'.EXTRA_GETS.'&variant_filter='.$_GET['variant_filter']);	
				// require_once('logic/shared_logic/problem_page.php');
			}
		}
	}


	//////////////////////////////////////////////////////
	// alt_variant_found form submit
	//////////////////////////////////////////////////////
	if (isset($_POST['alt_variant_found_submit']))
	{
		// find observed variant based on $_GET genes, coding, amino acid change, and run_id
		$search_array = array(
			'genes' 	 			=> 	$_GET['genes'],
			'coding'				=>	$_GET['coding'],
			'amino_acid_change' 	=> 	$_GET['amino_acid_change'],
			'run_id'				=> 	$_GET['run_id']
		);
		$current_variant_info = $db->listAll('observed-variant-run-id', $search_array);

		if (!empty($current_variant_info))
		{
			// update with knowledge base variant info
			$update_array = $current_variant_info[0];
			$update_array['coding'] = $knowledge_base[0]['coding']; 
			$update_array['amino_acid_change'] = $knowledge_base[0]['amino_acid_change'];

			$snv_result = $db->addOrModifyRecord('observed_variant_table', $update_array);
			// insert failed 
			if ($snv_result[0] != 1)
			{
				$message = 'Submission failed: '.$snv_result[0];
			}
			// successfully added
			else if ($snv_result[0] == 1)
			{
				//////////////////////////////////////////////////////
				// redirect back to report
				//////////////////////////////////////////////////////
				header('Location:'.REDIRECT_URL.'?page=verify_variants&'.EXTRA_GETS.'&variant_filter='.$_GET['variant_filter']);
			}
		}
		else
		{
			require_once('logic/shared_logic/problem_page.php');
		}
	}
?>