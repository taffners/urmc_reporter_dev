<?php

	if (
			!isset($_GET['control_type_id']) || 
			!isset($user_permssions) ||
			strpos($user_permssions, 'login_enhanced') == false
		)
	{
		header('Location:'.REDIRECT_URL.'?page=new_home');
	}
	$curr_control = $db->listAll('current-controls', $_GET['control_type_id']);

	$page_title = 'Add Expected Variant to '.$curr_control[0]['control_name'];

	$expected_var = array();

	if (isset($_GET['expected_control_variants_id']))
	{
		$expected_var = $db->listAll('expected-control-variant-by-id', $_GET['expected_control_variants_id']);	
		$page_title = 'Update Expected Variant for '.$curr_control[0]['control_name'];	
	}

	if (isset($_POST['add_expected_variant_submit']))
	{
		do
		{
			if (!preg_match("/^c./", $_POST['coding']))
			{
				$message = 'Please Make sure coding starts with c.';
				break;
			}
			else if (!preg_match("/^p./", $_POST['amino_acid_change']))
			{
				$message = 'Please Make sure protein starts with p.';
				break;
			}


			$add_arr = array();
			$add_arr['control_type_id'] = $_GET['control_type_id'];
			$add_arr['user_id'] = USER_ID;

			if (isset($_GET['expected_control_variants_id']))
			{
				$add_arr['expected_control_variants_id'] = $_GET['expected_control_variants_id'];
			}

			$add_arr['genes'] = $_POST['genes'];
			$add_arr['coding'] = $_POST['coding'];
			$add_arr['amino_acid_change'] = $_POST['amino_acid_change'];
			$add_arr['min_accepted_frequency'] = $_POST['min_accepted_frequency'];
			$add_arr['max_accepted_frequency'] = $_POST['max_accepted_frequency'];
			 
			$add_result = $db->addOrModifyRecord('expected_control_variants_table', $add_arr);

			header('Location:'.REDIRECT_URL.'?page=expected_variants&control_type_id='.$_GET['control_type_id']);

		} while(false);
	}

?>