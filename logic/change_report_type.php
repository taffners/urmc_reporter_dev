<?php
	require_once('logic/shared_logic/report_shared_logic.php');

	$page_title = 'Change Report Type';
	$softpath_users = $utils->ConvertAllUsersWithAPermissionToStr($db->listAll('get-all-users-with-a-permission', 'softpath'), 'and');

	if (isset($_POST['change_report_type_submit']) && (!isset($runInfoArray[0]) || !isset($runInfoArray[0]['report_type'])))
	{
		$message = 'Something went wrong';
	}

	else if (isset($_POST['change_report_type_submit']) && (!isset($_POST['report_type']) || empty($_POST['report_type'])))
	{
		$message = 'add a report type';
	}

	// Make sure report type is different than current report type
	else if (isset($_POST['change_report_type_submit']) && $_POST['report_type'] === $runInfoArray[0]['report_type'])
	{
		$message = 'Run type is already set to '. htmlspecialchars($runInfoArray[0]['report_type']).'.';
	}

	else if (isset($_POST['change_report_type_submit']) )
	{
		$updateArray = $runInfoArray[0];

		$updateArray['report_type'] = $_POST['report_type'];

		$db->addOrModifyRecord('run_info_table', $updateArray);	

		header('Location:'.REDIRECT_URL.'?page=show_edit_report&'.EXTRA_GETS);
	}
	
?>
