<?php
	
	if (!isset($_GET['training_step']))
	{

		header('Location:'.REDIRECT_URL.'?page=training&training_step=main');
	}


	$page_title = 'Training Step: '.$utils->UnderscoreCaseToHumanReadable($_GET['training_step']);
	$all_login_enhanced_users = $db->listAll('get-all-users-with-a-permission', 'login_enhanced');
	$str_login_enhanced_users = $utils->ConvertAllUsersWithAPermissionToStr($all_login_enhanced_users);


	$faqs = array(
		'General' => array(
			'how_to_change_password' => array(
				'Question' =>'How do I change password in infotrack?',
				'Answer' 	=> '​​An Infotrack Admin needs to reset it for you.  An email will be sent with a temporary password.  Then at the next login you will be asked to change your password.',
				'img'	=> 'none'
			),
			'what_to_do_if_missing_required_sample_log_book_fields' => array(
				'Question' =>'How do I login a sample that is missing a soft lab # or a MRN?',
				'Answer' 	=> '​​Press the <button type="button" data-unk-field="soft_lab_num" class="assign-unk-num btn btn-primary btn-primary-hover">
								Assign Unknown #
							</button> button next to the field in on add a sample to log book page.',
				'img'	=> 'none'
			),
			'Sample_comments' => array(
				'Question' =>'How do I add a comment to a sample when all the tests are complete?',
				'Answer' 	=> 'At the sample level you can always add a comment to a sample. Look up the sample in the log book and press the edit log book button. Then scroll down to the bottom of the page and add a comment.  By adding your comment here and not to the test itself it will be search-able in the future.  It will also show up in the log book when the sample is searched for.',
				'img'	=> 'none'
			)
		),
		'General Setup' => array(
			'General_setup_Add_an_instrument' => array(
				'Question' =>'How do I add an instrument in infotrack?',
				'Answer' 	=> '​​To add an instrument go to setup -> instruments then click on the add instrument button.',
				'img'	=> 'images/faqs/General_setup_Add_an_instrument.png'
			),
			'General_setup_Add_a_new_sample_type' => array(
				'Question' =>'How do I add a new sample type?',
				'Answer' 	=> '​​Make sure it is a necessity to add another sample type.  This is a rare event.  To add a new sample type enhanced login permissions are required.  The current people with these permissions are: '.$str_login_enhanced_users.'.  If you have in enhanced login permissions there are two ways to add a new sample type.  <ul><li>Via the sample log book add new sample type button.</li><li>Via the the menu bar Add a new sample type.</li></ul>',
				'img'	=> 'images/faqs/how_to_add_sample_type.png'
			)
			
		),
		'NGS' => array(
			'NGS_remove_variant_from_report' => array(
				'Question' =>'How to remove a variant from an NGS report?',
				'Answer' 	=> 'Everyone with NGS permissions has access to it.<br><br>
			It is in the verify variants page. (This is accessible after QC is complete)
			<br><br>Go to the include in report column and toggle the check box on and off.  There is also filter toggles above the table to show the included variants and all variants.',
				'img'	=> 'images/faqs/NGS_remove_variant_from_report.png'
			),
			'NGS_search' =>array(
				'Question' =>'Search NGS',
				'Answer' 	=> 'There are three different ways to search the NGS part of '.SITE_TITLE.'.  All search methods can be accessed via the NGS menu bar.  Search options include:<br><br><ul><li>Search observed variants
<ul>
	<li>This search will allow you to search every instance where a variant was observed.</li>
	<li>It will provide you will all variants even if they were not reported.</li>
	<li>If the variant was reported a link to the report will be provided otherwise it will be noted that the variant was not included in report.</li>
	<li>You can also use this search option to search by sample name.  This provide you a list of all variants present in the sample. Partial matching occurs during this search.</li>
</ul>


				</li><li>Search patients and their reports
<ul>
	<li>Use this search method to find all reports for a patient.  A overview list of all their reports will be provided including all variants and their VAFS.  Also included will be the VAFS for each variant in an easily comparable chart.</li>
	<li>Or you can search by variant to see all reports that have a particular variant.  This search does not include unreported variants.</li>
</ul>

				</li><li>Search Knowledge Base
<ul>
	<li>This search provides the contents of Knowledge Base which are primarily based on the variants found in Cosmic.  This search is not patient report centered, it is centered on the knowledge base.</li>
	<li>This search will provide links to genome location, references, cosmic, and transcript.</li>
	<li>As variants are encountered by the lab, interpretations, tiers, classifications, and patient reports are linked to them. A list of all reported tiers and interpretations on a variant can be accessed via this search.</li>
	<li>This search will provide no data on unreported variants</li>
</ul>
				</li></ul>',
				'img'	=> ''
			),
			'NGS_search_all_variant_interpts_per_variant' =>array(
				'Question' =>'How do I find all variant Interpretations for a particular variant?',
				'Answer' 	=> 'Go to NGS menu -> Search Knowledge Base.<br><br>Search for variant.<br><br>Click on the finger under Interpt.',
				'img'	=> 'images/faqs/NGS_search_all_variant_interpts_per_variant.png'
			),
			'NGS_mark_step_as_complete' =>array(
				'Question' =>'How do I continue on after a step is complete?',
				'Answer' 	=> 'Each step will need to be completed before the step below it will be accessible.  To complete a step press done or submit on each step.',
				'img'	=> 'none'
			),
			'NGS_delete_pending_tests' =>array(
				'Question' =>'How do I cancel pending NGS tests?',
				'Answer' 	=> 'To cancel pre NGS tests use the Cancel red x at the far right of each row in the pre ngs samples table. This will remove samples from any pools also.<br><br>To cancel in the samples with outstanding Reports section.  Open the NGS report building section of infotrack and click on stop reporting.<br><br> Both of these methods will cancel the test in the sample log book.',
				'img'	=> 'images/how_to_cancel_ngs_tests.png'
			), 
			// 'update_ngs_visit_info' =>array(
			// 	'Question' =>'How do I update NGS test sample log book info for report also?',
			// 	'Answer' 	=> 'If information for a sample that has a NGS test already added to it needs to be updated, the sample log book and the NGS visit have to be updated separately. This provides the ability to update a soft lab number only for the NGS test if it is more than 30 days old. <br><br>If you would like to update both the sample log book and the NGS report two edits will be necessary.  To start edit the sample log book by clicking on the edit log book button in the sample log book.  Do not do this step if a soft lab number expired and it just needs to be updated for the NGS test.  However, do leave a comment in the sample log book of the new soft lab number.<br><br>There are multiple ways to update the NGS report.<br><br>The first method is to use the Edit/Update Print/Cancel Test button from the sample log book and then the edit ordered test button. <br><img src="images/Edit_NGS_visit_method_1.png" width="400px" alt="Edit_NGS_visit_method_1"><br><br>The other methods depend on what stage of of completion the test is in.  If the test is pre ngs use the green visit button in the pre ngs samples table.<br><img src="images/Edit_NGS_visit_method_2.png" width="600px" alt="Edit_NGS_visit_method_2"><br><br>If sample is in the samples with outstanding reports list.  Open the report generator and click on add visit<br><img src="images/Edit_NGS_visit_method_3.png" width="600px" alt="Edit_NGS_visit_method_3"><br><br>If the report is in the completed reports section a director will have to send this report to revise and and then use the add visit above.  Or and admin can edit it.',
			// 	'img'	=> ''
			// ), 
 

		),
		'Genetics' => array(
			'genetics_consent_date' => array(
				'Question' =>'What date should I put for a genetics Consent Date?',
				'Answer' 	=> 'For genetics assays there is a field in Infotrack that is for "Consent Date" <br><br>
 <br><br>
If no consent has been received this should be left blank.  When we receive the consent you should go back and fill this field in with the date we received the consent.
If consent comes with the sample fill it in as the day you logged in the sample.
<br><br> 
In the future we intend to use this date so Infotrack can determine the turn around time for genetics assays.  The turn around time is based upon the date the consent is received. ',
				'img'	=> 'none'
			)

		),
		
		'Permissions' => array(

			'all_permissions' => array(
				'Question' =>'Overview of all permissions',
				'Answer' 	=> '',
				'img'	=> 'none',
				'sop_link' => '<iframe src="downloads/MD_Infotrack_SOP.pdf#page=5" style="width:90%; height:700px;" frameborder="0"></iframe>'
			),
			'permission_audits' => array(
				'Question' =>'Describe audit level permissions?',
				'Answer' 	=> 'Audit level permissions will provides access to performance audits and allows users the Allows users the ability to edit turnaround times of tests.
				',
				'img'	=> 'none',
				'sop_link' => '<iframe src="downloads/MD_Infotrack_SOP.pdf#page=11" style="width:90%; height:700px;" frameborder="0"></iframe>'
			),
			'permission_ngs' => array(
				'Question' =>'Describe ngs level permissions?',
				'Answer' 	=> 'Provides access to NGS report building and the progress of all NGS samples. NGS drop down menu is available. Changes homepage to NGS workflow samples',
				'img'	=> 'none',
				'sop_link' => '<iframe src="downloads/MD_Infotrack_SOP.pdf#page=34" style="width:90%; height:700px;" frameborder="0"></iframe>'
			),
			'permission_softpath' => array(
				'Question' =>'Describe softpath level permissions?',
				'Answer' 	=> 'Users are automatically emailed when NGS tests are requested and completed.  Softpath users are responsible for scanning printed NGS reports without header, attaching it to the patient medical record, and updating the scanned transfer to patient chart list.  
					<br><br>
					To access the scanned transfer to patient chart list go to NGS -> Scanned transfer to patient chart.  This page will show all of the reports not marked as scanned.  You can toggle between unscanned and scanned reports with the two buttons at the top of the page.  Once you scan the report please click on the complete radio button.  (without softpath permissions the radio button will not appear). Samples will only appear on this chart if it is marked as reportable.  This can be changed while making the report.  If the sample is changed to validation softpath users will not receive and email about the report and it will not be placed in the new scanned list.  Reports are automatically set to validation if the test is not live yet.  I set OFA go live date to July 6th.',
				'img'	=> 'none',
				'sop_link' => '<iframe src="downloads/Scanning_Molecular_NGS_reports_into_SOFTPATH.pdf#page=2" style="width:90%; height:700px;" frameborder="0"></iframe>'
			),
			'permission_manager' => array(
				'Question' =>'Describe manager level permissions?',
				'Answer' 	=> 'Manager level permissions 
				<ul>
					<li>Managers are sent automatic emails to notify them of expiring service contracts</li>
					<li>As of September 22, 2020 Managers gained the ability to edit some fields locked to every other user.  For example a manager can delete a comment on the sample log book.</li>
					<li>As of October 27th, 2020 Mangers gained the ability to add new single analyte tests</li>
					<li>As of November 3rd, 2020 Mangers gained the ability to add a reflex test tracker</li>
				</ul>
				',
				'img'	=> 'none',
				'sop_link' => '<iframe src="downloads/MD_Infotrack_SOP.pdf#page=5" style="width:90%; height:700px;" frameborder="0"></iframe>'
			)

		),
	);

?>

	
		
		