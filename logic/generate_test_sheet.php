<?php
	if (isset($_GET['ordered_test_id']) && isset($_GET['sample_log_book_id']))
	{
// $starttime = microtime(true);		
		// $sampleLogBookArray = $db->listAll('all-samples-in-log-book-by-sample-log-book-id',$_GET['sample_log_book_id']);
		$sampleLogBookArray = $db->listAll('non-stop-samples-in-log-book-by-sample-log-book-id',$_GET['sample_log_book_id']); // speed up 0.31118988990784
// $endtime = microtime(true);		
// $duration = $endtime - $starttime;
// var_dump($duration); 
// var_dump($sampleLogBookArray);
		require_once('logic/shared_logic/backwards_compatablity_dob_sex.php');

		$extractors = $db->listAll('instrument-groups-description', 'extractor');

// $starttime = microtime(true);
		if (isset($_GET['visit_id']) && !empty($_GET['visit_id']) && $_GET['visit_id'] !== 0)
		{
			$orderedTestArray = $db->listAll('ordered-test-info-ngs', $_GET['ordered_test_id']);
		}
		else
		{
			$orderedTestArray = $db->listAll('ordered-test-info', $_GET['ordered_test_id']); // speed up 0.11537098884583
		}
// $endtime = microtime(true);		
// $duration = $endtime - $starttime;
// var_dump($duration);	
// var_dump($orderedTestArray);
		
		$reflexArray = $db->listAll('all-sample-reflexes', $_GET['sample_log_book_id']); //0.00065398216247559

		
 //0.31220507621765

		//////////////////////////////////////////////////////////////////////
		// Find the address that is appropriate.  When we moved to Bailey road this 
		// became important.  Since all worksheets are automatically generated.  
		// After moving to bailey road I need to pull up the correct address for when.
		// Test is ordered (start_date)
		//////////////////////////////////////////////////////////////////////
		$report_date = isset($orderedTestArray[0]['start_date']) ? $orderedTestArray[0]['start_date'] : date('Y-m-d');
		$address_result = $db->listAll('lab-address-now', $report_date);

		if (isset($address_result[0]))
		{
			$address_array = array();
			foreach($address_result[0] as $col_name => $val)
			{
				if (substr($col_name, 0, 4) === 'line' && !empty($val))
				{
					array_push($address_array, $val);
				}
			}
		}

		// generate report
		$header_footer_array = array(
			'footer_status' 	=> 	False,
			'header_status'	=> 	True,
			'address_array'	=>	$address_array
		);
		$pdf = new PDF('P', 'mm', 'a4', $header_footer_array);
		
		$PDF_Data_array = array(
			'sampleLogBookArray' 	=> 	$sampleLogBookArray[0],
			'orderedTestArray'		=>	$orderedTestArray[0],
			'extractors'			=> 	$extractors,
			'reflexArray'			=> 	$reflexArray	
		);

		$pdf->addTestSheetData($PDF_Data_array);

		$pdf->AliasNbPages();

		$pdf->AddPage();

		$pdf->SampleTestSheetInfo();

		$pdf->AddTestInfo();
	
		// Check if history exists
		if (isset($orderedTestArray[0]['previous_positive_required']) && !empty($orderedTestArray[0]['previous_positive_required']) && $orderedTestArray[0]['previous_positive_required'] === 'yes')
		{
			// get history all history info and display it.  On April 27th 2021 I was developing a way to add 
			// multiple histories to a sample.  Because of this I pulled history out of the orderedTestArray and
			// added another query.  Hopefully this will also increase the speed of orderedTestArray.  There
			// are few tests that history is recorded for.

			$histories = $db->listAll('test-histories', $orderedTestArray[0]['ordered_test_id']);

			$pdf->testOrderedHistory($histories);
		}


		// Check if consent info exists
		if (isset($orderedTestArray[0]['consent_info']) && !empty($orderedTestArray[0]['consent_info']))
		{
			$pdf->testOrderedConsent();
		}

		// add Extraction info
		if 	(	
				$orderedTestArray[0]['extraction_info'] === 'yes' || 
				$orderedTestArray[0]['quantification_info'] === 'yes'
			)				
		{
			$quantifiers = $db->listAll('instrument-groups-description-quantifiers');
			
			$pdf->testOrderedDNAExtraction($quantifiers);
		}

		// if not DNA extraction and a DNA extraction was completed on this sample add Extraction info
		else
		{
			// see if extraction log for ordered_test_id exists
			$extraction_info = $db->listAll('extraction-log', $_GET['sample_log_book_id']);

			if (!empty($extraction_info))
			{
				$pdf->testOrderedPreviousExtractionInfo($extraction_info);
			}
		}

		// add idylla instruments, cartridge ID, lot ID, DNA vol, sample loaded, conc 
		if 	(
				isset($orderedTestArray[0]['idylla_instruments']) && 
				$orderedTestArray[0]['idylla_instruments'] === 'yes')
		{
			$idylla_instruments = $db->listAll('instrument-groups-description', 'Idylla instrument');

			$pdf->addIdyllaInstrumentsWorkSheetSection($idylla_instruments);
		}

		// Add hotspots, and quality section
		if 	(
				isset($orderedTestArray[0]['hotspot_variants']) && 
				isset($orderedTestArray[0]['orderable_tests_id']) &&
				$orderedTestArray[0]['hotspot_variants'] === 'yes')
		{
			$idylla_variants = $db->listAll('all-idylla-variants', $orderedTestArray[0]['orderable_tests_id']);

			$pdf->addIdyllaHotspotsWorkSheetSection($idylla_variants);
		}


		// if test is an idylla add variants. 
		// if (isset($orderedTestArray[0]['assay_name']) && isset($orderedTestArray[0]['ordered_test_id']) && $orderedTestArray[0]['assay_name'] === 'Idylla')
		// {
		// 	// get all variants for Idylla
		// 	$idylla_variants = $db->listAll('all-idylla-variants', $orderedTestArray[0]['orderable_tests_id']);

		// 	$idylla_instruments = $db->listAll('instrument-groups-description', 'Idylla instrument');

		// 		
		// }

		$pdf->testOrderedBy();

		$pdf->testOrderedComments();

		// show report in screen
		
		$pdf->Output('I');
	
	}
	else
	{
		header('Location:'.REDIRECT_URL.'?page=home');
	}

?>