<?php
	

	if (isset($_GET['instrument_id']))
	{
		$instrumentArray = $db->listAll('instrument-by-id', $_GET['instrument_id']);
		$serviceArray = $db->listAll('all-instrument-service-contracts', $_GET['instrument_id']);
		$page_title ='Service Contracts for '.$utils->instrumentInputName($instrumentArray[0]);	
	}
	else
	{
		$message = 'Something is wrong and instrument was not selected. Do not proceed.';
		$instrumentArray = array();
		$page_title ='Service Contracts';
	}	

?>