<?php

	if (isset($_GET['single_analyte_pool_id']) && !empty($_GET['single_analyte_pool_id']))
	{
		$samples_in_single_analyte_pool = $db->listAll('samples-in-single-analyte-pool', $_GET['single_analyte_pool_id']);	
		
		if (empty($samples_in_single_analyte_pool))
		{
			header('Location:'.REDIRECT_URL.'?page=new_home');
		}

		$page_title = $samples_in_single_analyte_pool[0]['test_name'].' Run # '.$samples_in_single_analyte_pool[0]['run_number'].'-'.date('y', strtotime($samples_in_single_analyte_pool[0]['start_date'])).' (Tech: '.$samples_in_single_analyte_pool[0]['pool_tech'].')';

		$num_pending_tests = $db->listAll('num-pending-tests-in-single-analyte-pool', $_GET['single_analyte_pool_id']);

		$all_comments = $db->listAll('comments-with-comment-type-variable', array('comment_type' => 'single_analyte_pool_table', 'comment_ref' => $_GET['single_analyte_pool_id']));
	}
	else
	{
		header('Location:'.REDIRECT_URL.'?page=new_home');
	}

	if  (
			isset($_POST['cancel_single_analyte_pool_submit']) && 
			(
				!isset($_POST['comments']) || 
				empty($_POST['comments'])
			)
		)
	{
		$message = 'Add a reason for canceling';
	}


	else if (isset($_POST['cancel_single_analyte_pool_submit']) && isset($_POST['comments']) && !empty($_POST['comments']))
	{

		do 
		{

			//////////////////////////////////////////
			// Change status of all samples in pool to pending
			//////////////////////////////////////////
			if (!empty($samples_in_single_analyte_pool))
			{
				$allTestsStatusUpdated = True;
				foreach ($samples_in_single_analyte_pool as $key => $test)
				{
					if (isset($test['ordered_test_id']) && is_numeric($test['ordered_test_id']))
					{
						$updateRecordResult = $db->updateRecord('ordered_test_table', 'ordered_test_id', $test['ordered_test_id'], 'test_status', 'pending');

						if (!$updateRecordResult[0])
						{
							$allTestsStatusUpdated = False;
							break;
						}
					}
					else
					{
						$allTestsStatusUpdated = False;
						break;
					}
				}
			}
			else
			{
				$message = 'Something went wrong with changing the test status to back to pending.  There are not tests in this pool.';
				break;
			}

			if (!$allTestsStatusUpdated)
			{
				$message = 'Something went wrong with changing the test status to back to pending.';
				break;
			}

			//////////////////////////////////////////
			// set single analyte pool to status canceled
			//////////////////////////////////////////
			$db->updateRecord('single_analyte_pool_table', 'single_analyte_pool_id', $_GET['single_analyte_pool_id'], 'status', 'canceled');

			//////////////////////////////////////////
			// Add comment for reason of canceling
			//////////////////////////////////////////
			if (isset($_POST['comments']) && !$utils->textareaEmpty($_POST['comments']))
			{
				$commentInsertArray = array();
				$commentInsertArray['user_id'] = USER_ID;
				$commentInsertArray['comment'] =  'Reason for Canceling: '.$_POST['comments'];

				//////////////////////////////////////////
				// Add comment if ordered_test_id single analyte
				//////////////////////////////////////////
				$commentInsertArray['comment_ref'] =  $_GET['single_analyte_pool_id'];
				$commentInsertArray['comment_type'] =  'single_analyte_pool_table';	

				$comment_result = $db->addOrModifyRecord('comment_table',$commentInsertArray);
			}
		} while(False);

		header('Location:'.REDIRECT_URL.'?page=new_home');
	}
?>