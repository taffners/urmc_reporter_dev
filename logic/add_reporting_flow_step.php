<?php

	if (isset($user_permssions) && strpos($user_permssions, 'admin') === false)
	{ 
		header('Location:'.REDIRECT_URL.'?page=new_home');	
	}
	$page_title = 'Add a '.SITE_TITLE_ABBR.' reporting flow step';


	$all_reporting_steps = $db->listAll('all-reporting-flow-steps');

	if (isset($_GET['reporting_flow_steps_id']))
	{
		$curr_reporting_flow_steps = $db->listAll('reporting-flow-steps-by-id', $_GET['reporting_flow_steps_id']);
	}
	else
	{
		$curr_reporting_flow_steps = array();
	}

	if (isset($_POST['add_reporting_flow_step_submit']))
	{
		$add_array = array();
		$add_array['user_id'] = USER_ID;
		$add_array['description'] = $_POST['description'];
		$add_array['no_variant_status'] = $_POST['no_variant_status'];
		$add_array['step_name'] = $_POST['step_name'];
		$add_array['flow_type'] = $_POST['flow_type'];
		$add_array['step_status'] = $_POST['step_status'];


		if (isset($_GET['reporting_flow_steps_id']))
		{
			$add_array['reporting_flow_steps_id']  = $_GET['reporting_flow_steps_id'];
		}
		
		$add_result = $db->addOrModifyRecord('reporting_flow_steps_table', $add_array);	

		header('Location:'.REDIRECT_URL.'?page=add_reporting_flow_step');	
	}
?>