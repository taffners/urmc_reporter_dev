<?php
	$page_title = 'Add a Reflex Tracker to';

	$allReflexsAvailable = $db->listAll('all-reflex-available');

	if (isset($_GET['sample_log_book_id']))
	{
		$searchSampleLogBookArray = array();
		$searchSampleLogBookArray['sample_log_book_id'] = $_GET['sample_log_book_id'];

		if (isset($_GET['visit_id']) && isset($_GET['patient_id']) && !empty($_GET['patient_id']) && !empty($_GET['visit_id']))
		{
			$searchSampleLogBookArray['visit_id'] = $_GET['visit_id'];
			$searchSampleLogBookArray['patient_id'] = $_GET['patient_id'];
		}


		$sampleLogBookArray = $db->listAll('sample-log-book-by-id', $searchSampleLogBookArray);

		$page_title = 'Add a Reflex Tracker to '.$sampleLogBookArray[0]['mol_num'];

	}

	else
	{
		// if sample_log_book_id is not supplied then this page will not work.  Therefore redirect user back to home page since something went wrong
		header('Location:'.REDIRECT_URL.'?page=home');
	}

	if (isset($_POST['add_reflex_tracker_submit']) && isset($_POST['reflex_id']) && !empty($_POST['reflex_id']) && isset($_GET['sample_log_book_id']))
	{
		////////////////////////////////
		// Data will need to be added to two tables
			// ordered_reflex_test_table
				// Add a reflex
			// status_linked_reflexes_xref_table
				// Add a pending status link for all reflex links
		///////////////////////////////

		// make sure reflex has linked reflexes otherwise something is wrong and do not proceed

		do 
		{
			$all_reflex_links = $db->listAll('reflex-links', $_POST['reflex_id']);

			if (empty($all_reflex_links))
			{
				$message = 'Message something is wrong with the selected reflex';
				break;
			}

			// Add ordered_reflex_test_table Data
			$add_reflex_array = array();
			$add_reflex_array['user_id'] = USER_ID;
			$add_reflex_array['reflex_id'] = $_POST['reflex_id'];
			$add_reflex_array['sample_log_book_id'] = $_GET['sample_log_book_id'];
			$add_reflex_array['reflex_status'] = 'pending';

			$reflex_result = $db->addOrModifyRecord('ordered_reflex_test_table',$add_reflex_array);

			if($reflex_result[0])
			{
				foreach ($all_reflex_links as $key => $link)
				{
					$add_link = array();
					$add_link['user_id'] = USER_ID;
					$add_link['ordered_reflex_test_id'] = $reflex_result[1];
					$add_link['reflex_test_link_id'] = $link['reflex_test_link_id'];
					$add_link['status'] = 'pending';
					$add_link_result = $db->addOrModifyRecord('status_linked_reflexes_xref_table',$add_link);
				
				}
			}


			header('Location:'.REDIRECT_URL.'?page=all_sample_tests&sample_log_book_id='.$_GET['sample_log_book_id']);
		} while(false);

		


	}


?>
