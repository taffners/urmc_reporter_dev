<?php
	

	if (isset($_GET['run_id'])) 
	{
		$page_title = 'Update Report';
		if (!isset($completed_steps))
		{

			$completed_steps = $db->listAll('all-complete-steps', $_GET['visit_id']);
		}

		$qc_run_step_status = $stepTracker->StepStatus($completed_steps, 'qc');
		$qc_variant_step_status = $stepTracker->StepStatus($completed_steps, 'qc_variant');	
	}
	else
	{
		$page_title = 'Add a Patient or Select Patient already added.';
	}
	//////////////////////////////////////////////////////
	// Fill in info already added for updating
	// To add a patient a visit_id is required therefore redirect to homepage if visit_id unknown
	// Make all arrays used to re-populate form if updating
	//////////////////////////////////////////////////////
	if (isset($_GET) && isset($_GET['visit_id']))
	{
		$completed_steps = $db->listAll('all-complete-steps', $_GET['visit_id']);
	}
	else
	{
		header('Location:'.REDIRECT_URL.'?page=home');
	}

	//////////////////////////////////////////////////////
	// submit form
	//////////////////////////////////////////////////////
	if (isset($_POST['add_patient_submit']))
	{
		
		//////////////////////////////////////////////////////
		// Find if patient already added to db
		//////////////////////////////////////////////////////
		$searchArray = array();
		$searchArray['medical_record_num'] = $_POST['medical_record_num'];
		$searchArray['dob'] = $dfs->ChangeDateFormatSQL($_POST['dob']);		

		// check if patient already added
		$patient_found_array = $db->listAll('find-patient', $searchArray);

		//////////////////////////////////////////////////////
		// since a patient has not been found based on MRN and DOB add a new patient
		//////////////////////////////////////////////////////
		if (empty($patient_found_array))
		{
			//////////////////////////////////////////////////////
			// Get all of the info entered into form 
			//////////////////////////////////////////////////////
			$patientAddArray = array();
			$patientAddArray['last_name'] = $_POST['last_name'];
			$patientAddArray['middle_name'] = $_POST['middle_name'];
			$patientAddArray['first_name'] = $_POST['first_name'];
			$patientAddArray['medical_record_num'] = $_POST['medical_record_num'];
			$patientAddArray['user_id'] = USER_ID;
			$patientAddArray['sex'] = $_POST['sex'];
			
			// change date format for inserting
			$patientAddArray['dob'] = $dfs->ChangeDateFormatSQL($_POST['dob']);

			// add patient_id if patient already added
			if ($_POST['patient_id'] !== '')
			{
				$patientAddArray['patient_id'] = $_POST['patient_id'];
			}

			//////////////////////////////////////////////////////
			// insert into Patient_table
			//////////////////////////////////////////////////////
			$patient_result = $db->addOrModifyRecord('patient_table',$patientAddArray);

			// report to user failed add
			if ($patient_result[0] != 1)
			{
				$message = 'Submission failed: '.$patient_result[0];
			}
			
			// successfully added
			else if ($patient_result[0] == 1)
			{
				//////////////////////////////////////////////////////
				// Update visit table to include patient id
				//////////////////////////////////////////////////////	
				$patient_id = $patient_result[1];	
				
				$updateVisitTableArray = $visitArray[0];
				$updateVisitTableArray['patient_id'] = $patient_id;	
				$visit_table_result = $db->addOrModifyRecord('visit_table',$updateVisitTableArray);

				//////////////////////////////////////////////////////
				// insert into pre_step_visit table
				//////////////////////////////////////////////////////	
				$url = $preStepTracker->UpdatePreStepInDb($page, $completed_steps, $visitArray[0]['visit_id'], USER_ID, RUN_ID, 'add_patient', 'passed'); 
				
				//////////////////////////////////////////////////////
				// redirect to homepage
				//////////////////////////////////////////////////////
				header('Location:'.REDIRECT_URL.'?page=home');
			}	
		}
		//////////////////////////////////////////////////////
		// redirect since this patient has already been added based on MRN and DOB
		//////////////////////////////////////////////////////
		else
		{
			header('Location:'.REDIRECT_URL.'?page=patient_found&patient_id='.$patient_found_array[0]['patient_id'].'&visit_id='.$_POST['visit_id'].'&pre_step=1');		
		}
	}


?>