<?php	
	$page_title = 'download validation docs';

	if (isset($_GET['instrument_validation_id']))
	{
		//////////////////////////////////////////////////////
		// Find validation folder and download all
		//////////////////////////////////////////////////////
		$instrument_val_folder = SERVER_SAVE_LOC.'validation_documents/'.ROOT_URL.'/'.$_GET['instrument_validation_id'];
		
		$instrument_array = $db->listAll('get-instrument-info-by-instrument-validation', $_GET['instrument_validation_id']);

		if (!empty($instrument_array))
		{
			//////////////////////////////////////////////////////////////////
			// Zip folder and download 
			//////////////////////////////////////////////////////////////////
			$download_zip_folder = SERVER_SAVE_LOC.'validation_documents/'.ROOT_URL.'/zip_downloads/';
			$zip_file = $download_zip_folder.'/Validation_'.$instrument_array[0]['manufacturer'].'_'.$instrument_array[0]['model'].'_yellow_tag_'.$instrument_array[0]['yellow_tag_num'].'_'.$instrument_array[0]['validation_date'].'.zip';

			// zip all existing files uploaded for this validation date
			$utils->zipFolderDownload($instrument_val_folder, $zip_file);

			// delete zip file after download
			unlink($zip_file);
		}
		
	}

	
?>