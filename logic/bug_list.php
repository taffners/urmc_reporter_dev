<?php
	$page_title = 'Bugs, Maintenance, and Downtime List Since 2020-11-13';

	if (isset($_GET['status']) && $_GET['status'] === 'pending')
	{
		$page_title = 'All pending Bugs and Maintenance list since 2020-11-13';
		$all_bugs = $qc_db->listAll('qc_db-all-bugs-pending', SITE_TITLE);
	}
	else
	{
		$all_bugs = $qc_db->listAll('qc_db-all-bugs', SITE_TITLE);
	}

?>