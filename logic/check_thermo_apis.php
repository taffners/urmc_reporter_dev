<?php
	$page_title = 'Check if Thermofisher APIs still work as expected';


	$Thermo_Check_APIs = new ThermoCheckAPIs();

	$Thermo_Check_APIs->run();

	$errs = $Thermo_Check_APIs->getErrors();
	$IR_command = $Thermo_Check_APIs->getIRCommandlineStatement();

?>