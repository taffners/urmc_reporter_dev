<?php
	$page_title = 'In Progress Workbook';
	
	$inProgressTests = $db->listAll('in-progress-tests');

	if (isset($_GET['orderable_tests_id']))
	{
		$inProgressArray = $db->listAll('ordered-tests-in-progress', $_GET['orderable_tests_id']);
	}
	else
	{
		$inProgressArray = $db->listAll('ordered-tests-in-progress');	
	}
	

	if (isset($_POST['filter-pending-tests-submit']) && !empty($_POST['orderable_tests_id']))
	{

		if ($_POST['orderable_tests_id'] === 'all')
		{
			header('Location:'.REDIRECT_URL.'?page=in_progress_work_book&nav_status=off');
		}
		else
		{
			header('Location:'.REDIRECT_URL.'?page=in_progress_work_book&nav_status=off&orderable_tests_id='.$_POST['orderable_tests_id']);	
		}
	}

?>