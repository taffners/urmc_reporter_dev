<?php

	$page_title = 'Add or update a module';

	$all_possible_diagrams = $utils->getAllFilesInDir('about/diagrams');

	if (isset($_GET['module_id']))
	{
		$module_array = $qc_db->listAll('qc_db-module-by-id', $_GET['module_id']);
	}
	else
	{
		$module_array = array();
	}

	if (isset($_POST['add_module_submit']))
	{
		$add_array = array();


		if (isset($_GET['module_id']))
		{
			$add_array['module_id'] = $_GET['module_id'];
		}		

		$add_array['module_name'] = $_POST['module_name'];
		$add_array['module_diagram'] = $_POST['module_diagram'];
		$add_array['module_description'] = $_POST['module_description'];

		$page_result = $qc_db->addOrModifyRecord('module_table',$add_array);

		header('Location:'.REDIRECT_URL.'?page=all_modules');

	}



?>