
<?php
     

     if (isset($_GET['knowledge_id']))
     {

     	$knowledge_base = $db->listAll('knowledge-by-id', $_GET['knowledge_id']);	

   		$variant_name = $knowledge_base[0]['genes'].' '.$knowledge_base[0]['coding'].' ('.$knowledge_base[0]['amino_acid_change'].')';

   		$page_title = 'Variant Tiers for: <br>'.$variant_name;

          $tiers = $db->listAll('tier-by-vt-xref', $_GET['vt_xref_id']); 
     }

     else
     {
     	$message = 'Search did not proceed';
     	$page_title = 'Variant Tiers';	
     }
     
     if (isset($_POST['submit']))
     {
          // Delete Record
          $db->deleteRecordById('variant_tier_xref', 'vt_xref_id', $_POST['vt_xref_id']);

          // Redirect to all tier info for this variant in the knowledge base
          header('Location:'.REDIRECT_URL.'?page=update_tiers_kb&knowledge_id='.$_GET['knowledge_id']);
     }
?>