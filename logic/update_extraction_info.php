<?php
	if (!isset($_GET['sample_log_book_id']) || !isset($_GET['ordered_test_id']) || !isset($_GET['extraction_log_id']))
	{
		header('Location:'.REDIRECT_URL.'?page=new_home');
	}

	$sampleLogBookArray = $db->listAll('all-samples-in-log-book-by-sample-log-book-id',$_GET['sample_log_book_id']);

	$page_title = 'Update extraction Info on '.$sampleLogBookArray[0]['first_name'].' '.$sampleLogBookArray[0]['last_name'].' '.$sampleLogBookArray[0]['medical_record_num'].' '.$sampleLogBookArray[0]['soft_lab_num'];

	$extraction_array = $db->listAll('extraction-log-by-id', $_GET['extraction_log_id']);

	$orderedTestArray = $db->listAll('ordered-test-info', $_GET['ordered_test_id']);

	$testInfo = $db->listAll('orderable-tests', $orderedTestArray[0]['orderable_tests_id']);

	if (isset($_POST['update_extraction_info_submit']) && !empty($_POST['comments']))
	{
		$add_extraction_array = array(
				'extraction_log_id' =>	$_GET['extraction_log_id'],
				'ordered_test_id'	=> 	$_GET['ordered_test_id'],
				'user_id'			=>  	USER_ID,
				'stock_conc'		=>	$_POST['stock_conc'],
				'dilution_exist'	=>	$_POST['dilution_exist'],
				'dilutions_conc'	=>	$_POST['dilution_conc'],
				'times_dilution'	=> 	$_POST['times_dilution'],
				'purity'			=> 	$_POST['purity'],
				'dilution_type'		=> 	isset($_POST['dilution_type']) ? $_POST['dilution_type']: '',
				'vol_dna'			=> 	isset($_POST['vol_dna']) ? $_POST['vol_dna']: '',
				'vol_eb'			=> 	isset($_POST['vol_eb']) ? $_POST['vol_eb']: '',
				'dilution_final_vol'=> 	isset($_POST['dilution_final_vol']) ? $_POST['dilution_final_vol']: '',
				'elution_volume'	=> 	isset($_POST['elution_volume']) ? $_POST['elution_volume']: '',
				'target_conc'		=>	isset($_POST['target_conc']) ? $_POST['target_conc']: '',
				'dna_conc_control_quant'=>	isset($_POST['dna_conc_control_quant']) ? $_POST['dna_conc_control_quant']: '',
				'quantification_method'	=>	isset($_POST['quantification_method']) ? $_POST['quantification_method']: ''
			);

			$add_extraction_result = $db->addOrModifyRecord('extraction_log_table',$add_extraction_array);
			
			$commentInsertArray = array();
			$commentInsertArray['user_id'] = USER_ID;
			$commentInsertArray['comment_ref'] =  $_GET['ordered_test_id'];
			$commentInsertArray['comment_type'] =  'ordered_test_table';
			$commentInsertArray['comment'] =  $_POST['comments'];

			$comment_result = $db->addOrModifyRecord('comment_table',$commentInsertArray);
		
			header('Location:'.REDIRECT_URL.'?page=all_sample_tests&sample_log_book_id='.$_GET['sample_log_book_id']);
	}
	else if (isset($_POST['update_extraction_info_submit']) && empty($_POST['comments']))
	{
		$message = 'Please provide a reason why you are updating this extraction';
	}



//  extraction_method      | varchar(10)   | YES  |     | NULL              |                             |
// | stock_conc             | float         | YES  |     | NULL              |                             |
// | dilution_exist         | varchar(3)    | YES  |     | NULL              |                             |
// | dilutions_conc         | float         | YES  |     | NULL              |                             |
// | time_stamp             | timestamp     | NO   |     | CURRENT_TIMESTAMP | on update CURRENT_TIMESTAMP |
// | times_dilution         | int(11)       | YES  |     | NULL              |                             |
// | purity                 | float         | YES  |     | NULL              |                             |
// | dilution_type          | varchar(10)   | YES  |     | NULL              |                             |
// | vol_dna                | decimal(10,0) | YES  |     | NULL              |                             |
// | vol_eb                 | decimal(10,0) | YES  |     | NULL              |                             |
// | dilution_final_vol     | decimal(10,0) | YES  |     | NULL              |                             |
// | elution_volume         | decimal(10,0) | YES  |     | NULL              |                             |
// | target_conc            | decimal(10,0) | YES  |     | NULL              |                             |
// | dna_conc_control_quant | decimal(10,0) | YES  |     | NULL              |                             |
// | quantification_method
?>