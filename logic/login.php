<?php
	$page_title = 'Welcome to '.htmlspecialchars(SITE_TITLE);

	if (empty($_SERVER['HTTPS']) || $_SERVER['HTTPS'] === "off") 
	{
	    $location = 'https://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
	    header('HTTP/1.1 301 Moved Permanently');
	    header('Location: ' . $location);
	    exit;
	}

	// To prevent brunt force discovery of logins suspend all logins if more than 10 failed logins occur in 
	// 30 mins

	////////////////////////////////////////////////
	// Prevent brute force login
	////////////////////////////////////////////////

	// find number of failed login's in the last 30 mins
	$num_failed_last_30_mins = $db->listAll('failed-logins-last-30-mins');
	$brute_force_occuring = False;
	if 	(
			isset($num_failed_last_30_mins[0]['login_count']) && 
			is_numeric($num_failed_last_30_mins[0]['login_count']) &&
			intval($num_failed_last_30_mins[0]['login_count']) >= 10
		)
	{
		$brute_force_occuring = True;
		$email_utils->emailAdmins('Brute Force logins is occurring on '.SITE_TITLE.':'.ROOT_URL.'.  In the last 30 mins '.$num_failed_last_30_mins[0]['login_count'].' failed logins have occurred.', 'Brute Force Occurring.');
	}
	else
	{

		if (isset($_POST['login_submit']) && (empty($_POST['email_address']) || empty($_POST['password'])))
		{
			$message = 'Please enter email address and password';
		}
		// Make sure a valid email address
		else if (	
					isset($_POST['login_submit']) && 
					!filter_var($_POST['email_address'], FILTER_VALIDATE_EMAIL) 
				)
		{
			$message = 'Enter an email address';
		}

		// Make sure either my gmail account or the email account contains urmc.rochester.edu
		else if (	
					isset($_POST['login_submit']) &&  				
					$_POST['email_address']	!== 'taffners@gmail.com' &&
					strpos(strtolower($_POST['email_address']), '@urmc.rochester.edu') === false
				)
		{
			$message = 'Enter an email address';
		}

		/////////////////////////////////////////////////////////////////
		// Make sure password is at least 7 chars long and no more than 60 chars
		////////////////////////////////////////////////////////////////
		else if (isset($_POST['login_submit']) && strlen($_POST['password']) < 7)
		{
			$message = 'Password too short';
		}

		else if (isset($_POST['login_submit']) && strlen($_POST['password']) > 60)
		{
			$message = 'Password too long';
		}

		else if(isset($_POST['login_submit']))
		{	

			$log_in_array = array();
			$log_in_array['email_address'] = strip_tags($_POST['email_address']);
			$log_in_array['password'] = $_POST['password'];

			$logged_in = $db->login($log_in_array);

			if(!$logged_in)
			{
				$message = 'login failed.';
			}

			else
			{						
				/////////////////////////////////////////////////////////////////
				// Everyday some checks should be performed to ensure everything is set up properly still.
				// Since I'm unsure how to set up a cronjob to do this I will run this when the first user logins 
				// in for the day.
				/////////////////////////////////////////////////////////////////
				$num_logins_today = $db->listAll('num-logins-today');

				if (!empty($num_logins_today) && $num_logins_today[0]['num_logins_today'] == 1)			
				{
					// Send an email to remind admins that the workDays holiday's needs to be updated
					if (strtotime(date("Y-m-d")) > strtotime('2022-09-01'))
					{
						$email_utils->emailAdmins('class.workDays needs to be updated.  There is a list of Holidays which will be out of date soon at the end of 2022.  Please add Holidays for the next few years.  DO NOT REMOVE ANY HOLIDAYS FROM THE ARRAY IN class.workDays.  UPDATE THE strtotime date in the logic/login.php to be set to a date close to the end of your new list for another reminder like this one.  Refer to: '.__FILE__.' Line #: '.__LINE__.' to update this reminder.', 'Software Update Required!!');

					}


					$Thermo_Check_APIs = new ThermoCheckAPIs();

					$Thermo_Check_APIs->run(False);

					$errs = $Thermo_Check_APIs->getErrors();	
					
					if (empty($errs))
					{
						$email_utils->emailAdminsDailyUpdate('All tests Passed
							', 'Passed');
					}
					else
					{
						$email_utils->emailAdminsDailyUpdate('An error occurred.  Go to the admin page to run tests.
							', 'error found');
					}

					
				}

				// redirect to homepage
				header('Location:'.REDIRECT_URL.'?page=new_home');
			}
		}
	}
	

?>
