<?php
	$page_title = 'Download Report';
	require_once('logic/shared_logic/report_shared_logic.php');
	require_once('logic/shared_logic/message_board.php');

	// find everyone with reviewer permission
	$all_reviewers = $db->listAll('users-review-permissions');
	$all_directors = $db->listAll('users-director-permissions');
	$all_softpath = $utils->ConvertAllUsersWithAPermissionToStr($db->listAll('get-all-users-with-a-permission', 'softpath'));

	if (isset($_GET['visit_id']) && isset($patientArray[0]) && isset($visitArray[0]) && isset($patientArray[0]['medical_record_num']) && isset($patientArray[0]['last_name']) && isset($visitArray[0]['mol_num']))
	{
		// get version information.  Original report will not have an entry here
		// empty array means that this report was not revised
		$revisionsArray = $db->listAll('revisions', $_GET['visit_id']);

		// Make an array of all of the pdf's saved in 
		// SERVER_SAVE_LOC/report_versions for the current sample include 
		// current report which is not saved in the the above directory.  
		// The current report is made on the fly
		if (!empty($revisionsArray))
		{
			$curr_report = '?page=generate_report&run_id='.$_GET['run_id'].'&visit_id='.$_GET['visit_id'].'&patient_id='.$_GET['patient_id'];
			$curr_reports = array(
				'curr_report'	=> 	$curr_report.'&header_status=on',	
				'no_header'	=> 	$curr_report.'&header_status=off'
			);

			// Get directory which should have the past pdfs for the this report
			$report_name = 'MRN_'.$patientArray[0]['medical_record_num'].'_MD_'.$visitArray[0]['mol_num'].'_'.$patientArray[0]['last_name'];
			$patient_dir = SERVER_SAVE_LOC.'report_versions/'.$report_name;
			$visit_dir = $patient_dir.'/'.$visitArray[0]['visit_id'];
		
			// make sure the directory exists
			if (file_exists($visit_dir))
			{
				// find all files in visit_dir
				$previous_reports = $utils->getAllFilesInDir($visit_dir);
					
			}

		}

	}

?>