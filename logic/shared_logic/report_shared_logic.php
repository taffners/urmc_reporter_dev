<?php
	$page_title = 'Update Report';
	
	if ($utils->validateGetInt('run_id') && $utils->validateGetInt('visit_id'))
	{

		$cols = $db->listAll('get-table-col-names', 'observed_variant_table');

		if (!isset($completed_steps))
		{

			$completed_steps = $db->listAll('all-complete-steps', $_GET['visit_id']);
		}
	
		$curr_step_complete = $utils->in_array_r($page, $completed_steps);
		$wet_bench_tech = $db->listAll('wet-bench-tech-run', $_GET['run_id']);
		
		$qc_run_step_status = $stepTracker->StepStatus($completed_steps, 'qc');
		$qc_variant_step_status = $stepTracker->StepStatus($completed_steps, 'qc_variant');
		$low_cov_status = $db->listAll('low-cov-status', $_GET['run_id']);		
	}

	if (isset($_GET['orderable_tests_id']))
	{
		$orderableTestArray = $db->listAll('orderable-tests', $_GET['orderable_tests_id']);
	}


	//////////////////////////////////////////////////////////////////////
	// Get genes used for the report as of the date the data was uploaded.
	// pre_step_visit table has a step called upload_NGS_data	
	//////////////////////////////////////////////////////////////////////
	// Get upload_NGS_data_date.  If visit_id not found use today's date
	if (!isset($upload_NGS_date))
	{
		if (isset($visitArray[0]['visit_id']))
		{
			// Get upload_NGS_data.  If not found use time_stamp in visitArray
			$upload_NGS_data_date = $db->listAll('upload_ngs_date_pre_step', $visitArray[0]['visit_id']);
			
			// If upload date not found use time_stamp in visitArray
			if (isset($upload_NGS_data_date[0]['upload_date']))
			{
				$upload_NGS_date = $upload_NGS_data_date[0]['upload_date'];
			}
			else
			{
				$dt = new DateTime($visitArray[0]['time_stamp']);
				$upload_NGS_date = $dt->format("Y-m-d");
			}		
		}
		else
		{
			$upload_NGS_date = date("Y-m-d");
		}
	}

	$lowCoverageArray = $db->listAll('low-coverage-by-run-id-with-gene-include-status', array('run_id'=> RUN_ID, 'ngs_panel_id' => $visitArray[0]['ngs_panel_id'], 'upload_NGS_date'=> $upload_NGS_date));

	////////////////////////////////////////////////////////////////////
	// Find all previous gene interpts for each gene going to report
	////////////////////////////////////////////////////////////////////
	if (isset($reportGeneOrder) && !empty($reportGeneOrder))
	{		
		$all_gene_interpts = array();
		$run_xref_interpts = array();

		foreach ($reportGeneOrder as $key => $gene)
		{
			////////////////////////////////////////////////////////////////
			// GENE INTERPTS
			// Get all interpts previously added for each interpt for genes 
			// ($all_gene_interpts) and get current interpt for the genes in the 
			// report ($run_xref_interpts)
			////////////////////////////////////////////////////////////////
			$gene_interpts = $db->listAll('all-gene-interpts-for-gene', $gene);
			$all_gene_interpts[$gene] = $gene_interpts;

			// find interpts already set for report for updating
			$search_array = array(
							'run_id' 	=> RUN_ID,
							'gene' 	=> $gene
						);

			$xref_found = $db->listAll('run-xref-gene-interpt-by-run-id-gene', $search_array);

			if (isset($xref_found[0]['interpt']) && $xref_found[0]['interpt'] !== null)
			{

				$run_xref_interpts[$gene] = $xref_found;
			}
		}
	}

	////////////////////////////////////////////////////////////////////////
	// Find genes which need to be confirmed.  
	////////////////////////////////////////////////////////////////////////
	$pending_confirmations = '';
	$confirmed = '';
	$failed_confirmation = '';

	if ( $page == 'review_report' || $page == 'show_edit_report' )
	{
		$completed_confirmation_status = $db->listAll('confirm-status-with-ovt-info', $_GET['run_id']);

		if (isset($completed_confirmation_status) && $completed_confirmation_status != '')
		{
			foreach($completed_confirmation_status as $i => $array)
			{
				if ($array['confirmation_status'] === 'pending')
				{
					$pending_confirmations .= $array['genes'].' '.$array['coding'].' ('.$array['amino_acid_change'].'), ';
				}

				elseif ($array['confirmation_status'] === 'confirmed')
				{
					$confirmed .= $array['genes'].' '.$array['coding'].' ('.$array['amino_acid_change'].'), ';
				}
				elseif ($array['confirmation_status'] === 'not confirmed')
				{
					$failed_confirmation .= $array['genes'].' '.$array['coding'].' ('.$array['amino_acid_change'].'), ';
				}
			}
		}		
	}
	
	////////////////////////////////////////////////////////////////////
	// Get Report interpt
	////////////////////////////////////////////////////////////////////
	$full_interpt = '';

	// get summary ($runInfoArray)
	if (isset($runInfoArray[0]['summary']) && $runInfoArray[0]['summary'] != null)
	{
		$full_interpt.=$runInfoArray[0]['summary'];
		$interpt_summary = $runInfoArray[0]['summary'];
	}

	////////////////////////////////////////////////////////////////////
	// Add both the gene_interpts and variant interpts in the order of 
	// reportGeneOrder
	////////////////////////////////////////////////////////////////////
	$interpt_without_summary = '';
	$genes_added = array();
	
	if (isset($reportMutantTables))
	{				
		foreach ($reportMutantTables as $SectionHeader => $SectionData)
		{
			if ($interpt_without_summary !== '')
			{
				$interpt_without_summary.= '<br><br>';	
			}

			// Only add section header if section is not empty
			if ($SectionHeader !== 'MUTATIONS' && sizeof($SectionData[0]) > 0)
			{
				// add section header
				$interpt_without_summary.=$SectionHeader;			
			}		

			// Add each section (MUTATIONS or VARIANTS OF UNKNOWN SIG)
			foreach($SectionData[0] as $key => $interpt)
			{

				if ($interpt_without_summary !== '')
				{
					$interpt_without_summary.= '<br><br>';	
				}

				// before adding an interpretation check if there is a gene interpt to add and make sure it wasn't already added.
				$gene = $interpt['Gene'];

				if (isset($run_xref_interpts[$gene][0]['interpt']) && !in_array($gene, $genes_added))
				{
					array_push($genes_added, $gene);
					$interpt_without_summary.=$run_xref_interpts[$gene][0]['interpt'].'<br><br>';
				}

				$interpt_without_summary.=$interpt['variant_interpt'];
			}
		}
		
		if ($full_interpt !== '')
		{
			$full_interpt.= '<br><br>';	
		}
		$full_interpt.=$interpt_without_summary;		
	}



?>