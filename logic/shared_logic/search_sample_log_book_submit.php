<?php

	if (
				isset($_POST['search-all-samples-submit']) && 
				(
					(
						isset($_POST['search']) && !empty($_POST['search']) 
					) ||
					(
						isset($_POST['search-by-start-date']) && !empty($_POST['search-by-start-date']) 
					) ||
					(
						isset($_POST['search-by-end-date']) && !empty($_POST['search-by-end-date']) 
					)
				)
			)
	{
		$url = 'Location: '.REDIRECT_URL.'?page=sample_log_book';

		if (isset($_POST['search']) && !empty($_POST['search']))
		{
			$log_book_search = filter_input(INPUT_POST, 'search', FILTER_SANITIZE_STRING);
			$url.= '&search='.$log_book_search;
		}

		if (isset($_POST['search-by-start-date']) && !empty($_POST['search-by-start-date']))
		{
			$url.= '&search_start_date='.$_POST['search-by-start-date'];
		}

		if (isset($_POST['search-by-end-date']) && !empty($_POST['search-by-end-date']))
		{
			$url.= '&search_end_date='.$_POST['search-by-end-date'];
		}

		header($url);	

	}
	
?>