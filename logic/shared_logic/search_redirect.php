<?php

	if 	(isset($_POST['submit']) && 
			(isset($_POST['search_term']) && $_POST['search_term'] !== '' && strlen(trim($_POST['search_term'])) > 2)  || 
			(isset($_POST['date_filter'])  && ($_POST['start_date'] !== '' || $_POST['end_date'] !== '') )  
		)
	{

		$search_type = '&search_type=normie';
		$url = 'Location:'.REDIRECT_URL.'?page='.$redirect_page.'&search_term='.$_POST['search_term'];

		// if radio_filter is a set send this to results also
		if (isset($_POST['radio_filter']))
		{
			$url .= '&radio_filter='.$_POST['radio_filter'];
		}
		
		// for date range searches a radio button can be added to search called date_filter.  
		// If this is checked 3 more variables need to be added to $url
		// search_type= (if date_filter == yes set to date) default is normie
		// start_date
		// end_date
		if (isset($_POST['date_filter']))
		{

			if ($_POST['date_filter'] === 'yes') 
			{
				$search_type = '&search_type=date';

				if ($_POST['start_date'] !== '')
				{
					$url .= '&start_date='.$_POST['start_date'];
				}

				if ($_POST['end_date'] !== '')
				{
					$url .= '&end_date='.$_POST['end_date'];
				}
			}
		}
		
		// send to search page search results
		header($url.$search_type);
		exit;

	}
	elseif (isset($_POST['date_filter']) && $_POST['date_filter'] === 'yes' && ($_POST['start_date'] === '' || $_POST['end_date'] === ''))
	{
		$message = 'Please enter a date';
	}

	elseif (isset($_POST['submit']) && trim($_POST['search_term']) === '' )
	{
		$message = 'Make Sure you enter data in the search field';
	}
	elseif (isset($_POST['submit']) &&  strlen(trim($_POST['search_term'])) <= 2 )
	{
		$message = 'Enter at least three characters as a search term';
	}

?>