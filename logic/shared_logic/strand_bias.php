<?php
	
	if ($utils->validateGetInt('run_id'))
	{
		$strand_bias_per_variant_inter = $db->listAll('qc-strand-bias', $_GET['run_id']);

		$strand_bias_per_variant = array();

		// add strand bias calculation
		foreach ($strand_bias_per_variant_inter as $key => $allele)
		{
			$allele_with_SB = $utils->strandOddsRatio($allele);

			$strand_bias_per_variant[$key] = $allele_with_SB;
		}	
	}
?>