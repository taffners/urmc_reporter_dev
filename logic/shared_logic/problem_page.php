<?php 
	$page_title = 'OOPS! something went wrong!';
	$problem_array = array(

			'problem_page'		=>	$_SERVER['QUERY_STRING'],
			'user_id'			=> 	USER_ID
		);

	$problem_result = $db->addOrModifyRecord('problem_pages_table', $problem_array);

	$doh_img = '<img src="images/homer_doh.jpg" alt="Homer computer doh">';

	// redirect to notes_slides this will give the user the ability to send an email about the problem
	header('Location:'.REDIRECT_URL.'?page=notes_slides&problem_id='.$problem_result[1]);

?>