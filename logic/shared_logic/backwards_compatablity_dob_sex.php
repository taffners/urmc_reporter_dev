<?php
	// When the DOB and Sex was added to the sample log book table there are dobs in the patient table that appeared to be lost.  Extract them and update sampleLogBookArray
	if (isset($sampleLogBookArray[0]) && empty($sampleLogBookArray[0]['dob']) && isset($sampleLogBookArray[0]) && empty($sampleLogBookArray[0]['sex']))
	{
		$dob_sex = $db->listAll('patient-dob-sex-from-sample-log-book-id', $_GET['sample_log_book_id']);
		
		if (isset($dob_sex[0]) && !empty($dob_sex[0]['dob']) && !empty($dob_sex[0]['sex']))	
		{
			$sampleLogBookArray[0]['dob'] = $dob_sex[0]['dob'];
			$sampleLogBookArray[0]['sex'] = $dob_sex[0]['sex'];		
		}		
	}
	
?>