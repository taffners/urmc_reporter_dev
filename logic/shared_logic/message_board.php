<?php
	//////////////////////////////////////////////////////////////////
	// run message board
	//////////////////////////////////////////////////////////////////
	# Get all previous messages in comment table with run_id and comment_type message_board
	$all_messages = $db->listAll('message-board-comments-plus-visit-comments', RUN_ID);

	# submit message and send email to people to notify
	if (isset($_POST['message_board_submit']) && !empty($_POST['message']) && !empty($_POST['message_board_memebers']) && $utils->validateGetInt('visit_id'))
	{
		/////////////////////////////////////////////////////
		# Find who to send message to 
			# $_POST['message_board_memebers'] only contains user_id
		/////////////////////////////////////////////////////
		$to = '';
		$to_names = '';
		
		if (!isset($_POST['message_board_memebers']['skip']))
		{
			foreach($_POST['message_board_memebers'] as $key => $member_userid)
			{
				$member_info = $db->listAll('user', $member_userid);

				if ($member_info[0]['email_address'] === 'taffners@gmail.com')
				{
					$member_info[0]['email_address'] = 'samantha_taffner@urmc.rochester.edu';
				}

				if ($to !== '')
				{
					$to.=',';
					$to_names.=', ';
				}

				$to.=$member_info[0]['email_address'];
				$to_names.=$member_info[0]['first_name'].' '.$member_info[0]['last_name'];
			}
			$user_info = $db->listAll('user', USER_ID);
			
			$to = $to;
			$subject = 'New Message for Report # '.$_GET['visit_id'];
			$body = 'Hello '.$to_names.',

				'.$user_info[0]['first_name'].' would like to send you this message:

				"'.$_POST['message'].'"';
			
			$email_utils->sendEmail($to, $subject, $body, 'skip');
		}
		
		/////////////////////////////////////////////////////
		# Add message to comment table
		/////////////////////////////////////////////////////
		$message_comment_array = array(
			'user_id'		=> 	USER_ID,
			'comment_ref'	=> 	RUN_ID,
			'comment_type'	=> 	'message_board',
			'comment'		=> 	$_POST['message']	
		);

		$comment_add_result =$db->addOrModifyRecord('comment_table', $message_comment_array);

		header('Location:'.REDIRECT_URL.'/?'.$_SERVER['QUERY_STRING'].'#message_board');		
	}
	else if (isset($_POST['message_board_submit']) && empty($_POST['message_board_memebers']))
	{
		$message = 'Select at least one person to notify of the message ';
	}
	else if (isset($_POST['message_board_submit']) && empty($_POST['message']))
	{
		$message = 'Add a message to submit';
	}
?>