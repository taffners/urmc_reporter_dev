<?php

	// For updating Visit info at 'visit_id' to $_GET Then run query to get $visitArray
	if (!$utils->validateGetInt('visit_id'))
	{
		$fatal_err_occured = True;
	}
	else
	{
		$where_array = array(
				'visit_id'	=> 	$_GET['visit_id'],
				'step'		=>	$page
			);
		$step_status = $db->listAll('this-pre-step-status', $where_array);

		// see if visit already added
		$visitArray = $db->listAll('add-visit-info', $_GET['visit_id']);
				;
	}	

	//////////////////////////////////////////////////////
	// submit form
	//////////////////////////////////////////////////////
	if (isset($_POST[$page.'_submit']))
	{
		$insert_array = array(
				'visit_id'			=> 	$_POST['visit_id'],
				'user_id'				=>	USER_ID,
				'step'				=> 	$page,
				'status'				=> 	$_POST['status'],
				'concentration'		=> 	$_POST['concentration']
			);

		if (isset($_POST['pre_step_visit_id'])  && $_POST['pre_step_visit_id'] !== '')
		{
			$insert_array['pre_step_visit_id'] = $_POST['pre_step_visit_id'];
		}

		//////////////////////////////////////////////////////
		// if permission director and status = Failed add pre step pre_complete also this will remove from pre ngs sample list
		//////////////////////////////////////////////////////		
		if (strpos($user_permssions, 'director') !== false && isset($_POST['status']) && $_POST['status'] === 'Failed')
		{

			$pre_step_result = $db->addOrModifyRecord('pre_step_visit',$insert_array);

			$insert_array['step'] = 'pre_complete';
			unset($insert_array['pre_step_visit_id']);
			unset($insert_array['concentration']);
			
			$pre_step_result = $db->addOrModifyRecord('pre_step_visit',$insert_array);	
			header('Location:'.REDIRECT_URL.'?page=home');
		}

		//////////////////////////////////////////////////////
		// Find if step already added to db if so update it.  
		//////////////////////////////////////////////////////
		else if (isset($_POST['status']) && $_POST['status'] !== 'Failed')
		{
			$pre_step_result = $db->addOrModifyRecord('pre_step_visit',$insert_array);
			header('Location:'.REDIRECT_URL.'?page=home');
		}
	}

?>