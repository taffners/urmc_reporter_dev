<?php

	// if $_Get[ngs_panel_id] is present filter lists based on ngs_panel
	// This is for report building pages not the home page and not pre steps
	if ($utils->validateGetInt('ngs_panel_id'))
	{
		if (isset($_GET['all_pending_reports']))
		{
			$pending_runs = $db->listAll('pending-tech-runs-ngs-panel-id-all', $_GET['ngs_panel_id']);
		}
		else
		{
			$pending_runs = $db->listAll('pending-tech-runs-ngs-panel-id', $_GET['ngs_panel_id']);
		}


		// $pending_pre_runs = $db->listAll('pending-pre-samples-ngs-panel-id', $_GET['ngs_panel_id']);
// $starttime = microtime(true);
		$pending_pre_runs = $db->listAll('pending-pre-samples-ngs-panel-id-testing', $_GET['ngs_panel_id']);
// $endtime = microtime(true);		
// $duration = $endtime - $starttime;
// var_dump($duration);

		// get pools that have a timestamp within the last 2 weeks
		if 	(	
				(
					isset($_GET['pool_view_type']) &&
					$_GET['pool_view_type'] === 'recent'
				) 
				||
				(
					!isset($_GET['pool_view_type'])
				)
			)
		{
			$poolSearchArr = array('ngs_panel_id' => $_GET['ngs_panel_id'], 'pool_view_type' => 'recent');
		}
		else
		{
			$poolSearchArr = array('ngs_panel_id' => $_GET['ngs_panel_id']);
		}



		$pending_pools = $db->listAll('pending-pools-ngs-panel-id', $poolSearchArr);
	}

	else
	{
		if (isset($_GET['all_pending_reports']))
		{
			$pending_runs = $db->listAll('pending-tech-runs-all');
		}
		else
		{
			$pending_runs = $db->listAll('pending-tech-runs');	
		}
		

		// Get data for pre_ngs_samples_list

		// visits by patients by pre step
			// visits which haven't failed 
			// visits which haven't completed last step
		$pending_pre_runs = $db->listAll('pending-pre-samples');

		// get pools that have a timestamp within the last 2 weeks
		if 	(	
				(
					isset($_GET['pool_view_type']) &&
					$_GET['pool_view_type'] === 'recent'
				) 
				||
				(
					!isset($_GET['pool_view_type'])
				)
			)
		{
			$pending_pools = $db->listAll('pending-pools', array('pool_view_type' => 'recent'));
		}
		else
		{
			$pending_pools = $db->listAll('pending-pools');
		}

		
// var_dump($pending_pools);		
	}


	if (isset($_GET['sample_type']))
	{


		$pending_pre_runs = $db->listAll('pending-pre-samples-sent-out'); 

	}
// $pending_pre_runs = $db->listAll('pending-pre-samples-test');

?>

