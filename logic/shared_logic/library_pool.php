<?php
	// The OFA and myeloid panel have different pool steps find which panel
	// is being used and adjust poolsteps accordingly 
	if (!$utils->validateGetInt('ngs_panel_id'))
	{
		$fatal_err_occured = True;
	}
	else if ($_GET['ngs_panel_id'] === '1')
	{
		// ofa steps
		$poolSteps = array(
			'pool_download_excel',
			'pool_dna_conc',
			'pool_amp_conc',
			'pool_add_barcode',
			'pool_download_run_template',
			// 'pool_plan_torrent_run',  // trying to use the torrent server API to make run template
			//'pool_run_qc'
			'pool_select_results',
			'pool_qc_backup_data',
			'pool_upload_data'
		);

		// The above steps are minimum but adding pcr templates and lots needs too 
		// happen in the future.

		$poolStepTracker = new StepTracker($db, $poolSteps, 'pool_steps_table');
	}
	else if ($_GET['ngs_panel_id'] === '2')
	{
		// myeloid steps
		$poolSteps = array(
			'pool_dna_conc',
			'pool_dilution_worksheet',
			'pool_amp_conc',			
			'pool_make_run_template'//, // sbsuser pass danryan#1
			//'pool_upload_samples'
		);
		$poolStepTracker = new StepTracker($db, $poolSteps, 'pool_steps_table');

	}
	
	if ($utils->validateGetInt('pool_chip_linker_id'))
	{

		$completed_steps = $db->listAll('all-pool-complete-steps', $_GET['pool_chip_linker_id']);

		if (isset($poolStepTracker))
		{
			$previous_step = $poolStepTracker->FindPreviousStep($page);
			$step_status = $poolStepTracker->StepStatus($completed_steps, $page);
			$next_step = $poolStepTracker->FindNextStep($page);

			$terminalStepBtnStatus = $poolStepTracker->terminalStepBtnStatus($completed_steps, $page, 'pool_qc_backup_data');

			$lockPageStatus = $poolStepTracker->lockPageStatus($completed_steps, $page);
		}

		$pending_pools = $db->listAll('curr-pending-pools', $_GET['pool_chip_linker_id']);

		$visits_in_pool = $db->listAll('visits-in-pool', $_GET['pool_chip_linker_id']);				

		
		// find out how many chips are in pool
		$chip_count = 0;

		$possible_chips = array('count_chip_a', 'count_chip_b');
		
		foreach ($possible_chips as $key => $chip)
		{
			if 	(	
					isset($pending_pools[0][$chip]) && 
					is_numeric($pending_pools[0][$chip]) &&
					intval($pending_pools[0][$chip]) > 0
				)
			{
				$chip_count++;
			}
		}
	}


	if (isset($_GET['ngs_panel_id']))
	{
		$current_ngs_panel = $db->listAll('ngs-panel-info-by-id', $_GET['ngs_panel_id']);
	}

?>