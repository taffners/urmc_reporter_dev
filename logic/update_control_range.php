<?php

	$page_title = 'Update Expected Variant';

	if (isset($_GET['control_type_id']) && isset($_GET['expected_control_variants_id']))
	{
		$control_info = $db->listAll('control-info', $_GET['control_type_id']);

		// add name of control to page title
		$page_title = 'Change Range for Expected Variant in '.$control_info[0]['control_name'];

		$search_array = array(
			'control_type_id'	=> 	$_GET['control_type_id'],
			'expected_control_variants_id'	=> 	$_GET['expected_control_variants_id']
		);

		$summary_control_vaf = $db->listAll('expected-variant-with-stats', $search_array);
	}

	if (isset($_POST['submit_update_control_range']))
	{
		$expected_variant = $db->listAll('expected-control-variant-by-id', $_POST['expected_control_variants_id']);		

		if (isset($expected_variant) && !empty($expected_variant))
		{
			$update_array = $expected_variant[0];

			$update_array['user_id'] = USER_ID;
			$update_array['min_accepted_frequency'] = $_POST['min_accepted_frequency'];
			$update_array['max_accepted_frequency'] = $_POST['max_accepted_frequency'];
			
			$db->addOrModifyRecord('expected_control_variants_table', $update_array);
			header("Refresh:0");

		}
		else
		{
			$message = 'Variant can not be found.  Something went wrong.';
		}
		
	}
?>