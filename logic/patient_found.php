<?php
	$page_title = 'Update Report';

	if (isset($_GET) && isset($_GET['patient_id']))
	{
		$patientArray = $db->listAll('select-patient-from-id', $_GET['patient_id']);

		$patientVisits = $db->listAll('all-patient-visits', $_GET['patient_id']);
	}

	if (isset($_POST['patient_found_submit']) && isset($_GET['visit_id']) && isset($_GET['patient_id']))
	{

		// add patient id to visit table
		$update_result = $db->updateRecord('visit_table', 'visit_id', $_GET['visit_id'],'patient_id', $_GET['patient_id']); 

		// if this is the patient button is pushed finish step 
		//////////////////////////////////////////////////////
		// insert into pre_step_visit table
		//////////////////////////////////////////////////////	
		$completed_steps = $db->listAll('all-complete-steps', $_GET['visit_id']);

		$url = $preStepTracker->UpdatePreStepInDb($page, $completed_steps, $_GET['visit_id'], USER_ID, 0, 'add_patient', 'passed'); 
		
		//////////////////////////////////////////////////////
		// redirect to homepage
		//////////////////////////////////////////////////////
		header('Location:'.REDIRECT_URL.'?page=home');
	}
?>