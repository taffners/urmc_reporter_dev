<?php
	$page_title = 'Make MiSEQ Run Template';

	require_once('logic/shared_logic/library_pool.php');

	if (isset($_GET['pool_chip_linker_id']))
	{
		$search_array = array('pool_chip_linker_id' => $_GET['pool_chip_linker_id']);

		$library_pool = $db->listAll('myeloid-library-pool', $search_array);	
	}	

	if (isset($_POST['pool_make_run_template_submit']) && isset($_GET['pool_chip_linker_id']) && isset($_GET['ngs_panel_id']))
	{
		// add step completed to step_run_xref if not already completed
		$url = $poolStepTracker->UpdatePoolStepInDb($_GET['page'], $completed_steps, $_GET['pool_chip_linker_id'], USER_ID, $_GET['ngs_panel_id']);

		/////////////////////////////////////////////////////////////////////
		// change pool_chip_linker_table status to pre_complete
			// Check to make sure not already completed
		/////////////////////////////////////////////////////////////////////
		$pool_chip_linker = $db->listAll('pool-chip-linker-by-id', $_GET['pool_chip_linker_id']);

		// Update status of pool_status to pre_complete
		if (isset($pool_chip_linker[0]['status']) && $pool_chip_linker[0]['status'] === 'pending')
		{
			$update_array = $pool_chip_linker[0];
			$update_array['user_id'] = USER_ID;
			$update_array['status'] = 'pre_complete';
			$update_results = $db->addOrModifyRecord('pool_chip_linker_table', $update_array);	
		}
	
		header('Location:'.REDIRECT_URL.'?page=home');
	}

?>