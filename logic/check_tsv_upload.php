<?php
	$page_title = 'Upload NGS Data';
	$cols = $db->GetTableColNames('observed_variant_table');

	if (isset($_GET) && isset($_GET['visit_id']) && isset($_GET['run_id']))
	{

		$completed_steps = $db->listAll('all-complete-pre-steps', $_GET['visit_id']);

		// see if visit already added
		$visitArray = $db->listAll('add-visit-info', $_GET['visit_id']);	
		$variantArray = $db->listAll('run-variants-included-filter', array('run_id'=>$_GET['run_id'], 'upload_NGS_date' =>date('Y-m-d'), 'ngs_panel_id'=>$visitArray[0]['ngs_panel_id']));	
		$num_snvs = sizeof($variantArray);
		$pending_pre_runs = $db->listAll('pending-pre-samples-by-visit-id', $_GET['visit_id']);

		$wet_bench_tech = $db->listAll('wet-bench-tech-run', $_GET['run_id']);
		$low_cov_status = $db->listAll('low-cov-status', $_GET['run_id']);
		
	}

	if (isset($_POST['check_tsv_upload_confirm']))
	{
		// Redirect back to the pool if pool_chip_linker_id and ngs_panel_id is set.  This means it was uploaded via a pool.
		if (isset($_GET['pool_chip_linker_id']) && isset($_GET['ngs_panel_id']))
		{
			header('Location:'.REDIRECT_URL.'?page=pool_upload_data&pool_chip_linker_id='.$_GET['pool_chip_linker_id'].'&ngs_panel_id='.$_GET['ngs_panel_id']);
		}
		else
		{
			header('Location:'.REDIRECT_URL.'?page=home');
		}		
	}
	else if (isset($_POST['check_tsv_upload_incorrect']))
	{
		$email_utils->emailAdminsProblemURL(__FILE__.' Error uploading data !! Line #: '.__LINE__.' Refer to bug report if the user wrote a message.');
		require_once('logic/shared_logic/problem_page.php');
	}
?>