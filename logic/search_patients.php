<?php
	$page_title = 'Search Finished Patient Reports';

	$all_tissues = $db->listAll('all-tissues');
	$all_primary_tumor_site = $db->listAll('all-primary-tumor-site');

	// send all searched items so search results
	if (isset($_POST['submit']))
	{
		do 
		{
			$redirect_page = 'patient_search_results';

			// send search to patient_search_results
			$url = $utils->SearchPostArray($redirect_page, $_POST);

			if ($url !== 'OOPS! Please enter a search parameter.')
			{
				header($url);
			}
			else
			{
				$message = $url;
			}		
		}while(False);
			
	}

	// // set a page to redirect to.  Then the search redirect script will redirect if minimum search criteria is met.  Like at least 2 chars.
	// $redirect_page = 'patient_search_results';
	// require_once('logic/shared_logic/search_redirect.php');

?>