<?php
	
	// add all drop down menu items
	$all_tissues = $db->listAll('all-tissues');
	$possible_diagnosis = $db->listAll('possible-diagnosis');
	$ngs_panels = $db->listAll('ngs-panel-info');

	// get all controls from control_type_table
	$positive_controls = $db->listAll('all-positive-controls');
	$negative_controls = $db->listAll('all-negative-controls');

	$last_mol_num = $db->listAll('get-next-mol-num', date('y'));

	// Find all possible field_name s from the control_step_regulator_table version_lot (step_regulator_name)  Then PHP will add all possible labels to the input #version in HTML.  Then JS will had and remove them depending on selected control
	$control_version_fields = $db->listAll('version-lot-control-step-regulator');

	// set page up for updating
	if (isset($_GET['run_id'])) 
	{
		$page_title = 'Update Report';

		if (!isset($completed_steps))
		{

			$completed_steps = $db->listAll('all-complete-steps', $_GET['visit_id']);
		}

		$qc_run_step_status = $stepTracker->StepStatus($completed_steps, 'qc');
		$qc_variant_step_status = $stepTracker->StepStatus($completed_steps, 'qc_variant');
	}

	// Add a new visit.  This form is only used for adding new NGS controls. NGS visits are now
	// added via the add_test page
	else
	{
		$page_title = 'Add a Visit';	
	}

	// For updating Visit info at 'visit_id' to $_GET Then run query to get $visitArray
	if (isset($_GET) && isset($_GET['visit_id']))
	{
		$completed_steps = $db->listAll('all-complete-steps', $_GET['visit_id']);

		// get any comments previously reported
		$previousComments = $db->listAll('previous-comments-add-visit', $_GET['visit_id']);
	}
	else
	{
		$completed_steps = array();
		$visitArray = array();
	}

	// Make sure NGS Panel is not '' before submitting. 
	if (isset($_POST['add_visit_submit']) && $_POST['ngs_panel_id'] == '')
	{
		$message = 'Please select a NGS Panel';
	} 

	// Make sure Sample Type not empty
	else if (isset($_POST['add_visit_submit']) && !$utils->varSetNotEmpty($_POST['sample_type']))
	{
		$message = 'Please choose a Sample Type';
	}

	// Add Positive Control 
	else if(isset($_POST['add_visit_submit']) && $_POST['ngs_panel_id'] != '' && $utils->varSetNotEmpty($_POST['sample_type']) && $_POST['sample_type'] === 'Positive Control' && $utils->varSetNotEmpty($_POST['control_type_id']['positive']) )
	{
		// get positive control only vars

		$add_array = array(
			
			'control_type_id' 	=> 	$_POST['control_type_id']['positive']
		);	

		$add_info = $db->addControlVisitPatient($add_array, $_POST);
		
		if (is_array($add_info))
		{
			// add step completed to step_run_xref if not already completed
          	$url = $preStepTracker->UpdatePreStepInDb($page, $completed_steps, $add_info['visit_id'], USER_ID, RUN_ID, 'add_visit', 'passed'); 

          	$url = $preStepTracker->UpdatePreStepInDb($page, $completed_steps, $add_info['visit_id'], USER_ID, RUN_ID, 'add_patient', 'passed');

			header('Location:'.REDIRECT_URL.'?page=home');
		}
		else
		{
			$message = $add_info;
		}
	}

	// Add Negative Control 
	else if(isset($_POST['add_visit_submit']) && $_POST['ngs_panel_id'] != '' && $utils->varSetNotEmpty($_POST['sample_type']) && $_POST['sample_type'] === 'Negative Control' && $utils->varSetNotEmpty($_POST['control_type_id']['negative']) )
	{
		// get positive control only vars

		$add_array = array(
			
			'control_type_id' 	=> 	$_POST['control_type_id']['negative']
		);	

		$add_info = $db->addControlVisitPatient($add_array, $_POST);
		
		if (is_array($add_info))
		{
			// add step completed to step_run_xref if not already completed
          	$url = $preStepTracker->UpdatePreStepInDb($page, $completed_steps, $add_info['visit_id'], USER_ID, RUN_ID, 'add_visit', 'passed'); 

          	$url = $preStepTracker->UpdatePreStepInDb($page, $completed_steps, $add_info['visit_id'], USER_ID, RUN_ID, 'add_patient', 'passed');

			header('Location:'.REDIRECT_URL.'?page=home');
		}
		else
		{
			$message = $add_info;
		}
	}

	/////////////////////////////////////////////////////////////////////////////
	// Make sure whitespace does not occur in the important fields mol_num, soft_lab_num, order_num
		// This should be taken care of in javascript file views/events/forms/common_form_classes but this
		// is another check because whitespace will be very problematic for running OFA.
	/////////////////////////////////////////////////////////////////////////////
	// make sure soft_lab_num does not have white space
	else if(isset($_POST['add_visit_submit']) && isset($_POST['soft_lab_num']) && strpos($_POST['soft_lab_num'], ' ') !== false)
	{
		$message = 'Soft Lab Number can not contain white spaces.';
	}

	// make sure mol_num does not have white space
	else if(isset($_POST['add_visit_submit']) && isset($_POST['mol_num']) && strpos($_POST['mol_num'], ' ') !== false)
	{
		$message = 'MD# can not contain white spaces.';
	}

	// make sure order_num does not have white space.  This is highly unlikely since 
	// there is a required input format.
	else if(isset($_POST['add_visit_submit']) && isset($_POST['order_num']) && strpos($_POST['order_num'], ' ') !== false)
	{
		$message = 'mol# can not contain white spaces.';
	}

	/////////////////////////////////////////////////////////////////////////////
	// Make sure the important fields mol_num, soft_lab_num, order_num are unique only if
	// the form is not being updated
	/////////////////////////////////////////////////////////////////////////////
	else if (!isset($_GET['visit_id']) && isset($_POST['add_visit_submit']) && isset($_POST['soft_lab_num']) && sizeof($db->listAll('unique-visit-fields', array('field_name' => 'soft_lab_num', 'value' => $_POST['soft_lab_num']))) > 0 )
	{
		$message = 'The Soft Lab Number needs to be unique.';
	}

	else if (!isset($_GET['visit_id']) && isset($_POST['add_visit_submit']) && isset($_POST['order_num']) && sizeof($db->listAll('unique-visit-fields', array('field_name' => 'order_num', 'value' => $_POST['order_num']))) > 0 )
	{
		$message = 'The mol# needs to be unique.';
	}

	else if (!isset($_GET['visit_id']) && isset($_POST['add_visit_submit']) && isset($_POST['mol_num']) && sizeof($db->listAll('unique-visit-fields', array('field_name' => 'mol_num', 'value' => $_POST['mol_num']))) > 0 )
	{
		$message = 'The MD# needs to be unique.';
	}
	// add section for patient samples and validation samples
	else if(isset($_POST['add_visit_submit']) && $_POST['ngs_panel_id'] != '' && ($_POST['sample_type'] === 'Validation' || $_POST['sample_type'] === 'Patient Sample' || $_POST['sample_type'] === 'Sent Out'))
	{
		
		$message = '';
		$new_visit = False;

		$insertVisitArray = array();

		// make array to submit to database
		$insertVisitArray['soft_path_num'] = $_POST['soft_path_num'];
		$insertVisitArray['soft_lab_num'] = $_POST['soft_lab_num'];
		$insertVisitArray['mol_num'] = $_POST['mol_num'];
		$insertVisitArray['primary_tumor_site'] = $_POST['primary_tumor_site'];
		$insertVisitArray['block'] = $_POST['block'];
		$insertVisitArray['user_id'] = USER_ID;
		$insertVisitArray['run_id'] = RUN_ID;
		$insertVisitArray['ngs_panel_id'] = $_POST['ngs_panel_id'];
		$insertVisitArray['sample_type'] = $_POST['sample_type'];
		///////////////////////////////////////////////////////////////////
		// Add test_tissue 
			// Add Unknown if test_tissue = ''
			// Capitalize first character of each word
			// Check if it's already in the tissue_type_table.  If it isn't in tissue_type_table add it
			
		///////////////////////////////////////////////////////////////////

		if (empty($_POST['test_tissue']))
		{
			$_POST['test_tissue'] = 'Unknown';
		}
		else
		{
			$_POST['test_tissue'] = ucwords($_POST['test_tissue']);
		}

		// search tissue_type_type table
		$search_result = $db->listAll('search-tissues', $_POST['test_tissue']);
		
		// if test_tissue not found in tissue type table add it
		if (empty($search_result))
		{
			$to_add = array('tissue' 	=> 	$_POST['test_tissue']);
			$add_result = $db->addOrModifyRecord('tissue_type_table', $to_add);	
		}

		$insertVisitArray['test_tissue'] = $_POST['test_tissue'];
		$insertVisitArray['diagnosis_id'] = $_POST['diagnosis_id'];
		$insertVisitArray['tumor_cellularity'] = intval($_POST['tumor_cellularity']);
		$insertVisitArray['req_physician'] = $_POST['req_physician'];
		$insertVisitArray['req_loc'] = $_POST['req_loc'];
		$insertVisitArray['received'] = $_POST['received'];
		$insertVisitArray['collected'] = $_POST['collected'];
		$insertVisitArray['account_num'] = $_POST['account_num'];
		$insertVisitArray['order_num'] = $_POST['order_num'];
		$insertVisitArray['chart_num'] = $_POST['chart_num'];

		if ($_POST['patient_id'] !== '')
		{
			$insertVisitArray['patient_id'] = $_POST['patient_id'];
		}

		// for updating visit
		if($_POST['visit_id'] !== '')
		{
			$insertVisitArray['visit_id'] = $_POST['visit_id'];
		}

		// if visit_id is empty then email users with softpath permissions for patient samples.  Make sure test is live first.
		else if ($_POST['sample_type'] === 'Patient Sample')
		{
			$ngs_panel_full_info = $db->listAll('ngs-panel-info-by-id', $visitArray[0]['ngs_panel_id']);
	
			if( strtotime($ngs_panel_full_info[0]['go_live_date']) <= strtotime('now') ) 
			{
				$email_utils->emailSoftpathsNewReport($_POST);
			}

		}

		$visit_result = $db->addOrModifyRecord('visit_table',$insertVisitArray);

          if($visit_result[0] == 1)
          {          	
          	$visit_id = $visit_result[1];
          	
          	// add any comments to the comment table
			if (isset($_POST['comments']) && !$utils->textareaEmpty($_POST['comments']))
			{	
				$commentInsertArray = array();
				$commentInsertArray['user_id'] = USER_ID;
				$commentInsertArray['comment_ref'] =  $visit_id;
				$commentInsertArray['comment_type'] =  'add_visit';
				$commentInsertArray['comment'] =  $_POST['comments'];

				$comment_result = $db->addOrModifyRecord('comment_table', $commentInsertArray);
			}


 			// add a completed step of sent_out
          	if ($_POST['sample_type'] === 'Sent Out')
          	{
          		// add step completed to step_run_xref if not already completed
          		$url = $preStepTracker->UpdatePreStepInDb('sent_out', $completed_steps, $visit_id, USER_ID, RUN_ID, 'sent_out', 'passed');
          	}

			// add step completed to step_run_xref if not already completed
          	$url = $preStepTracker->UpdatePreStepInDb($page, $completed_steps, $visit_id, USER_ID, RUN_ID, 'add_visit', 'passed');         	

			// redirect to homepage if run_id and visit_id is not set. This 
			// This would happen when the user is updating visit form from
			// pre-step. 
			if (isset($_GET['visit_id']) && !isset($_GET['run_id']))
			{
				header('Location:'.REDIRECT_URL.'?page=home');
			}

			// add patient 
			$redirect = 'Location:'.REDIRECT_URL.$url;

			if ($_POST['patient_id'] != '')
			{
				$redirect .= '&patient_id='.$_POST['patient_id'];
			}

			if ($_POST['run_id'] != '' && $_POST['run_id'] != 0)
			{
				$redirect .= '&run_id='.$_POST['run_id'];
			}
			
			header($redirect);				
          }
          else
          {
			$message = 'Submission failed';
          }
	}
?>