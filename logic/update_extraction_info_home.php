<?php
	
	if (!isset($_GET['sample_log_book_id']) || !isset($_GET['ordered_test_id']))
	{
		header('Location:'.REDIRECT_URL.'?page=new_home');
	}

	$sampleLogBookArray = $db->listAll('all-samples-in-log-book-by-sample-log-book-id',$_GET['sample_log_book_id']);

	$page_title = 'Update extraction Info on '.$sampleLogBookArray[0]['first_name'].' '.$sampleLogBookArray[0]['last_name'].' '.$sampleLogBookArray[0]['medical_record_num'].' '.$sampleLogBookArray[0]['soft_lab_num'];

	$extraction_users = $db->listAll('extraction-users-tasks', $_GET['ordered_test_id']);
?>