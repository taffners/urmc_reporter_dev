<?php

	if(!isset($_GET['confirmation_id']))
	{
		header('Location:'.REDIRECT_URL.'?page=new_home');
	}

	$confirmation_array = $db->listAll('confirmation-by-id', $_GET['confirmation_id']);

	$page_title = 'Update Confirmations';

	if (isset($_POST['update_confirmation_submit']))
	{
		$add_arr = array();
		$add_arr['confirmation_id'] = $_GET['confirmation_id'];
		$add_arr['user_id'] = $confirmation_array[0]['user_id'];
		$add_arr['run_id'] = $confirmation_array[0]['run_id'];
		$add_arr['visit_id'] = $confirmation_array[0]['visit_id'];
		$add_arr['observed_variant_id'] = $confirmation_array[0]['observed_variant_id'];
		$add_arr['message'] = $confirmation_array[0]['message'];
		$add_arr['outcome'] = $_POST['outcome'];
		$add_arr['status'] = $_POST['status'];

		$add_arr['tech_confirm_user_id'] = $_POST['tech_confirm_user_id'];
		$add_arr['tech_confirm_message'] = $_POST['tech_confirm_message'];

		$add_result = $db->addOrModifyRecord('confirmation_table',$add_arr);

		$subject = 'Finished Confirmation # '.$confirmation_array[0]['visit_id'];
		$body = $_POST['message'];

		$email_utils->sendEmail($confirmation_array[0]['confirmation_requester_email'], $subject, $body, 'skip');
			
		/////////////////////////////////////////////////////
		# Add message to comment table so it will show in the message board for record keeping.
		/////////////////////////////////////////////////////
		$message_comment_array = array(
			'user_id'		=> 	USER_ID,
			'comment_ref'	=> 	$confirmation_array[0]['run_id'],
			'comment_type'	=> 	'message_board',
			'comment'		=> 	$_POST['message']	
		);

		$comment_add_result =$db->addOrModifyRecord('comment_table', $message_comment_array);

		header('Location:'.REDIRECT_URL.'?page=confirmation_list');
	}
?>