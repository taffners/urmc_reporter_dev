<?php

	$page_title = 'Update Knowledge Base';

	ini_set('max_execution_time', '1200');

	////////////////////////////////////////////////////////////////
	// Get list of genes from cosmic/genes_in_assays.csv
	////////////////////////////////////////////////////////////////
	$file_reader_array = array(
		'db' 		=> 	$db,
		'file'   		=>	'cosmic/genes_in_assays.csv',
		'type'		=> 	'genes_in_assays'
	);		

	$genesFileReader = new FileReader($file_reader_array);

	$upload_genes = $genesFileReader->return_data();

	$genesFileReader->close_file();

	$cosmic_tsv = '/var/www/html/cosmic_tsvs/CosmicMutantExport.tsv';
	if 	(
			isset($_POST['split_into_separte_files_submit']) && 
			isset($upload_genes) && 
			file_exists($cosmic_tsv) && 
			!empty($upload_genes)
		)
	{
 
		$open_files_array = array();

		$cosmic_per_gene_folder = '/var/www/html/cosmic_tsvs/cosmic_per_gene';
		if (!file_exists($cosmic_per_gene_folder))
		{
			mkdir($cosmic_per_gene_folder);
		}

		//////////////////////////////////////////
		// Create a separate file for each gene in the upload_genes array
		//////////////////////////////////////////
		foreach($upload_genes as $key => $gene)
		{
			$curr_file = $cosmic_per_gene_folder.'/'.$gene.'.tsv';
			$open_files_array[$gene] = array('Open_file' => fopen($curr_file, 'w'), 'Variant_count' => 0);
		}
		
		//////////////////////////////////////////
		// Open up cosmic file for all genes in cosmic.  As of 4/29/2020 The cosmic file 
		// CosmicMutantExport.tsv has 46,232,473 lines.  Every time a gene has to be added all 46 Million 
		// lines needs to be read.  This is taking too long and causing a lag
		//////////////////////////////////////////
		$cof = fopen($cosmic_tsv, 'r');
		$line = fgets($cof);

		//////////////////////////////////////////
		// Add header to each gene file
		//////////////////////////////////////////
		foreach ($open_files_array as $gene => $gof)
		{
			fwrite($gof['Open_file'], $line);
		}

		//////////////////////////////////////////
		// Write all variants for genes of interest to gene file
		//////////////////////////////////////////
		$line_num = 0;
		$keep_var_count = 0;

		while(!feof($cof))
		{
			$line = fgets($cof);
			$line_num++;
			$split_line = explode("\t", $line);
			$gene = $split_line[0];
			
			// Ignore empty line and only get snvs relevant to the current gene updating
			// Some entries have genename_transcript These seem to be the least used
			// transcript.  I'm going to ignore these and only keep the genes without 
			// underscore
			if (in_array($gene, $upload_genes))
			{
				$keep_var_count++;

				fwrite($open_files_array[$gene]['Open_file'], $line);
				$open_files_array[$gene]['Variant_count']++;
			}	
		}

		////////////////////////////////////
		// close all files and Provide update for user
		////////////////////////////////////
		fclose($cof);
		
		$message = '<ul>';
		
		foreach($open_files_array as $gene => $gof)
		{
			fclose($gof['Open_file']);
			$message.= '<li>'.$gene.' has '.$gof['Variant_count'].' variants added</li>';
		}
		
		$message.='</ul>';

	}
	else if 	(
					isset($_POST['split_into_separte_files_submit']) && 
					isset($upload_genes) && 
					!file_exists($cosmic_tsv) && 
					!empty($upload_genes)
				)
	{
		$message = $cosmic_tsv.' Can not be found';
	}		
	
	//////////////////////////////////////////////////////////////////////////
	// Find cosmic file otherwise throw error
	//////////////////////////////////////////////////////////////////////////
	// $cosmic_tsv = '/var/www/html/cosmic_tsvs/cosmic_per_gene/RUNX1.tsv';
	// if (file_exists($cosmic_tsv))
	// {
	// 	//////////////////////////////////////////////////////////////////////////
	// 	// read thru cosmic tsv file and find all variants which equal $_POST['gene'] if none found throw error
	// 	//////////////////////////////////////////////////////////////////////////
	// 	$file_reader_array = array(
	// 		'db' 		=> 	$db,
	// 		'file'   		=>	$cosmic_tsv,
	// 		'type'		=> 	'update_cosmic_knowledge_base',
	// 		'gene_name'	=> 	'RUNX1'
	// 	);


	// 	$genesFileReader = new FileReader($file_reader_array);
	// 	echo json_encode($genesFileReader->updateCosmicKnowledgeBase());

	// 	$genesFileReader->close_file();
	// }	





?>