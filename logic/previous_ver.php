<?php
	// display any previous versions of the report which are saved as PDFs
	// in the SERVER_SAVE_LOC.'report_versions/'
	$patient_dir = SERVER_SAVE_LOC.'report_versions'.DIRECTORY_SEPARATOR.$_GET['report_name'];
	$visit_dir = $patient_dir.DIRECTORY_SEPARATOR.$_GET['visit_id'];
	$pdf_file = $visit_dir.DIRECTORY_SEPARATOR.$_GET['ver'].'.pdf';

	// display the pdf file virtually in screen
	$content = file_get_contents($pdf_file);
	header('Content-type: application/pdf');
	header('Content-Disposition: filename='.$_GET['ver'].'.pdf');
	echo $content;

?>