<?php
	$page_title = 'Add a Test to';

	// add all drop down menus for NGS tests
	$all_tissues = $db->listAll('all-tissues');
	$possible_diagnosis = $db->listAll('possible-diagnosis');
	$ngs_panels = $db->listAll('ngs-panel-info');

	$historyResults = $db->listAll('distinct-test-results');

	$allTestsAvailable = $db->listAll('all-tests-available-active-tests-minus-sendouts');

	if (isset($_GET['sample_log_book_id']))
	{
		$searchSampleLogBookArray = array();
		$searchSampleLogBookArray['sample_log_book_id'] = $_GET['sample_log_book_id'];

		if (isset($_GET['visit_id']) && isset($_GET['patient_id']) && !empty($_GET['patient_id']) && !empty($_GET['visit_id']))
		{
			$searchSampleLogBookArray['visit_id'] = $_GET['visit_id'];
			$searchSampleLogBookArray['patient_id'] = $_GET['patient_id'];
		}

		$sampleLogBookArray = $db->listAll('sample-log-book-by-id', $searchSampleLogBookArray);

		// get number of previous order num.  order_num is assigned via JS this is only to used to show or hide the assign-new-mol-num button on the template page.
		$num_tests_with_order_num = $db->listAll('num-tests-with-order-num', $_GET['sample_log_book_id']);

		$page_title = 'Add a Test to '.$sampleLogBookArray[0]['mol_num'];

		// get all previous test history
		$searchArray = array(
			'medical_record_num' 	=> empty($sampleLogBookArray[0]['medical_record_num']) ? 'NONE' : $sampleLogBookArray[0]['medical_record_num'],
			'mpi' 					=> empty($sampleLogBookArray[0]['mpi']) ? 'NONE' : $sampleLogBookArray[0]['mpi'],
			'last_name'				=> empty($sampleLogBookArray[0]['last_name']) ? 'NONE' : $sampleLogBookArray[0]['last_name'],
			'first_name' 			=> empty($sampleLogBookArray[0]['first_name']) ? 'NONE' : $sampleLogBookArray[0]['first_name']
		);

		$previous_tests_by_mrn = $db->listAll('patient-tests-history-mrn', $searchArray);	
	}

	if (!isset($_GET['visit_id']))
	{
		$visitArray = array();
	}

	if (isset($_GET['ordered_test_id']))
	{
		// Get info about data entered about the test
		$orderedTestArray = $db->listAll('ordered-test-info', $_GET['ordered_test_id']);

		if (isset($orderedTestArray[0]['orderable_tests_id']))
		{
			// Get info about the test that was ordered
			$testInfo = $db->listAll('orderable-tests', $orderedTestArray[0]['orderable_tests_id']);		
		}

		// get history section of test
		if (isset($orderedTestArray[0]['previous_positive_required']) && $orderedTestArray[0]['previous_positive_required'] === 'yes')
		{
			$histories = $db->listAll('test-histories', $orderedTestArray[0]['ordered_test_id']);
		}


		$searchArray = array(
			'comment_type'		=>	'ordered_test_table',
			'comment_ref'		=>	$_GET['ordered_test_id']
		);

		$previousComments = $db->listAll('previous-comments', $searchArray);	
	}
	else
	{
		$orderedTestArray = array();
	}	

	if (!isset($_GET['sample_log_book_id']) || empty($_GET['sample_log_book_id']))
	{
		header('Location:'.REDIRECT_URL.'?page=sample_log_book');
	}

	else if (isset($_POST['add_test_submit']) && !empty($_POST['add_test_submit']) && !empty($_POST['orderable_tests_id']))
	{	
		do 
		{

			////////////////////////////////////////////////////////////////////////////
			// Depending on the test which is chosen there are extra fields which need to 
			// be added.  
				// previous_positive_required 
					// Add to previous_positive_table
					// Default ordered_test_id 0.  This is for user_input of history
				// mol_num_required -> for NGS tests
					// Make a new visit and patient entry.
				// consent_required	
					// Add to ordered_test_table
				// custom_fields_available -> not being used
			////////////////////////////////////////////////////////////////////////////
			foreach($allTestsAvailable as $key => $test)
			{
				if ($test['orderable_tests_id'] === $_POST['orderable_tests_id'])
				{
					$current_test_info = $test;
					break;	
				}		
			}

			//////////////////////////////////////////////////////////////////////////////
			// Find amount of time allowed as expected turn around time if include_in_turn_around_calculation === yes
				// there are two instances which affect turn around time
					// start date depends on completion of another test
						// fields in orderable_tests_table
							// turnaround_time_depends_on_ordered_test === 'yes' 
							// dependency_orderable_tests_id != 0
						// There are some tests where the start date changes to the 
						// completion date of the dependent test if they were ordered
						// within 24 hours of each other.  This will be adjusted for
						// when the test is signed out.

					// exepected_turnaround_time depends on result of test.
						// rows in test_turnaround_table 
						// Add the greatest amount of turn around time				   
			//////////////////////////////////////////////////////////////////////////////
			$exepected_turnaround_time = 0;
			if ($current_test_info['include_in_turn_around_calculation'] === 'yes' )
			{
				// search orderable_tests_table to find dependent test
				$max_turn_around_time = $db->listAll('max-turn-around-time', $_POST['orderable_tests_id']);
				
				if (isset($max_turn_around_time[0]['max_turnaround_time']))
				{
					$exepected_turnaround_time = $max_turn_around_time[0]['max_turnaround_time'];
				}			
			}
			
			//////////////////////////////////////////////////////////////////////////////
			// Add visit and patient for NGS samples
				// There will not be any updating via this form for NGS samples since 
				// template will redirect all samples to visit form
			//////////////////////////////////////////////////////////////////////////////

			// Add new visit if visit_id is not set
			if ($current_test_info['mol_num_required'] === 'yes')
			{
				///////////////////////////////////////////////////////////////////////////
				// Update sample_log_book_table
					// patient info - not updatable if not empty
						// sex, dob
					// soft_lab_num, test_tissue


				// 'soft_path_num' => string '' (length=0)
				// 'soft_lab_num' => string 'UNK25' (length=5)
				// 'test_tissue' => string 'Colon' (length=5)
				// 'block' => string '' (length=0)
				// 'tumor_cellularity' => null
				// DO NOT UPDATE RECEIVED.  THIS IS LINKED TO TEST NOT SAMPLE.
				//////////////////////////////////////////////////////////////////////////
				if 	(
						isset($_POST['sex']) &&
						!empty($_POST['sex']) &&
						(
							$_POST['sex'] === 'F' ||
							$_POST['sex'] === 'M' ||
							$_POST['sex'] === 'U'
						) &&
						$_POST['sex'] !== $sampleLogBookArray[0]['sex'] 
					)
				{			
					$db->updateRecord('sample_log_book_table', 'sample_log_book_id', $_GET['sample_log_book_id'], 'sex', $_POST['sex']);
				}

				if 	(
						isset($_POST['dob']) &&
						!empty($_POST['dob']) &&
						$_POST['dob'] !== $sampleLogBookArray[0]['dob'] 
					)
				{			
					$db->updateRecord('sample_log_book_table', 'sample_log_book_id', $_GET['sample_log_book_id'], 'dob', $dfs->ChangeDateFormatSQL($_POST['dob']));
				}

				if 	(
						isset($_POST['soft_lab_num']) &&
						!empty($_POST['soft_lab_num']) &&
						$_POST['soft_lab_num'] !== $sampleLogBookArray[0]['soft_lab_num'] 
					)
				{			
					$db->updateRecord('sample_log_book_table', 'sample_log_book_id', $_GET['sample_log_book_id'], 'soft_lab_num', $_POST['soft_lab_num']);
				}

				if 	(
						isset($_POST['soft_path_num']) &&
						!empty($_POST['soft_path_num']) &&
						$_POST['soft_path_num'] !== $sampleLogBookArray[0]['soft_path_num'] 
					)
				{			
					$db->updateRecord('sample_log_book_table', 'sample_log_book_id', $_GET['sample_log_book_id'], 'soft_path_num', $_POST['soft_path_num']);
				}

				if 	(
						isset($_POST['test_tissue']) &&
						!empty($_POST['test_tissue']) &&
						$_POST['test_tissue'] !== $sampleLogBookArray[0]['test_tissue'] 
					)
				{			
					$db->updateRecord('sample_log_book_table', 'sample_log_book_id', $_GET['sample_log_book_id'], 'test_tissue',$_POST['test_tissue']);
				}

				if 	(
						isset($_POST['block']) &&
						!empty($_POST['block']) &&
						$_POST['block'] !== $sampleLogBookArray[0]['block'] 
					)
				{			
					$db->updateRecord('sample_log_book_table', 'sample_log_book_id', $_GET['sample_log_book_id'], 'block',$_POST['block']);
				}

				if 	(
						isset($_POST['tumor_cellularity']) &&
						!empty($_POST['tumor_cellularity']) &&
						$_POST['tumor_cellularity'] !== $sampleLogBookArray[0]['tumor_cellularity'] 
					)
				{			
					$db->updateRecord('sample_log_book_table', 'sample_log_book_id', $_GET['sample_log_book_id'], 'tumor_cellularity',$_POST['tumor_cellularity']);
				}

				//////////////////////////////////////////////////////
				// Find if patient already added to patient_table use a combination of information
				// added in sample_log_book_table (medical_record_num) and this form (dob) 
				//////////////////////////////////////////////////////
				$searchArray = array();
				$searchArray['medical_record_num'] = $_POST['medical_record_num'];
				$searchArray['dob'] = $dfs->ChangeDateFormatSQL($_POST['dob']);		

				// check if patient already added
				$patient_found_array = $db->listAll('find-patient', $searchArray);

				// if patient not in db add patient.
				if (empty($patient_found_array))
				{
					$add_patient_array = array(
						'last_name'	=> 	$_POST['last_name'],
						'middle_name'	=> 	$_POST['middle_name'],
						'first_name'	=> 	$_POST['first_name'],
						'dob'		=> 	$dfs->ChangeDateFormatSQL($_POST['dob']),
						'medical_record_num'	=> 	$_POST['medical_record_num'],
						'sex'		=> 	$_POST['sex'],
						'user_id'		=> USER_ID
					);

					//////////////////////////////////////////////////////
					// insert into Patient_table
					//////////////////////////////////////////////////////
					$patient_result = $db->addOrModifyRecord('patient_table',$add_patient_array);
					$patient_id = $patient_result[1];
				}

				// find if patient already in db.  If so use patient id
				else
				{
					$patient_id = $patient_found_array[0]['patient_id'];
				}
			
				// Add new ngs visit or update
				if (isset($patient_id))
				{
					$orderable_tests_info = $db->listAll('orderable-tests', $_POST['orderable_tests_id']);
					$_POST['test_name'] = $orderable_tests_info[0]['test_name'];
					$_POST['full_test_name'] = $orderable_tests_info[0]['full_test_name'];
					
					// ensure run_id is included for updates
					if (isset($visitArray[0]) && !empty($visitArray[0]))
					{
						$add_visit_array = $visitArray[0];
					}
					else
					{
						$add_visit_array = array();
					}
					
					$add_visit_array['ngs_panel_id'] = $orderable_tests_info[0]['ngs_panel_id'];
					$add_visit_array['user_id'] = USER_ID;
					$add_visit_array['soft_path_num'] = $_POST['soft_path_num'];
					$add_visit_array['soft_lab_num'] = $_POST['soft_lab_num'];
					$add_visit_array['mol_num'] = $_POST['mol_num']; // This is the MD#
					$add_visit_array['order_num'] = $_POST['order_num'];// This is the mol#  see template/add_test.php for more info
					$add_visit_array['primary_tumor_site'] = $_POST['primary_tumor_site'];
					$add_visit_array['test_tissue'] = $_POST['test_tissue'];
					$add_visit_array['block'] = $_POST['block'];
					$add_visit_array['tumor_cellularity'] = $_POST['tumor_cellularity'];
					$add_visit_array['patient_id'] = $patient_id;
					$add_visit_array['diagnosis_id'] = $_POST['diagnosis_id'];
					$add_visit_array['req_physician'] = $_POST['req_physician'];
					$add_visit_array['req_loc'] = $_POST['req_loc'];
					$add_visit_array['collected'] = $_POST['collected'];
					$add_visit_array['received'] = $_POST['received'];
					$add_visit_array['req_loc'] = $_POST['req_loc'];
					$add_visit_array['sample_type'] = $_POST['sample_type'];
					$add_visit_array['chart_num'] = $_POST['chart_num'];
					$add_visit_array['account_num'] = $_POST['account_num'];
					$add_visit_array['control_type_id'] = 0;
					
					if (isset($_GET['visit_id']))
					{
						$add_visit_array['visit_id'] = $_GET['visit_id'];
					}

					$visit_array = $db->addOrModifyRecord('visit_table',$add_visit_array);

					// add step completed to step_run_xref if not already completed
					$url = $preStepTracker->UpdatePreStepInDb('add_visit', array(), $visit_array[1], USER_ID, RUN_ID, 'add_visit', 'passed'); 

	          		$url = $preStepTracker->UpdatePreStepInDb('add_patient', array(), $visit_array[1], USER_ID, RUN_ID, 'add_patient', 'passed'); 

	          		// add a completed step of sent_out
		          	if ($_POST['sample_type'] === 'Sent Out')
		          	{
		          		// add step completed to step_run_xref if not already completed
		          		$url = $preStepTracker->UpdatePreStepInDb('sent_out', array(), $visit_array[1], USER_ID, RUN_ID, 'sent_out', 'passed');
		          	}

	          		// if visit_id is empty then email users with softpath permissions for patient samples.  Since sample_type is only used for NGS tests that is why this works.  Make sure test is live before sending.
					else if ($_POST['sample_type'] === 'Patient Sample' && !isset($_GET['visit_id']))
					{

						if (isset($visitArray[0]['ngs_panel_id']))
						{
							$ngs_panel_full_info = $db->listAll('ngs-panel-info-by-id', $visitArray[0]['ngs_panel_id']);
		
							if( strtotime($ngs_panel_full_info[0]['go_live_date']) <= strtotime('now') ) 
							{
								$email_utils->emailSoftpathsNewReport($_POST);
							}
						}
						else
						{
							$email_utils->emailSoftpathsNewReport($_POST);
						}
						
					}
				}
			}

			////////////////////////////////////////////////////////////////////////////
			// Add fields which are available for all tests (ordered_test_table)
			////////////////////////////////////////////////////////////////////////////
			$add_ordered_test_array = array(
				'sample_log_book_id'	=> 	$_GET['sample_log_book_id'],
				'orderable_tests_id'	=> 	$_POST['orderable_tests_id'],
				'soft_path_num'		=>	$_POST['soft_path_num'],
				'outside_case_num'		=>	$_POST['outside_case_num'], 
				'exepected_turnaround_time' => $exepected_turnaround_time,
				'user_id'				=> 	USER_ID,
				'ordered_test_id'		=> 	$_POST['ordered_test_id']
			);

			// Make start_date not updatable
			if (isset($orderedTestArray[0]['start_date']))
			{
				$add_ordered_test_array['start_date'] = $orderedTestArray[0]['start_date'];
			}
			else
			{
				$add_ordered_test_array['start_date'] = date('Y-m-d');
			}

			// for ngs samples add visit id
			if (isset($visit_array[1]))
			{
				$add_ordered_test_array['visit_id'] = $visit_array[1];
			}

			// for updates add ordered_test_id
			if (isset($_GET['ordered_test_id']))
			{
				$add_ordered_test_result['ordered_test_id'] = $_GET['ordered_test_id'];
			}

			//////////////////////////////////////////////////////////////////////////////
			// Add consent info to (ordered_test_table)
			//////////////////////////////////////////////////////////////////////////////		
			if ($current_test_info['consent_required'] === 'yes')
			{
				$add_ordered_test_array['consent_info'] = $_POST['consent_info'];

				/////////////////////////////////
				// On March 15th 2021 consent_for_research field was discontinued from collection via 
				// infotrack because it was no longer being collected on the consent form.  Dan Bach 
				// requested for its removal.  Since this form is updatable this field is only deactivated 
				// going forward so old data can still be viewed.
				////////////////////////////////
				if (
						isset($sampleLogBookArray[0]['date_received']) &&
						strtotime($sampleLogBookArray[0]['date_received']) < strtotime('2021-03-15')
					)
				{

					$add_ordered_test_array['consent_for_research'] = $_POST['consent_for_research'];
				}
				$add_ordered_test_array['consent_date'] = $_POST['consent_date'];
			}

			// Change test_status to Sent Out
	     	if ($_POST['sample_type'] === 'Sent Out')
	     	{
	     		$add_ordered_test_array['test_status'] = 'Sent Out';
	     	}

			$add_ordered_test_result = $db->addOrModifyRecord('ordered_test_table', $add_ordered_test_array);	

			if ($add_ordered_test_result[0] == 1)
			{
				$ordered_test_id = $add_ordered_test_result[1];

				/////////////////////////////////////////////////////////////////////////
				// On April 22nd 2021 I started building the ability to add multiple histories.  
				// POST data is provided in this manner:
							
					// 'history_overview' => 
					//     array (size=3)
					//       0 => string 'Weak Positive' (length=13)
					//       1 => string 'Negative' (length=8)
					//       2 => string 'Positive' (length=8)
					//   'previous_positive_id' => 
					//     array (size=3)
					//       0 => string '0' (length=1)
					//       1 => string '0' (length=1)
					//       2 => string '0' (length=1)
					//   'history_mol_num' => 
					//     array (size=3)
					//       0 => string '' (length=0)
					//       1 => string '' (length=0)
					//       2 => string 'md1' (length=3)
					//   'history_soft_path_num' => 
					//     array (size=3)
					//       0 => string '' (length=0)
					//       1 => string '' (length=0)
					//       2 => string 'soft1' (length=5)
					//   'history_test_tissue' => 
					//     array (size=3)
					//       0 => string '' (length=0)
					//       1 => string '' (length=0)
					//       2 => string 'Unknown' (length=7)
					//   'history_date' => 
					//     array (size=3)
					//       0 => string '' (length=0)
					//       1 => string '' (length=0)
					//       2 => string '2021-04-05' (length=10)

				// the required fields are history_overview.  They are not in arrays since they are all radio
				// buttons.
				// Add previous_positive info (previous_positive_table)
				// Previous positive history.  Only some tests need this.  Which are tests which 
				// have previous_positive_required = yes in orderable_tests_table
				// Tests currently are 6/3/2019 (IGH, IGK, FLT3 ITD, FLt3 835)
				/////////////////////////////////////////////////////////////////////////
				if ($current_test_info['previous_positive_required'] === 'yes')
				{
				
					foreach($_POST['history_overview'] as $key => $curr_history)
					{
						
						$previous_positive_array = array(
							'user_id'					=> 	USER_ID,
							'ordered_test_id'			=>	$ordered_test_id,
							'history_overview'			=> 	$curr_history,
							
							'history_mol_num'			=> 	$_POST['history_mol_num'][$key],
							'history_soft_path_num'		=> 	$_POST['history_soft_path_num'][$key],
							'history_test_tissue'		=> 	$_POST['history_test_tissue'][$key],
							'history_date'				=> 	$dfs->ChangeDateFormatSQL($_POST['history_date'][$key]),
							'previous_positive_id'		=>	$_POST['previous_positive_id'][$key]
						);							

						$previous_positive_result = $db->addOrModifyRecord('previous_positive_table', $previous_positive_array);
					}
				}

				/////////////////////////////////////////////////////////////////////////
				// Add Comments.  For ngs samples add comment to reference visit_table
				///////////////////////////////////////////////////////////////////////
				if (isset($visit_array[1]) && isset($_POST['comments']) && !$utils->textareaEmpty($_POST['comments']))
				{
					$commentInsertArray = array();
					$commentInsertArray['user_id'] = USER_ID;
					$commentInsertArray['comment_ref'] =  $visit_array[1];
					$commentInsertArray['comment_type'] =  'visit_table';
					$commentInsertArray['comment'] =  $_POST['comments'];

					$comment_result = $db->addOrModifyRecord('comment_table',$commentInsertArray);
				}
				else if (isset($_POST['comments']) && !$utils->textareaEmpty($_POST['comments']))
				{	
					$commentInsertArray = array();
					$commentInsertArray['user_id'] = USER_ID;
					$commentInsertArray['comment_ref'] =  $ordered_test_id;
					$commentInsertArray['comment_type'] =  'ordered_test_table';
					$commentInsertArray['comment'] =  $_POST['comments'];

					$comment_result = $db->addOrModifyRecord('comment_table',$commentInsertArray);
				}
				

				///////////////////////////
				// Update any ordered_reflex_test_table entry to complete if the test currentrly ordered is  the Last Reflex Test   
				//////////////////////////
				$searchArray = array(
					'orderable_tests_id' 	=>	$_POST['orderable_tests_id'],
					'sample_log_book_id'	=> 	$_GET['sample_log_book_id']
				);
				$reflexTestArray = $db->listAll('reflex-link-sample-test', $searchArray);
		

				if (
						!empty($reflexTestArray) && 
						isset($reflexTestArray[0]['ordered_reflex_test_id']) && 
						isset($reflexTestArray[0]['reflex_value']) &&
						$reflexTestArray[0]['reflex_value'] === 'Last Reflex Test'  &&
						isset($reflexTestArray[0]['status_linked_reflexes_xref_id'])
					)
				{
					
					$db->updateRecord('ordered_reflex_test_table', 'ordered_reflex_test_id', $reflexTestArray[0]['ordered_reflex_test_id'], 'reflex_status', 'complete');

					$db->updateRecord('status_linked_reflexes_xref_table', 'status_linked_reflexes_xref_id', $reflexTestArray[0]['status_linked_reflexes_xref_id'], 'status', 'complete');
				}



				header('Location:'.REDIRECT_URL.'?page=all_sample_tests&sample_log_book_id='.$_GET['sample_log_book_id']);
			}
			else
	          {
				$message = 'Submission failed';

				echo 'add_ordered_test_array<br>';
				var_dump($add_ordered_test_array);

				echo 'add_ordered_test_result:<br>';
				var_dump($add_ordered_test_result);
	          }
	    }while(false);
	}

?>
