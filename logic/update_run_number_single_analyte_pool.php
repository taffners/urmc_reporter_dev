<?php

	if (isset($_GET['single_analyte_pool_id']) && !empty($_GET['single_analyte_pool_id']))
	{
		$samples_in_single_analyte_pool = $db->listAll('samples-in-single-analyte-pool', $_GET['single_analyte_pool_id']);	
		
		if (empty($samples_in_single_analyte_pool))
		{
			header('Location:'.REDIRECT_URL.'?page=new_home');
		}

		$page_title = 'UPDATE RUN NUMBER FOR: '.$samples_in_single_analyte_pool[0]['test_name'].' Run # '.$samples_in_single_analyte_pool[0]['run_number'].'-'.date('y', strtotime($samples_in_single_analyte_pool[0]['start_date'])).' (Tech: '.$samples_in_single_analyte_pool[0]['pool_tech'].')';

		$num_pending_tests = $db->listAll('num-pending-tests-in-single-analyte-pool', $_GET['single_analyte_pool_id']);

		$all_comments = $db->listAll('comments-with-comment-type-variable', array('comment_type' => 'single_analyte_pool_table', 'comment_ref' => $_GET['single_analyte_pool_id']));
	}
	else
	{
		header('Location:'.REDIRECT_URL.'?page=new_home');
	}
	 
	if (
			isset($_POST['update_run_number_submit']) && 
			isset($_POST['comments']) && 
			!empty($_POST['comments']) && 
			isset($_POST['run_number']) && 
			!empty($_POST['run_number']) && 
			$_POST['run_number'] !== $samples_in_single_analyte_pool[0]['run_number']
		)
 	{

 		$db->updateRecord('single_analyte_pool_table', 'single_analyte_pool_id', $_GET['single_analyte_pool_id'], 'run_number', $_POST['run_number']);


 		//////////////////////
		// Add reason for change
		////////////////////
	
		$add_comment = array();
		$add_comment['user_id'] = USER_ID;
		$add_comment['comment_ref'] = $_GET['single_analyte_pool_id'];
		$add_comment['comment_type'] = 'single_analyte_pool_table';
		$add_comment['comment'] = $default_textarea = 'The run number is being updated from '.$samples_in_single_analyte_pool[0]['run_number'].' to '.strval($_POST['run_number']).' because: '.$_POST['comments'];

		$add_comment_result = $db->addOrModifyRecord('comment_table', $add_comment);	

		header('Location:'.REDIRECT_URL.'?page=single_analyte_pool_home&single_analyte_pool_id='.$_GET['single_analyte_pool_id']); 
 	}


 	else if (isset($_POST['update_run_number_submit']))
 	{
 		$message = 'Make sure a reason for changing the run number and run number are filled out';
 	}
?>
