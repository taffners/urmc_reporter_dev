<?php
	$page_title = 'Add a Reflex Tracker';
	$allTestsAvailable = $db->listAll('all-tests-available-with-results-reported-or-sendouts');

	$historyResults = $db->listAll('distinct-test-results');

	if (isset($_GET['reflex_id']))
	{
		$reflexArray = $db->listAll('reflex', $_GET['reflex_id']);
			
		$all_reflex_links = $db->listAll('reflex-links', $_GET['reflex_id']);
	
	}
	else
	{
		$reflexArray = array();
	}

	if (isset($_POST['add_a_reflex_test_tracker_submit']))
	{
		do
		{

			///////////////////////////////////////
			// Check that there are the same number of test_names as test_results
			// Example $_POST
				// array (size=6)
				//   'reflex_name' => string 'CALR (trigger: Negative) reflex to JAK2 (trigger: Negative) reflex to NGS Myeloid' (length=81)
				//   'test_name' => 
				//     array (size=3)
				//       0 => string 'CALR' (length=4)
				//       1 => string 'JAK2' (length=4)
				//       2 => string 'NGS Myeloid' (length=11)
				//   'test_result_0' => string 'Negative' (length=8)
				//   'test_result_1' => string 'Negative' (length=8)
				//   'test_result_2' => string 'Last Reflex Test' (length=16)
				//   'add_a_reflex_test_tracker_submit' => string 'Submit' (length=6)
			///////////////////////////////////////

			$num_results = $utils->num_occurances_substring_in_array(array_keys($_POST), 'test_result');
			if ($num_results !== sizeof($_POST['test_name']))
			{
				$message = 'Make sure you add the same number of triggers and linked tests';
				break;	
			}

			///////////////////////////////////////////////////////////////
			// Check that the last test_result is equal to 'Last Reflex Test' and no others
			//////////////////////////////////////////////////////////////
			$err_found = False;
			foreach ($_POST['test_name'] as $key => $test_name)
			{
				if (
						!isset($_POST['test_result_'.strval($key)]) || 
						empty($_POST['test_result_'.strval($key)])
					)
				{
					$err_found = True;
					$message = 'Make sure to fill in a reflex trigger result for every linked test.';
					break;
				}

				if (intval($key) + 1 == $num_results && $_POST['test_result_'.strval($key)] !== 'Last Reflex Test')
				{
					$err_found = True;
					$message = 'The last linked test needs to be equal to Last Reflex Test no Trigger.';
					break;
				}

				if (intval($key) + 1 != $num_results && $_POST['test_result_'.strval($key)] === 'Last Reflex Test')
				{
					$err_found = True;
					$message = 'Only the last linked test can be equal to Last Reflex Test no Trigger.';
					break;
				}
			}

			if ($err_found)
			{				
				break;
			}


			////////////////////////////////////////////////////////
			// Add to reflex table
			////////////////////////////////////////////////////////
			$add_reflex_array = array();

			if (isset($_GET['reflex_id']))
			{
				$add_reflex_array['reflex_id'] 	= $_GET['reflex_id'];
			}
			else
			{
				// make sure reflex name is not duplicated.  This will ensure the same reflex with the same triggers hasn't already been made.
				$reflex_found = $db->listAll('reflex-by-name', $_POST['reflex_name']);
				
				if (!empty($reflex_found))				
				{
					$message = 'The reflex name provided has already been added.  This means the same tests and triggers have used previously';
					break;
				}
			}
			
			$add_reflex_array['user_id'] = USER_ID;
			$add_reflex_array['reflex_name'] = $_POST['reflex_name'];

			$reflex_result = $db->addOrModifyRecord('reflex_table',$add_reflex_array);

			if($reflex_result[0] == 1)
			{          	
				$reflex_id = $reflex_result[1];

				//////////////////////////////////////////////////
				// Delete all previous links so new ones can be added
				/////////////////////////////////////////////////
				foreach ($all_reflex_links as $link_key => $link)
				{
					$db->deleteRecordById('reflex_test_link_table', 'reflex_test_link_id', $link['reflex_test_link_id']);
				}

				////////////////////////////////////////////////////////
				// Add to reflex test link table
				////////////////////////////////////////////////////////
				
				foreach ($_POST['test_name'] as $key => $test_name)
				{
					// find orderable_tests_id from database.  Test name was used as value instead of orderable_tests_id to be able to build the reflex_name
					$orderable_tests_result = $db->listAll('orderable-tests-by-test-name-only', $test_name);

					
					$add_arr = array();
					$add_arr['user_id'] = USER_ID;
					$add_arr['reflex_id'] = $reflex_id;
					$add_arr['orderable_tests_id'] = $orderable_tests_result[0]['orderable_tests_id'];
					$add_arr['reflex_order'] = $key;
					$add_arr['reflex_value'] = $_POST['test_result_'.strval($key)];

					$reflex_result = $db->addOrModifyRecord('reflex_test_link_table',$add_arr);

				}


				

				header('Location:'.REDIRECT_URL.'?page=setup_reflex_trackers');

			}



		} while(false);
	}

?>