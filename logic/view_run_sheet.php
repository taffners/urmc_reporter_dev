<?php

	$page_title = 'View Run Sheet';

	if (isset($_GET['run_sheet_general_id']))
	{
		$run_sheet_general = $db->listAll('run-sheet-general', $_GET['run_sheet_general_id']);

		$page_title = $run_sheet_general[0]['ngs_run_date'].'_'.$run_sheet_general[0]['sheet_panel_type'].' Run Sheet';

		$run_sheet_data = $db->listAll('run-sheet-rows', $_GET['run_sheet_general_id']);
	}
?>