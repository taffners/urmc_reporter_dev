<?php
	$page_title = 'Set up your homepage';

	$all_users = $db->listAll('get-all-active-users');

	$single_analyte_pool_view = $db->listAll('single-analyte-pool-view', USER_ID);

	$all_single_analyte_pool_view = $db->listAll('single-analyte-pool-view-all');

	if (isset($_POST['setup_home_page_submit']))
	{
		if (!isset($_POST['user_id']) || (empty($_POST['user_id'])) )
		{
			$curr_user_id = USER_ID;
		}
		else
		{
			$curr_user_id = $_POST['user_id'];

			// re find the single analyte_pool_values
			$single_analyte_pool_view = $db->listAll('single-analyte-pool-view', $_POST['user_id']);
		}	

		$add_array = array();

		$add_array['user_id'] = $curr_user_id;
		$add_array['feature'] = 'single_analyte_pool_view';
		$add_array['feature_choice'] = $_POST['single_analyte_pool_view'];

		if (!empty($single_analyte_pool_view) && isset($single_analyte_pool_view[0]['user_homepage_id']))
		{
			$add_array['user_homepage_id'] = $single_analyte_pool_view[0]['user_homepage_id'];
		}

		$db->addOrModifyRecord('user_homepage_table', $add_array);

		header('Location:'.REDIRECT_URL.'?page=new_home');
	}
?>