<?php
	require_once('logic/shared_logic/report_shared_logic.php');
	
	// find everyone with reviewer permission
	$all_reviewers = $db->listAll('users-review-permissions');
	$all_directors = $db->listAll('users-director-permissions');

	//////////////////////////////////////////////////////////////////
	// run message board
	//////////////////////////////////////////////////////////////////
	require_once('logic/shared_logic/message_board.php');

?>