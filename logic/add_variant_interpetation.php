<?php
	require_once('logic/shared_logic/report_shared_logic.php');

	////////////////////////////
	// !!!!!!!!!!!!!!!!!! NO IDEA WHY THIS IS HERE BELOW!!!!!!!!!!!!!!!!!!!!
	///////////////////////////
	if (isset($knowledge_base[0]['genes']) && isset($knowledge_base[0]['coding']) && isset($knowledge_base[0]['amino_acid_change']))
	{
		$variant_name = $knowledge_base[0]['genes'].' '.$knowledge_base[0]['coding'].' ('.$knowledge_base[0]['amino_acid_change'].')';
	}
	else
	{
		$variant_name = 'test_variant_name';
	}

	////////////////////////////
	// !!!!!!!!!!!!!!!!!! NO IDEA WHY THIS IS HERE ABOVE!!!!!!!!!!!!!!!!!!!!
	///////////////////////////


	// for submitting form
	if(isset($_POST['add_variant_interpetation_submit']))
	{
		do
		{

			// find inputs in $_POST which are array's these are the interpts and not the hidden vaules in all forms.  
			$error_found = False;
		
			foreach($_POST as $entry)
			{
				// find which variant_interpts are not empty
				if (is_array($entry)  && $entry['variant_interpt'] !== '' && $entry['empty_data'] !== $entry['variant_interpt'])
				{	
					////////////////////////////////////////////////////
					// check if interpt is already in comment_table	
					////////////////////////////////////////////////////
		               $interpt_adding = array(
		                                   'comment_ref'  =>   $entry['knowledge_id'],
		                                   'comment'      =>   trim($entry['variant_interpt']),
		                                   'user_id'      =>   USER_ID,
		                                   'comment_type' =>   'knowledge_base_table'
		                              );
		               $interpt_found = $db->listAll('comment-by-ref-and-comment', $interpt_adding);

		               // If variant_interpt isn't in comment_table add it 
	                    if (empty($interpt_found))
	               	{
	               		$comment_response = $db->addOrModifyRecord('comment_table',$interpt_adding);

	               		if ($comment_response[0] == 1)
	               		{
	             			
	               			$comment_id = $comment_response[1]; 
	               		}
	               		else
	               		{
	               			$message = 'Something went wrong while adding to run_xref_gene_interpt_table';
							$error_found = True;
							break;
	               		}
	               	}

	               	// if interpt already in db get comment id
	               	else
	               	{
	               		if ( isset($interpt_found[0]['comment_id']) && $interpt_found[0]['comment_id'] != null)
	               		{
	               			$comment_id = $interpt_found[0]['comment_id'];
	               		}
	               	}

	               	// update observed_variant_table interpt_id if comment_id is not null
	               	if (isset($comment_id) && !empty($comment_id) && !$error_found)
	               	{
	          			$observed_record = $db->listAll('observed-variant-by-id', $entry['observed_variant_id']);

						$observed_record[0]['interpt_id'] = strval($comment_id);

						if ($observed_record[0]['interpt_id'] !== $comment_id)
						{
							$observed_record[0]['interpt_id'] = $comment_id;	
							$observed_response = $db->addOrModifyRecord('observed_variant_table',$observed_record[0]);
						}				
	               	}
				}
				else if (is_array($entry) && isset($entry['observed_variant_id']))
				{
					$observed_record = $db->listAll('observed-variant-by-id', $entry['observed_variant_id']);

					if ($observed_record[0]['interpt_id'] != 0)
					{
						$observed_record[0]['interpt_id'] = 0;
					
						$observed_response = $db->addOrModifyRecord('observed_variant_table',$observed_record[0]);
					}
				}
			}

			// make sure the form only updates completed once
			// only update if the step has not been completed 
			$step_status = $stepTracker->StepStatus($completed_steps, $page);

			if (!$error_found && $step_status != 'completed')
			{		
		     	// add step completed to step_run_xref if not already completed
		     	$url = $stepTracker->UpdateStepInDb($page, $completed_steps, RUN_ID, USER_ID);

		     	header('Location:'.REDIRECT_URL.$url.'&'.EXTRA_GETS);
		     }
		     else if (!$error_found)
			{
				$next_step = $stepTracker->FindNextStep($page);
				header('Location:'.REDIRECT_URL.'?page='.$next_step.'&'.EXTRA_GETS);
			}
		} while(False);
	}

	else if (isset($_POST['add_variant_interpetation_skip_submit']))
	{
		$next_step = $stepTracker->SkipStep($completed_steps, $page, RUN_ID, USER_ID);
		header('Location:'.REDIRECT_URL.'?page='.$next_step.'&'.EXTRA_GETS);  
	}
?>
