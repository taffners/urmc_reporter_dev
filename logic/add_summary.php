<?php
	require_once('logic/shared_logic/report_shared_logic.php');
	

	// get interpretations for report
	if (isset($_GET) && isset($_GET['run_id']) && isset($num_snvs))
	{
		$variantArray = $db->listAll('run-variants-included-filter', array('run_id'=>$_GET['run_id'], 'upload_NGS_date' =>$upload_NGS_date, 'ngs_panel_id'=>$visitArray[0]['ngs_panel_id']));	
		$reportArray = $db->listAll('report-interpt', $_GET['run_id']);

		if ($num_snvs === 0)
		{
			$allSummaries = $db->listAll('all-summary-saved');
		}
	}
	else
	{
		header('Location:'.REDIRECT_URL.'?page=home');
	}

	if (!isset($num_snvs))
	{
		header('Location:'.REDIRECT_URL.'?page=home');	
	}

	if (isset($_POST['add_summary_skip_submit']))
	{
		$next_step = $stepTracker->SkipStep($completed_steps, $page, RUN_ID, USER_ID);
		header('Location:'.REDIRECT_URL.'?page='.$next_step.'&'.EXTRA_GETS);  
	}

	if(isset($_POST['add_summary_submit']))
	{

		///////////////////////////////////////////////////////////////////////
		// If no SNV has been found then add summary to no_snv_summary_table if summary not 
		// already in it
		//////////////////////////////////////////////////////////////////////
		if ($num_snvs === 0)
		{
			// find if summary in db already
			$searchResult = $db->listAll('summary-saved', $_POST['summary']);

			if (empty($searchResult))
			{
				// if summary not in db add it
				$summaryKeepArray = array(
						'summary' 	=> 	$_POST['summary'],
						'user_id'		=> 	USER_ID
					);

				$db->addOrModifyRecord('no_snv_summary_table', $summaryKeepArray);	
			}
		}

		// only update if the step has not been completed 
		$step_status = $stepTracker->StepStatus($completed_steps, $page);

		if ($step_status != 'completed')
		{
			// add step completed to step_run_xref if not already completed
     		$url = $stepTracker->UpdateStepInDb($page, $completed_steps, RUN_ID, USER_ID);
		}     	

		// update summary in run info table

		// get all data in the run info for this run
		$run_info = $db->listAll('run-info', $_GET['run_id']);
		$run_info = $run_info[0];

		
		// Add summary to run info table
		if (strlen(trim($_POST['summary'])) === 0)
		{	
			$run_info['summary'] = '';
			
			$db->addOrModifyRecord('run_info_table', $run_info);
		}
		else 
		{
			$run_info['summary'] = trim($_POST['summary']);
			
			$db->addOrModifyRecord('run_info_table', $run_info);
		}

		$next_step = $stepTracker->FindNextStep($page);
		header('Location:'.REDIRECT_URL.'?page='.$next_step.'&'.EXTRA_GETS);
	}


?>
