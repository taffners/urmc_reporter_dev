<?php
	require_once('logic/shared_logic/report_shared_logic.php');

	// find everyone with reviewer permission
	$all_reviewers = $db->listAll('users-review-permissions');
	$all_directors = $db->listAll('users-director-permissions');

	// get interpretations for report
	$reportArray = $db->listAll('report-interpt', $_GET['run_id']);

	$suggested_revisions = $db->listAll('revisions_run', $_GET['run_id']);

	$tiers = $db->listAll('all-tiers');
	$num_reviews = 0;

	$reviewers_emailed = $db->listAll('reviewers-emailed', $_GET['visit_id']);
	
	$directors_notifiying_reviewers = $db->listAll('directors-notifying-reviewers', $_GET['visit_id']);

	foreach ($completed_steps as $key=>$step)
	{
		if ($step['step'] === 'tech_approved')
		{
			$num_reviews++;
		}
	}

	////////////////////////////////////////////////
	// Change status of report.
	////////////////////////////////////////////////
	if(isset($_POST['review_report_submit']))
	{
		// only update if the step has not been completed 
		$step_status = $stepTracker->StepStatus($completed_steps, $page);

		//////////////////////////////////////////////////////////////////
		// FOR REVISING REPORTS!
		// Make sure to not overwrite the time of initial completion for
		// revising reports.
		//////////////////////////////////////////////////////////////////
		if ($step_status != 'completed' && $runInfoArray[0]['status'] === 'revising')
		{
			// add step completed to step_run_xref if not already completed
     		$url = $stepTracker->UpdateStepInDb($page, $completed_steps, RUN_ID, USER_ID);    		
		}     	

		// get all data in the run info for this run
		$run_info = $db->listAll('run-info', $_GET['run_id']);
		$run_info = $run_info[0];

		// update run status if the user is a tech change status from pending to tech approved, if user is a director change from tech approved to complete
		if (strpos($user_permssions, 'tech') !== false)
		{	

			// see if report was already approved. This happens
			// if director pushes confirmed while tech is working on their 
			// approval. In this instance allow tech to add their approval but do 
			// not change run_info_table status to tech approved.
			$confirm_status = $stepTracker->StepStatus($completed_steps, 'confirmed');

			// ONLY ALLOW TECH TO PUSH BTN IF REPORT STEP NOT CONFIRMED.
			if ($confirm_status === 'not_complete')
			{
				$run_info['status'] = 'tech approved'; 
				$db->addOrModifyRecord('run_info_table', $run_info);
			}
		
			// keep track of all techs confirmed
			// add confirmation of time and date and user
			$url = $stepTracker->UpdateStepInDb('tech_approved', $completed_steps, RUN_ID, USER_ID);	

			////////////////////////////////////////////////////////////////
			// If reviewers_emailed is a step. Email every unique director who notified a reviewer.  Also keep track of notifying the director the the sent_email_table with reviewer_finished as email_type
			////////////////////////////////////////////////////////////////	
			if 	(
					isset($directors_notifiying_reviewers) && 
					!empty($directors_notifiying_reviewers)
				)
			{
				$reviewer_name = $db->listAll('user', USER_ID);
								
				$to = $email_utils->getEmailAddresses($directors_notifiying_reviewers);
				$subject = 'A review of NGS reporter# '.$_GET['visit_id'].' was completed';
				$body = 'Hello '.$email_utils->getUserNames($directors_notifiying_reviewers).', 

					'.$reviewer_name[0]['first_name'].' approved NGS report# '.$_GET['visit_id'].'

					'.SITE_TITLE;				

				$email_info = array(
					'email_type' 			=> 	'reviewer_finished',
					'recipient_user_ids' 	=>	$email_utils->getUserIds($directors_notifiying_reviewers),
					'sender_user_id'		=> 	USER_ID,
					'ref_id'				=> 	$_GET['visit_id'],
					'ref_table'			=> 	'visit_table'
				);

				$email_utils->sendEmail($to, $subject, $body, $email_info);
			}
			header("Refresh:0");
		}
		elseif  (strpos($user_permssions, 'director') !== false || $runInfoArray[0]['status'] === 'revising')
		{
			////////////////////////////////////////////////////////////////////////////
			// Email softpath permission users of the newly finished report.  
				// Make sure the user didn't click on the button twice.  This shouldn't happen 
				// since the user is redirected however, they might use the back button and we 
				// do not want to cause spam emails.  Make sure report is reportable before sending email
			////////////////////////////////////////////////////////////////////////////

			if ($runInfoArray[0]['status'] !== 'completed' && isset($visitArray[0]['sample_type'])  && $runInfoArray[0]['report_type'] !== 'completed')// && $visitArray[0]['sample_type'] === 'Patient Sample')
			{
				$user_info = $db->listAll('user', USER_ID);

				$subject = 'Finished NGS report mol# '.$visitArray[0]['order_num'];
				$body = 	"Hello,

					NGS report is finished.

					Test Name: ".$visitArray[0]['ngs_panel']."
					mol#: ".$visitArray[0]['order_num']."
					MD#: ".$visitArray[0]['mol_num']."
					soft Lab#: ".$visitArray[0]['soft_lab_num']."
					soft Path#: ".$visitArray[0]['soft_path_num']."

					Please login to the ".SITE_TITLE." to obtain the PDF of the completed report.

					1. The PDF can be obtained by clicking the green PDF button next to the report in the Completed Reports table at the bottom of the home page.  
					2. Then choose the report without header to add to patient chart.  

					If you have any questions pertaining to this visit please contact ".$user_info[0]['first_name']." ".$user_info[0]['last_name'].' ('.$user_info[0]['email_address'].")

					Thank you for your help.

					URMC Molecular Team";

				$mail_result = $email_utils->emailSoftpaths($body,$subject);
			}

			// for reports under revision add change_summary to revise_table
			if ($runInfoArray[0]['status'] === 'revising')
			{
				$revise_array = array(
					'user_id'			=> 	USER_ID,
					'visit_id'		=> 	$_GET['visit_id'],
					'change_summary'	=>	$_POST['change_summary']
				);
			
				$db->addOrModifyRecord('revise_table', $revise_array);
			}

			///////////////////////////////////////////////////////////////
			// change status field in run_info_table to completed.  This will 
			// close the report from being edited again and it will be visible
			// in the completed list on the home page.  By using update record
			// instead of replace the time_stamp will be updated.
			///////////////////////////////////////////////////////////////
			$db->updateRecord('run_info_table', 'run_id', $_GET['run_id'], 'status', 'completed');

			//////////////////////////////////////////////////////////////////
			// Change status of single analyte pool to complete
			//////////////////////////////////////////////////////////////////
			if (isset($_GET['single_analyte_pool_id']) && !empty($_GET['single_analyte_pool_id']))
			{
				$db->updateRecord('single_analyte_pool_table', 'single_analyte_pool_id', $_GET['single_analyte_pool_id'], 'status', 'complete');
			}
			
			///////////////////////////////////////////////////////////////
			// Change status of ordered_test. This will change the test_status and 
			// add a finish_date for the turnaround_time
			///////////////////////////////////////////////////////////////
			// Find ordered_test by the visit_id.  If present proceed by updating it.
			$ordered_test = $db->listAll('ordered-test-by-visit-id', $_GET['visit_id']);

			if (isset($ordered_test[0]) && !empty($ordered_test[0]))
			{
				$ordered_test_update_array = $ordered_test[0];
				$ordered_test_update_array['test_status'] = 'complete';
				$ordered_test_update_array['finish_date'] = date('Y-m-d');
				
				// add an expected_turnaround_time
				$turn_around_time = $db->listAll('turn-around-time', $ordered_test[0]['orderable_tests_id']);

				$ordered_test_update_array['expected_turnaround_time'] = $turn_around_time[0]['turnaround_time'];

				$db->addOrModifyRecord('ordered_test_table', $ordered_test_update_array);
			}		

			/////////////////////////////////////////////////////////////////
			// Add the wet bench techs, reviewers, and directors to users_performed_task_table
				// Add sign_out and revise to task_table
					// wet_bench and analysis already in task_table
				// wet_bench in user_run_task_xref table using run_id linker
				// analysis
					// Should I add only techs approved or any user that completes a step?
				// sign out
					// under sign out add wet_bench, analysis, and sign_out tasks
				// revise
					// under revise only add revise				
			/////////////////////////////////////////////////////////////////

			// make sure step not already confirmed
			$step_status = $stepTracker->StepStatus($completed_steps, 'confirmed');
			
			if ($step_status != 'completed' && $runInfoArray[0]['status'] !== 'revising')
			{
				// add confirmation of time and date and user
				$url = $stepTracker->UpdateStepInDb('confirmed', $completed_steps, RUN_ID, USER_ID);	
				header('Location:'.REDIRECT_URL.'?page=download_report&'.EXTRA_GETS);
			}
			else
			{
				header('Location:'.REDIRECT_URL.'?page=download_report&'.EXTRA_GETS);
			}
		}
		header("Refresh:0");
	}

	// send reviewers an email 
	if (isset($_POST['notify_reviewers_submit']) && !empty($_POST['selected_reviewers']))
	{
		////////////////////////////////////////////////////////////////
		// Note who 
		////////////////////////////////////////////////////////////////		
		$emailer_name = $db->listAll('user', USER_ID);

		$url = $stepTracker->UpdateStepInDb('reviewers_emailed', $completed_steps, RUN_ID, USER_ID);

		////////////////////////////////////////////////////////////////
		// Send all reviewers an email
		////////////////////////////////////////////////////////////////
		$notified_users = '';

		$to = '';
		$recipient_user_ids = array();

		foreach ($_POST['selected_reviewers'] as $key => $user_id)
		{		
			array_push($recipient_user_ids, $user_id);

			// find reviewer user information
			$user_info = $db->listAll('user', $user_id);

			if ($to !== '')
			{
				$to.=',';
			}

			$to.= $user_info[0]['email_address'];

			// keep track of users notified so the user can be notified via $message var
			if ($notified_users !== '')
			{
				$notified_users.=', ';
			}
			$notified_users.=$user_info[0]['first_name'];
		}
		
		$subject = 'Please Review NGS reporter# '.$_GET['visit_id'];
		$body = 'Hello '.$user_info[0]['first_name'].',

			'.$emailer_name[0]['first_name'].' would like to notify you that NGS report# '.$_GET['visit_id'].' is now ready for your review.  Please login to the URMC NGS Reporter and review the report at your earliest convenience.  Make sure you click looks good submit. 

			Thanks

			'.SITE_TITLE;
		
		$email_info = array(
					'email_type' 			=> 	'reviewers_emailed',
					'recipient_user_ids' 	=>	$recipient_user_ids,
					'sender_user_id'		=> 	USER_ID,
					'ref_id'				=> 	$_GET['visit_id'],
					'ref_table'			=> 	'visit_table'
		);


		$email_utils->sendEmail($to, $subject, $body, $email_info);

		// $message = 'The following reviewers were notified to review the report '.$_GET['visit_id'].': '.$notified_users;

		// Refresh page.  This will reduce the risk of submitting the form more than once and now that the sent_email_table is implemented (2019-Dec-5) the user will be updated by the Notified Reviewers box.
		header('Refresh:0;'.$_SERVER['REQUEST_URI'].'#notified-reviewers-box');
	}

	//////////////////////////////////////////////////////////////////
	// run message board
	//////////////////////////////////////////////////////////////////
	require_once('logic/shared_logic/message_board.php');
?>
