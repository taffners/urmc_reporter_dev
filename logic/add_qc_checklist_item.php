<?php
	$page_title = 'Add a QC Checklist Item';

	///////////////////////////////
	// set up form
	///////////////////////////////
	if (isset($_GET['check_list_validation_step_id']))
	{
		$checkListArray = $qc_db->listAll('qc_db-qc-checklist-item', $_GET['check_list_validation_step_id']);

	}
	else
	{
		$checkListArray = array();
	}


	///////////////////////////////
	// submit form
	///////////////////////////////
	if (isset($_POST['add_qc_checklist_item_submit']))
	{
		
		$add_arrary = array();

		if (isset($_GET['check_list_validation_step_id']))
		{
			$add_arrary['check_list_validation_step_id'] = $_GET['check_list_validation_step_id'];
		}

		$add_arrary['field_type'] = $_POST['field_type'];
		$add_arrary['description'] = $_POST['description'];
		$add_arrary['step_name'] = $_POST['step_name'];
		$add_arrary['category'] = $_POST['category'];

		$page_result = $qc_db->addOrModifyRecord('check_list_validation_step_table',$add_arrary);

		header('Location:'.REDIRECT_URL.'?page=all_qc_checklist_item');


	}
?>