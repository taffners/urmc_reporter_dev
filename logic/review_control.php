<?php
	require_once('logic/shared_logic/report_shared_logic.php');
	$page_title = 'Review Control';
	$legend_title = 'Review Control';

	$all_directors = $db->listAll('users-director-permissions');

	if (isset($visitArray[0]['control_type_id']) && isset($_GET['run_id']))
	{
		$searchArray = array(
			'control_type_id' 	=> 	$visitArray[0]['control_type_id'],
			'run_id'			=>	$_GET['run_id']
		);
		
		// expected control variants
		$expected_control_variants = $db->listAll('control-expected-by-observed', $searchArray);

		$unexpected_control_variants = $db->listAll('unexpected-control-variants', $searchArray);

		$control_info = $db->listAll('control-info', $visitArray[0]['control_type_id']);

		$summary_search_array = array(
			'control_type_id'	=>	$visitArray[0]['control_type_id'],
			'ngs_panel_id'		=>	$visitArray[0]['ngs_panel_id']
		);

		$summary_control_vaf = $db->listAll('summary-control-vaf', $summary_search_array);

		// add name of control to page title
		$page_title = 'Historic Control Information for '.$control_info[0]['control_name'];
		$legend_title = 'Review Control '.$control_info[0]['control_name'];
	}

	if (isset($_POST['send_notice_submit']))
	{
		if (isset($_POST['message_board_memebers']))
		{
			// get email addresses for all members selected.
			$to = '';
			foreach ($_POST['message_board_memebers'] as $key => $member_userid)
			{
				$member_info = $db->listAll('user', $member_userid);
				$user_info = $db->listAll('user', USER_ID);

				if ($to !== '')
				{
					$to.=',';
				}

				$to.= $member_info[0]['email_address'];

				$to = $to;				
			}

			$subject = 'Control '.$control_info[0]['control_name'].' Report # '.$_POST['visit_id'];
			$body = $user_info[0]['first_name'].' Would like to notify you that the control '.$control_info[0]['control_name'].' Report # '.$_POST['visit_id'].' is ready for your review.';
			
			$email_utils->sendEmail($to, $subject, $body, 'skip');

			$message = $user_info[0]['first_name'].' notified '.$to.' that the control '.$control_info[0]['control_name'].' Report # '.$_POST['visit_id'].' is ready for review. '.date("Y-m-d h:i:sa");

				/////////////////////////////////////////////////////
				# Add message to comment table
				/////////////////////////////////////////////////////
				$message_comment_array = array(
					'user_id'		=> 	USER_ID,
					'comment_ref'	=> 	RUN_ID,
					'comment_type'	=> 	'message_board',
					'comment'		=> 	$message	
				);

				$comment_add_result =$db->addOrModifyRecord('comment_table', $message_comment_array);

				header('Location:'.REDIRECT_URL.'/?page=message_board&run_id='.RUN_ID.'&visit_id='.$_POST['visit_id'].'&patient_id='.$_POST['patient_id'].'#message_board');
		}
		else
		{
			$message = 'Select a Director to notify';
		}
	}

	if (isset($_POST['pass_control_submit']) && isset($_GET['run_id']))
	{
		// change run_info_table status to pass_control
		$db->updateRecord('run_info_table', 'run_id', $_GET['run_id'], 'status', 'pass_control');

		// redirect to home page.
		header('Location:'.REDIRECT_URL.'?page=home');
	}

	else if (isset($_POST['fail_control_submit']) && isset($_GET['run_id']))
	{
		// change run_info_table status to pass_control
		$db->updateRecord('run_info_table', 'run_id', $_GET['run_id'], 'status', 'fail_control');

		// redirect to home page.
		header('Location:'.REDIRECT_URL.'?page=home');
	}
?>
