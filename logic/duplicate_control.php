<?php
	if (
			!isset($_GET['control_type_id']) || 
			!isset($user_permssions) ||
			strpos($user_permssions, 'login_enhanced') == false
		)
	{
		header('Location:'.REDIRECT_URL.'?page=new_home');
	}

	$curr_control = $db->listAll('current-controls', $_GET['control_type_id']);

	$page_title = 'Duplicate Control '.$curr_control[0]['control_name'];


	if (isset($_POST['duplicate_control_submit']))
	{
		do
		{

			// make sure that the control_name is unique
			$previous_controls_with_control_name = $db->listAll('controls-by-name', $_POST['control_name']);

			if (!empty($previous_controls_with_control_name))
			{
				$message = 'Control names are required to be unique';
				break;
			}

			// Add new control
			$add_arr = array();
			$add_arr['user_id'] = USER_ID;
			$add_arr['type'] = $_POST['type'];
			$add_arr['control_name'] = $_POST['control_name'];
			$add_arr['description'] = $_POST['description'];
			$add_arr['catalog_number'] = $_POST['catalog_number'];
			$add_arr['dummy_last_name'] = $_POST['control_name'];
			$add_arr['dummy_first_name'] = $_POST['control_name'];
			$add_arr['dummy_medical_record_num'] = $_POST['control_name'];
			$add_arr['dummy_sex'] = isset($_POST['dummy_sex']) ? $_POST['dummy_sex'] :'';
			$add_arr['dummy_dob'] = '1901-01-01';

			// add new control and get control_type_id
			$add_result = $db->addOrModifyRecord('control_type_table', $add_arr);
			
			if ($add_result[0] == 1)
			{
				$control_type_id = $add_result[1];

				// Find all expected control variants for the control that is being duplicated
					// 0 => 
					//     array (size=6)
					//       'genes' => string 'NRAS' (length=4)
					//       'coding' => string 'c.183A>T' (length=8)
					//       'amino_acid_change' => string 'p.Gln61His' (length=10)
					//       'min_accepted_frequency' => string '9.5' (length=3)
					//       'max_accepted_frequency' => string '14.2' (length=4)
					//       'target_frequency_range' => null
				$expected_vars = $db->listAll('all-expected_control-variants-for-control', $_GET['control_type_id']);

				// add all of the variants to the new control_type_id
				foreach ($expected_vars as $key => $v)
				{
					$v['user_id'] = USER_ID;
					$v['control_type_id'] = $control_type_id;

					$add_var_result = $db->addOrModifyRecord('expected_control_variants_table', $v);
				}
			}
			else
			{
				$message = 'Something went wrong while adding the control.';
				break;
			}

			header('Location:'.REDIRECT_URL.'?page=ngs_control_home');


		}while(false);
	}
?>