<?php
	$page_title = 'Download Run Template';

	require_once('logic/shared_logic/library_pool.php');

	if (isset($_GET['pool_chip_linker_id']))
	{
		$pool_chip_linker_array = $db->listAll('pool-chip-linker-by-id', $_GET['pool_chip_linker_id']);	
		
	}
	else
	{
		$email_utils->emailAdminsProblemURL(__FILE__.' Error downloading run template !! Line #: '.__LINE__.' pool_chip_linker_id missing.');
	}


	if (
			isset($_POST['pool_download_run_template_submit']) && 
			isset($_GET['pool_chip_linker_id']) && 
			isset($_GET['ngs_panel_id']) && 
			isset($_GET['page']) && 
			isset($pool_chip_linker_array[0]) &&
			isset($_POST['ngs_run_date']) &&
			!empty($_POST['ngs_run_date']) 
		)
	{		
		// Add new ngs_run_date
		$update_array = $pool_chip_linker_array[0];
		$update_array['ngs_run_date'] = $dfs->ChangeDateFormatSQL($_POST['ngs_run_date']);

		$update_result = $db->addOrModifyRecord('pool_chip_linker_table', $update_array);

		// add step completed to step_run_xref if not already completed
		$url = $poolStepTracker->UpdatePoolStepInDb($_GET['page'], $completed_steps, $_GET['pool_chip_linker_id'], USER_ID, $_GET['ngs_panel_id']);
		header('Location:'.REDIRECT_URL.$url);		
	}
	else if (
				isset($_POST['pool_download_run_template_submit']) && 
				(
					!isset($_POST['ngs_run_date']) ||
					empty($_POST['ngs_run_date']) 
				)
			)
	{
		$message = 'Fill in sequencing date';
	}

?>