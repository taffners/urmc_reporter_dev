<?php

	$page_title = 'Version History';

	$all_possible_diagrams = $utils->getAllFilesInDir('about/diagrams');

	if (isset($_POST['version_info_submit']))
	{
		// Add new version info to database
		$add_array = $_POST;
		$add_array['user_id'] = USER_ID;

		$response = $db->addOrModifyRecord('version_history_table',$add_array);
		
		header("Refresh:0");
	}

?>
