<?php
	require_once('logic/shared_logic/report_shared_logic.php');
	
	$tiers = $db->listAll('all-tiers');

	if(isset($_POST['add_tiers_submit']))
	{

		// make sure the form only updates completed once
				// only update if the step has not been completed 
		$step_status = $stepTracker->StepStatus($completed_steps, $page);

		if ($step_status != 'completed')
		{
			
			// add step completed to step_run_xref if not already completed
     		$url = $stepTracker->UpdateStepInDb($page, $completed_steps, RUN_ID, USER_ID);
     		header('Location:'.REDIRECT_URL.$url.'&patient_id='.$_GET['patient_id'].'&visit_id='.$_GET['visit_id']);
		}    
		else
		{
			$next_step = $stepTracker->FindNextStep($page);
			header('Location:'.REDIRECT_URL.'?page='.$next_step.'&run_id='.$_GET['run_id'].'&patient_id='.$_GET['patient_id'].'&visit_id='.$_GET['visit_id']);
		}
	}
?>

