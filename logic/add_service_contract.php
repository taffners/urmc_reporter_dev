<?php
	
	if (isset($_GET['instrument_id']))
	{
		$instrumentArray = $db->listAll('instrument-by-id', $_GET['instrument_id']);
	}
	else
	{
		$message = 'Something is wrong and instrument was not selected. Do not proceed.';
		$instrumentArray = array();
	}	


	if (isset($_GET['instrument_service_contract_id']))
	{
		$page_title = 'Update Service Contract';

		$serviceArray = $db->listAll('instrument-service-contract-by-id', $_GET['instrument_service_contract_id']);
	}
	else
	{
		$page_title = 'Add a Service Contract';

		$serviceArray = array();
	}

	if (isset($_POST['add_service_contract_submit']) && isset($_GET['instrument_id']))
	{
		$insert_array = array(
			'instrument_id' 	=> 	$_GET['instrument_id'],
			'user_id' 		=> 	USER_ID,
			'expiration_date' => $dfs->ChangeDateFormatSQL($_POST['expiration_date']),
			'po' 			=>	$_POST['po'],
			'description' 		=>	$_POST['description'],
			'contract_num' 	=>	$_POST['contract_num'],
			'quote_num' 		=>	$_POST['quote_num'],
			'start_date' 		=>	$dfs->ChangeDateFormatSQL($_POST['start_date']),
			'price' 			=>	$_POST['price']
		);

		if (isset($_GET['instrument_service_contract_id']))
		{
			$insert_array['instrument_service_contract_id'] = $_GET['instrument_service_contract_id'];
		}

		$add_result = $db->addOrModifyRecord('instrument_service_contract_table',$insert_array);
		
		if ($add_result[0] == 1)
		{
			// redirect to list of all instruments
			header('Location:'.REDIRECT_URL.'?page=service_contract&instrument_id='.$_GET['instrument_id']);
		}
		else
		{
			var_dump($insert_array);
			var_dump($add_result);
			$email_utils->emailAdminsProblemURL('$add_result[0] != 1');
		}
	}

?>