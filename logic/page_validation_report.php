<?php

	if (isset($_GET['page_validation_id']) && isset($_GET['page_id']))
	{

		$pageArray = $qc_db->listAll('qc_db-qc-page-info', $_GET['page_id']);
		
		$page_title = 'Validation Report for the page '.$pageArray[0]['page_name'].'. '.SITE_TITLE.' '.VERSION.' ('.date('Y-m-d').')';
		$page_validation_info = $qc_db->listAll('qc_db-page-validation-info', $_GET['page_validation_id']);
	
		$qc_checklist_items = $qc_db->listAll('qc_db-qc-checklist-items-for-validation', $_GET['page_validation_id']);
// var_dump($qc_checklist_items);	
	}
	else
	{
		$page_title = 'Something went wrong!';	
	}


?>