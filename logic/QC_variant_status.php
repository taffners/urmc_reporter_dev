<?php
	require_once('logic/shared_logic/qc_variant_shared_logic.php');
	$page_title = 'Confirm All Steps Completed';
	$step_status = $qcVarStepTracker->StepStatus($completed_steps, $_GET['page']);

	// find everyone with reviewer permission
	$all_reviewers = $db->listAll('users-review-permissions');
	$all_directors = $db->listAll('users-director-permissions');

	//////////////////////////////////////////////////////////////////
	// run message board
	//////////////////////////////////////////////////////////////////
	require_once('logic/shared_logic/message_board.php');

	if (isset($_POST['QC_variant_status_submit']))
	{
		// make sure the form only updates completed once
		// only update if the step has not been completed 

		if ($step_status != 'completed')
		{			
			// add step completed to step_run_xref if not already completed
     		$url = $qcVarStepTracker->UpdateStepInDb($page, $completed_steps, RUN_ID, USER_ID);

     		$completed_steps_report = $db->listAll('all-complete-steps', $_GET['visit_id']);
     		$url = $stepTracker->UpdateStepInDb('qc_variant', $completed_steps_report, RUN_ID, USER_ID);
     		header("Refresh:0");     		
		}    
	}	
?>
