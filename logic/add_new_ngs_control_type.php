<?php
	if (
			!isset($user_permssions) ||
			strpos($user_permssions, 'login_enhanced') == false
		)
	{
		header('Location:'.REDIRECT_URL.'?page=new_home');
	}

	$curr_control = array();
	
	$page_title = 'Add New NGS Type Control';
	if (isset($_GET['control_type_id']))
	{
		$curr_control = $db->listAll('current-controls', $_GET['control_type_id']);	

		$page_title = 'Update Control '.$curr_control[0]['control_name'];
	}

	if (isset($_POST['add_new_ngs_control_type_submit']))
	{
		do
		{
			$add_arr = array();

			if (!isset($_GET['control_type_id']))
			{
				// make sure that the control_name is unique
				$previous_controls_with_control_name = $db->listAll('controls-by-name', $_POST['control_name']);

				if (!empty($previous_controls_with_control_name))
				{
					$message = 'Control names are required to be unique';
					break;
				}	
			}
			else
			{
				$add_arr['control_type_id'] = $_GET['control_type_id'];
			}
			
			// Add new control
			
			$add_arr['user_id'] = USER_ID;
			$add_arr['type'] = $_POST['type'];
			
			$add_arr['control_name'] = $_POST['control_name'];
			$add_arr['description'] = $_POST['description'];
			$add_arr['catalog_number'] = $_POST['catalog_number'];
			$add_arr['dummy_last_name'] = $_POST['control_name'];
			$add_arr['dummy_first_name'] = $_POST['control_name'];
			$add_arr['dummy_medical_record_num'] = $_POST['control_name'];
			$add_arr['dummy_sex'] = isset($_POST['dummy_sex']) ? $_POST['dummy_sex'] :'';
			$add_arr['dummy_dob'] = '1901-01-01';

			// add new control and get control_type_id
			$add_result = $db->addOrModifyRecord('control_type_table', $add_arr);

			header('Location:'.REDIRECT_URL.'?page=ngs_control_home');


		}while(false);
	}
?>