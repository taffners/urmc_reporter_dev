<?php
	

	if (isset($_GET['instrument_id']))
	{
		$instrumentArray = $db->listAll('instrument-by-id', $_GET['instrument_id']);
		$validationArray = $db->listAll('all-validations-by-instrument-id', $_GET['instrument_id']);
	
		$page_title ='Validations for '.$utils->instrumentInputName($instrumentArray[0]);	
	}
	else
	{
		$message = 'Something is wrong and instrument was not selected. Do not proceed.';
		$validationArray = array();
		$page_title ='Validations';
	}	

?>