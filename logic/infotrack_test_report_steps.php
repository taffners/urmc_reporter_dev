<?php
	if (
			isset($user_permssions) && 
			(strpos($user_permssions, 'admin') === false && 
			strpos($user_permssions, 'manager') === false) &&
			isset($_GET['orderable_tests_id'])
		)
	{ 
		header('Location:'.REDIRECT_URL.'?page=new_home');
	}

	$page_title = 'Test Report Building Steps';

	$all_reporting_flow_steps = $db->listAll('all-reporting-flow-steps');
	$curr_test_reporting_flow_steps = $db->listAll('reporting-flow-steps-by-test-id',$_GET['orderable_tests_id']);

	$orderableTestArray = $db->listAll('orderable-tests', $_GET['orderable_tests_id']);
	
	if (isset($_POST['infotrack_test_report_steps_submit']))
	{
		// Delete all reporting steps already added for the test
		foreach ($curr_test_reporting_flow_steps as $key => $reporting_step)
		{
			$db->deleteRecordById('reporting_steps_by_test_table', 'reporting_steps_by_test_id', $reporting_step['reporting_steps_by_test_id']);
		}

		$count = 0;
		foreach($_POST['reporting_steps_by_test'] as $reporting_flow_steps_id => $add_status)
		{
			if ($add_status === 'yes')
			{
				$count ++;
				$add_array = array();
				$add_array['user_id'] = USER_ID;
				$add_array['reporting_flow_steps_id'] = $reporting_flow_steps_id;
				$add_array['orderable_tests_id'] = $_GET['orderable_tests_id'];
				$add_array['ngs_panel_id'] = $orderableTestArray[0]['ngs_panel_id'];

				$add_array['step_order'] = $count;	

				$add_result = $db->addOrModifyRecord('reporting_steps_by_test_table',$add_array);			
			}
		}

		header("Refresh:0");
	}


?>