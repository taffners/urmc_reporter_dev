
<?php
     
     if (isset($_GET['knowledge_id']))
     {

     	$knowledge_base = $db->listAll('knowledge-by-id', $_GET['knowledge_id']);	

   		$ttk_vw_cols = $db->listAll('get-table-col-names', 'knowledge_base_table');

   		$variant_name = $knowledge_base[0]['genes'].' '.$knowledge_base[0]['coding'].' ('.$knowledge_base[0]['amino_acid_change'].')';

   		$page_title = 'Variant Tiers for: <br>'.$variant_name;

          $all_tiers = $db->listAll('all-tiers');
          $all_tissues = $db->listAll('all-tissues');

          $tiers = $db->listAll('tier-user', $_GET['knowledge_id']); 
         
     }

     else
     {
     	$message = 'Search did not proceed';
     	$page_title = 'Variant Tiers';	
     }
     
     
     if (isset($_POST['submit-add-tier']))
     {
          unset($_POST['submit-add-tier']);

          // Make sure tier or tissue are not empty
          if (empty($_POST['tier_id']) || empty($_POST['tissue_id']))
          {
               $message = 'Please fill in both a tier and a tissue';
          }
          
          else
          {
               $tier = $db->listAll('find-tier', $_POST);

               // if the tier is not already added for this variant add it
               if (empty($tier))
               {
                    $result = $db->addOrModifyRecord('variant_tier_xref', $_POST);
                    header("Refresh:0");
               }  
               else
               {
                    $message = 'This tier has already been added for this variant';
               }             
          }
     }

?>