<?php
	$page_title = 'Search Single Analyte Pools';

	// send all searched items so search results
	if (isset($_POST['submit']))
	{
		$redirect_page = 'single_analyte_pool_search_results';

		// send search to patient_search_results
		$url = $utils->SearchPostArray($redirect_page, $_POST);

		if ($url !== 'OOPS! Please enter a search parameter.')
		{
			header($url);
		}
		else
		{
			$message = $url;
		}		
	}

?>