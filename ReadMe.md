# **Molecular Diagnostics Infotrack**

# What is it?

Molecular Diagnostics Infotrack is a tool to speed up Clinical Molecular Analysis at University of Rochester Medical Center. It is transforming into a one stop, start to finish software tool where samples are tracked from login to report generation. Single analyte and NGS tests are all logged into Molecular Diagnostics Infotrack providing a test tracking system which allows managers the ability to easily produce monthly reports and track audits to evaluate laboratory productivity. Molecular Diagnostics Infotrack transforms into an analysis and report building system for NGS tests. Allowing users to access the growing knowledge base along with integrated publicly available databases.

# CAUTION

> * All reports are made on the fly therefore if you edit data manually which is in are report without sending the reports to revision it will affect all the signed our reports.  Therefore be very careful editing any data which is present in the following tables.  I might miss some so look before editing.
>> * knowledge_base_table
>> * patient_table 
>> * visit_table
>> * ngs_panel
>> * observed_variant_table
>> * Any test setup tables 
>> * genes_covered_by_panel_table
>> * comment_table
> * For security reasons this file should be moved but I placed it here since I'm the only developer and it was most likely not going to be missed here.

# Location

> *  server [pathlamp.urmc-sh.rochester.edu](pathlamp.urmc-sh.rochester.edu)

>> *  Web Live site

>>> *   https://pathlamp.urmc-sh.rochester.edu/urmc_reporter

>> * Web dev site
>>> *  https://pathlamp.urmc-sh.rochester.edu/devs/urmc_reporter

>> * From command line on server

>>> * Web Live site
>>>> *  /var/www/html/devs/urmc_reporter

>>> * Web dev site
>>>> *  /var/www/html/devs/urmc_reporter

> * IO wait trouble shooting reference

>> * https://bencane.com/2012/08/06/troubleshooting-high-io-wait-in-linux/

# Version Control

> * This project is under git version control.  If you do not know how to use git (coursera)[https://www.coursera.org/learn/version-control-with-git/home/welcome] has a good course for an introduction to git.

> * (Here's another good source)[https://www.atlassian.com/git/tutorials/learn-git-with-bitbucket-cloud]

> * git remote dev site

	[staffner@pathlamp urmc_reporter]$ pwd
	/var/www/html/devs/urmc_reporter
	[staffner@pathlamp urmc_reporter]$ git remote -v
	origin  https://taffners@bitbucket.org/taffners/urmc_reporter_dev.git (fetch)
	origin  https://gitlab.circ.rochester.edu/molecular_infotrack/molecular_infotrack.git (push)
	origin  https://taffners@bitbucket.org/taffners/urmc_reporter_dev.git (push)

> * git remote live site

	[staffner@pathlamp urmc_reporter]$ pwd
	/var/www/html/urmc_reporter
	[staffner@pathlamp urmc_reporter]$ git remote -v
	origin  https://taffners@bitbucket.org/taffners/urmc_reporter_dev.git (fetch)
	origin  https://taffners@bitbucket.org/taffners/urmc_reporter_dev.git (push)

> * Tips:
>> * pushing a new version live

>>> * I push the dev site to the git repo and then pull in the live site under sudo permissions.
>>> * Then follow directions under How to release a new version of infotrack section of this readme file

>> * view commit graph

	git log --oneline --graph

>> * Use tags to sink git repo with versions of MD Infotrack
>>> * List all tags

	git tag

>>> * Make a tag

	git tag v2.1.75 

>>> * push tags and commits to remote repo

	git push origin master --tag

>> * Branches

>>> * Create branch

>>> * Display branches

>>>> * Local

	git branch

>>>> * Display local and tracking branch names

	git branch --all

>>> * Checkout branch

>>> * Delete branch ids which are already merged in

	git branch -d feature2

>> * Merging
>>> * Make small frequent merges to avoid merging problems

>>> * Types of merges

>>>> * Fast forward Merge

>>>> * Merge Commit

	git merge --no-ff <branch> -m 'merge message'

>>>> * Rebasing commit

>>>>> * Moves a branch to the tip of another branch while erasing version history and changing sha-1 commit ids.  Therefore be very careful when using.  Might be useful for working of a dev branch when master had a hot fix performed.

>>>> * Squash Merge

>>>> * Merge only 1 file at a time

>>>>> * https://dev.to/alexruzenhack/git-merge-specific-file-from-another-branch-dl

>>>> * Amending last commit (WARNING Changes sha-1 commit ids)

	git commit --amend -m 'commit message without typo'

>> * Create a pull request

	git push --set-upstream origin branch_name

# Schema

> * All my schema diagrams can be found at make sure to open the file with diagrams.net.  I set this file to be accessible by anyone with this link because I didn't have anyone to share it with.  I will change the permissions to not be accessible by September 1st 2021 for security reasons. You should make a copy of the file for yourself.

	https://drive.google.com/file/d/1R0wbpD2sMCVdyZl5b0ozJDd8eZuuUsyC/view?usp=sharing

# Description of tools

* RedHat

>> * Notes:
>>> * [Check CPU utilization](https://blogs.oracle.com/pranav/how-to-find-out-cpu-utilization-in-linux)

	[root@pathlamp ~]# hostnamectl
	Static hostname: pathlamp
         Icon name: computer-vm
           Chassis: vm
        Machine ID: ceedfadf174348a781e05ac4a164ce63
           Boot ID: d83a609bbb234cb2b6fa26bca5eff2da
    Virtualization: vmware
    Operating System: RHEL
       CPE OS Name: cpe:/o:redhat:enterprise_linux:7.3:GA:server
            Kernel: Linux 3.10.0-514.el7.x86_64
      Architecture: x86-64

	[root@pathlamp ~]# rpm --query redhat-release-server
	redhat-release-server-7.3-7.el7.x86_64

	[root@pathlamp ~]# for i in $(ls /etc/*release); do echo ===$i===; cat $i; done
	===/etc/os-release===
	NAME="Red Hat Enterprise Linux Server"
	VERSION="7.3 (Maipo)"
	ID="rhel"
	ID_LIKE="fedora"
	VERSION_ID="7.3"
	PRETTY_NAME=RHEL
	ANSI_COLOR="0;31"
	CPE_NAME="cpe:/o:redhat:enterprise_linux:7.3:GA:server"
	HOME_URL="https://www.redhat.com/"
	BUG_REPORT_URL="https://bugzilla.redhat.com/"

	REDHAT_BUGZILLA_PRODUCT="Red Hat Enterprise Linux 7"
	REDHAT_BUGZILLA_PRODUCT_VERSION=7.3
	REDHAT_SUPPORT_PRODUCT="Red Hat Enterprise Linux"
	REDHAT_SUPPORT_PRODUCT_VERSION="7.3"
	===/etc/redhat-release===
	Red Hat Enterprise Linux Server release 7.3 (Maipo)
	===/etc/system-release===
	Red Hat Enterprise Linux Server release 7.3 (Maipo)

> * Database
>> * [Mariadb](https://mariadb.org/)
>>> * db info
>>>> * dev -> urmc_reporter_dev_db
>>>> * live -> urmc_reporter_db
>>>> * For database schema see 
>>>> * The name of MD Infotrack previously was urmc_reporter that is why the db is still called this	
>>>> * Because of miscommunication there are a few db column names called something different than what the user sees
>>>>> * visit_table -> dbname(mol_num) == username(md#) ex. 100-19
>>>>> * visit_table -> dbname(order_num) == username(mol_num) ex. 19-MOL185
>>>>> * sample_log_book_table -> dbname(mol_num) == username(md#) ex. 100-19

	MariaDB [(none)]> status
	--------------
	mysql  Ver 15.1 Distrib 5.5.56-MariaDB, for Linux (x86_64) using readline 5.1

	Connection id:          65425
	Current database:
	Current user:           root@localhost
	SSL:                    Not in use
	Current pager:          stdout
	Using outfile:          ''
	Using delimiter:        ;
	Server:                 MariaDB
	Server version:         5.5.56-MariaDB MariaDB Server
	Protocol version:       10
	Connection:             Localhost via UNIX socket
	Server characterset:    latin1
	Db     characterset:    latin1
	Client characterset:    utf8
	Conn.  characterset:    utf8
	UNIX socket:            /var/lib/mysql/mysql.sock
	Uptime:                 134 days 1 hour 15 min 30 sec

	Threads: 1  Questions: 1409051  Slow queries: 0  Opens: 462  Flush tables: 2  Open tables: 280  Queries per second avg: 0.121

>>> * Before manually editing any data in urmc_reporter_db save a back up
>>> * [useful back up tutorial](https://www.linode.com/docs/databases/mysql/use-mysqldump-to-back-up-mysql-or-mariadb/)

	$ sudo su

	$ sudo mysqldump -u root urmc_reporter_db --single-transaction --quick --lock-tables=false > /var/www/urmc_reporter_data/db_backups/urmc_reporter_db_2019_08_21.sql

> * Server
>> * [Apache 2.4.6](https://www.apache.org/)

	[root@pathlamp ~]# httpd -v
	Server version: Apache/2.4.6 (Red Hat Enterprise Linux)
	Server built:   May 28 2018 16:19:32

>>> * Notes:
>>>> * On RedHat apache2 == httpd
>>>> * restart server

	$ sudo systemctl restart httpd

>>>> * reboot server

	sudo reboot

>>>> * [Check apache status](https://www.tecmint.com/check-apache-httpd-status-and-uptime-in-linux/) 

	$ systemctl status httpd

> * PHP5
>> * [php5](https://www.php.net/manual/en/intro-whatis.php)

	[root@pathlamp ~]# php --version
	PHP 5.4.16 (cli) (built: Jan 23 2018 07:26:54)
	Copyright (c) 1997-2013 The PHP Group
	Zend Engine v2.4.0, Copyright (c) 1998-2013 Zend Technologies
	    with Xdebug v2.2.7, Copyright (c) 2002-2015, by Derick Rethans

	[root@pathlamp ~]# rpm -qa | grep php
	php-cli-5.4.16-45.el7.x86_64
	php-mbstring-5.4.16-45.el7.x86_64
	php-bcmath-5.4.16-45.el7.x86_64
	php-tcpdf-dejavu-sans-fonts-6.2.13-1.el7.noarch
	php-php-gettext-1.0.12-1.el7.noarch
	php-pdo-5.4.16-45.el7.x86_64
	phpMyAdmin-4.4.15.10-2.el7.noarch
	php-common-5.4.16-45.el7.x86_64
	php-process-5.4.16-45.el7.x86_64
	php-mysql-5.4.16-45.el7.x86_64
	php-tidy-5.4.16-7.el7.x86_64
	php-pear-1.9.4-21.el7.noarch
	php-devel-5.4.16-45.el7.x86_64
	php-fedora-autoloader-1.0.0-1.el7.noarch
	php-tcpdf-6.2.13-1.el7.noarch
	php-pecl-xdebug-2.2.7-1.el7.x86_64
	php-5.4.16-45.el7.x86_64
	php-xml-5.4.16-45.el7.x86_64
	php-gd-5.4.16-45.el7.x86_64

>> * [fpdf](http://www.fpdf.org/)
>> * [fpdf code 128 barcodes](http://www.fpdf.org/en/script/script88.php)
>> * [phpMussel-1 version 2.0](https://github.com/phpMussel/phpMussel/blob/v1/LICENSE.txt)
>>> * An ideal solution for shared hosting environments, where it's often not possible to utilize or install conventional anti-virus protection solutions, phpMussel is a PHP script designed to **detect trojans, viruses, malware and other threats**
>>> * Installed at: /var/www/phpMussel-1/loader.php

>>> Installs via [composer](https://getcomposer.org/doc/00-intro.md)
		
		"picqer/php-barcode-generator": "^0.3.0",
        "phpoffice/phpword": "^0.17.0",
        "zendframework/zend-escaper": "^2.5",
        "zendframework/zend-stdlib": "^2.5"

> * JavaScript
>> * [RequireJS 2.3.2](https://github.com/requirejs/requirejs/blob/master/)
>> * [jQuery  v3.5.1](jquery.org/license)
>> * [jQuery UI - v1.12.1](http://jqueryui.com)
>>> * Includes: widget.js, position.js, data.js, disable-selection.js, effect.js, effects/effect-blind.js, effects/effect-bounce.js, effects/effect-clip.js, effects/effect-drop.js, effects/effect-explode.js, effects/effect-fade.js, effects/effect-fold.js, effects/effect-highlight.js, effects/effect-puff.js, effects/effect-pulsate.js, effects/effect-scale.js, effects/effect-shake.js, effects/effect-size.js, effects/effect-slide.js, effects/effect-transfer.js, focusable.js, form-reset-mixin.js, jquery-1-7.js, keycode.js, labels.js, scroll-parent.js, tabbable.js, unique-id.js, widgets/accordion.js, widgets/autocomplete.js, widgets/button.js, widgets/checkboxradio.js, widgets/controlgroup.js, widgets/datepicker.js, widgets/dialog.js, widgets/draggable.js, widgets/droppable.js, widgets/menu.js, widgets/mouse.js, widgets/progressbar.js, widgets/resizable.js, widgets/selectable.js, widgets/selectmenu.js, widgets/slider.js, widgets/sortable.js, widgets/spinner.js, widgets/tabs.js, widgets/tooltip.js
>> * [Bootstrap v4.4.1](http://getbootstrap.com)
>> * [math.js version 7.1.0](https://github.com/josdejong/mathjs)
>> * [DataTables 1.10.21](http://datatables.net)
>> * [moment.js version : 2.27.0](momentjs.com)
>>> * Parse, validate, manipulate, and display dates and times in Javascript.  Needed for bootstrap-datetimejs
>> * [bootstrap-datetimejs version : 4.15.35](https://github.com/Eonasdan/bootstrap-datetimepicker)
>> * [hovercard v2.4](http://designwithpc.com/Plugins/Hovercard#demo)
>> * [CryptoJS v3.1.2](code.google.com/p/crypto-js)
>> * [Bootstrap-select v1.13.9](https://developer.snapappointments.com/bootstrap-select)
>> * [JsBarcode v3.6.0](https://lindell.me/JsBarcode)
>> * [Bootstrap Tourist v0.3.3](https://www.npmjs.com/package/bootstrap-tourist)

> * CSS
>> * [Bootstrap v4.4.1](http://getbootstrap.com)
>> * [Bootstrap Tourist v0.3.3](https://www.npmjs.com/package/bootstrap-tourist)
>> * [jQuery UI - v1.12.1](http://jqueryui.com)
>>> * Includes: core.css, accordion.css, autocomplete.css, menu.css, button.css, controlgroup.css, checkboxradio.css, datepicker.css, dialog.css, draggable.css, resizable.css, progressbar.css, selectable.css, selectmenu.css, slider.css, sortable.css, spinner.css, tabs.css, tooltip.css, theme.css
>> * [DataTables 1.10.21](http://cdn.datatables.net/1.10.21)
>> * [Bootstrap-select v1.13.9](https://developer.snapappointments.com/bootstrap-select)
>>> * Used to replace html datalist since bootstrap 4 removed it.
>> * [Font Awesome Free 5.4.2](https://fontawesome.com)
>>> * Used to replace bootstrap glyphicons

# Ingoing and outgoing activity from the pathlamp server

> * API's/services
>> * Controlled in class.api_utils.php 
>>> * [CIViC genes](https://civicdb.org/api/genes/)
>>> * [CIViC variants](https://civicdb.org/api/variants)
>>> * [ensembl](https://rest.ensembl.org/vep/human/region/)
>>> * [Torrent Server](http://10.149.58.208/rundb/api/v1/results/)
>>> * [torrrent suite](http://10.149.58.208/rundb/api/v1/torrentsuite/)
>>> * [torrrent analysismetrics](http://10.149.58.208/rundb/api/v1/analysismetrics/)
>>> * [torrrent qualitymetrics](http://10.149.58.208/rundb/api/v1/qualitymetrics/)
>>> * [torrrent libmetrics](http://10.149.58.208/rundb/api/v1/libmetrics/)
>>> * [torrrent experiment](http://10.149.58.208/rundb/api/v1/experiment/)
>>> * [torrrent pluginresult](http://10.149.58.208/rundb/api/v1/pluginresult/)

>> * API's running in thermo_backup_ion_reporter_run.py and check_IR_API.py
>>> * [Ion Reporter](https://10.149.59.209/api/v1/analysis?format=json&name=)

>> * API's running in utils.py
>>> * [mutalyzer](https://mutalyzer.nl/json/runMutalyzer?variant=)

>> * Testing now
>>> * [Torrent plannedexperiment](http://10.149.58.208/rundb/api/v1/plannedexperiment/)

>> * API's/services also running on the pathlamp server
>>> * [google charts](https://www.gstatic.com/charts/loader.js)
>>> * [geoplugin](http://www.geoplugin.net/json.gp?ip=)

> Emails are being sent out from the pathlamp server via PHP mail function
>> * [PHP Mail](https://www.php.net/manual/en/function.mail.php)

# Access 

> * Web app
>> * With admin access users can be added in admin page and permissions can be set there also.  See MD Infotrack user guide for more information. 

> * commandline
>> * use putty or WinSCP to access pathlamp.urmc-sh.rochester.edu

# Add a new gene to an NGS Panel

> * genes_covered_by_panel_table stores all data for the gene covered table in the NGS reports.  This includes gene, exon, accession_num, date_gene_added.  The original plan for the OFA was to go live will all genes.  Therefore all of the information for each covered gene was added to the database.  However, we actually are going live with a subset of 11 genes.  Therefore the date_gene_added was changed to 2040-01-01.  This way data will not be deleted but will not be added to the reports.  To add these genes back into the report in the future just edit this date to the current start date.  Make sure you put the actual date it is added so it does affect historic reports.

# Add a new test

> * NGS test
>> * This is a lengthly process which currently can not be done without re-writting the pre ngs steps.  This is because Zoltan and Bill Crowe would not let me develop this portion for the myeloid panel.  They wanted to stick with an error prone method of excel analysis.  Therefore I stopped developing this part of Infotrack to focus on things I could actually develop with head ache.  I left the university because of non forward looking behavior like this.   

> * Single analyte test
>> * Use the setup > tests function in the app

# Update cosmic ID 

> * If the cosmic number was never used before it will be accessible to update via the knowledge base with admin or director permissions.  Otherwise follow the following instructions.

> * Backup database first

	$ sudo su
	# sudo mysqldump -u root urmc_reporter_db --single-transaction --quick --lock-tables=false > /var/www/urmc_reporter_data/db_backups/urmc_reporter_db_2020_02_06.sql
	# exit

> * Access database

	$ sudo mysql -u root
	MariaDB [(none)]> use urmc_reporter_db;


> * Since reports are generated every time from the database we need to make sure we are not going to change a report that was already signed out. So we need to check to see if the variant is was already used for a different run_id.

	SELECT ovt.observed_variant_id AS ov_id, ovt.run_id, vt.visit_id, ovt.genes, ovt.coding, ovt.amino_acid_change AS aa_change FROM observed_variant_table ovt LEFT JOIN visit_table vt ON vt.run_id = ovt.run_id WHERE ovt.genes ="TP53" AND ovt.coding = "c.422G>A" AND ovt.amino_acid_change = "p.Cys141Tyr";

	+-------+--------+----------+-------+----------+-------------+
	| ov_id | run_id | visit_id | genes | coding | aa_change |
	+-------+--------+----------+-------+----------+-------------+
	| 14683 | 884 | 889 | TP53 | c.422G>A | p.Cys141Tyr |
	+-------+--------+----------+-------+----------+-------------+
	1 row in set (0.00 sec)

> * If multiple reports are found with this variant and they have been signed out find them all in Inftotrack and send them to revision.  This will save the report.  Then follow the steps below and sign out all of the revised reports making a note of why the report was sent to revision.

> * Since we only have one row the cosmic ID in the knowledge_base_table can be updated without causing any harm to previously signed out reports. (DO NOT CONTINUE IF FOUND IN MULTIPLE REPORTS. All of the reports will have to be sent to revise mode so the original report will be save in a hard copy.)Now find the variant in the knowledge_base_table

	SELECT kbt.knowledge_id AS k_id, kbt.genes, kbt.coding, kbt.amino_acid_change AS aa_change, kbt.cosmic FROM knowledge_base_table kbt WHERE kbt.genes ="TP53" AND kbt.coding = "c.422G>A" AND kbt.amino_acid_change = "p.Cys141Tyr";

	+------+-------+----------+-------------+--------+
	| k_id | genes | coding | aa_change | cosmic |
	+------+-------+----------+-------------+--------+
	| 447 | TP53 | c.422G>A | p.Cys141Tyr | 43708 |
	+------+-------+----------+-------------+--------+
	1 row in set (0.00 sec) 

> * Ensure there's only one. Then change the cosmic number. Be very certain that you are selecting the correct variant in the knowlege_base_table. You could end up inadvertently changing many or all entries.

	UPDATE knowledge_base_table SET cosmic = 131470 WHERE genes ="TP53" AND coding = "c.422G>A" AND amino_acid_change = "p.Cys141Tyr" AND knowledge_id = 447 AND cosmic = 43708;

# How to release a new version of infotrack

> * Find all updated pages by using git status on the command line.
> * Then inside of infotrack perform QC on each page that was updated and write a report.  Sometimes a new module will need to be added with a Schema diagram.  I will write this assuming you added a new module to infotrack.  If that is not the case you can skip the module section.

>> * The following instructions require Admin permissions in infotrack.

>> * Make the module diagram.  I use draw.io see Schema section of this file.  Save it as a png. Add it to the about/diagram folder on pathlamp.  I transfer the png with WinSCP. 

>> * Access the QC home page from the green check mark on the tool bar.  

>> * In the Admin QC section add a module and link to your diagram that you just added to the pathlamp server.

>> * Then go to the QA by pages (page=all_qc_pages) from the QC home.  

>> * Click on add all missing qc pages if you added new pages.  The new pages will automatically be added.

>> * Search for the page you added or updated.  

>> * Update the page.

>> * Then Add a validation. 

>> * Once you updated the page and added a validation for each page updated you are ready to push the new code to the live server.

>>> * Push to the live server via git.  

>>> * Then from the QC home page click on version history and update the version history with a short description of the update and a git commit id.

# How to update the passwords and API tokens

> * All API tokens and passwords are saved in the classes/class.api_utils.php file.  Update this file to update the passwords and API tokens.

> * Once a year the ion reporter requires an update of the password.  This includes a required update of the API token.  It does not say this but it is required.  

>> * Login to Ion Reporter website.  
>> * Click on the gear icon. Then click on manage API Token.
>> * Click on generate.  Then click on copy.
>> * Replace the Ion_Reporter[api_key] in the $server variable in the classes/class.api_utils.php file with the newly generated token.
>> * If any pools already produced fatal errors follow the directions in Something went wrong with the thermo backup of OFA data  

# Something went wrong with the thermo backup of OFA data

> * An error rarely occurs while backing up the OFA data however, sometimes it does occur. Infotrack is actually set up to error out with a fatal error if anything unexpected is encountered.  I designed it this way so something is not overlooked.  Errors normally occur related to changes in data.  

> * Files produced in the backup will help you figure out what went wrong.  It will most likely require an error handling update to the code to fix this.  To find the backup files go to W:\fs-ngs\ion_torrent\Run99_347_20210330_B_VWY5T_DAGJ06971_315/ir_backup.log file.
>>> * This file will most likely look like the following: 

	********************
	Input Command: 
		python/thermo_backup_ion_reporter_run.py -o /media/storage/fs-ngs/ion_torrent/Run99_347_20210330_B_VWY5T_DAGJ06971_315 -api_server https://10.149.59.209/api/v1/ -api_token Mjk5OWY1YmJlODc1Y2FkYTgzMGVkZDJiZjdhNDI4MjA3MmYwMmIxN2IyOWZlYzVhMThmNzAzYTQ4OTliOWRjMg -sample_list B_1573-21_K722B889_2669_3025_286 B_1579-21_K723A396_2673_3026_286 B_1580-21_K7238365_2674_3027_286 B_1604-21_K7242254_2676_3028_286 B_1611-21_K7248184_2680_3029_286 B_1612-21_K7247203_2681_3030_286 B_1613-21_K7249787_2698_3031_286 B_NA12877_v1-14_2695_3032_286 B_AcroMetrix_v1-8_2696_3034_286 B_NTC_v1-5_2697_3033_286 -library_pool_id 347
	********************

	sample backing up: /media/storage/fs-ngs/ion_torrent/Run99_347_20210330_B_VWY5T_DAGJ06971_315
	B_1573-21_K722B889_2669_3025_286
	{u'error': u'Authentication failed. Please use valid Authorization token/key '}

>>> * You will need to rerun the input command on the command line to fix the fatal error. If there are two chips in the pool you will have to do this for both chips.  Copy this command and replace the api_token with the newly generated api_token.  

>>> * On the command line (I use putty to access the command line) navigate to the python/thermo_backup_ion_reporter_run.py file.  Then run the command using python.  For instance:

	[staffner@pathlamp urmc_reporter]$ pwd
	/var/www/html/devs/urmc_reporter
	[staffner@pathlamp urmc_reporter]$ python python/thermo_backup_ion_reporter_run.py -o /media/storage/fs-ngs/ion_torrent/Run99_347_20210330_B_VWY5T_DAGJ06971_315 -api_server https://10.149.59.209/api/v1/ -api_token YmUyMzU5NzA1NzlmNjBkNDYyMTk3NjVmOTlmNTdjOWFiOGQwZTgxNWFkMzg5ODMzMTJkODU2NTI4Mjg1MGRiYQ -sample_list B_1573-21_K722B889_2669_3025_286 B_1579-21_K723A396_2673_3026_286 B_1580-21_K7238365_2674_3027_286 B_1604-21_K7242254_2676_3028_286 B_1611-21_K7248184_2680_3029_286 B_1612-21_K7247203_2681_3030_286 B_1613-21_K7249787_2698_3031_286 B_NA12877_v1-14_2695_3032_286 B_AcroMetrix_v1-8_2696_3034_286 B_NTC_v1-5_2697_3033_286 -library_pool_id 347

# Report was revised but old report is not visible on download_report page

> * Reports are made on the fly in infotrack.  Therefore if a report was signed out a backup PDF of the signed out report is made before a report can be revised.  These signed out reports are placed in the the /var/www/urmc_reporter_data/report_versions/ folder.

> * Reports are placed in a folder inside the report_versions folder with the following format MRN_E000000_MD_0000-21_lastName

> * If the MRN, MD#, or Last name of the patient was updated during the revision Infotrack will not be able to find this backup and it will not know there was a revision.  To fix this a copy of the folder will need to be made while updating the changed field.  Here's an example:

>> * The MRN was changed from E3491040 to E2421395.

	$ cd /var/www/urmc_reporter_data/report_versions/
	$ cp -R MRN_E3491040_MD_6190-21_Rettig MRN_E2421395_MD_6190-21_Rettig

# Reverse Approval process

> * Sometimes Paul wants to revert the approval process of a report and not revise the report.  The difference is during revise a backup of the current report will be saved and the new report will say the report was revised.

> * how to revert a report
>> * Look up report in Infotrack go to reports -> view or Revise All completed Reports.
>> * Search for the report by mol number.  For this example it was mol number 21-MOL1437
>> * DO NOT CLICK ON RED REVISE BUTTON !!!!!!!!!!!!!!!!!!!!!!!  If your permission settings show this button.
>> * Click on Green PDF button
>> * the URL Will now show all the relevant info to look this sample up in the database.

	https://pathlamp.urmc-sh.rochester.edu/urmc_reporter/?page=download_report&run_id=3915&visit_id=4195&patient_id=2997&header_status=on&revise=on&ngs_panel_id=2&orderable_tests_id=12&ordered_test_id=31974&sample_log_book_id=15303

>> * Login to pathlamp via command line
>> * back up db

	$ su sudo
	$ sudo mysqldump -u root urmc_reporter_db --single-transaction --quick --lock-tables=false > /var/www/urmc_reporter_data/db_backups/urmc_reporter_db_2021_11_19.sql
	$ exit

>> * login to the database

	$ sudo mysql -u root
	MariaDB [(none)]> use urmc_reporter_db;

>> * Check that the run ID is correct

	MariaDB [urmc_reporter_db]> SELECT * FROM run_info_table WHERE run_id=3915;

>> * From the output of the above query double check that the data matches the report in question.
>> * Also double check that the status equals complete.
>> * Set the status to pending

	MariaDB [urmc_reporter_db]> UPDATE run_info_table SET status = 'pending' WHERE run_id=3915;

>> * Double check that the report is now in the outstanding reports section.
>>> * Go to Infotrack APP.
>>> * With NGS permissions click on the Outstanding button under reports
>>> * Search for report using the mol number.  If the report appears it has been reverted correctly.
>>> * Email Paul to notify him that the report has been reverted for him.

# Version History

> **_How to access version history_** 

> * From inside the web app the version history can access from the version history button on the tool bar.

> * From the source code

>> * update about folder git log
	
	$  pwd 
	/var/www/html/devs/urmc_reporter
	$ git log > about/version_log.txt

>> * Access version_log.txt in the about folder

# Authors
>> * Samantha Taffner - Software Development